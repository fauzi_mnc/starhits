
$(window).on("scroll", function() {
    if($(window).scrollTop() > 50) {
        $(".col-header-content").style.padding = "0!important";
    } else {
        //remove the background property so it comes transparent again (defined in your css)
       $(".col-header-content").style.padding = "15px!important";
    }
});