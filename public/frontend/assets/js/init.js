// ===== Scroll to Top ==== 
$(window).scroll(function () {
  if ($(this).scrollTop() >= 500) { // If page is scrolled more than 50px
      $('#return-to-top').fadeIn(200); // Fade in the arrow
  } else {
      $('#return-to-top').fadeOut(200); // Else fade out the arrow
  }
});
$('#return-to-top').click(function () { // When arrow is clicked
  $('body,html').animate({
      scrollTop: 0 // Scroll to top of body
  }, 500);
});

// Slick Slider

$(".slider-homepage").slick({
          lazyLoad: 'ondemand', // ondemand progressive anticipated
          infinite: true,
          autoplay: true,
          autoplaySpeed: 5000,
          arrows: false,
          dots: false
          });

// Manually refresh positioning of slick
$('.slider-homepage').slick('setPosition');

$(".slider-top-videos").slick({
  lazyLoad: 'ondemand',
  variableWidth: true,
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  speed: 300,
  arrow: true,
  prevArrow:"<img class='a-left control-c prev slick-prev' src='"+baseUrl+"/frontend/assets/img/control/left.png'>",
  nextArrow:"<img class='a-right control-c next slick-next' src='"+baseUrl+"/frontend/assets/img/control/right.png'>",
  responsive: [
  {
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: true,
      dots: false
    }
  },
  {
    breakpoint: 640,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 320,
    settings: { 
      slidesToShow: 1,
      slidesToScroll: 1
        
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
]
  });

$(".slider-top-hits").slick({
  lazyLoad: 'ondemand',
  variableWidth: true,
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  speed: 300,
  arrow: true,
  prevArrow:"<img class='a-left control-c prev slick-prev' src='"+baseUrl+"/frontend/assets/img/control/left.png'>",
  nextArrow:"<img class='a-right control-c next slick-next' src='"+baseUrl+"/frontend/assets/img/control/right.png'>",
  responsive: [
  {
    breakpoint: 1024,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1,
      infinite: true,
      dots: false
    }
  },
  {
    breakpoint: 640,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 320,
    settings: { 
      slidesToShow: 1,
      slidesToScroll: 1
        
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
]
  });

$(".slider-top-pro").slick({
  lazyLoad: 'ondemand',
  variableWidth: true,
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  speed: 300,
  arrow: true,
  prevArrow:"<img class='a-left control-c prev slick-prev' src='"+baseUrl+"/frontend/assets/img/control/left.png'>",
  nextArrow:"<img class='a-right control-c next slick-next' src='"+baseUrl+"/frontend/assets/img/control/right.png'>",
  responsive: [
  {
    breakpoint: 1024,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1,
      infinite: true,
      dots: false
    }
  },
  {
    breakpoint: 640,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 320,
    settings: { 
      slidesToShow: 1,
      slidesToScroll: 1
        
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
]
  });

$(".slider-recommended-videos").slick({
  lazyLoad: 'ondemand',
  variableWidth: true,
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  speed: 300,
  arrow: true,
  prevArrow:"<img class='a-left control-c prev slick-prev' src='"+baseUrl+"/frontend/assets/img/control/left.png'>",
  nextArrow:"<img class='a-right control-c next slick-next' src='"+baseUrl+"/frontend/assets/img/control/right.png'>",
  responsive: [
  {
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: true,
      dots: false
    }
  },
  {
    breakpoint: 640,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 480,
    settings: {
      dots: false,
      slidesToShow: 1,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 320,
    settings: { 
      slidesToShow: 1,
      slidesToScroll: 1
        
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
]
  });

$(".slider-mid-sub-channels-videos").slick({
  lazyLoad: 'ondemand',
  variableWidth: true,
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  speed: 300,
  arrow: true,
  prevArrow:"<img class='a-left control-c prev slick-prev' src='"+baseUrl+"/frontend/assets/img/control/left.png'>",
  nextArrow:"<img class='a-right control-c next slick-next' src='"+baseUrl+"/frontend/assets/img/control/right.png'>",
  responsive: [
  {
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: true,
      dots: true
    }
  },
  {
    breakpoint: 640,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 320,
    settings: { 
      slidesToShow: 1,
      slidesToScroll: 1
        
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
]
  });

$(".slider-latest-channels-name-videos").slick({
  lazyLoad: 'ondemand',
  variableWidth: true,
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  speed: 300,
  arrow: true,
  prevArrow:"<img class='a-left control-c prev slick-prev' src='"+baseUrl+"/frontend/assets/img/control/left.png'>",
  nextArrow:"<img class='a-right control-c next slick-next' src='"+baseUrl+"/frontend/assets/img/control/right.png'>",
  responsive: [
  {
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: true,
      dots: false
    }
  },
  {
    breakpoint: 640,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 320,
    settings: { 
      slidesToShow: 1,
      slidesToScroll: 1
        
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
]
  });

$(".slider-popular-channels-name-videos").slick({
  lazyLoad: 'ondemand',
  variableWidth: true,
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  speed: 300,
  arrow: true,
  prevArrow:"<img class='a-left control-c prev slick-prev' src='"+baseUrl+"/frontend/assets/img/control/left.png'>",
  nextArrow:"<img class='a-right control-c next slick-next' src='"+baseUrl+"/frontend/assets/img/control/right.png'>",
  responsive: [
  {
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: true,
      dots: false
    }
  },
  {
    breakpoint: 640,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 320,
    settings: { 
      slidesToShow: 1,
      slidesToScroll: 1
        
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
]
  });

$(".slider-mid-sub-channels-series").slick({
  lazyLoad: 'ondemand',
  variableWidth: true,
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  speed: 300,
  arrow: true,
  prevArrow:"<img class='a-left control-c prev slick-prev' src='"+baseUrl+"/frontend/assets/img/control/left.png'>",
  nextArrow:"<img class='a-right control-c next slick-next' src='"+baseUrl+"/frontend/assets/img/control/right.png'>",
  responsive: [
  {
    breakpoint: 1024,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1,
      infinite: true,
      dots: false
    }
  },
  {
    breakpoint: 640,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 320,
    settings: { 
      slidesToShow: 1,
      slidesToScroll: 1
        
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
]
  });

$(".slider-latest-channels-name-series").slick({
  lazyLoad: 'ondemand',
  variableWidth: true,
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  speed: 300,
  arrow: true,
  prevArrow:"<img class='a-left control-c prev slick-prev' src='"+baseUrl+"/frontend/assets/img/control/left.png'>",
  nextArrow:"<img class='a-right control-c next slick-next' src='"+baseUrl+"/frontend/assets/img/control/right.png'>",
  responsive: [
  {
    breakpoint: 1024,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1,
      infinite: true,
      dots: false
    }
  },
  {
    breakpoint: 640,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 320,
    settings: { 
      slidesToShow: 1,
      slidesToScroll: 1
        
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
]
  });

$(".slider-popular-channels-name-series").slick({
  lazyLoad: 'ondemand',
  variableWidth: true,
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  speed: 300,
  arrow: true,
  prevArrow:"<img class='a-left control-c prev slick-prev' src='"+baseUrl+"/frontend/assets/img/control/left.png'>",
  nextArrow:"<img class='a-right control-c next slick-next' src='"+baseUrl+"/frontend/assets/img/control/right.png'>",
  responsive: [
  {
    breakpoint: 1024,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1,
      infinite: true,
      dots: false
    }
  },
  {
    breakpoint: 640,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 320,
    settings: { 
      slidesToShow: 1,
      slidesToScroll: 1
        
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
]
  });

$(".slider-latest-creators-videos").slick({
  lazyLoad: 'ondemand',
  variableWidth: true,
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  speed: 300,
  arrow: true,
  prevArrow:"<img class='a-left control-c prev slick-prev' src='"+baseUrl+"/frontend/assets/img/control/left.png'>",
  nextArrow:"<img class='a-right control-c next slick-next' src='"+baseUrl+"/frontend/assets/img/control/right.png'>",
  responsive: [
  {
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: true,
      dots: false
    }
  },
  {
    breakpoint: 640,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 320,
    settings: { 
      slidesToShow: 1,
      slidesToScroll: 1
        
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
]
  });

$(".slider-popular-creators-videos").slick({
  lazyLoad: 'ondemand',
  variableWidth: true,
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  speed: 300,
  arrow: true,
  prevArrow:"<img class='a-left control-c prev slick-prev' src='"+baseUrl+"/frontend/assets/img/control/left.png'>",
  nextArrow:"<img class='a-right control-c next slick-next' src='"+baseUrl+"/frontend/assets/img/control/right.png'>",
  responsive: [
  {
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: true,
      dots: false
    }
  },
  {
    breakpoint: 640,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 320,
    settings: { 
      slidesToShow: 1,
      slidesToScroll: 1
        
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
]
  });

$(".slider-latest-creators-series").slick({
  lazyLoad: 'ondemand',
  variableWidth: true,
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  speed: 300,
  arrow: true,
  prevArrow:"<img class='a-left control-c prev slick-prev' src='"+baseUrl+"/frontend/assets/img/control/left.png'>",
  nextArrow:"<img class='a-right control-c next slick-next' src='"+baseUrl+"/frontend/assets/img/control/right.png'>",
  responsive: [
  {
    breakpoint: 1024,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1,
      infinite: true,
      dots: false
    }
  },
  {
    breakpoint: 640,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 320,
    settings: { 
      slidesToShow: 1,
      slidesToScroll: 1
        
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
]
  });

$(".slider-popular-creators-series").slick({
  lazyLoad: 'ondemand',
  variableWidth: true,
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  speed: 300,
  arrow: true,
  prevArrow:"<img class='a-left control-c prev slick-prev' src='"+baseUrl+"/frontend/assets/img/control/left.png'>",
  nextArrow:"<img class='a-right control-c next slick-next' src='"+baseUrl+"/frontend/assets/img/control/right.png'>",
  responsive: [
  {
    breakpoint: 1024,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1,
      infinite: true,
      dots: false
    }
  },
  {
    breakpoint: 640,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 320,
    settings: { 
      slidesToShow: 1,
      slidesToScroll: 1
        
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
]
  });

$(".slider-recommended-creators-videos").slick({
  lazyLoad: 'ondemand',
  variableWidth: true,
  infinite: true,
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: true,
  speed: 300,
  arrow: true,
  prevArrow:"<img class='a-left control-c prev slick-prev' src='"+baseUrl+"/frontend/assets/img/control/left.png'>",
  nextArrow:"<img class='a-right control-c next slick-next' src='"+baseUrl+"/frontend/assets/img/control/right.png'>",
  responsive: [
  {
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: true,
      dots: false
    }
  },
  {
    breakpoint: 640,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 320,
    settings: { 
      slidesToShow: 1,
      slidesToScroll: 1
        
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
]
  });


// Load More

      $('.others-videos .col-sm-12 .loadMore').loadMoreResults({
    tag: {
      name: 'div',
      'class': 'col-others-videos'
    },
    displayedItems: 4,
    showItems: 8
  });

  $('.more-from-creators .col-sm-12 .loadMore').loadMoreResults({
    tag: {
      name: 'div',
      'class': 'col-more-from-creators'
    },
    displayedItems: 4,
    showItems: 8
  });

      $('.more-from-channels .col-sm-12 .loadMore').loadMoreResults({
    tag: {
      name: 'div',
      'class': 'col-more-from-channels'
    },
    displayedItems: 4,
    showItems: 8
  });

      $('.channels .col-md-12 .loadMore').loadMoreResults({
    tag: {
      name: 'div',
      'class': 'item-sub-channels'
    },
    displayedItems: 2,
    showItems: 1
  });




//On Scroll Functionality
  $(window).scroll(function () {
  var windowTop = $(window).scrollTop();
  windowTop > 50 ? $('nav').addClass('navShadow') : $('nav').removeClass('navShadow');
  windowTop > 50 ? $('ul').css('top','50px') : $('ul').css('top','50px');
});

//Smooth Scrolling Using Navigation Menu
$('a[href*="#"]').on('click', function(e){
  $('html,body').animate({
    scrollTop: $($(this).attr('href')).offset().top - 100
  },500);
  e.preventDefault();
});


var $window = $(window);
var $videoWrap = $('.video-wrap');
var $video = $('.video');
var videoHeight = $video.outerHeight();

$window.on('scroll',  function() {
var windowScrollTop = $window.scrollTop();
var videoBottom = videoHeight+$videoWrap.offset();
/* alert(videoBottom); */

if(screen.width <= 800) {
  var heightscroll = 200;
} else {
  var heightscroll = 500;
}
//alert(heightscroll);
if (windowScrollTop > heightscroll) {
  $videoWrap.height(heightscroll);
  $video.addClass('stuck');
} else {
  $videoWrap.height('auto');
  $video.removeClass('stuck');
  $video.show('.video');
}
});

$('.thumb').delay(10000).fadeOut('slow');

$(function() {
var a = $(".target-big");
$(window).scroll(function() {
  var scroll = $(window).scrollTop();

  if (scroll >= 550) {
    a.removeClass('target-big').addClass("target-small");
  } else {
    a.removeClass("target-small").addClass('target-big');
  }
});
});

/*
  $(document).scroll(function() {
var y = $(this).scrollTop();
if (y > 550) {
  $('#fixedban').fadeIn();
} else {
  $('#fixedban').fadeOut();
}
});
*/

/*    $(document).scroll(function() {
var y = $(this).scrollTop();
if (y = 250) {
  $('.stuck').fadeIn();
}
});*/

/*$(document).ready(function(c) {
$('.alert-close').on('click', function(c){
  $(this).parent().fadeOut('slow', function(c){
  });
});	
});*/

$(document).ready(function($) {
$('.alert-close').click(function() {
  $(this).closest('.stuck').hide();
});
});

$(function () {
  $("#wizard1").simpleWizard({
      cssClassStepActive: "active",
      cssClassStepDone: "done"
  });
});

$('#searchModal').on('shown.bs.modal', function () {
  $('#search').focus();
})