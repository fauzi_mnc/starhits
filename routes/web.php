<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return redirect('/');
});
//Reoptimized class loader:
Route::get('/reoptimized-class', function() {
    $exitCode = Artisan::call('optimize');
    return redirect('/');
});
//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return redirect('/');
});
//Clear Route clear:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return redirect('/');
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return redirect('/');
});
//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return redirect('/');
});
Route::get('/YoutubeGetAPI', function () {
    $exitCode = Artisan::call('YoutubeGetAPI');
    return redirect('/');
});
Route::get('/UpdateCampaign', function () {
    $exitCode = Artisan::call('UpdateCampaign');
    return redirect('/');
});
Route::get('/SitemapGenerate', function () {
    $exitCode = Artisan::call('SitemapGenerate');
    return redirect('/');
});

// Route::group(['namespace' => 'Auth'], function() {
//     //OAuth Facebook
//     Route::get('/auth/{provider}', 'LoginController@redirectToProvider');
//     Route::get('/auth/{provider}/callback', 'LoginController@handleProviderCallback');
// });

Route::group(['namespace' => 'Auth'], function() {
    Route::get('/auth/{provider}', 'CallbackController@redirectToProvider');
    Route::get('/auth/{provider}/callback', 'CallbackController@handleProviderCallback');
});

Route::get('/change_auth/{roles}', 'Backend\PersonalController@changeRole')->name('change_auth');
Route::get('/change_account/{id}', 'Backend\PersonalController@changeAccount')->name('change_account');
Route::get('/logout/', 'Backend\PersonalController@logout')->name('pesonal.logout');


Route::group(['namespace' => 'Backend', 'middleware' => ['auth']], function () {
    Route::get('/admin/add_account', 'PersonalController@formAccount')->name('personal.add_account');
    Route::post('/post/add_account', 'PersonalController@postAccount');
});

Route::group(['namespace' => 'Website'], function () {

    //Page Home
    Route::get('/home', 'HomeController@homepage')->name('home');
    Route::get('/', 'HomeController@homepage')->name('homepage');
    Route::post('/search', 'HomeController@search')->name('search');

    //Page Register
    Route::get('/register/user', 'RegisterController@create')->name('register.create');
    Route::get('/register/influencer', 'RegisterInfluencerController@create')->name('registerInfluencer.create');
    Route::post('/register/user/submit', 'RegisterController@store')->name('register.store');
    Route::post('/register/influencer/submit', 'RegisterInfluencerController@store')->name('registerInfluencer.store');
    Route::get('/get-state-list','RegisterInfluencerController@getStateList')->name('registerInfluencer.getstate');
    Route::get('/get-city-list','RegisterInfluencerController@getCityList')->name('registerInfluencer.getcity');

    //Activated User
    Route::get('/verify/{token}', 'VerifyUserController@verifyUser')->name('verifyUser');

    //Page Join
    Route::get('/join', 'PageController@join')->name('join');

    //Page Static 
    // Route::get('/page/{slug}', 'PageController@page')->name('page.slug');

    //Page About
    Route::get('/about-us', 'PageController@about')->name('about');

    //Page MCN
    Route::get('/about-us/multi-channel-network', 'PageController@mcn')->name('mcn');

    //Page Contact
    Route::get('/contact-us', 'PageController@contact')->name('contact');
    Route::post('/contact-us', 'ContactController@store')->name('contact.slug');

    //Page Policy
    Route::get('/privacy-policy', 'PageController@policy')->name('policy');

    //Page Term Of Use
    Route::get('/term-of-use', 'PageController@term')->name('term');

    //Page Term Of Use
    Route::get('/faq', 'PageController@faq')->name('faq');

    Route::post('/newsletter', 'PageController@newsletterStore')->name('newsletter');

    //Page Portfolio
    Route::get('/portfolio', 'PortfolioController@index')->name('portfolio');
    //Page Portfolio
    Route::get('/portfolio/{slug}', 'PortfolioController@details')->name('portfolio.slug');

    //Page Single Video
    Route::get('/videos/', 'VideosController@index')->name('videos');
    Route::get('/videos/{slug}', 'VideosController@videos')->name('videos.slug');
    Route::get('/videos/bookmark/{id}', 'VideosController@bookmark')->name('videos.bookmark');
    Route::get('/article/{slug}', 'VideosController@article')->name('article.slug');
    Route::get('/article/bookmark/{id}', 'VideosController@bookmark')->name('article.bookmark');

    //Page Channel
    Route::get('/channels', 'ChannelsController@index')->name('channels');
    Route::get('/channels/{slug}', 'ChannelsController@show')->name('channels.param');

    //Page Series
    Route::get('/series', 'SeriesController@index')->name('series');
    Route::get('/series/{slug}', 'SeriesController@show')->name('series.slug');
    Route::get('/series/bookmark/{id}', 'VideosController@bookmark')->name('series.bookmark');
    Route::get('/series/{slug}/{video}', 'SeriesController@videoSeries')->name('series.slug.videos');
    Route::get('/series-filter', 'SeriesController@filter')->name('series.filter');
    Route::post('/series-filter', 'SeriesController@filter')->name('series.result');

    //Page Creators
    Route::get('/creators', 'CreatorsController@index')->name('creators');
    Route::get('/creators/{slug}', 'CreatorsController@details')->name('creators.slug');
    Route::get('/creators-filter', 'CreatorsController@filter')->name('creators.filter');
    Route::post('/creators-filter', 'CreatorsController@filter')->name('creators.result');
    Route::get('/creators/{slug}/video-list', 'CreatorsController@videolist')->name('creators.slug.videolist');

    // Creator Validate Channel
    Route::get('/creators-validate/{provider_id}', 'ValidateChannelController@index')->name('creators.validate');
    Route::match(['put', 'patch'], '/creators-validate/{provider_id}', 'ValidateChannelController@update')->name('creators.validate.update');

    //Page Contact
    // Route::get('/contact', 'ContactController@index')->name('contact');
    // Route::post('/contact', 'ContactController@store')->name('contact.slug');

    Route::get('404', ['as' => '404', 'uses' => 'ErrorHandlerController@errorCode404']);
    Route::get('/a174417f3ec9dd53ec861a6a5d54d2b9/{var}', 'HomeController@a174417f3ec9dd53ec861a6a5d54d2b9')->name('a174417f3ec9dd53ec861a6a5d54d2b9');

});

Route::get('bank', 'Backend\BankController@index')->name('bank.index');
Route::get('instagram/check', 'Backend\InfluencerController@checkInstagram')->name('influencer.instagram.check');
Route::get('checkYoutube', 'Backend\CreatorController@checkYoutube')->name('creator.check');


/*++++++++++++++  ROLE ADMINISTRATOR  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

Route::group(['namespace' => 'Backend', 'middleware' => ['role:admin', 'auth'], 'prefix' => 'admin'], function () {

    // Route::get('/insta', 'InstaController@instaFeed');
    // Route::get('404', ['as' => '404', 'uses' => 'ErrorHandlerController@errorCode404']);
    
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('', 'DashboardController@index')->name('admin.dashboard.index');
    });

    Route::group(['prefix' => 'google'], function () {
        Route::get('', 'GoogleSheetController@index')->name('admin.google.index');
        Route::get('/create', 'GoogleSheetController@create')->name('admin.google.create');
        Route::get('/Oauth', 'GoogleSheetController@Oauth')->name('admin.google.Oauth');
        Route::get('/updateEntry/{file}', 'GoogleSheetController@updateEntry')->name('admin.google.updateEntry');
        Route::get('/readSpreadsheet', 'GoogleSheetController@readSpreadsheet')->name('admin.google.readSpreadsheet');
        Route::get('/spreadsheet', 'GoogleSheetController@spreadsheet')->name('admin.google.spreadsheet');
    });

    Route::get('/addaccount', 'PersonalController@formAccount')->name('personal.addaccount');
    Route::post('/post/addaccount', 'PersonalController@postAccount')->name('personal.post.addaccount');
    //Route::get('/post/addaccount', 'PersonalController@postAccount')->name('personal.post.addaccount');

    Route::group(['prefix' => 'channel'], function () {
        Route::get('', 'ChannelsController@index')->name('channel.index');
        Route::get('/create', 'ChannelsController@create')->name('channel.create');
        Route::post('', 'ChannelsController@store')->name('channel.store');
        // Route::get('/test', 'ChannelsController@apa')->name('channel.create');
        Route::get('/{type}/edit', 'ChannelsController@edit')->name('channel.edit');
        Route::put('/{type}', 'ChannelsController@update')->name('channel.update');
        Route::get('/{type}', 'ChannelsController@show')->name('channel.show');
        Route::delete('/{type}', 'ChannelsController@destroy')->name('channel.destroy');
    });

    Route::get('/newsletter', 'NewsletterController@index')->name('newsletter.index');

    Route::group(['prefix' => 'ads'], function () {
        Route::get('', 'AdsController@index')->name('ads.index');
        Route::get('/create', 'AdsController@create')->name('ads.create');
        Route::post('', 'AdsController@store')->name('ads.store');
        // Route::get('/test', 'AdsController@apa')->name('ads.create');
        Route::get('/{type}/edit', 'AdsController@edit')->name('ads.edit');
        Route::put('/{type}', 'AdsController@update')->name('ads.update');
        Route::get('/{type}', 'AdsController@show')->name('ads.show');
        Route::delete('/{type}', 'AdsController@destroy')->name('ads.destroy');
    });

    Route::group(['prefix' => 'category'], function () {
        Route::get('', 'CategoryController@index')->name('category.index');
        Route::get('/create', 'CategoryController@create')->name('category.create');
        Route::post('', 'CategoryController@store')->name('category.store');
        Route::get('/{type}/edit', 'CategoryController@edit')->name('category.edit');
        Route::put('/{type}', 'CategoryController@update')->name('category.update');
        // Route::get('/{type}', 'CategoryController@show')->name('category.show');
        // Route::delete('/{type}', 'CategoryController@destroy')->name('category.destroy');
    });

    Route::group(['prefix' => 'menu'], function () {
        Route::get('', 'MenuController@index')->name('menu.index');
        Route::get('/create-parent', 'MenuController@create_parent')->name('menu.create-parent');
        Route::get('/create-child', 'MenuController@create_child')->name('menu.create-child');
        Route::post('', 'MenuController@store')->name('menu.store');
        Route::get('/{type}/edit', 'MenuController@edit')->name('menu.edit');
        Route::put('/{type}', 'MenuController@update')->name('menu.update');
        // Route::get('/{type}', 'CategoryController@show')->name('category.show');
        // Route::delete('/{type}', 'CategoryController@destroy')->name('category.destroy');
    });

    Route::group(['prefix' => 'account_management'], function () {
        Route::get('', 'AccountManController@index')->name('account_management.index');
        Route::get('delete/{master}/{child}', 'AccountManController@DeleteAction')->name('account_management.delete');
    });

    Route::group(['prefix' => 'serie'], function () {
        Route::get('', 'SeriesController@index')->name('admin.serie.index');
        Route::get('/create', 'SeriesController@create')->name('admin.serie.create');
        Route::post('', 'SeriesController@store')->name('admin.serie.store');
        Route::get('/{type}/edit', 'SeriesController@edit')->name('admin.serie.edit');
        Route::put('/{type}', 'SeriesController@update')->name('admin.serie.update');
        Route::get('/{type}', 'SeriesController@show')->name('admin.serie.show');
        Route::post('/update-featured', 'SeriesController@updateFeatured')->name('admin.serie.updateFeatured');
        Route::delete('/{type}', 'SeriesController@destroy')->name('admin.serie.destroy');
    });

    Route::group(['prefix' => 'posts'], function () {
        Route::get('', 'PostsController@index')->name('admin.posts.index');
        Route::get('/create-automatic', 'PostsController@createAutomatic')->name('admin.posts.createAutomatic');
        Route::get('/create-manual', 'PostsController@createManual')->name('admin.posts.createManual');
        Route::post('', 'PostsController@store')->name('admin.posts.store');
        Route::get('/{type}/edit-automatic', 'PostsController@editAutomatic')->name('admin.posts.editAutomatic');
        Route::get('/{type}/edit-manual', 'PostsController@editManual')->name('admin.posts.editManual');
        Route::put('/{type}', 'PostsController@update')->name('admin.posts.update');
        // Route::get('/{type}', 'PostsController@show')->name('posts.show');
        Route::get('checkYoutube', 'PostsController@checkYoutube')->name('admin.posts.check');
        Route::post('/update-featured', 'PostsController@updateFeatured')->name('admin.posts.updateFeatured');
        Route::post('/get-series', 'PostsController@getSeries')->name('admin.posts.getSeries');
        Route::delete('/{type}', 'PostsController@destroy')->name('admin.posts.destroy');
    });

    Route::group(['prefix' => 'config'], function () {
        Route::get('', 'WebConfigController@index')->name('config.index');
        Route::get('/create', 'WebConfigController@create')->name('config.create');
        Route::post('', 'WebConfigController@store')->name('config.store');
        Route::get('/{type}/edit', 'WebConfigController@edit')->name('config.edit');
        Route::put('/{type}', 'WebConfigController@update')->name('config.update');
        Route::get('/{type}', 'WebConfigController@show')->name('config.show');
    });

    Route::group(['prefix' => 'email_config'], function () {
        Route::get('', 'EmailConfigController@index')->name('email_config.index');
        Route::get('/create', 'EmailConfigController@create')->name('email_config.create');
        Route::post('', 'EmailConfigController@store')->name('email_config.store');
        Route::get('/{type}/edit', 'EmailConfigController@edit')->name('email_config.edit');
        Route::put('/{type}', 'EmailConfigController@update')->name('email_config.update');
        Route::get('/{type}', 'EmailConfigController@show')->name('email_config.show');
    });

    // Route::group(['prefix' => 'bookmarks'], function () {
    //     Route::get('', 'BookmarksController@index')->name('bookmarks.index');
    // });


    Route::resource('user', 'UserController');
    Route::resource('role', 'RoleController');
    // Route::resource('brand', 'BrandController');

    Route::group(['prefix' => 'personal'], function () {
        Route::get('', 'PersonalController@index')->name('admin.personal.index');
        Route::get('/create', 'PersonalController@create')->name('admin.personal.create');
        Route::post('', 'PersonalController@store')->name('admin.personal.store');
        Route::get('/{type}/edit', 'PersonalController@edit')->name('admin.personal.edit');
        Route::put('/{type}', 'PersonalController@update')->name('admin.personal.update');
        Route::get('/{type}', 'PersonalController@show')->name('admin.personal.show');

        Route::get('/addaccount', 'PersonalController@logout')->name('admin.personal.addaccount');
    });

    Route::group(['prefix' => 'portofolio'], function () {
        Route::get('', 'PortofolioController@index')->name('portofolio.index');
        Route::get('/create', 'PortofolioController@create')->name('portofolio.create');
        Route::post('', 'PortofolioController@store')->name('portofolio.store');
        Route::get('/{type}/edit', 'PortofolioController@edit')->name('portofolio.edit');
        Route::put('/{type}', 'PortofolioController@update')->name('portofolio.update');
        Route::get('/{type}', 'PortofolioController@show')->name('portofolio.show');
        Route::get('destroy/{type}', 'PortofolioController@destroy')->name('portofolio.destroy');
    });

    Route::group(['prefix' => 'brand'], function () {
        Route::get('', 'BrandController@index')->name('brand.index');
        Route::get('/create', 'BrandController@create')->name('brand.create');
        Route::post('', 'BrandController@store')->name('brand.store');
        Route::get('/{type}/edit', 'BrandController@edit')->name('brand.edit');
        Route::put('/{type}', 'BrandController@updates')->name('brand.update');
        Route::get('/{type}', 'BrandController@show')->name('brand.show');
    });

    Route::group(['prefix' => 'influencer'], function () {
        Route::get('', 'InfluencerController@index')->name('influencer.index');
        Route::get('/create', 'InfluencerController@create')->name('influencer.create');
        Route::post('', 'InfluencerController@store')->name('influencer.store');
        Route::get('/{type}/edit', 'InfluencerController@edit')->name('influencer.edit');
        Route::put('/{type}', 'InfluencerController@update')->name('influencer.update');
        // Route::get('/{type}', 'InfluencerController@show')->name('influencer.show');
        Route::get('/browse-influencer', 'CampaignController@getInfluencer')->name('admin.influencer.browse');
        Route::get('/get-influencer', 'CampaignController@getDataInfluencer')->name('admin.influencer.getInfluencer');
        Route::delete('/{type}', 'InfluencerController@destroy')->name('influencer.destroy');

        Route::get('diagram/index/{id}', 'InfluencerDiagramController@index')->name('influencer.diagram.index');
        Route::get('influencer/api/{id}', 'InfluencerDiagramController@apiDiagram')->name('influencer.api');
    });

    Route::group(['prefix' => 'revenue'], function () {
        Route::get('approval', 'RevenueApprovalController@index')->name('revenue.approval.index');
        Route::get('approver', 'RevenueApproverController@index')->name('revenue.approver.index');
        Route::post('approver', 'RevenueApproverController@store')->name('revenue.approver.store');
        Route::put('approver', 'RevenueApproverController@update')->name('revenue.approver.update');
        Route::get('', 'RevenueController@index')->name('revenue.index');
        Route::get('/create', 'RevenueController@create')->name('revenue.create');
        Route::post('', 'RevenueController@store')->name('revenue.store');
        Route::get('/{type}/edit', 'RevenueController@edit')->name('revenue.edit');
        Route::get('/{id}/download', 'RevenueController@download')->name('revenue.download');
        Route::get('/{id}/download-excel', 'RevenueController@excel')->name('revenue.excel');
        Route::put('/{type}', 'RevenueController@update')->name('revenue.update');
        Route::post('/read-csv', 'RevenueController@readExcel')->name('revenue.read');
        Route::get('/{type}/view', 'RevenueController@view')->name('revenue.view');
        Route::get('/template', 'RevenueController@template')->name('revenue.template');
        Route::get('/download/template', 'RevenueController@downloadTry')->name('revenue.download.template');

        Route::get('/read-csv', 'RevenueController@uploadCsv')->name('revenue.upload');

        Route::get('/{id}/download_report', 'RevenueController@download_report')->name('revenue.download.report');
        Route::get('/{id}/download_report_pdf', 'RevenueController@download_report_pdf')->name('revenue.download.report_pdf');

        Route::post('/status_edit', 'RevenueController@statusEdit')->name('revenue.status_edit');
        Route::get('/{id}/status_checked', 'RevenueController@StatusChecked')->name('revenue.status_checked');
        Route::get('/{id}/status_approved', 'RevenueController@StatusApproved')->name('revenue.status_approved');
        Route::get('/{id}/status_revision', 'RevenueController@StatusRevision')->name('revenue.status_revision');
        Route::get('/{id}/status_rejected', 'RevenueController@StatusRejected')->name('revenue.status_rejected');
        //Route::post('/{id}/update/status_edit', 'RevenueController@statusUpdate')->name('revenue.update.status_edit');


        Route::get('/{type}/edit/revisi', 'RevenueController@edit_revisi')->name('revenue.edit.revisi');
        Route::post('/edit/revisi', 'RevenueController@update_revisi')->name('revenue.post.revisi');

    });
    Route::group(['prefix' => 'youtube'], function () {
        // analytics
        Route::get('/analytics', 'YoutubeController@index')->name('youtube.analytics.index');
        Route::get('/analyticsjson', 'YoutubeController@analyticJson')->name('youtube.analytics.json');
        Route::post('analytics/detail', 'YoutubeController@analyticdetail')->name('youtube.analytics.detail');
        Route::get('/analytics/detail/{date}/export_excell/', 'YoutubeController@excell_export')->name('youtube.analytics.detail.export_excell');
        Route::get('/analytics/detail/{date}/export_csv/', 'YoutubeController@csv_export')->name('youtube.analytics.detail.export_csv');
        Route::get('/analytics/detail/{date}/export_print/', 'YoutubeController@export_print')->name('youtube.analytics.detail.export_print');

        // youtube audit
        Route::get('/audits', 'YoutubeController@auditsIndex')->name('youtube.audits.index');
        Route::get('/auditsjson', 'YoutubeController@auditsJson')->name('youtube.audits.json');
        Route::post('/audits/detail', 'YoutubeController@auditsdetail')->name('youtube.audits.detail');
        Route::get('/audits/detail/{date}/export_excell/', 'YoutubeController@audits_excell_export')->name('youtube.audits.detail.export_excell');
        Route::get('/audits/detail/{date}/export_csv/', 'YoutubeController@audits_csv_export')->name('youtube.audits.detail.export_csv');
        Route::get('/audits/detail/{date}/export_print/', 'YoutubeController@audits_export_print')->name('youtube.audits.detail.export_print');

        // upload audit
        Route::get('/audits/report/', 'VideoYTController@index')->name('youtube.audits.report.index');
        Route::get('/audits/report/create', 'VideoYTController@create')->name('youtube.audits.report.create');
        Route::post('/audits/report/store', 'VideoYTController@store')->name('youtube.audits.report.store');
        Route::post('/audits/report/upload', 'VideoYTController@upload')->name('youtube.audits.report.upload');
        Route::get('/audits/report/file/{id}', 'VideoYTController@file')->name('youtube.audits.report.file');
        Route::get('/audits/report/consoleReadCsv', 'VideoYTController@consoleReadCsv')->name('youtube.audits.report.consoleReadCsv');
        Route::get('/audits/report/Bcxz', 'VideoYTController@Bcxz')->name('youtube.audits.report.Bcxz');

        // cms audit
        Route::get('/audits/cms', 'VideoYTController@cmsAudits')->name('youtube.audits.cms');
        Route::get('/audits/cms/monetize/{cms}', 'VideoYTController@cmsAuditsDetailsMonetize')->name('youtube.audits.cms.monetize');
        Route::get('/audits/cms/content/{cms}', 'VideoYTController@cmsAuditsDetailsContent')->name('youtube.audits.cms.content');
        Route::get('/audits/cms/monetize/{cms}/{field}/{time}/export_csv/', 'VideoYTController@csv_exportcmsAuditMonetize')->name('youtube.audits.cms.monetize.export_csv');
        Route::get('/audits/cms/content/{cms}/{field}/{time}/export_csv/', 'VideoYTController@csv_exportcmsAuditContent')->name('youtube.audits.cms.content.export_csv');

        // update content id
        Route::post('/audits/content/update', 'VideoYTController@updateContentID')->name('youtube.audits.cms.content.update');
    });

    Route::group(['prefix' => 'dsp'], function () {
        // Route::get('approval', 'RevenueApprovalController@index')->name('revenue.approval.index');

        // Route::get('approver', 'RevenueApproverController@index')->name('revenue.approver.index');
        // Route::post('approver', 'RevenueApproverController@store')->name('revenue.approver.store');
        // Route::put('approver', 'RevenueApproverController@update')->name('revenue.approver.update');

        Route::get('', 'DspController@index')->name('dsp.index');
        //Route::get('/create', 'DspController@create')->name('dsp.create');
        Route::post('', 'DspController@store')->name('dsp.store');
        Route::get('/{type}/edit', 'DspController@edit')->name('dsp.edit');
        Route::get('/{id}/download', 'DspController@download_report_pdf')->name('dsp.download');
        Route::get('/{id}/download-excel', 'DspController@excel')->name('dsp.excel');
        Route::put('/{type}', 'DspController@update')->name('dsp.update');
        Route::post('/read-csv', 'DspController@readExcel')->name('dsp.read');
        Route::get('/{type}/view', 'DspController@view')->name('dsp.view');
        Route::get('/template', 'DspController@template')->name('dsp.template');
        Route::get('/download/template', 'DspController@template')->name('dsp.download.template');

        Route::get('/read-csv', 'DspController@uploadCsv')->name('dsp.upload');

        Route::get('/{id}/download_report', 'DspController@download_report')->name('dsp.download.report');
        Route::get('/{id}/download_report_pdf', 'DspController@download_report_pdf')->name('dsp.download.report_pdf');


        Route::get('/create', 'DspController@createDSP')->name('dsp.create'); //Edited
        Route::get('/create_dsp', 'DspController@createDSP')->name('dsp.create_dsp');
        Route::post('/store_dsp', 'DspController@storeDSP')->name('dsp.store_dsp');

        Route::get('/checked/{id}', 'DspController@status_checked')->name('dsp.checked');
        Route::get('/revisi/{id}', 'DspController@status_revisi')->name('dsp.revisi');
        Route::get('/finish/{id}', 'DspController@status_finish')->name('dsp.finish');

        Route::get('{id}/dsp_detail/{isrc}', 'DspController@dsp_detail')->name('dsp.dsp_details');
        Route::get('{id}/dsp_detail_print/{isrc}', 'DspController@print_detail_lagu')->name('dsp.dsp_detail_print');
        Route::get('{id}/dsp_detail_print_fpp/{isrc}', 'DspController@print_fpp_detail_lagu')->name('dsp.dsp_detail_print_fpp');
        Route::get('/printview/{id}', 'DspController@print_lagu')->name('dsp.printview');

    });

    Route::group(['prefix' => 'masterlagu'], function () {
        Route::get('', 'MasterLaguController@index')->name('masterlagu.index');
        Route::get('/create', 'MasterLaguController@create')->name('masterlagu.create');
        Route::post('', 'MasterLaguController@store')->name('masterlagu.store');
        Route::get('/download/template', 'MasterLaguController@template')->name('masterlagu.download.template');
        Route::get('/{id}/edit', 'MasterLaguController@edit')->name('masterlagu.edit');
        Route::put('/{id}', 'MasterLaguController@update')->name('masterlagu.update');
        Route::get('/{id}/detail', 'MasterLaguController@detail_lagu')->name('masterlagu.detail.lagu');
        Route::get('/{id}/detail/singer', 'MasterLaguController@detail_singer')->name('masterlagu.detail.singer');
        Route::get('/{id}/detail/songwriter', 'MasterLaguController@detail_songwriter')->name('masterlagu.detail.songwriter');
        Route::get('/destroy/{id}', 'MasterLaguController@destroy')->name('masterlagu.destroy');

        Route::get('/export_csv/', 'MasterLaguController@csv_export')->name('masterlagu.export_csv');
        Route::get('/export_excell/', 'MasterLaguController@excell_export')->name('masterlagu.export_excell');
        Route::get('/export_print/', 'MasterLaguController@export_print')->name('masterlagu.export_print');
    });

    Route::group(['prefix' => 'mastersinger'], function () {
        Route::get('', 'MasterSingerController@index')->name('mastersinger.index');
        Route::get('/create', 'MasterSingerController@create')->name('mastersinger.create');
        Route::post('', 'MasterSingerController@store')->name('mastersinger.store');
        Route::get('/{id}/edit', 'MasterSingerController@edit')->name('mastersinger.edit');
        Route::put('/{id}', 'MasterSingerController@update')->name('mastersinger.update');
        Route::get('/destroy/{id}', 'MasterSingerController@destroy')->name('mastersinger.destroy');
    });
    
    Route::group(['prefix' => 'mastersongwriter'], function () {
        Route::get('', 'MasterSongwriterController@index')->name('mastersongwriter.index');
        Route::get('/create', 'MasterSongwriterController@create')->name('mastersongwriter.create');
        Route::post('', 'MasterSongwriterController@store')->name('mastersongwriter.store');
        Route::get('/{id}/edit', 'MasterSongwriterController@edit')->name('mastersongwriter.edit');
        Route::put('/{id}', 'MasterSongwriterController@update')->name('mastersongwriter.update');
        Route::get('/destroy/{id}', 'MasterSongwriterController@destroy')->name('mastersongwriter.destroy');
    });

    Route::group(['prefix' => 'payment'], function () {
        Route::get('', 'PaymentController@index')->name('payment.index');
        Route::get('/create', 'PaymentController@create')->name('payment.create');
        Route::post('', 'PaymentController@store')->name('payment.store');
        Route::get('/{type}/edit', 'PaymentController@edit')->name('payment.edit');
        Route::put('/{type}', 'PaymentController@update')->name('payment.update');
        Route::get('/{type}', 'PaymentController@show')->name('payment.show');
    });

    Route::group(['prefix' => 'platforms'], function () {
        Route::get('', 'PlatformsController@index')->name('platforms.index');
        Route::get('/callyt', 'PlatformsController@youtubeLogin')->name('platforms.callyt');
        Route::get('/callfb', 'PlatformsController@facebookLogin')->name('platforms.callfb');
        Route::get('youtube', 'PlatformsController@youtube')->name('platforms.youtube.login');
        Route::get('youtube/logout', 'PlatformsController@youtubeLogout')->name('platforms.youtube.logout');
        
        Route::get('/{provider}/log', 'PlatformsController@log')->name('platforms.log');
        Route::get('/{provider}/callback', 'PlatformsController@callbackLogin')->name('platforms.callback');
        Route::get('/upfb', 'PlatformsController@uploadFacebook')->name('platforms.upfb');
        Route::get('/tweet', 'PlatformsController@tweet')->name('platforms.tweet');
        Route::get('/logfb', 'PlatformsController@loginFb')->name('platforms.logfb');
        Route::get('/logdaily', 'PlatformsController@loginDaily')->name('platforms.logdaily');
        // Route::get('/callfb', 'PlatformsController@facebookLogin')->name('platforms.callfb');
        // Route::get('/{type}/edit', 'PersonalController@edit')->name('personal.edit');
        // Route::put('/{type}', 'PersonalController@update')->name('personal.update');
        // Route::get('/{type}', 'PersonalController@show')->name('personal.show');

        Route::get('twitterTimeLine', 'PlatformsController@twitterUserTimeLine');
        Route::post('tweet', ['as'=>'post.tweet','uses'=>'PlatformsController@tweet']);
    });

    Route::group(['prefix' => 'page'], function () {
        Route::get('', 'PageController@index')->name('page.index');
        Route::get('/create', 'PageController@create')->name('page.create');
        Route::post('', 'PageController@store')->name('page.store');
        Route::get('/{id}/edit', 'PageController@edit')->name('page.edit');
        Route::put('/{id}', 'PageController@update')->name('page.update');
        Route::get('/{id}', 'PageController@show')->name('page.show');
    });

    Route::group(['prefix' => 'creator'], function () {
        Route::get('', 'CreatorController@index')->name('creator.index');
        Route::get('create', 'CreatorController@create')->name('creator.create');
        Route::post('store', 'CreatorController@store')->name('creator.store');
        Route::get('/{id}/edit', 'CreatorController@edit')->name('creator.edit');
        Route::post('/{id}/edit', 'CreatorController@update')->name('creator.update');
        Route::delete('/{id}', 'UserController@destroy')->name('creator.destroy');
    });

    Route::group(['prefix' => 'campaign'], function () {
        Route::get('', 'CampaignController@index')->name('admin.campaign.index');
        Route::get('/create', 'CampaignController@create')->name('admin.campaign.create');
        Route::post('/children', 'CampaignController@children')->name('admin.campaign.children');
        Route::post('store', 'CampaignController@store')->name('admin.campaign.store');
        Route::get('/{id}/edit', 'CampaignController@edit')->name('admin.campaign.edit');
        Route::put('/{id}', 'CampaignController@update')->name('admin.campaign.update');
        Route::get('/{id}/add', 'CampaignController@add')->name('admin.campaign.add');
        Route::get('/{id}/look', 'CampaignController@showCampaign')->name('admin.campaign.look');
        Route::get('/{id}/budget', 'CampaignController@budget')->name('admin.campaign.budget');
        Route::post('/close', 'CampaignController@closeCampaign')->name('admin.campaign.close');
        Route::post('/approval', 'CampaignController@approval')->name('admin.campaign.approval');
        Route::post('/infl', 'CampaignController@infl')->name('admin.campaign.infl');
        Route::post('/upbudget', 'CampaignController@upbudget')->name('admin.campaign.upbudget');
        Route::get('/{id}/{camp}/addtotable', 'CampaignController@addtotable')->name('admin.campaign.addtotable');
        Route::get('/{id}/{camp}/rft', 'CampaignController@rft')->name('admin.campaign.rft');
        Route::get('/{id}/{camp}/rif', 'CampaignController@rif')->name('admin.campaign.rif');
        Route::delete('/{type}', 'CampaignController@destroy')->name('admin.campaign.destroy');
        Route::get('/browse-influencer', 'CampaignController@getInfluencer')->name('admin.campaign.browse');
        Route::get('/addtotableinfluencers', 'CampaignController@addToTableInfluencers')->name('admin.campaign.addtotableinfluencers');

    });

});
/*++++++++++++++  END ROLE ADMINISTRATOR  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


/*++++++++++++++  ROLE CREATOR  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
Route::group(['namespace' => 'Backend', 'middleware' => ['role:creator', 'auth'], 'prefix' => 'creator'], function () {
    Route::redirect('/home', '/creator/personal', 301);
    Route::get('/addaccount', 'PersonalController@formAccount')->name('personal.addaccount');
    Route::post('/post/addaccount', 'PersonalController@postAccount')->name('personal.post.addaccount');
    
    Route::group(['prefix' => 'serie'], function () {
        Route::get('', 'SeriesController@index')->name('creator.serie.index');
        Route::get('/create', 'SeriesController@create')->name('creator.serie.create');
        Route::post('', 'SeriesController@store')->name('creator.serie.store');
        Route::get('/{type}/edit', 'SeriesController@edit')->name('creator.serie.edit');
        Route::put('/{type}', 'SeriesController@update')->name('creator.serie.update');
        Route::get('/{type}', 'SeriesController@show')->name('creator.serie.show');
        Route::post('/update-featured', 'SeriesController@updateFeatured')->name('creator.serie.updateFeatured');
        Route::delete('/{type}', 'SeriesController@destroy')->name('creator.serie.destroy');
    });

    Route::group(['prefix' => 'personal'], function () {
        Route::get('', 'PersonalController@index')->name('creator.personal.index');
        Route::get('/create', 'PersonalController@create')->name('creator.personal.create');
        Route::post('', 'PersonalController@store')->name('creator.personal.store');
        Route::get('/{type}/edit', 'PersonalController@edit')->name('creator.personal.edit');
        Route::put('/{type}', 'PersonalController@update')->name('creator.personal.update');
        Route::get('/{type}', 'PersonalController@show')->name('creator.personal.show');
    });

    Route::group(['prefix' => 'posts'], function () {
        Route::get('', 'PostsController@index')->name('creator.posts.index');
        Route::get('/create-automatic', 'PostsController@createAutomatic')->name('creator.posts.createAutomatic');
        Route::get('/create-manual', 'PostsController@createManual')->name('creator.posts.createManual');
        Route::post('', 'PostsController@store')->name('creator.posts.store');
        Route::get('/{type}/edit-automatic', 'PostsController@editAutomatic')->name('creator.posts.editAutomatic');
        Route::get('/{type}/edit-manual', 'PostsController@editManual')->name('creator.posts.editManual');
        Route::put('/{type}', 'PostsController@update')->name('creator.posts.update');
        // Route::get('/{type}', 'PostsController@show')->name('posts.show');
        Route::get('checkYoutube', 'PostsController@checkYoutube')->name('creator.posts.check');
        Route::post('/update-featured', 'PostsController@updateFeatured')->name('creator.posts.updateFeatured');
        Route::post('/get-series', 'PostsController@getSeries')->name('creator.posts.getSeries');
    });
    
    Route::group(['prefix' => 'bookmark'], function () {
        Route::get('', 'BookmarkController@index')->name('creator.bookmark.index');
        Route::delete('/{id}', 'BookmarkController@destroy')->name('creator.bookmark.destroy');
    });

    Route::group(['prefix' => 'multiUpload'], function () {
        Route::post('/create', 'MultiUploadController@create')->name('creator.multiUpload.create');
        Route::post('/store', 'MultiUploadController@store')->name('creator.multiUpload.store');
        Route::get('/upload', 'MultiUploadController@getUpload')->name('creator.multiUpload.getUpload');
        Route::post('/upload', 'MultiUploadController@postUpload')->name('creator.multiUpload.postUpload');
    });

    Route::group(['prefix' => 'platforms'], function () {
        Route::get('', 'PlatformsController@index')->name('creator.platforms.index');
        Route::get('/log', 'PlatformsController@log')->name('creator.platforms.log');
        Route::get('/callyt', 'PlatformsController@youtubeLogin')->name('creator.platforms.callyt');
        Route::get('/callfb', 'PlatformsController@facebookLogin')->name('creator.platforms.callfb');
        Route::get('youtube', 'PlatformsController@youtube')->name('creator.platforms.youtube.login');
        Route::get('dailymotion', 'PlatformsController@dailymotion')->name('creator.platforms.dailymotion.login');
        Route::get('{provider}/logout', 'PlatformsController@logout')->name('creator.platforms.logout');
        Route::get('connectionCheck/{provider}', 'PlatformsController@connectionCheck')->name('creator.platforms.connectionCheck');
        
        Route::get('/{provider}/log', 'PlatformsController@log')->name('creator.platforms.log');
        Route::get('/{provider}/callback', 'PlatformsController@callbackLogin')->name('creator.platforms.callback');
        Route::get('/upfb', 'PlatformsController@uploadFacebook')->name('creator.platforms.upfb');
        Route::get('/tweet', 'PlatformsController@tweet')->name('creator.platforms.tweet');
        Route::get('/logfb', 'PlatformsController@loginFb')->name('creator.platforms.logfb');
        Route::get('/logdaily', 'PlatformsController@loginDaily')->name('creator.platforms.logdaily');
    });

    Route::group(['prefix' => 'analytic'], function () {
        Route::get('', 'AnalyticController@index')->name('creator.analytic.index');
        Route::post('postAnalytic', 'AnalyticController@insert')->name('creator.analytic.insert');
        Route::get('getAnalytic', 'AnalyticController@getData')->name('creator.analytic.get');
    });

    Route::group(['prefix' => 'revenue'], function () {
        Route::get('', 'RevenueCreatorController@index')->name('creator.revenue.index');
        Route::get('/{type}/view', 'RevenueCreatorController@view')->name('creator.revenue.view');
        Route::get('/{type}/pdf', 'RevenueCreatorController@pdf')->name('creator.revenue.pdf');
    });

    Route::group(['prefix' => 'video-reports'], function () {
        // channels audit
        Route::get('/audits/channel', 'VideoYTController@channelAudits')->name('youtube.audits.channel');
        Route::get('/audits/channel/monetize/{id}', 'VideoYTController@channelAuditsDetailsMonetize')->name('youtube.audits.channel.monetize');
        Route::get('/audits/channel/content/{id}', 'VideoYTController@channelAuditsDetailsContent')->name('youtube.audits.channel.content');
        Route::get('/audits/channel/monetize/{id}/{field}/{time}/export_csv/', 'VideoYTController@csv_exportcmsAuditMonetize')->name('youtube.audits.channel.monetize.export_csv');
        Route::get('/audits/channel/content/{id}/{field}/{time}/export_csv/', 'VideoYTController@csv_exportcmsAuditContent')->name('youtube.audits.channel.content.export_csv');
    });

    Route::group(['prefix' => 'masterlagu'], function () {
        Route::get('', 'MasterLaguController@index')->name('creator.masterlagu.index');
        Route::get('/{id}/detail', 'MasterLaguController@detail_lagu')->name('creator.masterlagu.detail.lagu');
        Route::get('/{id}/detail/singer', 'MasterLaguController@detail_singer')->name('creator.masterlagu.detail.singer');
        Route::get('/{id}/detail/songwriter', 'MasterLaguController@detail_songwriter')->name('creator.masterlagu.detail.songwriter');

        Route::get('/export_excell/', 'MasterLaguController@excell_export')->name('creator.masterlagu.export_excell');
        Route::get('/export_print/', 'MasterLaguController@export_print')->name('creator.masterlagu.export_print');
    });

    Route::get('/setting/{id}/{context}', 'CreatorController@setting')->name('self.creator.edit');
    Route::put('/{id}/{context}', 'CreatorController@updateSetting')->name('self.creator.update');
});
/*++++++++++++++  END ROLE CREATOR  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/



/*++++++++++++++  ROLE BRAND  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
Route::group(['namespace' => 'Backend', 'middleware' => ['role:brand', 'auth'], 'prefix' => 'brand'], function () {

    Route::get('/addaccount', 'PersonalController@formAccount')->name('personal.addaccount');
    Route::post('/post/addaccount', 'PersonalController@postAccount')->name('personal.post.addaccount');

    Route::group(['prefix' => 'campaign'], function () {
        Route::get('', 'CampaignController@index')->name('brand.campaign.index');
        Route::get('/create', 'CampaignController@create')->name('brand.campaign.create');
        Route::post('/children', 'CampaignController@children')->name('brand.campaign.children');
        Route::post('store', 'CampaignController@store')->name('brand.campaign.store');
        Route::get('/{id}/edit', 'CampaignController@edit')->name('brand.campaign.edit');
        Route::put('/{id}', 'CampaignController@update')->name('brand.campaign.update');
        Route::get('/{id}/add', 'CampaignController@add')->name('brand.campaign.add');
        Route::get('/{id}', 'CampaignController@show')->name('brand.campaign.show');
        Route::post('/approval', 'CampaignController@approval')->name('brand.campaign.approval');
        Route::post('/infl', 'CampaignController@infl')->name('brand.campaign.infl');
        Route::get('/{id}/look', 'CampaignController@showCampaign')->name('brand.campaign.look');
        Route::post('/close', 'CampaignController@closeCampaign')->name('brand.campaign.close');
        Route::get('/{id}/{camp}/addtotable', 'CampaignController@addtotable')->name('brand.campaign.addtotable');
        Route::get('/{id}/{camp}/rft', 'CampaignController@rft')->name('brand.campaign.rft');
        Route::get('/{id}/{camp}/rif', 'CampaignController@rif')->name('brand.campaign.rif');
        Route::delete('/{type}', 'CampaignController@destroy')->name('brand.campaign.destroy');
    });

    Route::group(['prefix' => 'influencer'], function () {
        Route::get('/browse-influencer', 'CampaignController@getInfluencer')->name('brand.influencer.browse');
        Route::get('/get-influencer', 'CampaignController@getDataInfluencer')->name('brand.influencer.getInfluencer');
    });

    Route::group(['prefix' => 'personal'], function () {
        Route::get('', 'PersonalController@index')->name('brand.personal.index');
        Route::get('/create', 'PersonalController@create')->name('brand.personal.create');
        Route::post('', 'PersonalController@store')->name('brand.personal.store');
        Route::get('/{type}/edit', 'PersonalController@edit')->name('brand.personal.edit');
        Route::put('/{type}', 'PersonalController@update')->name('brand.personal.update');
        Route::get('/{type}', 'PersonalController@show')->name('brand.personal.show');
    });

});
/*++++++++++++++  END ROLE BRAND  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


/*++++++++++++++  ROLE INFLUENCER  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
Route::group(['namespace' => 'Backend', 'middleware' => ['role:influencer', 'auth'], 'prefix' => 'influencer'], function () {
    Route::group(['prefix' => 'campaign'], function () {
        Route::get('', 'CampaignController@index')->name('influencer.campaign.index');
        Route::get('/{id}', 'CampaignController@show')->name('influencer.campaign.show');
        Route::post('/approval', 'CampaignController@approval')->name('influencer.campaign.approval');
        Route::get('/email/send', 'CampaignController@sendemail')->name('influencer.campaign.email.send');
    });

    Route::get('/setting/{id}/{context}', 'InfluencerController@setting')->name('self.influencer.edit');
    Route::put('/{id}/{context}', 'InfluencerController@update')->name('self.influencer.update');

    Route::redirect('/home', '/influencer/campaign', 301);
});
/*++++++++++++++  END ROLE INFLUENCER  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

Route::group(['namespace' => 'Backend', 'middleware' => ['role:member', 'auth'], 'prefix' => 'member'], function () {

    Route::get('/{id}/edit', 'MemberController@edit')->name('self.member.edit');
    Route::put('/{id}', 'MemberController@update')->name('self.member.update');
    Route::get('/{id}/upgrade', 'MemberController@upgrade')->name('self.member.upgrade');
    Route::get('/{id}/upgradeRole/{role}', 'MemberController@role')->name('self.member.role');
    Route::post('/{id}/upgradeRole/{role}', 'MemberController@storeRole')->name('self.member.role.store');
});


/*++++++++++++++  ROLE USER  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
Route::group(['namespace' => 'Backend', 'middleware' => ['role:user', 'auth'], 'prefix' => 'user'], function () {

    Route::get('/revokeYt', 'YoutubeController@revokeYt')->name('revokeYt');
    Route::get('/refreshToken', 'YoutubeController@refreshToken')->name('refreshToken');
    Route::get('/authentication', 'YoutubeController@authentication')->name('authentication.network');
    Route::get('/auth/{provider}', 'YoutubeController@redirectToProvider');
    Route::get('/auth/{provider}/callback', 'YoutubeController@handleProviderCallback');
    Route::get('/connect-cms', 'YoutubeController@selectcms')->name('connect.cms');
    Route::post('/connect-cms', 'YoutubeController@updateselectedcms')->name('update.connect.cms');
    Route::get('/list/videos/youtube', 'YoutubeController@ListVideosYoutube')->name('ListVideosYoutube');
    Route::get('waiting/channel', 'YoutubeController@saveChannellist')->name('youtube.analytics.savechannel');

    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('', 'DashboardController@index')->name('user.dashboard.index');
    });

    Route::get('/addaccount', 'PersonalController@formAccount')->name('personal.addaccount');
    Route::post('/post/addaccount', 'PersonalController@postAccount')->name('personal.post.addaccount');
    //Route::get('/post/addaccount', 'PersonalController@postAccount')->name('personal.post.addaccount');

    Route::group(['prefix' => 'serie'], function () {
        Route::get('', 'SeriesController@index')->name('user.serie.index');
        Route::get('/create', 'SeriesController@create')->name('user.serie.create');
        Route::post('', 'SeriesController@store')->name('user.serie.store');
        Route::get('/{type}/edit', 'SeriesController@edit')->name('user.serie.edit');
        Route::put('/{type}', 'SeriesController@update')->name('user.serie.update');
        Route::get('/{type}', 'SeriesController@show')->name('user.serie.show');
        Route::post('/update-featured', 'SeriesController@updateFeatured')->name('user.serie.updateFeatured');
        Route::delete('/{type}', 'SeriesController@destroy')->name('user.serie.destroy');
    });

    Route::group(['prefix' => 'posts'], function () {
        Route::get('', 'PostsController@index')->name('user.posts.index');
        Route::get('/create-automatic', 'PostsController@createAutomatic')->name('user.posts.createAutomatic');
        Route::get('/create-manual', 'PostsController@createManual')->name('user.posts.createManual');
        Route::post('', 'PostsController@store')->name('user.posts.store');
        Route::get('/{type}/edit-automatic', 'PostsController@editAutomatic')->name('user.posts.editAutomatic');
        Route::get('/{type}/edit-manual', 'PostsController@editManual')->name('user.posts.editManual');
        Route::put('/{type}', 'PostsController@update')->name('user.posts.update');
        // Route::get('/{type}', 'PostsController@show')->name('posts.show');
        Route::get('checkYoutube', 'PostsController@checkYoutube')->name('user.posts.check');
        Route::post('/update-featured', 'PostsController@updateFeatured')->name('user.posts.updateFeatured');
        Route::post('/get-series', 'PostsController@getSeries')->name('user.posts.getSeries');
        Route::delete('/{type}', 'PostsController@destroy')->name('user.posts.destroy');
    });

    Route::group(['prefix' => 'platforms'], function () {
        Route::get('', 'PlatformsController@index')->name('user.platforms.index');
        Route::get('/log', 'PlatformsController@log')->name('user.platforms.log');
        Route::get('/callyt', 'PlatformsController@youtubeLogin')->name('user.platforms.callyt');
        Route::get('/callfb', 'PlatformsController@facebookLogin')->name('user.platforms.callfb');
        Route::get('youtube', 'PlatformsController@youtube')->name('user.platforms.youtube.login');
        Route::get('dailymotion', 'PlatformsController@dailymotion')->name('user.platforms.dailymotion.login');
        Route::get('{provider}/logout', 'PlatformsController@logout')->name('user.platforms.logout');
        Route::get('connectionCheck/{provider}', 'PlatformsController@connectionCheck')->name('user.platforms.connectionCheck');
        Route::get('/{provider}/log', 'PlatformsController@log')->name('user.platforms.log');
        Route::get('/{provider}/callback', 'PlatformsController@callbackLogin')->name('user.platforms.callback');
        Route::get('/upfb', 'PlatformsController@uploadFacebook')->name('user.platforms.upfb');
        Route::get('/tweet', 'PlatformsController@tweet')->name('user.platforms.tweet');
        Route::get('/logfb', 'PlatformsController@loginFb')->name('user.platforms.logfb');
        Route::get('/logdaily', 'PlatformsController@loginDaily')->name('user.platforms.logdaily');
    });

    Route::group(['prefix' => 'personal'], function () {
        Route::get('', 'PersonalController@index')->name('user.personal.index');
        Route::get('/create', 'PersonalController@create')->name('user.personal.create');
        Route::post('', 'PersonalController@store')->name('user.personal.store');
        Route::get('/{type}/edit', 'PersonalController@edit')->name('user.personal.edit');
        Route::put('/{type}', 'PersonalController@update')->name('user.personal.update');
        Route::get('/{type}', 'PersonalController@show')->name('user.personal.show');
        
        Route::get('/formapikey', 'PersonalController@formKeySetting')->name('user.personal.formapikey');
        Route::post('/save_apikey', 'PersonalController@keysetting')->name('user.personal.apikey');
    });

    Route::group(['prefix' => 'channels'], function () {
        Route::get('/leaderboard', 'ChannelController@index')->name('user.channels.leaderboard.index');
        Route::get('/list', 'ChannelController@list')->name('user.channels.list.index');
    });
    Route::group(['prefix' => 'cms'], function () {
        Route::get('', 'YoutubeController@cmslist')->name('user.cms.index');
        Route::get('detail', 'YoutubeCmsController@index')->name('user.cms.detail');
        Route::get('api/detail', 'YoutubeCmsController@api_detail')->name('user.cms.apidetail');

        Route::get('channel/{id}', 'YoutubeCmsController@channel')->name('user.cms.channel');        
    });

    Route::group(['prefix' => 'ytreporting'], function () {
        Route::get('', 'YoutubeReportingController@index')->name('user.ytreporting.index');
        Route::get('revokeaccess', 'YoutubeReportingController@revokeAccess')->name('user.ytreporting.revokeaccess');
        Route::get('getanalytics', 'YoutubeReportingController@getYTAnalytic')->name('user.ytreporting.getanalytics');
        Route::get('loading/channelanalitict', 'YoutubeReportingController@getChannelAnalytic')->name('user.ytreporting.getChannelAnalytic');
        Route::get('oauth2callback', 'YoutubeReportingController@YTReportingCallback')->name('user.ytreporting.oauth2callback');
        Route::get('loading/getstaticchannel', 'YoutubeReportingController@getStaticChannel')->name('user.ytreporting.getStaticChannel');
        Route::get('loading/getapicontentowner', 'YoutubeReportingController@getAPIAnaliticContentOwner')->name('user.ytreporting.getAPIAnaliticContentOwner');
        Route::get('loading/testontentowner', 'YoutubeReportingController@testAPIContentOwner')->name('user.ytreporting.testAPIContentOwner');
        Route::get('tesfollowersig', 'YoutubeReportingController@getFollowersIG')->name('user.ytreporting.getFollowersIG');
        Route::get('tesfollowersfb', 'YoutubeReportingController@getFollowersFB')->name('user.ytreporting.getFollowersFB');
    });

});
/*++++++++++++++  END ROLE USER  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/*++++++++++++++  ROLE FINANCE  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
Route::group(['namespace' => 'Backend', 'middleware' => ['role:finance', 'auth'], 'prefix' => 'finance'], function () {
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('', 'DashboardController@index')->name('finance.dashboard.index');
    });

    Route::group(['prefix' => 'personal'], function () {
        Route::get('', 'PersonalController@index')->name('finance.personal.index');
        Route::post('', 'PersonalController@store')->name('finance.personal.store');
        Route::get('/{type}/edit', 'PersonalController@edit')->name('finance.personal.edit');
        Route::put('/{type}', 'PersonalController@update')->name('finance.personal.update');
        Route::get('/{type}', 'PersonalController@show')->name('finance.personal.show');
    });

    Route::group(['prefix' => 'revenue'], function () {
        Route::get('approval', 'RevenueApprovalController@index')->name('finance.revenue.approval.index');
        Route::get('approver', 'RevenueApproverController@index')->name('finance.revenue.approver.index');
        Route::post('approver', 'RevenueApproverController@store')->name('finance.revenue.approver.store');
        Route::put('approver', 'RevenueApproverController@update')->name('finance.revenue.approver.update');
        
        Route::get('', 'RevenueController@index')->name('finance.revenue.index');
        Route::get('/create', 'RevenueController@create')->name('finance.revenue.create');
        Route::post('', 'RevenueController@store')->name('finance.revenue.store');
        Route::get('/{type}/edit', 'RevenueController@edit')->name('finance.revenue.edit');
        Route::get('/{id}/download', 'RevenueController@download')->name('finance.revenue.download');
        Route::get('/{id}/download-excel', 'RevenueController@excel')->name('finance.revenue.excel');
        Route::put('/{type}', 'RevenueController@financeUpdate')->name('finance.revenue.update');
        Route::post('/read-csv', 'RevenueController@readExcel')->name('finance.revenue.read');
        Route::get('/read-csv', 'RevenueController@uploadCsv')->name('finance.revenue.upload');
        Route::get('/{type}/view', 'RevenueController@view')->name('finance.revenue.view');
        Route::get('/template', 'RevenueController@template')->name('finance.revenue.template');
        Route::get('/download/template', 'RevenueController@downloadTry')->name('finance.revenue.download.template');

        /*edited Controller */
        Route::get('/read-csv', 'RevenueController@uploadCsv')->name('finance.revenue.upload');

        Route::get('/{id}/download_report', 'RevenueController@download_report')->name('finance.revenue.download.report');
        Route::get('/{id}/download_report_pdf', 'RevenueController@download_report_pdf')->name('finance.revenue.download.report_pdf');

        Route::post('/status_edit', 'RevenueController@statusEdit')->name('finance.revenue.status_edit');
        Route::get('/{id}/status_checked', 'RevenueController@StatusChecked')->name('finance.revenue.status_checked');
        Route::get('/{id}/status_revision', 'RevenueController@StatusRevision')->name('finance.revenue.status_revision');
        Route::get('/create_finance', 'RevenueController@viewFinanceInput')->name('finance.revenue.create_finance');
        Route::post('/create_finance', 'RevenueController@financeInput')->name('finance.revenue.proses_finance');
        Route::post('/update_finance', 'RevenueController@financeUpdate')->name('finance.revenue.updated_finance');
        Route::get('/{type}/edit/revisi', 'RevenueController@edit_revisi')->name('finance.revenue.edit.revisi');
        Route::post('/edit/revisi', 'RevenueController@update_revisi')->name('finance.revenue.post.revisi');
        Route::get('/destroy_revenue/{id}', 'RevenueController@destroyRevenue')->name('finance.revenue.destroy_revenue');
    });

    Route::group(['prefix' => 'dsp'], function () {
        // Route::get('approval', 'RevenueApprovalController@index')->name('revenue.approval.index');

        // Route::get('approver', 'RevenueApproverController@index')->name('revenue.approver.index');
        // Route::post('approver', 'RevenueApproverController@store')->name('revenue.approver.store');
        // Route::put('approver', 'RevenueApproverController@update')->name('revenue.approver.update');

        Route::get('', 'DspController@index')->name('finance.dsp.index');
        //Route::get('/create', 'DspController@create')->name('dsp.create');
        Route::post('', 'DspController@store')->name('finance.dsp.store');
        Route::get('/{type}/edit', 'DspController@edit')->name('finance.dsp.edit');
        Route::get('/{id}/download', 'DspController@download_report_pdf')->name('finance.dsp.download');
        Route::get('/{id}/download-excel', 'DspController@excel')->name('finance.dsp.excel');
        Route::put('/{type}', 'DspController@update')->name('finance.dsp.update');
        Route::post('/read-csv', 'DspController@readExcel')->name('finance.dsp.read');
        Route::get('/{type}/view', 'DspController@view')->name('finance.dsp.view');
        Route::get('/template', 'DspController@template')->name('finance.dsp.template');
        Route::get('/download/template', 'DspController@template')->name('finance.dsp.download.template');

        Route::get('/read-csv', 'DspController@uploadCsv')->name('finance.dsp.upload');

        Route::get('/{id}/download_report', 'DspController@download_report')->name('finance.dsp.download.report');
        Route::get('/{id}/download_report_pdf', 'DspController@download_report_pdf')->name('finance.dsp.download.report_pdf');


        Route::get('/create', 'DspController@createDSP')->name('finance.dsp.create'); //Edited
        Route::get('/create_dsp', 'DspController@createDSP')->name('finance.dsp.create_dsp');
        Route::post('/store_dsp', 'DspController@storeDSP')->name('finance.dsp.store_dsp');

        Route::get('/checked/{id}', 'DspController@status_checked')->name('finance.dsp.checked');
        Route::get('/revisi/{id}', 'DspController@status_revisi')->name('finance.dsp.revisi');
        Route::get('/finish/{id}', 'DspController@status_finish')->name('finance.dsp.finish');

        Route::get('{id}/dsp_detail/{isrc}', 'DspController@dsp_detail')->name('finance.dsp.dsp_details');
        Route::get('{id}/dsp_detail_print/{isrc}', 'DspController@print_detail_lagu')->name('finance.dsp.dsp_detail_print');
        Route::get('{id}/dsp_detail_print_fpp/{isrc}', 'DspController@print_fpp_detail_lagu')->name('finance.dsp.dsp_detail_print_fpp');
        Route::get('/printview/{id}', 'DspController@print_lagu')->name('finance.dsp.printview');

    });

    Route::group(['prefix' => 'creator'], function () {
        Route::get('', 'CreatorController@index')->name('finance.creator.index');
        Route::get('create', 'CreatorController@create')->name('finance.creator.create');
        Route::post('store', 'CreatorController@store')->name('finance.creator.store');
        Route::get('/{id}/edit', 'CreatorController@edit')->name('finance.creator.edit');
        Route::put('/{id}', 'CreatorController@update')->name('finance.creator.update');
        Route::delete('/{id}', 'UserController@destroy')->name('finance.creator.destroy');
    });

});
// ++++++++++++++  END ROLE FINANCE  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/*++++++++++++++  ROLE LEGAL  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
Route::group(['namespace' => 'Backend', 'middleware' => ['role:legal', 'auth'], 'prefix' => 'legal'], function () {
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('', 'DashboardController@index')->name('legal.dashboard.index');
    });

    Route::group(['prefix' => 'masterlagu'], function () {
        Route::get('', 'MasterLaguController@index')->name('legal.masterlagu.index');
        Route::get('/create', 'MasterLaguController@create')->name('legal.masterlagu.create');
        Route::post('', 'MasterLaguController@store')->name('legal.masterlagu.store');
        Route::get('/download/template', 'MasterLaguController@template')->name('legal.masterlagu.download.template');
        Route::get('/{id}/edit', 'MasterLaguController@edit')->name('legal.masterlagu.edit');
        Route::put('/{id}', 'MasterLaguController@update')->name('legal.masterlagu.update');
        Route::get('/{id}/detail', 'MasterLaguController@detail_lagu')->name('legal.masterlagu.detail.lagu');
        Route::get('/{id}/detail/singer', 'MasterLaguController@detail_singer')->name('legal.masterlagu.detail.singer');
        Route::get('/{id}/detail/songwriter', 'MasterLaguController@detail_songwriter')->name('legal.masterlagu.detail.songwriter');
        Route::get('/destroy/{id}', 'MasterLaguController@destroy')->name('legal.masterlagu.destroy');

        Route::get('/export_csv/', 'MasterLaguController@csv_export')->name('legal.masterlagu.export_csv');
        Route::get('/export_excell/', 'MasterLaguController@excell_export')->name('legal.masterlagu.export_excell');
        Route::get('/export_print/', 'MasterLaguController@export_print')->name('legal.masterlagu.export_print');
    });

    Route::group(['prefix' => 'mastersinger'], function () {
        Route::get('', 'MasterSingerController@index')->name('legal.mastersinger.index');
        Route::get('/create', 'MasterSingerController@create')->name('legal.mastersinger.create');
        Route::post('', 'MasterSingerController@store')->name('legal.mastersinger.store');
        Route::get('/{id}/edit', 'MasterSingerController@edit')->name('legal.mastersinger.edit');
        Route::put('/{id}', 'MasterSingerController@update')->name('legal.mastersinger.update');
        Route::get('/destroy/{id}', 'MasterSingerController@destroy')->name('legal.mastersinger.destroy');
    });
    
    Route::group(['prefix' => 'mastersongwriter'], function () {
        Route::get('', 'MasterSongwriterController@index')->name('legal.mastersongwriter.index');
        Route::get('/create', 'MasterSongwriterController@create')->name('legal.mastersongwriter.create');
        Route::post('', 'MasterSongwriterController@store')->name('legal.mastersongwriter.store');
        Route::get('/{id}/edit', 'MasterSongwriterController@edit')->name('legal.mastersongwriter.edit');
        Route::put('/{id}', 'MasterSongwriterController@update')->name('legal.mastersongwriter.update');
        Route::get('/destroy/{id}', 'MasterSongwriterController@destroy')->name('legal.mastersongwriter.destroy');
    });

    Route::group(['prefix' => 'personal'], function () {
        Route::get('', 'PersonalController@index')->name('legal.personal.index');
        Route::get('/create', 'PersonalController@create')->name('legal.personal.create');
        Route::post('', 'PersonalController@store')->name('legal.personal.store');
        Route::get('/{type}/edit', 'PersonalController@edit')->name('legal.personal.edit');
        Route::put('/{type}', 'PersonalController@update')->name('legal.personal.update');
        Route::get('/{type}', 'PersonalController@show')->name('legal.personal.show');
    });

});
/*++++++++++++++  END ROLE LEGAL  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/*++++++++++++++  ROLE SINGER  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

Route::group(['namespace' => 'Backend', 'middleware' => ['role:singer', 'auth'], 'prefix' => 'singer'], function () {
    
    Route::get('/addaccount', 'PersonalController@formAccount')->name('personal.addaccount');
    Route::post('/post/addaccount', 'PersonalController@postAccount')->name('personal.post.addaccount');

    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('', 'DashboardController@index')->name('singer.dashboard.index');
    });

    Route::group(['prefix' => 'personal'], function () {
        Route::get('', 'PersonalController@index')->name('singer.personal.index');
        Route::get('/create', 'PersonalController@create')->name('singer.personal.create');
        Route::post('', 'PersonalController@store')->name('singer.personal.store');
        Route::get('/{type}/edit', 'PersonalController@edit')->name('singer.personal.edit');
        Route::put('/{type}', 'PersonalController@update')->name('singer.personal.update');
        Route::get('/{type}', 'PersonalController@show')->name('singer.personal.show');
    });

    Route::group(['prefix' => 'masterlagu'], function () {
        Route::get('', 'MasterLaguController@index')->name('singer.masterlagu.index');
        Route::get('/{id}/detail', 'MasterLaguController@detail_lagu')->name('singer.masterlagu.detail.lagu');

        Route::get('/export_excell/', 'MasterLaguController@excell_export')->name('singer.masterlagu.export_excell');
        Route::get('/export_print/', 'MasterLaguController@export_print')->name('singer.masterlagu.export_print');
    });

    Route::group(['prefix' => 'listlagu'], function () {
        Route::get('', 'DashboardController@index')->name('singer.listlagu.index');
    });
    Route::group(['prefix' => 'dsp'], function () {
        Route::get('', 'DspController@index')->name('singer.dsp.index');
        Route::get('/{type}/view', 'DspController@view')->name('singer.dsp.view');
        Route::get('{id}/dsp_detail/{isrc}', 'DspController@dsp_detail')->name('singer.dsp.dsp_details');
        //Route::get('{id}/dsp_detail_print/{isrc}', 'DspController@print_detail_lagu')->name('singer.dsp.dsp_detail_print');
        Route::get('{id}/dsp_detail_print/{isrc}', 'DspController@print_detail_lagu')->name('singer.dsp.dsp_detail_print');
        Route::get('{id}/dsp_detail_print_fpp/{isrc}', 'DspController@print_fpp_detail_lagu')->name('singer.dsp.dsp_detail_print_fpp');
        Route::get('/printview/{id}', 'DspController@print_lagu')->name('singer.dsp.printview');
    });
});

/*++++++++++++++  END ROLE SINGER  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


/*++++++++++++++  ROLE SONGWRITER  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

Route::group(['namespace' => 'Backend', 'middleware' => ['role:songwriter', 'auth'], 'prefix' => 'songwriter'], function () {
    
    Route::get('/addaccount', 'PersonalController@formAccount')->name('personal.addaccount');
    Route::post('/post/addaccount', 'PersonalController@postAccount')->name('personal.post.addaccount');

    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('', 'DashboardController@index')->name('songwriter.dashboard.index');
    });

    Route::group(['prefix' => 'personal'], function () {
        Route::get('', 'PersonalController@index')->name('songwriter.personal.index');
        Route::get('/create', 'PersonalController@create')->name('songwriter.personal.create');
        Route::post('', 'PersonalController@store')->name('songwriter.personal.store');
        Route::get('/{type}/edit', 'PersonalController@edit')->name('songwriter.personal.edit');
        Route::put('/{type}', 'PersonalController@update')->name('songwriter.personal.update');
        Route::get('/{type}', 'PersonalController@show')->name('songwriter.personal.show');
    });

    Route::group(['prefix' => 'masterlagu'], function () {
        Route::get('', 'MasterLaguController@index')->name('songwriter.masterlagu.index');

        Route::get('/export_excell/', 'MasterLaguController@excell_export')->name('songwriter.masterlagu.export_excell');
        Route::get('/export_print/', 'MasterLaguController@export_print')->name('songwriter.masterlagu.export_print');
    });

    Route::group(['prefix' => 'listlagu'], function () {
        Route::get('', 'DashboardController@index')->name('songwriter.listlagu.index');
    });
    Route::group(['prefix' => 'dsp'], function () {
        Route::get('', 'DspController@index')->name('songwriter.dsp.index');
        Route::get('/{type}/view', 'DspController@view')->name('songwriter.dsp.view');
        Route::get('{id}/dsp_detail/{isrc}', 'DspController@dsp_detail')->name('songwriter.dsp.dsp_details');
        Route::get('{id}/dsp_detail_print/{isrc}', 'DspController@print_detail_lagu')->name('songwriter.dsp.dsp_detail_print');
        Route::get('{id}/dsp_detail_print_fpp/{isrc}', 'DspController@print_fpp_detail_lagu')->name('songwriter.dsp.dsp_detail_print_fpp');
        Route::get('/printview/{id}', 'DspController@print_lagu')->name('songwriter.dsp.printview');
    });

});

/*++++++++++++++  END ROLE SONGWRITER  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/*++++++++++++++  ROLE ANR  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

Route::group(['namespace' => 'Backend', 'middleware' => ['role:anr', 'auth'], 'prefix' => 'anr'], function () {
    
    Route::get('/addaccount', 'PersonalController@formAccount')->name('personal.addaccount');
    Route::post('/post/addaccount', 'PersonalController@postAccount')->name('personal.post.addaccount');

    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('', 'DashboardController@index')->name('anr.dashboard.index');
    });

    Route::group(['prefix' => 'personal'], function () {
        Route::get('', 'PersonalController@index')->name('anr.personal.index');
        Route::get('/create', 'PersonalController@create')->name('anr.personal.create');
        Route::post('', 'PersonalController@store')->name('anr.personal.store');
        Route::get('/{type}/edit', 'PersonalController@edit')->name('anr.personal.edit');
        Route::put('/{type}', 'PersonalController@update')->name('anr.personal.update');
        Route::get('/{type}', 'PersonalController@show')->name('anr.personal.show');
    });

    Route::group(['prefix' => 'masterlagu'], function () {
        Route::get('', 'MasterLaguController@index')->name('anr.masterlagu.index');
        Route::get('/create', 'MasterLaguController@create')->name('anr.masterlagu.create');
        Route::post('', 'MasterLaguController@store')->name('anr.masterlagu.store');
        Route::get('/download/template', 'MasterLaguController@template')->name('anr.masterlagu.download.template');
        Route::get('/{id}/edit', 'MasterLaguController@edit')->name('anr.masterlagu.edit');
        Route::put('/{id}', 'MasterLaguController@update')->name('anr.masterlagu.update');
        Route::get('/{id}/detail', 'MasterLaguController@detail_lagu')->name('anr.masterlagu.detail.lagu');
        Route::get('/{id}/detail/singer', 'MasterLaguController@detail_singer')->name('anr.masterlagu.detail.singer');
        Route::get('/{id}/detail/songwriter', 'MasterLaguController@detail_songwriter')->name('anr.masterlagu.detail.songwriter');
        Route::get('/destroy/{id}', 'MasterLaguController@destroy')->name('anr.masterlagu.destroy');

        Route::get('/export_csv/', 'MasterLaguController@csv_export')->name('anr.masterlagu.export_csv');
        Route::get('/export_excell/', 'MasterLaguController@excell_export')->name('anr.masterlagu.export_excell');
        Route::get('/export_print/', 'MasterLaguController@export_print')->name('anr.masterlagu.export_print');
    });

    Route::group(['prefix' => 'mastersinger'], function () {
        Route::get('', 'MasterSingerController@index')->name('anr.mastersinger.index');
        Route::get('/create', 'MasterSingerController@create')->name('anr.mastersinger.create');
        Route::post('', 'MasterSingerController@store')->name('anr.mastersinger.store');
        Route::get('/{id}/edit', 'MasterSingerController@edit')->name('anr.mastersinger.edit');
        Route::put('/{id}', 'MasterSingerController@update')->name('anr.mastersinger.update');
        Route::get('/destroy/{id}', 'MasterSingerController@destroy')->name('anr.mastersinger.destroy');
    });
    
    Route::group(['prefix' => 'mastersongwriter'], function () {
        Route::get('', 'MasterSongwriterController@index')->name('anr.mastersongwriter.index');
        Route::get('/create', 'MasterSongwriterController@create')->name('anr.mastersongwriter.create');
        Route::post('', 'MasterSongwriterController@store')->name('anr.mastersongwriter.store');
        Route::get('/{id}/edit', 'MasterSongwriterController@edit')->name('anr.mastersongwriter.edit');
        Route::put('/{id}', 'MasterSongwriterController@update')->name('anr.mastersongwriter.update');
        Route::get('/destroy/{id}', 'MasterSongwriterController@destroy')->name('anr.mastersongwriter.destroy');
    });

    Route::group(['prefix' => 'dsp'], function () {
        Route::get('', 'DspController@index')->name('anr.dsp.index');
        Route::get('/{type}/view', 'DspController@view')->name('anr.dsp.view');
        Route::get('{id}/dsp_detail/{isrc}', 'DspController@dsp_detail')->name('anr.dsp.dsp_details');
        Route::get('{id}/dsp_detail_print/{isrc}', 'DspController@print_detail_lagu')->name('anr.dsp.dsp_detail_print');
        Route::get('{id}/dsp_detail_print_fpp/{isrc}', 'DspController@print_fpp_detail_lagu')->name('anr.dsp.dsp_detail_print_fpp');
        Route::get('/printview/{id}', 'DspController@print_lagu')->name('anr.dsp.printview');
    });

});

/*++++++++++++++  ROLE CMS  +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

Route::group(['namespace' => 'Backend', 'middleware' => ['role:cms', 'auth'], 'prefix' => 'cms'], function () {

    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('', 'DashboardController@index')->name('cms.dashboard.index');
    });

});

/*++++++++++++++  END ROLE CMS  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Auth::routes();
