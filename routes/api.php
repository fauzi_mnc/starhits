<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['namespace' => 'Modules'], function () {
    Route::get('/creators', 'APIWebsiteController@creatorsAPI')->name('api.creators');
    Route::get('/creators/video-list/{param}', 'APIWebsiteController@creatorsVideoListAPI')->name('api.creators.videolist');
    Route::get('/creators/feature-page', 'APIWebsiteController@creatorsFeaturePageAPI')->name('api.creators.feature.page');
    Route::get('/creators/no-feature-page', 'APIWebsiteController@creatorsNoFeaturePageAPI')->name('api.creators.feature.page');
    Route::get('/creators/feature-home', 'APIWebsiteController@creatorsFeatureHomeAPI')->name('api.creators.feature.home');
    Route::get('/creators/personal', 'APIWebsiteController@creatorsPersonalAPI')->name('api.creators.personal');
    Route::get('/creators/corporate', 'APIWebsiteController@creatorsCorporateAPI')->name('api.creators.corporate');
    Route::get('/creators/most-views', 'APIWebsiteController@creatorsMostViewsAPI')->name('api.creators.views');
    Route::get('/creators/most-subscribers', 'APIWebsiteController@creatorsMostSubsAPI')->name('api.creators.subscribers');
    Route::get('/creators/corporate', 'APIWebsiteController@creatorsCorporateAPI')->name('api.creators.corporate');
    Route::get('/series', 'APIWebsiteController@seriesAPI')->name('api.series');
    Route::get('/channels', 'APIWebsiteController@channelsAPI')->name('api.channels');
    Route::get('/channel-videos', 'APIWebsiteController@channelVideos')->name('api.channelVideos');
    Route::get('/channel-latest-video/{param}', 'APIWebsiteController@getDataVideoChannel')->name('api.getDataVideoChannel');
    Route::get('/channel-latest-series/{param}', 'APIWebsiteController@getDataSeriesChannel')->name('api.getDataSeriesChannel');
    Route::get('/portfolio/{slug}', 'APIWebsiteController@portfolioAPI')->name('api.portfolio.slug');
    Route::get('/videos', 'APIWebsiteController@videosAPI')->name('api.videos');
});
