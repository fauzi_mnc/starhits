<?php

namespace App\DataTables;

use App\Model\User;
use App\Model\CampaignPost;
use App\Model\Campaign;
use App\Model\Influencer;
use App\Model\Nego;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Illuminate\Http\Request;
use Auth;

class NInfluencerDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('ig_story', function($m){
                if (Nego::where([['camp_id', request()->segment(3)],['infl_id', $m->id],['post_id', 2]])->exists()) {
                    $nego = Nego::where([['camp_id', request()->segment(3)],['infl_id', $m->id],['post_id', 2]])->first();
                    return $nego->post_budget;
                }else{
                    return $m->detailInfluencer->instagram_story_posting_rate;
                }
            })->addColumn('ig_photo', function($m){
                if (Nego::where([['camp_id', request()->segment(3)],['infl_id', $m->id],['post_id', 3]])->exists()) {
                    $nego = Nego::where([['camp_id', request()->segment(3)],['infl_id', $m->id],['post_id', 3]])->first();
                    return $nego->post_budget;
                }else{
                    return $m->detailInfluencer->instagram_photo_posting_rate;
                }
            })->addColumn('ig_video', function($m){
                if (Nego::where([['camp_id', request()->segment(3)],['infl_id', $m->id],['post_id', 1]])->exists()) {
                    $nego = Nego::where([['camp_id', request()->segment(3)],['infl_id', $m->id],['post_id', 1]])->first();
                    return $nego->post_budget;
                }else{
                    return $m->detailInfluencer->instagram_video_posting_rate;
                }
            })->addColumn('youtube', function($m){
                if (Nego::where([['camp_id', request()->segment(3)],['infl_id', $m->id],['post_id', 4]])->exists()) {
                    $nego = Nego::where([['camp_id', request()->segment(3)],['infl_id', $m->id],['post_id', 4]])->first();
                    return $nego->post_budget;
                }else{
                    return $m->detailInfluencer->youtube_posting_rate;
                }
            })->addColumn('highlight', function($m){
                if (Nego::where([['camp_id', request()->segment(3)],['infl_id', $m->id],['post_id', 5]])->exists()) {
                    $nego = Nego::where([['camp_id', request()->segment(3)],['infl_id', $m->id],['post_id', 5]])->first();
                    return $nego->post_budget;
                }else{
                    return $m->detailInfluencer->ig_highlight_rate;
                }
            })->addColumn('package_rate', function($m){
                if (Nego::where([['camp_id', request()->segment(3)],['infl_id', $m->id],['post_id', 6]])->exists()) {
                    $nego = Nego::where([['camp_id', request()->segment(3)],['infl_id', $m->id],['post_id', 6]])->first();
                    return $nego->post_budget;
                }else{
                    return $m->detailInfluencer->package_rate;
                }
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Posts $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        $fil = Influencer::where('campaign_id', request()->segment(3))->get()->pluck('user_id')->toArray();
        return User::where('is_active', 1)
        ->with('accessrole')
        ->whereHas('accessrole', function($query){
            $query->where('role_id', '6');
        })
        ->with('influencer')
        ->whereHas('influencer', function($q) use ($fil){
            $q->whereIn('user_id', $fil)->where('approval', 1)->where('campaign_id', request()->segment(3));
        })->with('detailInfluencer');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'responsive' => true,
                'autoWidth' => false,
                'buttons' => [
                    // 'create',
                    // 'export',
                    // 'print',
                    'reset',
                    'reload',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name' => ['title' => 'Influencer'],
            'ig_story' => ['title' => 'Price IG Story'],
            'ig_photo' => ['title' => 'Price IG Photo'],
            'ig_video' => ['title' => 'Price IG Video'],
            'youtube' => ['title' => 'Price Youtube Video'],
            'highlight' => ['title' => 'Price IG Highlight'],
            'package_rate' => ['title' => 'All Package'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'influencer_datatable' . time();
    }
}