<?php

namespace App\DataTables;

use App\Model\Revenue;
use App\Model\RevenueCreator;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use App\Enum\RevenueStatus;
use Auth;

class RevenueCreatorDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $percentage = Auth::user()->percentage;

        return $dataTable
                        /*->addColumn('month', function($m){
                                $monthName = date('F', mktime(0, 0, 0, $m->month, 10)); // March
                                return $monthName; 
                            })*/
                        ->addColumn('month', function($m){
                            $monthName = date('F', mktime(0, 0, 0, $m->month, 10)); // March
                            return $monthName; 
                        })

                        ->addColumn('Year', function($m){
                            return $m->year;
                        })
                        ->addColumn('revenue', function($m){
                            $rows = $this->queryRevenue($m->id);
                            return "Rp. ".number_format($rows->revenue);

                        })->addColumn('CostProduction', function($m){
                            $rows = $this->queryRevenue($m->id);
                            return "Rp. ".number_format($rows->cost_production);

                        })->addColumn('NettRevenue', function($m){
                            $rows = $this->queryRevenue($m->id);
                            return "Rp. ".number_format($rows->nett_revenue);
                        })->addColumn('ShareRevenue', function($m){
                            $rows = $this->queryRevenue($m->id);
                            return "Rp. ".number_format($rows->share_revenue);

                        })->addColumn('status_paid', function($m){
                            $rows = $this->queryRevenue($m->id);
                            //return $rows->status_paid;
                            if($rows->status_paid == 3){
                                return "Paid";
                            }elseif($rows->status_paid == 2){
                                return "On Process";
                            }else{
                                return "Pending";
                            }
                        })
                        //->orderColumns(['Month', 'Year'], '-:column $1')
                        ->orderColumn('month', '-month $1')
                        //->orderColumn('revenue', '-column $1')
                        ->addColumn('action', 'revenue.creator.datatables_actions');
    }

    public function queryRevenue($val){
        $a = RevenueCreator::where([
                    'revenue_id'=>$val,
                    'creator_id'=>Auth::user()->id
                    ])->first();
        return $a;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Revenue $model)
    {
        $query = $model->with(['creator'])
                        ->whereHas('creator', function($query){
                            $query->where('creator_id', Auth::user()->id);
                            $query->where('status', 4);
                        });
                        

        if(request()->month){
            $query->where('month', request()->month);
        }

        /*if(request()->year){
            $query->where('year', request()->year);
        }
        if(request()->status){
            $query->where('status', request()->status);
        }*/
        /*if(request()->revenue){
            $query->where('revenue', request()->revenue);
        }*/
        

        return $query->orderBy('created_at', 'desc');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax([
                'url'  => '',
                'data' => "function(d) {
                    d.month  = $('#month').val();
                    d.year  = $('#Year').val();
                    d.revenue  = $('#revenue').val();
                    d.cost  = $('#CostProduction').val();
                    d.nett  = $('#NettRevenue').val();
                    d.share  = $('#ShareRevenue').val();
                    d.status_paid  = $('#status_paid').val();

                }",
            ])
            ->addAction(['width' => '80px'])
            ->parameters([
                'dom'     => 'Brtlip',
                'order'   => [[0, 'desc']],
                'responsive' => true,
                'autoWidth' => false,
                'buttons' => [
                // 'create',
                // 'export',
                // 'print',
                //   'reset',
                // 'reload',
                ],
            ]);
    }

    protected function getColumns()
    {
        return [
            'month',
            'year',
            'revenue',
            'CostProduction',
            'NettRevenue',
            'ShareRevenue',
            'status_paid',
            
        ];
    }

}
