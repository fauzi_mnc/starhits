<?php

namespace App\DataTables;

use App\Model\Campaign;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Illuminate\Http\Request;
use Auth;

class CampaignDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        if (Auth::user()->hasRole('influencer')) {
            return $dataTable->addColumn('is_active', function($m){
                            return $m->active? 'Yes' : 'No';
                        })->addColumn('approval', function ($m) {
                            foreach ($m->influencer as $value) {
                                if ($value->user_id == Auth::user()->id) {
                                    if($value->approval === 1){
                                        return 'Approved';
                                    }elseif ($value->approval === 0) {
                                        return 'Reject';
                                    }elseif (empty($value->approval)){
                                        return 'Process';
                                    }
                                }
                            }
                        })->addColumn('action', 'campaign.datatables_actions');
        }else{
            return $dataTable->addColumn('status', function ($m) {
                            if($m->status == '1'){
                                return 'Draft';
                            }elseif ($m->status == '2') {
                                return 'Process';
                            }elseif ($m->status == '3'){
                                return 'Running';
                            }elseif ($m->status == '4'){
                                return 'Expired';
                            }
                        })->addColumn('is_active', function($m){
                            return $m->active? 'Yes' : 'No';
                        })->addColumn('action', 'campaign.datatables_actions');
        }
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Posts $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Request $request, Campaign $model)
    {
        // dd($request->all());
        if (Auth::user()->roles->first()->name == 'admin') {
            if (!empty($request->statuz) && !empty($request->queri)) {
                return $model->with('user')->newQuery()->where('campaign.status', $request->statuz)->where('campaign.title', 'LIKE', '%'.$request->queri.'%')->orderBy('campaign.updated_at', 'desc');
            }elseif (!empty($request->statuz)) {
                return $model->with('user')->newQuery()->where('campaign.status', $request->statuz)->orderBy('campaign.updated_at', 'desc');
            }elseif (!empty($request->queri)) {
                return $model->with('user')->newQuery()->where('campaign.title', 'LIKE', '%'.$request->queri.'%')->orderBy('campaign.updated_at', 'desc');
            }else{
                return $model->with('user')->newQuery()->orderBy('campaign.updated_at', 'desc');
            }
        }elseif(Auth::user()->hasRole('brand')){
            return $model->with('user')->newQuery()->where('brand_id', Auth::user()->id)->orderBy('campaign.updated_at', 'desc');
        }elseif(Auth::user()->hasRole('influencer')) {
            return $model->with('user')->with('influencer')->whereHas('influencer', function($query){
                $query->where('user_id', Auth::user()->id);
            })->newQuery()->where('campaign.status', '!=', 1)->orderBy('campaign.updated_at', 'desc');
        }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        if (Auth::user()->roles->first()->name == 'admin') {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax([
                'url'  => '',
                'data' => "function(d) { 
                    d.statuz  = $('#status').val();
                    d.queri  = $('#query').val();
                }",
            ])
            ->addAction(['width' => '80px'])
            ->parameters([
                'sDom'    => 'rt<"bottom"ip><"clear">',
                'order'   => [[0, 'desc']],
                'responsive' => true,
                'autoWidth' => false,
                // 'buttons' => [
                    // 'create',
                    // 'excel',
                    // 'print',
                    // 'reset',
                    // 'reload',
                // ],
            ]);
        }else{
            return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'responsive' => true,
                'autoWidth' => false,
                'buttons' => [
                    // 'create',
                    // 'export',
                    // 'print',
                    'reset',
                    'reload',
                ],
            ]);
        }
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        if (Auth::user()->roles->first()->name == 'influencer') {
            return [
                'user' => ['title' => 'Brand', 'data' => 'user.brand_name', 'name' => 'user.brand_name'],
                'title' => ['title' => 'Campaign Title'],
                'is_active',
                'approval'
            ];
        }else{
            return [
                'user' => ['title' => 'Brand', 'data' => 'user.brand_name', 'name' => 'user.brand_name'],
                'title' => ['title' => 'Campaign Title'],
                'status',
                'is_active'
            ];
        }
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'campaign_datatable' . time();
    }
}