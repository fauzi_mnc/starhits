<?php

namespace App\DataTables;

use App\Model\Serie;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Auth;

class SeriesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('is_active', function($m){
                            return $m->is_active? 'Ya' : 'Tidak';
                        })->addColumn('channel', function($m){
                            return $m->channels? $m->channels->title : '';
                        })->addColumn('is_featured', function($m){
                            return $m->is_featured? '<div class="switch">
                                <div class="onoffswitch">
                                    <input type="checkbox" checked class="onoffswitch-checkbox featured" id="example'.$m->id.'" data-id="'.$m->id.'">
                                    <label class="onoffswitch-label" for="example'.$m->id.'">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>' : '<div class="switch">
                                <div class="onoffswitch">
                                    <input type="checkbox" class="onoffswitch-checkbox featured" id="example'.$m->id.'" data-id="'.$m->id.'">
                                    <label class="onoffswitch-label" for="example'.$m->id.'">
                                        <span class="onoffswitch-inner"></span>
                                        <span class="onoffswitch-switch"></span>
                                    </label>
                                </div>
                            </div>';
                        })->addColumn('action', 'series.datatables_actions')
                        ->rawColumns(['is_featured', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Serie $model)
    {
        if (Auth::user()->roles->first()->name == 'admin') {
            return $model->newQuery()->orderBy('updated_at', 'desc');
        }elseif (Auth::user()->roles->first()->name == 'user') {
            return $model->newQuery()->where('user_id', Auth::user()->id)->orderBy('updated_at', 'desc');
        }else{
            return $model->newQuery()->where('user_id', Auth::user()->id)->orderBy('updated_at', 'desc');
        }
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '120px'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'responsive' => true,
                'autoWidth' => false,
                'buttons' => [
                    // 'create',
                    // 'export',
                    // 'print',
                    'reset',
                    'reload',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'title',
            //'type',
            //'content',
            'channel',
            'image',
            'is_featured',
            'is_active'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'series_datatable_' . time();
    }
}