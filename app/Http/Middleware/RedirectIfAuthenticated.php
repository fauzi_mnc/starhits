<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $path = '';

            if (Auth::user()->hasRole('creator')) {
                $path = '/creator/personal';
            } elseif (Auth::user()->hasRole('brand')) {
                $path = '/brand/campaign';
            } elseif (Auth::user()->hasRole('influencer')) {
                $path = '/influencer/campaign';
            } elseif (Auth::user()->hasRole('member')) {
                $path = '/member/'.auth()->id().'/edit';
            } elseif (Auth::user()->hasRole('user')) {
                $path = '/user/dashboard';
            } elseif (Auth::user()->hasRole('finance')) {
                $path = '/finance/dashboard';
            } elseif (Auth::user()->hasRole('anr')) {
                $path = '/anr/dashboard';
            } elseif (Auth::user()->hasRole('legal')) {
                $path = '/legal/dashboard';
            }elseif (Auth::user()->hasRole('singer')) {
                $path = '/singer/dashboard';
            }elseif (Auth::user()->hasRole('songwriter')) {
                $path = '/songwriter/dashboard';
            }elseif (Auth::user()->hasRole('cms')) {
                $path = '/cms/dashboard';
            } else {
                $path = '/admin/dashboard';
            }
            
            return redirect($path);
        }

        return $next($request);
    }
}
