<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrUpdateChannelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'      => 'required|max:125|regex:/^[a-zA-Z0-9\s]*$/',
            'content'    => 'required|max:5000',
            'image'      => 'image|mimes:jpg,png,jpeg|max:1000'
        ];
    }

    public function messages()
    {
        return [
            'content.required' => 'Description field is required.',
            'content.max' => 'Description field may not be greater than 5000 characters.'
        ];
    }
}
