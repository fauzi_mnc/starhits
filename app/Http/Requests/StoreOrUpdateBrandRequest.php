<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\BrandEmail;

class StoreOrUpdateBrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|max:20',
            'email'     => ['required', 'unique:users,email', new BrandEmail],
            'password'  => "min:12|required|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/",
            'brand_name'=> 'required|max:30',
            'phone'     => ['max:30', 'regex:/\+?([ -]?\d+)+|\(\d+\)([ -]\d+)/', 'nullable']
        ];
    }

    public function messages()
    {
        return [
            'password.regex' => 'Your password should contain at-least 1 Uppercase, 1 Lowercase, 1 Numeric and 1 special character'
        ];
    }
}
