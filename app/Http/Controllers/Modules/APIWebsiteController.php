<?php

namespace App\Http\Controllers\Modules;

use App\Model\User;
use App\Model\Series;
use App\Model\Video;
use App\Model\Channel;
use App\Model\Portofolio;
use App\Model\ContentPortofolio;
use App\Http\Controllers\Website\Controller;
use Image;
use DB;
use Illuminate\Http\Request;


class APIWebsiteController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */

    // public function portfolioAPI()
    // {
    //     $portfolioAPI = Portofolio::select(DB::raw('id_portofolio,id_user,porto_title,porto_header,fnStripTags(content1) as content,porto_content,porto_img1,porto_img2,campaign_title,fnStripTags(campaign_content) as campaign_content,id_content,isi_content,media_content,content_portofolio.created_at,content_portofolio.updated_at,id,name'))->join('users', 'content_portofolio.id_user', '=', 'users.id')->paginate(6);
        
    //     return response()->json([
    //         'items' => $portfolioAPI->items(),
    //         'next_page_url' => $portfolioAPI->nextPageUrl()
    //     ])->header('Access-Control-Allow-Origin', '*');
    // }

    public function portfolioAPI($slug)
    {
        $portfolioAPI = Portofolio::select(DB::raw('id_portofolio,id_user,porto_title,porto_header,fnStripTags(content1) as content,porto_content,porto_img1,porto_img2,campaign_title,fnStripTags(campaign_content) as campaign_content,id_content,isi_content,media_content,content_portofolio.created_at,content_portofolio.updated_at,id,name'))->join('users', 'content_portofolio.id_user', '=', 'users.id')->where('porto_type', $slug)->paginate(6);
        
        return response()->json([
            'items' => $portfolioAPI->items(),
            'next_page_url' => $portfolioAPI->nextPageUrl()
        ])->header('Access-Control-Allow-Origin', '*');
    }

    public function videosAPI()
    {
        $videosAPI = Video::with(['users', 'channelVideo', 'series'])->orderBy('created_at', 'desc')->paginate(6);
        
        return response()->json([
            'items' => $videosAPI->items(),
            'next_page_url' => $videosAPI->nextPageUrl()
        ])->header('Access-Control-Allow-Origin', '*');
    }

    public function creatorsMostViewsAPI()
    {
        $creatorsAPI = User::select('id','name','name_channel','cover','image','image_width','image_height','provider_id','is_feature_home','is_feature_page','img_header','img_content','feature_home','feature_page','corporate_creator','feature_sort_home','feature_sort_home','subscribers','views','yt_subs','followers_ig','followers_fb','followers_twitter','slug')->with('socials')
        ->whereHas('accessrole', function ($query) {
            $query->where('role_id', '2');
        })
        ->where('is_active', '=', 1)
        ->where('is_show', '=', 1)
        ->orderBy('views', 'desc')
        ->paginate(6);
        
        return response()->json([
            'items' => $creatorsAPI->items(),
            'next_page_url' => $creatorsAPI->nextPageUrl()
        ])->header('Access-Control-Allow-Origin', '*');
    }

    public function creatorsMostSubsAPI()
    {
        $creatorsAPI = User::select('id','name','name_channel','cover','image','image_width','image_height','provider_id','is_feature_home','is_feature_page','img_header','img_content','feature_home','feature_page','corporate_creator','feature_sort_home','feature_sort_home','subscribers','views','yt_subs','followers_ig','followers_fb','followers_twitter','slug')->with('socials')
        ->whereHas('accessrole', function ($query) {
            $query->where('role_id', '2');
        })
        ->where('is_active', '=', 1)
        ->where('is_show', '=', 1)
        ->orderBy('subscribers', 'desc')
        ->paginate(6);
        
        return response()->json([
            'items' => $creatorsAPI->items(),
            'next_page_url' => $creatorsAPI->nextPageUrl()
        ])->header('Access-Control-Allow-Origin', '*');
    }

    public function creatorsPersonalAPI(Request $request)
    {
        if(empty($request->query('f'))){
            $creatorsAPI = User::select('id','name','name_channel','cover','image','image_width','image_height','provider_id','is_feature_home','is_feature_page','img_header','img_content','feature_home','feature_page','corporate_creator','feature_sort_home','feature_sort_home','subscribers','views','yt_subs','followers_ig','followers_fb','followers_twitter','slug')->with('socials')
            ->whereHas('accessrole', function ($query) {
                $query->where('role_id', '2');
            })
            ->where('is_active', '=', 1)
            ->where('is_show', '=', 1)
            ->where('corporate_creator', '=', 'creator_personal')
            ->groupBy('slug')
            ->paginate(6);
        }elseif($request->query('f') == 'most-subscribers'){
            $creatorsAPI = User::select('id','name','name_channel','cover','image','image_width','image_height','provider_id','is_feature_home','is_feature_page','img_header','img_content','feature_home','feature_page','corporate_creator','feature_sort_home','feature_sort_home','subscribers','views','yt_subs','followers_ig','followers_fb','followers_twitter','slug')->with('socials')
            ->whereHas('accessrole', function ($query) {
                $query->where('role_id', '2');
            })
            ->where('is_active', '=', 1)
            ->where('is_show', '=', 1)
            ->where('corporate_creator', '=', 'creator_personal')
            ->groupBy('slug')
            ->orderBy('subscribers', 'desc')
            ->paginate(6);
        }elseif($request->query('f') == 'most-views'){
            $creatorsAPI = User::select('id','name','name_channel','cover','image','image_width','image_height','provider_id','is_feature_home','is_feature_page','img_header','img_content','feature_home','feature_page','corporate_creator','feature_sort_home','feature_sort_home','subscribers','views','yt_subs','followers_ig','followers_fb','followers_twitter','slug')->with('socials')
            ->whereHas('accessrole', function ($query) {
                $query->where('role_id', '2');
            })
            ->where('is_active', '=', 1)
            ->where('is_show', '=', 1)
            ->where('corporate_creator', '=', 'creator_personal')
            ->groupBy('slug')
            ->orderBy('views', 'desc')
            ->paginate(6);
        }
        
        if(empty($request->query('f')) || empty($request->query('page'))){
            return response()->json([
                'items' => $creatorsAPI->items(),
                'next_page_url' => $creatorsAPI->nextPageUrl()
            ])->header('Access-Control-Allow-Origin', '*');
        }else{
            return response()->json([
                'items' => $creatorsAPI->items(),
                'next_page_url' => $creatorsAPI->nextPageUrl().'&f='.$request->query('f')
            ])->header('Access-Control-Allow-Origin', '*');
        }
    }

    public function creatorsCorporateAPI(Request $request)
    {
        if(empty($request->query('f'))){
            $creatorsAPI = User::select('id','name','name_channel','cover','image','image_width','image_height','provider_id','is_feature_home','is_feature_page','img_header','img_content','feature_home','feature_page','corporate_creator','feature_sort_home','feature_sort_home','subscribers','views','yt_subs','followers_ig','followers_fb','followers_twitter','slug')->with('socials')
            ->whereHas('accessrole', function ($query) {
                $query->where('role_id', '2');
            })
            ->where('is_active', '=', 1)
            ->where('is_show', '=', 1)
            ->where('corporate_creator', '=', 'creator_corporate')
            ->groupBy('id')
            ->paginate(6);
        }elseif($request->query('f') == 'most-subscribers'){
            $creatorsAPI = User::select('id','name','name_channel','cover','image','image_width','image_height','provider_id','is_feature_home','is_feature_page','img_header','img_content','feature_home','feature_page','corporate_creator','feature_sort_home','feature_sort_home','subscribers','views','yt_subs','followers_ig','followers_fb','followers_twitter','slug')->with('socials')
            ->whereHas('accessrole', function ($query) {
                $query->where('role_id', '2');
            })
            ->where('is_active', '=', 1)
            ->where('is_show', '=', 1)
            ->where('corporate_creator', '=', 'creator_corporate')
            ->groupBy('id')
            ->orderBy('subscribers', 'desc')
            ->paginate(6);
        }elseif($request->query('f') == 'most-views'){
            $creatorsAPI = User::select('id','name','name_channel','cover','image','image_width','image_height','provider_id','is_feature_home','is_feature_page','img_header','img_content','feature_home','feature_page','corporate_creator','feature_sort_home','feature_sort_home','subscribers','views','yt_subs','followers_ig','followers_fb','followers_twitter','slug')->with('socials')
            ->whereHas('accessrole', function ($query) {
                $query->where('role_id', '2');
            })
            ->where('is_active', '=', 1)
            ->where('is_show', '=', 1)
            ->where('corporate_creator', '=', 'creator_corporate')
            ->groupBy('id')
            ->orderBy('views', 'desc')
            ->paginate(6);
        }
        if(empty($request->query('f')) || $request->query('page') > 1){
            return response()->json([
                'items' => $creatorsAPI->items(),
                'next_page_url' => $creatorsAPI->nextPageUrl()
            ])->header('Access-Control-Allow-Origin', '*');
        }else{
            return response()->json([
                'items' => $creatorsAPI->items(),
                'next_page_url' => $creatorsAPI->nextPageUrl().'&f='.$request->query('f')
            ])->header('Access-Control-Allow-Origin', '*');
        }
    }

    public function creatorsFeatureHomeAPI()
    {
        $creatorsAPI = User::select('id','name','name_channel','cover','image','image_width','image_height','provider_id','is_feature_home','is_feature_page','img_header','img_content','feature_home','feature_page','corporate_creator','feature_sort_home','feature_sort_home','subscribers','views','yt_subs','followers_ig','followers_fb','followers_twitter','slug')->with('socials')
        ->whereHas('accessrole', function ($query) {
            $query->where('role_id', '2');
        })
        ->where('is_active', '=', 1)
        ->where('is_show', '=', 1)
        ->where('is_feature_home', '=', 1)
        ->whereNotNull('cover')
        ->groupBy('slug')
        ->orderBy('feature_sort_home', 'asc')
        ->paginate(8);
        
        return response()->json([
            'items' => $creatorsAPI->items(),
            'next_page_url' => $creatorsAPI->nextPageUrl()
        ])->header('Access-Control-Allow-Origin', '*');
    }

    public function creatorsFeaturePageAPI()
    {
        $creatorsAPI = User::select('id','name','name_channel','cover','image','image_width','image_height','provider_id','is_feature_home','is_feature_page','img_header','img_content','feature_home','feature_page','corporate_creator','feature_sort_home','feature_sort_home','subscribers','views','yt_subs','followers_ig','followers_fb','followers_twitter','slug')->with('socials')
        ->whereHas('accessrole', function ($query) {
            $query->where('role_id', '2');
        })
        ->where('is_active', '=', 1)
        ->where('is_show', '=', 1)
        ->where('is_feature_page', '=', 1)
        ->groupBy('slug')
        ->orderBy('feature_sort_page', 'asc')
        ->paginate(6);
        
        return response()->json([
            'items' => $creatorsAPI->items(),
            'next_page_url' => $creatorsAPI->nextPageUrl()
        ])->header('Access-Control-Allow-Origin', '*');
    }

    public function creatorsNoFeaturePageAPI()
    {
        $creatorsNoAPI = User::select('id','name','name_channel','cover','image','image_width','image_height','provider_id','is_feature_home','is_feature_page','img_header','img_content','feature_home','feature_page','corporate_creator','feature_sort_home','feature_sort_home','subscribers','views','yt_subs','followers_ig','followers_fb','followers_twitter','slug')->with('socials')
        ->whereHas('accessrole', function ($query) {
            $query->where('role_id', '2');
        })
        ->where('is_active', '=', 1)
        ->where('is_show', '=', 1)
        ->where('is_feature_page', '=', 0)
        ->where('corporate_creator', '=', 'creator_personal')
        ->groupBy('slug')
        ->paginate(6);
        
        return response()->json([
            'items' => $creatorsAPI->items(),
            'next_page_url' => $creatorsAPI->nextPageUrl()
        ])->header('Access-Control-Allow-Origin', '*');
    }

    public function creatorsVideoListAPI($slug)
    {
        $creators = User::select('id','name','name_channel','cover','image','image_width','image_height','provider_id','is_feature_home','is_feature_page','img_header','img_content','feature_home','feature_page','corporate_creator','feature_sort_home','feature_sort_home','subscribers','views','yt_subs','followers_ig','followers_fb','followers_twitter','slug')->with(['series', 'totalVideosSeries', 'socials'])->where('slug', $slug)->first();

        $creatorsAPI = $creators->videos()->orderBy('created_at', 'desc')->paginate(6);
        
        return response()->json([
            'items' => $creatorsAPI->items(),
            'next_page_url' => $creatorsAPI->nextPageUrl()
        ])->header('Access-Control-Allow-Origin', '*');
    }

    public function creatorsAPI()
    {
        $creatorsAPI = User::select('id','name','name_channel','cover','image','image_width','image_height','provider_id','is_feature_home','is_feature_page','img_header','img_content','feature_home','feature_page','corporate_creator','feature_sort_home','feature_sort_home','subscribers','views','yt_subs','followers_ig','followers_fb','followers_twitter','slug')->with('socials')
        ->whereHas('accessrole', function ($query) {
            $query->where('role_id', '2');
        })
        ->where('is_active', '=', 1)
        ->where('is_show', '=', 1)
        ->whereNotNull('cover')
        ->where('corporate_creator', '=', 'creator_personal')
        ->groupBy('slug')
        ->paginate(8);
        
        return response()->json([
            'items' => $creatorsAPI->items(),
            'next_page_url' => $creatorsAPI->nextPageUrl()
        ])->header('Access-Control-Allow-Origin', '*');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function seriesAPI()
    {
        $seriesAPI = Series::select(DB::raw('id,user_id,type,title,subtitle,slug,fnStripTags(excerpt) as excerpt,fnStripTags(content) as content,image,seo_title,seo_keyword,seo_description,parent_id,is_active,is_featured,is_popular,token,created_by,created_at,updated_at,viewed,attr_1,attr_2,attr_3,attr_4,attr_5,attr_6,attr_7,post_type,sorting'))->orderBy('created_at', 'desc')->where('is_active', '=', '1')->paginate(8);
        
        return response()->json([
            'items' => $seriesAPI->items(),
            'next_page_url' => $seriesAPI->nextPageUrl()
        ])->header('Access-Control-Allow-Origin', '*');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function channelVideos()
    {
        $channelVideos = Video::with('users')->orderBy('created_at', 'desc')->where('is_active', '=', '1')->paginate(4);

        return response()->json([
            'items' => $channelVideos->items(),
            'next_page_url' => $channelVideos->nextPageUrl()
        ])->header('Access-Control-Allow-Origin', '*');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDataVideoChannel($slug)
    {
        $channels = Channel::select(DB::raw('id,user_id,type,title,REPLACE(subtitle,"Subtitle ","") as subtitle,slug,excerpt,content,image,seo_title,seo_keyword,seo_description,parent_id,is_active,is_featured,is_popular,token,created_by,created_at,updated_at,viewed,attr_1,attr_2,attr_3,attr_4,attr_5,attr_6,attr_7,post_type,sorting'))->with(['videos'])->where('slug', '=', $slug)->first();

        $getLatestMusic = Video::select(DB::raw('id,user_id,type,title,substr(replace(subtitle,"Subtitle",""), 1, 40) as subtitle,slug,excerpt,content,image,seo_title,seo_keyword,seo_description,parent_id,is_active,is_featured,is_popular,token,created_by,created_at,updated_at,viewed,attr_1,attr_2,attr_3,attr_4,attr_5,attr_6,attr_7,post_type,sorting'))->with(['channelVideo', 'users'])->where('is_active', '=', '1')->whereHas('channelVideo', function ($query) use ($slug) {
            $query->where('slug', $slug);
        })->orderBy('created_at', 'desc')->paginate(6);

        return response()->json([
            'items' => $getLatestMusic->items(),
            'next_page_url' => $getLatestMusic->nextPageUrl()
        ])->header('Access-Control-Allow-Origin', '*');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDataSeriesChannel($slug)
    {
        $channels = Channel::with(['videos'])->where('slug', '=', $slug)->first();

        $getLatestSeries = $channels->series()->orderBy('created_at', 'DESC')->paginate(4);

        return response()->json([
            'items' => $getLatestSeries->items(),
            'next_page_url' => $getLatestSeries->nextPageUrl()
        ])->header('Access-Control-Allow-Origin', '*');
    }

	public function getCountHomes(){

        $ChannelsAnalytic = DB::table('youtube_channel_analitic');
        $viewCount = $ChannelsAnalytic->sum('views');
        $EstimateWatch = $ChannelsAnalytic->sum('estimatedMinutesWatched');
        $subsCount = $ChannelsAnalytic->sum('subscribers');

        $data = array(
            'views'  => bd_nice_number($viewCount),
            'watch' => $EstimateWatch,
            'subscribe' => $subsCount,
        );

        return response()->json([
            'items' => $data,
        ])->header('Access-Control-Allow-Origin', '');
    }

}
