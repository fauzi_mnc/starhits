<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Controller;
use App\Model\Channel;
use App\Model\Campaign;
use App\Model\Posts;
use App\Model\Series;
use App\Model\User;
use App\Model\Video;
use App\Model\MasterLagu;
use App\Model\Revenue;
use App\Model\YoutubeAnalitic;
use App\Model\Dsp;
use App\Model\RoleLagu;
use App\Model\YoutubePartner;
use Auth;
use Youtube;
use  alchemyguy\YoutubeLaravelApi\AuthenticateService;
use  alchemyguy\YoutubeLaravelApi\ChannelService;

class DashboardController extends Controller
{
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function index()
    {

        $creators = User::where('is_active', 1)->whereHas('accessrole', function ($query) {
                        $query->where('role_id', '2');
                    })->get()->count();
        $not_creators = User::where('is_active', 1)->whereHas('accessrole', function ($query) {
                        $query->where('role_id', '!=', '2');
                    })->get()->count();
        $series  = Series::where('is_active', 1)->get()->count();
        $channels = Channel::where('is_active', 1)->get()->count();
        $videos   = Posts::where('type', 'video')->get()->count();
        $articles   = Posts::where('type', 'article')->get()->count();
        $influencer = User::where('is_active', 1)
        ->with('accessrole', 'detailInfluencer')
        ->whereHas('accessrole', function($query){
            $query->where('role_id', '6');
        })->with('detailInfluencer')->get()->count();
        $campaign = Campaign::get()->count();
        if (Auth::user()->roles->first()->name == 'singer') {
            $masterlagu = RoleLagu::where('role_lagu.id_user', Auth::user()->id)
                        ->groupBy('id_master_lagu')
                        ->get()->count();
        }elseif (Auth::user()->roles->first()->name == 'songwriter') {
            $masterlagu = RoleLagu::where('role_lagu.id_user', Auth::user()->id)
                        ->groupBy('id_master_lagu')
                        ->get()->count();
        }else{
            $masterlagu = RoleLagu::groupBy('id_master_lagu')->get()->count();
        }

        $users = User::whereHas('accessrole', function ($query) {
                    $query->whereIn('role_id', ['1','3','4,','7','8','11','12']);
                    })->get()->count();
        $brands = User::whereHas('accessrole', function ($query) {
                    $query->whereIn('role_id', ['5']);
                    })->get()->count();

        $singers = User::whereHas('accessrole', function ($query) {
                    $query->where('role_id', '9');
                    })->get()->count();
        
        $songwriters = User::whereHas('accessrole', function ($query) {
                    $query->where('role_id', '10');
                    })->get()->count();           

        $userseries  = Series::where([
                                            ['user_id', '=', Auth::user()->id],
                                            ['is_active', '=', 1]
                                        ])->get()->count();
        $uservideos   = Posts::where([
                                            ['user_id', '=', Auth::user()->id],
                                            ['is_active', '=', 1]
                                        ])->get()->count();
        $userarticles   = Posts::where([
                                            ['type', '=', 'article'],
                                            ['user_id', '=', Auth::user()->id],
                                            ['is_active', '=', 1]
                                        ])->get()->count();

        $revenues = Revenue::get()->count();
        //$ytanalytics = YoutubeAnalitic::get()->count();
        $dsps = Dsp::get()->count();

        $youtubePartner = YoutubePartner::get()->count();

        //dd($youtubePartner);

        $auth = Auth::user();
        $youtubePartner = YoutubePartner::where([['user_id', '=',$auth->id],['selected', '=', 'Y']])->get()->count();
        if($auth->roles[0]->name == 'user'){
            if($youtubePartner == 0){
                return redirect(route('authentication.network'));
            }
        }

        return view('dashboard.index', compact('creators', 'not_creators', 'series', 'channels', 'videos', 'articles', 'influencer', 'campaign', 'masterlagu', 'singers', 'songwriters', 'users', 'brands','userseries', 'uservideos', 'userarticles', 'revenues', 'dsps','youtubePartner'));
    }
}
