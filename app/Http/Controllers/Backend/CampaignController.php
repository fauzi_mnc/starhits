<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use App\DataTables\CampaignDataTable;
use App\DataTables\OInfluencerDataTable;
use App\DataTables\NInfluencerDataTable;
use App\Http\Requests\StoreOrUpdateCampaignRequest;
use App\Http\Requests\UpdateBudgetRequest;
use App\Http\Requests\UpdateApprovalRequest;
use App\Repositories\UserRepository;
use App\Repositories\RoleRepository;
use App\Model\Campaign;
use App\Model\Category;
use App\Model\User;
use App\Model\PostTo;
use App\Model\Nego;
use App\Model\CategoryInst;
use App\Model\Tags;
use App\Model\CampaignTags;
use App\Model\CampaignPost;
use App\Model\CampaignCategory;
use App\Model\Influencer;
use App\Model\DetailInfluencer;
use Flash;
use Auth;
use Log;
use Mail;

class CampaignController extends Controller
{
    /**
     * The user repository instance.
     */
    protected $users;
    protected $roles;

    /**
     * Create a new controller instance.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users, RoleRepository $roles)
    {
        $this->users = $users;
        $this->roles = $roles;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CampaignDataTable $campaignDataTable)
    {   
        return $campaignDataTable->render('campaign.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::where('is_active', 1)
        ->with('accessrole')
        ->whereHas('accessrole', function($query){
            $query->where('role_id', '5');
        })
        ->get()
        ->pluck('brand_name', 'id')
        ->toArray();
        $post = PostTo::all();
        $category = CategoryInst::all();
        $tags = Tags::all()->pluck('name')->toArray();
        $parent = Category::where([['parent', '=', NULL], ['is_active', '=', '1']])->pluck('title', 'id');
        $cat = Category::where([['is_active', '=', '1']])->pluck('title', 'id');
        return view('campaign.create', compact('parent', 'user', 'post', 'category', 'tags', 'cat'));
    }

    public function children(Request $request)
    {
        return Category::where([['parent', '=', $request->parent], ['is_active', '=', '1']])->pluck('title', 'id');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrUpdateCampaignRequest $request)
    {
        if (Auth::user()->roles->first()->name == 'admin') {
            $input['brand_id'] = $request->brand_id;
            $brnd = User::where('id', $request->brand_id)->first();
        }else{
            $input['brand_id'] = Auth::user()->id;
            $brnd = User::where('id', Auth::user()->id)->first();
        }
        $input['type'] = $request->type;
        $input['category_id'] = $request->category_id;
        $input['title'] = $request->title;
        $input['start_date'] = $request->start_date;
        $input['end_date'] = $request->end_date;
        $input['socio_economic_status'] = $request->socio_economic_status;
        $input['brief_campaign'] = $request->brief_campaign;
        $input['budget'] = $request->budget + $brnd->saldo;
        $input['status'] = 1;
        $input['active'] = 1;

        if ($request->has('notes')) {
            $input['notes'] = $request->notes;
        }

        if ($request->has('end_date')) {
            $date = new \DateTime($request->end_date);
            $date->modify('+23 hour');
            $date->modify('+59 minute');
            $date->modify('+59 second');
            $input['end_date'] = date_format($date, 'Y-m-d H:i:s');
        }

        if (Auth::user()->roles->first()->name == 'admin') {
            User::where('id', $request->brand_id)->update(['saldo' => NULL]);
        }else{
            User::where('id', Auth::user()->id)->update(['saldo' => NULL]);
        }

        $id_camp = Campaign::create($input)->camp_id;

        if ($request->post_to != NULL) {
            foreach ($request->post_to as $value) {
                $input_post['campaign_id'] = $id_camp;
                $input_post['post_id'] = $value;
                CampaignPost::create($input_post);
            }
        }

        if ($request->category != NULL) {
            foreach ($request->category as $value) {
                $input_cat['campaign_id'] = $id_camp;
                $input_cat['category_id'] = $value;
                CampaignCategory::create($input_cat);
            }
        }

        $tags = explode(',', $request->tags);
        foreach($tags as $value){
            $input_terms['name'] = $value;
            $input_terms['slug'] = rtrim(preg_replace('/[^A-Za-z0-9\-]/', '-', str_replace('-', '', strtolower($value))),'-');
            $input_terms['type'] = 'tag';

            $terms = Tags::where('name', '=', $value)->first();
            if ($terms === null) {
                $id_terms = Tags::create($input_terms)->id;
                $input_content_terms['content_id'] = $id_camp;
                $input_content_terms['term_id'] = $id_terms;
                CampaignTags::create($input_content_terms);
            } else {
                $input_content_terms['content_id'] = $id_camp;
                $input_content_terms['term_id'] = $terms->id;
                CampaignTags::create($input_content_terms);
            }
        }

        Flash::success('Campaign saved successfully.');
        if (Auth::user()->roles->first()->name == 'admin') {
            return redirect(route('admin.campaign.index'));
        }else{
            return redirect(route('brand.campaign.index'));
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $camp = Campaign::find($id);
        $sub = Category::where('is_active', 1)->where('id', $camp->category_id)->get()->pluck('title', 'id')->toArray();
        $child = Category::where('is_active', 1)->where('id', $camp->category_id)->first();
        $par = Category::where('is_active', 1)->where('id', $child->parent)->first();
        $cat = Category::where('is_active', 1)->where('parent', $par->id)->get()->pluck('title', 'id')->toArray();
        $selected_category = $par->id;
        $user = User::where('is_active', 1)
        ->with('roles')
        ->whereHas('roles', function($query){
            $query->where('name', 'brand');
        })
        ->get()
        ->pluck('brand_name', 'id')
        ->toArray();
        $post = PostTo::all();
        $post_selected = CampaignPost::where('campaign_id', $id)->get();
        $category_selected = CampaignCategory::where('campaign_id', $id)->get();
        $category = CategoryInst::all();
        $tags = Tags::all()->pluck('name')->toArray();
        $parent = Category::where([['parent', '=', NULL], ['is_active', '=', '1']])->pluck('title', 'id');
        $content_terms = CampaignTags::where('content_id', $id)->get();
        // dd($content_terms);
        $terms = array();
        foreach ($content_terms as $value){
            $term = Tags::where('id', $value->term_id)->first();
            array_push($terms, $term->name);
        }
        // dd($terms);
        $camp['tags'] = implode(',', $terms);

        return view('campaign.edit', compact('camp', 'tags', 'parent', 'user', 'post', 'category', 'terms', 'cat', 'selected_category', 'post_selected', 'category_selected'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function update($id, StoreOrUpdateCampaignRequest $request)
    {
        $input = Campaign::find($id);
        if (Auth::user()->roles->first()->name == 'admin') {
            $input['brand_id'] = $request->brand_id;
        }
        $input['type'] = $request->type;
        $input['category_id'] = $request->category_id;
        $input['title'] = $request->title;
        $input['start_date'] = $request->start_date;
        $input['end_date'] = $request->end_date;
        $input['socio_economic_status'] = $request->socio_economic_status;
        $input['brief_campaign'] = $request->brief_campaign;
        $input['budget'] = $request->budget;

        if ($request->has('notes')) {
            $input['notes'] = $request->notes;
        }

        if ($request->has('end_date')) {
            $date = new \DateTime($request->end_date);
            $date->modify('+23 hour');
            $date->modify('+59 minute');
            $date->modify('+59 second');
            $input['end_date'] = date_format($date, 'Y-m-d H:i:s');
        }

        $input->save();

        CampaignPost::where('campaign_id', $id)->delete();
        if ($request->post_to != NULL) {
            foreach ($request->post_to as $value) {
                $input_post['campaign_id'] = $id;
                $input_post['post_id'] = $value;
                CampaignPost::create($input_post);
            }
        }
        CampaignCategory::where('campaign_id', $id)->delete();
        if ($request->category != NULL) {
            foreach ($request->category as $value) {
                $input_cat['campaign_id'] = $id;
                $input_cat['category_id'] = $value;
                CampaignCategory::create($input_cat);
            }
        }

        $tags = explode(',', $request->tags);

        CampaignTags::where('content_id', $id)->delete();
        foreach($tags as $value){
            $input_terms['name'] = $value;
            $input_terms['slug'] = rtrim(preg_replace('/[^A-Za-z0-9\-]/', '-', str_replace('-', '', strtolower($value))),'-');
            $input_terms['type'] = 'tag';

            $terms = Tags::where('name', '=', $value)->first();
            if ($terms === null) {
                $id_terms = Tags::create($input_terms)->id;
                $input_content_terms['content_id'] = $id;
                $input_content_terms['term_id'] = $id_terms;
                CampaignTags::create($input_content_terms);
            } else {
                $input_content_terms['content_id'] = $id;
                $input_content_terms['term_id'] = $terms->id;
                CampaignTags::create($input_content_terms);
            }
        }

        Flash::success('Campaign updated successfully.');
        if (Auth::user()->roles->first()->name == 'admin') {
            return redirect(route('admin.campaign.index'));
        }else{
            return redirect(route('brand.campaign.index'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $camp = Campaign::find($id);
        $user = User::where('is_active', 1)
        ->with('accessrole')
        ->whereHas('accessrole', function($query){
            $query->where('role_id', '5');
        })
        ->get()
        ->pluck('brand_name', 'id')
        ->toArray();
        $infl = Influencer::where([['user_id', Auth::user()->id], ['campaign_id', $id]])->first();
        // dd($infl);

        return view('campaign.show', compact('camp', 'user', 'infl'));
    }

    public function budget($id, NInfluencerDataTable $influencerDataTable){
        $camp = Campaign::find($id);
        $fil = Influencer::where('campaign_id', $id)->get()->pluck('user_id')->toArray();
        $user = User::where('is_active', 1)
                ->with('accessrole')
                ->whereHas('accessrole', function($query){
                    $query->where('role_id', '6');
                })
                ->with('influencer')
                ->whereHas('influencer', function($q) use ($fil){
                    $q->whereIn('user_id', $fil)->where('approval', 1)->where('campaign_id', request()->segment(3));
                })
                ->get()
                ->pluck('name', 'id')
                ->toArray();
        $post_selected = CampaignPost::where('campaign_id', $id)
                        ->get()
                        ->pluck('post_id')
                        ->toArray();
        $post = PostTo::whereIn('id', $post_selected)
                    ->pluck('title', 'id')
                    ->toArray();
        return $influencerDataTable->render('campaign.budget', compact('post', 'user', 'camp'));
    }

    public function upbudget(UpdateBudgetRequest $request){
        // dd($request->all());
        $camp = $request->id;
        $infl = $request->influencer;
        $post = $request->post_to;
        $check_infl = Influencer::where([['campaign_id', $camp],['user_id', $infl]])->first();
        $check_nego = Nego::where([['camp_id', $camp],['infl_id', $infl],['post_id', $post]])->first();

        if($check_infl === null){
            $in['campaign_id'] = $camp;
            $in['user_id'] = $infl;
            $in['approval'] = 1;
            Influencer::create($in);
        }else{
            $in['approval'] = 1;
            Influencer::where([['campaign_id', $camp],['user_id', $infl]])->update($in);
        }

        if ($check_nego === null) {
            $ng['camp_id'] = $camp;
            $ng['infl_id'] = $infl;
            $ng['post_id'] = $post;
            $ng['post_budget'] = $request->post_budget;
            Nego::create($ng);
        }else{
            $ng['post_budget'] = $request->post_budget;
            Nego::where([['camp_id', $camp],['infl_id', $infl],['post_id', $post]])->update($ng);
        }

        $user = User::where('id', $infl)->first();
        $data = array('name'=>"Update budget");

        Mail::send('campaign.mail', $data, function($message) use ($user) {
            $message->to($user->email, 'Penawaran')->subject('Penawaran kerjasama');
            $message->from('starhits.official@gmail.com','Starhits');
        });
        Flash::success('Budget updated successfully.');
        return redirect(route('admin.campaign.index'));
    }

    public function add($id, OInfluencerDataTable $influencerDataTable)
    {
        $camp = Campaign::find($id);
        $sub = Category::where('is_active', 1)->where('id', $camp->category_id)->get()->pluck('title', 'id')->toArray();
        $child = Category::where('is_active', 1)->where('id', $camp->category_id)->first();
        $par = Category::where('is_active', 1)->where('id', $child->parent)->first();
        $cat = Category::where('is_active', 1)->where('parent', $par->id)->get()->pluck('title', 'id')->toArray();
        $selected_category = $par->id;
        $user = User::where('is_active', 1)
        ->with('roles')
        ->whereHas('roles', function($query){
            $query->where('name', 'brand');
        })
        ->get()
        ->pluck('brand_name', 'id')
        ->toArray();
        $tes = ['', 'instagram_video_posting_rate', 'instagram_story_posting_rate', 'instagram_photo_posting_rate', 'youtube_posting_rate', 'ig_highlight_rate', 'package_rate'];
        $cam = CampaignPost::where('campaign_id', request()->segment(3))
        ->get()
        ->pluck('post_id')
        ->toArray();
        $filter = [];
        foreach ($cam as $val) {
            $filter[] = $tes[$val];
        }
        $infl = User::where('is_active', 1)
        ->with('influencer')->whereHas('influencer', function($query) use ($id){
            $query->where('campaign_id', $id);
        })->with('detailInfluencer')->get();
        $post = PostTo::all();
        $post_selected = CampaignPost::where('campaign_id', $id)->get();
        $category_selected = CampaignCategory::where('campaign_id', $id)->get();
        $category = CategoryInst::all();
        $tags = Tags::all()->pluck('name')->toArray();
        $parent = Category::where([['parent', '=', NULL], ['is_active', '=', '1']])->pluck('title', 'id');
        $content_terms = CampaignTags::where('content_id', $id)->get();
        $terms = array();
        foreach ($content_terms as $value){
            $term = Tags::where('id', $value->term_id)->first();
            array_push($terms, $term->name);
        }
        $camp['tags'] = implode(',', $terms);

        return $influencerDataTable->render('campaign.add', compact('camp', 'tags', 'parent', 'user', 'post', 'category', 'terms', 'cat', 'selected_category', 'post_selected', 'category_selected', 'infl', 'filter', 'id', 'cam'));
    }

    public function approval(UpdateApprovalRequest $request)
    {
        $input['approval'] = $request->approval;
        if ($request->approval == 0) {
            $input['reason'] = $request->reason;
            Influencer::where([['user_id', Auth::user()->id], ['campaign_id', $request->id]])->update($input);

            Flash::success('Campaign rejected successfully.');
        }else{
            Influencer::where([['user_id', Auth::user()->id], ['campaign_id', $request->id]])->update($input);

            Flash::success('Campaign approved successfully.');
        }

        return redirect(route('influencer.campaign.index'));
    }

    public function infl(Request $request)
    {
        if (!empty($request->check)) {
            $campaign = Campaign::find($request->id);
            $campaign['status'] = 2;
            $campaign->save();
            foreach ($request->check as $infl => $value) {
                $infl = Influencer::where([['user_id', $value], ['campaign_id', $request->id]])->first();
                if ($infl->approval == 4) {
                    Influencer::where([['user_id', $value], ['campaign_id', $request->id]])->update(['approval' => NULL]);
                    $user = User::where('id', $value)->first();
                    $data = array('name'=>"Surat Penawaran");

                    Mail::send('campaign.mail', $data, function($message) use ($user) {
                        $message->to($user->email, 'Penawaran')->subject('Penawaran kerjasama');
                        $message->from('starhits.official@gmail.com','Starhits');
                    });
                }
            }
            Flash::success('Influencer added successfully.');
        }
        if (Auth::user()->roles->first()->name == 'admin') {
            return redirect(route('admin.campaign.index'));
        }else{
            return redirect(route('brand.campaign.index'));
        }
    }

    public function addtotable($id, $camp){
        $count = 0;
        $campaign = Campaign::where('camp_id', $camp)->with('campaign_post')->first();
        $influ = Influencer::where('campaign_id', $camp)->get();
        foreach ($influ as $in) {
            $det = DetailInfluencer::where('user_id', $in->user_id)->first();
            foreach ($campaign->campaign_post as $post) {
                if($post->post_id == 1){
                    if (Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->exists()) {
                        $nego = Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->first();
                        $count += $nego->post_budget;
                    }else{
                        $count += $det->instagram_video_posting_rate;
                    }
                }elseif ($post->post_id == 2) {
                    if (Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->exists()) {
                        $nego = Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->first();
                        $count += $nego->post_budget;
                    }else{
                        $count += $det->instagram_story_posting_rate;
                    }
                }elseif ($post->post_id == 3) {
                    if (Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->exists()) {
                        $nego = Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->first();
                        $count += $nego->post_budget;
                    }else{
                        $count += $det->instagram_photo_posting_rate;
                    }
                }elseif ($post->post_id == 4) {
                    if (Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->exists()) {
                        $nego = Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->first();
                        $count += $nego->post_budget;
                    }else{
                        $count += $det->youtube_posting_rate;
                    }
                }elseif ($post->post_id == 5) {
                    if (Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->exists()) {
                        $nego = Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->first();
                        $count += $nego->post_budget;
                    }else{
                        $count += $det->ig_highlight_rate;
                    }
                }elseif ($post->post_id == 6) {
                    if (Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->exists()) {
                        $nego = Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->first();
                        $count += $nego->post_budget;
                    }else{
                        $count += $det->package_rate;
                    }
                }
            }
        }
        $infl = User::where('id', $id)
        ->with('detailInfluencer')->first();
        foreach ($campaign->campaign_post as $post) {
                if($post->post_id == 1){
                    $count += $infl->detailInfluencer->instagram_video_posting_rate;
                }elseif ($post->post_id == 2) {
                    $count += $infl->detailInfluencer->instagram_story_posting_rate;
                }elseif ($post->post_id == 3) {
                    $count += $infl->detailInfluencer->instagram_photo_posting_rate;
                }elseif ($post->post_id == 4) {
                    $count += $infl->detailInfluencer->youtube_posting_rate;
                }elseif ($post->post_id == 5) {
                    $count += $infl->detailInfluencer->ig_highlight_rate;
                }elseif ($post->post_id == 6) {
                    $count += $infl->detailInfluencer->package_rate;
                }
            }
        $infl = Influencer::where([['user_id', $id], ['campaign_id', $camp]])->first();
        if (empty($infl)) {
            if ($campaign->budget >= $count) {
                $input['user_id'] = $id;
                $input['campaign_id'] = $camp;
                $input['approval'] = 4;

                Influencer::create($input);
                Flash::success('Influencer added');
            }else{
                Flash::error('Insufficient campaigns budget. Please edit budget first.');
            }
        }

        if (Auth::user()->roles->first()->name == 'admin') {
            return redirect(route('admin.campaign.add', $camp));
        }else{
            return redirect(route('brand.campaign.add', $camp));
        }
    }

    public function rft($id, $camp){
        Influencer::where([['user_id', $id], ['campaign_id', $camp]])->delete();
        Flash::success('Influencer removed');
        
        if (Auth::user()->roles->first()->name == 'admin') {
            return redirect(route('admin.campaign.add', $camp));
        }else{
            return redirect(route('brand.campaign.add', $camp));
        }
    }

    public function rif($id, $camp){
        Influencer::where([['user_id', $id], ['campaign_id', $camp]])->delete();
        $user = User::where('id', $id)->first();
        $data = array('name'=>"Surat Pembatalan");

        Mail::send('campaign.rmail', $data, function($message) use ($user) {
            $message->to($user->email, 'Pembatalan')->subject('Pembatalan kerjasama');
            $message->from('starhits.official@gmail.com','Starhits');
        });
        Flash::success('Influencer removed');
        
        if (Auth::user()->roles->first()->name == 'admin') {
            return redirect(route('admin.campaign.add', $camp));
        }else{
            return redirect(route('brand.campaign.add', $camp));
        }
    }

    public function showCampaign($id, OInfluencerDataTable $influencerDataTable){
        $camp = Campaign::find($id);
        $sub = Category::where('is_active', 1)->where('id', $camp->category_id)->get()->pluck('title', 'id')->toArray();
        $child = Category::where('is_active', 1)->where('id', $camp->category_id)->first();
        $par = Category::where('is_active', 1)->where('id', $child->parent)->first();
        $cat = Category::where('is_active', 1)->where('parent', $par->id)->get()->pluck('title', 'id')->toArray();
        $selected_category = $par->id;
        $user = User::where('is_active', 1)
        ->with('roles')
        ->whereHas('roles', function($query){
            $query->where('name', 'brand');
        })
        ->get()
        ->pluck('brand_name', 'id')
        ->toArray();
        $tes = ['', 'instagram_video_posting_rate', 'instagram_story_posting_rate', 'instagram_photo_posting_rate', 'youtube_posting_rate', 'ig_highlight_rate', 'package_rate'];
        $cam = CampaignPost::where('campaign_id', request()->segment(3))
        ->get()
        ->pluck('post_id')
        ->toArray();
        $filter = [];
        foreach ($cam as $val) {
            $filter[] = $tes[$val];
        }
        $infl = User::where('is_active', 1)
        ->with('influencer')->whereHas('influencer', function($query) use ($id){
            $query->where('campaign_id', $id);
        })->with('detailInfluencer')->get();
        $post = PostTo::all();
        $post_selected = CampaignPost::where('campaign_id', $id)->get();
        $category_selected = CampaignCategory::where('campaign_id', $id)->get();
        $category = CategoryInst::all();
        $tags = Tags::all()->pluck('name')->toArray();
        $parent = Category::where([['parent', '=', NULL], ['is_active', '=', '1']])->pluck('title', 'id');
        $content_terms = CampaignTags::where('content_id', $id)->get();
        $terms = array();
        foreach ($content_terms as $value){
            $term = Tags::where('id', $value->term_id)->first();
            array_push($terms, $term->name);
        }
        $camp['tags'] = implode(',', $terms);

        return $influencerDataTable->render('campaign.finish', compact('camp', 'tags', 'parent', 'user', 'post', 'category', 'terms', 'cat', 'selected_category', 'post_selected', 'category_selected', 'infl', 'filter', 'id'));
    }

    public function closeCampaign(Request $request){
        if($request->action == 'finish'){
            $total = 0;
            $count = 0;
            $campaign = Campaign::where('camp_id', $request->id)->with('campaign_post')->first();
            $user = User::where('id', $campaign->brand_id)->first();
            $influ = Influencer::where([['campaign_id', $request->id], ['approval', 1]])->get();
            foreach ($influ as $in) {
                $det = DetailInfluencer::where('user_id', $in->user_id)->first();
                $if = User::where('id', $in->user_id)->first();
                foreach ($campaign->campaign_post as $post) {
                    if($post->post_id == 1){
                        if (Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->exists()) {
                            $nego = Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->first();
                            $count += $nego->post_budget;
                        }else{
                            $count += $det->instagram_video_posting_rate;
                        }
                    }elseif ($post->post_id == 2) {
                        if (Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->exists()) {
                            $nego = Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->first();
                            $count += $nego->post_budget;
                        }else{
                            $count += $det->instagram_story_posting_rate;
                        }
                    }elseif ($post->post_id == 3) {
                        if (Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->exists()) {
                            $nego = Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->first();
                            $count += $nego->post_budget;
                        }else{
                            $count += $det->instagram_photo_posting_rate;
                        }
                    }elseif ($post->post_id == 4) {
                        if (Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->exists()) {
                            $nego = Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->first();
                            $count += $nego->post_budget;
                        }else{
                            $count += $det->youtube_posting_rate;
                        }
                    }elseif ($post->post_id == 5) {
                        if (Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->exists()) {
                            $nego = Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->first();
                            $count += $nego->post_budget;
                        }else{
                            $count += $det->ig_highlight_rate;
                        }
                    }elseif ($post->post_id == 6) {
                        if (Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->exists()) {
                            $nego = Nego::where([['camp_id', $campaign->camp_id],['infl_id', $in->user_id],['post_id', $post->post_id]])->first();
                            $count += $nego->post_budget;
                        }else{
                            $count += $det->package_rate;
                        }
                    }
                }

                $data = array('name'=>"Surat Pemberitahuan");
                Mail::send('campaign.cmail', $data, function($message) use ($if) {
                    $message->to($if->email, 'Campaign')->subject('Campaign dijalankan');
                    $message->from('starhits.official@gmail.com','Starhits');
                });
            }

            $data = array('name'=>"Surat Pemberitahuan");
            Mail::send('campaign.cmail', $data, function($message) use ($user) {
                $message->to($user->email, 'Campaign')->subject('Campaign dijalankan');
                $message->from('starhits.official@gmail.com','Starhits');
            });
            $total = intval($user->saldo) + (intval($campaign->budget))-(intval($count));
            $camp = Campaign::find($request->id);
            $camp['status'] = 3;
            $camp->save();
            User::where('id', $campaign->brand_id)->update(['saldo' => $total]);
            Flash::success('Campaign finished successfully.');
            return redirect(route('admin.campaign.index'));
        }elseif($request->action == 'email'){
            foreach ($request->mail as $email => $value) {
                $user = User::where('id', $value)->first();
                $data = array('name'=>"Surat Penawaran");

                Mail::send('campaign.mail', $data, function($message) use ($user) {
                    $message->to($user->email, 'Penawaran')->subject('Penawaran kerjasama');
                    $message->from('starhits.official@gmail.com','Starhits');
                });
            }
            Flash::success('Email sent.');
            return redirect(route('admin.campaign.index'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->action == 'act') {
            $series = Campaign::find($id);

            if (empty($series)) {
                Flash::error('Campaign not found');

                if (Auth::user()->roles->first()->name == 'admin') {
                    return redirect(route('admin.campaign.index'));
                }else{
                    return redirect(route('brand.campaign.index'));
                }
            }
            $series['active'] = 1;

            $series->save();

            Flash::success('Campaign activated successfully.');
        }else{
            $series = Campaign::find($id);

            if (empty($series)) {
                Flash::error('Campaign not found');

                if (Auth::user()->roles->first()->name == 'admin') {
                    return redirect(route('admin.campaign.index'));
                }else{
                    return redirect(route('brand.campaign.index'));
                }
            }
            $series['active'] = 0;
            $series->save();

            Flash::success('Campaign inactivated successfully.');
        }
        
        if (Auth::user()->roles->first()->name == 'admin') {
            return redirect(route('admin.campaign.index'));
        }else{
            return redirect(route('brand.campaign.index'));
        }
    }

    public function sendemail()
    {
        $data = array('name'=>"Surat Penawaran");

        Mail::send('campaign.mail', $data, function($message) {
            $message->to('fauzi.mncgroup@gmail.com', 'Fauzi')->subject('Surat Cinta');
            $message->from('it.starhits@gmail.com','Starhits');
        });
        echo "Basic Email Sent. Check your inbox.";
    }

    public function getInfluencer()
    {   
        $post = ['', 'instagram_video_posting_rate', 'instagram_story_posting_rate', 'instagram_photo_posting_rate', 'youtube_posting_rate', 'ig_highlight_rate', 'package_rate'];
        $infl = User::where('is_active', 1)
        ->with('roles', 'detailInfluencer')
        ->whereHas('roles', function($query){
            $query->where('name', 'influencer');
        })->with('detailInfluencer');

        $influencers = $infl->get();

        $categories = CategoryInst::all();

        
        return view('campaign.browse_influencers', compact('influencers', 'categories'));
    }

    public function getDataInfluencer(Request $request)
    {
        $post = ['', 'instagram_video_posting_rate', 'instagram_story_posting_rate', 'instagram_photo_posting_rate', 'youtube_posting_rate', 'ig_highlight_rate', 'package_rate'];
        $cat = $request->cat_id;
        $infl = User::where('is_active', 1)
        ->with('roles', 'detailInfluencer')
        ->whereHas('roles', function($query){
            $query->where('name', 'influencer');
        })->with('detailInfluencer')
        ->whereHas('detailInfluencer', function($query) use ($request, $cat){
            if (!empty($request->username)) {
                $query->where('username', 'like', '%'.$request->username.'%');
            }

            if($request->max_followers != 0)
            {
                $query->whereBetween('followers_ig', array($request->min_followers, $request->max_followers));
            }

            if(!empty($request->cat_id)){
                $query->where(function($query) use ($cat){
                    $query->where('category', 'like', '%'.$cat[0].'%');
                    if (sizeof($cat) > 1) {
                        for ($i=1; $i < sizeof($cat); $i++) { 
                            $query->orWhere('category', 'like', '%'.$cat[$i].'%');
                        }
                    }
                });
            }
        });

        if (!empty($request->gender)) {
            $infl->where('gender', $request->gender);
        }

        $influencers = $infl->get();

        return response()->json([
            'result' => $influencers
        ]);
    }

    public function addToTableInfluencers(Request $request){
        $count = 0;
        $data = new \stdClass();
        $campaign = Campaign::where('camp_id', $request->id_campaign)->with('campaign_post')->first();
        $influ = Influencer::where('campaign_id', $request->id_campaign)->get();
        foreach ($influ as $in) {
            $det = DetailInfluencer::where('user_id', $in->user_id)->first();
            foreach ($campaign->campaign_post as $post) {
                if($post->post_id == 1){
                    $count += $det->instagram_video_posting_rate;
                }elseif ($post->post_id == 2) {
                    $count += $det->instagram_story_posting_rate;
                }elseif ($post->post_id == 3) {
                    $count += $det->instagram_photo_posting_rate;
                }elseif ($post->post_id == 4) {
                    $count += $det->youtube_posting_rate;
                }elseif ($post->post_id == 5) {
                    $count += $det->ig_highlight_rate;
                }elseif ($post->post_id == 6) {
                    $count += $det->package_rate;
                }
            }
        }
        foreach($request->id_influencers as $value){
            $infl = User::where('id', $value)
            ->with('detailInfluencer')->first();
            foreach ($campaign->campaign_post as $post) {
                    if($post->post_id == 1){
                        $count += $infl->detailInfluencer->instagram_video_posting_rate;
                    }elseif ($post->post_id == 2) {
                        $count += $infl->detailInfluencer->instagram_story_posting_rate;
                    }elseif ($post->post_id == 3) {
                        $count += $infl->detailInfluencer->instagram_photo_posting_rate;
                    }elseif ($post->post_id == 4) {
                        $count += $infl->detailInfluencer->youtube_posting_rate;
                    }elseif ($post->post_id == 5) {
                        $count += $infl->detailInfluencer->ig_highlight_rate;
                    }elseif ($post->post_id == 6) {
                        $count += $infl->detailInfluencer->package_rate;
                    }
                }
            $infl = Influencer::where([['user_id', $value], ['campaign_id', $request->id_campaign]])->first();

            if (empty($infl)) {
                if ($campaign->budget >= $count) {
                    $input['user_id'] = $value;
                    $input['campaign_id'] = $request->id_campaign;
                    $input['approval'] = 4;

                    Influencer::create($input);
                    $data->message = 'Influencer added';
                    $data->status = 'success';
                }else{

                    $data->message = 'Insufficient campaigns budget. Please edit budget first.';
                    $data->status = 'error';
                    return response()->json([
                        'result' => $data
                    ]);
                }
            }
        }

        $data->message = 'Influencer added';
        $data->status = 'success';
        return response()->json([
            'result' => $data
        ]);
    }
}
