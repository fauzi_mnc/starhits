<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use App\Repositories\UserRepository;
use Flash;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Storage;
use Log;
use App\Model\User;
use App\Model\UserGroup;
use App\Model\UserToken;
use App\Model\YoutubeAnalitic;
use App\Model\YoutubeAnaliticLink;
use App\Model\YoutubeVideo;
use App\Model\YoutubePartner;
use App\Model\YoutubeChannel;
use App\Model\YoutubeAnaliticVideo;
use Excel;
use Carbon;
use Auth;
use DB;
use PDF;
use Session;
use Youtube;
use Socialite;
use Google_Client;
use Google_Service_Drive;
use Google_Service_Sheets_Spreadsheet;
use Google_Service_Drive_DriveFile;
use Google_Service_Sheets;
use Google_Service_Sheets_ValueRange;
use Google_Service_YouTube;
//use Google_Service_YouTubePartner;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Backend\TwitterController;
use App\Http\Controllers\Auth\CallbackController;
use Abraham\TwitterOAuth\TwitterOAuth;

class YoutubeReportingController extends Controller
{

	protected $Callback;


    public function __construct(CallbackController $Callback)
    {
        $this->Callback = $Callback;
    }

	public function GoogleClient(){
		$Secret = DB::table('yt_content_secret')
					// ->where('user_id', auth()->user()->id)
					->where('type', 'youtube')
					->first();

		$client = new \Google_Client();
        $client->setClientId($Secret->client_id);
        $client->setClientSecret($Secret->client_secret);       
        $client->setScopes([
        	'https://www.googleapis.com/auth/youtube',
        	'https://www.googleapis.com/auth/youtube.readonly',
        	'https://www.googleapis.com/auth/yt-analytics-monetary.readonly',
        	'https://www.googleapis.com/auth/youtubepartner',
        	'https://www.googleapis.com/auth/yt-analytics.readonly'
        ]);
        $client->addScope(\Google_Service_YouTubeAnalytics::YT_ANALYTICS_READONLY);
        $client->setRedirectUri($Secret->client_redirect);
        $client->setAccessType('offline');
        $client->setPrompt('consent select_account');
		$client->setApprovalPrompt('force');
		return $client;	
	}

	public function revokeAccess(){
		$client = $this->GoogleClient();
		$client->revokeToken();

		dd($client);
	}

	public function getYTAnalytic(){ //TO GET ALL REPORTING ALL DAY CHANNEL
		$client = $this->GoogleClient();
		$token = DB::table('user_token')
				->where(['user_id'=> auth()->user()->id, 'type'=> 'youtube','action_dream'=>'yt-analytics'])
				->first();
        $token = collect($token)->except(['id', 'user_id', 'type'])->toArray();
        $dateNow = date("Y-m-d");
        session(['action_dream'=>"yt-analytics"]);

        //$Channels = YoutubeChannel::where(['channel_id'=>'UCoSkllfpgmFHtbVK835QaQg'])->first();

		$Channels = YoutubeChannel::all();
		// $Channels = YoutubeChannel::where(['content_id'=>'-0xjZRSiTg0o1Y8TD-jNBg'])->get();
		$views_count =0;

		if (!empty($token)) {
			if($client->isAccessTokenExpired()){ //Jika Akses tokennya Expired
	            $client->refreshToken($token['refresh_token']);
	            $accessToken = $client->getAccessToken();
	            $client->setAccessToken($accessToken);
	            $upd = DB::table('user_token')
	            	->where('user_id', auth()->user()->id)
	            	->where('type', 'youtube')
	            	->update([
		                'access_token'  => $accessToken['access_token'],
		                'refresh_token' => $accessToken['refresh_token'],
		                'token_type'    => $accessToken['token_type'],
		                'expires_in'    => $accessToken['expires_in'],
		                'created'       => $accessToken['created']
		            ]);
			}
			
			$youtube = new \Google_Service_YouTubeAnalytics($client);
			
			foreach ($Channels as $Channels) {
				$queryParams = [
				    'endDate' => $dateNow,
				    'filters' => 'channel=='.$Channels->channel_id,
				    'ids' => 'contentOwner=='.$Channels->content_id,
				    'dimensions'=>'day',
				    'sort'=>'day',
				    /*'metrics' => 'views,estimatedMinutesWatched,averageViewDuration,averageViewPercentage,subscribersGained',*/
				    'metrics' =>'views,likes,dislikes,comments,shares,estimatedRevenue,cpm,cardClickRate,cardClicks,cardImpressions,grossRevenue,subscribersLost,subscribersGained,estimatedMinutesWatched,monetizedPlaybacks',
				    'startDate' => $Channels->publishat
				];

				$response = $youtube->reports->query($queryParams);
				//dd(json_encode($response->rows));
				for ($i=0; $i < count($response->rows); $i++) { 
					$views_count = $views_count + $response->rows[$i][1];
					/*DB::table('youtube_cms_analitic')->insert([
						'user_id'=>Auth::user()->id,
						'content_id'=>$Channels->content_id,
						'channel_id'=>$Channels->channel_id,
						'date'=>$response->rows[$i][0],
						'views'=>$response->rows[$i][1],
						'likes'=>$response->rows[$i][2],
						'dislikes'=>$response->rows[$i][3],
						'comments'=>$response->rows[$i][4],
						'shares'=>$response->rows[$i][5],
						'estimatedRevenue'=>$response->rows[$i][6],
						'cpm'=>$response->rows[$i][7],
						'cardClickRate'=>$response->rows[$i][8],
						'cardClicks'=>$response->rows[$i][9],
						'cardImpressions'=>$response->rows[$i][10],
						'grossRevenue'=>$response->rows[$i][11],
						'subscribersLost'=>$response->rows[$i][12],
						'subscribersGained'=>$response->rows[$i][13],
						'estimatedMinutesWatched'=>$response->rows[$i][14],
						'monetizedPlaybacks'=>$response->rows[$i][15]
					]);*/
				}
			}

			return bd_nice_number($views_count);

		} else {
			$authUrl = $client->createAuthUrl();
			return redirect($authUrl);
		}
		
	}

	public function getChannelAnalytic(){
		$client = $this->GoogleClient();
		$token = DB::table('user_token')
				// ->where('user_id', auth()->user()->id)
				->where('type', 'youtube')
				->where('type', 'youtube')
				->first();

		$Secret = DB::table('yt_content_secret')
					// ->where('user_id', auth()->user()->id)
					->where('type', 'youtube')
					->first();

        $token = collect($token)->except(['id', 'user_id', 'type'])->toArray();
        $dateNow = date("Y-m-d");

       // $Channels = YoutubeChannel::where(['channel_id'=>'UCoSkllfpgmFHtbVK835QaQg'])->first();
       // $channel_id ="UCoSkllfpgmFHtbVK835QaQg";
        $apikey = $Secret->key;
        $this->getStaticChannel();

		if (!empty($token)) {
			if($client->isAccessTokenExpired()){ //Jika Akses tokennya Expired
	            $client->refreshToken($token['refresh_token']);
	            $accessToken = $client->getAccessToken();
	            $client->setAccessToken($accessToken);

	            DB::table('user_token')
                        // ->where('user_id', auth()->user()->id)
                        ->where('type', 'youtube')
                        ->where('action_dream','yt-analytics')
                        ->update([
                            'access_token'  => $accessToken['access_token'],
                            'refresh_token' => $accessToken['refresh_token'],
                            'token_type'    => $accessToken['token_type'],
                            'expires_in'    => $accessToken['expires_in'],
                            'created'       => $accessToken['created'],
                        ]);
			}
			
			$youtube = new \Google_Service_YouTubeAnalytics($client);
			
			$Channels = YoutubeChannel::all();
			foreach ($Channels as $Channels) {

				$queryParams = [
				    'endDate' => $dateNow,
				    'filters' => 'channel=='.$Channels->channel_id,
				    'ids' => 'contentOwner=='.$Channels->content_id,
				    'metrics' =>'views,likes,dislikes,comments,shares,estimatedRevenue,cpm,cardClickRate,cardClicks,cardImpressions,grossRevenue,subscribersLost,subscribersGained,estimatedMinutesWatched,monetizedPlaybacks',
				    'startDate' => $Channels->publishat
				];

				$response = $youtube->reports->query($queryParams);
				$isEmpty = DB::table('youtube_channel_analitic')
						->where(['channel_id'=>$Channels->channel_id])->first();

				if(empty($isEmpty)){
					DB::table('youtube_channel_analitic')->insert([
						// 'user_id'=>Auth::user()->id,
						'content_id'=>$Channels->content_id,
						'channel_id'=>$Channels->channel_id,
						'date'=>$dateNow,
						'views'=>isset($response->rows[0][0]) ? $response->rows[0][0]:0,
						'likes'=>isset($response->rows[0][1]) ? $response->rows[0][1]:0,
						'dislikes'=>isset($response->rows[0][2]) ? $response->rows[0][2]:0,
						'comments'=>isset($response->rows[0][3]) ? $response->rows[0][3]:0,
						'shares'=>isset($response->rows[0][4]) ? $response->rows[0][4]:0,
						'estimatedRevenue'=>isset($response->rows[0][5]) ? $response->rows[0][5]:0,
						'cpm'=>isset($response->rows[0][6]) ? $response->rows[0][6]:0,
						'cardClickRate'=>isset($response->rows[0][7]) ? $response->rows[0][7]:0,
						'cardClicks'=>isset($response->rows[0][8]) ? $response->rows[0][8]:0,
						'cardImpressions'=>isset($response->rows[0][9]) ? $response->rows[0][9]:0,
						'grossRevenue'=>isset($response->rows[0][10]) ? $response->rows[0][10]:0,
						'subscribersLost'=>isset($response->rows[0][11]) ? $response->rows[0][11]:0,
						'subscribersGained'=>isset($response->rows[0][12]) ? $response->rows[0][12]:0,
						'estimatedMinutesWatched'=>isset($response->rows[0][13]) ? $response->rows[0][13]:0,
						'monetizedPlaybacks'=>isset($response->rows[0][14]) ? $response->rows[0][14]:0,
					]);
					$this->getStaticChannel();
				}else{
					DB::table('youtube_channel_analitic')
					->where(['channel_id'=>$Channels->channel_id])
					->update([
						// 'user_id'=>Auth::user()->id,
						'content_id'=>$Channels->content_id,
						'channel_id'=>$Channels->channel_id,
						'date'=>$dateNow,
						'views'=>isset($response->rows[0][0]) ? $response->rows[0][0]:0,
						'likes'=>isset($response->rows[0][1]) ? $response->rows[0][1]:0,
						'dislikes'=>isset($response->rows[0][2]) ? $response->rows[0][2]:0,
						'comments'=>isset($response->rows[0][3]) ? $response->rows[0][3]:0,
						'shares'=>isset($response->rows[0][4]) ? $response->rows[0][4]:0,
						'estimatedRevenue'=>isset($response->rows[0][5]) ? $response->rows[0][5]:0,
						'cpm'=>isset($response->rows[0][6]) ? $response->rows[0][6]:0,
						'cardClickRate'=>isset($response->rows[0][7]) ? $response->rows[0][7]:0,
						'cardClicks'=>isset($response->rows[0][8]) ? $response->rows[0][8]:0,
						'cardImpressions'=>isset($response->rows[0][9]) ? $response->rows[0][9]:0,
						'grossRevenue'=>isset($response->rows[0][10]) ? $response->rows[0][10]:0,
						'subscribersLost'=>isset($response->rows[0][11]) ? $response->rows[0][11]:0,
						'subscribersGained'=>isset($response->rows[0][12]) ? $response->rows[0][12]:0,
						'estimatedMinutesWatched'=>isset($response->rows[0][13]) ? $response->rows[0][13]:0,
						'monetizedPlaybacks'=>isset($response->rows[0][14]) ? $response->rows[0][14]:0,
					]);
					$this->getStaticChannel();
				}
			}
		} else {
			$authUrl = $client->createAuthUrl();
			return redirect($authUrl);
		}
	}

	public function getStaticSubcriber($channel_id,$accessToken,$apikey){
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://www.googleapis.com/youtube/v3/channels?part=statistics&id=".$channel_id."&key=".$apikey."HTTP%2F1.1",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "accept: application/json",
		    "authorization: Bearer ".$accessToken,
		    "cache-control: no-cache",
		  ),
		));

		$response = curl_exec($curl);
		return $response;
	}


	public function getStaticChannel(){

		$client = $this->GoogleClient();

		$token = DB::table('user_token')
				// ->where('user_id', auth()->user()->id)
				->where('type', 'youtube')
				->where('type', 'youtube')
				->first();

		$Secret = DB::table('yt_content_secret')
					// ->where('user_id', auth()->user()->id)
					->where('type', 'youtube')
					->first();

        $token = collect($token)->except(['id', 'user_id', 'type'])->toArray();
        $dateNow = date("Y-m-d");

        if($client->isAccessTokenExpired()){ //Jika Akses tokennya Expired
            $client->refreshToken($token['refresh_token']);
            $accessToken = $client->getAccessToken();
            $client->setAccessToken($accessToken);

            DB::table('user_token')
                // ->where('user_id', auth()->user()->id)
                ->where('type', 'youtube')
                ->update([
                    'access_token'  => $accessToken['access_token'],
                    'refresh_token' => $accessToken['refresh_token'],
                    'token_type'    => $accessToken['token_type'],
                    'expires_in'    => $accessToken['expires_in'],
                    'created'       => $accessToken['created'],
                ]);
		}

		$service = new Google_Service_YouTube($client);
		$ytPartner = YoutubePartner::where(['selected'=>'Y'])->get();

		foreach ($ytPartner as $key => $value) {

			$queryParams = [
			    'managedByMe' => true,
			    'onBehalfOfContentOwner' => $value->content_id,
			    'maxResults' => 50,
			];
			$response = $service->channels->listChannels('snippet,statistics', $queryParams);
			$countRespone = count($response->items);

			for ($i=0; $i < $countRespone; $i++) {
				
				$idChannel 		 = $response->items[$i]->id;
				$subscriberCount = $response->items[$i]->statistics->subscriberCount ;
				$viewCount 		 = $response->items[$i]->statistics->viewCount;
				$videoCount 	 = $response->items[$i]->statistics->videoCount;

				DB::table('youtube_channel_analitic')
					->where(['channel_id'=>$idChannel])
					->update([
						'views'		 =>	$viewCount,
						'video_count'=> $videoCount,
						'subscribers'=> $subscriberCount,
					]);
			}
		}
	}

	
	public function getAPIAnaliticContentOwner(){
		$client = $this->Callback->GClient_Ytreporting();
		//$callbackUrl = 'http://starhits.com/auth/youtube/callback';

		/*$token = DB::table('user_token')
				->where('user_id', auth()->user()->id)
				->where('type', 'youtube')
				->first();*/

        $Secret = DB::table('yt_content_secret')
					// ->where('user_id', auth()->user()->id)
					->where('type', 'youtube')
					->first();
		
		$dateNow = date("Y-m-d");
		$token = DB::table('user_token')
				// ->where('user_id', auth()->user()->id)
				->where(['type'=> 'youtube'])
				->first();

        $token = collect($token)->except(['id', 'user_id', 'type'])->toArray();
		if($client->isAccessTokenExpired()){ 
           	$client->refreshToken($token['refresh_token']);
            $accessToken = $client->getAccessToken();
            $client->setAccessToken($accessToken);
            $upd = DB::table('user_token')
                    // ->where('user_id', auth()->user()->id)
                    ->where(['type'=> 'youtube'])
                    ->update([
                        'access_token'  => $accessToken['access_token'],
                        'refresh_token' => isset($accessToken['refresh_token']) ? $accessToken['refresh_token']:0,
                        'token_type'    => $accessToken['token_type'],
                        'expires_in'    => $accessToken['expires_in'],
                        'created'       => $accessToken['created'],
                    ]);
		}
	
		if (!empty($token)) {
			$Channels = YoutubePartner::all();
			//$Channels = YoutubePartner::where(['content_id'=>'BagRA3M6NZYMNezMOJp9gg'])->get();
			// return($Channels);
			foreach ($Channels as $value) {
				$this->CRUDAnaliticContentOwner($value,'claimed','self');
				$this->CRUDAnaliticContentOwner($value,'claimed','thirdParty');
			}
		} else {
			$authUrl = $client->createAuthUrl();
			return redirect($authUrl);
		}
	}

	public function CRUDAnaliticContentOwner($value,$claimedStatus,$Type){

		$dateNow = date("Y-m-d");
		$claim ="";
		$uploaderType ="";
		$Claimed = $claimedStatus;
		$Secret = DB::table('yt_content_secret')
                    // ->where('user_id', auth()->user()->id)
                    ->where('type', 'youtube')
                    ->first();
		$isUpdate = DB::table('youtube_partner_analitic')
				->where([
					'content_id'	=> $value->content_id,
					'uploaderType' 	=> $Type,
					'claimedStatus' => $Claimed,
					'updated_at'	=> $dateNow
				])->first();

		if(empty($isUpdate)){

			$isEmptys = DB::table('youtube_partner_analitic')
					->where([
						'content_id'	=> $value->content_id,
						'uploaderType' 	=> $Type,
						'claimedStatus' => $Claimed,
					])->first();

			if(!empty($claimedStatus)){
				$claim = 'claimedStatus=='.$claimedStatus.';';
			}
			if(!empty($Type)){
				$uploaderType = 'uploaderType=='.$Type;
			}

			/*
				$token = DB::table('user_token')
						->where('user_id', auth()->user()->id)
						->where('type', 'youtube')
						->where('type', 'youtube')
						->first();

		        $token = collect($token)->except(['id', 'user_id', 'type'])->toArray();*/

			
				
				/*$youtube = new \Google_Service_YouTubeAnalytics($client);
				$queryParams = [
					    'endDate' => $dateNow,
					    'filters' => $claim.''.$uploaderType,
					    'ids' => 'contentOwner=='.$value->content_id,
					    'metrics' =>'views,likes,dislikes,comments,shares,estimatedRevenue,cpm,cardClickRate,cardClicks,cardImpressions,grossRevenue,subscribersLost,subscribersGained,estimatedMinutesWatched,monetizedPlaybacks',
					    'startDate' => '2000-01-01'
					];
				$response = $youtube->reports->query($queryParams);
			*/
			$usertoken = UserToken::where([
				// 'user_id'		=>  auth()->user()->id,
				'type'			=> 'youtube'
			])->get();

			$ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://youtubeanalytics.googleapis.com/v2/reports?endDate=2019-10-24&filters='.$claim.''.$uploaderType.'&ids=contentOwner=='.$value->content_id.'&metrics=views,likes,dislikes,comments,shares,estimatedRevenue,cpm,cardClickRate,cardClicks,cardImpressions,grossRevenue,subscribersLost,subscribersGained,estimatedMinutesWatched,monetizedPlaybacks&startDate=2000-01-01&key='.$Secret->key);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

            $headers = array();
            $headers[] = 'Authorization: '.$usertoken[0]->token_type.' '.$usertoken[0]->access_token;
            $headers[] = 'Accept: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);
            $response = json_decode($result);
            dd($response->rows[0]);

			if(empty($isEmptys)){
				DB::table('youtube_partner_analitic')->insert([
					// 'user_id'=>auth()->user()->id,
					'content_id'=>$value->content_id,
					'created_at'=>$dateNow,
					'updated_at'=>$dateNow,
					'views'=>isset($response->rows[0][0]) ? $response->rows[0][0]:0,
					'likes'=>isset($response->rows[0][1]) ? $response->rows[0][1]:0,
					'dislikes'=>isset($response->rows[0][2]) ? $response->rows[0][2]:0,
					'comments'=>isset($response->rows[0][3]) ? $response->rows[0][3]:0,
					'shares'=>isset($response->rows[0][4]) ? $response->rows[0][4]:0,
					'estimatedRevenue'=>isset($response->rows[0][5]) ? $response->rows[0][5]:0,
					'cpm'=>isset($response->rows[0][6]) ? $response->rows[0][6]:0,
					'cardClickRate'=>isset($response->rows[0][7]) ? $response->rows[0][7]:0,
					'cardClicks'=>isset($response->rows[0][8]) ? $response->rows[0][8]:0,
					'cardImpressions'=>isset($response->rows[0][9]) ? $response->rows[0][9]:0,
					'grossRevenue'=>isset($response->rows[0][10]) ? $response->rows[0][10]:0,
					'subscribersLost'=>isset($response->rows[0][11]) ? $response->rows[0][11]:0,
					'subscribersGained'=>isset($response->rows[0][12]) ? $response->rows[0][12]:0,
					'estimatedMinutesWatched'=>isset($response->rows[0][13]) ? $response->rows[0][13]:0,
					'monetizedPlaybacks'=>isset($response->rows[0][14]) ? $response->rows[0][14]:0,
					'claimedStatus'=>$claimedStatus,
					'uploaderType'=>$Type
				]);
			}else{
				DB::table('youtube_partner_analitic')
				->where([
					'content_id'	=> $value->content_id,
					'uploaderType' 	=> $Type,
					'claimedStatus' => $Claimed,
				])->update([
					'user_id'=>auth()->user()->id,
					'content_id'=>$value->content_id,
					'updated_at'=>$dateNow,
					'views'=>isset($response->rows[0][0]) ? $response->rows[0][0]:0,
					'likes'=>isset($response->rows[0][1]) ? $response->rows[0][1]:0,
					'dislikes'=>isset($response->rows[0][2]) ? $response->rows[0][2]:0,
					'comments'=>isset($response->rows[0][3]) ? $response->rows[0][3]:0,
					'shares'=>isset($response->rows[0][4]) ? $response->rows[0][4]:0,
					'estimatedRevenue'=>isset($response->rows[0][5]) ? $response->rows[0][5]:0,
					'cpm'=>isset($response->rows[0][6]) ? $response->rows[0][6]:0,
					'cardClickRate'=>isset($response->rows[0][7]) ? $response->rows[0][7]:0,
					'cardClicks'=>isset($response->rows[0][8]) ? $response->rows[0][8]:0,
					'cardImpressions'=>isset($response->rows[0][9]) ? $response->rows[0][9]:0,
					'grossRevenue'=>isset($response->rows[0][10]) ? $response->rows[0][10]:0,
					'subscribersLost'=>isset($response->rows[0][11]) ? $response->rows[0][11]:0,
					'subscribersGained'=>isset($response->rows[0][12]) ? $response->rows[0][12]:0,
					'estimatedMinutesWatched'=>isset($response->rows[0][13]) ? $response->rows[0][13]:0,
					'monetizedPlaybacks'=>isset($response->rows[0][14]) ? $response->rows[0][14]:0,
					'claimedStatus'=>$claimedStatus,
					'uploaderType'=>$Type,
				]);
			}
		}
	}

	public function testAPIContentOwner(){
		//session(['action_dream'=>"yt-analytics"]);
		$client = $this->Callback->GClient_Ytreporting();
		

		$Secret = DB::table('yt_content_secret')
					->where('user_id', auth()->user()->id)
					->where('type', 'youtube')
					->first();
		
		$dateNow = date("Y-m-d");
		$token = DB::table('user_token')
				->where('user_id', auth()->user()->id)
				->where(['type'=> 'youtube'])
				->first();

        $token = collect($token)->except(['id', 'user_id', 'type'])->toArray();
		if($client->isAccessTokenExpired()){ 
           	$client->refreshToken($token['refresh_token']);
            $accessToken = $client->getAccessToken();
            $client->setAccessToken($accessToken);
            $upd = DB::table('user_token')
                    ->where('user_id', auth()->user()->id)
                    ->where('type', 'youtube')
                    ->update([
                        'access_token'  => $accessToken['access_token'],
                        'refresh_token' => isset($accessToken['refresh_token']) ? $accessToken['refresh_token']:0,
                        'token_type'    => $accessToken['token_type'],
                        'expires_in'    => $accessToken['expires_in'],
                        'created'       => $accessToken['created'],
                    ]);
		}

		$usertoken = UserToken::where([
			'user_id'		=>  auth()->user()->id,
			'type'			=> 'youtube'
		])->get();

		$ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://youtubeanalytics.googleapis.com/v2/reports?endDate=2019-10-24&filters=claimedStatus==claimed;uploaderType==thirdParty&ids=contentOwner==zOXcv2QPVZoNkW5hYMk49g&metrics=views,likes,dislikes,comments,shares,estimatedRevenue,cpm,cardClickRate,cardClicks,cardImpressions,grossRevenue,subscribersLost,subscribersGained,estimatedMinutesWatched,monetizedPlaybacks&startDate=2000-01-01&key=AIzaSyA-KUB0wyU0w0dIJCCkmF6ShS73-syL9zA');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

            $headers = array();
            $headers[] = 'Authorization: '.$usertoken[0]->token_type.' '.$usertoken[0]->access_token;
            $headers[] = 'Accept: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);
            $contentID = json_decode($result);
            dd($contentID);

		/*else{
			$accessToken = $client->getAccessToken();
            $client->setAccessToken($accessToken);
            //dd($client);
		}*/
		

		
		//$Channels = YoutubePartner::where(['content_id'=>'JhE7qylSPVhFQoRECOVRNQ'])->get();
		//foreach ($Channels as $value) {

		/*$youtube = new \Google_Service_YouTubeAnalytics($client);

			$queryParams = [
				    'endDate' => $dateNow,
				    'filters' => 'claimedStatus==claimed;uploaderType==self',
				    'ids' => 'contentOwner==zOXcv2QPVZoNkW5hYMk49g',
				    'metrics' =>'views,likes,dislikes,comments,shares,estimatedRevenue,cpm,cardClickRate,cardClicks,cardImpressions,grossRevenue,subscribersLost,subscribersGained,estimatedMinutesWatched,monetizedPlaybacks',
				    'startDate' => '2000-01-01'
				];

			$response = $youtube->reports->query($queryParams);
			dd($response);*/
			// Define service object for making API requests.
			/*$service = new \Google_Service_YouTubeAnalytics($client);

			$queryParams = [
			    'endDate' => '2019-10-24',
			    'filters' => 'claimedStatus==claimed;uploaderType==thirdParty',
			    'ids' => 'contentOwner==zOXcv2QPVZoNkW5hYMk49g',
			    'metrics' => 'views,likes,dislikes,comments,shares,estimatedRevenue,cpm,cardClickRate,cardClicks,cardImpressions,grossRevenue,subscribersLost,subscribersGained,estimatedMinutesWatched',
			    'startDate' => '2000-01-01'
			];

			$response = $service->reports->query($queryParams);
			dd($response);*/
		//}
	}


	public function getFollowersIG(){
		$otherPage = 'duren_kunciran';
		$urlToGet = "https://www.instagram.com/$otherPage/?__a=1";
		$headers = get_headers($urlToGet);
     	$responseCode = substr($headers[0], 9, 3);

     	if($responseCode ==200){
     		$response = file_get_contents($urlToGet);

			if ($response != false) {
			    $data = json_decode($response, true);
			    if ($data !== null) {

			        $follows = $data['graphql']['user']['edge_follow']['count'];
			        $followedBy = $data['graphql']['user']['edge_followed_by']['count'];
			        //return $follows . ' and ' . $followedBy;
			        return $data;
			    }
			}
     	}
	}

	public function getFollowersFB(){
		//The following code returns the Number of likes for any facebook page.

		//Page Id of TechRecite.com. Replace it with your page.
		$page_id = "GlenSIdol9";
		$likes = 0; //Initialize the count

		//Construct a Facebook URL
		$json_url ='https://graph.facebook.com/'.$page_id.'';
		$json = file_get_contents($json_url);
		$json_output = json_decode($json);

		//Extract the likes count from the JSON object
		if($json_output->likes){
		    $likes = $json_output->likes;
		}
		//Printing the count
		return ''.$likes.'';
	}

}