<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\DataTables\InfluencerDataTable;
use Flash;
use App\Http\Requests\StoreOrUpdateCreatorRequest;
use App\Http\Requests\UpdateCreatorRequest;
use Storage;

class BankController extends Controller
{
    public function index()
    {
        return response()->json([
            'result' => DB::table('bank')->get()
        ]);
    }
}
