<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use App\Repositories\MasterLaguRepository;
use App\Repositories\UserRepository;
use App\DataTables\DspDataTable;
use App\DataTables\MasterLaguDataTabel;
use App\DataTables\MasterLaguDetailDataTabel;
use Flash;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Storage;
use Log;
use App\Model\User;
use App\Model\Country;
use App\Model\RoleUser;
use App\Model\AccessRole;
use App\Model\MasterLagu;
use App\Model\RoleLagu;
use App\Model\RoleLaguCountry;
use App\Http\Requests\StoreOrUpdateMasterLaguRequest;
use Excel;
use Carbon;
use Auth;
use DB;
use PDF;

class MasterLaguController extends Controller
{
    protected $masterlagu;
    protected $users;


    public function __construct(MasterLaguRepository $masterlagu, UserRepository $users)
    {
        $this->masterlagu = $masterlagu;
    }
    public function index(){
        // $m = MasterLagu::with('rolelagu','rolelagu.penyanyi','rolelagu.pencipta','rolelagucountry.country')
        //         //->where('penyanyi.id_user', Auth::user()->id)
        //         // ->whereHas('rolelagu', function($q){
        //         //     $q->where('role_lagu.id_user', Auth::user()->id);
        //         // })
        //         ->where('id', 122)
        //         ->orderBy('master_lagu.id', 'DESC')
        //         ->get();
        //         return $m;
        //         dd($m);
        //dd(bcrypt('bastian2019'));
        //     $x = 0;
        //     $y = 0;
        //     while($x <= count($m)) {
        //         while($y <= count($m[$x])) {
        //             $singer = $m[$x]->rolelagu[$y]->penyanyi->name;
        //         }
        //         $x++;
        //     }
        //     return $singer;
            
        // $i = 0;
        // for ($query as $v) {
        //     return array($v[$query]->rolelagu[$query]->penyanyi->name);
        //     $i++;
        // }
        
        if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('legal') || Auth::user()->hasRole('anr')) {
            $masterlagu = MasterLagu::with('rolelagu','rolelagu.penyanyi','rolelagu.pencipta','rolelagucountry.country')
            ->orderBy('master_lagu.updated_at', 'DESC')->get();
        }else{
            $masterlagu = MasterLagu::with('rolelagu','rolelagu.penyanyi','rolelagu.pencipta','rolelagucountry.country')
            ->whereHas('rolelagu', function($q){
                $q->where('role_lagu.id_user', Auth::user()->id);
            })
            ->orderBy('master_lagu.updated_at', 'DESC')->get();
        }
        //return $masterlagu;

        $filsinger = User::join('access_role', 'users.id', '=', 'access_role.user_id')
                        ->where(['access_role.role_id'=>'9'])
                        ->orderBy('users.name_master', 'ASC')
                        ->groupBy('users.name_master')
                        ->get();
        $filsingers[''] = 'Filter By Singer';
        foreach($filsinger AS $fs){
            $filsingers[$fs->name_master] = $fs->name_master;
        }

        //return $filsinger;

        $release_title = MasterLagu::groupBy('release_title')->get();
        $release_titles[''] = 'Filter By Album';
        foreach($release_title AS $rs){
            $release_titles[$rs->release_title] = $rs->release_title;
        }

        if(!empty($request->album)){
            $title = MasterLagu::where('release_title', 'LIKE', '%'.$request->album.'%')->get();
        }else{
            $title = MasterLagu::get();
        }
        $titles[''] = 'Filter By Judul';
        foreach($title AS $s){
            $titles[$s->track_title] = $s->track_title;
        }
        
        $songwriter = User::join('access_role', 'users.id', '=', 'access_role.user_id')
                        ->where(['access_role.role_id'=>'10'])
                        ->orderBy('users.name_master', 'ASC')
                        ->groupBy('users.name_master')
                        ->get();
        $songwriters[''] = 'Filter By Songwriter / Publisher';
        foreach($songwriter AS $s){
            $songwriters[$s->name_master] = $s->name_master;
        }
        //return $MLaguDatatable->render('masterlagu.index', ['singers' => $singers, 'release_titles' => $release_titles, 'titles' => $titles, 'songwriters' => $songwriters]);
        return view('masterlagu.index', [
            'masterlagu'  => $masterlagu,
            'filsinger'  => $filsinger,
            'filsingers'  => $filsingers,
            'release_titles'  => $release_titles,
            'titles'  => $titles,
            'songwriters'  => $songwriters
        ]);
    }

    public function create(){
        $singerx = User::join('access_role', 'users.id', '=', 'access_role.user_id')
                        ->where(['access_role.role_id'=>'9'])
                        ->orderBy('users.name_master', 'ASC')
                        ->groupBy('users.name_master')
                        ->get();
        //dd($singers);
        $songwriterx = User::join('access_role', 'users.id', '=', 'access_role.user_id')
                        ->where(['access_role.role_id'=>'10'])
                        ->orderBy('users.name_master', 'ASC')
                        ->groupBy('users.name_master')
                        ->get();
        $countryx = Country::orderBy('sorting', 'asc')->orderBy('id', 'asc')->get();
        return view('masterlagu.create',compact('singerx','songwriterx','countryx'));
    }

    public function store(Request $request){
        ini_set('memory_limit', '-1');
        $data = new \stdClass();
        $array_dsp = array();
        if($request->file != NULL){
            $request->file->storeAs('uploads/master', $request->file->getClientOriginalName(), 'local_public');
            $file_path = public_path().'/uploads/master/'.$request->file->getClientOriginalName();
            
            if($request->file->getClientOriginalExtension() == 'xlsx'){
                $reader = ReaderFactory::create(Type::XLSX); // for XLSX files

                $reader->open($file_path);
                
                foreach ($reader->getSheetIterator() as $sheet) {
                    foreach ($sheet->getRowIterator() as $key => $row) {
                        if($key>1){
                            $strpenyanyi       = $row[0];
                            $strpencipta       = $row[4];
                            if (!empty($row[5])){
                                $checkMaster    = MasterLagu::where(['isrc'=>$row[5]])->first();
                                if(empty($checkMaster)) {
                                    $record = new MasterLagu();
                                    $record->track_title            = $row[1];
                                    $record->release_title          = $row[2];
                                    $record->release_date           = $row[3];
                                    $record->label_name             = 'Hits Records';
                                    $record->isrc                   = strtolower($row[5]);
                                    $record->upc                    = $row[6];
                                    if(!empty($row[7])){
                                        $record->percentage_pencipta    = $row[7];
                                    }elseif(!empty($row[8])){
                                        $record->percentage_pencipta    = $row[8];
                                    }
                                    $record->percentage_penyanyi    = $row[9];
                                    $record->status                 = '1';
                                    // $record->no_contract            = $row[10];
                                    $record->save();

                                    $penyanyi = str_replace('.', '', preg_split("/ (&|ft.|ft|x|X|,) /",$strpenyanyi));
                                    $pencipta = str_replace('.', '', preg_split("/ (&|ft.|ft|x|X|,) /",$strpencipta));
                                    //dd($penyanyi);
                                    foreach($penyanyi as $stringer) {
                                        $users_penyanyi = User::where('name', 'like', '%'.$stringer.'%')->whereOr('name_master', 'like', '%'.$stringer.'%')->first();
                                        //dd($users_penyanyi);
                                        if ($stringer != '') {
                                            if (empty($users_penyanyi)) {
                                                $in_user_penyanyi               = new User();
                                                $in_user_penyanyi->name         = ucwords(strtolower($stringer));
                                                $in_user_penyanyi->name_master  = ucwords(strtolower($stringer));
                                                $in_user_penyanyi->email        = str_replace(" ", ".", strtolower($stringer))."@starhits.id";
                                                $in_user_penyanyi->slug         = str_replace(" ", "-", strtolower($stringer));
                                                $in_user_penyanyi->password     = bcrypt('123456789012');
                                                $in_user_penyanyi->is_active    = 1;
                                                $in_user_penyanyi->save();

                                                $access_roles_penyanyi = AccessRole::where([['user_id', '=', $in_user_penyanyi->id],['role_id', '=', 9]])->first();
                                                if (empty($access_roles_penyanyi)) {
                                                    $rolePenyanyi = new AccessRole();
                                                    $rolePenyanyi->user_id = $in_user_penyanyi->id;
                                                    $rolePenyanyi->role_id = 9;
                                                    $rolePenyanyi->save();
                                                }

                                                $role_lagu_penyanyi = RoleLagu::where([['id_master_lagu', '=', $record->id],['id_user', '=', $in_user_penyanyi->id]])->first();
                                                if (empty($role_lagu_penyanyi)) {
                                                    $roleLagu = new RoleLagu();
                                                    $roleLagu->id_master_lagu = $record->id;
                                                    $roleLagu->id_user = $in_user_penyanyi->id;
                                                    $roleLagu->save();
                                                }                                            
                                            } else {
                                                
                                                User::where([
                                                    'id'=> $users_penyanyi->id
                                                ])
                                                ->update([
                                                    'name_master'  =>   ucwords(strtolower($stringer)),
                                                ]);

                                                $access_roles_penyanyi = AccessRole::where([['user_id', '=', $users_penyanyi->id],['role_id', '=', 9]])->first();
                                                if (empty($access_roles_penyanyi)) {
                                                    $rolePenyanyi = new AccessRole();
                                                    $rolePenyanyi->user_id = $users_penyanyi->id;
                                                    $rolePenyanyi->role_id = 9;
                                                    $rolePenyanyi->save();
                                                }

                                                $role_lagu_penyanyi = RoleLagu::where([['id_master_lagu', '=', $record->id],['id_user', '=', $users_penyanyi->id]])->first();
                                                if (empty($role_lagu_penyanyi)) {
                                                    $roleLagu = new RoleLagu();
                                                    $roleLagu->id_master_lagu = $record->id;
                                                    $roleLagu->id_user = $users_penyanyi->id;
                                                    $roleLagu->save();
                                                }
                                            }
                                        }
                                    }
                                    if (!empty($row[4])){
                                        foreach($pencipta as $strongwriter) {
                                            if (!empty($strongwriter)) {
                                                $users_pencipta = User::where('name', 'like', '%'.$strongwriter.'%')->whereOr('name_master', 'like', '%'.$strongwriter.'%')->first();
                                                if (empty($users_pencipta)) {
                                                    $in_user_pencipta               = new User();
                                                    $in_user_pencipta->name         = ucwords(strtolower($strongwriter));
                                                    $in_user_pencipta->name_master  = ucwords(strtolower($strongwriter));
                                                    $in_user_pencipta->email        = str_replace(" ", ".", strtolower($strongwriter))."@starhits.id";
                                                    $in_user_pencipta->slug         = str_replace(" ", "-", strtolower($strongwriter));
                                                    $in_user_pencipta->password     = bcrypt('123456789012');
                                                    $in_user_pencipta->is_active    = 1;
                                                    $in_user_pencipta->save();

                                                    $access_roles_pencipta = AccessRole::where([['user_id', '=', $in_user_pencipta->id],['role_id', '=', 10]])->first();
                                                    if (empty($access_roles_pencipta)) {
                                                        $rolePencipta = new AccessRole();
                                                        $rolePencipta->user_id  = $in_user_pencipta->id;
                                                        $rolePencipta->role_id  = 10;
                                                        $rolePencipta->save();
                                                    }
                                                    $role_lagu_pencipta = RoleLagu::where([['id_master_lagu', '=', $record->id],['id_user', '=', $in_user_pencipta->id]])->first();
                                                    if (empty($role_lagu_pencipta)) {
                                                        $roleLagu = new RoleLagu();
                                                        $roleLagu->id_master_lagu = $record->id;
                                                        $roleLagu->id_user = $in_user_pencipta->id;
                                                        $roleLagu->save();
                                                    }
                                                } else {

                                                    User::where([
                                                        'id'=> $users_pencipta->id
                                                    ])
                                                    ->update([
                                                        'name_master'  =>   ucwords(strtolower($strongwriter)),
                                                    ]);

                                                    $access_roles_pencipta = AccessRole::where([['user_id', '=', $users_pencipta->id],['role_id', '=', 10]])->first();
                                                    if (empty($access_roles_pencipta)) {
                                                        $rolePencipta = new AccessRole();
                                                        $rolePencipta->user_id  = $users_pencipta->id;
                                                        $rolePencipta->role_id  = 10;
                                                        $rolePencipta->save();
                                                    }
                                                    $role_lagu_pencipta = RoleLagu::where([['id_master_lagu', '=', $record->id],['id_user', '=', $users_pencipta->id]])->first();
                                                    if (empty($role_lagu_pencipta)) {
                                                        $roleLagu = new RoleLagu();
                                                        $roleLagu->id_master_lagu = $record->id;
                                                        $roleLagu->id_user = $users_pencipta->id;
                                                        $roleLagu->save();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $reader->close();
            unlink($file_path);
        }else{
            //INPUT MANUAL MASTER LAGU
            $role_id            =   $request->role_id;
            $singer             =   $request->singer;
            $transing           =   $request->transing;
            $songwriter         =   $request->songwriter;
            $transong           =   $request->transong;
            $track_title        =   $request->track_title;
            if(!empty($request->release_title)){
                $release_title      =   $request->release_title;
            }else{
                $release_title      =   "Self-Titled";
            }
            if(!empty($request->release_date)){
                $release_date       =   date('Y-m-d', strtotime($request->release_date));
            }else{
                $release_date       =   "0000:00:00";
            }
            $label_name         =   $request->label_name;
            $isrc               =   strtolower($request->isrc);
            $upc                =   $request->upc;
            $no_contract        =   $request->no_contract;
            $submitbutton       =   $request->submitbutton;
            if($submitbutton == 'Save'){
                $status = 1;
            }else{
                $status = 2;
            }

            //dd($transing);
            if(!empty($singer)){
                if(count($singer)>10){
                    Flash::error('Maximum singer 10.');
                    if (Auth::user()->hasRole('admin')) {
                        return redirect(route('masterlagu.index'));
                    }elseif (Auth::user()->hasRole('legal')) {
                        return redirect(route('legal.masterlagu.index'));
                    }elseif (Auth::user()->hasRole('anr')) {
                        return redirect(route('anr.masterlagu.index'));
                    }
                }
            }

            if(!empty($singer)){
                for ($i=0; $i < count($singer) ; $i++) { 
                    if(empty($singer[$i])){
                        Flash::error('Nama Penyanyi tidak boleh kosong.');
                        if (Auth::user()->hasRole('admin')) {
                            return redirect(route('masterlagu.index'));
                        }elseif (Auth::user()->hasRole('legal')) {
                            return redirect(route('legal.masterlagu.index'));
                        }elseif (Auth::user()->hasRole('anr')) {
                            return redirect(route('anr.masterlagu.index'));
                        }
                    }
                }
            }

            if(!empty($songwriter)){
                if(count($songwriter)>10){
                    Flash::error('Maximum songwritter 10.');
                    if (Auth::user()->hasRole('admin')) {
                        return redirect(route('masterlagu.index'));
                    }elseif (Auth::user()->hasRole('legal')) {
                        return redirect(route('legal.masterlagu.index'));
                    }elseif (Auth::user()->hasRole('anr')) {
                        return redirect(route('anr.masterlagu.index'));
                    }
                }
            }

            if ($request->percentpencipta == '') {
                $percentpencipta    =   0;
            }else{
                $percentpencipta    =   str_replace('%', '', $request->percentpencipta) / 100;
            }
            if ($request->percentpenyanyi == '') {
                $percentpenyanyi    =   0;
            }else{
                $percentpenyanyi    =   str_replace('%', '', $request->percentpenyanyi) / 100;
            }
            if ($request->percentrights == '') {
                $percentrights    =   0;
            }else{
                $percentrights    =   str_replace('%', '', $request->percentrights) / 100;
            }
            $country             =   $request->country;

            $penyanyi           = $singer;
            $pencipta           = $songwriter;
            $negara             = $country;
            if(empty($isrc)){
                $record = new MasterLagu();
                $record->track_title            = $track_title;
                $record->release_title          = $release_title;
                $record->release_date           = $release_date;
                $record->upc                    = $upc;
                $record->status                 = $status;
                $record->no_contract            = $no_contract;
                $record->percentage_pencipta    = $percentpencipta;
                $record->percentage_penyanyi    = $percentpenyanyi ;
                $record->percentage_rights      = $percentrights ;
                $record->save();
                if(!empty($singer)){
                    for ($x=0; $x < count($singer) ; $x++) {
                        RoleLagu::insert([
                            'id_master_lagu' => $record->id,
                            'id_user'        => $singer[$x]
                        ]);
                    }
                }
                if(!empty($songwriter)){
                    for ($i=0; $i < count($songwriter) ; $i++) { 
                        if(empty($songwriter[$i])){
                            Flash::error('Nama Pencipta / Publisher kosong..');
                            if (Auth::user()->hasRole('admin')) {
                                return redirect(route('masterlagu.index'));
                            }elseif (Auth::user()->hasRole('legal')) {
                                return redirect(route('legal.masterlagu.index'));
                            }elseif (Auth::user()->hasRole('anr')) {
                                return redirect(route('anr.masterlagu.index'));
                            }
                        }else{
                            RoleLagu::insert([
                                'id_master_lagu' => $record->id,
                                'id_user'        => $songwriter[$i]
                            ]);
                        }
                    }
                }
                if(!empty($country)){
                    for ($c=0; $c < count($country) ; $c++) { 
                        if(empty($country[$c])){
                            Flash::error('Data Country kosong..');
                            if (Auth::user()->hasRole('admin')) {
                                return redirect(route('masterlagu.index'));
                            }elseif (Auth::user()->hasRole('legal')) {
                                return redirect(route('legal.masterlagu.index'));
                            }elseif (Auth::user()->hasRole('anr')) {
                                return redirect(route('anr.masterlagu.index'));
                            }
                        }else{
                            RoleLaguCountry::insert([
                                'id_master_lagu' => $record->id,
                                'id_country' => $country[$c]
                            ]);
                        }
                    }
                }
                Flash::success('Master Lagu saved successfully.');
                if (Auth::user()->hasRole('admin')) {
                    return redirect(route('masterlagu.index'));
                }elseif (Auth::user()->hasRole('legal')) {
                    return redirect(route('legal.masterlagu.index'));
                }elseif (Auth::user()->hasRole('anr')) {
                    return redirect(route('anr.masterlagu.index'));
                }
            }else{
                $checkMaster        = MasterLagu::where(['isrc'=>$isrc])->first();
                if(empty($checkMaster)) {
                    
                    $record = new MasterLagu();
                    $record->track_title            = $track_title;
                    $record->release_title          = $release_title;
                    $record->release_date           = $release_date;
                    $record->isrc                   = strtolower($isrc);
                    $record->upc                    = $upc;
                    $record->status                 = $status;
                    $record->no_contract            = $no_contract;
                    $record->percentage_pencipta    = $percentpencipta;
                    $record->percentage_penyanyi    = $percentpenyanyi ;
                    $record->percentage_rights      = $percentrights ;
                    $record->save();

                    if(!empty($singer)){
                        for ($x=0; $x < count($singer) ; $x++) {
                            RoleLagu::insert([
                                'id_master_lagu' => $record->id,
                                'id_user'        => $singer[$x]
                            ]);
                        }
                    }
                    if(!empty($songwriter)){
                        for ($i=0; $i < count($songwriter) ; $i++) { 
                            if(empty($songwriter[$i])){
                                Flash::error('Nama Pencipta / Publisher kosong..');
                                if (Auth::user()->hasRole('admin')) {
                                    return redirect(route('masterlagu.index'));
                                }elseif (Auth::user()->hasRole('legal')) {
                                    return redirect(route('legal.masterlagu.index'));
                                }elseif (Auth::user()->hasRole('anr')) {
                                    return redirect(route('anr.masterlagu.index'));
                                }
                            }else{
                                RoleLagu::insert([
                                    'id_master_lagu' => $record->id,
                                    'id_user'        => $songwriter[$i]
                                ]);
                            }
                        }
                    }
                    if(!empty($country)){
                        for ($c=0; $c < count($country) ; $c++) { 
                            if(empty($country[$c])){
                                Flash::error('Data Country kosong..');
                                if (Auth::user()->hasRole('admin')) {
                                    return redirect(route('masterlagu.index'));
                                }elseif (Auth::user()->hasRole('legal')) {
                                    return redirect(route('legal.masterlagu.index'));
                                }elseif (Auth::user()->hasRole('anr')) {
                                    return redirect(route('anr.masterlagu.index'));
                                }
                            }else{
                                RoleLaguCountry::insert([
                                    'id_master_lagu' => $record->id,
                                    'id_country' => $country[$c]
                                ]);
                            }
                        }
                    }
                    Flash::success('Master Lagu saved successfully.');
                    if (Auth::user()->hasRole('admin')) {
                        return redirect(route('masterlagu.index'));
                    }elseif (Auth::user()->hasRole('legal')) {
                        return redirect(route('legal.masterlagu.index'));
                    }elseif (Auth::user()->hasRole('anr')) {
                        return redirect(route('anr.masterlagu.index'));
                    }
                }else{
                    Flash::error('Duplicate ISRC! Master Lagu failed to saved.');
                    if (Auth::user()->hasRole('admin')) {
                        return redirect(route('masterlagu.index'));
                    }elseif (Auth::user()->hasRole('legal')) {
                        return redirect(route('legal.masterlagu.index'));
                    }elseif (Auth::user()->hasRole('anr')) {
                        return redirect(route('anr.masterlagu.index'));
                    }
                }
            }
        }
    }

    public function edit(Request $request, $id){

        //Get data lama, untuk diedit
        $masterlagu = MasterLagu::with('rolelagu','rolelagu.user','rolelagucountry')->find($id);
        
        if (empty($masterlagu)) {
            Flash::error('Song Master not found');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('masterlagu.index'));
            }elseif (Auth::user()->hasRole('legal')) {
                return redirect(route('legal.masterlagu.index'));
            }elseif (Auth::user()->hasRole('anr')) {
                return redirect(route('anr.masterlagu.index'));
            }
        }

        //untuk di listing
        $ssinger = User::join('access_role', 'users.id', '=', 'access_role.user_id')
                        ->where(['access_role.role_id'=>'9'])
                        ->orderBy('users.name', 'ASC')
                        ->get();
        //untuk di listing
        $ssongwriter = User::join('access_role', 'users.id', '=', 'access_role.user_id')
                        ->where(['access_role.role_id'=>'10'])
                        ->orderBy('users.name', 'ASC')
                        ->groupBy('users.name')
                        ->get();
        //untuk di listing
        $scountry = Country::orderBy('sorting', 'asc')->orderBy('id', 'asc')->get();

        // $singers = User::where('id', '=', $masterlagu->id_penyanyi)->first();
        // $songwriters = User::where('id', '=', $masterlagu->id_pencipta)->first();

        $penyanyi = array(); //untuk parsing data view penyanyi
        foreach ($masterlagu->rolelagu as $key => $value) {
            if(!empty($value->penyanyi)){
                $record = new \stdClass();
                $record->id = $value->penyanyi->id;
                $record->name = $value->penyanyi->name;
                $record->is_transfer = $value->penyanyi->is_transfer;
                array_push($penyanyi, $record);
            }
        }

        $pencipta = array(); //untuk parsing data view Pencipta
        foreach ($masterlagu->rolelagu as $key => $value) {
            if(!empty($value->pencipta)){
                $recor = new \stdClass();
                $recor->id = $value->pencipta->id;
                $recor->name = $value->pencipta->name;
                $recor->is_transfer = $value->pencipta->is_transfer;
                array_push($pencipta, $recor);
            }
        }

        $negara = array(); //untuk parsing data view Pencipta
        foreach ($masterlagu->rolelagucountry as $key => $value) {
            if(!empty($value->country)){
                $recor = new \stdClass();
                $recor->id = $value->country->id;
                $recor->country_name = $value->country->country_name;
                array_push($negara, $recor);
            }
            //return $value->country;
        }
        //dd($ssongwriter);
        //dd( array_push($negara, $recor) );
        return view('masterlagu.edit', [
            'pencipta'      => $pencipta,
            'penyanyi'      => $penyanyi,
            'negara'        => $negara,
            'masterlagu'    => $masterlagu,
            'singerx'       => $ssinger,
            'songwriterx'   => $ssongwriter,
            'countryx'      => $scountry
        ]);
    }

    public function update(Request $request, $id){

        //dd($request);
        $master_id          =   $request->master_id;
        $singer             =   $request->singer;
        $transing           =   $request->transing;
        $songwriter         =   $request->songwriter;
        $transong           =   $request->transong;
        $track_title        =   $request->track_title;
        if(!empty($request->release_title)){
            $release_title      =   $request->release_title;
        }else{
            $release_title      =   "Self-Titled";
        }
        if(!empty($request->release_date)){
            $release_date       =   date('Y-m-d', strtotime($request->release_date));
        }else{
            $release_date       =   "0000:00:00";
        }
        $label_name         =   $request->label_name;
        $isrc               =   strtolower($request->isrc);
        $upc                =   $request->upc;
        $country            =   $request->country;
        $submitbutton       =   $request->submitbutton;
        if($submitbutton == 'Save'){
            $status = 1;
        }else{
            $status = 2;
        }
        $no_contract        =   $request->no_contract;

        // JIKA DATA TIDAK KOSONG
            if(!empty($songwriter)){
                if(count($singer)>10){
                    Flash::error('Maximum singer 10.');
                    if (Auth::user()->hasRole('admin')) {
                        return redirect('./admin/masterlagu/'.$master_id .'/edit');
                    }elseif (Auth::user()->hasRole('legal')) {
                        return redirect('./legal/masterlagu/'.$master_id .'/edit');
                    }elseif (Auth::user()->hasRole('anr')) {
                        return redirect('./anr/masterlagu/'.$master_id .'/edit');
                    }
                }
            }
            if(!empty($songwriter)){
                if(count($songwriter)>10){
                    Flash::error('Max 10imum songwritter 10.');
                    if (Auth::user()->hasRole('admin')) {
                        return redirect('./admin/masterlagu/'.$master_id .'/edit');
                    }elseif (Auth::user()->hasRole('legal')) {
                        return redirect('./legal/masterlagu/'.$master_id .'/edit');
                    }elseif (Auth::user()->hasRole('anr')) {
                        return redirect('./anr/masterlagu/'.$master_id .'/edit');
                    }
                }
            }
                

            if ($request->percentpencipta == '') {
                $percentpencipta    =   0;
            }else{
                $percentpencipta    =   str_replace('%', '', $request->percentpencipta) / 100;
            }
            if ($request->percentpenyanyi == '') {
                $percentpenyanyi    =   0;
            }else{
                $percentpenyanyi    =   str_replace('%', '', $request->percentpenyanyi) / 100;
            }
            if ($request->percentrights == '') {
                $percentrights    =   0;
            }else{
                $percentrights    =   str_replace('%', '', $request->percentrights) / 100;
            }
            //return $songwriter;
        $masterLagu = MasterLagu::where(['id'=> $master_id])
                    ->update([
                        'track_title'           =>   $track_title,
                        'release_title'         =>   $release_title,
                        'release_date'          =>   $release_date,
                        'label_name'            =>   $label_name,
                        'isrc'                  =>   $isrc,
                        'upc'                   =>   $upc,
                        'status'                =>   $status,
                        'no_contract'           =>   $no_contract,
                        'percentage_pencipta'   =>   $percentpencipta,
                        'percentage_penyanyi'   =>   $percentpenyanyi,
                        'percentage_rights'     =>   $percentrights,
                    ]);

        /*UNTUK UPDATE ROLE LAGU,
        HAPUS SEMUA DATA ROLE LAGU BERDASARKAN id_master_lagu, 
        KEMUDIAN DI INSERT BARU*/

        //dd($masterLagu);
        if($masterLagu){
            RoleLagu::where(['id_master_lagu'=> $master_id])->delete();
            RoleLaguCountry::where(['id_master_lagu'=> $master_id])->delete();

            if(!empty($singer)){
                for ($x=0; $x < count($singer) ; $x++) { 
                    if(empty($singer[$x])){
                        Flash::error('Field is required.');
                        if (Auth::user()->hasRole('admin')) {
                            return redirect('./admin/masterlagu/'.$master_id .'/edit');
                        } elseif (Auth::user()->hasRole('legal')) {
                            return redirect('./legal/masterlagu/'.$master_id .'/edit');
                        }
                    }
                    RoleLagu::insert([
                        'id_master_lagu' => $master_id,
                        'id_user'        => $singer[$x]
                    ]);
                }
            }

            if(!empty($songwriter)){
                for ($i=0; $i < count($songwriter) ; $i++) { 
                    if(empty($songwriter[$i])){
                        Flash::error('Field is required.');
                        if (Auth::user()->hasRole('admin')) {
                            return redirect('./admin/masterlagu/'.$master_id .'/edit');
                        }elseif (Auth::user()->hasRole('legal')) {
                            return redirect('./legal/masterlagu/'.$master_id .'/edit');
                        }elseif (Auth::user()->hasRole('anr')) {
                            return redirect('./anr/masterlagu/'.$master_id .'/edit');
                        }
                    }
                    RoleLagu::insert([
                        'id_master_lagu' => $master_id,
                        'id_user' => $songwriter[$i]
                    ]);
                }
            }

            if(!empty($country)){
                for ($c=0; $c < count($country) ; $c++) { 
                    if(empty($country[$c])){
                        Flash::error('Field is required.');
                        if (Auth::user()->hasRole('admin')) {
                            return redirect('./admin/masterlagu/'.$master_id .'/edit');
                        }elseif (Auth::user()->hasRole('legal')) {
                            return redirect('./legal/masterlagu/'.$master_id .'/edit');
                        }elseif (Auth::user()->hasRole('anr')) {
                            return redirect('./anr/masterlagu/'.$master_id .'/edit');
                        }
                    }

                    RoleLaguCountry::insert([
                        'id_master_lagu' => $master_id,
                        'id_country' => $country[$c]
                    ]);
                }
            }
            
            Flash::success('Master Lagu Updated successfully.');
            if (Auth::user()->hasRole('admin')) {
                return redirect('./admin/masterlagu/');
            }elseif (Auth::user()->hasRole('legal')) {
                return redirect('./legal/masterlagu/'.$master_id .'/edit');
            }elseif (Auth::user()->hasRole('anr')) {
                return redirect('./anr/masterlagu/'.$master_id .'/edit');
            }
        }else{
            Flash::error('Master Lagu Updated Failed.');
            if (Auth::user()->hasRole('admin')) {
                return redirect('./admin/masterlagu/');
            }elseif (Auth::user()->hasRole('legal')) {
                return redirect('./legal/masterlagu/'.$master_id .'/edit');
            }elseif (Auth::user()->hasRole('anr')) {
                return redirect('./anr/masterlagu/'.$master_id .'/edit');
            }
        }
    }
    
    public function destroy($id){
        $checklagu = $this->masterlagu->with(['rolelagu','rolelagu.penyanyi','rolelagu.pencipta'])->find($id);
        $masterlagu = MasterLagu::where('id', $id)->delete();

        if($masterlagu){
            RoleLagu::where('id_master_lagu', $id)->delete();
            Flash::success('Data Master Lagu Successfully Delete.');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('masterlagu.index'));
            }elseif(Auth::user()->hasRole('legal')){
                return redirect(route('legal.masterlagu.index'));
            }elseif(Auth::user()->hasRole('anr')){
                return redirect(route('anr.masterlagu.index'));
            }
        }else{
            Flash::error('Data Master Lagu Failed Delete.');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('masterlagu.index'));
            }elseif(Auth::user()->hasRole('legal')){
                return redirect(route('legal.masterlagu.index'));
            }elseif(Auth::user()->hasRole('anr')){
                return redirect(route('anr.masterlagu.index'));
            }
        }
    }
    public function template(){
        $pathToFile = public_path('/uploads/template/template_upload_masterlagu_excel.xlsx');
        return response()->download($pathToFile);
    }

    public function csv_export(){
        $masterlagu =$masterlagu = $this->masterlagu->with(['rolelagu.penyanyi','rolelagu.pencipta','rolelagucountry.country'])->get();

        return view('masterlagu.export_csv',compact('masterlagu'));
    }

    public function excell_export()
    {
        $id='';
        $filename = "Reporting-Master-Lagu_".date("Y-m-d");
        $excel = Excel::create($filename, function($excel) use ($id) {

            $excel->sheet('Sheet1', function($sheet) use ($id) {
                if (Auth::user()->roles->first()->name == 'singer') {
                    $query = $this->masterlagu->with('rolelagu','rolelagu.penyanyi','rolelagu.pencipta')
                    ->whereHas('rolelagu', function($q){
                        $q->where('role_lagu.id_user', Auth::user()->id);
                    })
                    ->orderBy('master_lagu.id', 'DESC')
                    ->get();
                }elseif (Auth::user()->roles->first()->name == 'songwriter') {
                    $query = $this->masterlagu->with('rolelagu','rolelagu.penyanyi','rolelagu.pencipta')
                    ->whereHas('rolelagu', function($q){
                        $q->where('role_lagu.id_user', Auth::user()->id);
                    })
                    ->orderBy('master_lagu.id', 'DESC')
                    ->get();
                }else{
                    $query = $this->masterlagu->with('rolelagu','rolelagu.penyanyi','rolelagu.pencipta')
                    ->orderBy('master_lagu.id', 'DESC')
                    ->get();
                }
                $sheet->loadView('masterlagu.export_excel', [
                    'data' => $query
                ]);
            });
        });
        return $excel->export('xlsx');
    }


    public function export_print()
    {
        if (Auth::user()->roles->first()->name == 'singer') {
            $data = $this->masterlagu->with('rolelagu','rolelagu.penyanyi','rolelagu.pencipta')
            ->whereHas('rolelagu', function($q){
                $q->where('role_lagu.id_user', Auth::user()->id);
            })
            ->orderBy('master_lagu.release_title', 'ASC')
            ->get();
        }elseif (Auth::user()->roles->first()->name == 'songwriter') {
            $data = $this->masterlagu->with('rolelagu','rolelagu.penyanyi','rolelagu.pencipta')
            ->whereHas('rolelagu', function($q){
                $q->where('role_lagu.id_user', Auth::user()->id);
            })
            ->orderBy('master_lagu.release_title', 'ASC')
            ->get();
        }else{
            $data = MasterLagu::with('rolelagu','rolelagucountry')
            ->orderBy('master_lagu.release_title', 'ASC')
            ->get();
            //return $data;
        }
        return view('masterlagu.export_print',compact('data'));
    }

    public function detail_lagu($id)
    {
        $masterlagu = $this->masterlagu->with(['rolelagu','rolelagu.penyanyi','rolelagu.pencipta','rolelagucountry.country'])->find($id);
        //return $masterlagu;

        if (empty($masterlagu)) {
            Flash::error('Master Lagu not found');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('masterlagu.index'));
            }elseif(Auth::user()->hasRole('legal')){
                return redirect(route('legal.masterlagu.index'));
            }elseif(Auth::user()->hasRole('anr')){
                return redirect(route('anr.masterlagu.index'));
            }
        }

        return view('masterlagu.detail.lagu.index', [
            'masterlagu'  => $masterlagu
        ]);
    }
    public function detail_singer($id)
    {
        $singer = User::with('rolelagu','rolelagu.masterlagu.rolelagu.pencipta')->find($id);
        //return $singer;
        if (empty($singer)) {
            Flash::error('Singer not found');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('masterlagu.index'));
            }elseif(Auth::user()->hasRole('legal')){
                return redirect(route('legal.masterlagu.index'));
            }elseif(Auth::user()->hasRole('anr')){
                return redirect(route('anr.masterlagu.index'));
            }
        }
        
        return view('masterlagu.detail.penyanyi.index', [
            'singer'  => $singer
        ]);
    }
    public function detail_songwriter($id)
    {
        $songwriter = User::with('rolelagu','rolelagu.masterlagu.rolelagu.penyanyi')->find($id);
        //return $singer;
        if (empty($songwriter)) {
            Flash::error('Songwriter not found');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('masterlagu.index'));
            }elseif(Auth::user()->hasRole('legal')){
                return redirect(route('legal.masterlagu.index'));
            }elseif(Auth::user()->hasRole('anr')){
                return redirect(route('anr.masterlagu.index'));
            }
        }
        
        return view('masterlagu.detail.pencipta.index', [
            'songwriter'  => $songwriter
        ]);
    }
}
