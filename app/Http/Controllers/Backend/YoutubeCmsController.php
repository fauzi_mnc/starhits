<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\InfluencerDiagram;
use App\Model\DetailInfluencer;
use App\Model\User;
use DB;

class YoutubeCmsController extends Controller
{
    
    public function GoogleClient(){
        $Secret = DB::table('yt_content_secret')
                    ->where('user_id', auth()->user()->id)
                    ->where('type', 'youtube')
                    ->first();

        $client = new \Google_Client();
        $client->setClientId($Secret->client_id);
        $client->setClientSecret($Secret->client_secret);       
        $client->setScopes([
            'https://www.googleapis.com/auth/youtube',
            'https://www.googleapis.com/auth/yt-analytics-monetary.readonly',
            'https://www.googleapis.com/auth/youtubepartner',
            'https://www.googleapis.com/auth/yt-analytics.readonly'
        ]);
        $client->addScope(\Google_Service_YouTubeAnalytics::YT_ANALYTICS_READONLY);
        $client->setRedirectUri($Secret->client_redirect);
        $client->setAccessType('offline');
        $client->setPrompt('consent select_account');
        $client->setApprovalPrompt('force');
        return $client; 
    }

    public function index()
    {   
        $id = '110';
        $datas = User::where('id',$id)->first();
        $diagram = InfluencerDiagram::where(['user_id'=>$id])->get();
        $details = DetailInfluencer::where(['user_id'=>$id])->first();
        $datas['diagram']=$diagram;
        $datas['details']=$details;
        //return $datas;
        return view('youtube.cms.cms_detail.index',compact('datas'));
    }

    public function api_detail()
    {
        //return "ada";
        /*$diagram = InfluencerDiagram::selectRaw("
                    id, user_id, created_at,
                    MONTHNAME(created_at) AS created,
                    AVG(followers_ig) AS followers_ig,
                    AVG(avglike) AS avglike,
                    AVG(avgcomment) AS avgcomment,
                    AVG(engagement) AS engagement")
                    ->where(['user_id'=>'110'])
                    ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at)'))->get();*/
        //$diagram = InfluencerDiagram::where(['user_id'=>$id])->get();
        $diagram = DB::table('youtube_channel_analitic')->where(['channel_id'=>'UC0nFwQMy3fAyWMvZSAKfP5A'])->get();
        return $diagram;
    }


    public function getYTAnalytic($id){ //TO GET ALL REPORTING ALL DAY CHANNEL
        
        $client = $this->GoogleClient();
        $token = DB::table('user_token')
                ->where(['user_id'=> auth()->user()->id, 'type'=> 'youtube','action_dream'=>'yt-analytics'])
                ->first();

        $token = collect($token)->except(['id', 'user_id', 'type'])->toArray();
        $dateNow = date("Y-m-d");
        session(['action_dream'=>"yt-analytics"]);

        //$Channels = YoutubeChannel::where(['channel_id'=>'UCoSkllfpgmFHtbVK835QaQg'])->first();

        $Channels = YoutubeChannel::all();

        if (!empty($token)) {
            if($client->isAccessTokenExpired()){ //Jika Akses tokennya Expired
                $client->refreshToken($token['refresh_token']);
                $accessToken = $client->getAccessToken();
                $client->setAccessToken($accessToken);
                $upd = DB::table('user_token')
                    ->where('user_id', auth()->user()->id)
                    ->where('type', 'youtube')
                    ->update([
                        'access_token'  => $accessToken['access_token'],
                        'refresh_token' => $accessToken['refresh_token'],
                        'token_type'    => $accessToken['token_type'],
                        'expires_in'    => $accessToken['expires_in'],
                        'created'       => $accessToken['created']
                    ]);
            }
            
            $youtube = new \Google_Service_YouTubeAnalytics($client);
            

            foreach ($Channels as $Channels) {
                $queryParams = [
                    'endDate' => $dateNow,
                    'filters' => 'channel=='.$Channels->channel_id,
                    'ids' => 'contentOwner=='.$Channels->content_id,
                    'dimensions'=>'day',
                    'sort'=>'day',
                    /*'metrics' => 'views,estimatedMinutesWatched,averageViewDuration,averageViewPercentage,subscribersGained',*/
                    'metrics' =>'views,likes,dislikes,comments,shares,estimatedRevenue,cpm,cardClickRate,cardClicks,cardImpressions,grossRevenue,subscribersLost,subscribersGained,estimatedMinutesWatched,monetizedPlaybacks',
                    'startDate' => $Channels->publishat
                ];

                $response = $youtube->reports->query($queryParams);
                for ($i=0; $i < count($response->rows); $i++) { 
                   /* DB::table('youtube_cms_analitic')->insert([
                        'user_id'=>Auth::user()->id,
                        'content_id'=>$Channels->content_id,
                        'channel_id'=>$Channels->channel_id,
                        'date'=>$response->rows[$i][0],
                        'views'=>$response->rows[$i][1],
                        'likes'=>$response->rows[$i][2],
                        'dislikes'=>$response->rows[$i][3],
                        'comments'=>$response->rows[$i][4],
                        'shares'=>$response->rows[$i][5],
                        'estimatedRevenue'=>$response->rows[$i][6],
                        'cpm'=>$response->rows[$i][7],
                        'cardClickRate'=>$response->rows[$i][8],
                        'cardClicks'=>$response->rows[$i][9],
                        'cardImpressions'=>$response->rows[$i][10],
                        'grossRevenue'=>$response->rows[$i][11],
                        'subscribersLost'=>$response->rows[$i][12],
                        'subscribersGained'=>$response->rows[$i][13],
                        'estimatedMinutesWatched'=>$response->rows[$i][14],
                        'monetizedPlaybacks'=>$response->rows[$i][15]
                    ]);*/
                }
            }
        } else {
            $authUrl = $client->createAuthUrl();
            return redirect($authUrl);
        }
        
    }

}
