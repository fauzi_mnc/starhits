<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\User;
use App\Model\City;
use App\Model\UserGroup;
use App\Model\AccessRole;
use App\Model\ContentSecret;
use Flash;
use DateTime;
use Auth;
use App\Http\Requests\UpdatePersonalRequest;
use App\Repositories\UserRepository;
use App\Repositories\RoleRepository;
use Session;
use Hash;

class PersonalController extends Controller
{
    /**
     * The page repository instance.
     */
    protected $users;
    protected $roles;

    /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users, RoleRepository $roles)
    {
        $this->users = $users;
        $this->roles = $roles;
    }
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function index()
    {
        $user = User::find(Auth::id());

        if (empty($user)) {
            Flash::error('User not found');

            if (Auth::user()->hasRole('creator')) {
                return redirect(route('creator.personal.index'));
            }elseif (Auth::user()->hasRole('brand')) {
                return redirect(route('brand.personal.index'));
            }elseif (Auth::user()->hasRole('user')) {
                return redirect(route('user.personal.index'));
            }elseif (Auth::user()->hasRole('finance')) {
                return redirect(route('finance.personal.index'));
            }else{
                return redirect(route('admin.personal.index'));
            }
        }
        $cities = City::where('id', '=', $user->city)->get();
        //dd($cities);
        return view('personal.index')->with('user', $user, $cities);
    }

    public function formaccount(){
        $creators = User::whereHas('accessrole', function ($query) {
            $query->where('role_id', '2');
        })->where('is_active', '=', '1')->whereNotNull('provider_id')->get();

        // return $creator;

        return view('personal.add_account', ['creators' => $creators]);
    }

    public function postAccount(Request $request){
        // dd($request->creator_child);
        $redirect = Auth::user()->roles[0]->name;
        $master = $request->creator_parent;
        $child = $request->creator_child;
        //return($model);
        if (!empty($master)) {
            for ($i=0; $i < count($child) ; $i++) { 
                UserGroup::insert(['user_master' => $master, 'user_child' => $child[$i] ]);
            }
            Flash::success('Data has been added.');
            return redirect($redirect.'/addaccount');
        }else{
            Flash::error('Failed to added!');
            return redirect($redirect.'/addaccount');   
        }
    }

    public function changeAccount($id){

        if(empty(Auth::user()->id)){
            return redirect('login');
        }else{
            $master = UserGroup::where(['user_master'=>Auth::user()->id])->first();
            $child  = UserGroup::where(['user_child'=>Auth::user()->id])->first();

            Auth::logout();

            if(!empty($master)){ //Jika auth tergolong Grup master
                //master > child
                $findSwitch = UserGroup::where(['user_master'=> $master->user_master, 'user_child'=>$id])->first();
                $user = User::where('id', $findSwitch->user_child)->first();
            }elseif(!empty($child)){ //Jika auth tergolong Grup child

                //child > child
                $findChild = UserGroup::where(['user_master'=> $child->user_master, 'user_child'=>$id])->first();
                if(!empty($findChild)){
                    $user = User::where('id', $findChild->user_child)->first();
                }
                
                //child > master
                $findMaster = UserGroup::where(['user_master'=> $id])->first();
                if(!empty($findMaster)){
                    $user = User::where('id', $findMaster->user_master)->first();
                }
            }
            
            /*$role = AccessRole::where(['user_id'=>$user->id])->first();
            $cekRole = RoleUser::where(['user_id'=>$user->id])->first();
            if(empty($cekRole)){
                $user->roles()->attach($role->role_id);
            }*/
           /// Session::forget('user_group');
           // Session::forget('group_master');

           // $UserGroup = UserGroup::where(['user_master'=>$user->id])->get();

            //return $UserGroup;
            

            Auth::login($user, true);

           // session(['user_group'=>$UserGroup]);

            ///$userz = Auth::user();
            /*$role = AccessRole::where(['user_id'=>$userz->id])->get();
            session(['user_role'=>$role]);*/

            if (Auth::user()->hasRole('creator')) {
                return redirect(url('creator/personal'));
            } elseif (Auth::user()->hasRole('brand')) {
                return redirect(url('/brand/campaign'));
            } elseif (Auth::user()->hasRole('influencer')) {
                return redirect(url('/influencer/campaign'));
            } elseif (Auth::user()->hasRole('member')) {
                return redirect(url('/member/'.auth()->id().'/edit'));
            } elseif (Auth::user()->hasRole('user')) {
                return redirect(url('/user/dashboard'));
            } elseif (Auth::user()->hasRole('finance')) {
                return redirect(url('/finance/dashboard'));
            } elseif (Auth::user()->hasRole('singer')) {
                return redirect(url('/singer/dashboard'));
            }elseif (Auth::user()->hasRole('songwriter')) {
                return redirect(url('/songwriter/dashboard'));
            } elseif (Auth::user()->hasRole('legal')) {
                return redirect(url('/legal/dashboard'));
            } elseif (Auth::user()->hasRole('anr')) {
                return redirect(url('/anr/dashboard'));
            } elseif (Auth::user()->hasRole('cms')) {
                return redirect(url('/cms/dashboard'));
            } else {
                return redirect(url('/admin/dashboard'));
            }  
        }   
    }

    public function changeRole($roles){
        // $role = AccessRole::find($roles);
        $user = User::where('id', Auth::user()->id)->first();
        $user->roles()->detach();
        $user->roles()->attach($roles); // id only 

        Auth::logout();
        Auth::login($user, true);

        if (Auth::user()->hasRole('creator')) {
            return redirect(url('creator/personal'));
        } elseif (Auth::user()->hasRole('brand')) {
            return redirect(url('/brand/campaign'));
        } elseif (Auth::user()->hasRole('influencer')) {
            return redirect(url('/influencer/campaign'));
        } elseif (Auth::user()->hasRole('member')) {
            return redirect(url('/member/'.auth()->id().'/edit'));
        } elseif (Auth::user()->hasRole('user')) {
            return redirect(url('/user/dashboard'));
        } elseif (Auth::user()->hasRole('finance')) {
            return redirect(url('/finance/dashboard'));
        } elseif (Auth::user()->hasRole('singer')) {
            return redirect(url('/singer/dashboard'));
        }elseif (Auth::user()->hasRole('songwriter')) {
            return redirect(url('/songwriter/dashboard'));
        } elseif (Auth::user()->hasRole('legal')) {
            return redirect(url('/legal/dashboard'));
        } elseif (Auth::user()->hasRole('anr')) {
            return redirect(url('/anr/dashboard'));
        } elseif (Auth::user()->hasRole('cms')) {
            return redirect(url('/cms/dashboard'));
        } else {
            return redirect(url('/admin/dashboard'));
        }     
     }

    public function logout(){
        Auth::logout();
        return redirect('login');
    }

    function edit($id)
    {
        $user = User::find($id);
        $cities = $this->users->getcities();

        if (empty($user)) {
            Flash::error('User not found');

            if (Auth::user()->hasRole('creator')) {
                return redirect(route('creator.personal.index'));
            }elseif (Auth::user()->hasRole('brand')) {
                return redirect(route('brand.personal.index'));
            }elseif (Auth::user()->hasRole('user')) {
                return redirect(route('user.personal.index'));
            }else{
                return redirect(route('admin.personal.index'));
            }
        }
        return view('personal.edit', [
            'cities' => $cities
        ])->with('user', $user);
    }

    public function update($id, UpdatePersonalRequest $request) {
        if (Auth::user()->roles->first()->name == 'brand') {
            $user = $this->users->find($id);
        
            if (empty($user)) {
                Flash::error('Brand not found');

                return redirect(route('brand.index'));
            }

            $input = $request->except(['_method', '_token']);

            $user = $this->users->update($input, $id);
        }else{
            $set = User::find($id);
            $set['name'] = $request->name;
            $set['gender'] = $request->gender;
            $set['email'] = $request->email;
            $set['dob'] = date("Y-m-d", strtotime($request->dob));
            $set['phone'] = $request->phone;
            $set['country'] = $request->country;
            $set['city'] = $request->city;
            $set['ktp'] = $request->ktp;
            $set['biodata'] = strip_tags($request->biodata);
            if ($request->image != NULL) {
                $set['image'] = '/uploads/creators/image/'.time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads/creators/image/'), time().'.'.$request->image->getClientOriginalExtension());
            }
            if ($request->cover != NULL) {
                $set['cover'] = '/uploads/creators/cover/'.time().'.'.$request->cover->getClientOriginalExtension();
                $request->cover->move(public_path('uploads/creators/cover/'), time().'.'.$request->cover->getClientOriginalExtension());
            }
            
            $set->save();
        }
        

        Flash::success('Personal updated successfully.');

        if (Auth::user()->roles->first()->name == 'creator') {
            return redirect(route('creator.personal.index'));
        }elseif (Auth::user()->roles->first()->name == 'brand') {
            return redirect(route('brand.personal.index'));
        }elseif (Auth::user()->roles->first()->name == 'user') {
            return redirect(route('user.personal.index'));
        }elseif (Auth::user()->roles->first()->name == 'finance') {
            return redirect(route('finance.personal.index'));
        }elseif (Auth::user()->roles->first()->name == 'legal') {
            return redirect(route('legal.personal.index'));
        }else{
            return redirect(route('admin.personal.index'));
        }
    }

    public function formKeySetting(){
        $datas  = ContentSecret::all();
        return view('keysetting.index',compact('datas'));
    }

    public function keysetting(Request $request){
        //ContentSecret::


    }
   

}
