<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repositories\UserRepository;
use App\Model\Bank;
use App\Model\CategoryInst;
use App\Repositories\RoleRepository;
use App\DataTables\InfluencerDataTable;
use Flash;
use App\Http\Requests\StoreOrUpdateCreatorRequest;
use App\Http\Requests\UpdateCreatorRequest;
use Storage;

class MemberController extends Controller
{
    /**
     * The page repository instance.
     */
    protected $users;
    protected $roles;

    /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users, RoleRepository $roles)
    {
        $this->users = $users;
        $this->roles = $roles;
    }

    public function upgrade($id)
    {
        $user = $this->users->with(['detailInfluencer'])->find($id);
        
        return view('member.upgrade', ['user' => $user]);
    }

    public function edit($id)
    {
        $user = $this->users->with(['detailInfluencer'])->find($id);
        
        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();

        if (empty($user)) {
            Flash::error('Member not found');

            return redirect(route('self.member.index'));
        }
        
        return view('member.setting', [
            'user'        => $user,
            'countries'   => $countries,
            'cities'      => $cities,
        ]);
    }

    public function update(Request $request, $id)
    {
        $user = $this->users->find($id);

        if (empty($user)) {
            Flash::error('Member not found');

            return redirect(route('self.member.index'));
        }
        
        $userInput = $request->only([
            'name',
            'phone',
            'country',
            'password',
            'category',
            'email',
            'gender',
            'city',
            'is_active'
        ]);
        
        $userInput['is_active'] =  ($request->is_active == 'on') ? 1 : 0;
        
        if(!empty($userInput)){
            $user = $this->users->update($userInput, $id);
        }

        Flash::success('Member updated successfully.');

        return redirect(route('self.member.edit', auth()->id()));
    }

    public function role($id, $role)
    {
        $user = $this->users->find($id);

        $categories = CategoryInst::all()->pluck('title')->toArray();
        
        if (empty($user)) {
            Flash::error('Member not found');

            return redirect(route('self.member.index'));
        }
        
        return view('member.'.$role, [
            'user' => $user, 
            'role' => $role,
            'categories'  => array_combine($categories,$categories)
        ]);
    }

    public function storeRole(Request $request, $id, $role)
    {
        $user = $this->users->find($id);

        if (empty($user)) {
            Flash::error('Member not found');

            return redirect(route('self.member.index'));
        }

        if($role == 'influencer'){
            $user->detailInfluencer()->create([
                'username' => $request->username,
                'category' => implode(',', $request->category)
            ]);
        } else {
            $input = [
                'subscribers' => $request->subscribers,
                'provider_id' => $request->provider_id,
                'image' => $request->image
            ];
            
            $path = 'uploads/creators/image/'.str_random(32).'.jpg';
            $image = Storage::disk('local_public')->put($path, file_get_contents($input['image']));
            $input['image'] = $path;

            $user->update($input);
        }
        
        $roleId = $this->roles->findWhere([
            'name'=> $role
        ])->first()->id;

        $user->roles()->sync([$roleId]);

        Flash::success('Member upgrade successfully.');

        return redirect(url($role.'/home'));
    }

}
