<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Flash;
use DateTime;
use Socialite;
use Carbon\Carbon;
use App\Extend\DailymotionCli;
use App\Model\Analytics;
use Hborras\TwitterAdsSDK\TwitterAds;

class PlatformsController extends Controller
{
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function index()
    {
        try {
            $token = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'youtube')->first();
            $users['youtube'] = Socialite::driver('youtube')->userFromToken($token->access_token);
            
        } catch (\Exception $e) {
            $users['youtube'] = NULL;
        }

        try {
            $token = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'twitter')->first();
            $users['twitter'] = Socialite::driver('twitter')->userFromTokenAndSecret($token->access_token, $token->token_secret);
            
        } catch (\Exception $e) {
            $users['twitter'] = NULL;
        }

        try {
            $token = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'dailymotion')->first();
            $users['dailymotion'] = Socialite::driver('dailymotion')->userFromToken($token->access_token);
            
        } catch (\Exception $e) {
            $users['dailymotion'] = NULL;
        }
        
        return view('platforms.index')->with(['users'=> $users]);
    }

    public function log($provider)
    {
        // return Socialite::with('youtube')->redirect();
        // return header('Location: ' . env('GOOGLE_REDIRECT'));

        if($provider == 'dailymotion'){
            return Socialite::with($provider)->scopes(['manage_videos', 'write', 'delete'])->redirect();
        }

        return Socialite::with($provider)->redirect();
    }

    public function callbackLogin($provider) {
        // if(!session_id()) {
        //     session_start();
        // }
        // $fb = new \Facebook\Facebook([
        //   'app_id' => '249172948960376', // Replace {app-id} with your app id
        //   'app_secret' => 'b350b44e60ab545e3f7ed3cfbb2df144',
        //   'default_graph_version' => 'v2.2',
        //   ]);

        // $helper = $fb->getRedirectLoginHelper();

        // try {
        //   $accessToken = $helper->getAccessToken();
        // } catch(\Facebook\Exceptions\FacebookResponseException $e) {
        //   // When Graph returns an error
        //   echo 'Graph returned an error: ' . $e->getMessage();
        //   exit;
        // } catch(\Facebook\Exceptions\FacebookSDKException $e) {
        //   // When validation fails or other local issues
        //   echo 'Facebook SDK returned an error: ' . $e->getMessage();
        //   exit;
        // }

        // if (! isset($accessToken)) {
        //   if ($helper->getError()) {
        //     header('HTTP/1.0 401 Unauthorized');
        //     echo "Error: " . $helper->getError() . "\n";
        //     echo "Error Code: " . $helper->getErrorCode() . "\n";
        //     echo "Error Reason: " . $helper->getErrorReason() . "\n";
        //     echo "Error Description: " . $helper->getErrorDescription() . "\n";
        //   } else {
        //     header('HTTP/1.0 400 Bad Request');
        //     echo 'Bad request';
        //   }
        //   exit;
        // }

        // // Logged in
        // echo '<h3>Access Token</h3>';
        // var_dump($accessToken->getanalytic());

        // // The OAuth 2.0 client handler helps us manage access tokens
        // $oAuth2Client = $fb->getOAuth2Client();

        // // Get the access token metadata from /debug_token
        // $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        // echo '<h3>Metadata</h3>';
        // var_dump($tokenMetadata);

        // // Validation (these will throw FacebookSDKException's when they fail)
        // $tokenMetadata->validateAppId('249172948960376'); // Replace {app-id} with your app id
        // // If you know the user ID this access token belongs to, you can validate it here
        // //$tokenMetadata->validateUserId('123');
        // $tokenMetadata->validateExpiration();

        // if (! $accessToken->isLongLived()) {
        //   // Exchanges a short-lived access token for a long-lived one
        //   try {
        //     $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
        //   } catch (\Facebook\Exceptions\FacebookSDKException $e) {
        //     echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
        //     exit;
        //   }

        //   echo '<h3>Long-lived</h3>';
        //   var_dump($accessToken->getanalytic());
        // }
        
        $accessToken = Socialite::driver($provider)->user();
        //$api = TwitterAds::init(env('TWITTER_CLIENT_ID'), env('TWITTER_CLIENT_SECRET'), $accessToken->token, $accessToken->tokenSecret);
        //$accounts = $api->getAccounts();
        //$account = new Account($accessToken->id);

        //dd($accounts);
            if($provider == 'twitter'){
            $accessTokenDb = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', $provider)->get();
            if(!$accessTokenDb->isEmpty()){
                DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', $provider)->update([
                    'access_token'  => $accessToken->token,
                    'token_secret'  => $accessToken->tokenSecret
                ]);
            } else {
                DB::table('user_token')->insert([
                    'user_id'       => auth()->user()->id,
                    'type'          => $provider,
                    'access_token'  => $accessToken->token,
                    'token_secret'  => $accessToken->tokenSecret
                ]);
            }

            
        }   
        
        return redirect()->route('creator.platforms.index');
    }

    public function logout($provider){

        switch ($provider) {
            case 'youtube':
                $client = new \Google_Client();
                $client->setClientId(env('GOOGLE_CLIENT_ID'));
                $client->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
                $client->setScopes('https://www.googleapis.com/auth/youtube');
                $client->setRedirectUri(env('GOOGLE_REDIRECT'));
                $client->setAccessType('offline');
                $client->setPrompt('consent select_account');
                $token = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'youtube')->first();
                $token = collect($token)->except(['id', 'user_id', 'type'])->toArray();
                $client->setAccessToken($token);
                $client->revokeToken();
                break;
            case 'twitter':
                $twitter  = \Codebird\Codebird::getInstance();
                $token = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'twitter')->first();
                $twitter->setToken($token->access_token, $token->token_secret);
                $twitter->logout();
                break;
            case 'dailymotion':
                $api = new DailymotionCli();
                $api->setGrantType(\Dailymotion::GRANT_TYPE_AUTHORIZATION, env('DAILYMOTION_CLIENT_ID'), env('DAILYMOTION_CLIENT_SECRET'), ['manage_videos', 'write', 'delete']);
        
                $result = $api->get('/logout', array());
                break;
            
            default:
                # code...
                break;
        }
        
        $result = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', $provider)->delete();

        return redirect()->route('creator.platforms.index');
    }
    
    public function dailymotionLogout(){
        $api = new DailymotionCli();
        $api->setGrantType(\Dailymotion::GRANT_TYPE_AUTHORIZATION, env('DAILYMOTION_CLIENT_ID'), env('DAILYMOTION_CLIENT_SECRET'), ['manage_videos', 'write', 'delete']);

        $result = $api->get('/logout', array());
        DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'dailymotion')->delete();

        return redirect()->route('creator.platforms.index');
    }

    public function dailymotion(){
        $api = new DailymotionCli();
        $api->setGrantType(\Dailymotion::GRANT_TYPE_AUTHORIZATION, env('DAILYMOTION_CLIENT_ID'), env('DAILYMOTION_CLIENT_SECRET'), ['manage_videos', 'write', 'delete']);

        try
        {
            $result_videos = $api->get(
                '/me/videos',
                array('fields' => array('id', 'title', 'owner', 'likes_total', 'created_time', 'views_total', 'views_last_week', 'views_last_month', 'views_last_day'))
            );
            $total_likes = 0;

            foreach ($result_videos['list'] as $index => $video){
                $total_likes += $video['likes_total'];
                $id_user = $video['owner'];
            }
            $result_me = $api->get('/user/'.$id_user, array('fields' => array('following_total', 'videos_total', 'views_total')));
            
            //dd($result_me['views_total']);
            $data['date'] = new DateTime;
            $data['views'] = $result_me['views_total'];
            $data['likes'] = $total_likes;
            $data['subscribers_gained'] = $result_me['following_total'];
            $data['type'] = 'dailymotion';
            $data['user_id'] = auth()->id();
            $data['created_at'] = new DateTime;
            $data['updated_at'] = $data['created_at'];
                
            //dd($data);                            
            $result = Analytics::insert($data);

            //dd($result);

            return redirect()->route('creator.platforms.index');
        }
        catch (\DailymotionAuthRequiredException $e)
        {
            // Redirect the user to the Dailymotion authorization page
            header('Location: ' . $api->getAuthorizationUrl());
            die();
        }
        catch (\DailymotionAuthRefusedException $e)
        {
            // Handle case when user refused to authorize
            // <YOUR CODE>
            return redirect()->route('creator.platforms.index');
            
        }
    }

    public function youtube() {

        $client = new \Google_Client();
        $client->setClientId(env('GOOGLE_CLIENT_ID'));
        $client->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
        $client->setScopes('https://www.googleapis.com/auth/youtube');
        $client->addScope(\Google_Service_YouTubeAnalytics::YT_ANALYTICS_READONLY);
        $client->setRedirectUri(env('GOOGLE_REDIRECT'));
        $client->setAccessType('offline');
        $client->setPrompt('consent select_account');

        // Define an object that will be used to make all API requests.
        $youtube = new \Google_Service_YouTube($client);
        
        if (isset($_GET['code'])) {     
            $client->authenticate($_GET['code']);
            $accessToken = $client->getAccessToken();

            $accessTokenDb = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'youtube')->get();
            if(!$accessTokenDb->isEmpty()){
                DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'youtube')->update([
                    'access_token'  => $accessToken['access_token'],
                    'refresh_token' => $accessToken['refresh_token'],
                    'token_type'    => $accessToken['token_type'],
                    'expires_in'    => $accessToken['expires_in'],
                    'created'       => $accessToken['created']
                ]);

                $analytics = Analytics::where('type', 'Youtube')->where('user_id', auth()->id())->get();
                
                //insert analytic for the first time
                if($analytics->isEmpty()){
                    $analytics = new \Google_Service_YouTubeAnalytics($client);
                    $metrics = 'views,comments,likes,dislikes,shares,estimatedMinutesWatched,estimatedRedMinutesWatched,averageViewDuration,averageViewPercentage,annotationClickThroughRate,annotationCloseRate,annotationImpressions,annotationClickableImpressions,annotationClosableImpressions,annotationClicks,annotationCloses,subscribersGained,subscribersLost';
                    
                    $result = $analytics->reports->query([
                        'ids'       => 'channel==MINE',
                        'startDate' => Carbon::now()->subQuarter()->toDateString(),
                        'metrics'   => $metrics,
                        'sort'      => 'day',
                        'endDate'   => Carbon::now()->toDateString(), 
                        'dimensions'=> 'day'
                    ]);       

                    $data = [];
                    foreach ($result->rows as $index => $analytic) {
                        
                        $data[$index]['date'] = $analytic[0];
                        $data[$index]['views'] = $analytic[1];
                        $data[$index]['comments'] = $analytic[2];
                        $data[$index]['likes'] = $analytic[3];
                        $data[$index]['dislikes'] = $analytic[4];
                        $data[$index]['shares'] = $analytic[5];
                        $data[$index]['estimate_minutes_watched'] = $analytic[6];
                        $data[$index]['average_view_duration'] = $analytic[8];
                        $data[$index]['annotation_click_through_rate'] = $analytic[10];
                        $data[$index]['annotation_close_rate'] = $analytic[11];
                        $data[$index]['annotation_impressions'] = $analytic[12];
                        $data[$index]['click_impressions'] = $analytic[13];
                        $data[$index]['close_impressions'] = $analytic[14];
                        $data[$index]['annotation_click'] = $analytic[15];
                        $data[$index]['annotation_close'] = $analytic[16];
                        $data[$index]['subscribers_gained'] = $analytic[17];
                        $data[$index]['subscribers_lost'] = $analytic[18];
                        $data[$index]['type'] = 'youtube';
                        $data[$index]['user_id'] = auth()->id();
                        $data[$index]['created_at'] = new DateTime;
                        $data[$index]['updated_at'] = $data[$index]['created_at'];
                        //get audience

                        $audience = $analytics->reports->query([
                            'ids'       => 'channel==MINE',
                            'startDate' => $data[$index]['date'],
                            'metrics'   => 'viewerPercentage',
                            'endDate'   => $data[$index]['date'], 
                            'dimensions'=> 'gender'
                        ]);

                        if(!empty($audience->rows)){
                            foreach($audience as $row){
                                if($row[1] == 100){
                                    if($row[0] == 'female'){
                                        $data[$index]['females'] = $row[1];
                                        $data[$index]['males'] = 0;
                                    }elseif($row[0] == 'male'){
                                        $data[$index]['males'] = $row[1];
                                        $data[$index]['females'] = 0; 
                                    }    
                                }else{
                                    if($row[0] == 'female'){
                                        $data[$index]['females'] = $row[1];
                                    }elseif($row[0] == 'male'){
                                        $data[$index]['males'] = $row[1];
                                    }
                                }
                            }
                        } else {
                            //dd($audience->rows);
                            $data[$index]['females'] = 0;
                            $data[$index]['males'] = 0;
                        }
                        
                    }
                    
                    $result = Analytics::insert($data);
                }
                
            } else {
                DB::table('user_token')->insert([
                    'user_id'       => auth()->user()->id,
                    'type'          => 'youtube',
                    'access_token'  => $accessToken['access_token'],
                    'refresh_token' => $accessToken['refresh_token'],
                    'token_type'    => $accessToken['token_type'],
                    'expires_in'    => $accessToken['expires_in'],
                    'created'       => $accessToken['created']
                ]);
                
                $analytics = Analytics::where('type', 'Youtube')->where('user_id', auth()->id())->get();
                
                //insert analytic for the first time
                if($analytics->isEmpty()){
                    $analytics = new \Google_Service_YouTubeAnalytics($client);
                    $metrics = 'views,comments,likes,dislikes,shares,estimatedMinutesWatched,estimatedRedMinutesWatched,averageViewDuration,averageViewPercentage,annotationClickThroughRate,annotationCloseRate,annotationImpressions,annotationClickableImpressions,annotationClosableImpressions,annotationClicks,annotationCloses,subscribersGained,subscribersLost';
                    
                    $result = $analytics->reports->query([
                        'ids'       => 'channel==MINE',
                        'startDate' => Carbon::now()->subQuarter()->toDateString(),
                        'metrics'   => $metrics,
                        'sort'      => 'day',
                        'endDate'   => Carbon::now()->toDateString(), 
                        'dimensions'=> 'day'
                    ]);       

                    $data = [];
                    foreach ($result->rows as $index => $analytic) {
                        
                        $data[$index]['date'] = $analytic[0];
                        $data[$index]['views'] = $analytic[1];
                        $data[$index]['comments'] = $analytic[2];
                        $data[$index]['likes'] = $analytic[3];
                        $data[$index]['dislikes'] = $analytic[4];
                        $data[$index]['shares'] = $analytic[5];
                        $data[$index]['estimate_minutes_watched'] = $analytic[6];
                        $data[$index]['average_view_duration'] = $analytic[8];
                        $data[$index]['annotation_click_through_rate'] = $analytic[10];
                        $data[$index]['annotation_close_rate'] = $analytic[11];
                        $data[$index]['annotation_impressions'] = $analytic[12];
                        $data[$index]['click_impressions'] = $analytic[13];
                        $data[$index]['close_impressions'] = $analytic[14];
                        $data[$index]['annotation_click'] = $analytic[15];
                        $data[$index]['annotation_close'] = $analytic[16];
                        $data[$index]['subscribers_gained'] = $analytic[17];
                        $data[$index]['subscribers_lost'] = $analytic[18];
                        $data[$index]['type'] = 'youtube';
                        $data[$index]['user_id'] = auth()->id();
                        $data[$index]['created_at'] = new DateTime;
                        $data[$index]['updated_at'] = $data[$index]['created_at'];
                        //get audience

                        $audience = $analytics->reports->query([
                            'ids'       => 'channel==MINE',
                            'startDate' => $data[$index]['date'],
                            'metrics'   => 'viewerPercentage',
                            'endDate'   => $data[$index]['date'], 
                            'dimensions'=> 'gender'
                        ]);

                        if(!empty($audience->rows)){
                            foreach($audience as $row){
                                if($row[1] == 100){
                                    if($row[0] == 'female'){
                                        $data[$index]['females'] = $row[1];
                                        $data[$index]['males'] = 0;
                                    }elseif($row[0] == 'male'){
                                        $data[$index]['males'] = $row[1];
                                        $data[$index]['females'] = 0; 
                                    }    
                                }else{
                                    if($row[0] == 'female'){
                                        $data[$index]['females'] = $row[1];
                                    }elseif($row[0] == 'male'){
                                        $data[$index]['males'] = $row[1];
                                    }
                                }
                            }
                        } else {
                            //dd($audience->rows);
                            $data[$index]['females'] = 0;
                            $data[$index]['males'] = 0;
                        }
                        
                    }
                    
                    $result = Analytics::insert($data);
                }
            }

            return redirect()->route('creator.platforms.index');
        }
        
        $token = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'youtube')->first();
        
        if ($token) {
            $token = collect($token)->except(['id', 'user_id', 'type'])->toArray();
            $client->setAccessToken($token);

        } else {
            header('Location: ' . $client->createAuthUrl());
            die();
        }
        
        if($client->isAccessTokenExpired()){
            $token = collect($token)->except(['id', 'user_id', 'type'])->toArray();
            $client->refreshToken($token['refresh_token']);
            $accessToken = $client->getAccessToken();
            DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'youtube')->update([
                'access_token'  => $accessToken['access_token'],
                'refresh_token' => $accessToken['refresh_token'],
                'token_type'    => $accessToken['token_type'],
                'expires_in'    => $accessToken['expires_in'],
                'created'       => $accessToken['created']
            ]);
        }
        
        return redirect()->route('creator.platforms.index');
    }

    public function facebookLogin() {
        $user = Socialite::driver('facebook')->stateless()->user();

        return view('platforms.index')->with(['user'=> $user]);
    }
    // public function facebookLogin() {
    //     $user = Socialite::driver('facebook')->stateless()->user();
    //     return view('platforms.index')->with(['user'=> $user]);
    // }

    public function loginFb(){
        if(!session_id()) {
            session_start();
        }
        $fb = new \Facebook\Facebook([
          'app_id' => '249172948960376', // Replace {app-id} with your app id
          'app_secret' => 'b350b44e60ab545e3f7ed3cfbb2df144',
          'default_graph_version' => 'v2.2',
          ]);

        $helper = $fb->getRedirectLoginHelper();

        // $permissions = ['email']; // Optional permissions
        $loginUrl = $helper->getLoginUrl('https://127.0.0.1:8000/admin/platforms/facebook/callback');

        echo '<a href="' . htmlspecialchars($loginUrl) . '">Log in with Facebook!</a>';
    }

    public function loginDaily(){
        
    }

    public function uploadFacebook() {
        $fb = new \Facebook\Facebook([
            'app_id' => '249172948960376',
            'app_secret' => 'b350b44e60ab545e3f7ed3cfbb2df144',
            'default_graph_version' => 'v2.12',
        ]);

        $data = [
            'title' => 'My Tester Video',
            'description' => 'Testing from 127.0.0.1',
            'source' => $fb->videoToUpload('C:/xampp/htdocs/star/public/uploads/videos/bmb.mp4'),
        ];

        try {
            $response = $fb->post('/me/videos', $data, 'EAADinxkcRHgBAChCfgjKK4xmLJQzSi8Mqwq8ZAZCH8k8WlfZApZAJbBJfiAulRIio1b4C94OtJGJ8TqNaRsKNaLCf7ZAZCobqfV6opFEPMjS2tIF7i9nUSnDoXSbQ4hteo1msmhuJgmXekSp6Ymhdb15ErKtrENPe4MJ6xZBR8FpeUaZBg9iGpXbvXTxgRHvPhDO5kLa4HN15QZDZD');
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
        // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
        // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $graphNode = $response->getGraphNode();
        var_dump($graphNode);

        echo 'Video ID: ' . $graphNode['id'];
    }

    public function connectionCheck($provider){
        
        try {
            $token = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', $provider)->first();
            $connection = Socialite::driver($provider);

            if($provider == 'twitter'){
                $result = $connection->userFromTokenAndSecret($token->access_token, $token->token_secret);
            } else {
                $result = $connection->userFromToken($token->access_token);
            }
            
        } catch (\Exception $e) {
            $result = NULL;
        }
        
        return response()->json([
            'status' => $result ? true : false
        ]);
    }
}

