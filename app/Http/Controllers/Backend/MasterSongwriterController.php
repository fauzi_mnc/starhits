<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use App\Repositories\MasterSongwriterRepository;
use App\Repositories\UserRepository;
use App\DataTables\DspDataTable;
use App\DataTables\MasterSongwriterDataTabel;
use Flash;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Storage;
use Log;
use App\Model\User;
use App\Model\RoleUser;
use App\Model\AccessRole;
use App\Model\MasterLagu;
use App\Model\RoleLagu;
use App\Model\Bank;
//use App\Http\Requests\StoreOrUpdateMasterSongwriterRequest;
use Excel;
use Carbon;
use Auth;
use DB;
use PDF;

class MasterSongwriterController extends Controller
{
    protected $mastersongwriter;
    protected $users;


    public function __construct(MasterSongwriterRepository $mastersongwriter, UserRepository $users)
    {
        $this->mastersongwriter = $mastersongwriter;
        $this->users = $users;
    }
    public function index(){
        $songwriter = User::join('access_role', 'users.id', '=', 'access_role.user_id')
                    ->where(['access_role.role_id'=>'10'])
                    ->orderBy('users.name_master', 'ASC')
                    ->groupBy('users.name_master')
                    ->get();
        return view('masterlagu.songwriter.index', [
        'songwriter'  => $songwriter
        ]);
    }
    public function create(){
        $songwriters = User::join('role_user', 'users.id', '=', 'role_user.user_id')
                        ->where(['role_user.role_id'=>'10'])
                        ->select('users.name')
                        ->orderBy('users.name', 'ASC')
                        ->groupBy('users.name')
                        ->get();
            $banks = Bank::where('active','Y')->orderBy('sort','desc')->orderBy('name','asc')->pluck('name', 'id')->toArray();
        return view('masterlagu.songwriter.create',compact('songwriters','banks'));
    }
    
    public function store(Request $request){
        $songwriter             =   $request->name_songwriter;
        $pencipta               =   explode(',', $songwriter);
        $name_songwriter        =   str_replace([' ', '.', '/', ','], ".", strtolower($songwriter));
        $typemail               =   $request->typemail;
        if(empty($request->email_songwriter)){
            $email_songwriter   =   $name_songwriter.'@starhits.id';
        }else{
            $email_songwriter   =   strtolower($request->email_songwriter);
        }
        if(empty($request->password_songwriter)){
            $password_songwriter            =   123456789012;
        }else{
            $password_songwriter            =   $request->password_songwriter;
        }        
        $role_songwriter        =   10;
        $bank_id                =   $request->bank_id;
        $bank_account_number    =   $request->bank_account_number;
        $bank_holder_name       =   $request->bank_holder_name;
        $bank_location          =   $request->bank_location;
        $npwp                   =   $request->npwp;

        $users_pencipta = User::whereIn('users.name', array($songwriter))->whereIn('users.email', array($email_songwriter))->first();
        //dd($users_pencipta->id);
        if ($songwriter != '') {
            if (empty($users_pencipta)) {
                $in_user_pencipta                       = new User();
                $in_user_pencipta->name                 = $songwriter;
                $in_user_pencipta->name_master          = $songwriter;
                $in_user_pencipta->email                = $email_songwriter;
                $in_user_pencipta->bank_id              = $bank_id;
                $in_user_pencipta->bank_account_number  = $bank_account_number;
                $in_user_pencipta->bank_holder_name     = $bank_holder_name;
                $in_user_pencipta->bank_location        = $bank_location;
                $in_user_pencipta->npwp                 = $npwp;
                $in_user_pencipta->slug                 = str_replace([' ', '.', '/', ','], '', strtolower($songwriter));
                $in_user_pencipta->password             = bcrypt($password_songwriter);
                $in_user_pencipta->save();

                $role_users_pencipta = AccessRole::where([['user_id', '=', $in_user_pencipta->id],['role_id', '=', 10]])->first();
                if (empty($role_users_pencipta)) {
                    $rolePencipta = new AccessRole();
                    $rolePencipta->user_id = $in_user_pencipta->id;
                    $rolePencipta->role_id = 10;
                    $rolePencipta->save();
                }            
            } else {
                Flash::error('Data Master Songwriter Failed Save. Email Already use');
                if (Auth::user()->hasRole('admin')) {
                    return redirect(route('mastersongwriter.index'));
                }elseif (Auth::user()->hasRole('anr')) {
                    return redirect(route('anr.mastersongwriter.index'));
                }else{
                    return redirect(route('legal.mastersongwriter.index'));
                }
            }
        }else{
            Flash::error('Data Master Songwriter Failed Save.');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('mastersongwriter.index'));
            }elseif(Auth::user()->hasRole('anr')) {
                return redirect(route('anr.mastersongwriter.index'));
            }else{
                return redirect(route('legal.mastersongwriter.index'));
            }
        }

        Flash::success('Master Songwriter saved successfully.');
        if (Auth::user()->hasRole('admin')) {
            return redirect(route('mastersongwriter.index'));
        }elseif (Auth::user()->hasRole('anr')) {
            return redirect(route('anr.mastersongwriter.index'));
        }else{
            return redirect(route('legal.mastersongwriter.index'));
        }
    }

    public function edit(Request $request, $id_pencipta){
        $songwriters = User::find($id_pencipta);
        if (empty($songwriters)) {
            Flash::error('Songwriter not found');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('mastersongwriter.index'));
            }elseif (Auth::user()->hasRole('anr')) {
                return redirect(route('anr.mastersongwriter.index'));
            }else{
                return redirect(route('legal.mastersongwriter.index'));
            }
        }
        $banks = Bank::all()->where('active','Y')->pluck('name', 'id')->toArray();
        return view('masterlagu.songwriter.edit', [ 'songwriters'   => $songwriters, 'banks' => $banks ]);
    }

    public function update(Request $request, $id){
        //dd($id);
        $user = User::find($id);
        //dd($user->name);
        
        if (empty($user)) {
            Flash::error('user not found');

            return redirect(route('user.index'));
        }

        $input = $request->except(['_method', '_token']);
        if(empty($request->password_songwriter)){
            
            $user = User::whereId($id)->update([
                'name'          =>   $request->name_songwriter,
                'name_master'   =>   $request->name_songwriter,
                'email'         =>   $request->email_songwriter,
                'bank_id'              =>   $request->bank_id,
                'bank_account_number'  =>   $request->bank_account_number,
                'bank_holder_name'     =>   $request->bank_holder_name,
                'bank_location'        =>   $request->bank_location,
                'npwp'                 =>   $request->npwp
            ]);

        }else{

            $user = User::whereId($id)->update([
                'name'          =>   $request->name_songwriter,
                'name_master'   =>   $request->name_songwriter,
                'email'         =>   $request->email_songwriter,
                'password'      =>   $request->password_songwriter,
                'bank_id'              =>   $request->bank_id,
                'bank_account_number'  =>   $request->bank_account_number,
                'bank_holder_name'     =>   $request->bank_holder_name,
                'bank_location'        =>   $request->bank_location,
                'npwp'                 =>   $request->npwp
            ]);

        }

        Flash::success('Songwriter Updated successfully.');
        //return redirect(route('mastersongwriter.index'));
        if (Auth::user()->hasRole('admin')) {
            return redirect(route('mastersongwriter.index'));
        }elseif (Auth::user()->hasRole('anr')) {
            return redirect(route('anr.mastersongwriter.index'));
        }else{
            return redirect(route('legal.mastersongwriter.index'));
        }
    }

    public function destroy($id){
        $user = User::find($id);
        $deleteuser = User::where('id', $id)->delete();

        if($deleteuser){
            Flash::success('Data Master Songwriter Successfully Delete.');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('mastersongwriter.index'));
            }elseif (Auth::user()->hasRole('anr')) {
                return redirect(route('anr.mastersongwriter.index'));
            }elseif(Auth::user()->hasRole('legal')){
                return redirect(route('legal.mastersongwriter.index'));
            }
        }else{
            Flash::error('Data Master Songwriter Failed Delete.');
            if (Auth::user()->hasRole('admin')) {
                return redirect(route('mastersongwriter.index'));
            }elseif (Auth::user()->hasRole('anr')) {
                return redirect(route('anr.mastersongwriter.index'));
            }elseif(Auth::user()->hasRole('legal')){
                return redirect(route('legal.mastersongwriter.index'));
            }
        }
    }
}
