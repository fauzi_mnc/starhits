<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Controller;
use Illuminate\Http\Request;
use App\Model\Channels;
use App\Model\Serie;
use App\Model\Video;
use App\Model\YoutubePartner;
use Illuminate\Support\Facades\DB;
use App\DataTables\ChannelDataTable;
use App\Http\Requests\StoreOrUpdateChannelRequest;
use Flash;
use Auth;

class ChannelController extends Controller
{
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function index(ChannelDataTable $channelDataTable)
    {   
        $auth = Auth::user()->roles[0]->name;
        $youtubePartner = YoutubePartner::get()->count();
        if($auth == 'user'){
            if($youtubePartner == 0){
                return redirect(route('authentication.network'));
            }
        }
        return $channelDataTable->render('channel.index');
    }

    public function create()
    {
        return view('channel.create');
    }

    public function store(StoreOrUpdateChannelRequest $request) {
        try {
            $record = Channels::orderBy('id', 'desc')->first()->attr_1;
            $input['attr_1'] = $record + 1;
        } catch (\Exception $e) {
            $input['attr_1'] = 1;
        }
        $input['user_id'] = Auth::id();
        $input['type'] = $request->type;
        $input['title'] = $request->title;
        $input['slug'] = rtrim(str_replace(' ', '-', strtolower($request->title)),'-');
        $input['content'] = strip_tags($request->content);
        if ($request->image != NULL) {
            $input['image'] = '/uploads/channel/'.time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/channel'), time().'.'.$request->image->getClientOriginalExtension());
        }
        $input['is_active'] = 1;

        Channels::create($input);
        Flash::success('Channel saved successfully.');

        return redirect(route('channel.index'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    function show($id)
    {
    	$channel = Channels::find($id);

        if (empty($channel)) {
            Flash::error('Channel not found');

            return redirect(route('channel.index'));
        }

        return view('channel.show')->with('channel', $channel);
    }

    function edit($id)
    {
        $channel = Channels::find($id);

        if (empty($channel)) {
            Flash::error('Channel not found');

            return redirect(route('channel.index'));
        }

        return view('channel.edit')->with('channel', $channel);
    }

    public function update($id, StoreOrUpdateChannelRequest $request) {
        $channel = Channels::find($id);
        $channel['user_id'] = Auth::id();
        $channel['type'] = $request->type;
        $channel['title'] = $request->title;
        $channel['content'] = strip_tags($request->content);
        $channel['slug'] = rtrim(str_replace(' ', '-', strtolower($request->title)),'-');
        if ($request->image != NULL) {
            $channel['image'] = '/uploads/channel/'.time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/channel'), time().'.'.$request->image->getClientOriginalExtension());
        }
        $series = Serie::where('parent_id', $channel->attr_1)->get();
        $channel->save();
        Flash::success('Channel update successfully.');

        return redirect(route('channel.index'));
    }

    public function destroy(Request $request,$id)
    {
        if ($request->action == 'act') {
            $channel = Channels::find($id);

            if (empty($channel)) {
                Flash::error('Channel not found');

                return redirect(route('channel.index'));
            }
            $channel['is_active'] = 1;

            $channel->save();

            Flash::success('Channel activated successfully.');
        }else{
            $channel = Channels::find($id);

            if (empty($channel)) {
                Flash::error('Channel not found');

                return redirect(route('channel.index'));
            }
            $channel['is_active'] = 0;
            $series = Serie::where('parent_id', $channel->attr_1)->get();
            foreach ($series as $seri) {
                $ser = Serie::find($seri->id);
                $ser['is_active'] = 0;
                $ser->save();
            }

            $channel->save();

            Flash::success('Channel inactivated successfully.');
        }

        

        return redirect(route('channel.index'));
    }

    function channeltable(){
        // dd('test');
        return view('channel.table');
    }
}
