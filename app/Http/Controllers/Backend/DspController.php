<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use App\Repositories\DspRepository;
use App\Repositories\UserRepository;
use App\DataTables\DspDataTable;
use App\DataTables\MasterLaguDataTabel;
use Flash;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Storage;
use Log;
use App\Model\User;
use App\Model\Dsp;
use App\Model\DspDetails;
use App\Model\RevenueApprover;
use App\Model\MasterLagu;
use App\Http\Requests\StoreDspRequest;
use App\Http\Requests\UpdateDspRequest;
use Excel;
use Carbon;
use Auth;
use DB;
use PDF;

class DspController extends Controller
{

    protected $dsp;
    protected $users;


    public function __construct(DspRepository $dsp, UserRepository $users)
    {
        $this->dsp = $dsp;
    }

    public function index(DspDataTable $dspDatatable)
    {   
        //return $dspDatatable->render('dsp.index');
        if(Auth::user()->hasRole('singer') || Auth::user()->hasRole('songwriter')) {
            $datas = Dsp::where(['status'=>3])->get();
        }else{
            $datas = Dsp::all();
        }
        

        return view('dsp.index',compact("datas"));
    }

    public function create()
    {
        return view('dsp.create');
    }

    public function store(StoreDspRequest $request)
    {
        $rm = date_create($request->reporting_month);
        $reporting_month = date_format($rm,"Y-m-d");
        //dd($reporting_month);
        $dsp_detail = $request->dsp_detail;
        $dspInput = [
            'reporting_month'   => $reporting_month,
        ];
        
        //$KL = Dsp::where(['month'=>$request->month_a, 'year'=>$request->year_a])->first();

        $dsp = $this->dsp->create($dspInput);
        if ($request->type == 1) {
            foreach ($dsp_detail as $dsp_details) {
                $data = explode(',', $dsp_details);
                //dd($data);
                $in_master[] = [
                    'dsp_id'                => $dsp->id,
                    'sales_month'           => date("Y-m-d", strtotime($data[0])),
                    'platform'              => $data[1],
                    'country'               => $data[2],
                    'label_name'            => $data[3],
                    'artist_name'           => $data[4],
                    'release_title'         => $data[5],
                    'track_title'           => $data[6],
                    'upc'                   => $data[7],
                    'isrc'                  => $data[8],
                    'release_catalog_nb'    => $data[9],
                    'release_type'          => $data[10],
                    'sales_type'            => $data[11],
                    'quantity'              => $data[12],
                    'client_pay'            => $data[13],
                    'unit_price'            => $data[14],
                    'mechanic'              => $data[15],
                    'gross_revenue'         => $data[16],
                    'client_share'          => $data[17],
                    'net_revenue_eur'       => $data[18],
                    'net_revenue_idr'       => $data[19],
                ];
            }
            DspDetails::insert($in_master);
        } else {
            foreach ($dsp_detail as $dsp_details) {
                $data = explode(',', $dsp_details);
                //dd($data);
                $in_master[] = [
                    'dsp_id'                => $dsp->id,
                    'sales_month'           => date("Y-m-d", strtotime($data[0])),
                    'platform'              => $data[1],
                    'country'               => $data[2],
                    'label_name'            => $data[3],
                    'artist_name'           => $data[4],
                    'release_title'         => $data[5],
                    'track_title'           => $data[6],
                    'upc'                   => $data[7],
                    'isrc'                  => $data[8],
                    'release_catalog_nb'    => $data[9],
                    'release_type'          => $data[10],
                    'sales_type'            => $data[11],
                    'quantity'              => $data[12],
                    'client_pay'            => $data[13],
                    'unit_price'            => $data[14],
                    'mechanic'              => $data[15],
                    'gross_revenue'         => $data[16],
                    'client_share'          => $data[17],
                    'net_revenue_eur'       => $data[18],
                    'net_revenue_idr'       => $data[19],
                ];
                DspDetails::insert($in_master);
            }
        }
        
        Flash::success('Dsp saved successfully.');
        return redirect(route('dsp.index'));
    }

    /* public function download($id){
        $dsp = $this->dsp->with(['detail'])->find($id);
        return response()->download(public_path($dsp->pdf));
    } */

    public function downloadTry(){
        return view('dsp.template_csv');
    }


    public function createDSP(){
        return view('dsp.create_dsp.insert');
    }

    public function storeDSP(Request $request){
        $rm = date_create($request->reporting_month);
        $reporting_month = date_format($rm,"Y-m-d");
        $dsp = new Dsp();
        $dsp->reporting_month = $reporting_month;
        $dsp->status = 0;
        $dsp->save();
        ini_set('memory_limit', '-1');
        $data = new \stdClass();
        $array_dsp = array();
        if($request->file != NULL){
            $request->file->storeAs('uploads/dsp', $request->file->getClientOriginalName(), 'local_public');
            $file_path = public_path().'/uploads/dsp/'.$request->file->getClientOriginalName();
            
            if($request->file->getClientOriginalExtension() == 'xlsx'){
                $reader = ReaderFactory::create(Type::XLSX); // for XLSX files

                $reader->open($file_path);
                
                foreach ($reader->getSheetIterator() as $sheet) {
                    foreach ($sheet->getRowIterator() as $key => $row) {
                        if($key>1){
                            if(empty($row[0])){
                                break;
                            }
                            $record = new \stdClass();
                            $record_sales_month         = $row[0];
                            $record->sales_month        = $record_sales_month->format('d-m-Y');
                            $record->platform           = $row[1];
                            $record->country            = str_replace(',', ' ', $row[2]);
                            $record->label_name         = $row[3];
                            $record->artist_name        = $row[4];
                            $record->release_title      = $row[5];
                            $record->track_title        = $row[6];
                            $record->upc                = $row[7];
                            $record->isrc               = $row[8];
                            $record->release_catalog_nb = $row[9];
                            $record->release_type       = $row[10];
                            $record->sales_type         = $row[11];

                            if(is_int(intval($row[12]))){
                                $record->quantity = preg_replace("/[^0-9]/","",$row[12]);
                            }else{
                                $record->quantity = 0;
                            }
                            if(is_int(intval($row[13]))){
                                $record->client_pay = preg_replace("/[^0-9]/","",$row[13]);
                            }else{
                                $record->client_pay = 0;
                            }
                            if(is_int(intval($row[14]))){
                                $record->unit_price = preg_replace("/[^0-9]/","",$row[14]);
                            }else{
                                $record->unit_price = 0;
                            }
                            if(is_int(intval($row[15]))){
                                $record->mechanic = preg_replace("/[^0-9]/","",$row[15]);
                            }else{
                                $record->mechanic = 0;
                            }
                            if(is_int(intval($row[16]))){
                                $record->gross_revenue = preg_replace("/[^0-9]/","",$row[16]);
                            }else{
                                $record->gross_revenue = 0;
                            }
                            if(is_int(intval($row[17]))){
                                $record->client_share = preg_replace("/[^0-9]/","",$row[17]);
                            }else{
                                $record->client_share = 0;
                            }
                            if(is_int(intval($row[18]))){
                                $record->net_revenue_eur = $row[18];
                            }else{
                                $record->net_revenue_eur = 0;
                            }
                            if(is_int(intval($row[19]))){
                                $record->net_revenue_idr = $row[19];
                            }else{
                                $record->net_revenue_idr = 0;
                            }
                            array_push($array_dsp, $record);                            
                        }
                    }
                }
            }else {
                $reader = ReaderFactory::create(Type::CSV); // for CSV files
                $reader->open($file_path);
                $file_path = public_path().'/uploads/dsp/'.$request->file->getClientOriginalName();
                $file = fopen($file_path,"r");
                $numb = 0;
                while(! feof($file)){
                    
                    $row = fgetcsv($file);
                    json_encode($row);
                    $cekRow = preg_replace("/[^0-9]/","",$row[0]);
                    $record = new \stdClass();
                    $numb++;
                    if($numb==1){  continue; }
                    $record_sales_month         = $row[0];
                    $record->sales_month        = date('d-m-Y', strtotime($record_sales_month));
                    $record->platform           = $row[1];
                    $record->country            = str_replace(',', ' ', $row[2]);
                    $record->label_name         = $row[3];
                    $record->artist_name        = $row[4];
                    $record->release_title      = $row[5];
                    $record->track_title        = $row[6];
                    $record->upc                = $row[7];
                    $record->isrc               = $row[8];
                    $record->release_catalog_nb = $row[9];
                    $record->release_type       = $row[10];
                    $record->sales_type         = $row[11];
                    if(is_int(intval($row[12]))){
                        $record->quantity = preg_replace("/[^0-9]/","",$row[12]);
                    }else{
                        $record->quantity = 0;
                    }
                    if(is_int(intval($row[13]))){
                        $record->client_pay = preg_replace("/[^0-9]/","",$row[13]);
                    }else{
                        $record->client_pay = 0;
                    }
                    if(is_int(intval($row[14]))){
                        $record->unit_price = preg_replace("/[^0-9]/","",$row[14]);
                    }else{
                        $record->unit_price = 0;
                    }
                    if(is_int(intval($row[15]))){
                        $record->mechanic = preg_replace("/[^0-9]/","",$row[15]);
                    }else{
                        $record->mechanic = 0;
                    }
                    if(is_int(intval($row[16]))){
                        $record->gross_revenue = preg_replace("/[^0-9]/","",$row[16]);
                    }else{
                        $record->gross_revenue = 0;
                    }
                    if(is_int(intval($row[17]))){
                        $record->client_share = preg_replace("/[^0-9]/","",$row[17]);
                    }else{
                        $record->client_share = 0;
                    }
                    if(is_int(intval($row[18]))){
                        $record->net_revenue_eur = preg_replace("/[^0-9]/","",$row[18]);
                    }else{
                        $record->net_revenue_eur = 0;
                    }
                    if(is_int(intval($row[19]))){
                        $record->net_revenue_idr = preg_replace("/[^0-9]/","",$row[19]);
                    }else{
                        $record->net_revenue_idr = 0;
                    }
                    if($record->platform==""){break;}

                    array_push($array_dsp, $record);
                }
                fclose($file);
            }
            $reader->close();
        }
        $dsp_detail = $array_dsp;
        foreach ($dsp_detail as $dsp_details) {
            $in_master = new DspDetails();
            $in_master->dsp_id                = $dsp->id;
            $in_master->sales_month           = date("Y-m-d", strtotime($dsp_details->sales_month));
            $in_master->platform              = $dsp_details->platform;
            $in_master->country               = $dsp_details->country;
            $in_master->label_name            = $dsp_details->label_name;
            $in_master->artist_name           = $dsp_details->artist_name;
            $in_master->release_title         = $dsp_details->release_title;
            $in_master->track_title           = $dsp_details->track_title;
            $in_master->upc                   = $dsp_details->upc;
            $in_master->isrc                  = strtolower(str_replace("-", "", $dsp_details->isrc));
            $in_master->release_catalog_nb    = $dsp_details->release_catalog_nb;
            $in_master->release_type          = $dsp_details->release_type;
            $in_master->sales_type            = $dsp_details->sales_type;
            $in_master->quantity              = $dsp_details->quantity;
            $in_master->client_pay            = $dsp_details->client_pay;
            $in_master->unit_price            = $dsp_details->unit_price;
            $in_master->mechanic              = $dsp_details->mechanic;
            $in_master->gross_revenue         = $dsp_details->gross_revenue;
            $in_master->client_share          = $dsp_details->client_share;
            $in_master->net_revenue_eur       = $dsp_details->net_revenue_eur;
            $in_master->net_revenue_idr       = $dsp_details->net_revenue_idr;
            $in_master->save();
        }
        Flash::success('Dsp saved successfully.');
        return redirect(route('dsp.index'));
    }
    

    public function create_singer(){
        return view('dsp.master.create_master_singer');
    }

    public function store_singer(Request $request){
      
        ini_set('memory_limit', '-1');
        $data = new \stdClass();
        $array_dsp = array();
        if($request->file != NULL){
            $request->file->storeAs('uploads/dsp', $request->file->getClientOriginalName(), 'local_public');
            $file_path = public_path().'/uploads/dsp/'.$request->file->getClientOriginalName();
            
            if($request->file->getClientOriginalExtension() == 'xlsx'){
                $reader = ReaderFactory::create(Type::XLSX); // for XLSX files

                $reader->open($file_path);
                
                foreach ($reader->getSheetIterator() as $sheet) {
                    foreach ($sheet->getRowIterator() as $key => $row) {
                        if($key>1){
                            if(empty($row[0])){
                                break;
                            }
                            $users = User::where('name', 'like', $row[4].'%')->first();
                            $cekMaster = MasterLagu::where(['isrc'=>$row[8]])->first();
                            /*if(empty($users)){ //Jika tidak terdaftar
                                $in_user = new User();
                                $in_user->name = $row[4];
                                $in_user->email = str_replace(" ", ".", strtolower($row[4]))."@starhits.id";
                                $in_user->slug = str_replace(" ", "", strtolower($row[4]));
                                $in_user->password = '123456789012';
                                $in_user->save();
                            }*/
                            if( !empty($users) ){
                                if(empty($cekMaster) ){
                                    $record         = new MasterLagu();;
                                    $record->id_user         = $users->id;
                                    $record->label_name         = $row[3];
                                    $record->release_title      = $row[5];
                                    $record->track_title        = $row[6];
                                    $record->upc                = $row[7];
                                    $record->isrc               = $row[8];
                                    $record->save();
                                }
                                 
                            }                          
                        }
                    }
                }
            }
            $reader->close();
        }
        Flash::success('Dsp saved successfully.');
        return redirect(route('dsp.index'));
    }

    public function index_singer(MasterLaguDataTabel $MLaguDatatable){
        return $MLaguDatatable->render('dsp.master.index_lagu');
    }
    /**
     * Read csv file.
     *
     */

    public function uploadCsv(){
        return view('dsp.upload_csv');
    }

   /* function readExcel Origin*/
    public function readExcel(Request $request){
        ini_set('memory_limit', '-1');
        $data = new \stdClass();
        $array_dsp = array();
        if($request->file != NULL){
            $request->file->storeAs('uploads/dsp', $request->file->getClientOriginalName(), 'local_public');
            $file_path = public_path().'/uploads/dsp/'.$request->file->getClientOriginalName();
            
            if($request->file->getClientOriginalExtension() == 'xlsx'){
                $reader = ReaderFactory::create(Type::XLSX); // for XLSX files

                $reader->open($file_path);
                
                foreach ($reader->getSheetIterator() as $sheet) {
                    foreach ($sheet->getRowIterator() as $key => $row) {
                        if($key>1){
                            if(empty($row[0])){
                                break;
                            }
                            $record = new \stdClass();
                            $record_sales_month         = $row[0];
                            $record->sales_month        = $record_sales_month->format('d-m-Y');
                            $record->platform           = $row[1];
                            $record->country            = str_replace(',', ' ', $row[2]);
                            $record->label_name         = $row[3];
                            $record->artist_name        = $row[4];
                            $record->release_title      = $row[5];
                            $record->track_title        = $row[6];
                            $record->upc                = $row[7];
                            $record->isrc               = $row[8];
                            $record->release_catalog_nb = $row[9];
                            $record->release_type       = $row[10];
                            $record->sales_type         = $row[11];

                            if(is_int(intval($row[12]))){
                                $record->quantity = preg_replace("/[^0-9]/","",$row[12]);
                            }else{
                                $record->quantity = 0;
                            }
                            if(is_int(intval($row[13]))){
                                $record->client_pay = preg_replace("/[^0-9]/","",$row[13]);
                            }else{
                                $record->client_pay = 0;
                            }
                            if(is_int(intval($row[14]))){
                                $record->unit_price = preg_replace("/[^0-9]/","",$row[14]);
                            }else{
                                $record->unit_price = 0;
                            }
                            if(is_int(intval($row[15]))){
                                $record->mechanic = preg_replace("/[^0-9]/","",$row[15]);
                            }else{
                                $record->mechanic = 0;
                            }
                            if(is_int(intval($row[16]))){
                                $record->gross_revenue = preg_replace("/[^0-9]/","",$row[16]);
                            }else{
                                $record->gross_revenue = 0;
                            }
                            if(is_int(intval($row[17]))){
                                $record->client_share = preg_replace("/[^0-9]/","",$row[17]);
                            }else{
                                $record->client_share = 0;
                            }
                            if(is_int(intval($row[18]))){
                                $record->net_revenue_eur = preg_replace("/[^0-9]/","",$row[18]);
                            }else{
                                $record->net_revenue_eur = 0;
                            }
                            if(is_int(intval($row[19]))){
                                $record->net_revenue_idr = preg_replace("/[^0-9]/","",$row[19]);
                            }else{
                                $record->net_revenue_idr = 0;
                            }
                            array_push($array_dsp, $record);                            
                        }
                    }
                }
            }else {
                $reader = ReaderFactory::create(Type::CSV); // for CSV files
                $reader->open($file_path);
                $file_path = public_path().'/uploads/dsp/'.$request->file->getClientOriginalName();
                $file = fopen($file_path,"r");
                $numb = 0;
                while(! feof($file)){
                    
                    $row = fgetcsv($file);
                    json_encode($row);
                    $cekRow = preg_replace("/[^0-9]/","",$row[0]);
                    $record = new \stdClass();
                    $numb++;
                    if($numb==1){  continue; }
                    $record_sales_month         = $row[0];
                    $record->sales_month        = date('d-m-Y', strtotime($record_sales_month));
                    $record->platform           = $row[1];
                    $record->country            = str_replace(',', ' ', $row[2]);
                    $record->label_name         = $row[3];
                    $record->artist_name        = $row[4];
                    $record->release_title      = $row[5];
                    $record->track_title        = $row[6];
                    $record->upc                = $row[7];
                    $record->isrc               = $row[8];
                    $record->release_catalog_nb = $row[9];
                    $record->release_type       = $row[10];
                    $record->sales_type         = $row[11];
                    if(is_int(intval($row[12]))){
                        $record->quantity = preg_replace("/[^0-9]/","",$row[12]);
                    }else{
                        $record->quantity = 0;
                    }
                    if(is_int(intval($row[13]))){
                        $record->client_pay = preg_replace("/[^0-9]/","",$row[13]);
                    }else{
                        $record->client_pay = 0;
                    }
                    if(is_int(intval($row[14]))){
                        $record->unit_price = preg_replace("/[^0-9]/","",$row[14]);
                    }else{
                        $record->unit_price = 0;
                    }
                    if(is_int(intval($row[15]))){
                        $record->mechanic = preg_replace("/[^0-9]/","",$row[15]);
                    }else{
                        $record->mechanic = 0;
                    }
                    if(is_int(intval($row[16]))){
                        $record->gross_revenue = preg_replace("/[^0-9]/","",$row[16]);
                    }else{
                        $record->gross_revenue = 0;
                    }
                    if(is_int(intval($row[17]))){
                        $record->client_share = preg_replace("/[^0-9]/","",$row[17]);
                    }else{
                        $record->client_share = 0;
                    }
                    if(is_int(intval($row[18]))){
                        $record->net_revenue_eur = preg_replace("/[^0-9]/","",$row[18]);
                    }else{
                        $record->net_revenue_eur = 0;
                    }
                    if(is_int(intval($row[19]))){
                        $record->net_revenue_idr = preg_replace("/[^0-9]/","",$row[19]);
                    }else{
                        $record->net_revenue_idr = 0;
                    }
                    if($record->platform==""){break;}

                    array_push($array_dsp, $record);
                }
                fclose($file);
            }
            $reader->close();
        }
        $data = $array_dsp;
        
        /*$response = response()->json([
            'data' => $data
        ]);*/
        return $data;
    }

    public function view($id)
    {   
        $user = Auth::user();
        $dsp = $this->dsp->find($id);          
        if(Auth::user()->hasRole('singer') || Auth::user()->hasRole('songwriter')) {

            $masterlagu =  MasterLagu::with('rolelagu','rolelagu.penyanyi','rolelagu.pencipta')
                            ->whereIn('isrc', function($query) use ($dsp,$user){
                                $query->select('isrc')
                                ->from(with(new DspDetails)->getTable())
                                ->where(['dsp_id'=> $dsp->id]);
                            })->whereHas('rolelagu', function($q){
                                $q->where('role_lagu.id_user', auth()->user()->id);
                            })->get();

            $isrc =  MasterLagu::with('rolelagu','rolelagu.penyanyi','rolelagu.pencipta')
                        ->whereIn('isrc', function($query) use($dsp) {
                            $query->select('isrc')
                            ->from(with(new DspDetails)->getTable())
                            ->where(['dsp_id'=> $dsp->id]);
                        })->whereHas('rolelagu', function($q){
                            $q->where('role_lagu.id_user', auth()->user()->id);
                        })->get()->pluck('isrc')->toArray();

            $totalDSP = DspDetails::where('dsp_id',$dsp->id)
                        ->whereIn('isrc', $isrc)
                        ->sum('net_revenue_idr');

            $allDSP = DspDetails::where(['dsp_id'=>$dsp->id])
                    ->whereIn('isrc', $isrc)
                    ->whereNotIn('isrc', function($query) use ($dsp){
                        $query->select('isrc')
                        ->from(with(new MasterLagu)->getTable())
                        ->where(['dsp_id'=> $dsp->id]);
                    })->groupBy('isrc')
                    ->get();

        }else{
            $masterlagu =  MasterLagu::with('rolelagu','rolelagu.penyanyi','rolelagu.pencipta')
                        ->whereIn('isrc', function($query) use ($dsp){
                            $query->select('isrc')
                            ->from(with(new DspDetails)->getTable())
                            ->where(['dsp_id'=> $dsp->id]);
                        })->get();

            $totalDSP = DspDetails::where(['dsp_id'=>$dsp->id])->sum('net_revenue_idr');

            $allDSP = DspDetails::where(['dsp_id'=>$dsp->id])
                    ->whereNotIn('isrc', function($query) use ($dsp){
                        $query->select('isrc')
                        ->from(with(new MasterLagu)->getTable());
                    })->groupBy('isrc')
                    ->get();
                    
        }

        $penyanyis = User::where('is_active', 1)
                    ->whereHas('accessrole', function($query){
                        $query->where('role_id', '9');
                    })
                    ->orderBy('name', 'asc')->get();

        $penciptas = User::where('is_active', 1)
                    ->whereHas('accessrole', function($query){
                        $query->where('role_id', '10');
                    })
                    ->orderBy('name', 'asc')->get();

        return view('dsp.view', [
            'masterlagu'  => $masterlagu,
            'dsp'   =>$dsp,
            'total_dsp'=>$totalDSP,
            'all_dsp'   =>$allDSP,
            'penyanyis'=>$penyanyis
        ]);

    }



    public function dsp_detail($id,$isrc){

        $dsp_detail = DspDetails::where(['dsp_id'=>$id,'isrc'=>$isrc])
                    ->groupBy('platform')
                    ->get();

        $masterlagu =  MasterLagu::with('rolelagu','rolelagu.penyanyi','rolelagu.pencipta')
            ->whereIn('isrc', function($query) use ($id,$isrc){
                $query->select('isrc')
                ->from(with(new DspDetails)->getTable())
                ->where(['isrc'=>$isrc , 'dsp_id'=>$id]);
            })->first();

        return view('dsp.detail', [
            'dsp_id'      => $id,
            'dsp_detail'  => $dsp_detail,
            'masterlagu'  => $masterlagu,
        ]);
    }

    public function print_detail_lagu($id,$isrc){

        $dsp_detail = DspDetails::where(['isrc'=>$isrc,'dsp_id'=>$id])
                    ->groupBy('platform')
                    ->get();

        $getdetails = DspDetails::where(['isrc'=>$isrc,'dsp_id'=>$id])
                    ->first();

        $masterlagu =  MasterLagu::with('rolelagu','rolelagu.penyanyi','rolelagu.pencipta')
            ->whereIn('isrc', function($query) use ($id,$isrc){
                $query->select('isrc')
                ->from(with(new DspDetails)->getTable())
                ->where(['isrc'=>$isrc , 'dsp_id'=>$id]);
            })->first();

        //return $masterlagu;

        $net_revenue=DspDetails::where(['isrc'=>$isrc,'dsp_id'=>$id])->sum('net_revenue_idr');

        $tabSing = 0;
        $tabSong = 0;
        $tableBank =0;

        foreach ($masterlagu->rolelagu as $key => $value) { //Berdasarkan ID Bank
            if(!empty($value->penyanyi)){
                $cekPenyanyi = $value->penyanyi->bank_id;
                if(!empty($cekPenyanyi)){
                    $tabSing = $tabSing+1;
                }
            }
            if(!empty($value->pencipta)){
                $cekPencipta = $value->pencipta->bank_id;
                if(!empty($cekPencipta)){
                    $tabSong = $tabSong+1;
                }
            }
        }

        /*foreach ($masterlagu->rolelagu as $key => $value) { //berdasarkan flag transfer di id bank
            if(!empty($value->penyanyi)){
                $tabSing = $tabSing+$value->transfer;
            }
            if(!empty($value->pencipta)){
                $tabSong = $tabSong+$value->transfer;
            }
        }*/

        if($tabSing >= $tabSong){
            $tableBank = $tabSing;
        }
        elseif($tabSong >= $tabSing){
            $tableBank = $tabSong;
        }
        
        $table['table_bank'] = $tableBank;
        $table['table_sing'] = $tabSing;
        $table['table_song'] = $tabSong;

        ///return $masterlagu->rolelagu;
        //return $table;

        return view('dsp.print_detail_lagu', [
            'dsp_detail'  => $dsp_detail,
            'masterlagu'  => $masterlagu,
            'getdetails'  => $getdetails,
            'net_revenue'  => $net_revenue,
            'table'        => $table
        ]);
    }

    public function print_fpp_detail_lagu($id,$isrc){

        $dsp_detail = DspDetails::where(['isrc'=>$isrc,'dsp_id'=>$id])
                    ->groupBy('platform')
                    ->get();

        $getdetails = DspDetails::where(['isrc'=>$isrc,'dsp_id'=>$id])
                    ->first();

        $masterlagu =  MasterLagu::with('rolelagu','rolelagu.penyanyi','rolelagu.pencipta')
            ->whereIn('isrc', function($query) use ($id,$isrc){
                $query->select('isrc')
                ->from(with(new DspDetails)->getTable())
                ->where(['isrc'=>$isrc , 'dsp_id'=>$id]);
            })->first();

        //return $masterlagu;

        $net_revenue=DspDetails::where(['isrc'=>$isrc,'dsp_id'=>$id])->sum('net_revenue_idr');

        $tabSing = 0;
        $tabSong = 0;
        $tableBank =0;

        foreach ($masterlagu->rolelagu as $key => $value) { //Berdasarkan ID Bank
            if(!empty($value->penyanyi)){
                $cekPenyanyi = $value->penyanyi->bank_id;
                if(!empty($cekPenyanyi)){
                    $tabSing = $tabSing+1;
                }
            }
            if(!empty($value->pencipta)){
                $cekPencipta = $value->pencipta->bank_id;
                if(!empty($cekPencipta)){
                    $tabSong = $tabSong+1;
                }
            }
        }

        /*foreach ($masterlagu->rolelagu as $key => $value) { //berdasarkan flag transfer di id bank
            if(!empty($value->penyanyi)){
                $tabSing = $tabSing+$value->transfer;
            }
            if(!empty($value->pencipta)){
                $tabSong = $tabSong+$value->transfer;
            }
        }*/

        if($tabSing >= $tabSong){
            $tableBank = $tabSing;
        }
        elseif($tabSong >= $tabSing){
            $tableBank = $tabSong;
        }
        
        $table['table_bank'] = $tableBank;
        $table['table_sing'] = $tabSing;
        $table['table_song'] = $tabSong;

        ///return $masterlagu->rolelagu;
        //return $table;

        return view('dsp.print_fpp_detail_lagu', [
            'dsp_detail'  => $dsp_detail,
            'masterlagu'  => $masterlagu,
            'getdetails'  => $getdetails,
            'net_revenue'  => $net_revenue,
            'table'        => $table
        ]);
    }

    public function print_lagu($id){
        $dsp = $this->dsp->find($id);
        $masterlagu =  MasterLagu::with('rolelagu','rolelagu.penyanyi','rolelagu.pencipta')
                        ->whereIn('isrc', function($query) use ($dsp){
                            $query->select('isrc')
                            ->from(with(new DspDetails)->getTable())
                            ->where('dsp_id', $dsp->id);
                        })
                        ->get();

        $approver = RevenueApprover::where('type', 1)->where('is_active', 1)->first();
        $checker = RevenueApprover::where('type', 2)->where('is_active', 1)->get();

        $allDSP = DspDetails::where(['dsp_id'=>$dsp->id])
                ->whereNotIn('isrc', function($query) use ($dsp){
                    $query->select('isrc')
                    ->from(with(new MasterLagu)->getTable());
                })->groupBy('isrc')
                ->get();
                //->sum('net_revenue_idr');

        if (empty($masterlagu)) {
            Flash::error('DSP Reports not found');
            return redirect(route('dsp.index'));
        }
        
        return view('dsp.print_lagu', [
            'masterlagu'  => $masterlagu,
            'dsp'   =>$dsp,
            'all_dsp'   =>$allDSP,
            'approver'=>$approver,
            'checker'=>$checker,
        ]);
    }

    public function excel($id){
        $dsps = $this->dsp->with(['detail'])->find($id);
        //dd($dsps->reporting_month);
        $reporting_month = date('d F Y', strtotime($dsps->reporting_month));
        $excel = Excel::create('Reporting-Dsp-'.$reporting_month.'', function($excel) use ($id) {

            $excel->sheet('Sheet1', function($sheet) use ($id) {
                $dsp = $this->dsp->with(['detail'])->find($id);
                
                $sum_net_revenue_idr = $dsp->detail->sum(function ($detail) {
                    return $detail->pivot->net_revenue_idr;
                });

                $month = date('d F Y', strtotime($dsp->reporting_month));
                $sheet->loadView('dsp.dsp_calculation_excel', [
                    'dsp' => $dsp,
                    'month' => $month
                ]);
            });
        });
        //dd($excel->download('xlsx'));
        return $excel->export('xlsx');
    }

    public function template(){
        $pathToFile = public_path('/uploads/template/template_upload_dsp_excel.xlsx');
        return response()->download($pathToFile);
    }


    /*Download reporting*/
    public function download_report($id){
        $dsp = $this->dsp->with(['detail'])->find($id);
        
        if (empty($dsp)) {
            Flash::error("Can't Download Excel, Dsp Report not found");
            return redirect(route('dsp.index'));
        }
        
        return view('dsp.report_excell', [
            'dsp'  => $dsp
        ]);
    }


    public function download_report_pdf($id){
        $dsp = $this->dsp->with(['detail'])->find($id);
        $role = auth()->user()->roles->first()->name;

        if (empty($dsp)) {
            Flash::error("Can't Download PDF, Dsp Report not found");
            return redirect(url($role."/dsp/"));
            //return redirect(route('dsp.index'));
        }
        
        return view('dsp.report_pdf', [
            'dsp'  => $dsp
        ]);
    }

    public function status_checked($id){
        $dsp = Dsp::where(['id'=>$id])->first();
        $dsp->status = 1;
        $dsp->save();
        $role = auth()->user()->roles->first()->name;

        if($dsp){

            Flash::success("Success, Dsp Report Was Checked");
            return redirect(url($role."/dsp/"));
        }else{
            Flash::error("Dsp Report Failed to Checked");
            return redirect(url($role."/dsp/"));
        }
       
    }

    /*public function status_revisi($id){
        $dsp = Dsp::where(['id'=>$id])->first();
        $dsp->status = 2;
        $dsp->save();

        if($dsp){
            Flash::success("Success, Dsp Report Was Checked");
            return redirect(route('dsp.index'));
        }else{
            Flash::error("Dsp Report Failed to Checked");
            return redirect(route('dsp.index'));
        }
    }*/

    public function status_finish($id){
        $dsp = Dsp::where(['id'=>$id])->first();
        $dsp->status = 3;
        $dsp->save();
        $role = auth()->user()->roles->first()->name;

        if($dsp){
            Flash::success("Success, Dsp Report Finished");
            return redirect(url($role."/dsp/"));
        }else{
            Flash::error("Dsp Report Change Status Failed");
            return redirect(url($role."/dsp/"));
        }
    }
}

