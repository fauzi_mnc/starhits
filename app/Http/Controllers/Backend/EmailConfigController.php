<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Controller;
use Illuminate\Http\Request;
use App\Model\SiteConfig;
use Illuminate\Support\Facades\DB;
use App\DataTables\EmailConfigDataTable;
use App\Http\Requests\StoreOrUpdateConfigRequest;
use Flash;

class EmailConfigController extends Controller
{
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function index()
    {
        $email_config = SiteConfig::where('key', 'like', '%'.'email_'.'%')->get();
        return view('email_config.create')->with(['email_config'=> $email_config]);
    }

    public function create()
    {
        return view('email_config.create');
    }

    public function store(Request $request) {
        $input = $request->except('_token');
        // dd($input);
        foreach ($input as $key => $value) {
            $email_config = SiteConfig::where('key', $key)->first();
            $email_config['value'] = $value;
            if($email_config->key == 'website_logo_header'){
                $email_config['value'] = '/uploads/email_config/'.time().'.'.$value->getClientOriginalExtension();
                $value->move(public_path('uploads/email_config'), time().'.'.$value->getClientOriginalExtension());
            }
            if (empty($email_config->key == 'is_streaming')) {
                $is_streaming = SiteConfig::where('key', 'is_streaming')->first();
                $is_streaming['value'] = NULL;
                $is_streaming->save();   
            }
            $email_config->save();    
        }
        Flash::success('Email Configuration saved successfully.');

        return redirect(route('email_config.index'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    function show($id)
    {
    	$email_config = SiteConfig::find($id);

        if (empty($email_config)) {
            Flash::error('Config not found');

            return redirect(route('email_config.index'));
        }

        return view('email_config.show')->with(['email_config'=> $email_config]);
    }

    function edit($id)
    {
        $email_config = SiteConfig::find($id);
        if (empty($email_config)) {
            Flash::error('Config not found');

            return redirect(route('email_config.index'));
        }

        return view('email_config.edit')->with(['email_config'=> $email_config]);
    }

    public function update($id, StoreOrUpdateConfigRequest $request) {
        $email_config = SiteConfig::find($id);
        $email_config['key'] = $request->key;
        $email_config['value'] = $request->value;
        
        $email_config->save();

        Flash::success('Email Configuration update successfully.');

        return redirect(route('email_config.index'));
    }
}
