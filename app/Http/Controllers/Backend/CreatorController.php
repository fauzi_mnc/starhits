<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Backend\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repositories\UserRepository;
use App\Repositories\RoleRepository;
use App\DataTables\CreatorDataTable;
use Flash;
use Youtube;
use App\Http\Requests\StoreOrUpdateCreatorRequest;
use App\Http\Requests\UpdateCreatorRequest;
use Storage;
use App\Model\Bank;
use App\Model\User;
use App\Model\Socials;
use App\Model\MasterAsset;
use App\Model\CategoryInst;
use App\Model\AccessRole;

class CreatorController extends Controller
{
    /**
     * The page repository instance.
     */
    protected $users;
    protected $roles;

    /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users, RoleRepository $roles)
    {
        $this->users = $users;
        $this->roles = $roles;
    }

    public function index(CreatorDataTable $creatorDatatable)
    {
        //return $creatorDatatable->render('creator.index');
        $creator = User::whereHas('accessrole', function ($query) {
            $query->where('role_id', '2');
        })->get();

        // $parse = parse_url('https://yt3.ggpht.com/a/AGF-l7_FJT2gLYagvylTeOf8ZC2iVvB7Pk4iHxwscg=s800-c-k-c0xffffffff-no-rj-mo');

        // return $parse['host'];

        return view('creator.index',['creator' => $creator]);
    }

    public function create()
    {
        return view('creator.create', [
        ]);
    }

    public function store(StoreOrUpdateCreatorRequest $request) {

        //dd($request);

        $input = $request->except('_token', 'username_instagram');
        
        if($request->has('cover')){
            $cover = $request->cover->store('uploads/creators/cover', 'local_public');
            $input['cover'] = $cover;
        }
        
        if($request->has('image')){
            $path = 'uploads/creators/image/'.str_random(32).'.png';
            $image = Storage::disk('local_public')->put($path, file_get_contents($input['image']));
            $input['image'] = $path;
        }

        if($request->has('img_header')){
            $path = 'uploads/creators/img_header/'.str_random(32).'.png';
            $image = Storage::disk('local_public')->put($path, file_get_contents($input['img_header']));
            $input['img_header'] = $path;
        }
        if($request->has('img_content')){
            $path = 'uploads/creators/img_content/'.str_random(32).'.png';
            $image = Storage::disk('local_public')->put($path, file_get_contents($input['img_content']));
            $input['img_content'] = $path;
        }
        if($request->has('feature1')){
            $path1 = 'uploads/creators/feature_home/'.str_random(32).'.png';
            $image = Storage::disk('local_public')->put($path1, file_get_contents($input['feature1']));
            $input['feature_home'] = $path1;
        }

        if($request->has('feature2')){
            $path2 = 'uploads/creators/feature_page/'.str_random(32).'.png';
            $image = Storage::disk('local_public')->put($path2, file_get_contents($input['feature2']));
            $input['feature_page'] = $path2;
        }
        

        if($request->is_feature=="on"){
            $input['is_feature_home']=1;
        }else{
            $input['is_feature_home']=0;
        }

        if($request->is_feature_page=="on"){
            $input['is_feature_page']=1;
        }else{
            $input['is_feature_page']=0;
        }

        $input['is_active'] = 1;
        $user = $this->users->create($input);

        MasterAsset::updateOrCreate(
            ['user_id' => $user->id, 'name' => 'ig'],
            ['url'   => $request->input('socials')[0] ?: '']
        );

        Socials::updateOrCreate(
            ['user_id' => $user->id, 'name' => 'ig'],
            ['url'   => $request->input('socials')[0] ?: '']
        );
        Socials::updateOrCreate(
            ['user_id' => $user->id, 'name' => 'fb'],
            ['url'   => $request->input('socials')[1] ?: '']
        );
        Socials::updateOrCreate(
            ['user_id' => $user->id, 'name' => 'twitter'],
            ['url'   => $request->input('socials')[2] ?: '']
        );
        

        $creatorId = $this->roles->findWhere([
            'name'=>'creator'
        ])->first()->id;
        $roles[] = $creatorId;    
        
        if($request->username_instagram){

            $user->detailInfluencer()->create(['username' => $request->username_instagram]);
            
            $influencerId = $this->roles->findWhere([
                'name'=>'influencer'
            ])->first()->id;
            $roles[] = $influencerId;
        }
        
        $user->roles()->sync($roles);

        $roleCreator = new AccessRole();
        $roleCreator->user_id = $user->id;
        $roleCreator->role_id = 2;
        $roleCreator->save();


        Flash::success('Creator saved successfully.');

        return redirect(route('creator.index'));
    }

    public function edit($id)
    {
        $user = User::whereHas('accessrole', function ($query) {
            $query->where('role_id', '2');
        })->get();
        foreach($user AS $u){
            $social = Socials::where('user_id', $u->id)->get();
            dd($social);
        }
        $ig = isset($social[0]) ? $social[0] : false;
        $fb = isset($social[1]) ? $social[1] : false;
        $tw = isset($social[2]) ? $social[2] : false;
        $gp = isset($social[3]) ? $social[3] : false;
        //dd($user->image);
        if (empty($user)) {
            Flash::error('Creator not found');

            return redirect(route('creator.index'));
        }

        return view('creator.edit', [
            'user'  => $user,
            'ig' => $ig,
            'fb' => $fb,
            'tw' => $tw,
            'gp' => $gp,
        ]);
    }

    public function update(UpdateCreatorRequest $request, $id)
    {
        $user = $this->users->find($id);

        if (empty($user)) {
            Flash::error('Creator not found');

            return redirect(route('creator.index'));
        }

        $input = $request->except(['_method', '_token']);
       // dd($input);

        if($request->is_feature=="on"){
            $input['is_feature_home']=1;
        }else{
            $input['is_feature_home']=0;
        }

        if($request->is_feature_page=="on"){
            $input['is_feature_page']=1;
        }else{
            $input['is_feature_page']=0;
        }

        if($request->file('cover')){
            $cover = $request->cover->store('uploads/creators/cover', 'local_public');
            $input['cover'] = $cover;
        }

        if($request->file('image')){
            $path = 'uploads/creators/image/'.str_random(32).'.png';
            $image = Storage::disk('local_public')->put($path, file_get_contents($input['image']));
            $input['image'] = $path;
        }

        if($request->has('img_header')){
            $path = 'uploads/creators/img_header/'.str_random(32).'.png';
            $image = Storage::disk('local_public')->put($path, file_get_contents($input['img_header']));
            $input['img_header'] = $path;
        }
        if($request->has('img_content')){
            $path = 'uploads/creators/img_content/'.str_random(32).'.png';
            $image = Storage::disk('local_public')->put($path, file_get_contents($input['img_content']));
            $input['img_content'] = $path;
        }
        if($request->has('feature1')){
            $path1 = 'uploads/creators/feature_home/'.str_random(32).'.png';
            $image = Storage::disk('local_public')->put($path1, file_get_contents($input['feature1']));
            $input['feature_home'] = $path1;
        }

        if($request->has('feature2')){
            $path2 = 'uploads/creators/feature_page/'.str_random(32).'.png';
            $image = Storage::disk('local_public')->put($path2, file_get_contents($input['feature2']));
            $input['feature_page'] = $path2;
        }
        

        //dd($input);

        Socials::updateOrCreate(
            ['user_id' => $id, 'name' => 'ig'],
            ['url'   => $request->input('socials')[0] ?: '']
        );
        Socials::updateOrCreate(
            ['user_id' => $id, 'name' => 'fb'],
            ['url'   => $request->input('socials')[1] ?: '']
        );
        Socials::updateOrCreate(
            ['user_id' => $id, 'name' => 'twitter'],
            ['url'   => $request->input('socials')[2] ?: '']
        );
    
        
        $user = $this->users->update($input, $id);
        
        if($request->username_instagram){
            if(!$user->detailInfluencer){
                $user->detailInfluencer()->create(['username' => $request->username_instagram]);
            } else {
                $user->detailInfluencer()->update(['username' => $request->username_instagram]);
            }
        }

        Flash::success('Creator updated successfully.');

        return redirect(route('creator.index'));
    }

    public function checkYoutube(Request $request){
        $url = $request->input('url');
        $url = rtrim($url,"/");
        $uri_parts = explode('/', $url);
        $uri_tail = end($uri_parts);
        if(empty(Youtube::getChannelById($uri_tail))){
            $channel = Youtube::getChannelByName($uri_tail);
        }else{
            $channel = Youtube::getChannelById($uri_tail);
        }

        return response()->json([
           'result' => $channel
        ]);
    }

    public function setting($id, $context)
    {
        $user = $this->users->with(['detailInfluencer'])->find($id);
        //return $user;
        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();
        $categories = CategoryInst::all()->pluck('title')->toArray();
        $banks = Bank::all()->pluck('name', 'id')->toArray();
        $social = Socials::where('user_id', $id)->get();
        $ig = isset($social[0]) ? $social[0] : false;
        $fb = isset($social[1]) ? $social[1] : false;
        $tw = isset($social[2]) ? $social[2] : false;
        $gp = isset($social[3]) ? $social[3] : false;

        if (empty($user)) {
            Flash::error('Creator not found');

            return redirect(route('creator.index'));
        }
        
        return view('creator.setting', [
            'ig'          => $ig,
            'fb'          => $fb,
            'tw'          => $tw,
            'gp'          => $gp,
            'user'        => $user,
            'countries'   => $countries,
            'cities'      => $cities,
            'banks'       => $banks,
            'context'     => $context,
            'categories'  => array_combine($categories,$categories)
        ]);
    }

    public function updateSetting(Request $request, $id, $context = null)
    {
        $user = $this->users->find($id);
        
        if (empty($user)) {
            Flash::error('Influencer not found');

            return redirect(route('creator.index'));
        }

        if($request->has('socials')){
            Socials::updateOrCreate(
                ['user_id' => $id, 'name' => 'ig'],
                ['url'   => $request->input('socials')[0] ?: '']
            );
            Socials::updateOrCreate(
                ['user_id' => $id, 'name' => 'fb'],
                ['url'   => $request->input('socials')[1] ?: '']
            );
            Socials::updateOrCreate(
                ['user_id' => $id, 'name' => 'twitter'],
                ['url'   => $request->input('socials')[2] ?: '']
            );
            Socials::updateOrCreate(
                ['user_id' => $id, 'name' => 'gplus'],
                ['url'   => $request->input('socials')[3] ?: '']
            );

            if($request->input('socials')[0]){
                if(!$user->hasRole('influencer')){
                    $influencerId = $this->roles->findWhere([
                        'name'=>'influencer'
                    ])->first()->id;
                    $user->roles()->attach($influencerId);
                }
                
                $user->detailInfluencer()->update(['username' => substr($request->input('socials')[0], strrpos($request->input('socials')[0], "/"))]);
            }
            Flash::success('Creator updated successfully.');

            return redirect(route('self.creator.edit', [auth()->id(), $context]));    
        }
        
        $userInput = $request->only([
            'name',
            'phone',
            'country',
            'password',
            'category',
            'email',
            'gender',
            'city',
            'is_active'
        ]);
        $influencerDetail = $request->only([
            'category',
            'instagram_photo_posting_rate',
            'instagram_video_posting_rate',
            'instagram_story_posting_rate',
            'youtube_posting_rate',
            'ig_highlight_rate',
            'package_rate',
            'bank',
            'bank_account_number',
            'bank_holder_name',
            'bank_location',
            'npwp',
            'username',
            'followers_ig',
            'profile_picture_ig'
        ]);
        
        if($request->has('category')){
            $influencerDetail['category'] = implode(',', $request->category);
        }
        
        if(!empty($userInput)){
            $userInput['is_active'] =  ($request->is_active == 'on') ? 1 : 0;
            $user = $this->users->update($userInput, $id);
        }
        
        if(!empty($influencerDetail)){
            $user->detailInfluencer()->update($influencerDetail);
        }

        Flash::success('Creator updated successfully.');

        if($context != null){
            return redirect(route('self.creator.edit', [auth()->id(), $context]));    
        }

        return redirect(route('creator.index'));
    }
}
