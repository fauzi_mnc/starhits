<?php
namespace App\Http\Controllers\Backend;


use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use App\Model\User;
use App\Model\UserGroup;
use App\Model\UserToken;
use App\Model\YoutubeAnalitic;
use App\Model\YoutubeAnaliticLink;
use App\Model\YoutubeVideo;
use App\Model\YoutubePartner;
use App\Model\YoutubeChannel;
use App\Model\YoutubeAnaliticVideo;
use App\Model\VideoUpload;
use App\Model\YoutubeAudit;
use App\Model\YoutubeAuditChannel;
use App\Model\YoutubeUpdateContentId;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Storage;
use Log;
use Excel;
use Carbon;
use Auth;
use DB;
use PDF;
use File;

class VideoYTController extends Controller
{

    public function index()
    {
        $datas =  VideoUpload::groupBy('datetime')->get();

       // return $datas;

        return view('youtube.video_report.index',compact('datas'));
    }

    public function create()
    {
        $partner = DB::table('youtube_partner')->get();

        return view('youtube.video_report.create',compact('partner'));
    }

    public function upload(Request $request){

        $yt_report   =   $request->yt_report;
        // $path = public_path().'/uploads/videos/video_report/'.date("dFY");
        $path = public_path().'/uploads/videos/video_report/05March2020';
        if(!is_dir($path)) {
            File::makeDirectory($path, 0777, true, true);
        }

        $content_id = $request->content_id;
        foreach ($yt_report as $key => $yt_report) {
        
            $yt_report->move($path.'/', $yt_report->getClientOriginalName());

            VideoUpload::insert([
                'content_id'      => $content_id[$key],
                // 'datetime'        => date("Y-m-d"),
                'datetime'        => "2020-03-05",
                'filename'        => $yt_report->getClientOriginalName(),
                // 'url_file'        =>"http://starhits.com/uploads/videos/video_report/".date("dFY")."/".$yt_report->getClientOriginalName(),
                'url_file'        =>"https://starhits.id/uploads/videos/video_report/05March2020/".$yt_report->getClientOriginalName(),
                // 'group_upload'    => date("dFY"),
                'group_upload'    => "05March2020",
                'status_proses'   => "Pending",
                // 'created_at'      => date("Y-m-d H:i:s"),
                // 'updated_at'      => date("Y-m-d H:i:s"),
                'created_at'      => "2020-03-05 10:40:00",
                'updated_at'      => "2020-03-05 10:40:00",
            ]);
        }

        return redirect(route('youtube.audits.report.index'));
    }

    public function file($id){
        $datas= VideoUpload::select('youtube_partner.display_name','youtube_partner.content_id','youtube_video_uploads.*')
        ->join('youtube_partner', 'youtube_video_uploads.content_id', '=', 'youtube_partner.content_id')
        ->where(['youtube_video_uploads.datetime'=>$id])->get();
        //return $datas;

        return view('youtube.video_report.files.index',compact('datas'));
    }


    public function store(Request $request)
    {
        ini_set('memory_limit', '-1');
        $data = new \stdClass();
        $array_cms = array();

        if($request->file != NULL){
            $request->file->storeAs('uploads/videos', $request->file->getClientOriginalName(), 'local_public');
            $file_path = public_path().'/uploads/videos/'.$request->file->getClientOriginalName();

            if($request->file->getClientOriginalExtension() == 'xlsx'){
                $reader = ReaderFactory::create(Type::XLSX); // for XLSX files

                $reader->open($file_path);
                foreach ($reader->getSheetIterator() as $sheet) {
                    foreach ($sheet->getRowIterator() as $key => $row) {
                        if($key>1){
                            if(empty($row[0])){
                                break;
                            }
                            $record  = new \stdClass();
                            $record->video_id  = $row[0];
                            $record->channel_id  = $row[1];
                            $record->channel_display_name  = $row[2];
                            $record->time_uploaded  = $row[3];
                            $record->time_published  = $row[4];
                            $record->video_title  = $row[5];
                            $record->video_length  = $row[6];
                            $record->views  = $row[7];
                            $record->comments  = $row[8];
                            $record->video_privacy_status  = $row[9];
                            $record->video_url  = $row[10];
                            $record->category  = $row[11];
                            $record->embedding_allowed  = $row[12];
                            $record->ratings_allowed  = $row[13];
                            $record->comments_allowed  = $row[14];
                            $record->claim_origin  = $row[15];
                            $record->content_type  = $row[16];
                            $record->upload_source  = $row[17];
                            $record->claimed_by_this_owner  = $row[18];
                            $record->claimed_by_another_owner  = $row[19];
                           // $record->other_owners_claiming  = $row[20];
                            $record->offweb_syndicatable  = $row[21];
                            $record->claim_id  = $row[22];
                            $record->asset_id  = $row[23];
                            $record->custom_id  = $row[24];
                            $record->effective_policy  = $row[25];
                            $record->third_party_video_id  = $row[26];
                            $record->in_video_ads_enabled  = $row[27];
                            $record->third_party_ads_enabled  = $row[28];
                            $record->display_ads_enabled  = $row[29];
                            $record->sponsored_cards_enabled  = $row[30];
                            $record->overlay_ads_enabled  = $row[31];
                            $record->nonskippable_video_ads_enabled  = $row[32];
                            $record->long_nonskippable_video_ads_enabled  = $row[33];
                            $record->skippable_video_ads_enabled  = $row[34];
                            $record->prerolls_enabled  = $row[35];
                            $record->midrolls_enabled  = $row[36];
                            $record->postrolls_enabled  = $row[37];
                            $record->isrc  = $row[38];
                            $record->eidr  = $row[39];
                            array_push($array_cms, $record);
                        }
                    }
                }
                $reader->close();
            }else {
                $reader = ReaderFactory::create(Type::CSV); // for CSV files
                $reader->open($file_path);
                $file_path = public_path().'/uploads/videos/'.$request->file->getClientOriginalName();
                $file = fopen($file_path,"r");
                $numb = 0;
                while(! feof($file)){
                    $row = fgetcsv($file);
                    json_encode($row);
                    // $cekRow = preg_replace("/[^0-9]/","",$row[0]);
                    $record = new \stdClass();
                    $numb++;
                    if($numb==1){  continue; }
                    $record  = new \stdClass();
                    $record->video_id  = $row[0];
                    $record->channel_id  = $row[1];
                    $record->channel_display_name  = $row[2];
                    $record->time_uploaded  = $row[3];
                    $record->time_published  = $row[4];
                    $record->video_title  = $row[5];
                    $record->video_length  = $row[6];
                    $record->views  = $row[7];
                    $record->comments  = $row[8];
                    $record->video_privacy_status  = $row[9];
                    $record->video_url  = $row[10];
                    $record->category  = $row[11];
                    $record->embedding_allowed  = $row[12];
                    $record->ratings_allowed  = $row[13];
                    $record->comments_allowed  = $row[14];
                    $record->claim_origin  = $row[15];
                    $record->content_type  = $row[16];
                    $record->upload_source  = $row[17];
                    $record->claimed_by_this_owner  = $row[18];
                    $record->claimed_by_another_owner  = $row[19];
                    //$record->other_owners_claiming  = $row[20];
                    $record->offweb_syndicatable  = $row[21];
                    $record->claim_id  = $row[22];
                    $record->asset_id  = $row[23];
                    $record->custom_id  = $row[24];
                    $record->effective_policy  = $row[25];
                    $record->third_party_video_id  = $row[26];
                    $record->in_video_ads_enabled  = $row[27];
                    $record->third_party_ads_enabled  = $row[28];
                    $record->display_ads_enabled  = $row[29];
                    $record->sponsored_cards_enabled  = $row[30];
                    $record->overlay_ads_enabled  = $row[31];
                    $record->nonskippable_video_ads_enabled  = $row[32];
                    $record->long_nonskippable_video_ads_enabled  = $row[33];
                    $record->skippable_video_ads_enabled  = $row[34];
                    $record->prerolls_enabled  = $row[35];
                    $record->midrolls_enabled  = $row[36];
                    $record->postrolls_enabled  = $row[37];
                    $record->isrc  = $row[38];
                    $record->eidr  = $row[39];
                    array_push($array_cms, $record);
                }
                fclose($file);
            }
            $reader->close();

            $videoInsert = $array_cms;

            foreach ($videoInsert as $data_video) {
                $cekVideo = YoutubeVideo::where(['video_id' => $data_video->video_id])->first();

                if(empty($cekVideo)){
                    DB::table('youtube_video')->insert([
                        'video_id' => $data_video->video_id,
                        //'content_id' => $data_video->video_id,
                        'channel_id' => $data_video->channel_id,
                        'channel_display_name' => $data_video->channel_display_name,
                        'time_uploaded' => $data_video->time_uploaded,
                        'time_published' => $data_video->time_published,
                        'video_title' => $data_video->video_title,
                        'video_length' => $data_video->video_length,
                        'views' => $data_video->views,
                        'comments' => $data_video->comments,
                        'video_privacy_status' => $data_video->video_privacy_status,
                        'video_url' => $data_video->video_url,
                        'category' => $data_video->category,
                        'embedding_allowed' => $data_video->embedding_allowed,
                        'ratings_allowed' => $data_video->ratings_allowed,
                        'comments_allowed' => $data_video->comments_allowed,
                        'claim_origin' => $data_video->claim_origin,
                        'content_type' => $data_video->content_type,
                        'upload_source' => $data_video->upload_source,
                        'claimed_by_this_owner' => $data_video->claimed_by_this_owner,
                        //'other_owners_claiming' => $data_video->other_owners_claiming,
                        'offweb_syndicatable' => $data_video->offweb_syndicatable,
                        'claim_id' => $data_video->claim_id,
                        'asset_id' => $data_video->asset_id,
                        'custom_id' => $data_video->custom_id,
                        'effective_policy' => $data_video->effective_policy,
                        'third_party_video_id' => $data_video->third_party_video_id,
                        'in_video_ads_enabled' => $data_video->in_video_ads_enabled,
                        'third_party_ads_enabled' => $data_video->third_party_ads_enabled,
                        'display_ads_enabled' => $data_video->display_ads_enabled,
                        'sponsored_cards_enabled' => $data_video->sponsored_cards_enabled,
                        'overlay_ads_enabled' => $data_video->overlay_ads_enabled,
                        'nonskippable_video_ads_enabled' => $data_video->nonskippable_video_ads_enabled,
                        'long_nonskippable_video_ads_enabled' => $data_video->long_nonskippable_video_ads_enabled,
                        'skippable_video_ads_enabled' => $data_video->skippable_video_ads_enabled,
                        'prerolls_enabled' => $data_video->prerolls_enabled,
                        'midrolls_enabled' => $data_video->midrolls_enabled,
                        'postrolls_enabled' => $data_video->postrolls_enabled,
                        'isrc' => $data_video->isrc,
                        'eidr' => $data_video->eidr,
                    ]);
                }
            }
        }

    }

    public function consoleReadCsv(){
        header('Content-Type: text/html; charset=UTF-8');
        mb_internal_encoding('utf8');
        ini_set('memory_limit', '-1');
        $data = new \stdClass();
        $array_cms = array();
        //$file_path = public_path().'/uploads/videos/'.$request->file->getClientOriginalName();
        $reader = ReaderFactory::create(Type::CSV); // for CSV files
        // $videos_upload = VideoUpload::where(['datetime'=> date("Y-m-d") ])->get();
        $videos_upload = VideoUpload::where(['datetime'=> '2020-03-05' ])->get();

        // return count($videos_upload);

        if( count($videos_upload) !=0 ){
            foreach ($videos_upload as $key => $videos_upload) {

                // DB::table('youtube_video')
                // ->where(['content_id'=> $videos_upload->content_id])->delete();

                VideoUpload::where('idvideo_uploads', $videos_upload->idvideo_uploads)
                ->update(['status_proses' => "In Process"]);


                // $filepath= public_path()."/uploads/videos/video_report/".date("dFY")."/".$videos_upload->filename;
                $filepath= public_path()."/uploads/videos/video_report/05March2020/".$videos_upload->filename;
                $reader->open($filepath);
                
                $file = fopen($filepath,"r");    
                $numb = 0;
                while(!feof($file)){

                    $row = fgetcsv($file);
                    // $row = str_getcsv($file);
                    json_encode($row);
                    // $cekRow = preg_replace("/[^0-9]/","",$row[0]);
                    $record = new \stdClass();
                    $numb++;
                    if($numb==1){ continue; }

                    $record  = new \stdClass();
                    $record->idvideo_upload = $videos_upload->idvideo_uploads;
                    $record->video_id  = isset($row[0]) ? $row[0]:0;
                    $record->content_id  = $videos_upload->content_id;
                    $record->channel_id  = isset($row[1]) ? $row[1]:0;
                    $record->channel_display_name  = isset($row[2]) ? $row[2]:0;
                    $record->time_uploaded  = isset($row[3]) ? $row[3]:0;
                    $record->time_published  = isset($row[4]) ? $row[4]:0;
                    $record->video_title  = isset($row[5]) ? $row[5]:0;
                    $record->video_length  =isset($row[6]) ? $row[6]:0;
                    $record->views  = isset($row[7]) ? $row[7]:0;
                    $record->comments  = isset($row[8]) ? $row[8]:0;
                    $record->video_privacy_status  = isset($row[9]) ? $row[9]:0;
                    $record->video_url  = isset($row[10]) ? $row[10]:0;
                    $record->category  = isset($row[11]) ? $row[11]:0;
                    $record->embedding_allowed  = isset($row[12]) ? $row[12]:0;
                    $record->ratings_allowed  = isset($row[13]) ? $row[13]:0;
                    $record->comments_allowed  = isset($row[14]) ? $row[14]:0;
                    $record->claim_origin  = isset($row[15]) ? $row[15]:0;
                    $record->content_type  = isset($row[16]) ? $row[16]:0;
                    $record->upload_source  = isset($row[17]) ? $row[17]:0;
                    $record->claimed_by_this_owner  = isset($row[18]) ? $row[18]:0;
                    $record->claimed_by_another_owner  =  isset($row[19]) ? $row[19]:0;
                    $record->other_owners_claiming  = isset($row[20]) ? $row[20]:0;
                    $record->offweb_syndicatable  = isset($row[21]) ? $row[21]:0;
                    $record->claim_id  = isset($row[22]) ? $row[22]:0;
                    $record->asset_id  = isset($row[23]) ? $row[23]:0;
                    $record->custom_id  = isset($row[24]) ? $row[24]:0;
                    $record->effective_policy  = isset($row[25]) ? $row[25]:0;
                    $record->third_party_video_id  = isset($row[26]) ? $row[26]:0;
                    $record->in_video_ads_enabled  = isset($row[27]) ? $row[27]:0;
                    $record->third_party_ads_enabled  = isset($row[28]) ? $row[28]:0;
                    $record->display_ads_enabled  = isset($row[29]) ? $row[29]:0;
                    $record->sponsored_cards_enabled  = isset($row[30]) ? $row[30]:0;
                    $record->overlay_ads_enabled  =isset($row[31]) ? $row[31]:0;
                    $record->nonskippable_video_ads_enabled  = isset($row[32]) ? $row[32]:0;
                    $record->long_nonskippable_video_ads_enabled  = isset($row[33]) ? $row[33]:0;
                    $record->skippable_video_ads_enabled  = isset($row[34]) ? $row[34]:0;
                    $record->prerolls_enabled  = isset($row[35]) ? $row[35]:0;
                    $record->midrolls_enabled  = isset($row[36]) ? $row[36]:0;
                    $record->postrolls_enabled  = isset($row[37]) ? $row[37]:0;
                    $record->isrc  = isset($row[38]) ? $row[38]:0;
                    $record->eidr  = isset($row[39]) ? $row[39]:0;
                    array_push($array_cms, $record);
                }
                $videoInsert = $array_cms;
            }
            fclose($file);
            $reader->close();

            for ($i=0; $i < count($videoInsert); $i++) {
                if(!empty($videoInsert[$i]->video_id)){
                    $insert = DB::table('youtube_video')->insert([
                        'idvideo_upload' => $videoInsert[$i]->idvideo_upload,
                        'video_id' => $videoInsert[$i]->video_id,
                        'content_id' => $videoInsert[$i]->content_id,
                        'channel_id' => $videoInsert[$i]->channel_id,
                        'channel_display_name' => $videoInsert[$i]->channel_display_name,
                        'time_uploaded' => $videoInsert[$i]->time_uploaded,
                        'time_published' => $videoInsert[$i]->time_published,
                        'video_title' => $videoInsert[$i]->video_title,
                        'video_length' => $videoInsert[$i]->video_length,
                        'views' => $videoInsert[$i]->views,
                        'comments' => $videoInsert[$i]->comments,
                        'video_privacy_status' => $videoInsert[$i]->video_privacy_status,
                        'video_url' => $videoInsert[$i]->video_url,
                        'category' => $videoInsert[$i]->category,
                        'embedding_allowed' => $videoInsert[$i]->embedding_allowed,
                        'ratings_allowed' => $videoInsert[$i]->ratings_allowed,
                        'comments_allowed' => $videoInsert[$i]->comments_allowed,
                        'claim_origin' => $videoInsert[$i]->claim_origin,
                        'content_type' => $videoInsert[$i]->content_type,
                        'upload_source' => $videoInsert[$i]->upload_source,
                        'claimed_by_this_owner' => $videoInsert[$i]->claimed_by_this_owner,
                        'claimed_by_another_owner' => $videoInsert[$i]->claimed_by_another_owner,
                        'other_owners_claiming' => $videoInsert[$i]->other_owners_claiming,
                        'offweb_syndicatable' => $videoInsert[$i]->offweb_syndicatable,
                        'claim_id' => $videoInsert[$i]->claim_id,
                        'asset_id' => $videoInsert[$i]->asset_id,
                        'custom_id' => $videoInsert[$i]->custom_id,
                        'effective_policy' => $videoInsert[$i]->effective_policy,
                        'third_party_video_id' => $videoInsert[$i]->third_party_video_id,
                        'in_video_ads_enabled' => $videoInsert[$i]->in_video_ads_enabled,
                        'third_party_ads_enabled' => $videoInsert[$i]->third_party_ads_enabled,
                        'display_ads_enabled' => $videoInsert[$i]->display_ads_enabled,
                        'sponsored_cards_enabled' => $videoInsert[$i]->sponsored_cards_enabled,
                        'overlay_ads_enabled' => $videoInsert[$i]->overlay_ads_enabled,
                        'nonskippable_video_ads_enabled' => $videoInsert[$i]->nonskippable_video_ads_enabled,
                        'long_nonskippable_video_ads_enabled' => $videoInsert[$i]->long_nonskippable_video_ads_enabled,
                        'skippable_video_ads_enabled' => $videoInsert[$i]->skippable_video_ads_enabled,
                        'prerolls_enabled' => $videoInsert[$i]->prerolls_enabled,
                        'midrolls_enabled' => $videoInsert[$i]->midrolls_enabled,
                        'postrolls_enabled' => $videoInsert[$i]->postrolls_enabled,
                        'isrc' => $videoInsert[$i]->isrc,
                        'eidr' => $videoInsert[$i]->eidr,
                        // 'time_process' => date("Y-m-d"),
                        'time_process' => '2020-03-05',
                    ]);
                }
            }
            // VideoUpload::where('datetime', date("Y-m-d"))->update(['status_proses' => "Finished"]);
            VideoUpload::where('datetime', '2020-03-05')->update(['status_proses' => "Finished"]);

        }else{
            return "Null";
        }
    }


    public function Bcxz(){

        /*$cmsID = DB::table('youtube_video')
                ->select('youtube_video.content_id')
                ->join('youtube_channel', 'youtube_video.channel_id', '=', 'youtube_channel.channel_id')
                ->groupBy('youtube_channel.content_id')
                ->get();*/

        /*$count = DB::table('youtube_video')
                ->where('time_process','=',date("Y-m-d"))
                ->where('display_ads_enabled','!=','Yes')
                ->where('channel_id','=','UCmp6P-RWbAoJadTi_iZpHJA')
                ->count();*/

        //$videos_upload = VideoUpload::where(['datetime'=> date("Y-m-d") ])->get();
        // $cmsID = DB::table('youtube_video')->groupBy('content_id')->get();
        //$channels = DB::table('youtube_channel')->where(['channel_id'=>])->first();
        //dd($cmsID);

        // $date = date("Y-m-d");
        $date = '2020-03-05';

        //if( count($videos_upload) !=0 ){
            //loop CMS
        $cmsID = YoutubeVideo::where('time_process','=',$date)->groupBy('content_id')->get();
        foreach ($cmsID as $cmsID) {
            
            $CekCMSNull = VideoUpload::where([
                            // 'datetime'=> date("Y-m-d"),
                            'datetime'=> $date,
                            'content_id'=>$cmsID->content_id
                        ])->first();

            if(!empty($CekCMSNull)){
                $total_video = YoutubeVideo::where(['content_id'=>$cmsID->content_id, 'time_process'=>$date])->where('video_id','!=','0')->count();

                $activeMonetez = DB::table('youtube_video')
                                ->where('time_process','=',$date)
                                ->where('claimed_by_another_owner','=','No')
                                ->where('in_video_ads_enabled','=','Yes')
                                ->where('content_id','=',$cmsID->content_id)
                                ->count();
                                
                $notactiveMonetez = DB::table('youtube_video')
                                ->where('time_process','=',$date)
                                ->where(['content_id'=>$cmsID->content_id])
                                ->where('claimed_by_another_owner','=','No')
                                ->where(function($q) {
                                    return $q
                                    ->where('in_video_ads_enabled','=','No')
                                    ->orWhere('in_video_ads_enabled','=',' ');
                                })
                                ->count();

                $copyright_claim = DB::table('youtube_video')
                                ->where('time_process','=',$date)
                                ->where(['claimed_by_another_owner'=>'Yes',
                                    'content_id'=>$cmsID->content_id
                                ])->count();

                $split_monetized = DB::table('youtube_video')
                                ->where('time_process','=',$date)
                                ->where([
                                    'claimed_by_this_owner'=>'Yes',
                                    'claimed_by_another_owner'=>'Yes',
                                    'content_id'=>$cmsID->content_id
                                ])->count();

                $No_display_ads_enabled = $this->CheckAds('display_ads_enabled',$cmsID->content_id);
                $No_overlay_ads_enabled = $this->CheckAds('overlay_ads_enabled',$cmsID->content_id);
                $No_sponsored_cards_enabled = $this->CheckAds('sponsored_cards_enabled',$cmsID->content_id);
                $No_skippable_video_ads_enabled = $this->CheckAds('skippable_video_ads_enabled', $cmsID->content_id);
                $No_nonskippable_video_ads_enabled = $this->CheckAds('nonskippable_video_ads_enabled', $cmsID->content_id);

                $blank = $this->findAudit('',$cmsID->content_id) ;
                $block = $this->findAudit('Block',$cmsID->content_id);
                $track = $this->findAudit('Track',$cmsID->content_id);
                $monetezOwner = $this->findAudit('Monetize',$cmsID->content_id);

                $cekAudit = DB::table('youtube_video_audit_cms')
                            ->where('content_id','=',$cmsID->content_id)
                            // ->groupBy('content_id')
                            ->where('created_at', 'like', '%'.$date.'%')
                            ->first();

                if(empty($cekAudit)){
                    DB::table('youtube_video_audit_cms')->insert([
                        'content_id'=> $cmsID->content_id,
                        'total_video'=>  $total_video ,
                        'active_monetize'=>  $activeMonetez == 0 ? 0 : $activeMonetez,
                        'no_display_ads'=> $No_display_ads_enabled == 0 ? 0 : $No_display_ads_enabled,
                        'no_overlay_ads'=> $No_overlay_ads_enabled == 0 ? 0 : $No_overlay_ads_enabled,
                        'no_sponsor_card'=> $No_sponsored_cards_enabled == 0 ? 0 : $No_sponsored_cards_enabled,
                        'no_skippable_video_ads'=> $No_skippable_video_ads_enabled == 0 ? 0 : $No_skippable_video_ads_enabled,
                        'no_nonskippable_video_ads'=> $No_nonskippable_video_ads_enabled == 0 ? 0 : $No_nonskippable_video_ads_enabled,
                        'no_active_monetize'=> $notactiveMonetez == 0 ? 0 : $notactiveMonetez,
                        'copyright_claim'=> $copyright_claim == 0 ? 0 : $copyright_claim,
                        'content_blank'=> $blank == 0 ? 0 : $blank,
                        'content_block'=> $block == 0 ? 0 : 0 ? 0 : $block,
                        'content_track'=> $track == 0 ? 0 : $track, 
                        'content_monetized_owner'=> $monetezOwner == 0 ? 0 : $monetezOwner,
                        'content_split_monetized'=> $split_monetized == 0 ? 0 : $split_monetized,
                        'persen_non_act_monetize'=> $notactiveMonetez == 0 ? 0 : ($notactiveMonetez/$total_video),
                        'persen_limited_ads'=> $total_video == 0 ? 0 : ($notactiveMonetez/$total_video),
                        'persen_format_ads_not_complete'=> ($No_display_ads_enabled + $No_overlay_ads_enabled + $No_sponsored_cards_enabled + $No_skippable_video_ads_enabled + $No_nonskippable_video_ads_enabled),
                        'persen_content_non_activated'=> $activeMonetez == 0 ? 0 : (($blank+$block+$track) / $activeMonetez),
                        'persen_copyright_claim'=> $total_video == 0 ? 0 : ($copyright_claim/$total_video),
                        'date_process' => $date,
                        // 'created_at'=>  date("Y-m-d H:i:s"),
                        // 'updated_at'=>  date("Y-m-d H:i:s"), //2016-11-13 23:17:''0
                        'created_at'=>  "2020-03-05 10:40:00",
                        'updated_at'=>  "2020-03-05 10:40:00", //2016-11-13 23:17:''0
                    ]);
                }
            }else{
                return "Null CMS";
            }
        }

        // $channelID = DB::table('youtube_video')->groupBy('channel_id')->get();
        $channelID = YoutubeVideo::where('time_process','=',$date)->groupBy('channel_id')->get();
        // return $channelID;
        foreach ($channelID as $channelID) {

            $total_video = YoutubeVideo::where(['channel_id'=>$channelID->channel_id, 'time_process'=>$date])->where('video_id','!=','0')->count();
            // return $total_video;

            $activeMonetez = DB::table('youtube_video')
                            ->where('claimed_by_another_owner','=','No')
                            ->where('in_video_ads_enabled','=','Yes')
                            ->where('channel_id','=',$channelID->channel_id)
                            ->where('time_process','=',$date)
                            ->count();
                            
            $notactiveMonetez = DB::table('youtube_video')
                            ->where(['channel_id'=>$channelID->channel_id])
                            ->where('claimed_by_another_owner','=','No')
                            ->where('time_process','=',$date)
                            ->where(function($q) {
                                return $q
                                ->where('in_video_ads_enabled','=','No')
                                ->orWhere('in_video_ads_enabled','=',' ');
                            })
                            ->count();
            // return $notactiveMonetez;

            $copyright_claim = DB::table('youtube_video')
                            ->where('time_process','=',$date)
                            ->where(['claimed_by_another_owner'=>'Yes',
                                'channel_id'=>$channelID->channel_id
                            ])->count();

            $split_monetized = DB::table('youtube_video')
                            ->where('time_process','=',$date)
                            ->where([
                                'claimed_by_this_owner'=>'Yes',
                                'claimed_by_another_owner'=>'Yes',
                                'channel_id'=>$channelID->channel_id
                            ])->count();

            $No_display_ads_enabled = $this->CheckAdsChannel('display_ads_enabled',$channelID->channel_id);
            $No_overlay_ads_enabled = $this->CheckAdsChannel('overlay_ads_enabled',$channelID->channel_id);
            $No_sponsored_cards_enabled = $this->CheckAdsChannel('sponsored_cards_enabled',$channelID->channel_id);
            $No_skippable_video_ads_enabled = $this->CheckAdsChannel('skippable_video_ads_enabled', $channelID->channel_id);
            $No_nonskippable_video_ads_enabled = $this->CheckAdsChannel('nonskippable_video_ads_enabled', $channelID->channel_id);

            $blank = $this->findAuditChannel('',$channelID->channel_id) ;
            $block = $this->findAuditChannel('Block',$channelID->channel_id);
            $track = $this->findAuditChannel('Track',$channelID->channel_id);
            $monetezOwner = $this->findAuditChannel('Monetize',$channelID->channel_id);

            $cekAuditChannel = DB::table('youtube_video_audit_channel')
                        ->where('content_id','=',$channelID->content_id)
                        ->where('channel_id','=',$channelID->channel_id)
                        ->where('created_at', 'like', '%'.$date.'%')
                        // ->groupBy('channel_id')
                        ->first();

            if(empty($cekAuditChannel)){
                DB::table('youtube_video_audit_channel')->insert([
                    'content_id'=> $channelID->content_id,
                    'channel_id'=> $channelID->channel_id,
                    'total_video'=>  $total_video ,
                    'active_monetize'=>  $activeMonetez == 0 ? 0 : $activeMonetez,
                    'no_display_ads'=> $No_display_ads_enabled == 0 ? 0 : $No_display_ads_enabled,
                    'no_overlay_ads'=> $No_overlay_ads_enabled == 0 ? 0 : $No_overlay_ads_enabled,
                    'no_sponsor_card'=> $No_sponsored_cards_enabled == 0 ? 0 : $No_sponsored_cards_enabled,
                    'no_skippable_video_ads'=> $No_skippable_video_ads_enabled == 0 ? 0 : $No_skippable_video_ads_enabled,
                    'no_nonskippable_video_ads'=> $No_nonskippable_video_ads_enabled == 0 ? 0 : $No_nonskippable_video_ads_enabled,
                    'no_active_monetize'=> $notactiveMonetez == 0 ? 0 : $notactiveMonetez,
                    'copyright_claim'=> $copyright_claim == 0 ? 0 : $copyright_claim,
                    'content_blank'=> $blank == 0 ? 0 : $blank,
                    'content_block'=> $block == 0 ? 0 : 0 ? 0 : $block,
                    'content_track'=> $track == 0 ? 0 : $track, 
                    'content_monetized_owner'=> $monetezOwner == 0 ? 0 : $monetezOwner,
                    'content_split_monetized'=> $split_monetized == 0 ? 0 : $split_monetized,
                    'persen_non_act_monetize'=> $notactiveMonetez == 0 ? 0 : ($notactiveMonetez/$total_video),
                    'persen_limited_ads'=> $total_video == 0 ? 0 : ($notactiveMonetez/$total_video),
                    'persen_format_ads_not_complete'=> ($No_display_ads_enabled + $No_overlay_ads_enabled + $No_sponsored_cards_enabled + $No_skippable_video_ads_enabled + $No_nonskippable_video_ads_enabled),
                    'persen_content_non_activated'=> $activeMonetez == 0 ? 0 : (($blank+$block+$track) / $activeMonetez),
                    'persen_copyright_claim'=> $total_video == 0 ? 0 : ($copyright_claim/$total_video),
                    'date_process' => $date,
                    // 'created_at'=>  date("Y-m-d H:i:s"),
                    // 'updated_at'=>  date("Y-m-d H:i:s"), //2016-11-13 23:17:''0
                    'created_at'=>  "2020-03-05 10:40:00",
                    'updated_at'=>  "2020-03-05 10:40:00", //2016-11-13 23:17:''0
                ]);
            }
        }
        // }
        /*}else{
            return "Null";
        }*/
    }

    public function findAudit($effective_policy, $content){
        if(empty($effective_policy)){
            $count = DB::table('youtube_video')
            ->where('effective_policy', '=', ' ')
            // ->where('time_process','=',date("Y-m-d"))
            ->where('time_process','=','2020-03-05')
            ->where('content_id','=',$content)
            ->where('in_video_ads_enabled','=','Yes')
            ->where('claimed_by_another_owner','=','No')
            ->count();
        }else{
            $count = DB::table('youtube_video')
            ->where('effective_policy', 'like', $effective_policy.'%')
            // ->where('time_process','=',date("Y-m-d"))
            ->where('time_process','=','2020-03-05')
            ->where('content_id','=',$content)
            ->where('in_video_ads_enabled','=','Yes')
            ->where('claimed_by_another_owner','=','No')
            ->count();
        }        
        return $count;
    }

    public function findAuditChannel($effective_policy, $channel){
        if(empty($effective_policy)){
            $count = DB::table('youtube_video')
            ->where('effective_policy', '=', ' ')
            // ->where('time_process','=',date("Y-m-d"))
            ->where('time_process','=','2020-03-05')
            ->where('channel_id','=',$channel)
            ->where('in_video_ads_enabled','=','Yes')
            ->where('claimed_by_another_owner','=','No')
            ->count();
        }else{
            $count = DB::table('youtube_video')
            ->where('effective_policy', 'like', $effective_policy.'%')
            // ->where('time_process','=',date("Y-m-d"))
            ->where('time_process','=','2020-03-05')
            ->where('channel_id','=',$channel)
            ->where('in_video_ads_enabled','=','Yes')
            ->where('claimed_by_another_owner','=','No')
            ->count();
        }        
        return $count;
    }

    public function CheckAds($value , $content){
        $count = DB::table('youtube_video')
                // ->where('time_process','=',date("Y-m-d"))
                ->where('time_process','=','2020-03-05')
                ->where('claimed_by_another_owner','=','No')
                ->where('in_video_ads_enabled','=','Yes')
                ->where(function($q) use($value) {
                    return $q
                    ->orWhere($value,'=','No')
                    ->orWhere($value,'=',' ');
                })
                ->where('content_id','=',$content)
                ->count();
        return $count;
    }

    public function CheckAdsChannel($value , $channel){
        $count = DB::table('youtube_video')
                // ->where('time_process','=',date("Y-m-d"))
                ->where('time_process','=','2020-03-05')
                ->where('claimed_by_another_owner','=','No')
                ->where('in_video_ads_enabled','=','Yes')
                ->where(function($q) use($value) {
                    return $q
                    ->orWhere($value,'=','No')
                    ->orWhere($value,'=',' ');
                })
                ->where('channel_id','=',$channel)
                ->count();
        return $count;
    }


    public function cmsAudits(){
        $cms = DB::table('youtube_video_audit_cms')
                ->select('youtube_partner.display_name','youtube_partner.content_id','youtube_video_audit_cms.created_at', 'youtube_video_audit_cms.total_video')
                ->join('youtube_partner', 'youtube_video_audit_cms.content_id', '=', 'youtube_partner.content_id')
                ->groupBy('youtube_partner.content_id')
               // ->orderBy('youtube_video_audit_cms.created_at', 'DESC')
                ->get();
        return view('youtube.audits.home.index',compact('cms'));
    }

    public function channelAudits(){
        $user_id = Auth::user()->id;
        $channel = User::where('id', $user_id)->get();
        $date = date("Y-m-d");
        // return $channel;
        // return $total_video;
        return view('youtube.audits.home.index',compact('channel'));
    }

    public function cmsAuditsDetailsMonetize($cms){
        $cmsaudit = DB::table('youtube_video_audit_cms')
                    ->select('youtube_partner.content_id','youtube_partner.display_name','youtube_video_audit_cms.*')
                    ->join('youtube_partner', 'youtube_video_audit_cms.content_id', '=', 'youtube_partner.content_id')
                    //->where('youtube_video_audit_cms.created_at', 'like', '%'.$date.'%')
                    ->where('youtube_video_audit_cms.content_id', '=', $cms)
                    ->get();
        //return $cmsaudit;
        $cms = DB::table('youtube_partner')->where(['content_id'=>$cms])->first();

        return view('youtube.audits.details.monetize.index',compact('cmsaudit','cms'));
    }

    public function channelAuditsDetailsMonetize($cms){
        $channelaudit = YoutubeAuditChannel::where('channel_id', '=', $cms)
                    ->get();
        //return $cmsaudit;
        $channel = User::where(['provider_id'=>$cms])->first();

        return view('youtube.audits.details.monetize.index',compact('channelaudit','channel'));
    }

    public function cmsAuditsDetailsContent($cms){
        $cmsaudit = DB::table('youtube_video_audit_cms')
                    ->select('youtube_partner.content_id','youtube_partner.display_name','youtube_video_audit_cms.*')
                    ->join('youtube_partner', 'youtube_video_audit_cms.content_id', '=', 'youtube_partner.content_id')
                    //->where('youtube_video_audit_cms.created_at', 'like', '%'.$date.'%')
                    ->where('youtube_video_audit_cms.content_id', '=', $cms)
                    ->get();
        // return $cmsaudit;
        $cms = DB::table('youtube_partner')->where(['content_id'=>$cms])->first();

        // return $cmsaudit;
        return view('youtube.audits.details.contentid.index',compact('cmsaudit','cms'));
    }

    public function channelAuditsDetailsContent($cms){
        $cmsaudit = DB::table('youtube_video_audit_cms')
                    ->select('youtube_partner.content_id','youtube_partner.display_name','youtube_video_audit_cms.*')
                    ->join('youtube_partner', 'youtube_video_audit_cms.content_id', '=', 'youtube_partner.content_id')
                    //->where('youtube_video_audit_cms.created_at', 'like', '%'.$date.'%')
                    ->where('youtube_video_audit_cms.content_id', '=', $cms)
                    ->get();
        // return $cmsaudit;
        $cms = DB::table('youtube_partner')->where(['content_id'=>$cms])->first();

        // return $cmsaudit;
        return view('youtube.audits.details.contentid.index',compact('cmsaudit','cms'));
    }

    public function csv_exportcmsAuditMonetize($cms,$field,$time){
        
        if($field == 'no_active_monetize'){
            $cmsaudit = DB::table('youtube_video')
                        ->where(['content_id'=>$cms])
                        ->where('claimed_by_another_owner','=','No')
                        ->where('time_process','=',$time)
                        ->where(function($q) {
                            return $q
                            ->where('in_video_ads_enabled','=','No')
                            ->orWhere('in_video_ads_enabled','=',' ');
                        })
                        ->get();
        }elseif($field == 'claimed_by_another_owner'){
            $cmsaudit = DB::table('youtube_video')
            ->where([
                'claimed_by_another_owner'=>'Yes',
                'content_id'=>$cms,
                'time_process'=>$time
            ])->get();
        }else{
            $cmsaudit = DB::table('youtube_video')
            ->where('time_process','=',$time)
            ->where('claimed_by_another_owner','=','No')
            ->where('in_video_ads_enabled','=','Yes')
            ->where(function($q) use($field) {
                return $q
                ->orWhere($field,'=','No')
                ->orWhere($field,'=',' ');
            })
            ->where('content_id','=',$cms)
            ->get();
        }

        $cmsname = DB::table('youtube_partner')
        ->where('content_id','=',$cms)
        ->first();

        // return $cmsaudit;
        
        return view('youtube.audits.details.monetize.export_csv',compact('cmsaudit','cms','field','time','cmsname'));
    }
    public function csv_exportcmsAuditContent($cms,$field,$time){
        if($field=='Blank'){
            $cmsaudit = DB::table('youtube_video')
            ->where('effective_policy', '=', ' ')
            ->where('time_process','=',$time)
            ->where('content_id','=',$cms)
            ->where('in_video_ads_enabled','=','Yes')
            ->where('claimed_by_another_owner','=','No')
            ->get();
        }elseif ($field=='split_monetize'){
            $cmsaudit = DB::table('youtube_video')
            ->where([
                'claimed_by_this_owner'=>'Yes',
                'claimed_by_another_owner'=>'Yes',
                'content_id'=>$cms
            ])->get();
        }else{
            $cmsaudit = DB::table('youtube_video')
            ->where('effective_policy', 'like', '%'.$field.'%')
            ->where('time_process','=',$time)
            ->where('content_id','=',$cms)
            ->where('in_video_ads_enabled','=','Yes')
            ->where('claimed_by_another_owner','=','No')
            ->get();
        } 
        
        $cmsname = DB::table('youtube_partner')
        ->where('content_id','=',$cms)
        ->first();

        // return $cmsname;
        
        return view('youtube.audits.details.contentid.export_csv',compact('cmsaudit','cms','field','time','cmsname'));
    }

    // Update Content ID
    public function updateContentID(Request $request)
    {
        ini_set('memory_limit', '-1');
        $path = public_path().'/uploads/videos/video_report/update/content_id';
        if(!is_dir($path)) {
            File::makeDirectory($path, 0777, true, true);
        }

        $data = new \stdClass();
        $array_cms = array();

        if($request->file != NULL){
            // $request->file->storeAs($path, $request->file->getClientOriginalName(), 'local_public');
            $request->file->move($path.'/', $request->file->getClientOriginalName());
            $file_path = $path.'/'.$request->file->getClientOriginalName();

            if($request->file->getClientOriginalExtension() == 'xlsx'){
                $reader = ReaderFactory::create(Type::XLSX); // for XLSX files

                $reader->open($file_path);
                foreach ($reader->getSheetIterator() as $sheet) {
                    foreach ($sheet->getRowIterator() as $key => $row) {
                        if($key>1){
                            if(empty($row[0])){
                                break;
                            }
                            $record  = new \stdClass();
                            $record->data  = $request->data;
                            $record->content_id  = $row[0];
                            $record->video_id  = $row[1];
                            $record->channel_id  = $row[2];
                            $record->channel_display_name  = $row[3];
                            $record->time_published  = $row[4];
                            $record->video_title  = $row[5];
                            $record->video_length  = $row[6];
                            $record->views  = $row[7];
                            $record->video_privacy_status  = $row[8];
                            $record->effective_policy  = $row[9];
                            $record->asset_id  = $row[10];
                            array_push($array_cms, $record);
                        }
                    }
                }
                $reader->close();
            }else {
                $reader = ReaderFactory::create(Type::CSV); // for CSV files
                $reader->open($file_path);
                $file_path = $path.'/'.$request->file->getClientOriginalName();
                $file = fopen($file_path,"r");
                $numb = 0;
                while(! feof($file)){
                    $row = fgetcsv($file);
                    json_encode($row);
                    // $row = str_replace(";",",",$row);
                    $record = new \stdClass();
                    $numb++;
                    // dd($row[0]);
                    if($numb==1){  continue; }
                    $record  = new \stdClass();
                    $record->data  = $request->data;
                    $record->content_id  = $row[0];
                    $record->video_id  = $row[1];
                    $record->channel_id  = $row[2];
                    $record->channel_display_name  = $row[3];
                    $record->time_published  = $row[4];
                    $record->video_title  = $row[5];
                    $record->video_length  = $row[6];
                    $record->views  = $row[7];
                    $record->video_privacy_status  = $row[8];
                    $record->effective_policy  = $row[9];
                    $record->asset_id  = $row[10];
                    array_push($array_cms, $record);
                }
                fclose($file);
            }
            $reader->close();

            $videoInsert = $array_cms;

            foreach ($videoInsert as $data_video) {
                $cekVideo = YoutubeUpdateContentId::where(['video_id' => $data_video->video_id])->first();

                if(empty($cekVideo)){
                    if(!empty($data_video->content_id)){
                        DB::table('youtube_video_content_id')->insert([
                            'data' => $data_video->data,
                            'content_id' => $data_video->content_id,
                            'video_id' => $data_video->video_id,
                            'channel_id' => $data_video->channel_id,
                            'channel_display_name' => $data_video->channel_display_name,
                            'time_published' => $data_video->time_published,
                            'video_title' => $data_video->video_title,
                            'video_length' => $data_video->video_length,
                            'views' => $data_video->views,
                            'video_privacy_status' => $data_video->video_privacy_status,
                            'effective_policy' => $data_video->effective_policy,
                            'asset_id' => $data_video->asset_id,
                        ]);
                    }
                }
            }
        }

        return redirect()->back();
    }
    // end update content id
}
