<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use App\Model\UserToken;
use App\Model\SocialFollowers;

use Auth;

class TwitterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function AddUserToken($getToken){
        $redirect = Auth::user()->roles[0]->name;
        $UserToken  = new UserToken();
        $UserToken->user_id = Auth::user()->id;
        $UserToken->type  = 'twitter';
        $UserToken->expires_in  = '3600';
        $UserToken->access_token  = $getToken->token;
        $UserToken->token_secret  = $getToken->tokenSecret;
        $UserToken->refresh_token  = $getToken->token;
        $UserToken->save();

        if($UserToken){
            return true;
        }else{
            return false;
        }
    }

    public function postTweet(){
        //$access_token = '235499191-glZEFdal0chzdE9LgQJNGZxEUZyreqd1dQoULMNo';
        //$access_token_secret = 'Fg4M8oiZaLQSJLxSnfq2v7LWS1Kky0u4oNYNuKQ1T3gds';

        $key = env('TWITTER_CLIENT_ID');
        $secret=env('TWITTER_CLIENT_SECRET');
        $getUserToken= UserToken::where(['user_id'=>Auth::user()->id])->first();

        $connection = new TwitterOAuth($key, $secret, $getUserToken->access_token, $getUserToken->token_secret);

        //$statues = $connection->post("statuses/update", ["status" => "hello world 123"]);

        $media1 = $connection->upload('media/upload', ['media' => '/path/to/file/kitten1.jpg']);
        $media2 = $connection->upload('media/upload', ['media' => '/path/to/file/kitten2.jpg']);

        $parameters = [
            'status' => 'Meow Meow Meow',
            'media_ids' => implode(',', [
                $media1->media_id_string, 
                $media2->media_id_string
            ])
        ];

        $result = $connection->post('statuses/update', $parameters);

        dd($result);
    }
    

    public function getFollowers($twurl){
        //$twurl = "https://twitter.com/AgathaChelsea18";
        $account = explode("/",$twurl);
        $twitter_username = $account[3]; 
        $data_twitter = file_get_contents('https://cdn.syndication.twimg.com/widgets/followbutton/info.json?screen_names='.$twitter_username); 
        $parsed_twitter =  json_decode($data_twitter,true);
        return $parsed_twitter[0];
    }


}
