<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Backend\Controller;
use App\Repositories\UserRepository;
use Flash;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Storage;
use Log;
use App\Model\User;
use App\Model\UserGroup;
use App\Model\UserToken;
use App\Model\YoutubeAnalitic;
use App\Model\YoutubeAnaliticLink;
use App\Model\YoutubeVideo;
use App\Model\YoutubePartner;
use App\Model\YoutubeChannel;
use App\Model\YoutubeAnaliticVideo;
use Excel;
use Carbon;
use Auth;
use DB;
use PDF;
use Session;
use Youtube;
use Socialite;
use Google_Client;
use Google_Service_Drive;
use Google_Service_Sheets_Spreadsheet;
use Google_Service_Drive_DriveFile;
use Google_Service_Sheets;
use Google_Service_Sheets_ValueRange;
use Google_Service_YouTube;
//use Google_Service_YouTubePartner;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Backend\TwitterController;
use App\Http\Controllers\Backend\YoutubeReportingController;
use App\Http\Controllers\Backend\VideoYTController;
use App\Http\Controllers\Auth\CallbackController;
use Abraham\TwitterOAuth\TwitterOAuth;

class YoutubeController extends Controller
{

    protected $users;
    protected $authLogin;
    protected $TwitterCtl;
    protected $YtReporting;
    protected $YtAudits;
    protected $Callback;


    public function __construct(UserRepository $users, LoginController $authLogin, TwitterController $TwitterCtl, YoutubeReportingController $YtReporting, VideoYTController $YtAudits, CallbackController $Callback)
    {
        $this->users = $users;
        $this->authLogin =  $authLogin;
        $this->TwitterCtl =  $TwitterCtl;
        $this->YtReporting =  $YtReporting;
        $this->YtAudits =  $YtAudits;
        $this->Callback =  $Callback;
    }

    public function GoogleClient(){

        $Secret = DB::table('yt_content_secret')
                    ->where('user_id', auth()->user()->id)
                    ->where('type', 'youtube')
                    ->first();

        $client = new \Google_Client();
        $client->setClientId($Secret->client_id);
        $client->setClientSecret($Secret->client_secret);
        //$client->setScopes('https://www.googleapis.com/auth/youtube');
        
        $client->setScopes([
            'https://www.googleapis.com/auth/youtube',
            'https://www.googleapis.com/auth/yt-analytics-monetary.readonly',
            'https://www.googleapis.com/auth/youtubepartner',
            'https://www.googleapis.com/auth/yt-analytics.readonly'
        ]);
        $client->addScope(\Google_Service_YouTubeAnalytics::YT_ANALYTICS_READONLY);
        $client->setRedirectUri($Secret->client_redirect);
        $client->setAccessType('offline');
        $client->setPrompt('consent select_account');
        $client->setApprovalPrompt('force');
        return $client;
    }

    public function index()
    {
        $typereporting[''] = 'Please Select';
        $typereporting[0] = 'Weekly';
        $typereporting[1] = 'Monthly';
        $typereporting[2] = 'Yearly';;

        //$channel = Youtube::getChannelByCustomUrl('AsiaMediaProductions');
        //dd($channel);

        return view('youtube.index', [
            'typereporting' => $typereporting
        ]);
    }

    public function analyticdetail(Request $request)
    {
        $date = $request->year.'-'.$request->month;        
        $chanalytics = DB::select("SELECT name_channel, unit, `subscribers` FROM (SELECT youtube_analitic.name_channel, users.`unit`, `user_id`, Concat(Group_concat(youtube_analitic.`subscribers`), ',') AS `subscribers` FROM youtube_analitic INNER JOIN users ON users.id = youtube_analitic.user_id WHERE Date_format(youtube_analitic.created_at, '%Y-%m') = '".$date."' AND users.is_active = 1 GROUP BY youtube_analitic.name_channel, `user_id`) aggr ORDER BY `unit`");
        
        $gsheetlink = YoutubeAnaliticLink::where(DB::raw("Date_format(created_at, '%Y-%m')"),$date)->first();
        $typereporting[''] = 'Please Select';
        $typereporting[0] = 'Weekly';
        $typereporting[1] = 'Monthly';
        $typereporting[2] = 'Yearly';
        
        return view('youtube.analytics.index', [
            'date'          => $date,
            'chanalytics'   => $chanalytics,
            'gsheetlink'    => $gsheetlink,
            'typereporting' => $typereporting
        ]);
    }

    
    public function analyticJson(){
        $class = new \stdClass();
        $array_dsp = array();
        $num = 1;
        $date = date("Y-m");
        
        $data = [];

        $data[0] = [
            $number[] = "LIST Of Youtube MNC Group (All Channels)",
            $name_channel[] = "",
            $program[] = "",
            $subscribers1[] = "",
            $subscribers4[] = "",
            $increasing[] = "",
        ];

        $data[1] = [
            $number[] = "Period 26 May 2019 - 25 June 2019",
            $name_channel[] = " ",
            $program[] = "  ",
            $subscribers1[] = "  ",
            $subscribers4[] = "  ",
            $increasing[] = "  ",
        ];

        $data[2] = [
            $number[] = "No",
            $name_channel[] = "Youtube Account",
            $program[] = "Program",
            $subscribers1[] = "Subscriber Week #1",
            $subscribers4[] = "Subscriber Week #4",
            $increasing[] = "% Increasing Subs (W4 / W1)",
        ];

        $getUnit = DB::select("SELECT * FROM `users` GROUP BY unit ORDER BY unit");
        $countUnit = count($getUnit);


        for($x=1; $x < $countUnit; $x++) {

            $data[] = [
                $number[] = $getUnit[$x]->unit,
                $name_channel[] = "  ",
                $program[] = "  ",
                $subscribers1[] = "  ",
                $subscribers4[] = "  ",
                $increasing[] = "  ",
            ];

            $var = "SELECT * FROM ( SELECT youtube_analitic.name_channel, users.`unit`, users.program, `user_id`, CONCAT( GROUP_CONCAT(youtube_analitic.`subscribers`), ',' ) AS `subscribers` FROM youtube_analitic INNER JOIN users ON users.id = youtube_analitic.user_id WHERE DATE_FORMAT( youtube_analitic.created_at, '%Y-%m' ) = '".$date."' AND users.is_active = 1 GROUP BY youtube_analitic.name_channel, `user_id` ) aggr WHERE `unit` = '".$getUnit[$x]->unit."' ORDER BY `unit`";
            
            $rows = DB::select($var);
            $countR = count($rows);

            for($i=0; $i < $countR; $i++) { 
                if(empty(explode(',', $rows[$i]->subscribers)[0])) {$sub1=0;}
                else{$sub1= explode(',', $rows[$i]->subscribers)[0];}
                
                if(empty(explode(',', $rows[$i]->subscribers)[1])) {$sub2=0;}
                else{ $sub2= explode(',', $rows[$i]->subscribers)[1]; }
                            
                if(empty(explode(',', $rows[$i]->subscribers)[2])) {$sub3=0;}
                else{ $sub3= explode(',', $rows[$i]->subscribers)[2]; }

                if(empty(explode(',', $rows[$i]->subscribers)[3])) {$sub4=0;}
                else{ $sub4= explode(',', $rows[$i]->subscribers)[3]; }
                
                
                if($sub1!="0" && $sub2=="0" && $sub3=="0" && $sub4=="0"){
                    $incres=0;
                }elseif($sub1!="0" && $sub2!="0" && $sub3=="0" && $sub4=="0"){
                    $incres = number_format((float)(($sub2-$sub1)/$sub1)*100, 2, '.', '');
                }elseif($sub1!="0" && $sub2!="0" && $sub3!="0" && $sub4=="0"){
                    $incres = number_format((float)(($sub3-$sub2)/$sub2)*100, 2, '.', '');
                }elseif($sub1!="0" && $sub2!="0" && $sub3!="0" && $sub4!="0"){
                    $incres = number_format((float)(($sub4-$sub3)/$sub3)*100, 2, '.', '');
                }else{
                    $incres = 0;
                }

                $data[] = [
                    $number[] = $num,
                    $name_channel[] = $rows[$i]->name_channel,
                    $program[] = $rows[$i]->program,
                    $subscribers1[] = $sub1,
                    $subscribers4[] = $sub4,
                    $increasing[] = $incres,
                ];
                $num++;
            }
            $data[] = [
                $number[] = "",
                $name_channel[] = "  ",
                $program[] = "  ",
                $subscribers1[] = "  ",
                $subscribers4[] = "  ",
                $increasing[] = "  ",
            ];

            
        }


        return ($data) ;
    }

    public function csv_export($date)
    {
        $chanalytics = DB::select("SELECT name_channel, unit, `subscribers` FROM (SELECT youtube_analitic.name_channel, users.unit, `user_id`, Concat(Group_concat(youtube_analitic.`subscribers` ORDER BY youtube_analitic.`subscribers` asc), ',') AS `subscribers` FROM youtube_analitic INNER JOIN users ON users.id = youtube_analitic.user_id WHERE Date_format(youtube_analitic.created_at, '%Y-%m') = '".$date."' AND users.is_active = 1 GROUP BY youtube_analitic.name_channel, `user_id`) aggr ORDER BY `unit`");

        return view('youtube.analytics.export_csv',compact('chanalytics'));
    }

    public function excell_export($date)
    {
        $id='';
        $filename = "Report_Youtube_Analytics_".$date;
        $excel = Excel::create($filename, function($excel) use ($id, $date) {

            $excel->sheet('Sheet1', function($sheet) use ($id, $date) {
                // $query = DB::select("SELECT name_channel, unit, `subscribers` FROM (SELECT youtube_analitic.name_channel, users.unit, `user_id`, Concat(Group_concat(youtube_analitic.`subscribers` ORDER BY youtube_analitic.`subscribers` asc), ',') AS `subscribers` FROM youtube_analitic INNER JOIN users ON users.id = youtube_analitic.user_id WHERE Date_format(youtube_analitic.created_at, '%Y-%m') = '".$date."' AND users.is_active = 1 GROUP BY youtube_analitic.name_channel, `user_id`) aggr ORDER BY `unit`");
                $query = DB::select("SELECT * FROM `users` where unit is not null GROUP BY unit ORDER BY unit");
                $sheet->loadView('youtube.analytics.export_excel', [
                    'data' => $query,
                    'date' => $date
                ]);
            });
        });
        return $excel->export('xlsx');
    }

    public function export_print($date)
    {
        $unit = DB::select("SELECT * FROM `users` GROUP BY unit ORDER BY unit");

        $chanalytics = DB::select("SELECT name_channel, unit, `subscribers` FROM (SELECT youtube_analitic.name_channel, users.unit, `user_id`, Concat(Group_concat(youtube_analitic.`subscribers`), ',') AS `subscribers` FROM youtube_analitic INNER JOIN users ON users.id = youtube_analitic.user_id WHERE Date_format(youtube_analitic.created_at, '%Y-%m') = '".$date."' AND users.is_active = 1 GROUP BY youtube_analitic.name_channel, `user_id`) aggr ORDER BY `unit`");

        return view('youtube.analytics.export_print',compact('unit','chanalytics','date'));
    }

    public function auditsIndex()
    {

        $typereporting[''] = 'Please Select';
        $typereporting[0] = 'Weekly';
        $typereporting[1] = 'Monthly';
        $typereporting[2] = 'Yearly';;

        return view('youtube.audits', [
            'typereporting' => $typereporting
        ]);
    }

    public function auditsdetail(Request $request)
    {
        //$this->YtAudits->Bcxz();
        $date = $request->year.'-'.$request->month;        
        $cmsaudit = DB::table('youtube_video_audit')
                    ->select('youtube_partner.content_id','youtube_partner.display_name','youtube_video_audit.*')
                    ->join('youtube_partner', 'youtube_video_audit.content_id', '=', 'youtube_partner.content_id')
                    ->where('youtube_video_audit.created_at', 'like', '%'.$date.'%')
                    ->get();
        
        //return $cmsaudit;


        $typereporting[''] = 'Please Select';
        $typereporting[0] = 'Weekly';
        $typereporting[1] = 'Monthly';
        $typereporting[2] = 'Yearly';
        
        return view('youtube.audits.index', [
            'date'          => $date,
            'cmsaudit'   => $cmsaudit,
            'typereporting' => $typereporting
        ]);
    }

    
    public function auditsJson(){
        $class = new \stdClass();
        $array_dsp = array();
        $num = 1;
        $date = date("Y-m");
        
        $data = [];

        $data[0] = [
            $number[] = "LIST Of Youtube MNC Group (All Channels)",
            $name_channel[] = "",
            $program[] = "",
            $subscribers1[] = "",
            $subscribers4[] = "",
            $increasing[] = "",
        ];

        $data[1] = [
            $number[] = "Period 26 May 2019 - 25 June 2019",
            $name_channel[] = " ",
            $program[] = "  ",
            $subscribers1[] = "  ",
            $subscribers4[] = "  ",
            $increasing[] = "  ",
        ];

        $data[2] = [
            $number[] = "No",
            $name_channel[] = "Youtube Account",
            $program[] = "Program",
            $subscribers1[] = "Subscriber Week #1",
            $subscribers4[] = "Subscriber Week #4",
            $increasing[] = "% Increasing Subs (W4 / W1)",
        ];

        $getUnit = DB::select("SELECT * FROM `users` GROUP BY unit ORDER BY unit");
        $countUnit = count($getUnit);


        for($x=1; $x < $countUnit; $x++) {

            $data[] = [
                $number[] = $getUnit[$x]->unit,
                $name_channel[] = "  ",
                $program[] = "  ",
                $subscribers1[] = "  ",
                $subscribers4[] = "  ",
                $increasing[] = "  ",
            ];

            $var = "SELECT * FROM ( SELECT youtube_analitic.name_channel, users.`unit`, users.program, `user_id`, CONCAT( GROUP_CONCAT(youtube_analitic.`subscribers`), ',' ) AS `subscribers` FROM youtube_analitic INNER JOIN users ON users.id = youtube_analitic.user_id WHERE DATE_FORMAT( youtube_analitic.created_at, '%Y-%m' ) = '".$date."' AND users.is_active = 1 GROUP BY youtube_analitic.name_channel, `user_id` ) aggr WHERE `unit` = '".$getUnit[$x]->unit."' ORDER BY `unit`";
            
            $rows = DB::select($var);
            $countR = count($rows);

            for($i=0; $i < $countR; $i++) { 
                if(empty(explode(',', $rows[$i]->subscribers)[0])) {$sub1=0;}
                else{$sub1= explode(',', $rows[$i]->subscribers)[0];}
                
                if(empty(explode(',', $rows[$i]->subscribers)[1])) {$sub2=0;}
                else{ $sub2= explode(',', $rows[$i]->subscribers)[1]; }
                            
                if(empty(explode(',', $rows[$i]->subscribers)[2])) {$sub3=0;}
                else{ $sub3= explode(',', $rows[$i]->subscribers)[2]; }

                if(empty(explode(',', $rows[$i]->subscribers)[3])) {$sub4=0;}
                else{ $sub4= explode(',', $rows[$i]->subscribers)[3]; }
                
                
                if($sub1!="0" && $sub2=="0" && $sub3=="0" && $sub4=="0"){
                    $incres=0;
                }elseif($sub1!="0" && $sub2!="0" && $sub3=="0" && $sub4=="0"){
                    $incres = number_format((float)(($sub2-$sub1)/$sub1)*100, 2, '.', '');
                }elseif($sub1!="0" && $sub2!="0" && $sub3!="0" && $sub4=="0"){
                    $incres = number_format((float)(($sub3-$sub2)/$sub2)*100, 2, '.', '');
                }elseif($sub1!="0" && $sub2!="0" && $sub3!="0" && $sub4!="0"){
                    $incres = number_format((float)(($sub4-$sub3)/$sub3)*100, 2, '.', '');
                }else{
                    $incres = 0;
                }

                $data[] = [
                    $number[] = $num,
                    $name_channel[] = $rows[$i]->name_channel,
                    $program[] = $rows[$i]->program,
                    $subscribers1[] = $sub1,
                    $subscribers4[] = $sub4,
                    $increasing[] = $incres,
                ];
                $num++;
            }
            $data[] = [
                $number[] = "",
                $name_channel[] = "  ",
                $program[] = "  ",
                $subscribers1[] = "  ",
                $subscribers4[] = "  ",
                $increasing[] = "  ",
            ];

            
        }


        return ($data) ;
    }

    public function audits_csv_export($date)
    {
        $chanalytics = DB::select("SELECT name_channel, unit, `subscribers` FROM (SELECT youtube_analitic.name_channel, users.unit, `user_id`, Concat(Group_concat(youtube_analitic.`subscribers` ORDER BY youtube_analitic.`subscribers` asc), ',') AS `subscribers` FROM youtube_analitic INNER JOIN users ON users.id = youtube_analitic.user_id WHERE Date_format(youtube_analitic.created_at, '%Y-%m') = '".$date."' AND users.is_active = 1 GROUP BY youtube_analitic.name_channel, `user_id`) aggr ORDER BY `unit`");

        return view('youtube.audits.export_csv',compact('chanalytics'));
    }

    public function audits_excell_export($date)
    {
        $unit = DB::select("SELECT * FROM `users` GROUP BY unit ORDER BY unit");

        $chanalytics = DB::select("SELECT name_channel, unit, `subscribers` FROM (SELECT youtube_analitic.name_channel, users.unit, `user_id`, Concat(Group_concat(youtube_analitic.`subscribers`), ',') AS `subscribers` FROM youtube_analitic INNER JOIN users ON users.id = youtube_analitic.user_id WHERE Date_format(youtube_analitic.created_at, '%Y-%m') = '".$date."' AND users.is_active = 1 GROUP BY youtube_analitic.name_channel, `user_id`) aggr ORDER BY `unit`");

        return view('youtube.audits.export_excel',compact('unit','chanalytics','date'));
    }


    public function audits_export_print($date)
    {
        $unit = DB::select("SELECT * FROM `users` GROUP BY unit ORDER BY unit");

        $chanalytics = DB::select("SELECT name_channel, unit, `subscribers` FROM (SELECT youtube_analitic.name_channel, users.unit, `user_id`, Concat(Group_concat(youtube_analitic.`subscribers`), ',') AS `subscribers` FROM youtube_analitic INNER JOIN users ON users.id = youtube_analitic.user_id WHERE Date_format(youtube_analitic.created_at, '%Y-%m') = '".$date."' AND users.is_active = 1 GROUP BY youtube_analitic.name_channel, `user_id`) aggr ORDER BY `unit`");

        return view('youtube.audits.export_print',compact('unit','chanalytics','date'));
    }

    public function redirectToProvider($provider){
        $client = $this->Callback->GClient_Ytreporting();

        $token = DB::table('user_token')
                ->where('user_id', auth()->user()->id)
                ->where(['type'=> 'youtube','action_dream'=>'connect_cms'])
                ->first();

        if(empty($token)){
            session(['action_dream'=>"connect_cms"]);
            $authUrl = $client->createAuthUrl();
            return redirect($authUrl);
        }
        //dd($token);

        //return "yt ".$provider;
        /*if(!config("services.$provider")) abort('404');
        
        return Socialite::driver($provider)
            ->scopes([
                'https://www.googleapis.com/auth/youtube',
                'https://www.googleapis.com/auth/youtube.force-ssl',
                'https://www.googleapis.com/auth/youtube.readonly',
                'https://www.googleapis.com/auth/youtubepartner-channel-audit',
                'https://www.googleapis.com/auth/youtubepartner',
                // 'https://www.googleapis.com/auth/youtube.upload',
            ])
            ->redirect();*/
    }

    public function getYTPartner($gettoken){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/youtube/partner/v1/contentOwners?fetchMine=true&key=/-KUB0wyU0w0dIJCCkmF6ShS73-syL9zA');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authorization: Bearer '.$gettoken->token;
        $headers[] = 'Accept: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            return $ch;
        }
        curl_close($ch);
        $contentID = json_decode($result, true);
        return($contentID);
    }

    public function AddUserToken($user){
        $insert = DB::table('user_token')->insert([
            'user_id'=>Auth::user()->id,
            'type'=>'youtube',
            'access_token' => $user->accessTokenResponseBody['access_token'],
            'expires_in' => $user->accessTokenResponseBody['expires_in'],
            'scopes' => $user->accessTokenResponseBody['scope'],
            'token_type' => $user->accessTokenResponseBody['token_type'],
            'token_secret' => $user->token,
            'refresh_token' => $user->refreshToken,
        ]);

        return $insert;
    }

    public function getChannellist(){
        $auth = Auth::user();
        $cmslist = YoutubePartner::get();
        $usertoken = UserToken::where('user_id', $auth->id)->get();
        $Secret = DB::table('yt_content_secret')
                    ->where('user_id', auth()->user()->id)
                    ->where('type', 'youtube')
                    ->first();
        //$channel = Youtube::getChannelById('UCmp6P-RWbAoJadTi_iZpHJA');
        ///dd($channel);
        foreach ($cmslist as $cms){            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/youtube/v3/channels?part=contentDetails,snippet,statistics,auditDetails,topicDetails,localizations,status&managedByMe=true&maxResults=50&onBehalfOfContentOwner='.$cms->content_id.'&key='.$Secret->client_id);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

            $headers = array();
            $headers[] = 'Authorization: '.$usertoken[0]->token_type.' '.$usertoken[0]->access_token;
            $headers[] = 'Accept: application/json';
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                echo 'Error:' . curl_error($ch);
            }
            curl_close($ch);
            $contentID = json_decode($result);
            dd($contentID);
        }
       // }
    }

    public function saveChannellist(){
        $Secret = DB::table('yt_content_secret')
                    ->where('user_id', auth()->user()->id)
                    ->where('type', 'youtube')
                    ->first();
        $auth = Auth::user();

        $youtubePartner = YoutubePartner::where([['user_id', '=',$auth->id],['selected', '=', 'Y']])->get()->count();
        if($auth->roles[0]->name == 'user'){
            if($youtubePartner == 0){
                return redirect(route('authentication.network'));
            }
        }

        $token = DB::table('user_token')
                ->where('user_id', auth()->user()->id)
                ->where('type', 'youtube')
               // ->where('action_dream','yt-analytics')
                ->first();

        $callbackUrl = 'http://starhits.com/auth/youtube/callback';
        $client = new \Google_Client();
        $client->setClientId($Secret->client_id);
        $client->setClientSecret($Secret->client_secret);
        $client->setScopes([
            'https://www.googleapis.com/auth/youtube',
            'https://www.googleapis.com/auth/youtube.readonly',
            'https://www.googleapis.com/auth/youtubepartner',
            'https://www.googleapis.com/auth/youtubepartner-channel-audit',
            'https://www.googleapis.com/auth/youtube.force-ssl',
        ]);
        $client->addScope(\Google_Service_YouTubeAnalytics::YT_ANALYTICS_READONLY);
        $client->setRedirectUri($Secret->client_redirect);
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');
        $client->setPrompt('consent select_account');

        $nextPageToken = "";
        if (empty($token)) {
            $authUrl = $client->createAuthUrl();
            return redirect($authUrl);
        }else{
            $client->getAccessToken();
            if($client->isAccessTokenExpired()){ 
                $client->refreshToken($token->refresh_token);
                $accessToken = $client->getAccessToken();
                $client->setAccessToken($accessToken);
                $upd = DB::table('user_token')
                        ->where('user_id', auth()->user()->id)
                        ->where('type', 'youtube')
                        ->update([
                            'access_token'  => $accessToken['access_token'],
                            'refresh_token' => isset($accessToken['refresh_token']) ? $accessToken['refresh_token']:0,
                            'token_type'    => $accessToken['token_type'],
                            'expires_in'    => $accessToken['expires_in'],
                            'created'       => $accessToken['created'],
                            'action_dream'  => "yt-analytics"
                        ]);
            }

            $cmslist = YoutubePartner::where(['selected'=>'Y'])->get();
            // $cmslist = YoutubePartner::where([
            //         'selected'=>'Y',
            //         'content_id'=>'gcQyg_8bQAaTHf15cMGA1g'
            //     ])->get();
            $usertoken = UserToken::where('user_id', $auth->id)->get();
            foreach ($cmslist as $cms){
                $contentID =  $this->getAPICHannels($usertoken,$cms,$Secret,$nextPageToken);
                if(!empty($contentID->nextPageToken)){
                    $this->getAPICHannels($usertoken,$cms,$Secret,$contentID->nextPageToken);
                }
                // return(array($this->getAPICHannels($usertoken,$cms,$Secret,$nextPageToken)));

            }

            return redirect(route('user.cms.index'));
        }
    }

    public function getAPICHannels($usertoken,$cms,$Secret,$nextPageToken){
        $ch = curl_init();

        if($nextPageToken=="" || $nextPageToken==null){
        curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/youtube/v3/channels?part=contentDetails,snippet&managedByMe=true&maxResults=50&onBehalfOfContentOwner='.$cms->content_id.'&key='.$Secret->client_id);
        }else{
            curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/youtube/v3/channels?part=contentDetails,snippet&managedByMe=true&pageToken='.$nextPageToken.'&maxResults=50&onBehalfOfContentOwner='.$cms->content_id.'&key='.$Secret->client_id);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Authorization: '.$usertoken[0]->token_type.' '.$usertoken[0]->access_token;
        $headers[] = 'Accept: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        $contentID = json_decode($result);
        $content = $contentID->items;

        foreach ($content as $key => $value) {
            if(!empty($value->snippet->customUrl)){
                $customUrl=$value->snippet->customUrl;
            }else{
                $customUrl="";
            }

            if(!empty($value->snippet->publishedAt)){
                $publishedAt=$value->snippet->publishedAt;
            }else{
                $publishedAt="";
            }

            if(!empty($value->snippet->country)){
                $country=$value->snippet->country;
            }else{
                $country="";
            }

            if(!empty($value->snippet->thumbnails->default->url)){
                $thumbnails=$value->snippet->thumbnails->default->url;
            }else{
                $thumbnails="";
            }

            $find = YoutubeChannel::where(['channel_id'=>$value->id])->first();
            if(empty($find)){
                YoutubeChannel::insert([
                    'content_id'=>$cms->content_id,
                    'channel_id'=>$value->id,
                    'customerurl'=>$customUrl,
                    'publishat'=> $publishedAt,
                    'country'=> $country,
                    'thumbnails_channel'=> $thumbnails,
                    'user_id'=>auth()->user()->id
                ]);
            }else{
                YoutubeChannel::where(['channel_id'=>$value->id])
                ->update([
                    'content_id'=>$cms->content_id,
                    'customerurl'=>$customUrl,
                    'publishat'=> $publishedAt,
                    'country'=> $country,
                    'thumbnails_channel'=> $thumbnails
                ]);
            }
        }
        return $contentID;
    }

    public function cmslist(){
        $auth = Auth::user();
        $dateNow = date("Y-m-d");
        $youtubePartner = YoutubePartner::where([['user_id', '=',$auth->id],['selected', '=', 'Y']])->get()->count();

        if($auth->roles[0]->name == 'user'){
            if($youtubePartner == 0){
                return redirect(route('authentication.network'));
            }
        }

        $CheckingChannel = YoutubeChannel::where(['user_id'=>$auth->id])->first();
        if(empty($CheckingChannel)){
            session(['action_dream'=>"yt-analytics"]);
            return redirect(route('youtube.analytics.savechannel'));
        }
        // dd($auth->roles[0]->name);

        /*$CheckSubs = DB::table('youtube_partner_analitic')
                            ->where([
                                'user_id'   => auth()->user()->id,
                                'updated_at'=> $dateNow
                            ])->first();
        if(empty($CheckSubs)){
            $this->YtReporting->getStaticChannel();
        }*/

        $cmslist = YoutubePartner::where(['selected'=>'Y'])->get();

        $data = new \stdClass();
        $array = array();

        foreach ($cmslist as $key => $cms) {
            $rows = new \stdClass();

            $ytChannel = YoutubeChannel::where(['content_id'=>$cms->content_id]);
            $countChannel = $ytChannel->count();
            $getChannel = $ytChannel->first();

            //$countVideos = YoutubeVideo::where(['content_id'=>$cms->content_id])->count();

            $ChannelsAnalytic = DB::table('youtube_channel_analitic')
                                ->where([
                                    'content_id'=> $cms->content_id,
                                   // 'date'      => $dateNow,
                                    //'user_id'   => auth()->user()->id
                                ]);

            $ParnerAnalytic = DB::table('youtube_partner_analitic')
                                ->where([
                                    'content_id'=> $cms->content_id,
                                    //'updated_at'      => $dateNow,
                                ]);
                                //->where('uploaderType','!=','');

            $viewCount = $ParnerAnalytic->sum('views');
            $EstimateWatch = $ChannelsAnalytic->sum('estimatedMinutesWatched');
            $subsCount = $ChannelsAnalytic->sum('subscribers');
            $monetizedPlaybacks = $ParnerAnalytic->sum('monetizedPlaybacks');

            $viewCount2 = $ChannelsAnalytic->sum('views');
            $EstimateWatch2 = $ChannelsAnalytic->sum('estimatedMinutesWatched');
            $countVideos = $ChannelsAnalytic->sum('video_count');

            $rows->countchannel=$countChannel;
            $rows->countviews = $viewCount;
            $rows->EstimateWatch = $EstimateWatch;
            $rows->countsub = $subsCount;
            $rows->countVideos = $countVideos;
            $rows->cms=$cms;
            $rows->channel = $getChannel;
            $rows->monetizedPlaybacks = $monetizedPlaybacks;
            array_push($array, $rows);
        }
        $data = $array;

        return view('youtube.cms.index', [
            'youtubePartner' => $youtubePartner,
            'cmslist' => $data
        ]);
        
    }

    /*public function SyncAnaliticYoutube(){
        $dateNow = date("Y-m-d");
        $reload = "";

        $CheckTODAY = DB::table('youtube_channel_analitic')
                    ->where([
                        'user_id'   => auth()->user()->id,
                        'date'      => $dateNow
                    ])->first();

        if(empty($CheckTODAY)){
            $this->YtReporting->getChannelAnalytic();
            $this->YtReporting->getStaticChannel();
            $reload="reload";
        }

        $CheckContentOwner = DB::table('youtube_partner_analitic')
                            ->where([
                                'user_id'   => auth()->user()->id,
                                'updated_at'=> $dateNow
                            ])->first();
        if(empty($CheckContentOwner)){
            $this->YtReporting->getAPIAnaliticContentOwner();
            $reload="reload";
        }

        return $reload;
    }*/

    public function repairingCMSvideo(){
        $youtube = Youtube::getPlaylistItemsByPlaylistId('PLYYllRG75inCwKI4CzloRvpdZ4L91QLvx');
        //$youtube  = Youtube::getVideoInfo('bZ4Hu_kqX88');
        /*foreach ($youtube['results'] as $key => $value) {
            $vid = $value;
            echo $vid."<br>";
        }*/
        dd($youtube['results']);
        /*$channels =YoutubeChannel::all();
        foreach ($channels as $myChannelID) {
            echo "cms :".$myChannelID->content_id.", chan ID : ".$myChannelID->channel_id."<br>";

            YoutubeVideo::where('channel_id', $myChannelID->channel_id)
                        ->update(['content_id' => $myChannelID->content_id]);
        }*/
    }


    public function revokeYt(){
        //$usertoken = UserToken::where('user_id', $auth->id)->get();
        $client = new \Google_Client();
        $client->setClientId(env('GOOGLE_CLIENT_ID'));
        $client->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
        $client->setScopes('https://www.googleapis.com/auth/youtube');
        $client->setRedirectUri(env('GOOGLE_REDIRECT'));
        $client->setAccessType('offline');
        $client->setPrompt('consent select_account');
        $token = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'youtube')->first();
        $token = collect($token)->except(['id', 'user_id', 'type'])->toArray();
        //dd($client);
        //$client->setAccessToken($token);
        $client->revokeToken($token);

        dd($client);
    }


    public function refreshToken(){
        if($client->isAccessTokenExpired()){
            $token = collect($token)->except(['id', 'user_id', 'type'])->toArray();
            $client->refreshToken($token['refresh_token']);
            $accessToken = $client->getAccessToken();
            DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'youtube')->update([
                'access_token'  => $accessToken['access_token'],
                'refresh_token' => $accessToken['refresh_token'],
                'token_type'    => $accessToken['token_type'],
                'expires_in'    => $accessToken['expires_in'],
                'created'       => $accessToken['created']
            ]);
        }
    }

    public function authentication()
    {
        $auth = Auth::user();
        $youtubePartner = YoutubePartner::where([['user_id', '=',$auth->id]])->get()->count();

        if($auth->roles[0]->name == 'user'){
            if($youtubePartner>0){
                return redirect(route('connect.cms'));
            }
        }
        session(['action_dream'=>"connect_cms"]);


        return view('youtube.cms.authentication', [
            'youtubePartner' => $youtubePartner
        ]);
    }

    public function selectcms()
    {
        $auth = Auth::user();
        $youtubePartner = YoutubePartner::where([['user_id', '=',$auth->id],['selected', '=', 'N']])->get()->count();
        $cmslist = YoutubePartner::where([['user_id', '=',$auth->id],['selected', '=', 'N']])->get();

        if($auth->roles[0]->name == 'user'){
            if($youtubePartner == 0){
                return redirect(route('authentication.network'));
            }
        }

        return view('youtube.cms.select_cms', [
            'youtubePartner' => $youtubePartner,
            'cmslist' => $cmslist
        ]);
    }

    public function updateselectedcms(Request $request)
    {
        $auth = Auth::user();
        $youtubePartner = YoutubePartner::where([['user_id', '=',$auth->id],['selected', '=', 'N']])->get()->count();
        $cmslist = YoutubePartner::where([['user_id', '=',$auth->id],['selected', '=', 'N']])->get();

        if($auth->roles[0]->name == 'user'){
            if($youtubePartner == 0){
                return redirect(route('authentication.network'));
            }
        }

        $idYP = $request->check;
        YoutubePartner::whereIn('id', $idYP)->update(array('selected' => 'Y'));
        return redirect(route('user.dashboard.index'));
    }

    public function ListVideosYoutube(){
        
        $myApiKey = env('YOUTUBE_API_KEY'); // Provide your API Key
        $channels =YoutubeChannel::all();
        //$channels =YoutubeChannel::where(['channel_id'=>'UCtuz6YEPdkRqZlms_GRQW3Q'])->get();

        foreach ($channels as $myChannelID) {

            //$checkChannel = YoutubeVideo::where([ 'channel_id' => $myChannelID->channel_id ])->first();
            //if(empty($checkChannel)){
                //echo $myChannelID->channel_id."<br>";
                $myQuery = "https://www.googleapis.com/youtube/v3/search?key=".$myApiKey."&channelId=".$myChannelID->channel_id."&part=snippet,id&maxResults=50";
                //$channelID = "UCtuz6YEPdkRqZlms_GRQW3Q";

                //$myQuery = "https://www.googleapis.com/youtube/v3/search?key=AIzaSyA-KUB0wyU0w0dIJCCkmF6ShS73-syL9zA&channelId=UCtuz6YEPdkRqZlms_GRQW3Q&part=snippet,id&maxResults=50";

                $videoList = file_get_contents($myQuery);
                $decoded = json_decode($videoList);            
                //dd($decoded);

                /*if(!empty($decoded->nextPageToken)){
                    //$myQuery = "https://www.googleapis.com/youtube/v3/search?key=".$myApiKey."&channelId=".$channelID."&part=snippet,id&maxResults=50&pageToken=".$decoded->nextPageToken;

                    $myQuery = "https://www.googleapis.com/youtube/v3/search?key=AIzaSyA-KUB0wyU0w0dIJCCkmF6ShS73-syL9zA&channelId=UCtuz6YEPdkRqZlms_GRQW3Q&part=snippet,id&maxResults=50&pageToken=".$decoded->nextPageToken;
                    $videoList = file_get_contents($myQuery);
                    $decoded = json_decode($videoList);    
                }*/
                //dd($decoded);

                foreach ($decoded->items as $value) {

                    if(!empty($value->id->videoId)){
                        $videoId=$value->id->videoId;
                    }else{
                        $videoId="";
                    }

                    $findVideo = YoutubeVideo::where(['video_id'=>$videoId ])->first();
                    if(empty($findVideo)){
                        YoutubeVideo::insert([
                            'channel_id'    => $this->CheckEmpty($value->snippet->channelId) ,
                            'video_id'      => $videoId,
                            'title_video'   => $this->CheckEmpty($value->snippet->title),
                            'desc_video'    => $this->CheckEmpty($value->snippet->description),
                            'thumbnails'    => $this->CheckEmpty($value->snippet->thumbnails->high->url),
                            'publish_at'    => $this->CheckEmpty($value->snippet->publishedAt),
                            'channel_title' => $this->CheckEmpty($value->snippet->channelTitle)
                        ]);  
                    }
                }
           // }
        } //endforeach
        //$videos = YoutubeVideo::where(['video_id'=>$value->id->videoId])->get();
       // return view('multi_upload.list_videos',compact('videos'));

    }

    public function CheckEmpty($value){
        $datas="";
        if(!empty($value)){
            $datas=$value;
        }else{
            $datas="";
        }
        return $datas;
    }

    public function ListVideosYoutube_backup(){

        $callbackUrl = 'http://starhits.com/auth/youtube/callback';
       /* $client = new \Google_Client();
        $client->setClientId(env('GOOGLE_CLIENT_ID'));
        $client->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
        $client->setScopes('https://www.googleapis.com/auth/youtube');
        $client->addScope(\Google_Service_YouTubeAnalytics::YT_ANALYTICS_READONLY);
        $client->setRedirectUri($callbackUrl);
        $client->setAccessType('offline');
        $client->setPrompt('consent select_account');*/


        $token = DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'youtube')->first();
        $token = collect($token)->except(['id', 'user_id', 'type'])->toArray();
        $users = User::where(['id'=>auth()->user()->id])->first();
        $myApiKey = env('YOUTUBE_API_KEY'); // Provide your API Key
        $myChannelID = $users->provider_id; // Provide your Channel ID
        $maxResults="10"; // Number of results to display
         
        // Make an API call to store list of videos to JSON variable
        $myQuery = "https://www.googleapis.com/youtube/v3/search?key=$myApiKey&channelId=$myChannelID&part=snippet,id&order=date";
        $videoList = file_get_contents($myQuery);
         
        // Convert JSON to PHP Array
        $decoded = json_decode($videoList);

        /*if($client->isAccessTokenExpired()){
            $token = collect($token)->except(['id', 'user_id', 'type'])->toArray();
            $client->refreshToken($token['refresh_token']);
            $accessToken = $client->getAccessToken();
            DB::table('user_token')->where('user_id', auth()->user()->id)->where('type', 'youtube')->update([
                'access_token'  => $accessToken['access_token'],
                'refresh_token' => $accessToken['refresh_token'],
                'token_type'    => $accessToken['token_type'],
                'expires_in'    => $accessToken['expires_in'],
                'created'       => $accessToken['created']
            ]);
        }

        $client->setAccessToken($token);
        */

        // Define service object for making API requests.
        //"https://www.googleapis.com/youtube/v3/search?key=AIzaSyA-KUB0wyU0w0dIJCCkmF6ShS73-syL9zA&channelId=UCqh2cjmc183OJF3kg6uS1MA&part=snippet,id&order=date&maxResults=20
        
        //dd($decoded);
       /* foreach ($decoded as $key => $items) {
            
        }*/

    }


}
