<?php

namespace App\Http\Controllers\Backend;

use App\Model\InfluencerDiagram;
use Illuminate\Http\Request;
use App\Model\DetailInfluencer;
use App\Model\User;
use DB;

class InfluencerDiagramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {   
        $datas = User::where('id',$id)->first();
        $diagram = InfluencerDiagram::where(['user_id'=>$id])->get();
        $details = DetailInfluencer::where(['user_id'=>$id])->first();
        $datas['diagram']=$diagram;
        $datas['details']=$details;
        //return $datas;
        return view('influencer.diagram.index',compact('datas'));
    }

    public function apiDiagram($id){
        $diagram = InfluencerDiagram::selectRaw("
        id, user_id, created_at,
        MONTHNAME(created_at) AS created,
        AVG(followers_ig) AS followers_ig,
        AVG(avglike) AS avglike,
        AVG(avgcomment) AS avgcomment,
        AVG(engagement) AS engagement")->where(['user_id'=>$id])->groupBy(DB::raw('YEAR(created_at), MONTH(created_at)'))->get();
        // $diagram = InfluencerDiagram::where(['user_id'=>$id])->get();
        return $diagram;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\InfluencerDiagram  $influencerDiagram
     * @return \Illuminate\Http\Response
     */
    public function show(InfluencerDiagram $influencerDiagram)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\InfluencerDiagram  $influencerDiagram
     * @return \Illuminate\Http\Response
     */
    public function edit(InfluencerDiagram $influencerDiagram)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\InfluencerDiagram  $influencerDiagram
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InfluencerDiagram $influencerDiagram)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\InfluencerDiagram  $influencerDiagram
     * @return \Illuminate\Http\Response
     */
    public function destroy(InfluencerDiagram $influencerDiagram)
    {
        //
    }
}
