<?php

namespace App\Http\Controllers\Website;

use Alert;
use App\Http\Controllers\Website\Controller;
use App\Model\User;
use App\Model\City;
use App\Model\Country;
use App\Model\CategoryInst;
use App\Model\Bank;
use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Khill\Duration\Duration;

class CreatorsController extends Controller
{
    protected $users;

    /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/


    public function index()
    {
        // $id = 'agatha.chelsea.official';
        // $appid = env('FACEBOOK_CLIENT_ID');
        // $appsecret = env('FACEBOOK_CLIENT_SECRET');

        // $json_url ='https://graph.facebook.com/'.$id.'?access_token='.$appid.'|'.$appsecret.'&fields=fan_count';
        // $json = file_get_contents($json_url);
        // $json_output = json_decode($json);
        // //Extract the likes count from the JSON object
        // if($json_output->fan_count){
        //     return $fan_count = $json_output->fan_count;
        // }else{
        //     return 0;
        // }
        // $creatorsAPI = User::with('socials')
        // ->where('is_active', '=', 1)
        // // ->where('is_show', '=', 1)
        // ->whereNotNull('cover')
        // ->groupBy('slug')->get();

        // return $creatorsAPI;

        // $twitter_username = 'starhitsid,fauzipriyatama,AgathaChelsea18'; 
        // $data_twitter = file_get_contents('https://cdn.syndication.twimg.com/widgets/followbutton/info.json?screen_names='.$twitter_username); 
        // $parsed_twitter =  json_decode($data_twitter,true);
        // $twitter_followers =  $parsed_twitter[0]['followers_count'];

        // return $parsed_twitter;
        return view('frontend.creators.index');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function show($slug)
    {
        $duration = new Duration;

        $creators = User::with(['series', 'totalVideosSeries', 'socials'])->where('slug', $slug)->first();

        if (!$creators) {
            # code...
            return redirect('404');
        }

        // dd($creators);
        $latestVideos = $creators->videos()->orderBy('created_at', 'desc')->get();
        $popularVideos = $creators->videos()->orderBy('viewed', 'desc')->get();
        $latestSeries = $creators->totalVideosSeries()->where('is_active', '=', '1')->orderBy('created_at', 'desc')->get();
        $popularSeries = $creators->totalVideosSeries()->where('is_active', '=', '1')->orderBy('viewed', 'desc')->get();
        $moreVideos = $creators->videos()->get();
        $otherVideos = DB::table('contents as vid')
            ->select('vid.title', 'vid.attr_7', 'ch.title as channeltitle', 'vid.image', 'vid.attr_6', 'vid.slug')
            ->join(DB::raw('(SELECT * FROM contents WHERE type = \'channel\') as ch'), 'ch.attr_1', '=', 'vid.attr_1')
            ->where('vid.type', '=', 'video')
            ->where('vid.is_active', '=', '1')
            ->groupBy('ch.attr_1')
            ->orderBy('vid.created_at', 'desc')
            ->inRandomOrder()
            ->limit(4)
            ->get();
        $recomendedCreators = $creators->videos()->where('is_featured', '=', '1')->inRandomOrder()->get();
        $metaTag = User::where('slug', '=', $slug)->first();
        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();
        $categories = CategoryInst::all();
        $banks = Bank::all()->pluck('name', 'id')->toArray();

        return view('frontend.creators.show',
            ['countries'    => $countries,
            'cities'    => $cities,
            'categories'    => $categories,
            'banks' => $banks], 
            compact('duration', 'creators', 'latestVideos', 'popularVideos', 'latestSeries', 'popularSeries', 'moreVideos', 'otherVideos', 'recomendedCreators', 'metaTag'));
    }

    public function details($slug)
    {
        if ($slug=='corporate'){
            return view('frontend.creators.corporate');
        }elseif ($slug=='personal'){
            return view('frontend.creators.personal');
        }else{
        $duration = new Duration;

        $creators = User::with(['series', 'totalVideosSeries', 'socials'])->where('slug', $slug)->first();

        // return $creators;

        if (!$creators) {
            # code...
            return redirect('404');
        }

        // dd($creators);
        $latestVideos = $creators->videos()->orderBy('created_at', 'desc')->limit('3')->get();
        $popularVideos = $creators->videos()->orderBy('viewed', 'desc')->get();
        $latestSeries = $creators->totalVideosSeries()->where('is_active', '=', '1')->orderBy('created_at', 'desc')->get();
        $popularSeries = $creators->totalVideosSeries()->where('is_active', '=', '1')->orderBy('viewed', 'desc')->get();
        $moreVideos = $creators->videos()->get();
        $otherVideos = DB::table('contents as vid')
            ->select('vid.title', 'vid.attr_7', 'ch.title as channeltitle', 'vid.image', 'vid.attr_6', 'vid.slug')
            ->join(DB::raw('(SELECT * FROM contents WHERE type = \'channel\') as ch'), 'ch.attr_1', '=', 'vid.attr_1')
            ->where('vid.type', '=', 'video')
            ->where('vid.is_active', '=', '1')
            ->groupBy('ch.attr_1')
            ->orderBy('vid.created_at', 'desc')
            ->inRandomOrder()
            ->limit(4)
            ->get();
        $recomendedCreators = $creators->videos()->where('is_featured', '=', '1')->inRandomOrder()->get();
        $metaTag = User::where('slug', '=', $slug)->first();
        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();
        $categories = CategoryInst::all();
        $banks = Bank::all()->pluck('name', 'id')->toArray();

        // return $latestVideos;

        return view('frontend.creators.details',
            ['countries'    => $countries,
            'cities'    => $cities,
            'categories'    => $categories,
            'banks' => $banks], 
            compact('duration', 'creators', 'latestVideos', 'popularVideos', 'latestSeries', 'popularSeries', 'moreVideos', 'otherVideos', 'recomendedCreators', 'metaTag'));
        }
    }

    public function videolist($slug)
    {
        // $duration = new Duration;

        $creators = User::with(['series', 'totalVideosSeries', 'socials'])->where('slug', $slug)->first();

        // if (!$creators) {
        //     # code...
        //     return redirect('404');
        // }

        // // dd($creators);
        // $latestVideos = $creators->videos()->orderBy('created_at', 'desc')->get();
        // $popularVideos = $creators->videos()->orderBy('viewed', 'desc')->get();
        // $latestSeries = $creators->totalVideosSeries()->where('is_active', '=', '1')->orderBy('created_at', 'desc')->get();
        // $popularSeries = $creators->totalVideosSeries()->where('is_active', '=', '1')->orderBy('viewed', 'desc')->get();
        // $moreVideos = $creators->videos()->get();
        // $otherVideos = DB::table('contents as vid')
        //     ->select('vid.title', 'vid.attr_7', 'ch.title as channeltitle', 'vid.image', 'vid.attr_6', 'vid.slug')
        //     ->join(DB::raw('(SELECT * FROM contents WHERE type = \'channel\') as ch'), 'ch.attr_1', '=', 'vid.attr_1')
        //     ->where('vid.type', '=', 'video')
        //     ->where('vid.is_active', '=', '1')
        //     ->groupBy('ch.attr_1')
        //     ->orderBy('vid.created_at', 'desc')
        //     ->inRandomOrder()
        //     ->limit(4)
        //     ->get();
        // $recomendedCreators = $creators->videos()->where('is_featured', '=', '1')->inRandomOrder()->get();
        // $metaTag = User::where('slug', '=', $slug)->first();
        // $countries = $this->users->getCountries();
        // $cities = $this->users->getcities();
        // $categories = CategoryInst::all();
        // $banks = Bank::all()->pluck('name', 'id')->toArray();

        // return $latestSeries;

        return view('frontend.creators.video-list',compact('creators','slug'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function filter()
    {
        $models = array();
        $type   = '';
        $sortby = '';
        if (isset($_POST['search'])) {

            $type   = $_POST['type'];
            $sortby = $_POST['sortby'];

            switch ($sortby) {
                case '1':
                case 1:
                    $sort = 'viewed'; // vewed
                    break;
                case '2':
                case 2:
                    $sort = 'attr_2'; // liked
                    break;
                case '3':
                case 3:
                    $sort = 'attr_4'; // comment
                    break;
                case '4':
                case 4:
                    $sort = 'created_at'; // date publish
                    break;

                default:
                    # code...
                    Alert::info('Sort Cannot Be Empty!');
                    return redirect()->back();
                    break;
            }

            if (isset($sort)) {

                // creators videos
                $creators = User::with(['series', 'totalVideosSeries'])->where('name', 'like', '%' . $type . '%')->first();
                if (!empty($creators)) {

                    $latestVideos = $creators->videos()->orderBy($sort, 'desc')->get();

                    if (sizeof($latestVideos) > 0) {
                        foreach ($latestVideos as $key => $value) {
                            $models[] = array(
                                'id'         => $value->id,
                                'title'      => $value->title,
                                'image'      => $value->image,
                                'created_at' => $value->created_at,
                                'slug'       => $value->slug,
                            );
                        }
                    }
                }

                if (sizeof($models) <= 0) {
                    Alert::info('There is no data to display!');
                    return redirect()->back();
                }

                $models = Collection::make($models)->take(20);
            }
        }

        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();
        $categories = CategoryInst::all();
        $banks = Bank::all()->pluck('name', 'id')->toArray();

        return view('frontend.creators.filter',
            ['countries'    => $countries,
            'cities'    => $cities,
            'categories'    => $categories,
            'banks' => $banks],  
            compact('models', 'type', 'sort'));
    }
}
