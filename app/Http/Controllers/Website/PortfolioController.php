<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Website\Controller;
use App\Model\User;
use App\Model\City;
use App\Model\Country;
use App\Model\CategoryInst;
use App\Model\Bank;
use App\Model\Portofolio;
use App\Model\ContentPortofolio;
use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Khill\Duration\Duration;

class PortfolioController extends Controller
{
    protected $users;

    /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/


    public function index()
    {
        return view('frontend.portfolio.index');
    }

    public function details($slug)
    {
        $datas = Portofolio::join('users', 'content_portofolio.id_user', '=', 'users.id')
                ->where(['id_portofolio'=>$slug])
                ->first();

        $creator = User::whereHas('accessrole', function ($query) {
                        $query->where('role_id', '2');
                    })->orderBy('name','asc')
                    ->get();

        // return $datas;

        $string = $datas->media_content;
        $media_content = explode(',',$string);

        return view('frontend.portfolio.details', [
            'datas'         => $datas,
            'creator'       => $creator,
            'media_content' => $media_content
        ]);
    }
}
