<?php

namespace App\Http\Controllers\Website;

use Alert;
use App\Http\Controllers\Website\Controller;
use App\Model\Content;
use App\Model\Page;
use App\Model\User;
use App\Model\SiteConfig;
use App\Model\City;
use App\Model\Country;
use App\Model\CategoryInst;
use App\Model\Bank;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Newsletter;

class PageController extends Controller
{

    protected $users;

    /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /* public function __construct()
    {
        // $this->middleware('auth');
    } */

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function page($slug)
    {
        $pagesDynamic = Page::where('slug', '=', $slug)->first();

        $metaTag = Content::where('slug', '=', $slug)->first();

        // dd($metaTag->toArray());

        $contacts = SiteConfig::all();
        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();
        $categories = CategoryInst::all();
        $banks = Bank::all()->pluck('name', 'id')->toArray();
        
        $models = array();
        if(sizeof($contacts)>0){
            foreach ($contacts as $key => $value) {
                $models[$value->key] = $value->value;
            }
        }

        return view('frontend.partials._page-dynamics',
            ['countries'    => $countries,
            'cities'    => $cities,
            'categories'    => $categories,
            'banks' => $banks],
            compact('pagesDynamic', 'metaTag', 'models'));
    }

    public function about()
    {
        return view('frontend.pages.about');
    }

    public function mcn()
    {
        return view('frontend.pages.mcn');
    }

    public function contact()
    {
        return view('frontend.pages.contact');
    }

    public function policy()
    {
        return view('frontend.pages.policy');
    }
    
    public function term()
    {
        return view('frontend.pages.term');
    }

    public function faq()
    {
        return view('frontend.pages.faq');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function join()
    {
        $joinUs = Content::where('type', '=', 'join')->get();

        $partnerShip = Content::where('type', '=', 'partnership')->get();

        $someCreators = User::with('videos')->where('is_active', '=', 1)
            ->whereHas('roles', function ($q) {
                $q->where('name', 'creator');
            })
            ->inRandomOrder()
            ->limit(8)
            ->get();

        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();
        $categories = CategoryInst::all();
        $banks = Bank::all()->pluck('name', 'id')->toArray();

        return view('frontend.join', 
            ['countries'    => $countries,
            'cities'    => $cities,
            'categories'    => $categories,
            'banks' => $banks],
            compact('joinUs', 'partnerShip', 'someCreators'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function newsletterStore(Request $request)
    {
        if (!Newsletter::isSubscribed($request->subsc_email)) {
            Newsletter::subscribePending($request->subsc_email);

            Alert::info('Thanks for subscription.');
            return redirect()->back();
        }

        Alert::info('You are already subscribe!');
        return redirect()->back();
    }
}
