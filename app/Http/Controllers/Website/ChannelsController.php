<?php

namespace App\Http\Controllers\Website;

use App\Model\Channel;
use App\Repositories\UserRepository;
use App\Http\Controllers\Website\Controller;

class ChannelsController extends Controller
{
    protected $users;

    /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }
    
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function index()
    {   
        $channels = Channel::where('is_active', '=', '1')->orderBy('sorting', 'asc')->get();
        // return ($channels);
        return view('frontend.channels.index', compact('channels'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function show($slug)
    {   

        if ($slug == 'music') {
            return view('frontend.channels.modules.music');
        } elseif($slug == 'kids'){
            return view('frontend.channels.modules.kids');
        } elseif($slug == 'entertainment'){
            return view('frontend.channels.modules.entertainment');
        } elseif($slug == 'lifestyle'){
            return view('frontend.channels.modules.lifestyle');
        } elseif($slug == 'infotainment'){
            return view('frontend.channels.modules.infotainment');
        } else{
            return redirect('404');
        }
        
    }
}
