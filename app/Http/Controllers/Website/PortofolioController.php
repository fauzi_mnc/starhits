<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
//use App\Http\Controllers\Controller;
use Storage;
use Flash;

use App\Http\Controllers\Backend\Controller;
use App\Model\User;
use App\Model\Portofolio;
use App\Model\ContentPortofolio;


class PortofolioController extends Controller
{
   
    public function index()
    {
        // Portofolio::all();
        $datas = Portofolio::join('users', 'content_portofolio.id_user', '=', 'users.id')->get();
       // return $datas;
        return view('portofolio.index', compact('datas'));
    }

   
    public function create()
    {
        $creator = User::whereHas('accessrole', function ($query) {
            $query->where('role_id', '2');
        })->orderBy('name','asc')
        ->get();

        //return $creator;

        return view('portofolio.create', compact('creator'));
    }

    public function store(Request $request)
    {
        $input = $request->except('_token');

        if($request->has('porto_img1')){
            $path1 = 'uploads/portofolio/'.str_random(32).'.png';
            $image = Storage::disk('local_public')->put($path1, file_get_contents($input['porto_img1']));
            $input['porto_img1'] = $path1;
        }
        if($request->has('porto_img2')){
            $path2 = 'uploads/portofolio/'.str_random(32).'.png';
            $image = Storage::disk('local_public')->put($path2, file_get_contents($input['porto_img2']));
            $input['porto_img2'] = $path2;
        }
        if($request->has('porto_header')){
            $path3 = 'uploads/portofolio/'.str_random(32).'.png';
            $image = Storage::disk('local_public')->put($path3, file_get_contents($input['porto_header']));
            $input['porto_header'] = $path3;
        }

        $user = ContentPortofolio::create($input);

        if($user){
            Flash::success('Create portofolio saved successfully.');
            return redirect(route('portofolio.index'));
        }else{
            Flash::error('Create portofolio saved successfully.');
            return redirect(route('portofolio.index'));
        }
        
    }

    public function show($id)
    {
        
    }

    public function edit($id)
    {
        $datas = Portofolio::join('users', 'content_portofolio.id_user', '=', 'users.id')
                ->where(['id_portofolio'=>$id])
                ->first();

        $creator = User::whereHas('accessrole', function ($query) {
                        $query->where('role_id', '2');
                    })->orderBy('name','asc')
                    ->get();

        return view('portofolio.edit', [
            'datas'  => $datas,
            'creator'=> $creator
        ]);
    }


    public function update(Request $request, $id)
    {
        $input = $request->except('_token','_method');
        
        $edits = ContentPortofolio::find($id);

        $porto_img1 = $edits->porto_img1;
        $porto_img2 = $edits->porto_img2;
        $porto_header = $edits->porto_header;

        if($request->has('porto_img1')){
            $path1 = 'uploads/portofolio/'.str_random(32).'.png';
            $image = Storage::disk('local_public')->put($path1, file_get_contents($request->porto_img1));
            $porto_img1 = $path1;
        }
        if($request->has('porto_img2')){
            $path2 = 'uploads/portofolio/'.str_random(32).'.png';
            $image2 = Storage::disk('local_public')->put($path2, file_get_contents($request->porto_img2));
            $porto_img2 = $path2;
        }
        if($request->has('porto_header')){
            $path3 = 'uploads/portofolio/'.str_random(32).'.png';
            $image3 = Storage::disk('local_public')->put($path3, file_get_contents($request->porto_header));
            $porto_header = $path3;
        }

        $edits->porto_title = $request->porto_title;
        $edits->porto_header = $porto_header;
        $edits->porto_content = $request->porto_content;
        $edits->content1 = $request->content1;
        $edits->porto_img1 = $porto_img1;
        $edits->porto_img2 = $porto_img2;
        $edits->campaign_title = $request->campaign_title;
        $edits->campaign_content = $request->campaign_content;
        $edits->id_content = $request->id_content;
        $edits->isi_content = $request->isi_content;
        $edits->media_content = $request->media_content;
        $edits->id_user = $request->id_user;
        $edits->save();
    
        /*'porto_title',
        'porto_header',
        'porto_content',
        'porto_img1',
        'porto_img2',
        'campaign_title',
        'campaign_content',
        'id_content',
        'isi_content',
        'media_content',
        'id_user'*/
       // $user = ContentPortofolio::update($input,$id);


        if($edits){
            Flash::success('Update portofolio saved successfully.');
            return redirect(route('portofolio.index'));
        }else{
            Flash::error('Update portofolio saved successfully.');
            return redirect(route('portofolio.index'));
        }
    }


    public function destroy($id)
    {
        $data = ContentPortofolio::find($id);
        $data->delete();

        if($data){
            Flash::success('Delete portofolio saved successfully.');
            return redirect(route('portofolio.index'));
        }else{
            Flash::error('Delete portofolio saved successfully.');
            return redirect(route('portofolio.index'));
        }    }
}
