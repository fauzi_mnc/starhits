<?php

namespace App\Http\Controllers\Website;

use Alert;
use App\Http\Controllers\Website\Controller;
use App\Model\Article;
use App\Model\Content;
use App\Model\Series;
use App\Model\User;
use App\Model\Video;
use App\Model\City;
use App\Model\Country;
use App\Model\CategoryInst;
use App\Model\Bank;
use App\Repositories\UserRepository;
use Auth;
use Illuminate\Support\Facades\DB;
use Khill\Duration\Duration;
use Youtube;

class VideosController extends Controller
{
    protected $users;

    /**
     * Create a new controller instance.
     *
     * @param  PageRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }
    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function index()
    {
        return view('frontend.pages.videos');
    }

    public function videos($slug)
    {
        $duration = new Duration;
        
        $singleVideos = Video::with(['users', 'channelVideo', 'series'])
            ->where('slug', '=', $slug)
            ->first();
            
        $videos = $singleVideos->users->videos->take(10);

        $singleVideos->link = '';
        if (isset($singleVideos->attr_5)) {
            $link =
            '<iframe id="player" src="//www.metube.id/embed/' . $singleVideos->attr_6 . '" frameborder="0" allowfullscreen></iframe>';
            if ($singleVideos->attr_5 == 'youtube') {
                $link = '
                <iframe id="player" src="https://www.youtube.com/embed/' . $singleVideos->attr_6 . '?modestbranding=1&showinfo=1&fs=1&autoplay=1&rel=0" allowFullScreen="allowFullScreen"></iframe>';
            }
            $singleVideos->link = $link;
        }

        $userVideos = User::where('id', '=', $singleVideos->user_id)->first();

        $moreVideos = $userVideos->videos()->where('id', '!=', $singleVideos->id)->orderBy('created_at', 'desc')->limit('8')->get();

        $recomendedSeries = Series::with(['videos', 'channels', 'users'])
            ->where(function ($query) {
                $query->where('is_featured', '=', '1')
                    ->where('is_active', '=', '1');
            })
            ->get();

        $relatedVideos = DB::table('contents as c')
            ->select('c.slug', 'c.title', 'c.attr_6', 'c.attr_7', 'ct.content_id', 't.name as termname')
            ->join('contents_terms as ct', 'ct.content_id', '=', 'c.id')
            ->join('terms as t', 't.id', '=', 'ct.term_id')
            ->whereRaw('t.id IN (SELECT term_id FROM contents_terms WHERE content_id = ' . $singleVideos->id . ')')
            ->where('ct.content_id', '!=', $singleVideos->id)
            ->where('c.is_active', '=', '1')
            ->groupBy('slug')
            ->get();

        $bookmarkClass = '';
        if (isset(Auth::user()->id)) {

            $bookmark = DB::table('bookmark')
                ->select('*')
                ->where('content_id', '=', $singleVideos->id)
                ->where('user_id', '=', Auth::user()->id)
                ->get();

            $bookmarkClass = $bookmark->isNotEmpty() ? '' : 'active';
        }

        $metaTag = Content::where('slug', '=', $slug)->first();
        
        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();
        $categories = CategoryInst::all();
        $banks = Bank::all()->pluck('name', 'id')->toArray();

        // return $userVideos;

        return view('frontend.creators.video-detail',
           ['countries'    => $countries,
            'cities'    => $cities,
            'categories'    => $categories,
            'banks' => $banks],
            compact('duration', 'singleVideos', 'userVideos', 'videos', 'moreVideos', 'recomendedSeries', 'relatedVideos', 'bookmarkClass', 'metaTag'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function article($slug)
    {
        $duration = new Duration;

        $singleArticle = Article::with(['users', 'channelVideo', 'series'])->where('slug', '=', $slug)->first();

        $articles = $singleArticle->users->articles->take(10);

        $singleArticle->link = '';
        if (isset($singleArticle->attr_5)) {
            $link =
            '<iframe src="//www.metube.id/embed/' . $singleArticle->attr_6 . '" frameborder="0" allowfullscreen></iframe>';
            if ($singleArticle->attr_5 == 'youtube') {
                $link = '
                <iframe id="player" allowFullScreen="allowFullScreen" src="https://www.youtube.com/embed/' . $singleArticle->attr_6 . '?modestbranding=1&showinfo=1&fs=1&autoplay=1&rel=0" allowFullScreen="allowFullScreen"></iframe>';
            }
            $singleArticle->link = $link;
        }

        $userVideos = User::where('id', '=', $singleArticle->user_id)->first();

        $moreArticles = $userVideos->articles()->where('id', '!=', $singleArticle->id)->orderBy('created_at', 'desc')->get();

        $recomendedSeries = Series::with(['videos', 'channels', 'users', 'articles'])->where(function ($query) {
            $query->where('is_featured', '=', '1')
                ->where('is_active', '=', '1');
        })->get();

        $relatedArticles = DB::table('contents as c')
            ->select('c.slug', 'c.title', 'c.attr_6', 'c.attr_7', 'c.image', 'ct.content_id', 't.name as termname')
            ->join('contents_terms as ct', 'ct.content_id', '=', 'c.id')
            ->join('terms as t', 't.id', '=', 'ct.term_id')
            ->whereRaw('t.id IN (SELECT term_id FROM contents_terms WHERE content_id = ' . $singleArticle->id . ')')
            ->where('ct.content_id', '!=', $singleArticle->id)
            ->where('c.is_active', '=', '1')
            ->groupBy('slug')
            ->get();

        $bookmarkClass = '';
        if (isset(Auth::user()->id)) {

            $bookmark = DB::table('bookmark')
                ->select('*')
                ->where('content_id', '=', $singleArticle->id)
                ->where('user_id', '=', Auth::user()->id)
                ->get();

            $bookmarkClass = $bookmark->isNotEmpty() ? '' : 'active';
        }

        $countries = $this->users->getCountries();
        $cities = $this->users->getcities();
        $categories = CategoryInst::all();
        $banks = Bank::all()->pluck('name', 'id')->toArray();

        return view('frontend.partials._single-article',
            ['countries'    => $countries,
            'cities'    => $cities,
            'categories'    => $categories,
            'banks' => $banks],
            compact('duration', 'singleArticle', 'articles', 'moreArticles', 'recomendedSeries', 'relatedArticles', 'bookmarkClass'));
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function bookmark($id)
    {
        $bookmark = DB::table('bookmark')
            ->select('*')
            ->where('content_id', '=', $id)
            ->where('user_id', '=', Auth::user()->id)
            ->get();

        if (!$bookmark->isNotEmpty()) {
            DB::table('bookmark')->insert(
                ['content_id' => $id,
                    'user_id'     => Auth::user()->id]
            );
            Alert::info('Bookmark has been saved !!');
        } else {
            DB::table('bookmark')
                ->where('content_id', '=', $id)
                ->where('user_id', '=', Auth::user()->id)
                ->delete();
            Alert::info('Bookmark has been deleted !!');
        }

        return back();
    }
}
