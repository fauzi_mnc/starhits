<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Website\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repositories\UserRepository;
use App\Model\SiteConfig;
use App\Model\User;
use App\Model\Bank;
use App\Model\Socials;
use App\Model\CategoryInst;
use App\Model\VerifyUser;
use App\Mail\VerifyInfluencerMail;
use App\Repositories\RoleRepository;
use App\DataTables\InfluencerDataTable;
use Flash;
use App\Http\Requests\StoreInfluencerRequest;
use Storage;
use Mail;

class registerInfluencerController extends Controller
{
    /**
     * The page repository instance.
     */
    protected $users;
    protected $roles;
    
    public function __construct(UserRepository $users, RoleRepository $roles)
    {
        $this->users = $users;
        $this->roles = $roles;
    }

    public function create()
    {
        $countries = $this->users->getCountries();
        $province = $this->users->getProvince();
        $cities = $this->users->getcities();
        $banks = Bank::all()->pluck('name', 'id')->toArray();
        $categories = CategoryInst::all();

        return view('frontend.partials._modals-register-influencer', [
            'countries'   => $countries,
            'province'      => $province,
            'cities'      => $cities,
            'banks'       => $banks,
            'categories'  => $categories
        ]);
    }

    public function store(StoreInfluencerRequest $request) {
        $user = [
            'name'      => $request->name,
            'phone'     => $request->phone,
            'country'   => $request->country,
            'password'  => $request->password,
            'category'  => $request->category,
            'email'     => $request->email,
            'gender'    => $request->gender,
            'city'      => $request->city,
            'is_active' => 0
        ];
        $influencerDetail = $request->only([
            'category',
            //'bank_id',
            //'bank_account_number',
            //'bank_holder_name',
            //'bank_location',
            //'npwp',
            'username',
            'followers_ig',
            'profile_picture_ig'
        ]);
        $influencerDetail['category'] = implode(',', $request->category);

       // dd($user);

        $user = $this->users->create($user);

        $user->detailInfluencer()->create($influencerDetail);

        $influencerId = $this->roles->findWhere([
            'name'=>'influencer'
        ])->first()->id;

        $user->roles()->sync([$influencerId]);
        $verifyUser = VerifyUser::create([
            'user_id' => $user->id,
            'token' => str_random(40)
        ]);
        Mail::to($user->email)->send(new VerifyInfluencerMail($user));
        //$data = array('name'=>"Sam Jose", "body" => "Test mail");

        /*Mail::send('email.registerInfluencerEmail', $data, function($message) use ($user) {
            $message->to($user->email, "received")
                    ->subject('Artisans Web Testing Mail');
            $message->from('fauzi.mncgroup@gmail.com','Artisans Web');
        });*/

        Flash::success('successfully register. Please check your email to activation your account.');
        return redirect()->back();
    }
}