<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class YoutubeAnaliticVideo extends Model
{
    protected $table    = 'youtube_analitic_video';

    public $timestamps	= true;
}
