<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class YoutubeChannel extends Model
{
    protected $table    = 'youtube_channel';

    public $timestamps	= false;
}
