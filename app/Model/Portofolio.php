<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Portofolio extends Model
{
    protected $table = 'content_portofolio';
    protected $primaryKey = 'id_portofolio';
	protected $fillable = [
		'id_user',
		'porto_title',
		'porto_header',
		'porto_content',
		'porto_img1',
		'porto_img2',
		'campaign_title',
		'campaign_content',
		'id_content',
		'isi_content',
		'media_content',
	];
	public $timestamps = true;
}
