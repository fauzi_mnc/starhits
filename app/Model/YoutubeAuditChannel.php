<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class YoutubeAuditChannel extends Model
{
    protected $table    = 'youtube_video_audit_channel';
    public $timestamps  = true;
}
