<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SocialFollowers extends Model
{
    protected $table    = 'social_followers';
    protected $primaryKey = "id_social_followers";
    ///public $incrementing = false;
	public $timestamps	= true;

    protected $fillable = [
        'user_id','social_id', 'screen_name', 'followers','link','type'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'id');
    }
}
