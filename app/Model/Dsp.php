<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Dsp extends Model
{
	protected $table = 'dsp';
	protected $fillable = [
		'reporting_month'
		];
		
		function detail()
		{
				return $this->belongsToMany(Dsp::class, 'dsp_details', 'dsp_id')
										->withPivot('sales_month', 'platform', 'country', 'label_name', 'artist_name', 'release_title', 'track_title', 'upc', 'isrc', 'release_catalog_nb', 'release_type', 'sales_type', 'quantity', 'client_pay', 'unit_price', 'mechanic', 'gross_revenue', 'client_share', 'net_revenue_eur', 'net_revenue_idr');
		}
}
