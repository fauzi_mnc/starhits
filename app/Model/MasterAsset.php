<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MasterAsset extends Model
{
    //
    protected $table   = 'master_asset';
    public $timestamps = true;
}
