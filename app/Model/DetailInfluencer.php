<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DetailInfluencer extends Model
{
	protected $table = 'detail_influencer';
	public $timestamps  = false;
	protected $fillable = [
		'user_id', 'bank_id', 'bank_account_number', 'bank_holder_name', 'bank_location', 'npwp', 'instagram_video_posting_rate',
		'instagram_story_posting_rate', 'instagram_photo_posting_rate', 'youtube_posting_rate', 'category', 'username', 'ig_highlight_rate','package_rate','followers_ig','profile_picture_ig'
	];

	public function influencer()
	{
		return $this->belongsTo('App\model\User');
	}

	public function bank()
	{
		return $this->belongsTo('App\Model\Bank');
	}
}
