<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Nego extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $table    = 'nego';
    protected $fillable = ['camp_id', 'infl_id', 'post_id', 'post_budget'];
    
    protected $auditInclude = [
        'title',
        'content',
    ];
}
