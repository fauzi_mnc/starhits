<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ContentPortofolio extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;

	protected $table    = 'content_portofolio';
	public $timestamps  = true;
    protected $primaryKey = 'id_portofolio';
    protected $fillable = [
        'img_thumbnail','porto_type','porto_title','porto_header','porto_content','content1'
        ,'porto_img1','porto_img2','campaign_title','campaign_content',
        'id_content','isi_content','media_content','id_user','porto_views','porto_engagement','porto_performance'
    ];

}
