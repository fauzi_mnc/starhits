<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ContentSecret extends Model
{
	protected $table    = 'yt_content_secret';
	protected $primary = 'id_secret';
    protected $fillable = [
		'user_id', 'client_id','client_secret', 'client_redirect', 'key','type'
	];
    public $timestamps  = true;
}
