<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class YoutubeVideo extends Model
{
    protected $table    = 'youtube_video';
    public $timestamps  = false;

	protected $fillable = [
		'idvideo_upload',
		'channel_id',
		'video_id',
		'title_video',
		'desc_video',
		'thumbnails',
		'publish_at',
		'channel_title'
	];
}
