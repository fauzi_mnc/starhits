<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class YoutubeAudit extends Model
{
    protected $table    = 'youtube_video_audit_cms';
    public $timestamps  = true;

}
