<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    protected $table    = 'contacts';
    protected $primaryKey = "id";
    ///public $incrementing = false;
    public $timestamps	= true;
    protected $fillable = [
        'name', 'email', 'company', 'subject', 'message','is_read'
    ];
}
