<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RevenueApprover extends Model
{

	protected $table    = 'revenue_approver';
    protected $fillable = [
        'name', 'email', 'is_active', 'type', 'created_at', 'updated_at',
    ];

}
