<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RevenueCreator extends Model
{
    protected $table    = 'revenue_creator';
    public $timestamps = false;
    protected $fillable = [
		'revenue_id', 'creator_id', 'revenue', 'cost_production', 'nett_revenue', 'share_revenue','date_payment','status_paid','status_total'
    ];


}
