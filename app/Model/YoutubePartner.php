<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class YoutubePartner extends Model
{
    protected $table    = 'youtube_partner';
    public $timestamps  = true;

	protected $fillable = [
		'id', 'user_id', 'content_id', 'email_primary', 'email_conflict', 'email_dispute', 'email_fingerprintreport'
	];
}