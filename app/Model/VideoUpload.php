<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VideoUpload extends Model
{ 
    protected $table    = 'youtube_video_uploads';
	public $timestamps	= true;
    protected $primaryKey = 'idvideo_uploads';

	protected $fillable = [
		'datetime', 'filename', 'url_file', 'group_upload'
	];
}
