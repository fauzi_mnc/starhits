<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ContentUsers extends Model implements Auditable
{
	use \OwenIt\Auditing\Auditable;

	protected $table    = 'content_users';
	public $timestamps  = false;
    protected $fillable = [
        'content_id', 'user_id', 'created_at', 'updated_at'
    ];

    protected $auditInclude = [
        'title',
        'content',
    ];
}
