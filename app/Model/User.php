<?php

namespace App\Model;

use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;
use Auth;

class User extends Authenticatable implements AuditableContract
{
    use Auditable;
    use Notifiable;
    use EntrustUserTrait;
    protected $appends = ['totalVideos', 'totalSeries', 'totalVideosSeries'];
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $dates = ['dob', 'last_sign_in', 'current_sign_in'];
    protected $with = ['roles'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'name_master', 'name_channel', 'email', 'password', 'biodata', 'slug', 'password', 'image', 'cover',
        'provider_id', 'subscribers','followers_ig','followers_fb','followers_twitter', 'percentage', 'is_active', 'brand_name', 'phone', 'is_login', 'city',
        'gender','is_feature_page','is_feature_home','img_header','img_content','feature_home','feature_page','feature_sort_home','feature_sort_page','corporate_creator','unit'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'is_login'
    ];

    protected $auditInclude = [
        'title',
        'content',
    ];

    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    public function verifyUser()
    {
        return $this->hasOne('App\Model\VerifyUser');
    }
    
    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    function videos()
    {
        return $this->hasMany(Video::class, 'user_id')
                    ->where('is_active', '=', '1');
    }

    public function role()
    {
        return $this->belongsTo('App\Model\Role');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    function articles()
    {
        return $this->hasMany(Article::class, 'user_id')
                    ->where('is_active', '=', '1');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    function videosPopular()
    {
        return $this->hasMany(Video::class, 'user_id')
                    ->orderBy('viewed', 'desc');
    }

    public function scopeOfRole($query, $roleName)
    {
        return $query->whereHas('roles', function($q){
            $q->where('name', $roleName);
        });
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function gettotalVideosAttribute()
    {
        return $this->videos()->count();
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function gettotalVideosSeriesAttribute()
    {
        return '';
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function series()
    {
        return $this->hasMany(Content::class, 'user_id')
            ->whereIn('type', ['starhits', 'starpro'])
            ->orderBy('created_at', 'desc');
    }

    public function campaign()
    {
        return $this->hasMany(Campaign::class, 'brand_id');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function totalSeries()
    {
        return $this->hasMany(Content::class, 'user_id')
            ->where('is_active', '=', '1')
            ->whereIn('type', ['starhits', 'starpro']);
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    public function totalVideosSeries()
    {
        return $this->hasMany(Series::class, 'user_id');
    }

    /**
     * undocumented function
     *
     * @return void
     * @author
     **/
    public function gettotalSeriesAttribute()
    {
        return $this->totalSeries()->count();
    }

    /**
     * undocumented function
     *
     * @return void
     * @author 
     **/
    public function socials()
    {
        return $this->hasMany(Socials::class, 'user_id')->whereIn('name', ['fb','ig','twitter'])->orderBy('name', 'asc');
    }

    public function bookmarkPost()
    {
        return $this->belongsToMany('App\model\Posts', 'bookmark', 'user_id', 'content_id')->withPivot('user_id', 'content_id');
    }

    public function detailInfluencer()
    {
        return $this->hasOne('App\Model\DetailInfluencer');
    }

    function influencer()
    {
        return $this->hasMany(Influencer::class, 'user_id');
    }

    function masterlagu()
    {
        return $this->hasMany(MasterLagu::class, 'id_user');
    }

    function rolelagu(){
        return $this->hasMany(RoleLagu::class, 'id_user');
    }

    public function accessrole(){
        return $this->hasMany(AccessRole::class,'user_id');
    }

    public function groupuser(){
        return $this->hasMany(UserGroup::class,'user_master');
    }

    function revenue()
    {
        return $this->belongsToMany(Revenue::class, 'revenue_creator', 'creator_id', 'revenue_id');
    }
}
