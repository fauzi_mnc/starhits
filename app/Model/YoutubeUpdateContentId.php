<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class YoutubeUpdateContentId extends Model
{
    protected $table    = 'youtube_video_content_id';
	public $timestamps	= true;
}
