<?php

namespace App\Console;

use App\Console\Commands\UpdateCampaign;
use App\Console\Commands\YoutubeGetAPI;
use App\Console\Commands\SitemapGenerate;
use App\Console\Commands\SocialGetFollowers;
use App\Console\Commands\InstagramGetAPI;
use App\Console\Commands\YoutubeGetAPIWeek;
use App\Console\Commands\YoutubeAnaliticAPI;
use App\Console\Commands\YoutubeAnalyticDay;
use App\Console\Commands\YoutubeGetAPIMonthly;
use App\Console\Commands\YoutubeGetVideosAPI;
use App\Console\Commands\RealtimeHomeAnalitics;
use App\Console\Commands\VideoAPISaveConsole;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        YoutubeGetAPI::class,
        UpdateCampaign::class,
        SitemapGenerate::class,
        SocialGetFollowers::class,
        InstagramGetAPI::class,
        YoutubeGetAPIWeek::class,
        YoutubeGetAPIMonthly::class,
        YoutubeAnaliticAPI::class,
        YoutubeAnalyticDay::class,
        YoutubeGetVideosAPI::class,
        RealtimeHomeAnalitics::class,
        VideoAPISaveConsole::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        
        $filePath_RealtimeHomeAnalitics = base_path()."/storage/logs/cron_RealtimeHomeAnalitics.log";
        //Fungsi command untuk data youtube get api
        $schedule->command('RealtimeHomeAnalitics')
            ->timezone('Asia/Jakarta')
            ->dailyAt('01:00')
            ->emailOutputTo('fauzi.mncgroup@gmail.com');
            //->sendOutputTo($filePath_YoutubeAnaliticAPI)
            //->appendOutputTo($filePath_YoutubeAnaliticAPI);
        
        $filePath_VideoAPISaveConsole = base_path()."/storage/logs/cron_VideoAPISaveConsole.log";
        //Fungsi command untuk data youtube get api
        $schedule->command('VideoAPISaveConsole')
            ->timezone('Asia/Jakarta')
            ->dailyAt('02:00')
            ->emailOutputTo('fauzi.mncgroup@gmail.com');
            //->sendOutputTo($filePath_YoutubeAnaliticAPI)
            //->appendOutputTo($filePath_YoutubeAnaliticAPI);

        $filePath_YoutubeGetVideosAPI = base_path()."/storage/logs/cron_YoutubeGetVideosAPI.log";
        //Fungsi command untuk data youtube get api
        $schedule->command('YoutubeGetVideosAPI')
            ->timezone('Asia/Jakarta')
            ->weekly()
            ->emailOutputTo('fauzi.mncgroup@gmail.com');
            //->sendOutputTo($filePath_YoutubeAnaliticAPI)
            //->appendOutputTo($filePath_YoutubeAnaliticAPI);

        $filePath_YoutubeAnalyticDay = base_path()."/storage/logs/cron_YoutubeAnalyticDay.log";
        //Fungsi command untuk data youtube get api
        $schedule->command('YoutubeAnalyticDay')
            ->timezone('Asia/Jakarta')
            ->dailyAt('03:00')
            ->emailOutputTo('fauzi.mncgroup@gmail.com');
            //->sendOutputTo($filePath_YoutubeAnaliticAPI)
            //->appendOutputTo($filePath_YoutubeAnaliticAPI);


        $filePath_YoutubeAnaliticAPI = base_path()."/storage/logs/cron_YoutubeAnaliticAPI.log";
        //Fungsi command untuk data youtube get api
        $schedule->command('YoutubeAnaliticAPI')
            ->timezone('Asia/Jakarta')
            ->dailyAt('23:00')
            ->emailOutputTo('fauzi.mncgroup@gmail.com');
            //->sendOutputTo($filePath_YoutubeAnaliticAPI)
            //->appendOutputTo($filePath_YoutubeAnaliticAPI);


        $filePath_YoutubeGetAPI = base_path()."/storage/logs/cron_YoutubeGetAPI.log";
        //Fungsi command untuk data youtube get api
        $schedule->command('YoutubeGetAPI')
            ->timezone('Asia/Jakarta')
            ->dailyAt('00:15')
            ->emailOutputTo('fauzi.mncgroup@gmail.com');
            //->sendOutputTo($filePath_YoutubeGetAPI)
            //->appendOutputTo($filePath_YoutubeGetAPI);

        $filePath_YoutubeGetAPIWeek = base_path()."/storage/logs/cron_YoutubeGetAPIWeek.log";
        //Fungsi command untuk data youtube get api
        $schedule->command('YoutubeGetAPIWeek')
            ->timezone('Asia/Jakarta')
            ->weekly()
            ->emailOutputTo('fauzi.mncgroup@gmail.com');
            //->sendOutputTo($filePath_YoutubeGetAPI)
            //->appendOutputTo($filePath_YoutubeGetAPI);

        $filePath_YoutubeGetAPIMonthly = base_path()."/storage/logs/cron_YoutubeGetAPIMonthly.log";
        //Fungsi command untuk data youtube get api
        $schedule->command('YoutubeGetAPIMonthly')
            ->timezone('Asia/Jakarta')
            ->monthly()
            ->emailOutputTo('fauzi.mncgroup@gmail.com');
            //->sendOutputTo($filePath_YoutubeGetAPI)
            //->appendOutputTo($filePath_YoutubeGetAPI);

        $filePath_UpdateCampaign = base_path()."/storage/logs/cron_UpdateCampaign.log";
       /* $schedule->call(function () {
            DB::table('campaign')->whereIn('status', [1, 2, 3])->whereDate('end_date', '<', Carbon::today())->update(['status' => 4]);
        })->hourlyAt(5);*/
        $schedule->command('UpdateCampaign')
        ->timezone('Asia/Jakarta')
        ->twiceDaily(1, 13)
        ->emailOutputTo('fauzi.mncgroup@gmail.com');
        //->sendOutputTo($filePath_UpdateCampaign)
        //->appendOutputTo($filePath_UpdateCampaign);

        $filePath_SitemapGenerate = base_path()."/storage/logs/cron_SitemapGenerate.log";
        $schedule->command('SitemapGenerate')
        ->timezone('Asia/Jakarta')
        ->twiceDaily(9, 21)
        ->emailOutputTo('fauzi.mncgroup@gmail.com');
        //->sendOutputTo($filePath_SitemapGenerate)
        //->appendOutputTo($filePath_SitemapGenerate);

        $filePath_SocialGetFollowers = base_path()."/storage/logs/cron_SocialGetFollowers.log";
        $schedule->command('SocialGetFollowers')
        ->timezone('Asia/Jakarta')
        ->dailyAt('01:00')
        ->emailOutputTo('fauzi.mncgroup@gmail.com');
        //->sendOutputTo($filePath_SitemapGenerate)
        //->appendOutputTo($filePath_SitemapGenerate);

        $filePath_InstagramGetAPI = base_path()."/storage/logs/cron_InstagramGetAPI.log";
        $schedule->command('InstagramGetAPI')
        ->timezone('Asia/Jakarta')
        ->hourlyAt(45)
        ->emailOutputTo('fauzi.mncgroup@gmail.com');
        //->sendOutputTo($filePath_SitemapGenerate)
        //->appendOutputTo($filePath_SitemapGenerate);

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
