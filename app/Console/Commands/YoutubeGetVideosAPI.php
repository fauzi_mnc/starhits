<?php

namespace App\Console\Commands;

use App\Model\Content;
use App\Model\User;
use App\Model\Video;
use App\Model\YoutubeChannel;
use App\Model\YoutubeVideo;
use App\Model\YoutubeAnalitic;
use App\Model\YoutubeAnaliticVideo;
use App\Model\YoutubeAnaliticLink;
use Illuminate\Console\Command;
use Youtube;
/*use App\Http\Controllers\Backend\YoutubeController;
use App\Http\Controllers\Backend\GoogleSheetController;*/


class YoutubeGetVideosAPI extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'YoutubeGetVideosAPI';
    protected $description = 'Youtube get Videos';
    protected $youtubeAnalitic;
    protected $googleSheet;

    public function __construct()
    {
        parent::__construct();
       /* $this->youtubeAnalitic = $youtubeAnalitic;
        $this->googleSheet = $googleSheet;*/
    }    



    public function handle()
    {
        //$singleCreators = User::get();
        $date = date("Y-m");
        $dateNow = date("Y-m-d");
        $Videos = YoutubeVideo::all();
        $myApiKey = env('YOUTUBE_API_KEY'); // Provide your API Key
        //$channels =YoutubeChannel::all();
        $channels = "UCU_fuBEbZ0Wcf3ZNWEtxwzg"; //UCE9-bV_MCGgLnH7v4HSApDg

        //foreach ($channels as $channels) {
        $myQuery = "https://www.googleapis.com/youtube/v3/search?key=".$myApiKey."&channelId=".$channels."&part=snippet,id&maxResults=50";

        //}
            $videoList = file_get_contents($myQuery);
            $decoded = json_decode($videoList);
            $nextToken="";
            //var_dump($decoded);
            if(!empty($decoded->nextPageToken)){
                $nextPageToken = $decoded->nextPageToken;
                $totalVid = $decoded->pageInfo->totalResults;
                $vidPages = $decoded->pageInfo->resultsPerPage;
                $totalPages = (int)($totalVid/$vidPages);
                
                if($nextToken==""){
                     $nextToken = $nextPageToken;
                }else{
                    $nextToken="";
                }

                var_dump("total pages : ".$totalPages);
                for ($i=0; $i < $totalPages; $i++) { 
                    var_dump("page : ".$i.", Next :".$nextToken);
                    $nextToken = $this->getApi($nextToken,$channels,$myApiKey);
                }
            }else{
                $this->saveVideos1($decoded,$channels);
            }
       // }
        /*if(!empty($decoded->nextPageToken)){
            $myQuery = "https://www.googleapis.com/youtube/v3/search?key=".$myApiKey."&channelId=".$channelID."&part=snippet,id&maxResults=50&pageToken=".$decoded->nextPageToken;

            $videoList = file_get_contents($myQuery);
            $decoded = json_decode($videoList);    
        }*/
    }

    public function saveVideos1($decoded,$channelId){
        foreach ($decoded->items as $value) {
            if(!empty($value->id->videoId)){
                $videoId=$value->id->videoId;
            }else{
                $videoId="";
            }

            $findVideo = YoutubeVideo::where(['video_id'=>$videoId ])->first();
            if(empty($findVideo)){
                $insert = YoutubeVideo::insert([
                    'channel_id'    => $channelId,
                    'video_id'      => $videoId,
                ]);  
                var_dump("insert :".$insert."".$videoId);
            }
        }
    }

    public function getApi($nextToken,$channelID,$myApiKey){
        $myQuery = "https://www.googleapis.com/youtube/v3/search?key=".$myApiKey."&channelId=".$channelID."&part=snippet,id&maxResults=50&pageToken=".$nextToken;

        $videoList = file_get_contents($myQuery);
        $decoded = json_decode($videoList);    
        foreach ($decoded->items as $value) {

            if(!empty($value->id->videoId)){
                $videoId=$value->id->videoId;
                $logs = $this->saveVideos($value,$videoId,$channelID);
                var_dump($logs);
            }
            if(!empty($value->id->playlistId)){

                $playListToken ="";
                $totalItems ="";
                $temsPages ="";
                $totalPages2 ="";
                $getPlaylist = Youtube::getPlaylistItemsByPlaylistId($value->id->playlistId);
                
                if($getPlaylist['info']['nextPageToken'] == false){
                    //First save From Playlist
                    //Just Insert
                    foreach ($getPlaylist['results'] as $key => $resVid) {
                        $videoId = $resVid->contentDetails->videoId;
                        $logs = $this->saveVideos($resVid,$videoId,$channelID);
                        var_dump("Playlist 1: ".$value->id->playlistId.", ".$logs);
                    }

                }else{
                    
                    $playListToken = $getPlaylist['info']['nextPageToken'];
                    $totalItems = $getPlaylist['info']['totalResults'];
                    //$temsPages = $getPlaylist['info']['resultsPerPage'];
                    $totalPages2 = (int)($totalItems/50);
                    
                    /*foreach ($getPlaylist['results'] as $key => $resVid) {
                        $videoId = $resVid->contentDetails->videoId;
                        $logs = $this->saveVideos($resVid,$videoId,$channelID);
                        var_dump("Playlist arr 1: ".$value->id->playlistId.", ".$logs);
                    }*/

                    for($a=0; $a < $totalPages2+1; $a++) { //Looping 50 data
                        var_dump("Total pages: ".$totalPages2.",  nextPage: ". $playListToken);
                        $playListToken = $this->getPlaylistItems($value,$channelID,$playListToken); 
                    }
                }
            }            
        }
        
        if(!empty($decoded->nextPageToken)){
           return $decoded->nextPageToken; 
        }else{
            return false;
        }
    }

    public function getPlaylistItems($value,$channelID,$nextPage=''){

        if($nextPage==""){
            $varReturn = Youtube::getPlaylistItemsByPlaylistId($value->id->playlistId); 
        }else{
            $varReturn = Youtube::getPlaylistItemsByPlaylistId($value->id->playlistId,$nextPage);
        }
        foreach ($varReturn['results'] as $key => $resVid) {
            $videoId = $resVid->contentDetails->videoId;
            $logs = $this->saveVideos($resVid,$videoId,$channelID);
            var_dump("Playlist arr 2: ".$value->id->playlistId.", ".$logs);
        }

        if(!empty($varReturn['info']['nextPageToken'])){
            return $varReturn['info']['nextPageToken'];
        } else{
            return false;
        }
    }

    public function saveVideos($value,$videoId,$channel_id){
        $return = "";
        $findVideo = YoutubeVideo::where(['video_id'=>$videoId ])->first();
        if(empty($findVideo)){
            $insert = YoutubeVideo::insert([
                'channel_id'    => $channel_id,
                'video_id'      => $videoId,
            ]);  

            $return="insert :".$insert.", ".$videoId."";
        }
        return $return;
    }





}
