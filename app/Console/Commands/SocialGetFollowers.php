<?php

namespace App\Console\Commands;

use App\Model\User;
use App\Model\Socials;
use App\Model\SocialFollowers;
use Illuminate\Console\Command;
use Spatie\Sitemap\SitemapGenerator;
use App\Http\Controllers\Backend\TwitterController;
use Youtube;


class SocialGetFollowers extends Command
{

    protected $signature = 'SocialGetFollowers';
    protected $description = 'Social Get Followers';
    protected $TwitterCtrl;

    public function __construct(TwitterController $TwitterCtrl)
    {
        parent::__construct();
        $this->TwitterCtrl = $TwitterCtrl;
    }

    
    public function handle()
    {
        $date = date("Y-m");

        $getIDTwitter = Socials::where(['name'=>'twitter'])
                ->where('url','!=','')
                ->get();
        //var_dump($getIDTwitter);
        // GET FOLLOWERS TWITTER
        //$CheckNow = SocialFollowers::where('created_at', 'like', '%'.$date.'%')->first();
        //var_dump($CheckNow);
        //if(empty($CheckNow)) {
            foreach ($getIDTwitter as $key => $value) {
                $data = $this->TwitterCtrl->getFollowers($value->url);
                $insert = SocialFollowers::insert([
                    'user_id'       => $value->user_id,
                    'social_id'     => $data['id'],
                    'screen_name'   => $data['name'],
                    'followers'     => $data['followers_count'],
                    'type'          => 'twitter',
                    'created_at'    => date("Y-m-d H:i:s"),
                    'updated_at'    => date("Y-m-d H:i:s")
                ]);
                var_dump("twitter : ".$data['name']);

                Socials::where('id', $value->id)
                    ->update(['followers' => $data['followers_count'] ]);

                User::where('id', $value->user_id)
                ->update(['followers_twitter' => bd_nice_numbers($data['followers_count']) ]);
            }
        //}


            // GET FOLLOWERS INSTAGRAM
        $detailInfluencers = Socials::where(['name'=>'ig'])
                ->where('url','!=','')
                ->get();
        foreach ($detailInfluencers as $value) {

            $account = explode("/",$value->url);
            $name = $account[3];
            $urlToGet ="https://www.instagram.com/$name/?__a=1";

            $headers = get_headers($urlToGet);
            $responseCode = substr($headers[0], 9, 3);

            if($responseCode ==200){
                $response = file_get_contents($urlToGet);
                if ($response !== false) {
                    $data = json_decode($response, true);
                    if ($data !== null) {
                        //ProfilePage
                        $user_id = $data["graphql"]["user"]["id"];
                        $username = $data["graphql"]["user"]["username"];
                        $user_followers = $data["graphql"]["user"]["edge_followed_by"]["count"];
                        $user_pic = $data["graphql"]["user"]["profile_pic_url"];
                        $user_pic_hd = $data["graphql"]["user"]["profile_pic_url_hd"];
                        $biography =$data["graphql"]["user"]["biography"];
                        $external_url = $data["graphql"]["user"]["external_url"];
                        $business_category =$data["graphql"]["user"]["business_category_name"];

                        $CheckNowIG = SocialFollowers::where('created_at', 'like', '%'.$date.'%')->get();

                        if(empty($CheckNowIG)){
                            $insert = SocialFollowers::insert([
                                'user_id'       => $value->user_id,
                                'social_id'     => $user_id,
                                'screen_name'   => $username,
                                'followers'     => $user_followers,
                                'type'          => 'ig',
                                'created_at'    => date("Y-m-d H:i:s"),
                                'updated_at'    => date("Y-m-d H:i:s")
                            ]);
                        }
                        var_dump("ig : ".$user_followers);

                        Socials::where('id', $value->id)
                        ->update(['followers' => $user_followers]);

                        User::where('id', $value->user_id)
                        ->update(['followers_ig' => bd_nice_numbers($user_followers) ]);
                    }
                }
            }
            
        }


        $singleCreators = User::get();
        // GET FOLLOWERS SUBSCRIBER YOUTUBE
        foreach ($singleCreators as $value) {
            if (!is_null($value->provider_id)) {
                if(empty(Youtube::getChannelById($value->provider_id))){
                    $channel = Youtube::getChannelByName($value->provider_id);
                }else{
                    $channel = Youtube::getChannelById($value->provider_id);
                }
                var_dump("subscriber :".$value->provider_id);
                
                if (!empty($channel)) {
                    $value->update(
                        [   
                            'name'          => $channel->snippet->title,
                            'name_master'   => $channel->snippet->title,
                            'name_channel'  => $channel->snippet->title,
                            'subscribers'   => bd_nice_numbers($channel->statistics->subscriberCount),
                            'provider_id'   => $channel->id,
                            'yt_subs'       => bd_nice_numbers($channel->statistics->subscriberCount)
                        ]
                    );
                }
            }
        }



    }
}
