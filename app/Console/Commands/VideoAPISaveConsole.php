<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\DetailInfluencer;
use App\Model\InfluencerDiagram;
use App\Model\User;
use App\Model\Video;
use App\Model\YoutubeChannel;
use App\Model\YoutubeVideo;
use App\Model\VideoUpload;
use App\Model\YoutubeAnalitic;
use App\Model\YoutubeAnaliticVideo;
use App\Model\YoutubeAnaliticLink;


use App\Http\Controllers\Backend\VideoYTController;


class VideoAPISaveConsole extends Command
{

    protected $signature = 'VideoAPISaveConsole';
    protected $description = 'Video API Save Console';
    protected $VideoYTController;

    public function __construct(VideoYTController $VideoYTController)
    {
        parent::__construct();
        $this->VideoYTController = $VideoYTController;
    }

    public function handle()
    {
    	$readCsv = $this->VideoYTController->consoleReadCsv();
    	var_dump("output readCsv: ".$readCsv."");

        $BeReadChangers = $this->VideoYTController->Bcxz();
        var_dump("output Bcxz: ".$BeReadChangers."");

    }

}
