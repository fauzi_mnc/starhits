<?php

namespace App\Console\Commands;

use App\Model\Content;
use App\Model\User;
use App\Model\Video;
use App\Model\YoutubeChannel;
use App\Model\YoutubeVideo;
use App\Model\YoutubeAnalitic;
use App\Model\YoutubeAnaliticVideo;
use App\Model\YoutubeAnaliticLink;
use Illuminate\Console\Command;
use Youtube;
/*use App\Http\Controllers\Backend\YoutubeController;
use App\Http\Controllers\Backend\GoogleSheetController;*/
use Google\Auth\HttpHandler\HttpClientCache;


class YoutubeAnaliticAPI extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'YoutubeAnaliticAPI';
    protected $description = 'Youtube API Analitic';
    protected $youtubeAnalitic;
    protected $googleSheet;

    public function __construct()
    {
        parent::__construct();
       /* $this->youtubeAnalitic = $youtubeAnalitic;
        $this->googleSheet = $googleSheet;*/
    }    



    public function handle()
    {
        //$singleCreators = User::get();
        $date = date("Y-m");
        $dateNow = date("Y-m-d");
        //$getFileID = YoutubeAnaliticLink::where('created_at', 'like', '%'.$date.'%')->first();
        /*if(empty($getFileID)){
            $values = $this->googleSheet->create();
            $insertt = YoutubeAnaliticLink::insert([   
                    'link'       => "https://docs.google.com/spreadsheets/d/".$values."/edit", 
                    'fileid'       => $values,
                    'created_at'    => date("Y-m-d H:i:s"),
                    'updated_at'    => date("Y-m-d H:i:s")
                ]);
        }*/

        $Videos = YoutubeVideo::all();

        if(!empty($Videos)){
            
            foreach ($Videos as $valVideo) {

                $cekVideos = YoutubeAnaliticVideo::where(['video_id'=>$valVideo->video_id])
                            ->where('created_at', 'like', '%'.$dateNow.'%')->first();
                if(empty($cekVideos)){

                    if(!empty($valVideo->video_id)){
                        $videos  = Youtube::getVideoInfo($valVideo->video_id);  //get data api from youtube

                        $var = YoutubeAnaliticVideo::insert([
                            'content_id'    => $valVideo->content_id,
                            'channel_id'    => $valVideo->channel_id,
                            'video_id'      => $valVideo->video_id,
                            'view_count' => isset($videos->statistics->viewCount) ? $videos->statistics->viewCount:0,
                            'like_count' => isset($videos->statistics->likeCount) ? $videos->statistics->likeCount:0,
                            'dislike_count' => isset($videos->statistics->dislikeCount) ? $videos->statistics->dislikeCount:0,
                            'favorite_count' => isset($videos->statistics->favoriteCount) ? $videos->statistics->favoriteCount:0,
                            'comment_count' => isset($videos->statistics->commentCount) ? $videos->statistics->commentCount:0,
                            'created_at'    => date("Y-m-d"),
                            'updated_at'    => date("Y-m-d")
                        ]); 
                        var_dump($valVideo->video_id."-".$var);
                    }

                }
            }
        }


            $Channel = YoutubeChannel::all();
            foreach ($Channel as $value) {
                if (!is_null($value->channel_id)) {
                    $channel = Youtube::getChannelById($value->channel_id);
                    if (!empty($channel)) {
                         $viewer = YoutubeAnaliticVideo::where('channel_id', $value->channel_id)->count('view_count');
                         $like_count = YoutubeAnaliticVideo::where('channel_id', $value->channel_id)->count('like_count');
                         $dislike_count = YoutubeAnaliticVideo::where('channel_id', $value->channel_id)->count('dislike_count');
                         $favorite_count = YoutubeAnaliticVideo::where('channel_id', $value->channel_id)->count('favorite_count');
                         $comment_count = YoutubeAnaliticVideo::where('channel_id', $value->channel_id)->count('comment_count');

                        $insertt = YoutubeAnalitic::insert([   
                            'user_id'       => $value->id,
                            'content_id'    => $value->content_id,
                            'channel_id'    => $value->channel_id,
                            'name'          => $channel->snippet->title,
                            'name_channel'  => $channel->snippet->title,
                            'subscribers'   => $channel->statistics->subscriberCount,
                            'viewer'        => $viewer,
                            'like_count'    => $like_count,
                            'dislike_count' => $dislike_count,
                            'comment_count' => $comment_count,
                            'created_at'    => date("Y-m-d H:i:s"),
                            'updated_at'    => date("Y-m-d H:i:s")
                        ]);
                       var_dump("Analitic :".$value->content_id);
                    }
                }
            
            //$getFileID = YoutubeAnaliticLink::where('created_at', 'like', '%'.$date.'%')->first();

            //$values = $this->googleSheet->updateEntry($getFileID->fileid);
        }
        
        //var_dump($values);

        
    }
}
