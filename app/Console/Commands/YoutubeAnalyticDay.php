<?php

namespace App\Console\Commands;

use App\Model\Content;
use App\Model\User;
use App\Model\Video;
use App\Model\YoutubeAnalitic;
use App\Model\YoutubeAnaliticLink;
use Illuminate\Console\Command;
use App\Model\YoutubeChannel;
use Youtube;
use DB;
use App\Http\Controllers\Backend\YoutubeController;
use App\Http\Controllers\Backend\GoogleSheetController;
use App\Http\Controllers\Backend\YoutubeReportingController;


class YoutubeAnalyticDay extends Command
{

    protected $signature = 'YoutubeAnalyticDay';
    protected $description = 'Youtube Analytic Dayly';
    protected $youtubeReporting;

    public function __construct(YoutubeReportingController $youtubeReporting)
    {
        parent::__construct();
        $this->youtubeReporting = $youtubeReporting;
    }    

    

    public function GoogleClient($Secret){
        $client = new \Google_Client();
        $client->setClientId($Secret->client_id);
        $client->setClientSecret($Secret->client_secret);      
        $client->setScopes([
            'https://www.googleapis.com/auth/youtube',
            'https://www.googleapis.com/auth/yt-analytics-monetary.readonly',
            'https://www.googleapis.com/auth/youtubepartner',
            'https://www.googleapis.com/auth/yt-analytics.readonly'
        ]);
        $client->addScope(\Google_Service_YouTubeAnalytics::YT_ANALYTICS_READONLY);
        $client->setRedirectUri($Secret->client_redirect);
        $client->setAccessType('offline');
        $client->setPrompt('consent select_account');
        $client->setApprovalPrompt('force');
        return $client;
        
    }

    public function handle()
    {
        $dateNow = date("Y-m-d");
        $Secret = DB::table('yt_content_secret')
                   ->where('type', 'youtube')
                   ->get();

        foreach ($Secret as $key => $Secret) {
            $client = $this->GoogleClient($Secret);
            $token = DB::table('user_token')
                    ->where(['user_id'=> $Secret->user_id, 'type'=> 'youtube','action_dream'=>'yt-analytics'])
                    ->first();
            $token = collect($token)->except(['id', 'user_id', 'type'])->toArray();
            $apikey = $Secret->key;
           // $cmslist = YoutubePartner::where(['selected'=>'Y','user_id'=>$Secret->user_id])->get();

            $youtube = new \Google_Service_YouTubeAnalytics($client);
            $Channels = YoutubeChannel::where(['user_id'=>$Secret->user_id])->get();
            foreach ($Channels as $numb => $Channels) {

                $queryParams = [
                    'endDate' => $dateNow, //'2019-07-01',
                    'filters' => 'channel=='.$Channels->channel_id,
                    'ids' => 'contentOwner=='.$Channels->content_id,
                    'metrics' =>'views,likes,dislikes,comments,shares,estimatedRevenue,cpm,cardClickRate,cardClicks,cardImpressions,grossRevenue,subscribersLost,subscribersGained,estimatedMinutesWatched,monetizedPlaybacks',
                    'startDate' => $Channels->publishat
                ];

                $response = $youtube->reports->query($queryParams);

                //<---- GET SUM SUBSCRIBER
                $getSubs = $this->youtubeReporting->getStaticSubcriber($Channels->channel_id, $token['access_token'],$apikey);
                if(!empty(json_decode($getSubs)->items)){ 
                    $statics_channel = json_decode($getSubs)->items;
                    $count_subscriber = isset($statics_channel[0]->statistics->subscriberCount) ? $statics_channel[0]->statistics->subscriberCount:0;
                    $count_video = isset($statics_channel[0]->statistics->videoCount) ? $statics_channel[0]->statistics->videoCount:0;
                }else{
                    $count_subscriber =0;
                    $count_video =0;
                }
                //<---- END GET SUM SUBSCRIBER

                $updateAnalytic = DB::table('youtube_channel_analitic')
                    ->where(['channel_id'=>$Channels->channel_id])
                    ->update([
                        'date'=>$dateNow,
                        'views'=>isset($response->rows[0][0]) ? $response->rows[0][0]:0,
                        'likes'=>isset($response->rows[0][1]) ? $response->rows[0][1]:0,
                        'dislikes'=>isset($response->rows[0][2]) ? $response->rows[0][2]:0,
                        'comments'=>isset($response->rows[0][3]) ? $response->rows[0][3]:0,
                        'shares'=>isset($response->rows[0][4]) ? $response->rows[0][4]:0,
                        'estimatedRevenue'=>isset($response->rows[0][5]) ? $response->rows[0][5]:0,
                        'cpm'=>isset($response->rows[0][6]) ? $response->rows[0][6]:0,
                        'cardClickRate'=>isset($response->rows[0][7]) ? $response->rows[0][7]:0,
                        'cardClicks'=>isset($response->rows[0][8]) ? $response->rows[0][8]:0,
                        'cardImpressions'=>isset($response->rows[0][9]) ? $response->rows[0][9]:0,
                        'grossRevenue'=>isset($response->rows[0][10]) ? $response->rows[0][10]:0,
                        'subscribersLost'=>isset($response->rows[0][11]) ? $response->rows[0][11]:0,
                        'subscribersGained'=>isset($response->rows[0][12]) ? $response->rows[0][12]:0,
                        'estimatedMinutesWatched'=>isset($response->rows[0][13]) ? $response->rows[0][13]:0,
                        'monetizedPlaybacks'=>isset($response->rows[0][14]) ? $response->rows[0][14]:0,
                        'subscribers'=> $count_subscriber,
                        'video_count'=> $count_video,
                    ]);
                var_dump($updateAnalytic.", ".$Channels->channel_id);
            }
        }
    }
}
