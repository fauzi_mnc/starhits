<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\DetailInfluencer;
use App\Model\InfluencerDiagram;
use App\Model\User;
use App\Model\Video;
use App\Model\YoutubeChannel;
use App\Model\YoutubeVideo;
use App\Model\YoutubeAnalitic;
use App\Model\YoutubeAnaliticVideo;
use App\Model\YoutubeAnaliticLink;

use App\Http\Controllers\Backend\YoutubeReportingController;
use App\Http\Controllers\Backend\YoutubeController;



class RealtimeHomeAnalitics extends Command
{

    protected $signature = 'RealtimeHomeAnalitics';
    protected $description = 'Realtime Home Analitics';
    protected $youtubeAnalitic;
    protected $YoutubeReporting;

    public function __construct(YoutubeController $youtubeAnalitic, YoutubeReportingController $YoutubeReporting)
    {
        parent::__construct();
        $this->youtubeAnalitic = $youtubeAnalitic;
        $this->YoutubeReporting = $YoutubeReporting;
    }

    public function handle()
    {
        // $getAPICHannels = $this->youtubeAnalitic->getAPICHannels();
        // var_dump("output APICHannels : ".$getAPICHannels."");
        
        $getContentOwner = $this->YoutubeReporting->getAPIAnaliticContentOwner();
    	var_dump("output ContentOwner : ".$getContentOwner."");

        $getChannelAnalytic = $this->YoutubeReporting->getChannelAnalytic();
        var_dump("output ChannelAnalytic : ".$getChannelAnalytic);

        $getgetStaticChannel = $this->YoutubeReporting->getStaticChannel();
        var_dump("output StaticAnalytic : ".$getgetStaticChannel);
    }

}
