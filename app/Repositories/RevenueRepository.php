<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Model\Revenue;
use App\Validators\UserValidator;

/**
 * Class UserRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RevenueRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Revenue::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    

    // public function create(array $input){

    //     $input['slug'] = rtrim(str_replace(' ', '-', strtolower($input['name'])),'-');
    //     $input['password'] = bcrypt($input['password']);
        
    //     return $this->model->create($input);
    // }

    // public function update(array $input, $id){
    //     $input['slug'] = rtrim(str_replace(' ', '-', strtolower($input['name'])),'-');

    //     if(isset($input['password'])){
    //         $input['password'] = bcrypt($input['password']);
    //     }
        
    //     return parent::update($input, $id);
    // }

    public function getCountries(){
        return User::select('country')->whereNotNull('country')->pluck('country', 'country')->toArray();
    }

    public function getCities(){
        return User::select('city')->whereNotNull('city')->pluck('city', 'city')->toArray();
    }
}
