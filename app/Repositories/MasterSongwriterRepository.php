<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Model\MasterLagu;
use App\Validators\UserValidator;

class MasterSongwriterRepository extends BaseRepository
{
    public function model()
    {
        return MasterLagu::class;
    }
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
