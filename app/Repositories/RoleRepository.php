<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Model\Role;

/**
 * Class RoleRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RoleRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Role::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    

    public function create(array $input){

        $input['name'] = strtolower($input['display_name']);
        
        return $this->model->create($input);
    }

    public function update(array $input, $id){
        $input['name'] = strtolower($input['display_name']);
        
        return parent::update($input, $id);
    }

    public function getList(){
        return Role::where('id', '!=', '2')->pluck('display_name', 'id')->toArray();
    }
}
