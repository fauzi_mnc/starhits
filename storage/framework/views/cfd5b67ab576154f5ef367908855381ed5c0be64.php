<!-- Stored in resources/views/child.blade.php -->


<?php $__env->startSection('title', 'Creators'); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid creators">
    <div class="col-md-12">
        <div class="col-sm-12">
            <div class="title">
                <div class="col-sm-12" style="padding: 0;">
                    <div class="row">
                        <div class="col-sm-8" style="padding: 0;">
                            <h7>
                                CREATORS
                            </h7>
                        </div>
                        <div class="col-sm-4" style="padding: 0;">
                            <a class="btn btn-filter filter" data-target=".bs-example-modal-lg-filter" data-toggle="modal" href="#">
                                <i class="fa fa-filter">
                                </i>
                                Filter
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12" style="padding: 0 10px 0px 10px">
            <div class="<?php echo e(count($creators)>=1? 'row text-center text-lg-left item-creators':''); ?>">
                <?php $__empty_1 = true; $__currentLoopData = $creators; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <div class="col-lg-3 col-md-4 col-xs-6 col-creators">
                    <a href="<?php echo e(url('/creators/'.$value->slug)); ?>">
                        <img class="img-fluid" src="<?php echo e(url('/'). Image::url($value->cover,400,250,array('crop'))); ?>"/>
                    </a>
                    <div class="title-creators">
                        <h7>
                            <a href="<?php echo e(url('/creators/'.$value->slug)); ?>">
                                <?php echo e($value->name); ?>

                            </a>
                        </h7>
                    </div>
                    <div class="videos">
                        <p>
                            <a href="#">
                                <?php echo e($value->totalVideos); ?> Videos
                            </a>
                        </p>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                            There is no data to display!
                        </div>
                    </br>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-12 page">
            <div class="row navigation">
                <?php echo e($creators->links('vendor.pagination.custom')); ?>

            </div>
        </div>
    </div>
</div>

<?php echo $__env->make('frontend.creators.modals-filter', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>