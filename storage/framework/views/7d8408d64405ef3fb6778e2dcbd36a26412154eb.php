<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Starhits</title>

    <link href="<?php echo e(asset('css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/font-awesome/css/font-awesome.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/inspinia.css')); ?>" rel="stylesheet">
    <style id="antiClickjack">body{display:none !important;}</style>
    <style>
        .forgot-password {
            color: #e6e6e6;
            font-size: 16px;
            font-weight: bold;
        }

        .forgot-password:hover {
            color:  #fff;
        }
        .field-icon {
            float: right;
            margin-left: -25px;
            margin-top: -25px;
            position: relative;
            z-index: 2;
        }
    </style>
</head>

<body>
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h2 style="font-size: 50px;" class="logo-name">Starhits</h2>

            </div>
            <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <form id="form-login" class="form-horizontal" method="POST" action="<?php echo e(route('login')); ?>">
                <?php echo e(csrf_field()); ?>

                <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                    <input id="email" type="text" class="form-control required" name="email" value="<?php echo e(old('email')); ?>" placeholder="Email" autofocus>

                    <?php if($errors->has('email')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('email')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
                <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                    <input id="password" type="password" class="form-control required" name="password" placeholder="Password">
                    <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>

                    <?php if($errors->has('password')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('password')); ?></strong>
                        </span>
                    <?php endif; ?>
                </div>
                <?php if(env('APP_ENV') != 'local'): ?>
                <div class="form-group<?php echo e($errors->has('g-recaptcha-response') ? ' has-error' : ''); ?>" style="margin: 10px auto;">
                <?php echo NoCaptcha::display(); ?>

                <?php if($errors->has('g-recaptcha-response')): ?>
                    <span class="help-block">
                        <strong><?php echo e($errors->first('g-recaptcha-response')); ?></strong>
                    </span>
                <?php endif; ?>
                </div>
                <?php endif; ?>

                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                <a class="forgot-password" href="<?php echo e(route('password.request')); ?>">
                    <small>Forgot password?</small>
                </a>

                <!-- <a href="#"><small>Forgot password?</small></a> -->
                <!-- <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a> -->
            </form>
            <!-- <p class="m-t"> <small>Caraka Teknologi Indonesia © 2017</small> </p> -->
        </div>
    </div>

    <!-- Mainly scripts -->
    <script async="" src="//www.google-analytics.com/analytics.js"></script>
    <script src="<?php echo e(asset('js/jquery/jquery-3.1.1.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script>
    <?php echo NoCaptcha::renderJs(); ?>

    <script>
    $(document).ready(function() {
      $("#form-login").on("submit",function(e) {
        //e.preventDefault();
        var form = $(this);
        var field = form.find("input[name=password]");
        var hash = btoa(field.val());
        var hash2 = btoa(hash);
        var slice = hash2.substr(hash2.length - 2);
        hash2 = hash2.slice(0, -2);
        var random_char = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        var slice_random = random_char.substring(0, 14); //only get 14 characters
        var new_str = hash2+slice_random+slice;
        field.val(new_str);
      });

      if (self === top) {
            var antiClickjack = document.getElementById("antiClickjack");
            antiClickjack.parentNode.removeChild(antiClickjack);
        } else {
            top.location = self.location;
        }
    });

    $(".toggle-password").click(function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
    </script>
</body>
</html>
