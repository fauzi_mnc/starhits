<?php $__env->startSection('breadcrumb'); ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Series Table</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Series</a>
            </li>
            <li class="active">
                <strong>Table</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentCrud'); ?>
<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Series</h5>
                    <?php if(auth()->user()->roles->first()->name === 'admin'): ?>
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="<?php echo e(route('admin.serie.create')); ?>">Add New</a>
                    <?php else: ?>
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="<?php echo e(route('creator.serie.create')); ?>">Add New</a>
                    <?php endif; ?>
                </div>

                <div class="ibox-content">
                    <?php echo $__env->make('series.table', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>