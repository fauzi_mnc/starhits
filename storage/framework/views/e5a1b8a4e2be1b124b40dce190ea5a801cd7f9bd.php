<?php $__env->startSection('css'); ?>
    <link href="<?php echo e(asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css')); ?>" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />

    <?php echo $__env->make('layouts.datatables_css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php echo $dataTable->table(['width' => '100%']); ?>


<?php $__env->startSection('scripts'); ?>
    <script src="<?php echo e(asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>"></script>
    <script src="https://unpkg.com/sweetalert2@7.21.1/dist/sweetalert2.all.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/piexif.min.js" type="text/javascript"></script>
    <!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview. 
        This must be loaded before fileinput.min.js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/sortable.min.js" type="text/javascript"></script>
    <!-- purify.min.js is only needed if you wish to purify HTML content in your preview for 
        HTML files. This must be loaded before fileinput.min.js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/purify.min.js" type="text/javascript"></script>
    <!-- popper.min.js below is needed if you use bootstrap 4.x. You can also use the bootstrap js 
    3.3.x versions without popper.min.js. -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
    <!-- optionally if you need a theme like font awesome theme you can include it as mentioned below -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/themes/fa/theme.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
    <?php echo $__env->make('layouts.datatables_js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $dataTable->scripts(); ?>

    <?php echo $__env->make('layouts.datatables_limit', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script>
    $('.form-horizontal').find('.btn-search').on('click', function(e) {
        LaravelDataTables["dataTableBuilder"].draw();
        e.preventDefault();
    });
    $('.form-horizontal').find('.clear-filtering').on('click', function(e) {
        $('#month').val('');
        $('#year').val('');
        LaravelDataTables["dataTableBuilder"].draw();
        e.preventDefault();
    });

    $(document).ready(function(){
        /*$('#month').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            viewMode: 'months',
            minViewMode: "months",
            format: 'MM'
        });*/

        $('#year').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            orientation: "bottom right",
            viewMode: 'years',
            minViewMode: "years",
            format: 'yyyy'
        });
    });

    LaravelDataTables["dataTableBuilder"].on('click', '.approval', function(e){
        var id = $(this).data('id');
        var url = $(this).data('url');

        $("#form-approval").attr('action', url);
        $("#form-revision").attr('action', '<?php echo e(url("admin/revenue")); ?>/'+id+'/edit');
        $('#modal-approval').modal('show');
    });

    $("#btn-approve").click(function(){
        $("#status").val(4);
        $("#status-on-upload").val(4);
        $('#modal-approval').modal('hide');
        setTimeout(() => {
            $('#modal-upload').modal('show');
        }, 1000);
        
        $("#form-upload").attr('action', $("#form-approval").attr('action'));
    });

    $("#btn-reject").click(function(){
        $("#status").val(6);
        $('#modal-approval').modal('hide');
        swal({
            title: "Are you sure want to reject this data?",
            showCancelButton: true,
            type: "warning",
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        })
        .then((result) => {
            if (result.value) {
                $('#modal-reason').find('.modal-title').empty();
                $('#modal-reason').find('.modal-title').html('Reason To Reject');
                $('#modal-reason').modal('show');
            }
            
        });
    });

    $("#btn-revision").click(function(){
        $('#modal-approval').modal('hide');
        $("#status").val(5);
        swal({
            title: "Are you sure want to revision this data?",
            showCancelButton: true,
            type: "warning",
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        })
        .then((result) => {
            if (result.value) {
                $('#modal-reason').find('.modal-title').empty();
                $('#modal-reason').find('.modal-title').html('Reason To Revision');
                $('#modal-reason').modal('show');
            }
            
        });
        
    });

    $('#btn-submit').click(function(){
        var valid = $('#form-reject').valid();
        if(valid == true){
            $('#modal-reason').modal('hide');
            $('#reason').val($('#reason_val').val());
            $("#form-approval").submit();
        }
        
    });

    $("#evidence").fileinput({
        uploadUrl: "#",
        showBrowse: false,
        browseOnZoneClick: true,
        showUpload: false,
        allowedFileExtensions: ["pdf"],
        maxFileSize: 512000,
        fileActionSettings: {
            showUpload: false,
        }
    });
    </script>
<?php $__env->stopSection(); ?>