<!-- Stored in resources/views/child.blade.php -->


<?php $__env->startSection('title', 'Search Series'); ?>

<?php $__env->startSection('content'); ?>

<div class="container-fluid filter">
    <div class="row">
        <div class="col-sm-12">
            <div class="top-filter-area padding-tb-10">
                <div class="container-fluid">
                    <form action="" method="post">
                        <?php echo e(csrf_field()); ?>

                    <div class="row">
                        <!--<div class="col"></div>-->
                        <div class="col-sm-4">
                            <input name="type" class="form-control" placeholder="Video by Series" type="text">
                            </input>
                        </div>
                        <div class="col-sm-4">
                            <select name="sortby" class="select-box">
                                <option selected="selected" value="0">
                                    Sort by
                                </option>
                                <option value="1">
                                    Most Viewed
                                </option>
                                <option value="2">
                                    Most Liked
                                </option>
                                <option value="3">
                                    Most Commented
                                </option>
                                <option value="4">
                                    Published Date
                                </option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-filter btn-lg btn-block site-btn" name="search">
                                <i class="fa fa-search">
                                </i>
                                Search
                            </button>
                        </div>
                        <!--<div class="col"></div>-->
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="search-result">
                <div class="container-fluid">
                    <div class="row">
                        <?php if(sizeof($models)>0): ?>
                        <div class="col-md-12">
                            <h5>
                                <?php echo e(sizeof($models)); ?> matches sort by: <?php echo e($type); ?> in most <?php echo e($sort); ?> - <?php echo e(sizeof($models)); ?>

                            </h5>
                            <hr>
                                <ul class="updates">
                                    <?php $__currentLoopData = $models; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li>
                                        <a href="<?php echo e(url('/videos/'.$value['slug'])); ?>">
                                            <?php echo e($value['title']); ?>

                                            <span>
                                                <?php echo e($value['created_at']); ?>

                                            </span>
                                        </a>
                                    </li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                                <div class="col-md-12 page">
                                    <div class="row navigation">
                                        
                                    </div>
                                </div>
                            </hr>
                        </div>
                        <?php else: ?>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>