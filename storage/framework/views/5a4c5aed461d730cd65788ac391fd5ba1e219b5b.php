<!-- Stored in resources/views/child.blade.php -->


<?php $__env->startSection('content'); ?>

<div class="container-fluid not-found">
    <div class="row">
        <div class="col-md-12 text-center">
            <label>
                This is somewhat embarrasing, isn't it?
                <br>
                    <small>
                        Sorry, we couldn't find what you were looking for. But, please enjoy these contents from our site.
                    </small>
                </br>
            </label>
            <h1>
                4
                <img src="<?php echo e(asset('frontend/assets/img/icon/face.png')); ?>">
                    4
                </img>
            </h1>
        </div>
    </div>
</div>
<div class="container-fluid not-found-recommended">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        RECOMMENDED VIDEOS
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row text-center text-lg-left item-others-videos">
                    <?php $__currentLoopData = $recomendedVideos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-sm-3 col-others-videos">
                        <div class="item">
                            <div class="imgTitle">
                                <div class="top-videos-overlay">
                                    <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                        <?php if(!empty($value->image)): ?>
                                            <img class="img-fluid top-videos-overlay" src="<?php echo e(url('/'). Image::url($value->image,400,225,array('crop'))); ?>" />
                                        <?php else: ?>
                                            <img class="img-fluid top-videos-overlay" src="https://img.youtube.com/vi/<?php echo e($value->attr_6); ?>/mqdefault.jpg" style="width: 400; height: 250;" />
                                        <?php endif; ?>
                                    </a>
                                    <?php if(!empty($value->attr_7)): ?>
                                        <div class="item-top-videos-overlay">
                                            <?php echo e($duration->formatted($value->attr_7)); ?>

                                        </div>
                                    <?php else: ?>

                                    <?php endif; ?>
                                </div>
                            </div>
                            <p>
                                <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                    <?php echo e($value->title); ?>

                                </a>
                            </p>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid not-found-series">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        RECOMMENDED SERIES
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class=" row item-not-found-series">
                    <?php $__currentLoopData = $recomendedSeries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-lg-3 col-sm-6 col-xs-6 item">
                        <article class="caption">
                            <img class="caption__media img-fluid" src="<?php echo e(url('/'). Image::url($value->image,400,550,array('crop'))); ?>" />
                            <a href="<?php echo e(url('/series/'.$value->slug)); ?>">
                                <div class="caption__overlay">
                                    <h1 class="caption__overlay__title">
                                        <?php echo e($value->title); ?>

                                    </h1>
                                    <h7 class="caption__overlay__content">
                                        <?php echo e($value->totalVideo); ?> Videos
                                    </h7>
                                    <p class="caption__overlay__content">
                                        <?php echo str_limit($value->content, 200); ?>

                                    </p>
                                </div>
                            </a>
                        </article>
                        <p>
                            <a href="<?php echo e(url('/series/'.$value->slug)); ?>">
                                <?php echo e($value->title); ?>

                            </a>
                        </p>
                        <p class="videos">
                            <a href="#">
                                <?php echo e($value->totalVideo); ?> Videos
                            </a>
                        </p>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid not-found-popular">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        POPULAR CREATOR BASED ON VIEWS
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row text-center text-lg-left item-not-found-popular">
                    <?php $__currentLoopData = $popularCreators; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-lg-2 col-sm-6 col-xs-6 col-not-found-popular">
                        <div class="item">
                            <div class="imgTitle">
                                <div class="top-videos-overlay">
                                    <a href="<?php echo e(url('/creators/'.$value->slug)); ?>">
                                        <img class="img-fluid top-videos-overlay" src="<?php echo e(url('/'). Image::url($value->image,150,150,array('crop'))); ?>" />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid recommended-videos">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        LATEST VIDEOS
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="slider-recommended-videos">
                    <?php $__currentLoopData = $latestVideoStarHits; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div>
                        <div class="item">
                            <div class="imgTitle">
                                <div class="top-videos-overlay">
                                    <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                        <?php if(!empty($value->image)): ?>
                                            <img class="img-fluid top-videos-overlay" src="<?php echo e(url('/'). Image::url($value->image,400,225,array('crop'))); ?>" />
                                        <?php else: ?>
                                            <img class="img-fluid top-videos-overlay" src="https://img.youtube.com/vi/<?php echo e($value->attr_6); ?>/mqdefault.jpg" style="width: 400; height: 250;" />
                                        <?php endif; ?>
                                    </a>
                                    <?php if(!empty($value->attr_7)): ?>
                                        <div class="item-top-videos-overlay">
                                            <?php echo e($duration->formatted($value->attr_7)); ?>

                                        </div>
                                    <?php else: ?>

                                    <?php endif; ?>
                                </div>
                            </div>
                            <p>
                                <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                    <?php echo e($value->title); ?>

                                </a>
                            </p>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>