
<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('css/fileinput.min.css')); ?>" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css">
<?php echo $__env->make('layouts.datatables_css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
<?php echo $__env->make('layouts.datatables_css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<!-- Name Field -->
<div class="form-group">
    <?php echo Form::label('name', 'Name:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => 30]); ?>

    </div>
</div>

<!-- Email Field -->
<div class="form-group">
    <?php echo Form::label('email', 'Email:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::text('email', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => 50]); ?>

    </div>
</div>

<!-- Password Field -->
<div class="form-group">
    <?php echo Form::label('password', 'Password:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-<?php echo e($formType == 'edit' ? '8' : '10'); ?>">
        <?php echo Form::text('password', null, ['class' => 'form-control', 'required' => 'required', 'minlength' => 12]); ?>

    </div>
    <?php if($formType == 'edit'): ?>
    <div class="col-sm-2">
        <button type="button" id="reset-pwd" class="btn btn-default">Reset</button>
    </div>
    <?php endif; ?>
</div>

<!-- Image Field -->
<div class="form-group">
    <?php echo Form::label('image', 'Image:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <div class="kv-photo center-block text-center">
            <input id="photo" name="image" type="file" class="file-loading">
        </div>

        <div id="kv-photo-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
    </div>
</div>

<!-- bio Field -->
<div class="form-group">
    <?php echo Form::label('is_active', 'Is Active:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
    <?php echo Form::select('is_active', ['1' => 'active', '0' => 'Inactive', '' => 'select'], null, ['class' => 'form-control']); ?>

    </div>
</div>

<!-- role Field -->
<div class="form-group">
    <?php echo Form::label('roles', 'Roles:', ['class' => 'col-sm-2 control-label']); ?>

    
    <div class="col-sm-6">
        <?php echo Form::select('roles[]', $roles, null, ['class' => 'selectpicker form-control', 'id' => 'roles', 'multiple' => 'multiple']); ?>

    </div>
    
    <div class="col-sm-4">
        <table class="table table-bordered" id="table-role">
            <thead>
                <tr>
                    <th>Selected Role</th>
                </tr>
            </thead>
            <tbody>
                <?php if($formType == 'edit'): ?>
                <?php $__currentLoopData = $user->roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($role->display_name); ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
    <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

    <a href="<?php echo route('user.index'); ?>" class="btn btn-default">Cancel</a>
    </div>
</div>

<?php $__env->startSection('scripts'); ?>
<script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/piexif.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/sortable.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/purify.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/fileinput.min.js')); ?>"></script>
<script>
    
    var formType = '<?php echo e($formType); ?>';
    $('#photo').val('');

    $("#photo").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="<?php echo e(($formType == 'edit' && $user->image) ? asset($user->image) : asset('img/default_avatar_male.jpg')); ?>" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "jpeg"]
    });

    $("#cover").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-cover-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="<?php echo e(($formType == 'edit' && $user->cover) ? asset($user->cover) : asset('img/default_avatar_male.jpg')); ?>" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "jpeg"]
    });
    
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
<?php echo $__env->make('layouts.datatables_js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script>
    var table = $("#table-role").DataTable({"dom": 'tp'});
    <?php if($formType == 'edit'): ?>
    $('#password').attr('disabled', true);
    $('#reset-pwd').click(function(){
        $('#password').val('');
        $('#password').attr('disabled', false);
    });
    <?php endif; ?>
    $('#roles').multiselect({
        onChange: function(element, checked) {
            if(checked){
                table.row.add( [ element.html() ] )
                .draw()
                .node();
            } else {
                var indexes = table.rows().eq( 0 ).filter( function (rowIdx) {
                    return table.cell( rowIdx, 0 ).data() === element.html() ? true : false;
                });
                
                table.rows( indexes ).remove().draw();
            }
        }
    }); 
</script>

<?php $__env->stopSection(); ?>