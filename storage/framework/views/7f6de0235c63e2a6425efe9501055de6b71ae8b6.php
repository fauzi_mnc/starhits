<!-- Stored in resources/views/child.blade.php -->


<?php if($alreadyUser = 'true'): ?>

    <?php $__env->startSection('title', 'already'); ?>

    <?php $__env->startSection('content'); ?>

    <div class="container-fluid mt-5" style="height:15em;">
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-danger" role="alert">
                    <h4 class="text-center">Hy <?php echo e($authCreators->name); ?>, you are already part of our creator. please visit your dashboard. <a style="text-decoration:none;" href="<?php echo e(url('/creator/serie')); ?>">Here</a></h4>
                </div>
            </div>
        </div>
    </div>

    <style>
        #footer{display: none !important}
    </style>

    <?php $__env->stopSection(); ?>

<?php else: ?>

    <?php if($totalSubscriber > $minSubcriber && $statusSubcriber = 'false'): ?>

    <?php $__env->startSection('title', 'Join'); ?>

    <?php $__env->startSection('content'); ?>
    <!-- Page Content -->
    <div class="container-fluid access">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h2>
                        Join Here For Dashboard Access
                    </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 box-one">
                    <div class="card">
                        <div class="card-body">
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                We have successfully connected with your Youtube channel and collected following information:
                            </div>
                            <?php $links = explode("/", $authCreators->image);?>

                            <?php if(in_array($links[0], ['http:', 'https:'])): ?>
                                <img class="img-fluid" src="<?php echo e($authCreators->image); ?>" style="width: 150px; height: 150px; margin:0 auto; display:block; border-radius:5px;"/>
                            <?php else: ?>
                                <img class="img-fluid" src="<?php echo e(url('/'). Image::url($authCreators->image,150,150,array('crop'))); ?>" style="margin:0 auto; display:block; border-radius:5px;" />
                            <?php endif; ?>
                            <form>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">
                                        Channel Title
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" readonly class="form-control-plaintext" id="#" value="<?php echo e($authCreators->name_channel); ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">
                                        Email
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="email" readonly class="form-control-plaintext" id="#" value="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">
                                        Channel View
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" readonly class="form-control-plaintext" id="#" value="<?php echo e($channelView); ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">
                                        Subscribers
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" readonly class="form-control-plaintext" id="#" value="<?php echo e($totalSubscriber); ?>">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label">
                                        Videos
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" readonly class="form-control-plaintext" id="#" value="<?php echo e($totalVideo); ?>">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 box-two">
                    <div class="card">
                        <div class="card-body">
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                Please fill out the following form in order to complete your application to starhits.id
                            </div>
                            <?php if(count($errors)): ?>
                                <div class="alert alert-danger slideup">
                                    <strong>Whoops!</strong>
                                    <br/>
                                    There were some problems with your input.
                                    <br/>
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                            <form method="POST" action="<?php echo e(route('creators.validate.update',$authCreators->provider_id)); ?>" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>

                                <?php echo e(method_field('PUT')); ?>

                                <?php echo e(method_field('PATCH')); ?>

                                <input name="subscribers" type="hidden" value="<?php echo e($totalSubscriber); ?>">
                                <input name="channelview" type="hidden" value="<?php echo e($channelView); ?>">
                                <input name="totalvideo" type="hidden" value="<?php echo e($totalVideo); ?>">
                                <div class="form-group row<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                                    <div class="col">
                                        <input class="form-control" name="name" value="<?php echo e(old('name')); ?>"  placeholder="Full Name**" type="text"  class="form-control required">
                                        </input>
                                        <span class="text-danger"><?php echo e($errors->first('name')); ?></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6 dempet <?php echo e($errors->has('gender') ? ' has-error' : ''); ?>">
                                        <select class="form-control" name="gender">
                                            <option value="" selected>
                                                Select Gender
                                            </option>
                                            <option value="male">
                                                Male
                                            </option>
                                            <option value="female">
                                                Female
                                            </option>
                                        </select>
                                        <span class="text-danger"><?php echo e($errors->first('gender')); ?></span>
                                    </div>
                                    <div class="col-sm-6 <?php echo e($errors->has('dob') ? ' has-error' : ''); ?>">
                                        <input id="datepicker" name="dob" placeholder="Date Of Birth" class="form-control required"/>
                                        <span class="text-danger"><?php echo e($errors->first('dob')); ?></span>
                                    </div>
                                </div>
                                <div class="form-group row<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                                    <div class="col-sm-6 dempet">
                                        <input class="form-control" name="email" placeholder="Email Address" type="email" class="form-control required">
                                        <span class="text-danger"><?php echo e($errors->first('email')); ?></span>
                                    </div>
                                    <div class="col-sm-6<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                                        <input class="form-control" name="password" placeholder="Password" type="password" class="form-control required">
                                        <span class="text-danger"><?php echo e($errors->first('password')); ?></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col<?php echo e($errors->has('cover') ? ' has-error' : ''); ?>">
                                        <div class="custom-file">
                                            <input class="custom-file-input" type="file" name="cover">
                                                <label class="custom-file-label" for="customFile">
                                                    Upload Cover
                                                </label>
                                        </div>
                                        <span class="text-danger"><?php echo e($errors->first('cover')); ?></span>
                                    </div>
                                </div>
                                <div class="col-md-12 box-three">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-check">
                                                <input class="form-check-input" id="accept" type="checkbox">
                                                    <label for="accept" class="form-check-label">
                                                        I agree to the General Terms and Conditions and understand that i am electronically signing this agreement to join the Starhits.id
                                                    </label>
                                                </input>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 box-four">
                                    <button class="btn btn-warning" id="form-submitbtn" disabled="disabled" type="submit">
                                        Apply Now
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php $__env->stopSection(); ?>

    <?php elseif($statusSubcriber == 'true'): ?>

    <?php $__env->startSection('title', 'failed'); ?>

    <?php $__env->startSection('content'); ?>

    <div class="container-fluid" style="height:15em;">
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-danger" role="alert">
                    <h4 class="text-center">Your Subcribers Count is hidden, please display Your Subcribers Count. <a style="text-decoration:none;" href="https://www.youtube.com/advanced_settings">Here</a></h4>
                </div>
                <div class="text-center">
                    <p>Auto redirect to Home in <span id="count">10</span> second</p>
                    <p>or</p>
                    <p> Back to <a style="text-decoration:none;" href="<?php echo e(url('/logout')); ?>" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" >Homepage</a>
                </div>
            </div>
        </div>
    </div>
    <form action="<?php echo e(url('/logout')); ?>" id="logout-form1" method="POST" style="display: none;">
        <?php echo e(csrf_field()); ?>

        <input style="display: none;" type="submit" value="logout">
        </input>
    </form>
    <script type="text/javascript">
        window.onload = function(){
    
        (function(){
            var counter = 10;
        
            setInterval(function() {
            counter--;
            if (counter >= 0) {
                span = document.getElementById("count");
                span.innerHTML = counter;
            }
            // Display 'counter' wherever you want to display it.
            if (counter === 0) {
                document.getElementById("logout-form1").submit();
                clearInterval(counter);
            }
            
            }, 1000);
            
        })();
            
        }
    </script>
    <?php $__env->stopSection(); ?>

    <?php else: ?>

    <?php $__env->startSection('title', 'failed'); ?>

    <?php $__env->startSection('content'); ?>

    <div class="container-fluid" style="height:15em;">
        <div class="row">
            <div class="col-sm-12">
                <div class="alert alert-danger" role="alert">
                    <h4 class="text-center"> You can't register because Your Subcribers less than 1000 subscribers. </h4>
                </div>
                <div class="text-center">
                    <p>Auto redirect to Home in <span id="count">5</span> second</p>
                    <p>or</p>
                    <p> Back to <a style="text-decoration:none;" href="<?php echo e(url('/logout')); ?>" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" >Homepage</a>
                </div>
            </div>
        </div>
    </div>
    <form action="<?php echo e(url('/logout')); ?>" id="logout-form" method="POST" style="display: none;">
        <?php echo e(csrf_field()); ?>

        <input style="display: none;" type="submit" value="logout">
        </input>
    </form>
    <script type="text/javascript">
        window.onload = function(){
    
        (function(){
            var counter = 5;
        
            setInterval(function() {
            counter--;
            if (counter >= 0) {
                span = document.getElementById("count");
                span.innerHTML = counter;
            }
            // Display 'counter' wherever you want to display it.
            if (counter === 0) {
                document.getElementById("logout-form").submit();
                clearInterval(counter);
            }
            
            }, 1000);
            
        })();
            
        }
    </script>
    <?php $__env->stopSection(); ?>

    <?php endif; ?>

<?php endif; ?>

<?php $__env->startPush('scripts'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        $(".slideup").fadeTo(2000, 500).slideUp(500, function(){
            $(".slideup").slideUp(500);
        });
        $(".alert-success").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert-success").slideUp(500);
        });
    });
	$('#accept').click(function() {
		if ($('#form-submitbtn').is(':disabled')) {
	    	$('#form-submitbtn').removeAttr('disabled');
	    } else {
	    	$('#form-submitbtn').attr('disabled', 'disabled');
	    }
	});

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputgambar").change(function () {
        readURL(this);
    });
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>