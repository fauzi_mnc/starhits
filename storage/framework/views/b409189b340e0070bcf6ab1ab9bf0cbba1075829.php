<!-- Stored in resources/views/child.blade.php -->


<?php $__env->startSection('title', 'Grow with us'); ?>

<?php $__env->startSection('content'); ?>

<div class="container-fluid join">
    <div class="row">
        <div class="content-text">
            <div class="col-md-12">
                <div class="top-half">
                    <button class="join">
                        GROW WITH US
                    </button>
                    <h5 class="top-title">
                        Apply For Partnership
                    </h5>
                    <div class="top-description">
                        <p class="description-text">
                            To begin with, we need to connect to your YouTube account.
                        </p>
                        <p class="description-text">
                            Your data & privacy is our prime priority. We will collect the following data only
                        </p>
                    </div>
                    <div class="icons">
                        <div class="row">
                            <?php $__currentLoopData = $partnerShip; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-md-4">
                                <img class="icon1" src="<?php echo e(url('/'). Image::url($value->image,98,98,array('crop'))); ?>">
                                    <p class="icon1-description">
                                        <?php echo e($value->title); ?>

                                    </p>
                                </img>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                    <?php if(auth()->guard()->guest()): ?>
                    <a href="<?php echo e(url('auth/youtube')); ?>">
                        <button class="btn authenticate">
                            AUTHENTICATE YOUR CHANNEL
                        </button>
                    </a>
                    <?php endif; ?>
                </div>
            </div>
            <div class="container-fluid join-recommended">
                <div class="title">
                    <h7>
                        SOME OF OUR CREATORS
                    </h7>
                </div>
                <div class="col-md-12">
                    <div class="<?php echo e(count($someCreators)>=1? 'row text-center text-lg-left item-others-videos':''); ?>">
                        <?php $__empty_1 = true; $__currentLoopData = $someCreators; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                        <div class="col-sm-3 col-others-videos">
                            <div class="item">
                                <div class="imgTitle">
                                    <div class="top-videos-overlay">
                                        <a href="<?php echo e(url('/creators/'.$value->slug)); ?>">
                                            <?php if(!empty($value->cover)): ?>
                                                <img alt="" class="img-fluid top-videos-overlay" src="<?php echo e(url('/'). Image::url($value->cover,400,250,array('crop'))); ?>"/>
                                            <?php else: ?>
                                                <img class="img-fluid top-videos-overlay" src="<?php echo e(url('/'). Image::url('frontend/assets/img/icon/default.png',400,250,array('crop'))); ?>"/>
                                            <?php endif; ?>
                                        </a>
                                    </div>
                                </div>
                                <p>
                                    <a href="<?php echo e(url('/creators/'.$value->slug)); ?>">
                                        <?php echo e($value->name); ?>

                                    </a>
                                </p>
                                <label>
                                    <a href="#">
                                        <?php echo e($value->totalVideos); ?> Videos
                                    </a>
                                </label>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <div class="col-sm-12">
                            <br>
                                <div class="alert alert-warning text-center" role="alert">
                                    There is no data to display!
                                </div>
                            </br>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="bottom-half">
                        <div class="bottom-info">
                            <h5 class="bottom-title">
                                Why You Should Join Us?
                            </h5>
                            <p class="bottom-description">
                                To begin with, we need to connect to your YouTube account.
                                <br>
                                    Your data & privacy is our prime priority. We will collect the following data only
                                </br>
                            </p>
                            <div class="icons">
                                <div class="row">
                                    <div class="col">
                                    </div>
                                    <div class="col-md-7">
                                        <div class="row">
                                            <?php $__currentLoopData = $joinUs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="col-md-4 item-icons">
                                                <div class="left-square">
                                                    <img alt="" class="img-fluid" src="<?php echo e(url('/'). Image::url($value->image,165,165,array('crop'))); ?>">
                                                    </img>
                                                </div>
                                                <p class="square-description">
                                                    <?php echo e($value->title); ?>

                                                </p>
                                            </div>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                    </div>
                                    <div class="col">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>