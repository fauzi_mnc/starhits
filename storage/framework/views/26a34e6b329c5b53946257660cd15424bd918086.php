<!-- Stored in resources/views/child.blade.php -->


<?php $__env->startSection('title', 'Channels'); ?>

<?php $__env->startPush('preloader'); ?>
<div class="se-pre-con">
    <div class="col-sm-12 text-center pre-con">
        <div class="col">
            <div class="logo-preloader">
                <img alt="" class="img-fluid" src="<?php echo e(asset('frontend/assets/img/logo-home.png')); ?>"></img>
            </div>
        </div>
        <div class="col">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg">
                <defs>
                    <filter id="gooey">
                        <fegaussianblur in="SourceGraphic" result="blur" stddeviation="10">
                        </fegaussianblur>
                        <fecolormatrix in="blur" mode="matrix" result="goo" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7">
                        </fecolormatrix>
                        <feblend in="SourceGraphic" in2="goo">
                        </feblend>
                    </filter>
                </defs>
            </svg>
            <div class="blob blob-0"></div>
            <div class="blob blob-1"></div>
            <div class="blob blob-2"></div>
            <div class="blob blob-3"></div>
            <div class="blob blob-4"></div>
            <div class="blob blob-5"></div>
        </div>
    </div>
</div>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

<div class="container-fluid channels">
    <div class="row title">
        <div class="col-md-12">
            <h4>
                Channels
            </h4>
        </div>
    </div>
    <div class="col-md-12">
        <div class="<?php echo e(count($channels)>=1? 'sub-channels loadMore':''); ?>">
        <?php $__empty_1 = true; $__currentLoopData = $channels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <div class="col-sm-12 item-sub-channels">
                <div>
                    <div class="row mid-sub-channels">
                        <div class="col-sm-2 item-mid-sub-channels-image">
                            <a class="d-block" href="<?php echo e(url('/channels/'.$value->slug)); ?>">
                                <img src="<?php echo e(url('/'). Image::url($value->image,150,150,array('crop'))); ?>" />
                            </a>
                        </div>
                        <div class="col-sm-5 item-mid-sub-channels-text">
                            <div class="item-mid-sub-channels-text-title">
                                <h4>
                                    <a href="<?php echo e(url('/channels/'.$value->slug)); ?>">
                                        <?php echo e(strtoupper($value->title)); ?>

                                    </a>
                                </h4>
                            </div>
                            <div class="item-mid-sub-channels-text-paragraph">
                                <p>
                                    <?php echo str_limit($value->content, 100); ?>

                                </p>
                            </div>
                            <div class="item-mid-sub-channels-text-videos">
                                <label>
                                    <?php echo e($value->totalvideo); ?> Videos | <?php echo e($value->totalseries); ?> Series
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row mid-sub-channels-videos">
                        <div class="col-md-12" style="padding: 0;">
                            <div class="title">
                                <h7>
                                    LATEST <?php echo e(strtoupper($value->title)); ?>

                                </h7>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding: 0;">
                            <div class="<?php echo e(count($videos[$value->id])>=1? 'slider-mid-sub-channels-videos':''); ?>">
                                <?php $__empty_2 = true; $__currentLoopData = $videos[$value->id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vid): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_2 = false; ?>
                                <div>
                                    <div class="item">
                                        <div class="imgTitle">
                                            <div class="mid-sub-channels-videos-overlay">
                                                <a href="<?php echo e(url('/videos/'.$vid->slug)); ?>">
                                                    <?php if($vid->image): ?>
                                                        <img class="img-fluid mid-sub-channels-videos-overlay" src="<?php echo e(url('/'). Image::url($vid->image,400,225,array('crop'))); ?>" />
                                                    <?php else: ?>
                                                        <img class="img-fluid mid-sub-channels-videos-overlay" src="https://img.youtube.com/vi/<?php echo e($vid->attr_6); ?>/mqdefault.jpg" style="width: 400; height: 250;" />
                                                    <?php endif; ?>
                                                </a>
                                                <?php if(!empty($value->attr_7)): ?>
                                                    <div class="item-mid-sub-channels-videos-overlay">
                                                        <?php echo e($duration->formatted($vid->attr_7)); ?>

                                                    </div>
                                                <?php else: ?>

                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <p>
                                            <a href="<?php echo e(url('/videos/'.$vid->slug)); ?>">
                                                <?php echo e($vid->title); ?>

                                            </a>
                                        </p>
                                    </div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_2): ?>
                                <div class="col-sm-12">
                                <br>
                                    <div class="alert alert-warning text-center" role="alert">
                                      There is no data to display!
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="row mid-sub-channels-series">
                        <div class="col-md-12" style="padding: 0;">
                            <div class="title">
                                <h7>
                                    LATEST <?php echo e(strtoupper($value->title)); ?> SERIES
                                </h7>
                            </div>
                        </div>
                        <div class="col-sm-12" style="padding: 0;">
                            <div class="<?php echo e(count($series[$value->id])>=1? 'slider-mid-sub-channels-series':''); ?>">
                                <?php $__empty_2 = true; $__currentLoopData = $series[$value->id]; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ser): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_2 = false; ?>
                                <div>
                                    <div class="item">
                                        <article class="caption">
                                            <img class="caption__media img-fluid" src="<?php echo e(url('/'). Image::url($ser->image,400,550,array('crop'))); ?>" />
                                            <a href="<?php echo e(url('/series/'.$ser->slug)); ?>">
                                                <div class="caption__overlay">
                                                    <h1 class="caption__overlay__title">
                                                        <?php echo e($ser->title); ?>

                                                    </h1>
                                                    <h7 class="caption__overlay__content">
                                                        <?php echo e($ser->totalVideo); ?> Videos
                                                    </h7>
                                                    <?php if(!empty($ser->excerpt)): ?>
                                                    <p class="caption__overlay__content">
                                                        <?php echo strip_tags($ser->excerpt); ?>

                                                    </p>
                                                    <?php else: ?>
                                                    <p class="caption__overlay__content">
                                                        <?php echo strip_tags(str_limit($ser->content, 100)); ?>

                                                    </p>
                                                    <?php endif; ?>
                                                </div>
                                            </a>
                                        </article>
                                        <p>
                                            <a href="<?php echo e(url('/series/'.$ser->slug)); ?>">
                                                <?php echo e($ser->title); ?>

                                            </a>
                                        </p>
                                        <p class="videos">
                                            <a href="#">
                                                <?php echo e($ser->totalVideo); ?> Videos
                                            </a>
                                        </p>
                                    </div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_2): ?>
                                <div class="col-sm-12">
                                <br>
                                    <div class="alert alert-warning text-center" role="alert">
                                      There is no data to display!
                                    </div>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        <div class="col-sm-12">
        <br>
            <div class="alert alert-warning text-center" role="alert">
              There is no data to display!
            </div>
        </div>
        <?php endif; ?>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>