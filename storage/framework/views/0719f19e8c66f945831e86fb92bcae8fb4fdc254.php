<!-- Stored in resources/views/child.blade.php -->


<?php $__env->startSection('title', 'Page Title'); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid series">
    <div class="col-md-12">
        <div class="col-sm-12">
            <div class="title">
                <div class="col-sm-12" style="padding: 0;">
                    <div class="row">
                        <div class="col-md-8" style="padding: 0;">
                            <h7>
                                SERIES
                            </h7>
                        </div>
                        <div class="col-md-4" style="padding: 0;">
                            <a class="btn btn-filter filter" data-target=".bs-example-modal-lg-filter" data-toggle="modal" href="#">
                                <i class="fa fa-filter" style="font-size:24px">
                                </i>
                                Filter
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="<?php echo e(count($series)>=1? 'row item-series':''); ?>">
            <?php $__empty_1 = true; $__currentLoopData = $series; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <div class="col-lg-6 col-series">
                <div class="row">
                    <div class="col-sm-6 inner-col-series-left">
                        <article class="caption">
                            <a href="<?php echo e(url('/series/'.$value->slug)); ?>">
                                <img class="caption__media img-fluid" src="<?php echo e(url('/'). Image::url($value->image,350,500,array('crop'))); ?>"/>
                            </a>
                            <a href="#">
                                <div class="caption__overlay">
                                    <h1 class="caption__overlay__title">
                                        <?php echo e($value->title); ?>

                                    </h1>
                                    <p class="caption__overlay__content">
                                        <?php echo str_limit($value->content, 200); ?>

                                    </p>
                                </div>
                            </a>
                        </article>
                    </div>
                    <div class="col-sm-6 inner-col-series-right">
                        <div class="title-series">
                            <h7>
                                <a href="<?php echo e(url('/series/'.$value->slug)); ?>">
                                    <?php echo e($value->title); ?>

                                </a>
                            </h7>
                        </div>
                        <div class="description text-justify">
                            <p>
                                <?php echo str_limit($value->content, 200); ?>

                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            <div class="col-sm-12">
                <br>
                    <div class="alert alert-warning text-center" role="alert">
                        There is no data to display!
                    </div>
                </br>
            </div>
            <?php endif; ?>
        </div>
        <div class="col-md-12 page">
            <div class="row navigation">
                <?php echo e($series->links('vendor.pagination.custom')); ?>

            </div>
        </div>
    </div>
</div>

<?php echo $__env->make('frontend.series.modals-filter', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>