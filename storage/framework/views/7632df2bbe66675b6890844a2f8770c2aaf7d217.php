<div class='btn-group'>
    <a href="<?php echo e(route('page.show', $id)); ?>" class='btn btn-default btn-xs' title="Show Page">
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="<?php echo e(route('page.edit', $id)); ?>" class='btn btn-default btn-xs' title="Edit Page">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
</div>