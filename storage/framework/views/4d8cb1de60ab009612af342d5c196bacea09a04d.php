<?php $__env->startPush('meta'); ?>    
        <meta property="og:url" content="<?php echo e(url()->current()); ?>" />
        <meta property="og:type" content="#" />
        <meta property="og:title" content="<?php if(isset($models['website_title'])): ?><?php echo e($models['website_title']); ?><?php endif; ?>" />
        <meta property="og:description" content="<?php if(isset($models['website_slogan'])): ?><?php echo e($models['website_slogan']); ?><?php endif; ?>" />
        <meta property="og:image" content="<?php echo e(url($models['website_logo_header'])); ?>" />
        <meta property="fb:app_id" content="952765028218113" />
<?php $__env->stopPush(); ?>

<!-- Stored in resources/views/child.blade.php -->


<?php $__env->startPush('preloader'); ?>
<div class="se-pre-con">
    <div class="col-sm-12 text-center pre-con">
        <div class="col">
            <div class="logo-preloader">
                <img alt="" class="img-fluid" src="<?php echo e(asset('frontend/assets/img/logo-home.png')); ?>">
                </img>
            </div>
        </div>
        <div class="col">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg">
                <defs>
                    <filter id="gooey">
                        <fegaussianblur in="SourceGraphic" result="blur" stddeviation="10">
                        </fegaussianblur>
                        <fecolormatrix in="blur" mode="matrix" result="goo" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7">
                        </fecolormatrix>
                        <feblend in="SourceGraphic" in2="goo">
                        </feblend>
                    </filter>
                </defs>
            </svg>
            <div class="blob blob-0"></div>
            <div class="blob blob-1"></div>
            <div class="blob blob-2"></div>
            <div class="blob blob-3"></div>
            <div class="blob blob-4"></div>
            <div class="blob blob-5"></div>
        </div>
    </div>
</div>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?> 

<?php if(empty($SiteConfig['configs']['is_streaming'])): ?>
<div class="container-fluid top-content">
    <div class="row">
        <div class="col-md-8">
            <div class="slider-homepage">
                <?php $__currentLoopData = $videoStarHits; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div>
                    <div class="top-block">
                        <div class="top-media">
                            <div class="thumb-homepage">
                                <?php if($value->post_type == 'automatic'): ?>
                                <a class="play" href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                    <img src="<?php echo e(asset('frontend/assets/img/icon/play_mobile_480_black.png')); ?>" alt="Play">
                                </a>
                                <a class="target" href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                    <?php echo e($value->title); ?>

                                </a>
                                <?php elseif($value->post_type == 'manual'): ?>
                                    <?php if(!empty($value->attr_6)): ?>
                                    <a class="play" href="<?php echo e(url('/article/'.$value->slug)); ?>">
                                        <img src="<?php echo e(asset('frontend/assets/img/icon/play_mobile_480_black.png')); ?>" alt="Play">
                                    </a>
                                    <a class="target" href="<?php echo e(url('/article/'.$value->slug)); ?>">
                                        <?php echo e($value->title); ?>

                                    </a>
                                    <?php elseif(empty($value->attr_6)): ?>
                                    <a class="target" href="<?php echo e(url('/article/'.$value->slug)); ?>">
                                        <?php echo e($value->title); ?>

                                    </a>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>


                            <!-- Check status header -->
                            <?php if($value->post_type == 'automatic'): ?>
                                <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                    <?php if(!empty($value->image)): ?>
                                        <img class="img-fluid" src="<?php echo e(url('/'). Image::url($value->image,886,490,array('crop'))); ?>"/>
                                    <?php else: ?>
                                        <?php $headers = get_headers("https://i3.ytimg.com/vi/".$value->attr_6."/maxresdefault.jpg", 1);
                                            $header = substr($headers[0], 9, 3);
                                        ?>

                                        <?php if($header == 404):?>
                                            <img class="img-fluid" src="https://i3.ytimg.com/vi/<?php echo e($value->attr_6); ?>/mqdefault.jpg"/>
                                        <?php else:?>
                                            <img class="img-fluid" src="https://i3.ytimg.com/vi/<?php echo e($value->attr_6); ?>/maxresdefault.jpg"/>
                                        <?php endif?>
                                    <?php endif; ?>
                                </a>
                                <?php elseif($value->post_type == 'manual'): ?>
                                <a href="<?php echo e(url('/article/'.$value->slug)); ?>">
                                    <?php if(!empty($value->image)): ?>
                                        <img class="img-fluid" src="<?php echo e(url('/'). Image::url($value->image,886,490,array('crop'))); ?>"/>
                                    <?php else: ?>
                                        <?php $headers = get_headers("https://i3.ytimg.com/vi/".$value->attr_6."/maxresdefault.jpg", 1);
                                            $header = substr($headers[0], 9, 3);
                                        ?>
                                        <?php if($header == 404):?>
                                            <img class="img-fluid" src="https://i3.ytimg.com/vi/<?php echo e($value->attr_6); ?>/mqdefault.jpg"/>
                                        <?php else:?>
                                            <img class="img-fluid" src="https://i3.ytimg.com/vi/<?php echo e($value->attr_6); ?>/maxresdefault.jpg"/>
                                        <?php endif?>
                                    <?php endif; ?>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
        <div class="col-md-4 listing-block">
            <div class="title">
                <h7>
                    LATEST FROM STAR HITS
                </h7>
            </div>
            <div class="all-media">
                <div class="<?php echo e(count($videoStarHits)>=1? 'all-media-child':''); ?>">
                    <?php $__empty_1 = true; $__currentLoopData = $videoStarHits; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <div class="media">
                        <?php if(!empty($value->attr_7)): ?>
                            <div class="item-top-overlay">
                                <?php echo e($duration->formatted($value->attr_7)); ?>

                            </div>
                        <?php else: ?>

                        <?php endif; ?>

                        <?php if($value->post_type == 'automatic'): ?>
                            <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                <?php if(!empty($value->image)): ?>
                                    <img class="d-flex align-self-start img-fluid top-overlay" src="<?php echo e(url('/'). Image::url($value->image,550,350,array('crop'))); ?>"/>
                                <?php else: ?>
                                    <img class="d-flex align-self-start img-fluid top-overlay" src="https://i3.ytimg.com/vi/<?php echo e($value->attr_6); ?>/mqdefault.jpg" style="width: 513; height: 350;"/>
                                <?php endif; ?>
                            </a>
                            <?php elseif($value->post_type == 'manual'): ?>
                            <a href="<?php echo e(url('/article/'.$value->slug)); ?>">
                                <?php if(!empty($value->image)): ?>
                                    <img class="d-flex align-self-start img-fluid top-overlay" src="<?php echo e(url('/'). Image::url($value->image,550,350,array('crop'))); ?>"/>
                                <?php else: ?>
                                    <img class="d-flex align-self-start img-fluid top-overlay" src="https://i3.ytimg.com/vi/<?php echo e($value->attr_6); ?>/mqdefault.jpg" style="width: 513; height: 350;"/>
                                <?php endif; ?>
                            </a>
                        <?php endif; ?>
                        <div class="media-body pl-3">
                            <div class="media-title-series">
                                <?php if(!empty($value->series()->first()->title)): ?>
                                <a href="#">
                                        <?php echo e($value->series()->first()->title); ?>

                                    <?php else: ?>
                                        <?php echo e($value->channelVideo()->first()->title); ?>

                                </a>
                                <?php endif; ?>
                            </div> 
                            <div class="media-title">
                                <?php if($value->post_type == 'manual'): ?>
                                <a href="<?php echo e(url('/article/'.$value->slug)); ?>">
                                    <?php echo e($value->title); ?>

                                </a>
                                <?php else: ?>
                                <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                    <?php echo e($value->title); ?>

                                </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php else: ?>
<?php echo $__env->make('frontend.partials._live-stream', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php endif; ?>
<div class="col-sm-12 main-ads text-center">
    <a href="#"><img class="center-ads" src="<?php echo e(asset('frontend/assets/img/ads-728x90-web-series.jpg')); ?>" alt=""></a>
</div>

<div class="container-fluid top-videos">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <div class="title">
                <h7>
                    WEEKLY TOP VIDEOS
                </h7>
            </div>
        </div>
        <div class="col-md-12">
            <div class="<?php echo e(count($weeklyTopVideos)>=1? 'slider-top-videos':''); ?>">
                <?php $__empty_1 = true; $__currentLoopData = $weeklyTopVideos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <div>
                    <div class="item">
                        <div class="imgTitle">
                            <div class="top-videos-overlay">
                                <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                    <?php if(!empty($value->image)): ?>
                                        <img src="<?php echo e(url('/'). Image::url($value->image,400,225,array('crop'))); ?>" class="img-fluid top-videos-overlay" alt="">
                                    <?php else: ?>
                                        <img class="img-fluid top-videos-overlay" src="https://i3.ytimg.com/vi/<?php echo e($value->attr_6); ?>/mqdefault.jpg" style="width: 400; height: 225;"/>
                                    <?php endif; ?>
                                </a>
                                <?php if(!empty($value->attr_7)): ?>
                                    <div class="item-top-videos-overlay">
                                        <?php echo e($duration->formatted($value->attr_7)); ?>

                                    </div>
                                <?php else: ?>

                                <?php endif; ?>
                            </div>
                        </div>
                        <p>
                            <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                <?php echo e($value->title); ?>

                            </a>
                        </p>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                            There is no data to display!
                        </div>
                    </br>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid top-hits">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <div class="title">
                <h7>
                    STAR HITS SERIES
                </h7>
            </div>
        </div>
        <div class="col-md-12">
            <div class="<?php echo e(count($seriesStarHits)>=1? 'slider-top-hits':''); ?>">
                <?php $__empty_1 = true; $__currentLoopData = $seriesStarHits; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <div>
                    <div class="item">
                        <article class="caption">
                            <img class="caption__media img-fluid" src="<?php echo e(url('/'). Image::url($value->image,350,500,array('crop'))); ?>"/>
                            <a href="<?php echo e(url('/series/'.$value->slug)); ?>">
                                <div class="caption__overlay">
                                    <h1 class="caption__overlay__title">
                                        <?php echo e($value->title); ?>

                                    </h1>
                                    <h7 class="caption__overlay__content">
                                        <?php echo e($value->totalVideo); ?> Videos
                                    </h7>
                                    <?php if(!empty($value->excerpt)): ?>
                                    <p class="caption__overlay__content">
                                        <?php echo strip_tags($value->excerpt); ?>

                                    </p>
                                    <?php else: ?>
                                    <p class="caption__overlay__content">
                                        <?php echo strip_tags(str_limit($value->content, 100)); ?>

                                    </p>
                                    <?php endif; ?>
                                </div>
                            </a>
                        </article>
                        <p>
                            <a href="<?php echo e(url('/series/'.$value->slug)); ?>">
                                <?php echo e($value->title); ?>

                            </a>
                        </p>
                        <p class="videos">
                            <a href="#">
                                <?php echo e($value->totalVideo); ?> Videos
                            </a>
                        </p>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                            There is no data to display!
                        </div>
                    </br>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid top-pro">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <div class="title">
                <h7>
                    STAR PRO SERIES
                </h7>
            </div>
        </div>
        <div class="col-md-12">
            <div class="<?php echo e(count($seriesStarPro)>=1? 'slider-top-pro':''); ?>">
                <?php $__empty_1 = true; $__currentLoopData = $seriesStarPro; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <div>
                    <div class="item">
                        <article class="caption">
                            <img class="caption__media img-fluid" src="<?php echo e(url('/'). Image::url($value->image,350,500,array('crop'))); ?>"/>
                            <a href="<?php echo e(url('/series/'.$value->slug)); ?>">
                                <div class="caption__overlay">
                                    <h1 class="caption__overlay__title">
                                        <?php echo e($value->title); ?>

                                    </h1>
                                    <h7 class="caption__overlay__content">
                                        <?php echo e($value->totalVideo); ?> Videos
                                    </h7>
                                    <?php if(!empty($value->excerpt)): ?>
                                    <p class="caption__overlay__content">
                                        <?php echo strip_tags($value->excerpt); ?>

                                    </p>
                                    <?php else: ?>
                                    <p class="caption__overlay__content">
                                        <?php echo strip_tags(str_limit($value->content, 100)); ?>

                                    </p>
                                    <?php endif; ?>
                                </div>
                            </a>
                        </article>
                        <p>
                            <a href="<?php echo e(url('/series/'.$value->slug)); ?>">
                                <?php echo e($value->title); ?>

                            </a>
                        </p>
                        <p class="videos">
                            <a href="#">
                                <?php echo e($value->totalVideo); ?> Videos
                            </a>
                        </p>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                            There is no data to display!
                        </div>
                    </br>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid recommended-videos">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <div class="title">
                <h7>
                    RECOMMENDED VIDEOS
                </h7>
            </div>
        </div>
        <div class="col-md-12">
            <div class="<?php echo e(count($recomendedVideos)>=1? 'slider-recommended-videos':''); ?>">
                <?php $__empty_1 = true; $__currentLoopData = $recomendedVideos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <div>
                    <div class="item">
                        <div class="imgTitle">
                            <div class="top-videos-overlay">
                                <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                    <?php if(!empty($value->image)): ?>
                                        <img class="img-fluid top-videos-overlay" src="<?php echo e(url('/'). Image::url($value->image,400,225,array('crop'))); ?>"/>
                                    <?php else: ?>
                                        <img class="img-fluid top-videos-overlay" src="https://i3.ytimg.com/vi/<?php echo e($value->attr_6); ?>/mqdefault.jpg" style="width: 400; height: 250;"/>
                                    <?php endif; ?>
                                </a>
                                <?php if(!empty($value->attr_7)): ?>
                                    <div class="item-top-videos-overlay">
                                        <?php echo e($duration->formatted($value->attr_7)); ?>

                                    </div>
                                <?php else: ?>

                                <?php endif; ?>
                            </div>
                        </div>
                        <p>
                            <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                <?php echo e($value->title); ?>

                            </a>
                        </p>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                            There is no data to display!
                        </div>
                    </br>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid others-videos">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <div class="title">
                <h7>
                    OTHERS VIDEOS
                </h7>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="<?php echo e(count($otherVideos)>=1? 'row text-center text-lg-left item-others-videos loadMore':''); ?>">
                <?php $__empty_1 = true; $__currentLoopData = $otherVideos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <div class="col-sm-3 col-others-videos">
                    <div class="item">
                        <div class="imgTitle">
                            <div class="top-videos-overlay">
                                <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                    <?php if(!empty($value->image)): ?>
                                        <img class="img-fluid top-videos-overlay" src="<?php echo e(url('/'). Image::url($value->image,400,225,array('crop'))); ?>"/>
                                    <?php else: ?>
                                        <img class="img-fluid top-videos-overlay" src="https://i3.ytimg.com/vi/<?php echo e($value->attr_6); ?>/mqdefault.jpg" style="width: 400; height: 250;"/>
                                    <?php endif; ?>
                                </a>
                                <?php if(!empty($value->attr_7)): ?>
                                    <div class="item-top-videos-overlay">
                                        <?php echo e($duration->formatted($value->attr_7)); ?>

                                    </div>
                                <?php else: ?>

                                <?php endif; ?>
                            </div>
                        </div>
                        <p>
                            <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                <?php echo e($value->title); ?>

                            </a>
                        </p>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                            There is no data to display!
                        </div>
                    </br>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>