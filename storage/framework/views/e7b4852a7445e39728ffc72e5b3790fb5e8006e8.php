<?php $__env->startSection('title', 'Create User'); ?>

<?php $__env->startSection('content'); ?>
<div class="text-center">
    <h1>
        Create Star Hits account
    </h1>
</div>
<div class="w-75 mx-auto">
    <form method="post" action="<?php echo e(route('register')); ?>">
        <?php echo e(csrf_field()); ?>

        <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
            <input class="form-control required" name="name" value="<?php echo e(old('name')); ?>" placeholder="Full Name" style="font-family:Fontawesome;" type="text">
            </input>
        </div>
        <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
            <input class="form-control required" name="email" value="<?php echo e(old('email')); ?>" placeholder="Email" style="font-family:Fontawesome;" type="email">
            </input>
        </div>
        <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
            <input class="form-control required" name="password" placeholder="Password" style="font-family:Fontawesome;" type="password">
            </input>
        </div>
        <div class="form-group">
            <input class="form-control required" name="password_confirmation" placeholder="Confirm Password" style="font-family:Fontawesome;" type="password">
            </input>
        </div>
        <div class="form-group row">
            <div class="col-sm-12">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" id="accept">
                            <p style="font-size: 10px; margin-top: 3px;">
                                By Signing Up, You Agree to the
                                <a href="#">
                                    Term of Use
                                </a>
                                and
                                <a href="#">
                                    Privacy Policy
                                </a>
                            </p>
                        </input>
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group text-center">
            <button class="btn login-btn" id="form-signup" type="submit" disabled="disabled">
                Sign Up
            </button>
        </div>
        <div class="or-seperator">
        </div>
        <p class="text-center">
            Already a Star Hits User?
            <a aria-label="Close" data-dismiss="modal" data-target="#loginModal" data-toggle="modal" href="#">
                Login now!
            </a>
        </p>
    </form>
</div>

<?php $__env->startPush('scripts'); ?>
<script type="text/javascript">
    $('#accept').click(function() {
        if ($('#form-signup').is(':disabled')) {
            $('#form-signup').removeAttr('disabled');
        } else {
            $('#form-signup').attr('disabled', 'disabled');
        }
    });
</script>
<?php $__env->stopPush(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>