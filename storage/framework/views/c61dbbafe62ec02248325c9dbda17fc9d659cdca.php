<?php $__env->startSection('breadcrumb'); ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Dashboard</h2>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentCrud'); ?>

<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <a href="<?php echo e(route('admin.posts.index')); ?>"><h5>Videos</h5></a>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php echo e($videos); ?></h1>
                    <small>Total Videos</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <a href="<?php echo e(route('admin.posts.index')); ?>"><h5>Articles</h5></a>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php echo e($articles); ?></h1>
                    <small>Total Articles</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <a href="<?php echo e(route('channel.index')); ?>"><h5>Channels</h5></a>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php echo e($channels); ?></h1>
                    <small>Total Channels</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <a href="<?php echo e(route('admin.serie.index')); ?>"><h5>Series</h5></a>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php echo e($series); ?></h1>
                    <small>Total Series</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <a href="<?php echo e(route('creator.index')); ?>"><h5>Creators</h5></a>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php echo e($creators); ?></h1>
                    <small>Total Creators</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <a href="<?php echo e(route('user.index')); ?>"><h5>Not Creators</h5></a>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins"><?php echo e($not_creators); ?></h1>
                    <small>Total Not Creators</small>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>