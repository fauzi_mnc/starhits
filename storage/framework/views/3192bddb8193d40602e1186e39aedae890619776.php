<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('css/plugins/switchery/switchery.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('css/plugins/bootstrap-select.min.css')); ?>" rel="stylesheet">
<style>
.wizard > .content > .body{
    position: relative !important;
}
strong{
    padding-right: 30px;
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb'); ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Creator</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Creator</a>
            </li>
            <li class="active">
                <strong>Setting</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentCrud'); ?>
<?php if($errors->any()): ?>
    <div class="alert alert-danger">
        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
<?php endif; ?>
<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Setting
            </div>
            <div class="panel-body">
                <?php echo Form::model($user, ['route' => ['self.creator.update', $user->id, $context], 'method' => 'put', 'class' => 'form', 'files' => true, 'id' => 'example-form']); ?>

                <div>
                    <?php if($context == 'personal'): ?>
                    <input type="hidden" value="personal" name="context">
                    <h3>Personal Information</h3>
                    <section class="">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <?php echo Form::label('name', 'Full Name:', []); ?>

                                    <?php echo Form::text('name', null, ['class' => 'form-control required']); ?>

                                    
                                </div>
                                <div class="form-group">
                                    <?php echo Form::label('phone', 'Phone Number:', []); ?>

                                    <?php echo Form::text('phone', null, ['class' => 'form-control required']); ?>

                                    
                                </div>
                                <div class="form-group">
                                    <?php echo Form::label('country', 'Country:', []); ?>

                                    <?php echo Form::select('country', $countries, null, ['class' => 'form-control', 'placholder' => 'Select Country']); ?>

                                    
                                </div>
                                <div class="form-group">
                                    <?php echo Form::label('password', 'Password:', []); ?>

                                    
                                    <div class="input-group">
                                        <?php echo Form::password('password', ['class' => 'form-control required']); ?>

                                        <span class="input-group-btn"> 
                                            <button type="button" class="btn btn-warning pull-right" id="reset-pwd">Reset</button>
                                        </span>
                                    </div>    
                                    
                                </div>
                                <div class="form-group">
                                    <?php echo Form::label('username', 'Username Instagram:', []); ?>

                                    <?php echo Form::text('username', $user->detailInfluencer->username, ['class' => 'form-control required']); ?>

                                    <br>
                                    <button type="button" class="btn btn-primary pull-right" id="btn-check">Check</button>                                            
                                </div>
                                <div class="form-group" id="instagram-profile">
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <?php echo Form::label('category', 'Creator Category:', []); ?>

                                    <?php echo Form::select('category[]', $categories, explode(',', $user->detailInfluencer->category), ['class' => 'form-control selectpicker', 'multiple']); ?>

                                    
                                </div>
                                <div class="form-group">
                                    <?php echo Form::label('email', 'Email:', []); ?>

                                    <?php echo Form::email('email', null, ['class' => 'form-control required']); ?>

                                    
                                </div>
                                <div class="form-group">
                                    <?php echo Form::label('gender', 'Gender:', []); ?>

                                    <br>
                                    <label class="radio-inline">
                                        <input type="radio" name="gender" value="F" <?php echo e(($user->gender == 'F') ? 'checked' : ''); ?>>Female
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="gender" value="M" <?php echo e(($user->gender == 'M') ? 'checked' : ''); ?>>Male
                                    </label>
                                    
                                </div>
                                <div class="form-group">
                                    <?php echo Form::label('city', 'Town/City:', []); ?>

                                    <?php echo Form::select('city', $cities, null, ['class' => 'form-control selectpicker', 'placholder' => 'Select City', 'data-live-search' => 'true', 'data-size' => 7]); ?>

                                    
                                </div>
                                <div class="form-group">
                                    <?php echo Form::label('is_active', 'Is Active:', []); ?>

                                    <br>
                                    <?php echo Form::checkbox('is_active', null, ($user->detailInfluencer->is_active == 1) ? 'on' : '',['class' => 'form-control js-switch required', 'data-switchery' => true]); ?>

                                
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php elseif($context == 'rate_card' && auth()->user()->hasRole('influencer')): ?>
                    <input type="hidden" value="rate_card" name="context">
                    <h3>Rate Card</h3>
                    <section class="">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <?php echo Form::label('instagram_photo_posting_rate', 'Harga Posting Photo Instagram:', []); ?>

                                    <?php echo Form::text('instagram_photo_posting_rate', $user->detailInfluencer->instagram_photo_posting_rate, ['class' => 'form-control']); ?>

                                    
                                </div>
                                <div class="form-group">
                                    <?php echo Form::label('instagram_video_posting_rate', 'Harga Posting Video Instagram:', []); ?>

                                    <?php echo Form::text('instagram_video_posting_rate', $user->detailInfluencer->instagram_video_posting_rate, ['class' => 'form-control']); ?>

                                    
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <?php echo Form::label('instagram_story_posting_rate', 'Harga Posting Story Instagram:', []); ?>

                                    <?php echo Form::text('instagram_story_posting_rate', $user->detailInfluencer->instagram_story_posting_rate, ['class' => 'form-control']); ?>

                                    
                                </div>
                                <div class="form-group">
                                    <?php echo Form::label('youtube_posting_rate', 'Harga Posting Youtube:', []); ?>

                                    <?php echo Form::text('youtube_posting_rate', $user->detailInfluencer->youtube_posting_rate, ['class' => 'form-control']); ?>

                                    
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <?php echo Form::label('ig_highlight_rate', 'Harga Instagram Highlight:', []); ?>

                                    <?php echo Form::text('ig_highlight_rate', $user->detailInfluencer->ig_highlight_rate, ['class' => 'form-control']); ?>

                                    
                                </div>
                            </div>
                            
                            <?php if(auth()->user()->hasRole('admin')): ?>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <?php echo Form::label('package_rate', 'Harga Package:', []); ?>

                                    <?php echo Form::text('package_rate', $user->detailInfluencer->package_rate, ['class' => 'form-control']); ?>

                                    
                                </div>
                            </div>
                            <?php endif; ?>
                            
                        </div>
                    </section>
                    <?php elseif($context == 'payment' && auth()->user()->hasRole('influencer')): ?>
                    <input type="hidden" value="payment" name="context">
                    <h3>Payment Information</h3>
                    <section>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <?php echo Form::label('bank_id', 'Bank:', []); ?>

                                    
                                    <?php echo Form::select('bank_id', $banks,  $user->detailInfluencer->bank_id , ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => 7]); ?>

                                </div>
                                <div class="form-group">
                                    <?php echo Form::label('bank_account_number', 'Bank Account Number:', []); ?>

                                    <?php echo Form::text('bank_account_number', $user->detailInfluencer->bank_account_number, ['class' => 'form-control required']); ?>

                                    
                                </div>
                                <div class="form-group">
                                    <?php echo Form::label('bank_holder_name', 'Bank Holder Name:', []); ?>

                                    <?php echo Form::text('bank_holder_name',  $user->detailInfluencer->bank_holder_name, ['class' => 'form-control required']); ?>

                                    
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <?php echo Form::label('bank_location', 'Bank Location:', []); ?>

                                    <?php echo Form::text('bank_location',  $user->detailInfluencer->bank_location, ['class' => 'form-control']); ?>

                                    
                                </div>
                                <div class="form-group">
                                    <?php echo Form::label('npwp', 'NPWP:', []); ?>

                                    <?php echo Form::text('npwp',  $user->detailInfluencer->npwp, ['class' => 'form-control required']); ?>

                                    
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php elseif($context == 'social_media'): ?>
                    <input type="hidden" value="social_media" name="context">
                    <h3>Social Media</h3>
                    <section>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="input-group m-b"><span class="input-group-addon"><i class="fa fa-instagram"></i></span>
                                    <input type="text" id="username_instagram" name="socials[]" value="<?php echo e(($ig) ? $ig->url : ''); ?>" placeholder="username" class="form-control" maxlength="100">
                                    <span class="input-group-btn"> 
                                            <button type="button" class="btn btn-primary pull-right" id="btn-check-instagram">Check</button>
                                        </span>
                                </div>
                                <div id="instagram-profile">
                                </div>
                                <div class="input-group m-b"><span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                    <input type="text" name="socials[]" value="<?php echo e(($fb) ? $fb->url : ''); ?>" placeholder="https://www.facebook.com/username" class="form-control" maxlength="100">
                                </div>
                                <div class="input-group m-b"><span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                    <input type="text" name="socials[]" value="<?php echo e(($tw) ? $tw->url : ''); ?>" placeholder="https://twitter.com/username" class="form-control" maxlength="100">
                                </div>
                                <div class="input-group m-b"><span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                                    <input type="text" name="socials[]" value="<?php echo e(($gp) ? $gp->url : ''); ?>" placeholder="https://plus.google.com/u/0/username" class="form-control" maxlength="100">
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php endif; ?>
                </div>
                <button type="submit" class="btn btn-primary">Save Change</button>
                <a href="<?php echo e(route('self.creator.edit', [auth()->id(), 'personal'])); ?>" class="btn btn-default">Cancel</a>
                </form>
            </div>

        </div>
        
    </div>
</div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('scripts'); ?>
    <script src="<?php echo e(asset('js/plugins/switchery/switchery.js')); ?>"></script>
    <script src="<?php echo e(asset('js/plugins/bootstrap-select.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/plugins/typeahed.min.js')); ?>"></script>
    
    <script>
        $(document).ready(function(){
            var elem = document.querySelector('.js-switch');
            var switchery = new Switchery(elem, { disabled: true });
    
            $.get("<?php echo e(route('bank.index')); ?>", function(data){
                
                $("#bank_id_display").typeahead({ 
                    source:data.result,
                    afterSelect: function (item) {
                        $('#bank_id').val(item.id);
                    }
                });
            },'json');
    
        });
    
    <?php if($context == 'personal'): ?>
    $('#password').attr('disabled', true);
    $('#reset-pwd').click(function(){
        $('#password').val('');
        $('#password').attr('disabled', false);
    });
    <?php endif; ?>

    <?php if($context == 'social_media'): ?>
    $('#btn-check-instagram').click(function(){
        var username = $('#username_instagram').val().substr($('#username_instagram').val().lastIndexOf('/') + 1);
        $.get( "<?php echo e(route('influencer.instagram.check')); ?>", { username:  username}, function(data) {
            if(data.result == false){
                $('#instagram-profile').prepend(
                    '<div class="alert alert-danger">Invalid username Instagram</div>'
                );

                setTimeout(() => {
                    $('.alert-danger').remove();
                }, 5000);
                
            } else {
                $('#instagram-profile').empty();
                $('#instagram-profile').append(
                    '<div class="feed-element" id="element-instagram">'+
                        '<a href="#" class="pull-left">'+
                            '<img alt="image" class="img-circle" src="'+data.result.profilePicture+'" style="height:100px;width:100px;">'+
                        '</a>'+
                        '<div class="media-body">'+
                            '<h3>'+data.result.userName+'</h3> <br>'+
                            '<strong style="padding: 0px 10px;">'+data.result.mediaCount+' post</strong><strong style="padding: 0px 10px;">'+data.result.followers+' followers</strong><strong style="padding: 0px 10px;">'+ data.result.following +' following</small></strong>'+
                        '</div>'+
                    '</div>'
                );
                $('#username_instagram').parent().parent().find('.error').remove();
            }
        });

    });

    $('form').submit(function(e){
        
        if($('#username_instagram').val() != '' && $('#element-instagram').length == 0){
            $('#username_instagram').parent().parent().find('.error').remove();
            $('#username_instagram').parent().parent().prepend('<label class="error">Please Click Check Button!</label>');
            e.preventDefault();
        } else {
            $('form').submit();
        }
        
        return false;
    });
    <?php endif; ?>

    <?php if($user->detailInfluencer): ?>
    if($('#username_instagram').val() != ''){
        var username = $('#username_instagram').val().substr($('#username_instagram').val().lastIndexOf('/') + 1);
        $.get( "<?php echo e(route('influencer.instagram.check')); ?>", { username:  username}, function(data) {
            if(data.result == false){
                $('#instagram-profile').prepend(
                    '<div class="alert alert-danger">Invalid username Instagram</div>'
                );

                setTimeout(() => {
                    $('.alert-danger').remove();
                }, 5000);
                
            } else {
                $('#username_instagram').val(data.result.userName);
                $('#instagram-profile').empty();
                $('#instagram-profile').append(
                    '<div class="feed-element" id="element-instagram">'+
                        '<a href="#" class="pull-left">'+
                            '<img alt="image" class="img-circle" src="'+data.result.profilePicture+'" style="height:100px;width:100px;">'+
                        '</a>'+
                        '<div class="media-body">'+
                            '<h3>'+data.result.userName+'</h3> <br>'+
                            '<strong style="padding: 0px 10px;">'+data.result.mediaCount+' post</strong>  <strong style="padding: 0px 10px;">'+data.result.followers+' followers</strong>  <strong style="padding: 0px 10px;">'+ data.result.following +' following</small></strong>'+
                        '</div>'+
                    '</div>'
                );
            }
        });
    }
    <?php endif; ?>
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>