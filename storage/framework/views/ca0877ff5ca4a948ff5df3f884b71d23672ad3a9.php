<!-- Stored in resources/views/child.blade.php -->


<?php $__env->startSection('title', 'Page Title'); ?>

<?php $__env->startSection('content'); ?>

<!-- Page Content -->
<div class="container-fluid access">
    <div class="row">
        <div class="col-sm-12">
            <div class="title">
                <h2>
                    Join Here For Dashboard Access
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 box-one">
                <div class="card">
                    <div class="card-body">
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            We have successfully connected with your Youtube channel and collected following information:
                        </div>
                        <?php $links = explode("/", $authCreators->image);?>

                        <?php if(in_array($links[0], ['http:', 'https:'])): ?>
                            <img class="img-fluid" src="<?php echo e($authCreators->image); ?>" style="width: 150px; height: 150px; margin:0 auto; display:block; border-radius:5px;"/>
                        <?php else: ?>
                            <img class="img-fluid" src="<?php echo e(url('/'). Image::url($authCreators->image,150,150,array('crop'))); ?>" style="margin:0 auto; display:block; border-radius:5px;" />
                        <?php endif; ?>
                        <form>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">
                                    Channel Title
                                </label>
                                <div class="col-sm-8">
                                	<input type="text" readonly class="form-control-plaintext" id="#" value="<?php echo e($authCreators->name_channel); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">
                                    Email
                                </label>
                                <div class="col-sm-8">
                                	<input type="email" readonly class="form-control-plaintext" id="#" value="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">
                                    Channel View
                                </label>
                                <div class="col-sm-8">
                                	<input type="text" readonly class="form-control-plaintext" id="#" value="<?php echo e($channelView); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">
                                    Subscribers
                                </label>
                                <div class="col-sm-8">
                                	<input type="text" readonly class="form-control-plaintext" id="#" value="<?php echo e($totalSubscriber); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">
                                    Videos
                                </label>
                                <div class="col-sm-8">
                                	<input type="text" readonly class="form-control-plaintext" id="#" value="<?php echo e($totalVideo); ?>">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6 box-two">
                <div class="card">
                    <div class="card-body">
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            Please fill out the following form in order to complete your application to starhits.id
                        </div>
                        <?php if(count($errors)): ?>
							<div class="alert alert-danger">
								<strong>Whoops!</strong> There were some problems with your input.
								<br/>
								<ul>
									<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<li><?php echo e($error); ?></li>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</ul>
							</div>
						<?php endif; ?>
                        <form method="POST" action="<?php echo e(route('creators.validate.update',$authCreators->provider_id)); ?>" enctype="multipart/form-data">
                        	<?php echo e(csrf_field()); ?>

		                    <?php echo e(method_field('PUT')); ?>

		                    <?php echo e(method_field('PATCH')); ?>

		                    <input name="subscribers" type="hidden" value="<?php echo e($totalSubscriber); ?>">
		                    <input name="channelview" type="hidden" value="<?php echo e($channelView); ?>">
		                    <input name="totalvideo" type="hidden" value="<?php echo e($totalVideo); ?>">
                            <div class="form-group row<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                                <div class="col">
                                    <input class="form-control" name="name" value="<?php echo e(old('name')); ?>"  placeholder="Full Name" type="text" required>
                                    </input>
                                    <span class="text-danger"><?php echo e($errors->first('name')); ?></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 dempet">
                                    <select class="form-control" name="gender">
                                        <option selected="">
                                            Select Gender
                                        </option>
                                        <option value="male">
                                            Male
                                        </option>
                                        <option value="female">
                                            Female
                                        </option>
                                    </select>
                                </div>
                                <div class="col-sm-6<?php echo e($errors->has('gender') ? ' has-error' : ''); ?>">
                                    <input id="datepicker" name="dob" placeholder="Date Of Birth" required/>
                                    <span class="text-danger"><?php echo e($errors->first('dob')); ?></span>
                                </div>
                            </div>
                            <div class="form-group row<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                                <div class="col-sm-6 dempet">
                                    <input class="form-control" name="email" placeholder="Email Address" type="email" required></input>
                                    <span class="text-danger"><?php echo e($errors->first('email')); ?></span>
                                </div>
                                <div class="col-sm-6<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                                    <input class="form-control" name="password" placeholder="Password" type="password" required></input>
                                    <span class="text-danger"><?php echo e($errors->first('password')); ?></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col">
                                    <div class="custom-file">
                                        <input class="custom-file-input" type="file" name="cover">
                                            <label class="custom-file-label" for="customFile">
                                                Upload Cover
                                            </label>
                                        </input>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 box-three">
				                <div class="card">
				                    <div class="card-body">
				                        <div class="form-check">
				                            <input class="form-check-input" id="accept" type="checkbox">
				                                <label class="form-check-label">
				                                    I agree to the General Terms and Conditions and understand that i am electronically signing this agreement to join the Starhits.id
				                                </label>
				                            </input>
				                        </div>
				                    </div>
				                </div>
				            </div>
				            <div class="col-md-12 box-four">
				                <button class="btn btn-warning" id="form-submitbtn" disabled="disabled" type="submit">
				                    Apply Now
				                </button>
				            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>


<?php $__env->startPush('scripts'); ?>
<script type="text/javascript">
	$('#accept').click(function() {
		if ($('#form-submitbtn').is(':disabled')) {
	    	$('#form-submitbtn').removeAttr('disabled');
	    } else {
	    	$('#form-submitbtn').attr('disabled', 'disabled');
	    }
	});

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#inputgambar").change(function () {
        readURL(this);
    });
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>