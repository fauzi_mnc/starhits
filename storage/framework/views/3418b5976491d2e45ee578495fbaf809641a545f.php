<?php $__env->startPush('meta'); ?>    
        <meta property="og:url" content="<?php echo e(url()->current()); ?>" />
        <meta property="og:title" content="<?php echo e($metaTag->name); ?>" />
        <meta property="og:description" content="<?php echo e($metaTag->biodata); ?>" />
        <meta property="og:image" content="<?php echo e(url($metaTag->image)); ?>" />
        <meta property="fb:app_id" content="952765028218113" />
<?php $__env->stopPush(); ?>
<!-- Stored in resources/views/child.blade.php -->


<?php $__env->startSection('title', $metaTag->name); ?>

<?php $__env->startPush('preloader'); ?>
<div class="se-pre-con">
    <div class="col-sm-12 text-center pre-con">
        <div class="col">
            <div class="logo-preloader">
                <img alt="" class="img-fluid" src="<?php echo e(asset('frontend/assets/img/logo-home.png')); ?>">
                </img>
            </div>
        </div>
        <div class="col">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg">
                <defs>
                    <filter id="gooey">
                        <fegaussianblur in="SourceGraphic" result="blur" stddeviation="10">
                        </fegaussianblur>
                        <fecolormatrix in="blur" mode="matrix" result="goo" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7">
                        </fecolormatrix>
                        <feblend in="SourceGraphic" in2="goo">
                        </feblend>
                    </filter>
                </defs>
            </svg>
            <div class="blob blob-0"></div>
            <div class="blob blob-1"></div>
            <div class="blob blob-2"></div>
            <div class="blob blob-3"></div>
            <div class="blob blob-4"></div>
            <div class="blob blob-5"></div>
        </div>
    </div>
</div>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>
<div class="container-fluid single-creators">
    <div class="col-sm-12">
        <div class="item-single-creators">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-2 item-single-creators-image">
                        <a class="d-block" href="#">
                            <?php if(!empty($creators->image)): ?>
                               <?php $links = explode("/", $creators->image);?>

                                <?php if(in_array($links[0], ['http:', 'https:'])): ?>
                                    <img src="<?php echo e($creators->image); ?>" style="width: 150px; height: 150px;"/>
                                <?php else: ?>
                                    <img src="<?php echo e(url('/'). Image::url($creators->image,150,150,array('crop'))); ?>"/>
                                <?php endif; ?>
                            <?php else: ?>
                                <img src="<?php echo e(url('/'). Image::url('frontend/assets/img/icon/default.png',150,150,array('crop'))); ?>"/>
                            <?php endif; ?>
                        </a>
                    </div>
                    <div class="col-sm-5 item-single-creators-text">
                        <div class="item-single-creators-text-title">
                            <h4>
                                <?php echo e($creators->name); ?>

                            </h4>
                        </div>
                        <div class="item-single-creators-text-paragraph">
                            <p>
                                <?php echo e($creators->biodata); ?>

                            </p>
                        </div>
                        <div class="item-single-creators-text-videos">
                            <label>
                                <a class="subscriber" href="#">
                                    <?php if(!empty($creators->provider_id)): ?>
                                        <?php echo e(bd_nice_number($creators->subscribers)); ?> Subscriber
                                    <?php else: ?>
                                        0 Subscriber
                                    <?php endif; ?>
                                </a>
                                <a href="#">
                                    <?php echo e($creators->totalVideos); ?> Videos
                                </a>
                                |
                                <a href="#">
                                    <?php echo e($creators->totalSeries); ?> Series
                                </a>
                            </label>
                        </div>
                        <div class="item-single-creators-text-share">
                            <div class="entry-social">
                                <?php if(sizeof($creators->socials)>0): ?>
                                    <?php $__currentLoopData = $creators->socials; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $social): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="<?php echo e($social->name); ?>">
                                            <?php if(!empty($social->url)): ?>
                                                <a href="<?php echo e($social->url); ?>" target="_blank"></a>
                                            <?php endif; ?>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="latest-creators-videos">
            <div class="col-md-12">
                <div class="title">
                    <h7>
                        LATEST <?php echo e(strtoupper($creators->name)); ?> VIDEOS
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="<?php echo e(count($latestVideos)>=1? 'slider-latest-creators-videos':''); ?>">
                    <?php $__empty_1 = true; $__currentLoopData = $latestVideos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <div>
                        <div class="item">
                            <div class="imgTitle">
                                <div class="latest-creators-videos-overlay">
                                    <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                        <?php if(!empty($value->image)): ?>
                                            <img class="img-fluid latest-creators-videos-overlay" src="<?php echo e(url('/'). Image::url($value->image,400,225,array('crop'))); ?>"/>
                                        <?php else: ?>
                                            <img class="img-fluid latest-creators-videos-overlay" src="https://img.youtube.com/vi/<?php echo e($value->attr_6); ?>/mqdefault.jpg" style="width: 400; height: 250;"/>
                                        <?php endif; ?>
                                    </a>
                                    <?php if(!empty($value->attr_7)): ?>
                                        <div class="item-latest-creators-videos-overlay">
                                            <?php echo e($duration->formatted($value->attr_7)); ?>

                                        </div>
                                    <?php else: ?>

                                    <?php endif; ?>
                                </div>
                            </div>
                            <p>
                                <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                    <?php echo e($value->title); ?>

                                </a>
                            </p>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="popular-creators-videos">
            <div class="col-md-12">
                <div class="title">
                    <h7>
                        POPULAR <?php echo e(strtoupper($creators->name)); ?> VIDEOS
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="<?php echo e(count($popularVideos)>=1? 'slider-popular-creators-videos':''); ?>">
                    <?php $__empty_1 = true; $__currentLoopData = $popularVideos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <div>
                        <div class="item">
                            <div class="imgTitle">
                                <div class="popular-creators-videos-overlay">
                                    <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                        <?php if(!empty($value->image)): ?>
                                            <img class="img-fluid latest-creators-videos-overlay" src="<?php echo e(url('/'). Image::url($value->image,400,225,array('crop'))); ?>"/>
                                        <?php else: ?>
                                            <img class="img-fluid latest-creators-videos-overlay" src="https://img.youtube.com/vi/<?php echo e($value->attr_6); ?>/mqdefault.jpg" style="width: 400; height: 250;"/>
                                        <?php endif; ?>
                                    </a>
                                    <?php if(!empty($value->attr_7)): ?>
                                        <div class="item-popular-creators-videos-overlay">
                                            <?php echo e($duration->formatted($value->attr_7)); ?>

                                        </div>
                                    <?php else: ?>

                                    <?php endif; ?>
                                </div>
                            </div>
                            <p>
                                <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                    <?php echo e($value->title); ?>

                                </a>
                            </p>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="latest-creators-series">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        LATEST <?php echo e(strtoupper($creators->name)); ?> SERIES
                    </h7>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="<?php echo e(count($latestSeries)>=1? 'slider-latest-creators-series':''); ?>">
                    <?php $__empty_1 = true; $__currentLoopData = $latestSeries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <div>
                        <div class="item">
                            <article class="caption">
                                <img class="caption__media img-fluid" src="<?php echo e(url('/'). Image::url($value->image,400,550,array('crop'))); ?>"/>
                                <a href="<?php echo e(url('/series/'.$value->slug)); ?>">
                                    <div class="caption__overlay">
                                        <h1 class="caption__overlay__title">
                                            <?php echo e($value->title); ?>

                                        </h1>
                                        <h7 class="caption__overlay__content">
                                            <?php echo e($value->totalVideo); ?> Videos
                                        </h7>
                                        <?php if(!empty($value->excerpt)): ?>
                                        <p class="caption__overlay__content">
                                            <?php echo strip_tags($value->excerpt); ?>

                                        </p>
                                        <?php else: ?>
                                            <?php if(!empty($value->excerpt)): ?>
                                            <p class="caption__overlay__content">
                                                <?php echo strip_tags($value->excerpt); ?>

                                            </p>
                                            <?php else: ?>
                                            <p class="caption__overlay__content">
                                                <?php echo strip_tags(str_limit($value->content, 100)); ?>

                                            </p>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                </a>
                            </article>
                            <p>
                                <a href="<?php echo e(url('/series/'.$value->slug)); ?>">
                                    <?php echo e($value->title); ?>

                                </a>
                            </p>
                            <p class="videos">
                                <a href="#">
                                    <?php echo e($value->totalVideo); ?> Videos
                                </a>
                            </p>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="popular-creators-series">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        POPULAR <?php echo e(strtoupper($creators->name)); ?> SERIES
                    </h7>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="<?php echo e(count($popularSeries)>=1? 'slider-popular-creators-series':''); ?>">
                    <?php $__empty_1 = true; $__currentLoopData = $popularSeries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <div>
                        <div class="item">
                            <article class="caption">
                                <img class="caption__media img-fluid" src="<?php echo e(url('/'). Image::url($value->image,400,550,array('crop'))); ?>"/>
                                <a href="<?php echo e(url('/series/'.$value->slug)); ?>">
                                    <div class="caption__overlay">
                                        <h1 class="caption__overlay__title">
                                            <?php echo e($value->title); ?>

                                        </h1>
                                        <h7 class="caption__overlay__content">
                                            <?php echo e($value->totalVideo); ?> Videos
                                        </h7>
                                        <?php if(!empty($value->excerpt)): ?>
                                        <p class="caption__overlay__content">
                                            <?php echo strip_tags($value->excerpt); ?>

                                        </p>
                                        <?php else: ?>
                                        <p class="caption__overlay__content">
                                            <?php echo strip_tags(str_limit($value->content, 100)); ?>

                                        </p>
                                        <?php endif; ?>
                                    </div>
                                </a>
                            </article>
                            <p>
                                <a href="<?php echo e(url('/series/'.$value->slug)); ?>">
                                    <?php echo e($value->title); ?>

                                </a>
                            </p>
                            <p class="videos">
                                <a href="#">
                                    <?php echo e($value->totalVideo); ?> Videos
                                </a>
                            </p>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="recommended-creators-videos">
            <div class="col-md-12">
                <div class="title">
                    <h7>
                        RECOMMENDED <?php echo e(strtoupper($creators->name)); ?> VIDEOS
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="<?php echo e(count($recomendedCreators)>=1? 'slider-recommended-creators-videos':''); ?>">
                    <?php $__empty_1 = true; $__currentLoopData = $recomendedCreators; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <div>
                        <div class="item">
                            <div class="imgTitle">
                                <div class="recommended-creators-videos-overlay">
                                    <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                        <?php if(!empty($value->image)): ?>
                                            <img class="img-fluid top-videos-overlay" src="<?php echo e(url('/'). Image::url($value->image,400,225,array('crop'))); ?>"/>
                                        <?php else: ?>
                                            <img class="img-fluid top-videos-overlay" src="https://img.youtube.com/vi/<?php echo e($value->attr_6); ?>/mqdefault.jpg" style="width: 400; height: 250;"/>
                                        <?php endif; ?>
                                    </a>
                                    <?php if(!empty($value->attr_7)): ?>
                                        <div class="item-recommended-creators-videos-overlay">
                                            <?php echo e($duration->formatted($value->attr_7)); ?>

                                        </div>
                                    <?php else: ?>

                                    <?php endif; ?>
                                </div>
                            </div>
                            <p>
                                <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                    <?php echo e($value->title); ?>

                                </a>
                            </p>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid others-videos">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <div class="title">
                <h7>
                    OTHERS VIDEOS
                </h7>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="<?php echo e(count($otherVideos)>=1? 'row text-center text-lg-left item-others-videos':''); ?>">
                <?php $__empty_1 = true; $__currentLoopData = $otherVideos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <div class="col-sm-3 col-others-videos">
                    <div class="item">
                        <div class="imgTitle">
                            <div class="top-videos-overlay">
                                <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                    <?php if(!empty($value->image)): ?>
                                        <img class="img-fluid top-videos-overlay" src="<?php echo e(url('/'). Image::url($value->image,400,225,array('crop'))); ?>"/>
                                    <?php else: ?>
                                        <img class="img-fluid top-videos-overlay" src="https://img.youtube.com/vi/<?php echo e($value->attr_6); ?>/mqdefault.jpg" style="width: 400; height: 250;"/>
                                    <?php endif; ?>
                                </a>
                                <?php if(!empty($value->attr_7)): ?>
                                    <div class="item-top-videos-overlay">
                                        <?php echo e($duration->formatted($value->attr_7)); ?>

                                    </div>
                                <?php else: ?>

                                <?php endif; ?>
                            </div>
                        </div>
                        <p>
                            <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                <?php echo e($value->title); ?>

                            </a>
                        </p>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                            There is no data to display!
                        </div>
                    </br>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid more-from-creators">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <div class="title">
                <h7>
                    MORE FROM CREATOR
                </h7>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="<?php echo e(count($moreVideos)>=1? 'row text-center text-lg-left item-more-from-creators loadMore':''); ?>">
                <?php $__empty_1 = true; $__currentLoopData = $moreVideos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <div class="col-sm-3 col-more-from-creators">
                    <div class="item">
                        <div class="imgTitle">
                            <div class="top-videos-overlay">
                                <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                    <?php if(!empty($value->image)): ?>
                                        <img class="img-fluid top-videos-overlay" src="<?php echo e(url('/'). Image::url($value->image,400,225,array('crop'))); ?>"/>
                                    <?php else: ?>
                                        <img class="img-fluid top-videos-overlay" src="https://img.youtube.com/vi/<?php echo e($value->attr_6); ?>/mqdefault.jpg" style="width: 400; height: 250;"/>
                                    <?php endif; ?>
                                </a>
                                <div class="item-top-videos-overlay">
                                    <?php echo e($duration->formatted($value->attr_7)); ?>

                                </div>
                            </div>
                        </div>
                        <p>
                            <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                <?php echo e($value->title); ?>

                            </a>
                        </p>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                            There is no data to display!
                        </div>
                    </br>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>