<?php echo Form::open(['route' => ['creator.destroy', $id], 'method' => 'delete']); ?>

<div class='btn-group'>
    <a href="<?php echo e(route('influencer.edit', $id)); ?>" class='btn btn-default btn-xs' title="Add To Influencer">
        <i class="glyphicon glyphicon-star"></i>
    </a>
    <a href="<?php echo e(route('creator.edit', $id)); ?>" class='btn btn-default btn-xs' title="Edit Creator">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    <?php if($is_active == 1): ?>
    <?php echo Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'title' => 'Inactive Creator',
        'onclick' => "return confirm('Do you want to inactive this creator?')",
        'name' => 'action',
        'value' => 'inact'
    ]); ?>

    <?php else: ?>
    <?php echo Form::button('<i class="glyphicon glyphicon-ok"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-success btn-xs',
        'title' => 'Activated Creator',
        'onclick' => "return confirm('Do you want to activated this creator?')",
        'name' => 'action',
        'value' => 'act'
    ]); ?>

    <?php endif; ?>
</div>
<?php echo Form::close(); ?>