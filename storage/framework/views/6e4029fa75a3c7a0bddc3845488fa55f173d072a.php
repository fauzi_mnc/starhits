<?php $__env->startSection('breadcrumb'); ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Posts</h2>
        <ol class="breadcrumb">
            <li>
                <?php if(auth()->user()->roles->first()->name === 'admin'): ?>
                    <a href="<?php echo e(route('admin.posts.index')); ?>">Posts</a>
                <?php else: ?>
                    <a href="<?php echo e(route('creator.posts.index')); ?>">Posts</a>
                <?php endif; ?>
            </li>
            <li class="active">
                <strong>Create Manual</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentCrud'); ?>
<?php if($errors->any()): ?>
    <div class="alert alert-danger">
        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
<?php endif; ?>
<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Create Post Manual</h5>
        </div>
        <div class="ibox-content">
        <?php if(auth()->user()->roles->first()->name === 'admin'): ?>
            <?php echo Form::open(['route' => 'admin.posts.store', 'class' => 'form-horizontal', 'files' => true]); ?>

        <?php else: ?>
            <?php echo Form::open(['route' => 'creator.posts.store', 'class' => 'form-horizontal', 'files' => true]); ?>

        <?php endif; ?>

        <?php echo $__env->make('posts.fields', ['formType' => 'create',  'postType' => 'manual'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <?php echo Form::close(); ?>

        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>