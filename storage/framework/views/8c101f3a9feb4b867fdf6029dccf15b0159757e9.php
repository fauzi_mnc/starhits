<?php $__env->startSection('css'); ?>
    <?php echo $__env->make('layouts.datatables_css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<div class="table-responsive">
<?php echo $dataTable->table(['width' => '100%']); ?>

</div>
<?php $__env->startSection('scripts'); ?>
    <?php echo $__env->make('layouts.datatables_js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $dataTable->scripts(); ?>

<script>
	function changeRecommend(_id){
    		$.ajax({
    		<?php if(auth()->user()->roles->first()->name === 'admin'): ?>
	          url: "<?php echo e(route('admin.serie.updateFeatured')); ?>",
	        <?php else: ?>
	          url: "<?php echo e(route('creator.serie.updateFeatured')); ?>",
	        <?php endif; ?>
	          method: "POST",
	          data: {
	          	id: _id
	          },
	          headers: {
			        'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
			  },
	          success: function(response){
	            console.log(response);
	               toastr.success("Mengubah Feature Berhasil");
	          },
	          error: function(response){
	            console.log(response);
	          }
	        });
	    }
    $(document).on('click','.featured',function(e) {
  	var id = $(this).attr('data-id');
	    		changeRecommend(id);
});
</script>
<?php echo $__env->make('layouts.datatables_limit', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>