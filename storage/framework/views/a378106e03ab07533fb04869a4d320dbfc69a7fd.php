<!-- Navigation -->
<div class="header fixed-top">
    <nav class="navbar navbar-expand-lg navbar-dark" style="overflow: hidden;">
        <div class="container">
            <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                <img alt="" class="logo" src="<?php echo e(url($SiteConfig['configs']['website_logo_header'])); ?>"/>
            </a>
            <button aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarResponsive" data-toggle="collapse" type="button">
                <span class="navbar-toggler-icon">
                </span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo e(url('/')); ?>">
                            Home
                            <span class="sr-only">
                                (current)
                            </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo e(url('series')); ?>">
                            Series
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo e(url('creators')); ?>">
                            Creators
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo e(url('channels')); ?>">
                            Channels
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo e(url('join')); ?>">
                            Join Us
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo e(url('page/about-us')); ?>">
                            About Us
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo e(url('contact')); ?>">
                            Contact Us
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <div class="text-center">
                                
                                <div class="col-xs-12 main-ads text-center">
                                    <a href="#"><img class="center-ads" src="<?php echo e(asset('frontend/assets/img/ads-330x43-web-series.jpg')); ?>" alt=""></a>
                                </div>
                        </div>
                    </li>
                </ul>
                <ul class="navbar-nav pull-rights">
                    <li class="nav-item">
                        <div class="separated">
                        </div>
                        <?php $__currentLoopData = $SiteConfig['adsInsideHeaders']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(empty($value->target)): ?>
                            <a class="nav-link dissapear" href="<?php echo e($value->youtube_id); ?>" target="_blank">
                                <img alt="" class="ads d-none d-sm-block d-sm-none d-md-block .d-md-none .d-lg-block .d-lg-none .d-xl-block" src="<?php echo e(url('/'). Image::url($value->image,935,123,array('crop'))); ?>"/>
                            </a>
                            <?php else: ?>
                            <a class="nav-link dissapear" href="<?php echo e($value->youtube_id); ?>">
                                <img alt="" class="ads d-none d-sm-block d-sm-none d-md-block .d-md-none .d-lg-block .d-lg-none .d-xl-block" src="<?php echo e(url('/'). Image::url($value->image,935,123,array('crop'))); ?>"/>
                            </a>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <span class="fa fa-search fa-lg" data-target=".bs-example-modal-lg" data-toggle="modal">
                                <div class="mobile">
                                    Search
                                </div>
                            </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <?php if(auth()->guard()->guest()): ?>
                        <a class="nav-link" href="#">
                            <span class="fa fa-user fa-lg" data-target="#loginModal" data-toggle="modal">
                            </span>
                        </a>
                        <?php else: ?>
                        <div class="dropdown show">
                            <a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="dropdownMenuLink">
                                <span class="fa fa-user fa-lg">
                                </span>
                            </a>
                            <div aria-labelledby="dropdownMenuLink" class="dropdown-menu">
                                <?php if (\Entrust::hasRole('admin')) : ?>
                                <a class="dropdown-item" href="<?php echo e(url('admin/dashboard')); ?>" target="_blank">
                                    Dashboard Admin
                                </a>
                                <?php endif; // Entrust::hasRole ?>
                                <?php if (\Entrust::hasRole('creator')) : ?>
                                <a class="dropdown-item" href="<?php echo e(url('creator/serie')); ?>" target="_blank">
                                    My Account
                                </a>
                                <?php endif; // Entrust::hasRole ?>
                                <?php if (\Entrust::hasRole('influencer')) : ?>
                                <a class="dropdown-item" href="<?php echo e(url('influencer/campaign')); ?>" target="_blank">
                                    My Account
                                </a>
                                <?php endif; // Entrust::hasRole ?>
                                <?php if (\Entrust::hasRole('member')) : ?>
                                <a class="dropdown-item" href="<?php echo e(route('self.member.edit', auth()->user()->id)); ?>" target="_blank">
                                    My Account
                                </a>
                                <?php endif; // Entrust::hasRole ?>
                                <a class="dropdown-item" href="<?php echo e(url('/logout')); ?>" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                    Log Out
                                </a>
                                <form action="<?php echo e(url('/logout')); ?>" id="logout-form" method="POST" style="display: none;">
                                    <?php echo e(csrf_field()); ?>

                                    <input style="display: none;" type="submit" value="logout">
                                    </input>
                                </form>
                            </div>
                        </div>
                        <?php endif; ?>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>