<?php $__env->startPush('meta'); ?>    
        <meta property="og:url" content="<?php echo e(url()->current()); ?>" />
        <meta property="og:title" content="<?php echo e($metaTag->title); ?>" />
        <meta property="og:description" content="<?php echo e($metaTag->content); ?>" />
        <meta property="og:image" content="<?php echo e(url($metaTag->image)); ?>" />
        <meta property="fb:app_id" content="952765028218113" />
<?php $__env->stopPush(); ?>
<!-- Stored in resources/views/child.blade.php -->


<?php $__env->startSection('title', $metaTag->title); ?>

<?php $__env->startPush('preloader'); ?>
<div class="se-pre-con">
    <div class="col-sm-12 text-center pre-con">
        <div class="col">
            <div class="logo-preloader">
                <img alt="" class="img-fluid" src="<?php echo e(asset('frontend/assets/img/logo-home.png')); ?>">
                </img>
            </div>
        </div>
        <div class="col">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg">
                <defs>
                    <filter id="gooey">
                        <fegaussianblur in="SourceGraphic" result="blur" stddeviation="10">
                        </fegaussianblur>
                        <fecolormatrix in="blur" mode="matrix" result="goo" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7">
                        </fecolormatrix>
                        <feblend in="SourceGraphic" in2="goo">
                        </feblend>
                    </filter>
                </defs>
            </svg>
            <div class="blob blob-0"></div>
            <div class="blob blob-1"></div>
            <div class="blob blob-2"></div>
            <div class="blob blob-3"></div>
            <div class="blob blob-4"></div>
            <div class="blob blob-5"></div>
        </div>
    </div>
</div>
<?php $__env->stopPush(); ?>

<?php $__env->startSection('content'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="top-content-single">
                <div class="top-slider">
                    <div class="item">
                        <div class="top-block">
                            <div class="top-media">
                                <div class="video-wrap">
                                    <div class="video">
                                        <div class="alert-close pull-right"><img src="<?php echo e(asset('frontend/assets/img/icon/close.png')); ?>" alt=""></div>
                                        <div class="youtube embed-responsive embed-responsive-16by9">
                                            <?php echo $video->link; ?>

                                            <div class="thumb">
                                                <a class="target-big" href="<?php echo e(url('/videos/'.$video->slug)); ?>">
                                                    <?php echo e($video->title); ?>

                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="articles">
                    <div class="container-fluid">
                        <div class="row">
                            <?php if(session('alert')): ?>
                                <div class="alert alert-success">
                                    <?php echo e(session('alert')); ?>

                                </div>
                            <?php endif; ?>
                            <div class="col-md-12 main-article">
                                <div class="main-article">
                                    <div class="entry-social">
                                        <?php if(auth()->guard()->guest()): ?>
                                        <div class="a2a_kit bm">
                                            <!-- <a href="#" target="_blank">
                                                <div class="mobile-text">
                                                    Bookmark
                                                </div>
                                            </a> -->
                                        </div>
                                        <?php else: ?>
                                        <div class="a2a_kit bm">
                                            <a href="<?php echo e(route('series.bookmark', ['id' => $video->id])); ?>" class="<?php echo e($bookmarkClass); ?>">
                                                <div class="mobile-text">
                                                    Bookmark
                                                </div>
                                            </a>
                                        </div>
                                        <?php endif; ?>
                                        <div class="a2a_kit wa">
                                            <a class="a2a_button_whatsapp link-social what-color" data-action="share/whatsapp/share" href="whatsapp://send?text=Hello World!" target="_blank">
                                                <div class="mobile-text">
                                                    Share
                                                </div>
                                            </a>
                                        </div>
                                        <div class="a2a_kit fb">
                                            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo e(url('/videos/'.$video->slug)); ?>" target="_blank">
                                                <div class="mobile-text">
                                                    Share
                                                </div>
                                            </a>
                                        </div>
                                        <div class="a2a_kit twitter">
                                            <a href="https://twitter.com/intent/tweet?url=<?php echo e(url('/videos/'.$video->slug)); ?>" target="_blank">
                                                <div class="mobile-text">
                                                    Tweet
                                                </div>
                                            </a>
                                        </div>
                                        <div class="a2a_kit gplus">
                                            <a href="https://plus.google.com/share?url=<?php echo e(url('/videos/'.$video->slug)); ?>" target="_blank">
                                                <div class="mobile-text">
                                                    Share
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="title">
                                        <h2>
                                            <?php echo e($video->title); ?>

                                        </h2>
                                    </div>
                                    <div class="sub-title">
                                        <div class="row">
                                            <div class="col-md-12 main-sub-title">
                                                <h4>
                                                    <?php echo e($series->channels->title); ?>

                                                </h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="sub-title">
                                        <div class="row">
                                            <div class="col-md-12 views-2">
                                                <section>
                                                    By
                                                    <strong>
                                                        <a href="<?php echo e(url('/creators/'.$series->users->slug)); ?>">
                                                            <?php echo e($series->users->name); ?>

                                                        </a>
                                                    </strong>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="paragraph">
                                        <p class="main-paragraph">
                                            <?php echo $series->content; ?>

                                        </p>
                                    </div>
                                    <div class="col-sm-12 main-ads-articles text-center">
                                        <?php $__currentLoopData = $SiteConfig['adsUnderContent']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if(empty($value->target)): ?>
                                            <a href="<?php echo e($value->youtube_id); ?>" target="_blank">
                                                <img alt="" class="center-ads" src="<?php echo e(url('/'). Image::url($value->image,935,123,array('crop'))); ?>"/>
                                            </a>
                                            <?php else: ?>
                                            <a href="<?php echo e($value->youtube_id); ?>">
                                                <img alt="" class="center-ads" src="<?php echo e(url('/'). Image::url($value->image,935,123,array('crop'))); ?>"/>
                                            </a>
                                            <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                    <br>
                                        <div id="disqus_thread">
                                        </div>
                                    </br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="listing-block">
                <div class="title">
                    <div class="row">
                        <div class="col">
                            <h6>
                                <?php echo e($series->title); ?>

                            </h6>
                            <!-- Auto Next --> 
                            <div class="col play">
                                <p>
                                    <span class="auto">
                                        AUTO NEXT
                                    </span>
                                    <label class="switch">
                                        <input id="autonext" type="checkbox">
                                            <span class="slider round"></span>
                                    </label>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="all-media">sss
                    <div class="all-media-child">
                        <?php $__currentLoopData = $series->videos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="media">
                            <?php if(!empty($value->attr_7)): ?>
                            <div class="item-top-overlay">
                                <?php echo e($duration->formatted($value->attr_7)); ?>

                            </div>
                            <?php else: ?>

                            <?php endif; ?>
                            <a href="<?php echo e(url('/series/'.$slug.'/'.$value->slug)); ?>">
                                <?php if(!empty($value->image)): ?>
                                    <img class="d-flex align-self-start img-fluid top-overlay" src="<?php echo e(url('/'). Image::url($value->image,513,325,array('crop'))); ?>"/>
                                <?php else: ?>
                                    <img class="d-flex align-self-start img-fluid top-overlay" src="https://img.youtube.com/vi/<?php echo e($value->attr_6); ?>/hqdefault.jpg" style="width: 513; height: 350;"/>
                                <?php endif; ?>
                            </a>
                            <div class="media-body pl-3">
                                <div class="media-title-series">
                                    <a href="#">
                                        <?php echo e($series->title); ?>

                                    </a>
                                </div>
                                <div class="media-title">
                                    <a href="<?php echo e(url('/series/'.$slug.'/'.$value->slug)); ?>">
                                        <?php echo e($value->title); ?>

                                    </a>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
            <div class="sidebar my-auto">
                <div class="main-ads-mobile text-center">
                    <div class="sidebar-ads-1">
                        <?php $__currentLoopData = $SiteConfig['adsRightContentI']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(empty($value->target)): ?>
                            <a href="<?php echo e($value->youtube_id); ?>" target="_blank">
                                <img alt="" src="<?php echo e(url('/'). Image::url($value->image,144,542,array('crop'))); ?>"/>
                            </a>
                            <?php else: ?>
                            <a href="<?php echo e($value->youtube_id); ?>">
                                <img alt="" src="<?php echo e(url('/'). Image::url($value->image,144,542,array('crop'))); ?>"/>
                            </a>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
                <div class="main-ads text-center">
                    <div class="sidebar-ads-1">
                        <?php $__currentLoopData = $SiteConfig['adsRightContentI']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(empty($value->target)): ?>
                            <a href="<?php echo e($value->youtube_id); ?>" target="_blank">
                                <img alt="" src="<?php echo e(url('/'). Image::url($value->image,144,542,array('crop'))); ?>"/>
                            </a>
                            <?php else: ?>
                            <a href="<?php echo e($value->youtube_id); ?>">
                                <img alt="" src="<?php echo e(url('/'). Image::url($value->image,144,542,array('crop'))); ?>"/>
                            </a>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
                <div class="main-ads text-center">
                    <div class="sidebar-ads-2">
                        <?php $__currentLoopData = $SiteConfig['adsRightContentII']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php if(empty($value->target)): ?>
                            <a href="<?php echo e($value->youtube_id); ?>" target="_blank">
                                <img alt="" src="<?php echo e(url('/'). Image::url($value->image,144,542,array('crop'))); ?>"/>
                            </a>
                            <?php else: ?>
                            <a href="<?php echo e($value->youtube_id); ?>">
                                <img alt="" src="<?php echo e(url('/'). Image::url($value->image,144,542,array('crop'))); ?>"/>
                            </a>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid top-videos">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        MORE FROM CREATORS
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="<?php echo e(count($moreVideos)>=1? 'slider-top-videos':''); ?>">
                    <?php $__empty_1 = true; $__currentLoopData = $moreVideos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <div>
                        <div class="item">
                            <div class="imgTitle">
                                <div class="top-videos-overlay">
                                    <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                        <?php if(!empty($value->image)): ?>
                                            <img class="img-fluid top-videos-overlay" src="<?php echo e(url('/'). Image::url($value->image,400,225,array('crop'))); ?>"/>
                                        <?php else: ?>
                                            <img class="img-fluid top-videos-overlay" src="https://img.youtube.com/vi/<?php echo e($value->attr_6); ?>/mqdefault.jpg" style="width: 400; height: 250;"/>
                                        <?php endif; ?>
                                    </a>
                                    <?php if(!empty($value->attr_7)): ?>
                                    <div class="item-top-videos-overlay">
                                        <?php echo e($duration->formatted($value->attr_7)); ?>

                                    </div>
                                    <?php else: ?>

                                    <?php endif; ?>
                                </div>
                            </div>
                            <p>
                                <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                    <?php echo e($value->title); ?>

                                </a>
                            </p>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid top-videos">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        RELATED VIDEOS
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="<?php echo e(count($relatedVideos)>=1? 'slider-top-videos':''); ?>">
                    <?php $__empty_1 = true; $__currentLoopData = $relatedVideos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <div>
                        <div class="item">
                            <div class="imgTitle">
                                <div class="top-videos-overlay">
                                    <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                        <?php if(!empty($value->image)): ?>
                                            <img class="img-fluid top-videos-overlay" src="<?php echo e(url('/'). Image::url($value->image,886,445,array('crop'))); ?>"/>
                                        <?php else: ?>
                                            <img class="img-fluid top-videos-overlay" src="https://img.youtube.com/vi/<?php echo e($value->attr_6); ?>/mqdefault.jpg" style="width: 886; height: 490;"/>
                                        <?php endif; ?>
                                    </a>
                                    <?php if(!empty($value->attr_7)): ?>
                                    <div class="item-top-videos-overlay">
                                        <?php echo e($duration->formatted($value->attr_7)); ?>

                                    </div>
                                    <?php else: ?>

                                    <?php endif; ?>
                                </div>
                            </div>
                            <p>
                                <a href="<?php echo e(url('/videos/'.$value->slug)); ?>">
                                    <?php echo e($value->title); ?>

                                </a>
                            </p>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid top-pro">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        RECOMMENDED SERIES
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="<?php echo e(count($recomendedSeries)>=1? 'slider-top-pro':''); ?>">
                    <?php $__empty_1 = true; $__currentLoopData = $recomendedSeries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <div>
                        <div class="item">
                            <article class="caption">
                                <img class="caption__media img-fluid" src="<?php echo e(url('/'). Image::url($value->image,400,550,array('crop'))); ?>"/>
                                <a href="<?php echo e(url('/series/'.$value->slug)); ?>">
                                    <div class="caption__overlay">
                                        <h1 class="caption__overlay__title">
                                            <?php echo e($value->title); ?>

                                        </h1>
                                        <h7 class="caption__overlay__content">
                                            <?php echo e($value->totalVideo); ?> Videos
                                        </h7>
                                        <?php if(!empty($value->excerpt)): ?>
                                        <p class="caption__overlay__content">
                                            <?php echo strip_tags($value->excerpt); ?>

                                        </p>
                                        <?php else: ?>
                                        <p class="caption__overlay__content">
                                            <?php echo strip_tags(str_limit($value->content, 100)); ?>

                                        </p>
                                        <?php endif; ?>
                                    </div>
                                </a>
                            </article>
                            <p>
                                <a href="<?php echo e(url('/series/'.$value->slug)); ?>">
                                    <?php echo e($value->title); ?>

                                </a>
                            </p>
                            <p class="videos">
                                <a href="#">
                                    <?php echo e($value->totalVideo); ?> Videos
                                </a>
                            </p>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>