<?php if(auth()->user()->roles->first()->name === 'admin'): ?>
	<?php if($post_type == 'manual'): ?>
	<div class='btn-group'>
	    <a href="<?php echo e(route('admin.posts.editManual', $id)); ?>" class='btn btn-default btn-xs' title="Edit Post">
	        <i class="glyphicon glyphicon-edit"></i>
	    </a>
	</div>
	<?php else: ?>
	<div class='btn-group'>
	    <a href="<?php echo e(route('admin.posts.editAutomatic', $id)); ?>" class='btn btn-default btn-xs' title="Edit Post">
	        <i class="glyphicon glyphicon-edit"></i>
	    </a>
	</div>
	<?php endif; ?>
<?php else: ?>
	<?php if($post_type == 'manual'): ?>
	<div class='btn-group'>
	    <a href="<?php echo e(route('creator.posts.editManual', $id)); ?>" class='btn btn-default btn-xs' title="Edit Post">
	        <i class="glyphicon glyphicon-edit"></i>
	    </a>
	</div>
	<?php else: ?>
	<div class='btn-group'>
	    <a href="<?php echo e(route('creator.posts.editAutomatic', $id)); ?>" class='btn btn-default btn-xs' title="Edit Post">
	        <i class="glyphicon glyphicon-edit"></i>
	    </a>
	</div>
	<?php endif; ?>
<?php endif; ?>