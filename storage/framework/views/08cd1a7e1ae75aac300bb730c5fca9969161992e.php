<?php $__env->startPush('meta'); ?>    
        <meta property="og:url" content="<?php echo e(url()->current()); ?>" />
        <meta property="og:title" content="<?php echo e($metaTag->title); ?>" />
        <meta property="og:description" content="<?php echo e($metaTag->content); ?>" />
        <meta property="fb:app_id" content="952765028218113" />
<?php $__env->stopPush(); ?>

<!-- Stored in resources/views/child.blade.php -->


<?php $__env->startSection('title', 'Page Title'); ?>

<?php $__env->startSection('content'); ?>

<div class="container-fluid blank-page">
    <div class="col-sm-12">
        <div class="title">
            <h2>
                <?php echo e($pagesDynamic->title); ?>

            </h2>
        </div>
        <hr>
            <div class="col-md-12" style="padding: 0;">                  
                <div class="text">
                    <p>
                        <b>
                            <font size="5">
                                <?php echo e($pagesDynamic->subtitle); ?>

                            </font>
                        </b>
                    </p>
                    <p>
                        <?php echo $pagesDynamic->content; ?>

                    </p>
                </div>
            </div>
        </hr>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>