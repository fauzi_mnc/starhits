<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('css/fileinput.min.css')); ?>" rel="stylesheet">
<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
.wizard > .content > .body{
    position: relative !important;
}
strong{
    padding-right: 30px;
}
</style>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb'); ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Creator</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Creator</a>
            </li>
            <li class="active">
                <strong>Create</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('contentCrud'); ?>
<?php if($errors->any()): ?>
    <div class="alert alert-danger">
        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
<?php endif; ?>
<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Create Creator</h5>
                </div>
                <div class="ibox-content">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            Personal Information
                        </div>
                        <div class="panel-body">
                        <?php echo Form::open(['route' => 'creator.store', 'class' => 'form-horizontal', 'files' => true]); ?>

                        
                        <!-- Name Field -->
                        <div class="form-group">
                            <?php echo Form::label('name', 'Creator:', ['class' => 'col-sm-2 control-label']); ?>


                            <div class="col-sm-10">
                                <?php echo Form::text('name', null, ['class' => 'form-control', 'maxlength' => 60, 'required' => true]); ?>

                            </div>
                        </div>

                        <!-- Name Field -->
                        <div class="form-group">
                            <?php echo Form::label('email', 'Email:', ['class' => 'col-sm-2 control-label']); ?>


                            <div class="col-sm-10">
                                <?php echo Form::text('email', null, ['class' => 'form-control', 'maxlength' => 60, 'required' => true]); ?>

                            </div>
                        </div>

                        <!-- Picture Field -->
                        <div class="form-group">
                            <?php echo Form::label('image', 'Picture:', ['class' => 'col-sm-2 control-label']); ?>


                            <div class="col-sm-10">
                                <div class="kv-photo center-block text-center">
                                    <input id="photo" name="image" type="file" class="file-loading">
                                </div>

                                <div id="kv-photo-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
                            </div>
                        </div>

                        <!-- Channel Field -->
                        <div class="form-group">
                            <?php echo Form::label('channel', 'Channel Youtube:', ['class' => 'col-sm-2 control-label']); ?>


                            <div class="col-sm-10">
                                <?php echo Form::text('provider_id', null, ['class' => 'form-control', 'id' => 'channel']); ?>

                            </div>
                        </div>

                        <div class="form-group">
                            <?php echo Form::label('', '', ['class' => 'col-sm-2 control-label']); ?>

                            <div class="col-sm-10" id="youtube-profile">
                                
                                <button type="button" class="btn btn-default pull-right" id="btn-check">Check</button>
                            </div>
                        </div>

                        <input id="image" name="image" type="hidden">
                        <input id="subscribers" name="subscribers" type="hidden">

                        <!-- Biodata Field -->
                        <div class="form-group">
                            <?php echo Form::label('bio', 'Biodata:', ['class' => 'col-sm-2 control-label']); ?>


                            <div class="col-sm-10">
                                <?php echo Form::textarea('biodata', null, ['class' => 'form-control']); ?>

                            </div>
                        </div>

                        <!-- Password Field -->
                        <div class="form-group">
                            <?php echo Form::label('password', 'Password:', ['class' => 'col-sm-2 control-label']); ?>


                            <div class="col-sm-10">
                                <?php echo Form::text('password', null, ['class' => 'form-control', 'required' => true]); ?>

                            </div>
                        </div>

                        <!-- Cover Field -->
                        <div class="form-group">
                            <?php echo Form::label('cover', 'Cover:', ['class' => 'col-sm-2 control-label']); ?>


                            <div class="col-sm-10">
                                <div class="kv-photo center-block text-center">
                                    <input id="cover" name="cover" type="file" class="file-loading">
                                </div>

                                <div id="kv-cover-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
                            </div>
                        </div>



                        <!-- Percentage Field -->
                        <div class="form-group">
                            <?php echo Form::label('percentage', 'Percentage:', ['class' => 'col-sm-2 control-label']); ?>


                            <div class="col-sm-10">
                                <?php echo Form::text('percentage', null, ['class' => 'form-control', 'maxlength' => 3, 'required' => true]); ?>

                            </div>
                        </div>

                        </div>

                    </div>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            Social Media
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group m-b"><span class="input-group-addon"><i class="fa fa-instagram"></i></span>
                                        <input type="text" name="socials[]" placeholder="https://www.instagram.com/username" class="form-control" maxlength="100">
                                    </div>
                                    <div class="input-group m-b"><span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                        <input type="text" name="socials[]" placeholder="https://www.facebook.com/username" class="form-control" maxlength="100">
                                    </div>
                                    <div class="input-group m-b"><span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                        <input type="text" name="socials[]" placeholder="https://twitter.com/username" class="form-control" maxlength="100">
                                    </div>
                                    <div class="input-group m-b"><span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                                        <input type="text" name="socials[]" placeholder="https://plus.google.com/u/0/username" class="form-control" maxlength="100">
                                    </div>
                                </div>
                            </div>
                        </div>    
                    </div>
                    
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            Connect To Instagram
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <?php echo Form::label('username_instagram', 'Username Instagram:', ['class' => 'col-sm-2 control-label']); ?>


                                <div class="col-sm-10">
                                    <div class="input-group">
                                        <?php echo Form::text('username_instagram', null, ['class' => 'form-control required']); ?>

                                        <span class="input-group-btn"> 
                                            <button type="button" class="btn btn-primary pull-right" id="btn-check-instagram">Check</button>
                                        </span>
                                    </div>
                                    
                                    <div id="instagram-profile">
                                    </div>
                                </div>
                            </div>
                        </div>    
                    </div>
                    
                     <!-- Submit Field -->
                     <div class="pull-right">
                        <?php echo Form::submit('Save', ['class' => 'btn btn-primary', 'disabled' => true, 'id'=>'btn-submit']); ?>

                        <a href="<?php echo e(route('creator.index')); ?>" class="btn btn-default">Cancel</a>
                    </div>        
                    <?php echo Form::close(); ?>

                </div>
            </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/piexif.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/sortable.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/purify.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/fileinput.min.js')); ?>"></script>
<script>

    $("#cover").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-cover-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="<?php echo e(asset('img/default_avatar_male.jpg')); ?>" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "jpeg"]
    });

    $("#photo").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="<?php echo e(asset('img/default_avatar_male.jpg')); ?>" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "jpeg"]
    });
    $('#btn-check').click(function(){
        $.get( "<?php echo e(route('creator.check')); ?>", { url:  $('#channel').val()}, function(data) {
            if(data.result == false){
                $('#youtube-profile').prepend(
                    '<div class="alert alert-danger">Invalid Youtube Channel</div>'
                );
                $('#channel').val('');
                setTimeout(() => {
                    $('.alert-danger').remove();
                }, 5000);
                
            } else {
                $('#channel').val(data.result.id);
                $('#youtube-profile').empty();
                $('#youtube-profile').append(
                    '<div class="feed-element">'+
                        '<a href="#" class="pull-left">'+
                            '<img alt="image" class="img-circle" src="'+data.result.snippet.thumbnails.medium.url+'" style="height:100px;width:100px;">'+
                        '</a>'+
                        '<div class="media-body">'+
                            '<strong>'+data.result.snippet.title+'</strong> <br>'+
                            '<small class="text-muted">'+data.result.statistics.subscriberCount+' subscribers</small>'+
                        '</div>'+
                    '</div>'
                );
                $('#subscribers').val(data.result.statistics.subscriberCount);
                $('#image').val(data.result.snippet.thumbnails.medium.url);
            }
        });

        $('#btn-submit').attr('disabled', false);
    });

    $('#btn-submit').click(function(e){
        
        if($('#username_instagram').val() != '' && $('#element-instagram').length == 0){
            $('#username_instagram').parent().parent().find('.error').remove();
            $('#username_instagram').parent().parent().append('<label class="error">Please Click Check Button!</label>');

        } else {
            $('form').submit();
        }
        
        return false;
    });

    $('#btn-check-instagram').click(function(){
        $.get( "<?php echo e(route('influencer.instagram.check')); ?>", { username:  $('#username_instagram').val()}, function(data) {
            if(data.result == false){
                $('#instagram-profile').prepend(
                    '<div class="alert alert-danger">Invalid username Instagram</div>'
                );

                setTimeout(() => {
                    $('.alert-danger').remove();
                }, 5000);
                
            } else {
                $('#username_instagram').val(data.result.userName);
                $('#instagram-profile').empty();
                $('#instagram-profile').append(
                    '<div class="feed-element" id="element-instagram">'+
                        '<a href="#" class="pull-left">'+
                            '<img alt="image" class="img-circle" src="'+data.result.profilePicture+'" style="height:100px;width:100px;">'+
                        '</a>'+
                        '<div class="media-body">'+
                            '<h3>'+data.result.userName+'</h3> <br>'+
                            '<strong style="padding: 0px 10px;">'+data.result.mediaCount+' post</strong><strong style="padding: 0px 10px;">'+data.result.followers+' followers</strong><strong style="padding: 0px 10px;">'+ data.result.following +' following</small></strong>'+
                        '</div>'+
                    '</div>'
                );
                $('#username_instagram').parent().parent().find('.error').remove();
            }
        });

    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>