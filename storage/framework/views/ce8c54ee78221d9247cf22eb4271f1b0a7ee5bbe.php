<div aria-hidden="true" aria-labelledby="resetModalLabel" class="modal fade" id="resModal" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="col text-center">
                    <div class="avatar">
                        <img alt="StarHits" src="<?php echo e(asset('frontend/assets/img/black_logo.png')); ?>">
                    </div>
                </div>
                <div class="inline-block" style="display:absolute;">
                    <button type="button" class="close float-right" aria-label="Close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <div class="w-75 mx-auto">
                    <form action="<?php echo e(route('password.email')); ?>" method="POST">
                        <?php echo e(csrf_field()); ?>

                        <div class="form-group">
                            <input class="form-control" id="email" name="email" placeholder="Email" required="" style="font-family:Roboto Condensed;" type="email" value="<?php echo e(old('email')); ?>">
                            </input>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary btn-lg btn-block login-btn" type="submit">
                                Reset Password
                            </button>
                        </div>
                        <div class="or-seperator">
                        </div>
                        <p class="text-center small">
                            Already a Star Hits User?
                            <a aria-label="Close" data-dismiss="modal" data-target="#loginModal" data-toggle="modal" href="#">
                                Login now!
                            </a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>