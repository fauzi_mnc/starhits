
<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('css/fileinput.min.css')); ?>" rel="stylesheet">

<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
<?php echo $__env->make('layouts.datatables_css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php if(auth()->user()->roles->first()->name === 'brand'): ?>
<!-- Title Field -->
<div class="form-group">
    <?php echo Form::label('brand-name', 'Brand Name:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::text('brand_name', null, ['class' => 'form-control', 'disabled' => true]); ?>

    </div>
</div>

<!-- PIC Field -->
<div class="form-group">
    <?php echo Form::label('pic-name', 'PIC:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::text('name', null, ['class' => 'form-control', 'disabled' => true]); ?>

    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    <?php echo Form::label('email', 'Email Address:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::email('email', null, ['class' => 'form-control', 'disabled' => true, 'readonly' => ($formType == 'edit') ? true : false]); ?>

    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    <?php echo Form::label('phone', 'Phone Number:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::text('phone', null, ['class' => 'form-control', 'disabled' => true]); ?>

    </div>
</div>
<?php if($formType == 'edit'): ?>
<!-- Password Field -->
<div class="form-group">
    <?php echo Form::label('password', 'Password:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-8">
        <?php echo Form::text('password', null, ['class' => 'form-control', 'disabled' => true]); ?>

    </div>
    
    <div class="col-sm-2">
        <button type="button" id="reset-pwd" class="btn btn-default">Reset</button>
    </div>
    
</div>
<?php else: ?>
<!-- Title Field -->
<div class="form-group">
    <?php echo Form::label('password', 'Password:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::text('password', null, ['class' => 'form-control', 'disabled' => true]); ?>

    </div>
</div>
<?php endif; ?>
<!-- Title Field -->
<div class="form-group">
    <?php echo Form::label('saldo', 'Saldo:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <div class="input-group m-b">
            <span class="input-group-addon">Rp</span>
            <?php echo Form::text('saldo', null, ['class' => 'form-control', 'readonly' => true]); ?>

        </div>
        
    </div>
</div>
<?php else: ?>
<!-- Picture Field -->
<div class="form-group">
    <?php echo Form::label('image', 'Photo:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <div class="kv-photo center-block text-center">
            <input id="photo" name="image" type="file" class="file-loading" disabled>
        </div>

        <div id="kv-photo-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
    </div>
</div>
<!-- Picture Field -->
<div class="form-group">
    <?php echo Form::label('cover', 'Background Image:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <div class="kv-photo center-block text-center">
            <input id="cover" name="cover" type="file" class="file-loading" disabled>
        </div>

        <div id="kv-photo-errors-2" class="center-block alert alert-block alert-danger" style="display:none"></div>
    </div>
</div>
<!-- Title Field -->
<div class="form-group">
    <?php echo Form::label('full-name', 'Full Name:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::text('name', null, ['class' => 'form-control', 'disabled' => true]); ?>

    </div>
</div>

<!-- Status Field -->
<div class="form-group">
    <?php echo Form::label('gender', 'Gender:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::select('gender', ['M' => 'Male', 'F' => 'Female'], null, ['class' => 'form-control', 'disabled' => true]); ?>

    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    <?php echo Form::label('email', 'Email Address:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::text('email', null, ['class' => 'form-control', 'disabled' => true]); ?>

    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    <?php echo Form::label('dob', 'Date of Birth:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::text('dob', isset($user->dob) ? $user->dob->format('d-m-Y') : '', ['class' => 'form-control dateinput', 'disabled' => true]); ?>

    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    <?php echo Form::label('phone', 'Phone Number:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::text('phone', null, ['class' => 'form-control', 'disabled' => true]); ?>

    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    <?php echo Form::label('country', 'Country:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::text('country', null, ['class' => 'form-control', 'disabled' => true]); ?>

    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    <?php echo Form::label('city', 'City:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::text('city', null, ['class' => 'form-control', 'disabled' => true]); ?>

    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    <?php echo Form::label('ktp', 'KTP/SIM:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::text('ktp', null, ['class' => 'form-control', 'disabled' => true]); ?>

    </div>
</div>

<!-- Sub Title Field -->
<div class="form-group">
    <?php echo Form::label('biodata', 'Biodata:', ['class' => 'col-sm-2 control-label']); ?>


    <div class="col-sm-10">
        <?php echo Form::textarea('biodata', null, ['class' => 'form-control', 'disabled' => true]); ?>

    </div>
</div>
<?php endif; ?>
<!-- Submit Field -->
<div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
    <?php if(auth()->user()->roles->first()->name === 'creator'): ?>
    <a href="<?php echo route('creator.personal.edit', $user->id); ?>" class="btn btn-default">Edit</a>
    <?php elseif(auth()->user()->roles->first()->name === 'brand'): ?>
    <a href="<?php echo route('brand.personal.edit', $user->id); ?>" class="btn btn-default">Edit</a>
    <?php else: ?>
    <a href="<?php echo route('admin.personal.edit', $user->id); ?>" class="btn btn-default">Edit</a>
    <?php endif; ?>
    </div>
</div>

<?php $__env->startSection('scripts'); ?>
<script src="<?php echo e(asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/piexif.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/sortable.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/purify.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/fileinput.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/plugins/tinymce/jquery.tinymce.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/plugins/tinymce/tinymce.min.js')); ?>"></script>
<script>

    var formType = '<?php echo e($formType); ?>';
    $('#photo').val('');
    $('.dateinput').datepicker({
        format: 'dd-mm-yyyy'
    });

    $("#photo").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="<?php echo e(($formType == 'edit' && $user->image) ? asset($user->image) : asset('img/default_avatar_male.jpg')); ?>" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png"]
    });

    $("#cover").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-2',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="<?php echo e(($formType == 'edit' && $user->cover) ? asset($user->cover) : asset('img/default_avatar_male.jpg')); ?>" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png"]
    });
    
</script>
<?php if($formType == 'edit'): ?>
<?php echo $__env->make('layouts.datatables_js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script>
    $.fn.dataTable.ext.buttons.create = {
        action: function (e, dt, button, config) {
            $('#myModal').modal('show');
        }
    };

    var hash = document.location.hash;
    if (hash) {
        console.log(hash);
        $('.nav-tabs a[href="'+hash+'"]').tab('show')
    }

    // Change hash for page-reload
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
    window.location.hash = e.target.hash;
    });
</script>
<?php endif; ?>
<?php $__env->stopSection(); ?>