<?php $__env->startSection('css'); ?>
    <?php echo $__env->make('layouts.datatables_css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<link href="<?php echo e(asset('css/plugins/iCheck/custom.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('breadcrumb'); ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Browse Influencer</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Campaign</a>
            </li>
            <li>
                View
            </li>
            <li class="active">
                <strong>Browse Influencers</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentCrud'); ?>
    <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row border-bottom white-bg dashboard-header">
        <div class="col-sm-12">
            <label>Gender : </label>
            <div style="display: inline-block; vertical-align: top;">
                <div class="i-checks"><label> <input type="radio" value="" checked name="gender"> <i></i> All </label></div>
                <div class="i-checks"><label> <input type="radio" value="M" name="gender"> <i></i> Male </label></div>
                <div class="i-checks"><label> <input type="radio" value="F" name="gender"> <i></i> Female </label></div>
            </div>
        </div>
        <div class="col-sm-12">
            <label>Followers : </label>
            <div style="display: inline-block; vertical-align: top;">
                <div class="i-checks col-sm-4"><label> <input type="radio" data-id="1" name="follower"> <i></i> Less than 5k </label></div>
                <div class="i-checks col-sm-4"><label> <input type="radio" data-id="2" name="follower"> <i></i> 5k-10k </label></div>
                <div class="i-checks col-sm-4"><label> <input type="radio" data-id="3" name="follower"> <i></i> 10k-100k </label></div>
                <div class="i-checks col-sm-4"><label> <input type="radio" data-id="4" name="follower"> <i></i> 100k-500k </label></div>
                <div class="i-checks col-sm-4"><label> <input type="radio" data-id="5" name="follower"> <i></i> 500k-1000k+ </label></div>
            </div>
        </div>
        <div class="col-sm-12">
            <label>Categories :</label>
            <div style="display:inline-block; vertical-align: top;">
                <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="i-checks col-sm-4">
                    <label> <input name="cat" type="checkbox" value="<?php echo e($val->title); ?>"> <i></i> <?php echo e($val->title); ?> </label>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="input-group"><input type="text" name="query" id="query" placeholder="username" class="input-sm form-control"> <span class="input-group-btn">
                <button type="button" class="btn btn-sm btn-primary" id="gas"> Filter</button> </span>
            </div>
        </div>
            <div class="col-sm-4"><button type="button" class="btn btn-sm btn-danger" id="hapus"> Clear Filter</button>
        </div>
    </div>
    <!-- <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Browse Influencers</h5>
                </div>

                <div class="ibox-content">
                    <div class="col-sm-12">
                        <label>Gender : </label>
                        <div style="display: inline-block; vertical-align: top;">
                            <div class="i-checks"><label> <input type="radio" value="" checked name="gender"> <i></i> All </label></div>
                            <div class="i-checks"><label> <input type="radio" value="M" name="gender"> <i></i> Male </label></div>
                            <div class="i-checks"><label> <input type="radio" value="F" name="gender"> <i></i> Female </label></div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label>Followers : </label>
                        <div style="display: inline-block; vertical-align: top;">
                            <div class="i-checks col-sm-4"><label> <input type="radio" data-id="1" name="follower"> <i></i> Less than 5k </label></div>
                            <div class="i-checks col-sm-4"><label> <input type="radio" data-id="2" name="follower"> <i></i> 5k-10k </label></div>
                            <div class="i-checks col-sm-4"><label> <input type="radio" data-id="3" name="follower"> <i></i> 10k-100k </label></div>
                            <div class="i-checks col-sm-4"><label> <input type="radio" data-id="4" name="follower"> <i></i> 100k-500k </label></div>
                            <div class="i-checks col-sm-4"><label> <input type="radio" data-id="5" name="follower"> <i></i> 500k-1000k+ </label></div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <label>Categories :</label>
                        <div style="display:inline-block; vertical-align: top;">
                            <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="i-checks col-sm-4">
                                <label> <input name="cat" type="checkbox" value="<?php echo e($val->title); ?>"> <i></i> <?php echo e($val->title); ?> </label>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="input-group"><input type="text" name="query" id="query" placeholder="username" class="input-sm form-control"> <span class="input-group-btn">
                            <button type="button" class="btn btn-sm btn-primary" id="gas"> Filter</button> </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

                    <div class="row list">
                        <?php $__currentLoopData = $influencers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $influencer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                       
                        <div class="col-lg-4">
                            <div class="widget-head-color-box navy-bg p-lg text-center">
                                <div class="m-b-md">
                                <h2 class="font-bold no-margins">
                                    <?php echo e($influencer->name); ?>

                                </h2>
                                    <small><?php echo e($influencer->detailInfluencer->username); ?></small>
                                </div>
                                <img src="<?php echo e($influencer->detailInfluencer->profile_picture_ig); ?>" class="img-circle circle-border m-b-md" alt="profile">
                                <div>
                                    <span><?php echo e($influencer->detailInfluencer->followers_ig); ?> Followers</span>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <!-- <table class="table table-responsive table-bordered" id="table-influencer" style="margin-top: 40px">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Display Picture</th>
                                <th>Username</th>
                                <th>Total Followers</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php ($i=1); ?>
                            <?php $__currentLoopData = $influencers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $influencer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td>
                                        <?php echo e($i++); ?>

                                    </td>
                                    <td>
                                        <img src="<?php echo e($influencer->detailInfluencer->profile_picture_ig); ?>" width="100px"></img>
                                    </td>
                                    <td>
                                        <?php echo e($influencer->detailInfluencer->username); ?>

                                    </td>
                                    <td>
                                        <?php echo e($influencer->detailInfluencer->followers_ig); ?>

                                    </td>
                                    <td>
                                        <input name="influencers[]" type="checkbox" data-id="<?php echo e($influencer->detailInfluencer->user_id); ?>">
                                    </td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table> -->
                    <div class="row border-bottom white-bg dashboard-header" style="margin-top: 10px;">
                        <div class="col-sm-12">
                            <?php if(auth()->user()->roles->first()->name === 'admin'): ?>
                            <a href="<?php echo e(route('influencer.index')); ?>" class="btn btn-default pull-right" style="margin-right: 15px">Back</a>
                            <?php endif; ?>
                        </div>
                    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script src="<?php echo e(asset('js/plugins/iCheck/icheck.min.js')); ?>"></script>
    <?php echo $__env->make('layouts.datatables_js', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <script>

        var max_followers = 0;
        var min_followers = 0;
        var cat_id = null;
        var username = null;
        var gender = null;
        var influencers = [];
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        var list = $('.list');

        var table = $('#table-influencer').DataTable({
            "lengthChange": false,
            "searching": false
        });

        $('#query').bind("change keyup", function() {
            var val = $(this).val();
            var regex = /[%*]/g;
            if (val.match(regex)) {
                val = val.replace(regex, "");
                $(this).val(val);
            }
            $("p").html(val);
        });

        $('#hapus').click(function(){
            $('.i-checks').iCheck('uncheck');
            $('#query').val('');

            max_followers = 0;
            min_followers = 0;
            cat_id = null;
            username = null;
            gender = null;
            getData();
        });

        $('#gas').click(function(){
            var cat = $('[name="cat"]:checked').map(function(){
                            return $(this).val();
                        }).toArray();
            console.log(cat);

            var id = $('[name="follower"]:checked').data("id");
            console.log(id);

            switch (id) {
                case 1:
                    max_followers = 5000;
                    min_followers = 0;
                    break;
                case 2:
                    max_followers = 10000;
                    min_followers = 5000;
                    break;
                case 3:
                    max_followers = 100000;
                    min_followers = 10000;
                    break;
                case 4:
                    max_followers = 500000;
                    min_followers = 100000;
                    break;
                case 5:
                    max_followers = 1000000000;
                    min_followers = 500000;
                    break;
            }
            console.log(id);

            var gen = $('[name="gender"]:checked').val();
            console.log($('[name="gender"]:checked').val());

            var user = $('[name="query"]').val();

            cat_id = cat;
            username = user;
            gender = gen;
            getData();
        });

        $('#btn-followers').click(function(){
            $('.btn-followers').prop('disabled', false);
            $('.btn-all').prop('disabled', false);
            $(this).prop('disabled', true);
            var id = $(this).data("id");

            switch (id) {
                case 1:
                    max_followers = 5000;
                    min_followers = 0;
                    break;
                case 2:
                    max_followers = 10000;
                    min_followers = 5000;
                    break;
                case 3:
                    max_followers = 100000;
                    min_followers = 10000;
                    break;
                case 4:
                    max_followers = 500000;
                    min_followers = 100000;
                    break;
                case 5:
                    max_followers = 1000000000;
                    min_followers = 500000;
                    break;
            }

            getData();

        });

        $('.btn-all').click(function(){
            $('.btn-followers').prop('disabled', false);
            $(this).prop('disabled', true);
            max_followers = 0;
            min_followers = 0;
            getData();
        });

        $('.btn-category').click(function(){
            $('.btn-category').prop('disabled', false);
            $(this).prop('disabled', true);
            var id = $(this).data("id");

            cat_id = id;
            getData();
        });

        $('#btn-save').click(function(){
            console.log('save');
            $.each($("input[name='influencers[]']"), function () {
                //console.log($(this));
                if($(this).is(":checked"))
                {   
                    influencers.push($(this).data("id"));
                }
            });
            //console.log(influencers);

            $.ajax({
                url: "<?php echo e(route('admin.campaign.addtotableinfluencers')); ?>",
                method: "GET",
                data: {
                    id_campaign: <?php echo e(Request::segment(3)); ?>,
                    id_influencers: influencers
                },
                success: function(response){
                    console.log(response);
                    if(response.result.status == 'success'){
                        console.log('test');
                        toastr.success("Influencer added");
                        window.location.replace("<?php echo e(route('admin.campaign.look', Request::segment(3))); ?>");
                    }else{
                        toastr.error("Insufficient campaigns budget. Please edit budget first.");
                    }                    
                },
                error: function(response){
                    console.log(response);
                }
            }); 
        });

        function getData()
        {   
            $.ajax({
                <?php if(auth()->user()->roles->first()->name === 'admin'): ?>
                url: "<?php echo e(route('admin.influencer.getInfluencer')); ?>",
                <?php else: ?>
                url: "<?php echo e(route('brand.influencer.getInfluencer')); ?>",
                <?php endif; ?>
                method: "GET",
                data: {
                    max_followers: max_followers,
                    min_followers: min_followers,
                    cat_id: cat_id,
                    username: username,
                    gender: gender,
                },
                success: function(response){
                    console.log(response);
                    if(response.result.length > 0){
                        list.empty();
                        $.each(response.result, function (index, value) {
                            list.append('<div class="col-lg-4"><div class="widget-head-color-box navy-bg p-lg text-center"><div class="m-b-md"><h2 class="font-bold no-margins">'+value.name+'</h2><small>'+value.detail_influencer.username+'</small></div><img src="'+value.detail_influencer.profile_picture_ig+'" class="img-circle circle-border m-b-md" alt="profile"><div><span>'+value.detail_influencer.followers_ig+' Followers</span></div></div></div>');
                        });
                    }
                    else
                    {
                        list.empty();
                        list.append('<div class="col-lg-12" style="margin-top: 20px;"><div class="ibox float-e-margins"><div class="ibox-content text-center p-md"><h2><span class="text-navy">Sorry, the user you are looking for was not found.</span><p></div></div></div>');
                    }
                    
                },
                error: function(response){
                    console.log(response);
                }
            });            
            
        }

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>