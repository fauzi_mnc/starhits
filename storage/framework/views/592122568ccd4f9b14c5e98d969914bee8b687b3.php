<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <span>
                        <img width="48" height="48" alt="image" class="img-circle" src="<?php echo e(asset(auth()->user()->image)); ?>">
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo e(auth()->user()->name); ?></strong>
                             </span> <span class="text-muted text-xs block"><?php echo e(auth()->user()->roles()->where('name', request()->segment(1))->first()->display_name); ?> <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <?php $__currentLoopData = auth()->user()->roles()->whereNotIn('name', [request()->segment(1)])->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li><a href="<?php echo e(url("$row->name")); ?>/home">As <?php echo e($row->display_name); ?></a></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                    
                </div>
            </li>
            <?php if(auth()->user()->hasRole(['admin', 'superior finance'])): ?>
            <li class="<?php echo e(Request::is('admin/dashboard*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('dashboard.index')); ?>"><i class="fa fa-dashboard <?php echo e((Request::is('dashboard') ? 'active' : '')); ?>"></i> <span class="nav-label">Dashboard</span></a>
            </li>
            <li class="<?php echo e(Request::is('admin/channel*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('channel.index')); ?>"><i class="fa fa-adjust <?php echo e((Request::is('channel') ? 'active' : '')); ?>"></i> <span class="nav-label">Channel</span></a>
            </li>
            <li class="<?php echo e(Request::is('admin/creator*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('creator.index')); ?>"><i class="fa fa-group <?php echo e((Request::is('creator') ? 'active' : '')); ?>"></i> <span class="nav-label">Creator</span></a>
            </li>
            <li class="<?php echo e(Request::is('admin/serie*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('admin.serie.index')); ?>"><i class="fa fa-star <?php echo e((Request::is('serie') ? 'active' : '')); ?>"></i> <span class="nav-label">Series</span></a>
            </li>
            <li class="<?php echo e(Request::is('admin/posts*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('admin.posts.index')); ?>"><i class="fa fa-play <?php echo e((Request::is('posts') ? 'active' : '')); ?>"></i> <span class="nav-label">Posts</span></a>
            </li>
            <li class="<?php echo e(Request::is('admin/ads*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('ads.index')); ?>"><i class="fa fa-language <?php echo e((Request::is('ads') ? 'active' : '')); ?>"></i> <span class="nav-label">Ads</span></a>
            </li>
            <li class="<?php echo e(Request::is('admin/brand*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('brand.index')); ?>"><i class="fa fa-diamond <?php echo e((Request::is('brand') ? 'active' : '')); ?>"></i> <span class="nav-label">Brands</span></a>
            </li>
            <li class="<?php echo e(Request::is('admin/influencer*') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-diamond"></i> <span class="nav-label">Influencers</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo e(Request::is('admin/influencer') ? 'active' : ''); ?>"><a href="<?php echo e(route('influencer.index')); ?>"><i class="fa fa fa-diamond"></i> Influencers</a></li>
                    <li class="<?php echo e(Request::is('admin/influencer/browse-influencer*') ? 'active' : ''); ?>"><a href="<?php echo e(route('admin.influencer.browse')); ?>"><i class="fa fa-users"></i> Browse Influencer</a></li>
                </ul>
            </li>
            <!-- <li class="<?php echo e(Request::is('admin/influencer*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('influencer.index')); ?>"><i class="fa fa-diamond <?php echo e((Request::is('influencer') ? 'active' : '')); ?>"></i> <span class="nav-label">Influencers</span></a>
            </li> -->
            <li class="<?php echo e(Request::is('admin/revenue*') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-money"></i> <span class="nav-label">Revenue</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo e(Request::is('admin/revenue') ? 'active' : ''); ?>"><a href="<?php echo e(route('revenue.index')); ?>"><i class="fa fa-money"></i> Revenue</a></li>
                    <li class="<?php echo e(Request::is('admin/revenue/approval*') ? 'active' : ''); ?>"><a href="<?php echo e(route('revenue.approval.index')); ?>"><i class="fa fa-refresh"></i> Pending Approval</a></li>
                    <li class="<?php echo e(Request::is('admin/revenue/approver*') ? 'active' : ''); ?>"><a href="<?php echo e(route('revenue.approver.index')); ?>"><i class="fa fa-user-plus"></i> Approver</a></li>
                </ul>
            </li>
            <li class="<?php echo e(Request::is('admin/campaign*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('admin.campaign.index')); ?>"><i class="fa fa-handshake-o <?php echo e((Request::is('campaign') ? 'active' : '')); ?>"></i> <span class="nav-label">Campaign</span></a>
            </li>
            <!-- <li class="<?php echo e(Request::is('admin/platforms*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('platforms.index')); ?>"><i class="<?php echo e((Request::is('platforms') ? 'active' : '')); ?>"></i> <span class="nav-label">Platforms</span></a>
            </li> -->
            <li class="<?php echo e(Request::is('admin/config*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('config.index')); ?>"><i class="fa fa-gears <?php echo e((Request::is('config') ? 'active' : '')); ?>"></i> <span class="nav-label">Website Configuration</span></a>
            </li>
            <li class="<?php echo e(Request::is('admin/personal*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('admin.personal.index')); ?>"><i class="fa fa-user <?php echo e((Request::is('personal') ? 'active' : '')); ?>"></i> <span class="nav-label">Personal Setting</span></a>
            </li>
            <!-- <li class="<?php echo e(Request::is('admin/payment*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('payment.index')); ?>"><i class="<?php echo e((Request::is('payment') ? 'active' : '')); ?>"></i> <span class="nav-label">Payment Setting</span></a>
            </li> -->
            <li class="<?php echo e(Request::is('admin/page*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('page.index')); ?>"><i class="fa fa-file <?php echo e((Request::is('page') ? 'active' : '')); ?>"></i> <span class="nav-label">Pages</span></a>
            </li>
            <li class="<?php echo e(Request::is('admin/menu*', 'admin/user*', 'admin/role*', 'admin/category*') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">System Admin</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo e(Request::is('admin/user*') ? 'active' : ''); ?>"><a href="<?php echo e(route('user.index')); ?>"><i class="fa fa-user-plus"></i> User Management</a></li>
                    <!-- <li class="<?php echo e(Request::is('admin/menu*') ? 'active' : ''); ?>"><a href="<?php echo e(route('role.index')); ?>">Menu Management</a></li> -->
                    <li class="<?php echo e(Request::is('admin/role*') ? 'active' : ''); ?>"><a href="<?php echo e(route('role.index')); ?>"><i class="fa fa-users"></i> Role Management</a></li>
                    <li class="<?php echo e(Request::is('admin/category*') ? 'active' : ''); ?>"><a href="<?php echo e(route('category.index')); ?>"><i class="fa fa-clone"></i> Product Category</a></li>
                    <li class="<?php echo e(Request::is('admin/menu*') ? 'active' : ''); ?>"><a href="<?php echo e(route('menu.index')); ?>"><i class="fa fa-navicon"></i> Menu Management</a></li>
                </ul>
            </li>
            <?php elseif(auth()->user()->hasRole('creator') && request()->segment(1) == 'creator'): ?>
            <li class="<?php echo e(Request::is('creator/serie*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('creator.serie.index')); ?>"><i class="fa fa-star <?php echo e((Request::is('serie') ? 'active' : '')); ?>"></i> <span class="nav-label">Series</span></a>
            </li>
            <li class="<?php echo e(Request::is('creator/posts*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('creator.posts.index')); ?>"><i class="fa fa-play <?php echo e((Request::is('posts') ? 'active' : '')); ?>"></i> <span class="nav-label">Posts</span></a>
            <li class="<?php echo e(Request::is('creator/bookmark*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('creator.bookmark.index')); ?>"><i class="fa fa-address-book <?php echo e((Request::is('bookmark') ? 'active' : '')); ?>"></i> <span class="nav-label">Bookmark</span></a>
            </li>
            <li class="<?php echo e(Request::is('creator/platforms*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('creator.platforms.index')); ?>"><i class="fa fa-globe <?php echo e((Request::is('platforms') ? 'active' : '')); ?>"></i> <span class="nav-label">Platforms</span></a>
            </li>
            <li class="<?php echo e(Request::is('creator/multiUpload*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('creator.multiUpload.getUpload')); ?>"><i class="fa fa-upload <?php echo e((Request::is('multiUpload') ? 'active' : '')); ?>"></i> <span class="nav-label">Multi Upload</span></a>
            </li>
            <li class="<?php echo e(Request::is('creator/analytic*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('creator.analytic.index')); ?>"><i class="fa fa-bar-chart <?php echo e((Request::is('analytic') ? 'active' : '')); ?>"></i> <span class="nav-label">Analytic</span></a>
            </li>
            <li class="<?php echo e(Request::is('creator/revenue*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('creator.revenue.index')); ?>"><i class="fa fa-money <?php echo e((Request::is('revenue') ? 'active' : '')); ?>"></i> <span class="nav-label">Revenue</span></a>
            </li>
            <li class="<?php echo e(Request::is('creator/setting*') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">Setting</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo e(Request::is('personal') ? 'active' : ''); ?>"><a href="<?php echo e(route('creator.personal.index')); ?>"><i class=""></i> Personal</a></li>
                    <?php if(auth()->user()->hasRole('influencer')): ?>
                    <li class="<?php echo e(Request::is('creator/setting/*/payment*') ? 'active' : ''); ?>"><a href="<?php echo e(route('self.creator.edit', [auth()->id(), 'payment'])); ?>"><i class=""></i> Payment</a></li>
                    <li class="<?php echo e(Request::is('creator/setting/*/rate_card*') ? 'active' : ''); ?>"><a href="<?php echo e(route('self.creator.edit', [auth()->id(), 'rate_card'])); ?>"><i class=""></i> Rate Card</a></li>
                    <?php endif; ?>
                    <li class="<?php echo e(Request::is('creator/setting/*/social_media*') ? 'active' : ''); ?>"><a href="<?php echo e(route('self.creator.edit', [auth()->id(), 'social_media'])); ?>"><i class=""></i> Social Media</a></li>
                </ul>
            </li>
            <?php elseif(auth()->user()->roles->first()->name === 'brand'): ?>
            <li class="<?php echo e(Request::is('brand/influencer*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('brand.influencer.browse')); ?>"><i class="fa fa-users <?php echo e((Request::is('influencer') ? 'active' : '')); ?>"></i> <span class="nav-label">Browse Influencer</span></a>
            </li>
            <li class="<?php echo e(Request::is('brand/campaign*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('brand.campaign.index')); ?>"><i class="fa fa-handshake-o <?php echo e((Request::is('campaign') ? 'active' : '')); ?>"></i> <span class="nav-label">Campaign</span></a>
            </li>
            <li class="<?php echo e(Request::is('brand/personal*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('brand.personal.index')); ?>"><i class="fa fa-user <?php echo e((Request::is('personal') ? 'active' : '')); ?>"></i> <span class="nav-label">Personal Setting</span></a>
            </li>
            <?php elseif(auth()->user()->hasRole('influencer') && request()->segment(1) == 'influencer'): ?>
            <li class="<?php echo e(Request::is('influencer/campaign*') ? 'active' : ''); ?>">
                <a href="<?php echo e(route('influencer.campaign.index')); ?>"><i class="fa fa-handshake-o <?php echo e((Request::is('campaign') ? 'active' : '')); ?>"></i> <span class="nav-label">Campaign</span></a>
            </li>
            <li class="<?php echo e(Request::is('influencer/setting*') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">Setting</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo e(Request::is('influencer/setting/*/personal*') ? 'active' : ''); ?>"><a href="<?php echo e(route('self.influencer.edit', [auth()->id(), 'personal'])); ?>"><i class=""></i> Personal</a></li>
                    <li class="<?php echo e(Request::is('influencer/setting/*/payment*') ? 'active' : ''); ?>"><a href="<?php echo e(route('self.influencer.edit', [auth()->id(), 'payment'])); ?>"><i class=""></i> Payment</a></li>
                    <li class="<?php echo e(Request::is('influencer/setting/*/rate_card*') ? 'active' : ''); ?>"><a href="<?php echo e(route('self.influencer.edit', [auth()->id(), 'rate_card'])); ?>"><i class=""></i> Rate Card</a></li>
                    <li class="<?php echo e(Request::is('influencer/setting/*/social_media*') ? 'active' : ''); ?>"><a href="<?php echo e(route('self.influencer.edit', [auth()->id(), 'social_media'])); ?>"><i class=""></i> Social Media</a></li>
                </ul>
            </li>
            <?php elseif(auth()->user()->hasRole('member')): ?>
            <li class="<?php echo e(Request::is('member*') ? 'active' : ''); ?>">
                <a href="#"><i class="fa fa-cog"></i> <span class="nav-label">Setting</span>
                <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li class="<?php echo e(Request::is('member/*/edit*') ? 'active' : ''); ?>"><a href="<?php echo e(route('self.member.edit', [auth()->id()])); ?>"><i class=""></i> Personal</a></li>
                    <li class="<?php echo e(Request::is('member/*/upgrade*') ? 'active' : ''); ?>"><a href="<?php echo e(route('self.member.upgrade', [auth()->id()])); ?>"><i class=""></i> Upgrade Account</a></li>
                </ul>
            </li>
            <?php endif; ?>
        </ul>
    </div>
</nav>