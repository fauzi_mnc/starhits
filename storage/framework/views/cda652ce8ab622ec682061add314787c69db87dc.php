<?php $__env->startSection('breadcrumb'); ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Posts</h2>
        <ol class="breadcrumb">
            <li>
                Posts
            </li>
            <?php if(auth()->user()->roles->first()->name === 'admin'): ?>
            <li class="active">
                <a href="<?php echo e(route('admin.posts.index')); ?>"><strong>Table</strong></a>
            </li>
            <?php else: ?>
            <li class="active">
                <a href="<?php echo e(route('creator.posts.index')); ?>"><strong>Table</strong></a>
            </li>
            <?php endif; ?>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentCrud'); ?>
<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Posts</h5>
                    <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modalAddPosts" style="margin-top: -10px;margin-bottom: 5px">
                        Add Post
                    </button>
                    <div class="modal inmodal fade" id="modalAddPosts" tabindex="-1" role="dialog"  aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Choose Add Post</h4>
                                </div>
                                <div class="modal-body">
                                    <?php if(auth()->user()->roles->first()->name === 'admin'): ?>
                                    <a class="btn btn-primary" href="<?php echo e(route('admin.posts.createManual')); ?>">Create Post Manual</a>
                                    <a class="btn btn-primary" href="<?php echo e(route('admin.posts.createAutomatic')); ?>">Create Post Automatic</a>
                                    <?php else: ?>
                                    <a class="btn btn-primary" href="<?php echo e(route('creator.posts.createManual')); ?>">Create Post Manual</a>
                                    <a class="btn btn-primary" href="<?php echo e(route('creator.posts.createAutomatic')); ?>">Create Post Automatic</a>
                                    <?php endif; ?>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ibox-content">
                    <?php echo $__env->make('posts.table', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>