<?php if($paginator->hasPages()): ?>
<ul class="pagination pagination_type1 pagination_type3 col-md-12">
    <li class="pagination__item col-sm-4 my-auto">
        <a class="pagination__number" href="<?php echo e($paginator->previousPageUrl()); ?>">
            <span class="pagination__control <?php echo e((isset($_GET['page']) && $_GET['page'] > 1) ? 'pagination__control_next':'pagination__control_prev'); ?>">
                Newer
            </span>
        </a>
    </li>
    <li class="pagination__item col-sm-4">
        <div class="paginate">
            <div class="above-number">
                <span class="pagination__number">
                    <?php echo e($paginator->currentPage()); ?>

                </span>
            </div>
            <div class="bottom-number">
                <span class="pagination__number">
                    <?php echo e($paginator->lastPage()); ?>

                </span>
            </div>
        </div>
    </li>
    <li class="pagination__item col-sm-4 my-auto">
        <a class="pagination__number" href="<?php echo e($paginator->nextPageUrl()); ?>">
            <span class="pagination__control pagination__control_next">
                Older
            </span>
        </a>
    </li>
</ul>
<?php endif; ?>
