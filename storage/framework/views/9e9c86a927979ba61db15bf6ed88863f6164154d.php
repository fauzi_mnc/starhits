<div class='btn-group'>
    <a href="<?php echo e(route('role.edit', $id)); ?>" class='btn btn-default btn-xs' title="Edit Role">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
</div>