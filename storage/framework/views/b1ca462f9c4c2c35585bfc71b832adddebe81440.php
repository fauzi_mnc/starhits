<!-- Stored in resources/views/layouts/app.blade.php -->
<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
    <head>
        <link rel="shortcut icon" href="<?php echo e(asset('favicon.ico')); ?>">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Star Hits is a digital network that develops creator, talent or influencers, to cooperate with the brand and to create the ecosystem that are connected in the content.">
        <meta name="author" content="">
        <?php if(Request::server ("SERVER_NAME") == 'starhits.id'): ?>
        <!-- starhits.id -->
        <meta name="google-site-verification" content="faXytG0YQOHovhpU4FQJQIfH8BzRG_AUYVMyxSj3x5Q" />
        <?php endif; ?>
        <?php if(Request::server ("SERVER_NAME") == 'starhits.com'): ?>
        <!-- starhits.com -->
        <meta name="google-site-verification" content="1LWlOlljjs-uqtRDTFDgpI1pAultCpoCe34OLsU_iuU" />
        <?php endif; ?>
        <?php if(Request::server ("SERVER_NAME") == 'devapp.mncgroup.com'): ?>
        <!-- devapp.mncgroup.com -->
        <meta name="google-site-verification" content="ttKnh3tVbV_kekxislWx7tZ8E9Mxs3Q5MaLIX7KPmiU" />
        <?php endif; ?>
        <?php if(Request::server ("SERVER_NAME") == 'localhost/starhits/public'): ?>
        <!-- starhits.com -->
        <meta name="google-site-verification" content="1LWlOlljjs-uqtRDTFDgpI1pAultCpoCe34OLsU_iuU" />
        <?php endif; ?>

        <?php echo $__env->yieldPushContent('meta'); ?>

        <!-- Email From Star Hits Official -->
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-92172449-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-92172449-1');
        </script>
        <title>
            <?php if($__env->yieldContent('title')): ?>
                <?php echo $__env->yieldContent('title'); ?> -
            <?php endif; ?>
            <?php if(isset($SiteConfig['configs']['website_title'])): ?>
                <?php echo e($SiteConfig['configs']['website_title']); ?>

            <?php endif; ?>
        </title>
        <!-- Bootstrap core CSS -->
        <link href="<?php echo e(asset('frontend/assets/vendor/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo e(asset('frontend/assets/vendor/font-awesome/css/font-awesome.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('frontend/assets/vendor/slick/css/slick.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('frontend/assets/vendor/slick/css/slick-theme.css')); ?>">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />

        <!-- Custom styles for this template -->
        <link rel="stylesheet" href="<?php echo e(asset('frontend/assets/css/style.min.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('frontend/assets/css/media.min.css')); ?>">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
        <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
        <link href="<?php echo e(asset('frontend/assets/css/jquery.simplewizard.css')); ?>" rel="stylesheet" />
        <link href="<?php echo e(asset('css/plugins/switchery/switchery.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(asset('frontend/assets/css/bootstrap-select.css')); ?>" rel="stylesheet">
        <style>
        .btn-selectpick{
            background: #faffbe;
            border-color: #faffbe;
            box-shadow: none !important;
            border-radius: 4px;
        }
        .wizard > .content > .body{
            position: relative !important;
        }
        strong{
            padding-right: 30px;
        }
        </style>

        <script>
            var baseUrl = "<?php echo e(url('/')); ?>";
        </script>
        <style id="antiClickjack"></style>
        <script async custom-element="amp-auto-ads"
                src="https://cdn.ampproject.org/v0/amp-auto-ads-0.1.js">
        </script>
        <script type="text/javascript" src="https://dev.mncdigital.info/public/js/mncdig.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                mncdigAuth('3bklCZNb7a2AkoiX', 'mJ1L0wMxzH2HFcqc');
            })
        </script>
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-5195976246447231",
            enable_page_level_ads: true
        });
        </script>
    </head>
    <body>
        <?php if(env('APP_ENV') != 'local'): ?>
            <?php echo $__env->yieldPushContent('preloader'); ?>
        <?php endif; ?>

        <!-- Page Content -->
        <?php echo $__env->make('frontend.partials._header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="content">
            <amp-auto-ads type="adsense"
                data-ad-client="ca-pub-5195976246447231">
            </amp-auto-ads>
            <?php echo $__env->yieldContent('content'); ?>
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        </div>
        <?php echo $__env->make('frontend.partials._footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

        <div class="modal fade bs-example-modal-lg" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form action="<?php echo e(url('/search')); ?>" method="post">
                    <?php echo e(csrf_field()); ?>   
                    <div id="custom-search-input">
                        <div class="input-group col-md-12">
                            <input id="search" type="text" class="form-control search input-lg" name="search" placeholder="Search" autofocus />
                                <span class="input-group-btn">
                                    <button class="btn btn-info btn-lg" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>    

        <?php if(auth()->guard()->guest()): ?>
            <?php echo $__env->make('frontend.partials._modals-login', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <?php echo $__env->make('frontend.partials._modals-register-role', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            
            

            <?php echo $__env->make('frontend.partials._modals-forgotpassword', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php endif; ?>

        <a href="javascript:" id="return-to-top"><i class="fa fa-chevron-up"></i></a>

        <!-- App scripts -->
        

        <!-- Bootstrap core JavaScript -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo e(asset('frontend/assets/js/popper.js')); ?>"></script>
        <script type="text/javascript" src="<?php echo e(asset('frontend/assets/vendor/bootstrap/js/bootstrap.min.js')); ?>"></script>
        <script type="text/javascript" src="<?php echo e(asset('frontend/assets/js/bootstrap-select.js')); ?>"></script>
        <script type="text/javascript" src="<?php echo e(asset('frontend/assets/vendor/navstrap/js/navstrap.min.js')); ?>"></script>
        <script type="text/javascript" src="<?php echo e(asset('frontend/assets/vendor/slick/js/slick.min.js')); ?>"></script>
        <script type="text/javascript" src="<?php echo e(asset('frontend/assets/vendor/loadmore/js/loadMoreResults.js')); ?>"></script>
        <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?php echo e(asset('frontend/assets/js/addtoany.js')); ?>"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

        <!-- jQuery UI -->
        <script src="<?php echo e(asset('js/plugins/jquery-ui/jquery-ui.min.js')); ?>"></script>

        <!-- Include this after the sweet alert js file -->
        <?php echo $__env->make('sweet::alert', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        
        <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
        <script>
            $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4'
            });
        </script>

        <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
        <script>
            //paste this code under head tag or in a seperate js file.
            // Wait for window load
            $(window).load(function() {
                // Animate loader off screen
                $(".se-pre-con").fadeOut("slow");;
            });
        </script>

        <!-- Custom JavaScript -->
        <script src="<?php echo e(asset('frontend/assets/js/init.js')); ?>"></script>
        <?php echo $__env->yieldPushContent('scripts'); ?>
        <script>
        /**
        *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.

        *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
        /*
        var disqus_config = function () {
        this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        };
        */
        (function() { // DON'T EDIT BELOW THIS LINE
            var d = document, s = d.createElement('script');
                s.src = 'https://starhits-id.disqus.com/embed.js';
                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
            })
        ();
        </script>
        <script id="dsq-count-scr" src="//starhits-id.disqus.com/count.js" async></script>
        <script>
            $(document).ready(function() {
                $("#form-login").on("submit",function(e) {
                //e.preventDefault();
                var form = $(this);
                var field = form.find("input[name=password]");
                var hash = btoa(field.val());
                var hash2 = btoa(hash);
                var slice = hash2.substr(hash2.length - 2);
                hash2 = hash2.slice(0, -2);
                var random_char = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
                var slice_random = random_char.substring(0, 14); //only get 14 characters
                var new_str = hash2+slice_random+slice;
                field.val(new_str);
            });

                if (self === top) {
                    var antiClickjack = document.getElementById("antiClickjack");
                        antiClickjack.parentNode.removeChild(antiClickjack);
                } else {
                    top.location = self.location;
                }
            });
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script>
        <script src="<?php echo e(asset('frontend/assets/js/jquery.simplewizard.js')); ?>"></script>
        <script>
            $(function () {
                $("#wizard1").simpleWizard({
                    cssClassStepActive: "active",
                    cssClassStepDone: "done",
                    onFinish: function() {
                        alert("Wizard finished")
                    }
                });
            });
        </script>
    </body>
</html>
