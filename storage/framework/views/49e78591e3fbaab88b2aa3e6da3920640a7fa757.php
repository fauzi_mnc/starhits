<?php $__env->startSection('breadcrumb'); ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Menu Table</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Menu</a>
            </li>
            <li class="active">
                <strong>Table</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentCrud'); ?>
<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Menu</h5>
                    <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modalAddMenu" style="margin-top: -10px;margin-bottom: 5px">
                        Add
                    </button>
                    <div class="modal inmodal fade" id="modalAddMenu" tabindex="-1" role="dialog"  aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Choose Add Menu</h4>
                                </div>
                                <div class="modal-body">
                                    <a class="btn btn-primary" href="<?php echo e(route('menu.create-parent')); ?>">Create Menu Parent</a>
                                    <a class="btn btn-primary" href="<?php echo e(route('menu.create-child')); ?>">Create Menu Child</a>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ibox-content">
                    <?php echo $__env->make('menu.table', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>