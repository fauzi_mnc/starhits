<div aria-hidden="true" aria-labelledby="regModalLabel" class="modal fade" id="regModal" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="login-form">
                <form method="post" action="<?php echo e(route('register')); ?>">
                    <?php echo e(csrf_field()); ?>

                    <div class="avatar">
                        <img alt="StarHits" src="<?php echo e(asset('frontend/assets/img/black_logo.png')); ?>">
                        </img>
                    </div>
                    <div class="text-center">
                        <p>
                            Create Star Hits account
                        </p>
                    </div>
                    <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                        <input class="form-control" name="name" value="<?php echo e(old('name')); ?>" placeholder="Full Name" required="required" style="font-family:Fontawesome;" type="text">
                        </input>
                    </div>
                    <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                        <input class="form-control" name="email" value="<?php echo e(old('email')); ?>" placeholder="Email" required="required" style="font-family:Fontawesome;" type="email">
                        </input>
                    </div>
                    <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                        <input class="form-control" name="password" placeholder="Password" required="required" style="font-family:Fontawesome;" type="password">
                        </input>
                    </div>
                    <div class="form-group">
                        <input class="form-control" name="password_confirmation" placeholder="Confirm Password" required="required" style="font-family:Fontawesome;" type="password">
                        </input>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" id="accept">
                                        <p style="font-size: 10px; margin-top: 3px;">
                                            By Signing Up, You Agree to the
                                            <a href="#">
                                                Term of Use
                                            </a>
                                            and
                                            <a href="#">
                                                Privacy Policy
                                            </a>
                                        </p>
                                    </input>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary btn-lg btn-block login-btn" id="form-submitbtn" type="submit" disabled="disabled">
                            Sign Up
                        </button>
                    </div>
                    <div class="or-seperator">
                    </div>
                    <p class="text-center small">
                        Already a Star Hits User?
                        <a aria-label="Close" data-dismiss="modal" data-target="#loginModal" data-toggle="modal" href="#">
                            Login now!
                        </a>
                    </p>
                </form>
            </div>
        </div>
    </div>
</div>

<?php $__env->startPush('scripts'); ?>
<script type="text/javascript">
    $('#accept').click(function() {
        if ($('#form-submitbtn').is(':disabled')) {
            $('#form-submitbtn').removeAttr('disabled');
        } else {
            $('#form-submitbtn').attr('disabled', 'disabled');
        }
    });
</script>
<?php $__env->stopPush(); ?>
