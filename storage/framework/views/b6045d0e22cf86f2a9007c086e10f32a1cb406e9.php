<?php $__env->startSection('breadcrumb'); ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Campaign Table</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Campaign</a>
            </li>
            <li class="active">
                <strong>Table</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentCrud'); ?>
<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Campaign</h5>
                    <?php if(auth()->user()->roles->first()->name === 'admin'): ?>
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="<?php echo e(route('admin.campaign.create')); ?>">Add New</a>
                    <?php else: ?>
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="<?php echo e(route('brand.campaign.create')); ?>">Add New</a>
                    <?php endif; ?>
                </div>

                <div class="ibox-content">
                    <?php if(auth()->user()->roles->first()->name === 'admin'): ?>
                    <div class="col-sm-3 m-b-xs">
                        <select class="input-sm form-control input-s-sm inline" id="status">
                            <option disabled selected>Search by status</option>
                            <option value="1">Draft</option>
                            <option value="2">Process</option>
                            <option value="3">Running</option>
                            <option value="4">Expired</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group"><input type="text" id="query" placeholder="Search title" class="input-sm form-control"> <span class="input-group-btn">
                            <button type="button" class="btn btn-sm btn-primary" id="gas"> Go!</button> </span>
                        </div>
                    </div>
                    <?php echo $__env->make('campaign.table', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php else: ?>
                    <?php echo $__env->make('campaign.otable', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>