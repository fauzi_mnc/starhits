<?php $__env->startPush('meta'); ?>    
    <meta property="og:url" content="<?php echo e(url()->current()); ?>" />
    <meta property="og:title" content="<?php echo e($metaTag->title); ?>" />
    <meta property="og:description" content="<?php echo e(\Illuminate\Support\Str::words(preg_replace('/&#?[a-z0-9]+;/i','', strip_tags($metaTag->content)), 80 )); ?>" />
    <?php if(!empty($metaTag->image)): ?>
    <meta property="og:image" content="<?php echo e($metaTag->image); ?>" />
    <?php else: ?>
    <meta property="og:image" content="<?php echo e(url($models['website_logo_header'])); ?>" />
    <?php endif; ?>
    <meta property="fb:app_id" content="952765028218113" />
<?php $__env->stopPush(); ?>

<!-- Stored in resources/views/child.blade.php -->


<?php $__env->startSection('title', $pagesDynamic->title); ?>

<?php $__env->startSection('content'); ?>

<div class="container-fluid blank-page">
    <div class="col-sm-12">
        <div class="title">
            <h2>
                <?php echo e($pagesDynamic->title); ?>

            </h2>
        </div>
        <hr>
            <div class="col-md-12" style="padding: 0;">                  
                <div class="text">
                    <p>
                        <b>
                            <font size="5">
                                <?php echo e($pagesDynamic->subtitle); ?>

                            </font>
                        </b>
                    </p>
                    <p>
                        <?php echo $pagesDynamic->content; ?>

                    </p>
                </div>
            </div>
        </hr>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>