<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('css/jquery-step.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('css/plugins/switchery/switchery.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('css/plugins/bootstrap-select.min.css')); ?>" rel="stylesheet">
<style>
.wizard > .content > .body{
    position: relative !important;
}
strong{
    padding-right: 30px;
}
</style>
<?php $__env->stopSection(); ?>
<div>
    <h3>Personal Information</h3>
    <section class="form">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <?php echo Form::label('name', 'Full Name:', []); ?>

                    <?php echo Form::text('name', null, ['class' => 'form-control required']); ?>

                    
                </div>
                <div class="form-group">
                    <?php echo Form::label('phone', 'Phone Number:', []); ?>

                    <?php echo Form::text('phone', null, ['class' => 'form-control required']); ?>

                    
                </div>
                <div class="form-group">
                    <?php echo Form::label('country', 'Country:', []); ?>

                    <?php echo Form::select('country', $countries, null, ['class' => 'form-control', 'placholder' => 'Select City']); ?>

                    
                </div>
                <div class="form-group">
                    <?php echo Form::label('password', 'Password:', []); ?>

                    
                    <?php if($formType == 'edit'): ?>
                    <div class="input-group">
                        <?php echo Form::password('password', ['class' => 'form-control required']); ?>

                        <span class="input-group-btn"> 
                            <button type="button" class="btn btn-warning pull-right" id="reset-pwd">Reset</button>
                        </span>
                    </div>    
                    <?php else: ?>
                    <?php echo Form::password('password', ['class' => 'form-control required']); ?>

                    <?php endif; ?>
                    
                </div>
                <div class="form-group">
                    <?php echo Form::label('username', 'Username Instagram:', []); ?>

                    <?php echo Form::text('username', ($formType == 'edit') ? !empty($user->detailInfluencer->username) ? $user->detailInfluencer->username : null : null, ['class' => 'form-control required']); ?>

                    <input type="hidden" id="followers_ig" name="followers_ig" value="<?php echo e(($formType == 'edit') ? !empty($user->detailInfluencer->followers_ig) ? $user->detailInfluencer->followers_ig : '' : ''); ?>">
                    <input type="hidden" id="profile_picture_ig" name="profile_picture_ig" value="<?php echo e(($formType == 'edit') ? !empty($user->detailInfluencer->profile_picture_ig) ? $user->detailInfluencer->profile_picture_ig : '' : ''); ?>">
                    <br>
                    <button type="button" class="btn btn-primary pull-right" id="btn-check">Check</button>                                            
                </div>
                <div class="form-group" id="instagram-profile">
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <?php echo Form::label('category', 'Influencer Category:', []); ?>

                    <?php echo Form::select('category[]', $categories, ($formType == 'edit') ? !empty($user->detailInfluencer->category) ? explode(',', $user->detailInfluencer->category) : null : null, ['class' => 'form-control selectpicker required', 'multiple']); ?>

                    
                </div>
                <div class="form-group">
                    <?php echo Form::label('email', 'Email:', []); ?>

                    <?php echo Form::email('email', null, ['class' => 'form-control required']); ?>

                    
                </div>
                <div class="form-group">
                    <?php echo Form::label('gender', 'Gender:', []); ?>

                    <br>
                    <label class="radio-inline">
                        <input type="radio" name="gender" value="F" <?php echo e(($formType == 'edit' && $user->gender == 'F') ? 'checked' : ''); ?>>Female
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="gender" value="M" <?php echo e(($formType == 'edit' && $user->gender == 'M') ? 'checked' : ''); ?>>Male
                    </label>
                    
                </div>
                <div class="form-group">
                    <?php echo Form::label('city', 'Town/City:', []); ?>

                    <?php echo Form::select('city', $cities, null, ['class' => 'form-control selectpicker', 'placholder' => 'Select City', 'data-live-search' => 'true', 'data-size' => 7]); ?>

                    
                </div>
                <div class="form-group">
                    <?php echo Form::label('is_active', 'Is Active:', []); ?>

                    <br>
                    <?php echo Form::checkbox('is_active', null, ($formType == 'edit') ? !empty($user->detailInfluencer->is_active) ? $user->detailInfluencer->is_active : null : null,['class' => 'form-control js-switch required', 'data-switchery' => true]); ?>

                
                </div>
            </div>
        </div>
    </section>
    <h3>Rate Card</h3>
    <section class="form">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <?php echo Form::label('instagram_photo_posting_rate', 'Harga Posting Photo Instagram:', []); ?>

                    <?php echo Form::text('instagram_photo_posting_rate', ($formType == 'edit') ? !empty($user->detailInfluencer->instagram_photo_posting_rate) ? $user->detailInfluencer->instagram_photo_posting_rate : null : null, ['class' => 'form-control']); ?>

                    
                </div>
                <div class="form-group">
                    <?php echo Form::label('instagram_video_posting_rate', 'Harga Posting Video Instagram:', []); ?>

                    <?php echo Form::text('instagram_video_posting_rate', ($formType == 'edit') ? !empty($user->detailInfluencer->instagram_video_posting_rate) ? $user->detailInfluencer->instagram_video_posting_rate : null : null, ['class' => 'form-control']); ?>

                    
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <?php echo Form::label('instagram_story_posting_rate', 'Harga Posting Story Instagram:', []); ?>

                    <?php echo Form::text('instagram_story_posting_rate', ($formType == 'edit') ? !empty($user->detailInfluencer->instagram_story_posting_rate) ? $user->detailInfluencer->instagram_story_posting_rate : null : null, ['class' => 'form-control']); ?>

                    
                </div>
                <div class="form-group">
                    <?php echo Form::label('youtube_posting_rate', 'Harga Posting Youtube:', []); ?>

                    <?php echo Form::text('youtube_posting_rate', ($formType == 'edit') ? !empty($user->detailInfluencer->youtube_posting_rate) ? $user->detailInfluencer->youtube_posting_rate : null : null, ['class' => 'form-control']); ?>

                    
                </div>
            </div>

            <div class="col-lg-6">
                <div class="form-group">
                    <?php echo Form::label('ig_highlight_rate', 'Harga Instagram Highlight:', []); ?>

                    <?php echo Form::text('ig_highlight_rate', ($formType == 'edit') ? !empty($user->detailInfluencer->ig_highlight_rate) ? $user->detailInfluencer->ig_highlight_rate : null : null, ['class' => 'form-control']); ?>

                    
                </div>
            </div>
            
            <?php if(auth()->user()->hasRole('admin')): ?>
            <div class="col-lg-6">
                <div class="form-group">
                    <?php echo Form::label('package_rate', 'Harga Package:', []); ?>

                    <?php echo Form::text('package_rate', ($formType == 'edit') ? !empty($user->detailInfluencer->package_rate) ? $user->detailInfluencer->package_rate : null : null, ['class' => 'form-control']); ?>

                    
                </div>
            </div>
            <?php endif; ?>
            
        </div>
    </section>
    <h3>Payment Information</h3>
    <section>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <?php echo Form::label('bank_id', 'Bank:', []); ?>

                    
                    <?php echo Form::select('bank_id', $banks, ($formType == 'edit') ? !empty($user->detailInfluencer->bank_id) ? $user->detailInfluencer->bank_id : null : null, ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => 7]); ?>

                </div>
                <div class="form-group">
                    <?php echo Form::label('bank_account_number', 'Bank Account Number:', []); ?>

                    <?php echo Form::text('bank_account_number', ($formType == 'edit') ? !empty($user->detailInfluencer->bank_account_number) ? $user->detailInfluencer->bank_account_number : null : null, ['class' => 'form-control required']); ?>

                    
                </div>
                <div class="form-group">
                    <?php echo Form::label('bank_holder_name', 'Bank Holder Name:', []); ?>

                    <?php echo Form::text('bank_holder_name', ($formType == 'edit') ? !empty($user->detailInfluencer->bank_holder_name) ? $user->detailInfluencer->bank_holder_name : null : null, ['class' => 'form-control required']); ?>

                    
                </div>
                <div class="form-group">
                    <?php echo Form::label('bank_location', 'Bank Location:', []); ?>

                    <?php echo Form::text('bank_location', ($formType == 'edit') ? !empty($user->detailInfluencer->bank_location) ? $user->detailInfluencer->bank_location : null : null, ['class' => 'form-control']); ?>

                    
                </div>
                <div class="form-group">
                    <?php echo Form::label('npwp', 'NPWP:', []); ?>

                    <?php echo Form::text('npwp', ($formType == 'edit') ? !empty($user->detailInfluencer->npwp) ? $user->detailInfluencer->npwp : null : null, ['class' => 'form-control required']); ?>

                    
                </div>
            </div>
        </div>
    </section>
</div>

<?php $__env->startSection('scripts'); ?>
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.js"></script>
    <script src="<?php echo e(asset('js/plugins/switchery/switchery.js')); ?>"></script>
    <script src="<?php echo e(asset('js/plugins/bootstrap-select.min.js')); ?>"></script>
    <script src="<?php echo e(asset('js/plugins/typeahed.min.js')); ?>"></script>
    
    <script>
        $(document).ready(function(){
            var elem = document.querySelector('.js-switch');
            var switchery = new Switchery(elem, { disabled: true });
            
    
            $('#btn-check').click(function(){
                $.get( "<?php echo e(route('influencer.instagram.check')); ?>", { username:  $('#username').val()}, function(data) {
                    if(data.result == false){
                        $('#instagram-profile').prepend(
                            '<div class="alert alert-danger">Invalid Instagram Username</div>'
                        );
    
                        setTimeout(() => {
                            $('.alert-danger').remove();
                        }, 5000);
                        
                    } else {
                        $('#username').val(data.result.userName);
                        $('#followers_ig').val(data.result.followers);
                        $('#profile_picture_ig').val(data.result.profilePicture);
                        $('#instagram-profile').empty();
                        $('#instagram-profile').append(
                            '<div class="feed-element">'+
                                '<a href="#" class="pull-left">'+
                                    '<img alt="image" class="img-circle" src="'+data.result.profilePicture+'" style="height:100px;width:100px;">'+
                                '</a>'+
                                '<div class="media-body">'+
                                    '<h3>'+data.result.userName+'</h3> <br>'+
                                    '<strong>'+data.result.mediaCount+' post</strong><strong>'+data.result.followers+' followers</strong><strong>'+ data.result.following +' following</small></strong>'+
                                '</div>'+
                            '</div>'
                        );
                    }
                });
                $("#example-form").find("a:contains('Next')").show()
            });
            <?php if($formType == 'create'): ?>
            $("#example-form").find("a:contains('Next')").hide()
            <?php endif; ?>
        });

        var form = $("#example-form");
    
        form.children("div").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            onStepChanging: function (event, currentIndex, newIndex)
            {
                if (currentIndex > newIndex)
                {
                    return true;
                }
                
                if (currentIndex < newIndex)
                {
                    // To remove error styles
                    form.find(".body:eq(" + newIndex + ") label.error").remove();
                    form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
                }

                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onFinishing: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled";
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                form.submit();
            }
        });    
    
    <?php if($formType == 'edit'): ?>
    $('#password').attr('disabled', true);
    $('#reset-pwd').click(function(){
        $('#password').val('');
        $('#password').attr('disabled', false);
    });
    <?php endif; ?>
    </script>
<?php $__env->stopSection(); ?>