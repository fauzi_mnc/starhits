<?php $__env->startSection('breadcrumb'); ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Platforms</h2>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentCrud'); ?>
<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content text-center p-md">

                    <h2>Increase your audience by connecting to more video channels.</h2>

                    <p>
                        You can set the default preferences for each platform. Alternatively, the system will automatically retrieve data you have previously entered on the respective platforms.
                    </p>


                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="contact-box">
                <div class="col-sm-2">
                    <div class="text-center">
                        <img alt="image" class="img-circle m-t-xs img-responsive" src="<?php echo e(asset('/images/yts.png')); ?>">
                        <div class="m-t-xs font-bold">Youtube</div>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="col-sm-10 yutub">
                        <p>Launched in May 2005, YouTube allows billions of people to discover, watch and share originally-created videos. YouTube provides a forum for people to connect, inform, and inspire others across the globe and acts as a distribution platform for original content creators and advertisers large and small</p>
                        <br>
                        <div class="text-center">
                            <a href="#" class="btn btn-md btn-warning m-t-n-xs" id='byutub'><strong><span id="txyutub">Connect</span></strong></a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- <div class="col-lg-12">
            <div class="contact-box">
                <div class="col-sm-2">
                    <div class="text-center">
                        <img alt="image" class="img-circle m-t-xs img-responsive" src="<?php echo e(asset('/images/fb.png')); ?>">
                        <div class="m-t-xs font-bold">Facebook</div>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="col-sm-10 facebook">
                        <p>Facebook is a social networking site that makes it easy for you to connect and share with your family and friends online. Originally designed for college students, Facebook was created in 2004 by Mark Zuckerberg while he was enrolled at Harvard University. By 2006, anyone over the age of 13 with a valid email address could join Facebook. Today, Facebook is the world's largest social network, with more than 1 billion users worldwide.</p>
                        <br>
                        <div class="text-center">
                            <a href="<?php echo e(route('platforms.log', 'facebook')); ?>" class="btn btn-md btn-warning m-t-n-xs" id='bfacebook'><strong><span id="txfacebook">Connect</span></strong></a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div> -->
        <div class="col-lg-12">
            <div class="contact-box">
                <div class="col-sm-2">
                    <div class="text-center">
                        <img alt="image" class="img-circle m-t-xs img-responsive" src="<?php echo e(asset('/images/twt.jpg')); ?>">
                        <div class="m-t-xs font-bold">Twitter</div>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="col-sm-10 twitter">
                        <p>Twitter was created in March 2006 by Jack Dorsey, Noah Glass, Biz Stone, and Evan Williams and launched in July of that year. The service rapidly gained worldwide popularity. In 2012, more than 100 million users posted 340 million tweets a day, and the service handled an average of 1.6 billion search queries per day. In 2013, it was one of the ten most-visited websites and has been described as "the SMS of the Internet". As of 2016, Twitter had more than 319 million monthly active users. On the day of the 2016 U.S. presidential election, Twitter proved to be the largest source of breaking news, with 40 million election-related tweets sent by 10 p.m. (Eastern Time) that day.</p>
                        <br>
                        <div class="text-center">
                            <a href="<?php echo e(route('creator.platforms.log', 'twitter')); ?>" class="btn btn-md btn-warning m-t-n-xs" id='btwitter'><strong><span id="txtwitter">Connect</span></strong></a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="contact-box">
                <div class="col-sm-2">
                    <div class="text-center">
                        <img alt="image" class="img-circle m-t-xs img-responsive" src="<?php echo e(asset('/images/dailymotion.png')); ?>">
                        <div class="m-t-xs font-bold">Dailymotion</div>
                    </div>
                </div>
                <div class="col-sm-10">
                    <div class="col-sm-10 dailymotion">
                        <p>Dailymotion is a video-sharing technology platform. It is majority owned by Vivendi.[2] North American launch partners include BBC News, VICE, Bloomberg, Hearst Digital Media, and more.[3] Dailymotion is available worldwide, in 18 languages and 35 localised versions featuring local home pages and local content. It has more than 300 million unique monthly users.</p>
                        <br>
                        <div class="text-center">
                            <a href="<?php echo e(route('creator.platforms.dailymotion.login')); ?>" class="btn btn-md btn-warning m-t-n-xs" id='bdailymotion'><strong><span id="txdailymotion">Connect</span></strong></a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script src="https://unpkg.com/sweetalert2@7.21.1/dist/sweetalert2.all.js"></script>
<script>

    <?php if(isset($users['twitter'])): ?>
    $('.twitter p').replaceWith('<p><?php echo e($users["twitter"]->email); ?></p><p><?php echo e($users["twitter"]->name); ?></p><p>Total Video</p><a href="#">twitter_link</a>');
    $('#btwitter').addClass('btn-danger').removeClass('btn-warning');
    $('#btwitter').attr("href","<?php echo e(route('creator.platforms.logout', 'twitter')); ?>");
    $('#txtwitter').text('Disconnect');
    <?php endif; ?>
    <?php if(isset($users['youtube'])): ?>
    $('.yutub p').replaceWith('<p><?php echo e($users["youtube"]->email); ?></p><p><?php echo e($users["youtube"]->nickname); ?></p><p>Total Video</p><a href="#">youtube_link</a>');
    $('#byutub').addClass('btn-danger').removeClass('btn-warning');
    $('#byutub').attr("href","<?php echo e(route('creator.platforms.logout', 'youtube')); ?>");
    $('#txyutub').text('Disconnect');
    <?php else: ?>
    $('#byutub').click(function(e){
        swal({
            title: "Information",
            html: "Please make sure to verify your youtube account by click this link <a href='https://www.youtube.com/verify' target='_blank'>https://www.youtube.com/verify</a>!",
            type: "warning",
        })
        .then((result) => {
            
            window.location.replace("<?php echo e(route('creator.platforms.youtube.login')); ?>");
            
        });

        return false;        
    });
    <?php endif; ?>
    <?php if(isset($users['dailymotion'])): ?>
    $('.dailymotion p').replaceWith('<p><?php echo e($users["dailymotion"]->email); ?></p><p><?php echo e($users["dailymotion"]->nickname); ?></p><p>Total Video</p><a href="#">dailymotion_link</a>');
    $('#bdailymotion').addClass('btn-danger').removeClass('btn-warning');
    $('#bdailymotion').attr("href","<?php echo e(route('creator.platforms.logout', 'dailymotion')); ?>");
    $('#txdailymotion').text('Disconnect');
    <?php endif; ?>
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>