<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('css/fileinput.min.css')); ?>" rel="stylesheet">
<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
.btn-preview {
    margin-right: 15px;
}
</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumb'); ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Web Setting</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo e(route('config.index')); ?>">Config</a>
            </li>
            <li class="active">
                <strong>Edit</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentCrud'); ?>
<?php if($errors->any()): ?>
    <div class="alert alert-danger">
        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
<?php endif; ?>
<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Config</h5>
    </div>
    <div class="ibox-content">
    <?php echo Form::open(['route' => 'config.store', 'class' => 'form-horizontal', 'files' => true]); ?>

    
    <?php $__env->startSection('css'); ?>
        <link href="<?php echo e(asset('css/fileinput.min.css')); ?>" rel="stylesheet">

        <style>
        .kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
            margin: 0;
            padding: 0;
            border: none;
            box-shadow: none;
            text-align: center;
        }
        .kv-photo .file-input {
            display: table-cell;
            max-width: 220px;
        }
        .kv-reqd {
            color: red;
            font-family: monospace;
            font-weight: normal;
        }
        </style>
        <?php echo $__env->make('layouts.datatables_css', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php $__env->stopSection(); ?>
    <?php $__currentLoopData = $config; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $con): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php if($con->key == 'website_logo_header'): ?>
    <!-- Picture Field -->
    <div class="form-group">
        <?php echo Form::label('value', $con->key, ['class' => 'col-sm-2 control-label']); ?>


        <div class="col-sm-10">
            <div class="kv-photo center-block text-center">
                <input id="photo" name="<?php echo e($con->key); ?>" type="file" class="file-loading">
            </div>

            <div id="kv-photo-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
        </div>
    </div>
    <?php elseif($con->key == 'url_type'): ?>
    <div class="form-group">
        <?php echo Form::label('type', 'URL Type:', ['class' => 'col-sm-2 control-label']); ?>


        <div class="col-sm-10">
            <?php echo Form::select('url_type', ['0' => 'Youtube', '1' => 'Metube'], isset($con->value) ? $con->value : '', ['placeholder' => 'Select Type', 'class' => 'form-control', 'required' => 'required']); ?>

        </div>
    </div>
    <?php elseif($con->key == 'website_streaming_url'): ?>
    <div class="form-group">
        <?php echo Form::label('value', 'URL Live Streaming', ['class' => 'col-sm-2 control-label']); ?>


        <div class="col-sm-10">
            <?php echo Form::textarea($con->key, $con->value, ['class' => 'form-control', 'maxlength' => 300]); ?>

        </div>
    </div>
    <?php elseif($con->key == 'is_streaming'): ?>
    <div class="form-group">
        <?php echo Form::label('is_streaming', 'Is Streaming:', ['class' => 'col-sm-2 control-label']); ?>


        <div class="col-sm-10">
            <label class="checkbox-inline">
                <?php echo Form::checkbox('is_streaming', '1', isset($con->value) ? true : false); ?> <span style="visibility: hidden;">a</span>
            </label>
        </div>
    </div>
    <?php else: ?>
    <div class="form-group">
        <?php echo Form::label('value', $con->key, ['class' => 'col-sm-2 control-label']); ?>


        <div class="col-sm-10">
            <?php echo Form::textarea($con->key, $con->value, ['class' => 'form-control', 'maxlength' => 300]); ?>

        </div>
    </div>
    <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <!-- Submit Field -->
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-2">
        <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

        <a href="<?php echo route('config.index'); ?>" class="btn btn-default">Cancel</a>
        </div>
    </div>
                <?php echo Form::close(); ?>

                </div>
            </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/piexif.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/sortable.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/purify.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/plugins/bootstrap-fileinput/fileinput.min.js')); ?>"></script>
<script>

    <?php $__currentLoopData = $config; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $con): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php if($con->key == 'website_logo_header'): ?>
        $('#photo').val('');

        $("#photo").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
            removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-photo-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            defaultPreviewContent: '<img src="<?php echo e(($con->value) ? $con->value : asset('img/default_avatar_male.jpg')); ?>" alt="Your icon" style="width:160px">',
            layoutTemplates: {main2: '{preview} {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png"]
        });
    <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>