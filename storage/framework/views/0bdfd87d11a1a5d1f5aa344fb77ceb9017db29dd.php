<?php $__env->startPush('meta'); ?>    
        <meta property="og:url" content="<?php echo e(url()->current()); ?>" />
        <meta property="og:type" content="#" />
        <meta property="og:title" content="<?php if(isset($models['website_title'])): ?><?php echo e($models['website_title']); ?><?php endif; ?>" />
        <meta property="og:description" content="<?php if(isset($models['website_slogan'])): ?><?php echo e($models['website_slogan']); ?><?php endif; ?>" />
        <meta property="og:image" content="<?php echo e(url($models['website_logo_header'])); ?>" />
        <meta property="fb:app_id" content="952765028218113" />
<?php $__env->stopPush(); ?>
<!-- Stored in resources/views/child.blade.php -->


<?php $__env->startSection('title', 'Contact us'); ?>

<?php $__env->startSection('content'); ?>

<div class="container-fluid contact">
    <div class="row">
        <div class="content-text">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        CONTACT US
                    </h7>
                </div>
                <div class="inquiry-form">
                    <div class="col-sm-6 left-side">
                        <h3 class="head">
                            What can we help?
                        </h3>
                        <p class="info">
                            <?php if(isset($models['website_title_contact'])): ?>
                                <?php echo e($models['website_title_contact']); ?>

                            <?php endif; ?>
                        </p>
                        <h7 class="info-name">
                            ADDRESS
                        </h7>
                        <h6 class="info-details">
                            <?php if(isset($models['website_address'])): ?>
                                <?php echo $models['website_address']; ?>

                            <?php endif; ?>
                        </h6>
                        <br>
                            <h7 class="info-name">
                                PHONE
                            </h7>
                            <h6 class="info-details">
                                <?php if(isset($models['website_phone'])): ?>
                                    <?php echo e($models['website_phone']); ?>

                                <?php endif; ?>
                            </h6>
                            <br>
                                <h7 class="info-name">
                                    EMAIL
                                </h7>
                                <h6 class="info-details">
                                    <?php if(isset($models['website_email'])): ?>
                                        <?php echo e($models['website_email']); ?>

                                    <?php endif; ?>
                                </h6>
                                <?php echo e($errors->contact->first('name')); ?>

                                <?php echo e($errors->contact->first('email')); ?>

                                <br>
                                <form action="/contact" method="post">
                                	<?php echo e(csrf_field()); ?>

                                    <div class="inquiry">
                                        <input class="inquiry-fname" id="fname" name="name" placeholder="    First Name" required="" style="font-family:Arial, FontAwesome" type="text">
                                            <input class="inquiry-lname" id="lname" name="last_name" placeholder="    Last Name" required="" style="font-family:Arial, FontAwesome" type="text">
                                                <input class="inquiry-email" id="email" name="email" placeholder="    Email" required="" style="font-family:Arial, FontAwesome" type="email">
                                                   <textarea class= "inquiry-message" name="message" placeholder=" Your message"></textarea>
                                                   <?php if(env('APP_ENV') != 'local'): ?> 
                                                   <div class="form-group">
                                                        <?php echo NoCaptcha::display(); ?>

                                                    </div>
                                                    <?php endif; ?>
                                                    <button class="inquiry-send" type="submit">
                                                        SEND
                                                    </button>
                                                </input>
                                            </input>
                                        </input>
                                    </div>
                                </form>
                                </br>
                            </br>
                        </br>
                    </div>
                    <div class="col-sm-6 right-side">
                        <div id="map">
                            <script>
                                function initMap() {
                                        var uluru = {lat:-6.194810, lng:106.767260};
                                        var map = new google.maps.Map(document.getElementById('map'), {
                                        zoom: 15,
                                        center: uluru
                                        });
                                        var marker = new google.maps.Marker({
                                        position: uluru,
                                        map: map
                                        });
                                        }
                            </script>
                            <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSzCvXcZP3lvGxY7Wf5jKurZEot4tdWSU&callback=initMap">
                            </script>
                        </div>
                        <br>
                            <br>
                            </br>
                        </br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>