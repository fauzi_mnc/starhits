<div class='btn-group'>
    <a href="<?php echo e(route('brand.show', $id)); ?>" class='btn btn-default btn-xs' title="Show Brand">
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="<?php echo e(route('brand.edit', $id)); ?>" class='btn btn-default btn-xs' title="Edit Brand">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
</div>