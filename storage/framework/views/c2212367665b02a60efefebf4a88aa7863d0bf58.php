<?php $__env->startSection('breadcrumb'); ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Bookmark</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Bookmark</a>
            </li>
            <li class="active">
                <strong>List</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentCrud'); ?>
<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Bookmark</h5>
                </div>

                <div class="ibox-content">
                    <div class="feed-activity-list">
                        <?php $__currentLoopData = $bookmarks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="feed-element">
                            <a href="#" class="pull-left">
                                <img alt="image" height="100" class="img" src="<?php echo e(asset($row->image)); ?>">
                            </a>
                            <div class="media-body ">
                                <small class="pull-right text-navy">
                                    <?php echo Form::open(['route' => ['creator.bookmark.destroy', $row->id], 'method' => 'delete']); ?>


                                        <?php echo Form::button('<i class="fa fa-star"></i>', [
                                            'type' => 'submit',
                                            'class' => 'btn btn-outline btn-warning  dim',
                                            'onclick' => "return confirm('Are you sure want to remove this bookmark?')"
                                        ]); ?>

                                    <?php echo Form::close(); ?>

                                </small>
                                <?php echo e($row->title); ?>

                            </div>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <?php echo e($bookmarks->links()); ?>

                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>