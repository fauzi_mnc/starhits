<?php $__env->startSection('breadcrumb'); ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Personal Setting</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Personal</a>
            </li>
            <li class="active">
                <strong>Setting</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('contentCrud'); ?>
<?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Personal</h5>
                </div>
                <div class="ibox-content">
                <?php if(auth()->user()->roles->first()->name === 'creator'): ?>
                <?php echo Form::model($user, ['route' => ['creator.personal.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]); ?>

                        <?php echo $__env->make('personal.show_fields', ['formType' => 'edit'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo Form::close(); ?>

                <?php elseif(auth()->user()->roles->first()->name === 'brand'): ?>
                <?php echo Form::model($user, ['route' => ['brand.personal.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]); ?>

                        <?php echo $__env->make('personal.show_fields', ['formType' => 'edit'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo Form::close(); ?>

                <?php else: ?>
                <?php echo Form::model($user, ['route' => ['admin.personal.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]); ?>

                        <?php echo $__env->make('personal.show_fields', ['formType' => 'edit'], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php echo Form::close(); ?>

                <?php endif; ?>
                </div>
            </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.crud', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>