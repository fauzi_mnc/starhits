<!doctype html>
<html amp lang="en">

<head>
    <title>
        @if ($__env->yieldContent('title')) @yield('title') - @endif @if(isset($SiteConfig['configs']['website_title'])) {{ $SiteConfig['configs']['website_title'] }} @endif
    </title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1,initial-scale=1, shrink-to-fit=no">
    <meta name="msapplication-square70x70logo" content="{{ asset('favicon.png') }}">
    <meta name="msapplication-square150x150logo" content="{{ asset('favicon.png') }}">
    <meta name="msapplication-wide310x150logo" content="{{ asset('favicon.png') }}">
    <meta name="msapplication-square310x310logo" content="{{ asset('favicon.png') }}">
    <meta property="og:url" content="{{ url()->current() }}" />
    <link rel="apple-touch-icon" href="{{ asset('favicon.png') }}">
    <link rel="apple-touch-startup-image" href="{{ asset('favicon.png') }}">
    <link rel="shortcut icon" href="{{ asset('favicon.png') }}" type="image/vnd.microsoft.icon">
    <link rel="shortcut icon" sizes="196x196" href="{{ asset('favicon.png') }}">
    <link rel="apple-touch-icon" href="{{ asset('favicon.png') }}">
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,900,900i&display=swap" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <style amp-custom>
        {{ include"frontend/assets/css/bootstrap.min.css" }}
        {{ include"frontend/assets/css/style.min.css" }}
    </style>
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>

    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <link rel="preload" as="script" href="https://cdn.ampproject.org/v0.js">
    <link rel="preconnect dns-prefetch" href="https://fonts.gstatic.com/" crossorigin>
    <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
    <script async custom-element="amp-selector" src="https://cdn.ampproject.org/v0/amp-selector-0.1.js"></script>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
    <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
    <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
    <script async custom-element="amp-animation" src="https://cdn.ampproject.org/v0/amp-animation-0.1.js"></script>
    <script async custom-element="amp-position-observer" src="https://cdn.ampproject.org/v0/amp-position-observer-0.1.js"></script>
    <script async custom-element="amp-social-share" src="https://cdn.ampproject.org/v0/amp-social-share-0.1.js"></script>
    <script custom-element="amp-accordion" src="https://cdn.ampproject.org/v0/amp-accordion-0.1.js" async=""></script>
    {{-- <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script> --}}
    <script async custom-element="amp-list" src="https://cdn.ampproject.org/v0/amp-list-0.1.js"></script>
    <script async custom-element="amp-live-list" src="https://cdn.ampproject.org/v0/amp-live-list-0.1.js"></script>
    <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.2.js"></script>
    <script async custom-element="amp-video" src="https://cdn.ampproject.org/v0/amp-video-0.1.js"></script>
    <script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
    <script async custom-element="amp-recaptcha-input" src="https://cdn.ampproject.org/v0/amp-recaptcha-input-0.1.js"></script>
    {!! NoCaptcha::renderJs() !!}

    <link rel="canonical" href="{{ Request::url() }}">
    @yield('meta')
</head>

<body>
    @yield('heading')

    @include('frontend.includes._menu')

    <div class="body">

        {{-- LOGO --}}
        @include('frontend.includes._logo')
        
        <div class="content">
        @if(Request::route()->getName() == 'creators' || Request::route()->getName() == 'series')
            <div class="row">
        @else
            <div class="container-fluid">
        @endif
                
                {{-- HEADER --}}
                @include('frontend.includes._header')

                {{-- CONTENT --}}
                @yield('content')
                
            </div>
        </div>

        {{-- FOOTER --}}
        @include('frontend.includes._footer')

    </div>

    <div class="scrolltop-wrap">
        <a href="#" role="button" aria-label="Scroll to top">
            &#x276E;
        </a>
    </div>

    {{-- <div class="floating_buttons">
        <a href="#" target="_blank">
            <amp-layout layout="fixed" width="24" height="24">
                <amp-img layout="responsive" width="1" height="1" src="frontend/assets/svgs/facebook-f.svg"></amp-img>
            </amp-layout>
        </a>
        <a href="#" target="_blank">
            <amp-layout layout="fixed" width="24" height="24">
                <amp-img layout="responsive" width="1" height="1" src="frontend/assets/svgs/twitter.svg"></amp-img>
            </amp-layout>
        </a>
        <a href="#" target="_blank">
            <amp-layout layout="fixed" width="24" height="24">
                <amp-img layout="responsive" width="1" height="1" src="frontend/assets/svgs/instagram.svg"></amp-img>
            </amp-layout>
        </a>
        <a href="#" target="_blank">
            <amp-layout layout="fixed" width="24" height="24">
                <amp-img layout="responsive" width="1" height="1" src="frontend/assets/svgs/envelope.svg"></amp-img>
            </amp-layout>
        </a>
    </div> --}}
</body>
</html>