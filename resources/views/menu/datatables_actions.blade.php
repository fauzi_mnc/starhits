<div class='btn-group'>
    <a href="{{ route('menu.edit', $id) }}" class='btn btn-default btn-xs' title="Edit Category">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
</div>