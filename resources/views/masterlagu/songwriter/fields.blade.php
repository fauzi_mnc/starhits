
@section('css')
<link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css">
@include('layouts.datatables_css')
<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@include('layouts.datatables_css')
@endsection
<div>
{!! Form::hidden('songwriter_id', ($formType=='edit') ? $songwriters->id_penyanyi : null, ['id' => 'songwriter_id'] ) !!}
<!-- Name Field -->
<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('name_songwriter', 'Name:', ['class' => 'col-sm-2 control-label']) !!}

        <div class="col-sm-10 col-lg-6">
            {!! Form::text('name_songwriter', ($formType=='edit') ? $songwriters->name : null, ['class' => 'form-control', 'autocomplete' => 'off', 'required']) !!}
        </div>
    </div>
</div>

<!-- Email Field -->
<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('email_songwriter', 'Email:', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-10 col-lg-6">
            {!! Form::email('email_songwriter', ($formType=='edit') ? $songwriters->email : null, ($formType=='edit') ? ['class' => 'form-control', 'autocomplete' => 'off', 'readonly'] : ['class' => 'form-control', 'maxlength' => 50, 'autocomplete' => 'off']) !!}
        </div>
    </div>
</div>

<!-- Password Field -->
<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('password_songwriter', 'Password:', ['class' => 'col-sm-2 control-label']) !!}

        <div class="col-sm-10 col-lg-6">
            {!! Form::password('password_songwriter' , array('class' => 'form-control', 'minlength' => 8, 'id' => 'password_songwriter')) !!}
        </div>
        @if($formType == 'edit')
        <div class="col-sm-2">
            <button type="button" id="reset-pwd" class="btn btn-default">Reset</button>
        </div>
        @endif
    </div>
</div>

<!-- Bank Field -->
<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('bank_id', 'Bank:', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-10 col-lg-6">
        {!! Form::select('bank_id', $banks, ($formType == 'edit') ? !empty($songwriters->bank_id) ? $songwriters->bank_id : null : null, ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => 7]) !!}
        </div>
    </div>
</div>

<!-- Bank Number Field -->
<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('bank_account_number', 'Bank Account Number:', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-10 col-lg-6">
        {!! Form::text('bank_account_number', ($formType == 'edit') ? !empty($songwriters->bank_account_number) ? $songwriters->bank_account_number : null : null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<!-- Bank Holder Name Field -->
<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('bank_holder_name', 'Bank Holder Name:', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-10 col-lg-6">
        {!! Form::text('bank_holder_name', ($formType == 'edit') ? !empty($songwriters->bank_holder_name) ? $songwriters->bank_holder_name : null : null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<!-- Bank Location Field -->
<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('bank_location', 'Bank Location:', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-10 col-lg-6">
        {!! Form::text('bank_location', ($formType == 'edit') ? !empty($songwriters->bank_location) ? $songwriters->bank_location : null : null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<!-- NPWP Field -->
<div class="col-md-12">
    <div class="form-group">
        {!! Form::label('npwp', 'NPWP:', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-10 col-lg-6">
        {!! Form::text('npwp', ($formType == 'edit') ? !empty($songwriters->npwp) ? $songwriters->npwp : null : null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<!-- Image Field -->
{{-- <div class="form-group">
    {!! Form::label('image', 'Image:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <div class="kv-photo center-block text-center">
            <input id="photo" name="image" type="file" class="file-loading">
        </div>

        <div id="kv-photo-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
    </div>
</div> --}}

<!-- Submit Field -->
<div class="col-md-12">
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-2">
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        @if(auth()->user()->roles->first()->name == 'admin')
        <a href="{!! route('mastersongwriter.index') !!}" class="btn btn-default">Cancel</a>
        @elseif(auth()->user()->roles->first()->name == 'legal')
        <a href="{!! route('legal.mastersongwriter.index') !!}" class="btn btn-default">Cancel</a>
        @elseif(auth()->user()->roles->first()->name == 'anr')
        <a href="{!! route('anr.mastersongwriter.index') !!}" class="btn btn-default">Cancel</a>
        @endif
        </div>
    </div>
</div>

</div>
@section('scripts')
<script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script>
    
    var formType = '{{ $formType }}';
    $('#photo').val('');

    $("#photo").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="{{ ($formType == 'edit' && $songwriters->image) ? asset($songwriters->image) : asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "jpeg"]
    });

    $("#cover").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-cover-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="{{ ($formType == 'edit' && $songwriters->cover) ? asset($songwriters->cover) : asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "jpeg"]
    });
    
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
@include('layouts.datatables_js')
<script>
    var table = $("#table-role").DataTable({"dom": 'tp'});
    @if($formType == 'edit')
    $('#password_songwriter').attr('disabled', true);
    $('#reset-pwd').click(function(){
        $('#password_songwriter').val('');
        $('#password_songwriter').attr('disabled', false);
    });
    @endif
    $('#roles').multiselect({
        onChange: function(element, checked) {
            if(checked){
                table.row.add( [ element.html() ] )
                .draw()
                .node();
            } else {
                var indexes = table.rows().eq( 0 ).filter( function (rowIdx) {
                    return table.cell( rowIdx, 0 ).data() === element.html() ? true : false;
                });
                
                table.rows( indexes ).remove().draw();
            }
        }
    });
    $(document).ready(function(){
        $('#typemail').change(function(){
            if($(this).val() == 1){
                $('#emailsongwriter').hide();
            }else if($(this).val() == 2) {
                $('#emailsongwriter').show();
            }else{
                $('#emailsongwriter').hide();
            }
        });
    }); 
</script>

@endsection