@if(auth()->user()->roles->first()->name == 'admin')
<div class='btn-group'>
    <a href="{{ route('mastersongwriter.edit', $s->id) }}" data-url="{{ route('mastersongwriter.edit', $s->id) }}" class='btn btn-primary btn-xs' title="Edit Master Songwriter">
        <i class="fa fa-pencil"></i> Edit
    </a>
    <a href="{{ route('mastersongwriter.destroy', $s->id) }}" class='btn btn-danger btn-xs' title="Delete Master Songwriter" onclick="return confirm('Are you sure to delete?')" style="margin-left:5px;">
        <i class="fa fa-trash"></i> Delete
    </a>
</div>
@elseif(auth()->user()->roles->first()->name == 'legal')
<div class='btn-group'>
    <a href="{{ route('legal.mastersongwriter.edit', $s->id) }}" data-url="{{ route('legal.mastersongwriter.edit', $s->id) }}" class='btn btn-primary btn-xs' title="Edit Master Songwriter">
        <i class="fa fa-pencil"></i> Edit
    </a>
    {{-- <a href="{{ route('legal.mastersongwriter.destroy', $s->id) }}" class='btn btn-danger btn-xs' title="Delete Master Songwriter" onclick="return confirm('Are you sure to delete?')" style="margin-left:5px;">
        <i class="fa fa-trash"></i> Delete
    </a> --}}
</div>
@elseif(auth()->user()->roles->first()->name == 'anr')
<div class='btn-group'>
    <a href="{{ route('anr.mastersongwriter.edit', $s->id) }}" data-url="{{ route('anr.mastersongwriter.edit', $s->id) }}" class='btn btn-primary btn-xs' title="Edit Master Songwriter">
        <i class="fa fa-pencil"></i> Edit
    </a>
    {{-- <a href="{{ route('legal.mastersongwriter.destroy', $s->id) }}" class='btn btn-danger btn-xs' title="Delete Master Songwriter" onclick="return confirm('Are you sure to delete?')" style="margin-left:5px;">
        <i class="fa fa-trash"></i> Delete
    </a> --}}
</div>
@endif
