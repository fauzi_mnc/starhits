@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Master Songwriter / Publisher</h2>
        <ol class="breadcrumb">
            <li>
                @if(auth()->user()->roles->first()->name === 'admin')
                <a href="{{ route('mastersongwriter.index') }}">Master Songwriter / Publisher</a>
                @elseif(auth()->user()->roles->first()->name === 'legal')
                <a href="{{ route('legal.mastersongwriter.index') }}">Master Songwriter / Publisher</a>
                @endif
            </li>
            <li class="active">
                <strong>Edit</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Edit Master Songwriter / Publisher
            </div>
            <div class="panel-body">
                @if(auth()->user()->roles->first()->name == 'admin')
                {!! Form::model($songwriters, ['route' => ['mastersongwriter.update', $songwriters->id], 'method' => 'put', 'class' => 'form-horizontal']) !!}
                @elseif(auth()->user()->roles->first()->name == 'legal')
                {!! Form::model($songwriters, ['route' => ['legal.mastersongwriter.update', $songwriters->id], 'method' => 'put', 'class' => 'form-horizontal']) !!}
                @elseif(auth()->user()->roles->first()->name == 'anr')
                {!! Form::model($songwriters, ['route' => ['anr.mastersongwriter.update', $songwriters->id], 'method' => 'put', 'class' => 'form-horizontal']) !!}
                @endif
                @include('masterlagu.songwriter.fields', ['formType' => 'edit'])
                {!! Form::close() !!}
                </form>
            </div>

        </div>
        
    </div>
</div>
@endsection