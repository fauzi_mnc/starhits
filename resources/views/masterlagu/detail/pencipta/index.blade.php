@extends('layouts.crud')

@section('breadcrumb')
@php
    if(auth()->user()->roles->first()->name == 'admin'){
        $route = "masterlagu.index";
    }elseif(auth()->user()->roles->first()->name == 'legal'){
        $route = "legal.masterlagu.index";
    }elseif(auth()->user()->roles->first()->name == 'anr'){
        $route = "anr.masterlagu.index";
    }elseif(auth()->user()->roles->first()->name == 'creator'){
        $route = "creator.masterlagu.index";
    }elseif(auth()->user()->roles->first()->name == 'singer'){
        $route = "singer.masterlagu.index";
    }elseif(auth()->user()->roles->first()->name == 'legal'){
        $route = "songwriter.masterlagu.index";
    }
@endphp
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-file-audio-o"></i> Master Lagu</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route($route) }}">Master Lagu</a>
            </li>
            <li class="active">
                <strong>Data Lagu Pencipta / Publishing : {{ $songwriter->name_master }}</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Daftar Lagu Pencipta / Publishing : {{ $songwriter->name_master }}</h5>
                </div>
                <div class="ibox-content">
                        {{-- <form style="margin-bottom: 10px;" class="form form-horizontal" role="form">
                            <div class="form-body">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <select name="album" id="album">
                                            <option value="">Filter By Album</option>
                                                @foreach($songwriter->rolelagu AS $row)
                                                    <option value="{{ $row->masterlagu->release_title }}">{{ $row->masterlagu->release_title }}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                            {!! Form::select('judul', $titles, null, ['class' => 'form-control', 'id' => 'judul', 'onchange'=>'this.form.submit()']) !!}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="pull-right">
                                        <button type="button" class="btn btn-default clear-filtering"><i class="fa fa-undo"></i> Clear Filtering</button>
                                        <button type="button" class="btn btn-success btn-search"><i class="fa fa-search"></i> Search</button>
                                    </div>
                                </div>
                            </div>
                        </form> --}}
                    @include('masterlagu.detail.pencipta.table')
                </div>
            </div>
        </div>
    </div>
@endsection


