@if(auth()->user()->roles->first()->name == 'admin')
<div class='btn-group'>
    <a href="{{ route('masterlagu.edit', $id) }}" data-url="{{ route('masterlagu.edit', $id) }}" class='btn btn-primary btn-xs' title="Edit Master Lagu">
        <i class="fa fa-pencil"></i>
    </a>
    <a href="{{ route('masterlagu.destroy', $id) }}" class='btn btn-danger btn-xs' title="Delete Master Lagu" onclick="return confirm('Are you sure to delete?')" style="margin-left:5px;">
        <i class="fa fa-trash"></i>
    </a>
</div>
@elseif(auth()->user()->roles->first()->name == 'legal')
<div class='btn-group'>
    <a href="{{ route('masterlagu.edit', $id) }}" data-url="{{ route('masterlagu.edit', $id) }}" class='btn btn-primary btn-xs' title="Edit Master Lagu">
        <i class="fa fa-pencil"></i>
    </a>
    <a href="{{ route('masterlagu.destroy', $id) }}" class='btn btn-danger btn-xs' title="Delete Master Lagu" onclick="return confirm('Are you sure to delete?')" style="margin-left:5px;">
        <i class="fa fa-trash"></i>
    </a>
</div>
@endif
