@extends('layouts.crud')
@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Master Songwriter</h2>
        <ol class="breadcrumb">
            <li>
                @if(auth()->user()->roles->first()->name === 'admin')
                <a href="{{ route('masterlagu.index') }}">Master Songwriter</a>
                @elseif(auth()->user()->roles->first()->name === 'legal')
                <a href="{{ route('legal.masterlagu.index') }}">Master Songwriter</a>
                @elseif(auth()->user()->roles->first()->name === 'anr')
                <a href="{{ route('anr.masterlagu.index') }}">Master Songwriter</a>
                @endif
            </li>
            <li class="active">
                <strong>Create</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Create Master Songwriter
                </div>
                <div class="panel-body" >
                    @if(auth()->user()->roles->first()->name == 'admin')
                    {!! Form::open(['route' => ['masterlagu.store'], 'files' => true, 'class' => 'form-horizontal', 'id' => 'example-form']) !!}
                    @elseif(auth()->user()->roles->first()->name == 'legal')
                    {!! Form::open(['route' => ['legal.masterlagu.store'], 'files' => true, 'class' => 'form-horizontal', 'id' => 'example-form']) !!}
                    @elseif(auth()->user()->roles->first()->name == 'anr')
                    {!! Form::open(['route' => ['anr.masterlagu.store'], 'files' => true, 'class' => 'form-horizontal', 'id' => 'example-form']) !!}
                    @endif
                    @include('masterlagu.fields', ['formType' => 'create'])
                </div>
            </div>
        </div>
    </div>
@endsection