@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Payment Setting</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Payment</a>
            </li>
            <li class="active">
                <strong>Setting</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@include('flash::message')
@section('contentCrud')

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Payment</h5>
                </div>
                <div class="ibox-content">
                {!! Form::model($user, ['route' => ['payment.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) !!}
                        @include('payment.show_fields', ['formType' => 'edit'])
                    {!! Form::close() !!}
                </div>
            </div>
@endsection
