@extends('layouts.crud')

@section('css')
    <link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css">
    @include('layouts.datatables_css')
    <style>
    .kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
        margin: 0;
        padding: 0;
        border: none;
        box-shadow: none;
        text-align: center;
    }
    .kv-photo .file-input {
        display: table-cell;
        max-width: 220px;
    }
    .kv-reqd {
        color: red;
        font-family: monospace;
        font-weight: normal;
    }
    </style>
    @include('layouts.datatables_css')
@endsection

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Add Account</h2>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins ">
                <div class="ibox-title">
                    <h5>Add Account</h5>
                </div>
                <div class="ibox-content">
                    <form id="form-add-account" class="form-horizontal" method="POST" action="{{url('')}}/{{ auth()->user()->roles()->where('name', request()->segment(1))->first()->name }}/post/addaccount">
                        {{ csrf_field() }}
                        <div class="row">
                        <div class="col-md-6">
                            <label for="creator_parent">Parent Creator</label>
                            <div>
                                <select id="creator_parent" type="text" class="form-control" name="creator_parent" required>
                                    <option value="">None Selected</option>
                                    @foreach ($creators as $d)
                                        <option value="{{ $d->id }}">{{ $d->name_channel }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="creator_child">Child Creator</label>
                            <div>
                                <select name="creator_child[]" id="creator_child"  class="form-control block" multiple="multiple">
                                    @foreach ($creators as $d)
                                        <option value="{{ $d->id }}">{{ $d->name_channel }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div style="margin:30px auto;">
                                <button type="submit" class="btn btn-primary">Process</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
    @include('layouts.datatables_js')
    <script>
        var table1 = $("#table1").DataTable({"dom": 'tp'});
        var table2 = $("#table2").DataTable({"dom": 'tp'});
        $('#creator_parent').multiselect({
            inheritClass: true,
            enableFiltering: true,
            filterBehavior: 'text',
            onChange: function(element1, checked1) {
    
                console.log(element1);
    
                if(checked1){
                    table1.row.add( [ element1.html() ] )
                    .draw()
                    .node();
                } else {
                    var indexes = table1.rows().eq( 0 ).filter( function (rowIdx) {
                        return table1.cell( rowIdx, 0 ).data() === element1.html() ? true : false;
                    });
                    
                    table1.rows( indexes ).remove().draw();
                }
            }
        });
        $('#creator_child').multiselect({
            dropRight: true,
            inheritClass: true,
            enableFiltering: true,
            filterBehavior: 'text',
            onChange: function(element2, checked2) {
    
                console.log(element2);
    
                if(checked2){
                    table2.row.add( [ element2.html() ] )
                    .draw()
                    .node();
                } else {
                    var indexes = table2.rows().eq( 0 ).filter( function (rowIdx) {
                        return table2.cell( rowIdx, 0 ).data() === element2.html() ? true : false;
                    });
                    
                    table2.rows( indexes ).remove().draw();
                }
            }
        }); 
    </script>
@endsection