
@section('css')
<link href="{{ asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet">
<link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">

<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@include('layouts.datatables_css')
@endsection

@if(auth()->user()->roles->first()->name === 'brand')
<!-- Title Field -->
<div class="form-group">
    {!! Form::label('brand-name', 'Brand Name:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('brand_name', null, ['class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>

<!-- PIC Field -->
<div class="form-group">
    {!! Form::label('pic-name', 'PIC:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('name', null, ['class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('email', 'Email Address:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::email('email', null, ['class' => 'form-control', 'disabled' => true, 'readonly' => ($formType == 'edit') ? true : false]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone Number:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('phone', null, ['class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>
@if($formType == 'edit')
<!-- Password Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-8">
        {!! Form::text('password', null, ['class' => 'form-control', 'disabled' => true]) !!}
    </div>
    
    <div class="col-sm-2">
        <button type="button" id="reset-pwd" class="btn btn-default">Reset</button>
    </div>
    
</div>
@else
<!-- Title Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('password', null, ['class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>
@endif
<!-- Title Field -->
<div class="form-group">
    {!! Form::label('saldo', 'Saldo:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <div class="input-group m-b">
            <span class="input-group-addon">Rp</span>
            {!! Form::text('saldo', null, ['class' => 'form-control', 'readonly' => true]) !!}
        </div>
        
    </div>
</div>
@else
<!-- Picture Field -->
<div class="form-group">
    {!! Form::label('image', 'Photo:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <div class="kv-photo center-block text-center">
            <input id="photo" name="image" type="file" class="file-loading" disabled>
        </div>

        <div id="kv-photo-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
    </div>
</div>
<!-- Picture Field -->
<div class="form-group">
    {!! Form::label('cover', 'Background Image:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <div class="kv-photo center-block text-center">
            <input id="cover" name="cover" type="file" class="file-loading" disabled>
        </div>

        <div id="kv-photo-errors-2" class="center-block alert alert-block alert-danger" style="display:none"></div>
    </div>
</div>
<!-- Title Field -->
<div class="form-group">
    {!! Form::label('full-name', 'Full Name:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('name', null, ['class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('gender', 'Gender:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('gender', ['M' => 'Male', 'F' => 'Female'], null, ['class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('email', 'Email Address:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('email', null, ['class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('dob', 'Date of Birth:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('dob', isset($user->dob) ? $user->dob->format('d-m-Y') : '', ['class' => 'form-control dateinput', 'disabled' => true]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone Number:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('phone', null, ['class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('country', 'Country:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('country', null, ['class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('city', 'City:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('city', isset($cities) ? $cities->name : null , ['class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group">
    {!! Form::label('ktp', 'KTP/SIM:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('ktp', null, ['class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>

<!-- Sub Title Field -->
<div class="form-group">
    {!! Form::label('biodata', 'Biodata:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::textarea('biodata', null, ['class' => 'form-control', 'disabled' => true]) !!}
    </div>
</div>
@endif
<!-- Submit Field -->
<div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
        <a href="{{url('')}}/{{auth()->user()->roles->first()->name}}/personal/{{$user->id}}/edit" class="btn btn-default">Edit</a>
    </div>
</div>

@section('scripts')
<script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/tinymce.min.js') }}"></script>
<script>

    var formType = '{{ $formType }}';
    $('#photo').val('');
    $('.dateinput').datepicker({
        format: 'dd-mm-yyyy'
    });

    $("#photo").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="{{ ($formType == 'edit' && $user->image) ? asset($user->image) : asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png"]
    });

    $("#cover").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-2',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="{{ ($formType == 'edit' && $user->cover) ? asset($user->cover) : asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png"]
    });
    
</script>
@if($formType == 'edit')
@include('layouts.datatables_js')
<script>
    $.fn.dataTable.ext.buttons.create = {
        action: function (e, dt, button, config) {
            $('#myModal').modal('show');
        }
    };

    var hash = document.location.hash;
    if (hash) {
        console.log(hash);
        $('.nav-tabs a[href="'+hash+'"]').tab('show')
    }

    // Change hash for page-reload
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
    window.location.hash = e.target.hash;
    });
</script>
@endif
@endsection