@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Personal Setting</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Personal</a>
            </li>
            <li class="active">
                <strong>Setting</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Personal</h5>
                </div>
                <div class="ibox-content">


                   
                @if(auth()->user()->roles->first()->name === 'creator')
                
                    {!! Form::model($user, ['route' => ['creator.personal.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) !!}
                            @include('personal.show_fields', ['formType' => 'edit'])
                    {!! Form::close() !!}
                @elseif(auth()->user()->roles->first()->name === 'brand')
                
                    {!! Form::model($user, ['route' => ['brand.personal.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) !!}
                            @include('personal.show_fields', ['formType' => 'edit'])
                    {!! Form::close() !!}
                @elseif(auth()->user()->roles->first()->name === 'user')
                
                    {!! Form::model($user, ['route' => ['user.personal.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) !!}
                            @include('personal.show_fields', ['formType' => 'edit'])
                    {!! Form::close() !!}  @elseif(auth()->user()->roles->first()->name === 'user')

                @elseif(auth()->user()->roles->first()->name === 'finance')
                    {!! Form::model($user, ['route' => ['finance.personal.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) !!}
                            @include('personal.show_fields', ['formType' => 'edit'])
                    {!! Form::close() !!}
                @elseif(auth()->user()->roles->first()->name === 'legal')
                    {!! Form::model($user, ['route' => ['legal.personal.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) !!}
                            @include('personal.show_fields', ['formType' => 'edit'])
                    {!! Form::close() !!}
                @else
                    {!! Form::model($user, ['route' => ['admin.personal.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) !!}
                            @include('personal.show_fields', ['formType' => 'edit'])
                    {!! Form::close() !!}
                @endif
                
                </div>
            </div>
@endsection
