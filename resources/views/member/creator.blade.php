@extends('layouts.crud')

@section('css')
<link href="{{ asset('css/plugins/switchery/switchery.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/bootstrap-select.min.css') }}" rel="stylesheet">
<style>
.wizard > .content > .body{
    position: relative !important;
}
strong{
    padding-right: 30px;
}
</style>
@endsection

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Member</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Member</a>
            </li>
            <li class="active">
                <strong>Ugrade</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Setting
            </div>
            <div class="panel-body">
                {!! Form::open(['route' => ['self.member.role.store', $user->id, $role], 'method' => 'post', 'class' => 'form', 'files' => true, 'id' => 'example-form']) !!}
                    <input id="image" name="image" type="hidden">
                    <input id="subscribers" name="subscribers" type="hidden">
                    <div class="form-group">
                            {!! Form::label('channel', 'Channel Youtube:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-10">
                                {!! Form::text('provider_id', null, ['class' => 'form-control', 'id' => 'channel']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('', '', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-10" id="youtube-profile">
                                
                                <button type="button" class="btn btn-default pull-right" id="btn-check">Connect</button>
                            </div>
                        </div>
                <button type="submit" class="btn btn-primary" id="btn-submit">Save Change</button>
                <a href="{{route('self.member.upgrade', [auth()->id()])}}" class="btn btn-default">Cancel</a>
                </form>
            </div>

        </div>
        
    </div>
</div>
@endsection


@section('scripts')
    <script src="{{ asset('js/plugins/switchery/switchery.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('js/plugins/typeahed.min.js') }}"></script>
    
    <script>
        $('#btn-submit').attr('disabled', true);

        $('#btn-check').click(function(){
            $.get( "{{route('creator.check')}}", { url:  $('#channel').val()}, function(data) {
                if(data.result == false){
                    $('#youtube-profile').prepend(
                        '<div class="alert alert-danger">Invalid Youtube Channel</div>'
                    );

                    setTimeout(() => {
                        $('.alert-danger').remove();
                    }, 5000);
                    
                } else {
                    $('#channel').val(data.result.id);
                    $('#youtube-profile').empty();
                    $('#youtube-profile').append(
                        '<div class="feed-element">'+
                            '<a href="#" class="pull-left">'+
                                '<img alt="image" class="img-circle" src="'+data.result.snippet.thumbnails.medium.url+'" style="height:100px;width:100px;">'+
                            '</a>'+
                            '<div class="media-body">'+
                                '<strong>'+data.result.snippet.title+'</strong> <br>'+
                                '<small class="text-muted">'+data.result.statistics.subscriberCount+' subscribers</small>'+
                            '</div>'+
                        '</div>'
                    );
                    $('#subscribers').val(data.result.statistics.subscriberCount);
                    $('#image').val(data.result.snippet.thumbnails.medium.url);
                    $('#btn-submit').attr('disabled', false);
                }
            });

        });
    </script>
@endsection