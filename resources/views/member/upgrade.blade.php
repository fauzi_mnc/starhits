@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Member</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Member</a>
            </li>
            <li class="active">
                <strong>Upgrade Account</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Upgrade Account
            </div>
            <div class="panel-body" style="text-align: center;">
                {!! Form::open(['route' => ['self.member.upgrade', $user->id], 'id' => 'example-form', 'files' => true]) !!}
                    <h3>Do You Want To Upgrade To?</h3>
                    <a href="{{ route('self.member.role', [$user->id, 'influencer']) }}" type="button" class="btn btn-outline btn-success">Influencer</a>
                    <a href="{{ route('self.member.role', [$user->id, 'creator']) }}" type="button" class="btn btn-outline btn-success">Creator</a>
                </form>
            </div>

        </div>
        
    </div>
</div>
@endsection