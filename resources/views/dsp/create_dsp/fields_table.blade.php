@section('css')
<link href="{{ asset('css/jquery-step.css') }}" rel="stylesheet">
<link href="{{ asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/plugins/bootstrap-select.min.css') }}" rel="stylesheet">
<style type="text/css">
    #btn-save{
        margin-top: 100px;
    }
</style>
@include('layouts.datatables_css')
@endsection
<div>
    <div class="form-group">
        <div class="col-sm-12 table-responsive">
            <table class="table table-bordered" id="table-dsp-detail">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Sales Month</th>
                        <th>Platform</th>
                        <th>Country</th>
                        <th>Label Name</th>
                        <th>Artist Name</th>
                        <th>Release Title</th>
                        <th>Track Title</th>
                        <th>UPC</th>
                        <th>ISRC</th>
                        <th>Release Catalog NB</th>
                        <th>Release Type</th>
                        <th>Sales Type</th>
                        <th>Quantity</th>
                        <th>Client Pay</th>
                        <th>Unit Price</th>
                        <th>Mechanic</th>
                        <th>Gross Revenue</th>
                        <th>Client Share</th>
                        <th>Net Revenue EUR</th>
                        <th>Net Revenue IDR</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    //var_dump($data);
                    ?>
                    @php 
                    $i = 1;
                    $no =0;
                    @endphp
                    @foreach($data as $row)
                        <tr>
                            <td>{!!$i++;!!}</td>
                            <td>{{date("d F Y", strtotime($row->sales_month))}}</td>
                            <td>{{$row->platform}}</td>
                            <td>{{$row->country}}</td>
                            <td>{{$row->label_name}}</td>
                            <td>{{$row->release_title}}</td>
                            <td>{{$row->track_title}}</td>
                            <td>{{$row->upc}}</td>
                            <td>{{$row->isrc}}</td>
                            <td>{{$row->release_catalog_nb}}</td>
                            <td>{{$row->release_type}}</td>
                            <td>{{$row->sales_type}}</td>
                            <td>{{$row->quantity}}</td>
                            <td>{!!number_format((float)$row->client_pay)!!},-</td>
                            <td>{!!number_format((float)$row->unit_price)!!},-</td>
                            <td>{!!number_format((float)$row->mechanic)!!},-</td>
                            <td>{!!number_format((float)$row->gross_revenue)!!},-</td>
                            <td>{!!number_format((float)$row->client_share)!!},-</td>
                            <td>{!!number_format((float)$row->net_revenue_eur)!!},-</td>
                            <td>{!!number_format((float)$row->net_revenue_idr)!!},-</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <button type="submit" class="btn btn-primary pull-right" id="btn-save">
        Save And Submit
    </button> 


</div>
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/piexif.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/purify.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/themes/fa/theme.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.js"></script>
<script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-select.min.js') }}"></script>
@include('layouts.datatables_js')
<script>
    function readCsv(){
    
    }

    $(document).ready(function(){
        $('.datepicker').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            //viewMode: 'months',
            // minViewMode: 'months',
            format: 'dd-mm-yyyy'
        });

        $('.monthpicker').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            viewMode: 'months',
            minViewMode: 'months',
            format: 'mm'
        });
    
        $('.yearpicker').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            viewMode: 'years',
            minViewMode: 'years',
            format: 'yyyy'
        });
        
        $('#type').change(function(){
            if($(this).val() == 1){
                $('#type-content').show();
            } else {
                $('#type-content').hide();
            }
        });
    
        $("#input-id").fileinput({
            uploadUrl: "#",
            showUpload: false,
            allowedFileExtensions: ["csv", 'xlsx'],
            fileActionSettings: {
                showUpload: false,
            }
        });
    
        $('#input-id').change(function(){
            if($(this).get(0).files.length === 0){
                $('#btn-process-csv').attr('disabled', true); 
            }else{
                $('#btn-process-csv').attr('disabled', false);
            }
        });
    
        $('#btn-process-csv').click(function(){
            readCsv();
        });
    
     
    });
    
   
    
    $(".actions").on("click","#btn-save",function(){
        $('#status').val(3);
        form.submit();
    });
    
    $(".actions").on("click","#btn-update",function(){
        $('#status').val(3);
        form.submit();
    });
    
    $('#btn-dsp').click(function(){
    form.validate().settings.ignore = ":disabled";
    
    if(form.valid()){
        table.row.add( [ index, $('#detail_id option:selected').html(), $('#estimate_gross_dsp').val(),'<button type="button" data-id="'+$('#detail_id').val()+'" class="btn btn-default edit">Edit</button>'] )
            .draw()
            .node();

        $('#content-dsp').append('<input type="hidden" name="dsp_detail[]" value="'+[$('#detail_id').val(), $('#estimate_gross_dsp').val()]+'">');
        $('.rm-detail').find('[value='+$('#detail_id').val()+']').remove();
        $('.rm-detail').selectpicker('refresh');
        $('#estimate_gross_dsp').val('');
        index++;
    }
    
    });
    

</script>
@endsection