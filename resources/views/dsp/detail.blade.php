@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Dsp</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Dsp</a>
            </li>
            <li class="active">
                <strong>View</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')
@include('flash::message')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Dsp Details
            </div>
            <div class="panel-body">
            	@section('css')
<style>
    .wizard > .content > .body{
    position: relative !important;
    }
    strong{
    padding-right: 30px;
    }
    .footer{
    position: relative;
    }
    .pagination>li{
    display: inline !important;
    }

    .borderless td, .borderless tr {
        border: none;
    }
    table.borderless td,table.borderless th{
         border: none !important;
    }
    .number{
        text-align:right;
    }
</style>
@include('layouts.datatables_css')
@endsection
    <section >
        <div class="col-lg-6">
            <table class="table borderless">
                <tr>
                    <td>Penyanyi</td>
                    <td> :
                        @php $countSing = 0;
                            $retSing = "";
                        @endphp             
                        @foreach($masterlagu->rolelagu as $key => $singer)
                            @if(!empty($singer->penyanyi))
                                <?php 
                                    $retSing .= $singer->penyanyi->name_master." & ";
                                ?>
                            @endif
                        @endforeach
                        <?php 
                            echo rtrim($retSing," & ");
                        ?>
                    
                    </td>
                </tr>
                <tr>
                    <td>Pencipta</td>
                    <td>: 
                        @php $countSong = 0;
                            $ret="";
                        @endphp     
                        @foreach($masterlagu->rolelagu as $keys => $song)
                            @if(!empty($song->pencipta))
                            <?php 
                                $ret .= $song->pencipta->name_master." & ";
                            ?>
                            @endif              
                        
                        @endforeach
                            <?php 
                                echo rtrim($ret," & ");
                            ?>
                        
                    </td>
                </tr>
                <tr>
                    <td>Judul Lagu</td>
                    <td>: {{$masterlagu->track_title}}</td>                    
                </tr>
                <tr>
                    <td>Album</td>
                    <td>: {{$masterlagu->release_title}}</td>
                </tr>
                <tr>
                    <td>Label</td>
                    <td>: {{$masterlagu->label_name}}</td>
                </tr>
            </table>
        </div>
        <hr>  
        @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance')|| auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal'))
            <div class="col-sm-3 pull-right">
                <a href="{{url('')}}/{{ auth()->user()->roles()->where('name', request()->segment(1))->first()->name }}/dsp/{{$dsp_id}}/dsp_detail_print/{{$masterlagu->isrc}}" class="btn btn-primary" style="margin-right:20px;" target="_blank"><i class="fa fa-print"></i> Print</a>
                <a href="{{url('')}}/{{ auth()->user()->roles()->where('name', request()->segment(1))->first()->name }}/dsp/{{$dsp_id}}/dsp_detail_print_fpp/{{$masterlagu->isrc}}" class="btn btn-primary" target="_blank"><i class="fa fa-print"></i> Print FPP</a>
            </div>	
        @endif

    </section>
    <br>
    <br>
    <br>

    <section >

            <div class="form-group">
                <div class="col-sm-12 table-responsive">
                    <table class="table table-bordered" id="table-dsp-detail">
                        <thead>
                            <tr >                                
                                <th>Platform</th>     
                                @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance')|| auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal') || auth()->user()->hasRole('singer'))                           
                                    <th>Revenue Penyanyi (Rp)</th>
                                @endif

                                @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance')||auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal')|| auth()->user()->hasRole('songwriter'))
                                    <th>Revenue Pencipta (Rp)</th>
                                @endif

                                @if(auth()->user()->hasRole('admin')|| auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal') || auth()->user()->hasRole('finance'))
                                    <th>Revenue (Rp)</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @php 
                                $no =0;
                                $totSing =0;
                                $totSong =0;
                                $totRev = 0;
                            @endphp
                            @foreach($dsp_detail as $key => $row)
                           		@php
                                    $net_revenue_idr = App\Model\DspDetails::where([
				                                    'isrc'=>$row->isrc,'dsp_id'=>$row->dsp_id,
				                                    'platform'=>$row->platform])
				                                    ->sum('net_revenue_idr');

				                	$revenue = round($net_revenue_idr,0);
				                	$sing = ($revenue*$masterlagu->percentage_penyanyi);
				                	$song = ($revenue*$masterlagu->percentage_pencipta);

                                    $totRev = $totRev+$revenue;
                                    $totSing = $totSing+$sing;
                                    $totSong = $totSong+$song;

                                @endphp
                                <tr>
                                    <td>{{$row->platform}}</td>   
                                     @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance') || auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal') || auth()->user()->hasRole('singer'))                                 
                                    <td class="number">{{number_format((float)$sing,0)}}</td>
                                    @endif

                                     @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance')|| auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal') || auth()->user()->hasRole('songwriter'))
                                    <td class="number">{{number_format((float)$song,0)}}</td>
                                    @endif

                                     @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal') || auth()->user()->hasRole('finance'))
                                    <td class="number">{{number_format((float)$revenue,0)}}</td>
                                    @endif

                                </tr>
                                @php $no ++; @endphp
                            @endforeach
                        </tbody>


                        <tfoot >
                            <tr style="font-weight: bold;">
                                <td >Total Revenue</td>
                                
                                 @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance')|| auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal') || auth()->user()->hasRole('singer'))
                                <td class="number">{{number_format((float)round($totSing,0),0)}}</td>
                                @endif

                                 @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance')|| auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal') || auth()->user()->hasRole('songwriter'))
                                <td class="number">{{number_format((float)round($totSong,0),0)}}</td>
                                @endif

                                 @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal') || auth()->user()->hasRole('finance'))
                                <td class="number">{{number_format((float)round($totRev,0),0)}}</td>
                                @endif
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
                <a href="{{url('')}}/{{ auth()->user()->roles()->where('name', request()->segment(1))->first()->name }}/dsp/{{$dsp_id}}/view" class="btn btn-success pull-right" id="btn-add-total">Back</a>
    </section>
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/piexif.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/purify.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/themes/fa/theme.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.js"></script>
<script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-select.min.js') }}"></script>
@include('layouts.datatables_js')
<script>
      
    $(document).ready(function(){
    	$('#table-dsp-detail').DataTable();
    	$("div").removeClass("ui-toolbar");
        $('.datepicker').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            //viewMode: 'months',
            // minViewMode: 'months',
            format: 'dd-mm-yyyy'
        });

        $('.monthpicker').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            viewMode: 'months',
            minViewMode: 'months',
            format: 'mm'
        });
    
        $('.yearpicker').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            viewMode: 'years',
            minViewMode: 'years',
            format: 'yyyy'
        });      
          
    });
    
</script>
@endsection

            </div>

        </div>
        
    </div>
</div>
@endsection