@section('css')
<link href="{{ asset('css/jquery-step.css') }}" rel="stylesheet">
<link href="{{ asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/plugins/bootstrap-select.min.css') }}" rel="stylesheet">
<style>
    .wizard > .content > .body{
    position: relative !important;
    }
    strong{
    padding-right: 30px;
    }
    .footer{
    position: relative;
    }
    .pagination>li{
    display: inline !important;
    }
    .number{
        text-align:right;
    }
</style>
@include('layouts.datatables_css')
@endsection
<div>
    @if($formType == 'create')
    <h3>Type Selection</h3>
    <section>
        <div class="form-group">
            <a href="{{route('dsp.template')}}" class="btn btn-primary pull-right" id="btn-template">Download Template</a>
        </div>
        <div class="form-group">
            {!! Form::label('reporting_month', 'Date:', ['class' => 'reporting_month col-sm-2 control-label']) !!}
            <div class="col-sm-4">
                {!! Form::text('reporting_month', null, ['class' => 'reporting_month form-control datepicker required']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('type', 'Input Dsp:', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10">
                {!! Form::select('type', ['' => 'Select', 1 => 'Upload Excel or CSV'], null, ['class' => 'form-control required']) !!}
            </div>
        </div>
        <div class="form-group" id="type-content" style="display:none">
            {!! Form::label('file', '', ['class' => 'col-sm-2 control-label']) !!}
            <div class="col-sm-10" >
                <input id="input-id" type="file" name="csv" data-preview-file-type="text" >
            </div>
        </div>
    </section>
    @endif
    @if($formType != 'view')
    <h3>Dsp Input</h3>
    @endif
    <section>
        <div>
            <div class="form-group">
                @if($formType != 'view' )
                {!! Form::label('reporting_month_a', 'Date:', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-4">
                    {!! Form::text('reporting_month_a', ($formType == 'view' ) ? date('d-m-Y', strtotime($dsp->reporting_month)) : null, ['class' => 'reporting_month_a form-control datepicker']) !!}
                </div>
                @endif
                @if(!empty($type))
                    @if($type == 'csv')
                        <input type="hidden" name="reupload" value="true">
                    @endif
                @endif
            </div>
            @if($formType == 'view')
                <div class="row" >
                    <div class="col-sm-4" style="margin-left: 50px;">
                        <h2> {{ date('d F Y', strtotime($dsp->reporting_month)) }} </h2><br>                     
                    </div>

                    @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance'))
                        <div class="col-sm-4">
                            <h2>Total Revenue : Rp. {{number_format((float)round($total_dsp,0),0)}},-</h2><br>  
                        </div>
                    @endif

                    @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance') || auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal'))
                    <div class="col-sm-2 pull-right">
                        <a href="{{url('')}}/{{ auth()->user()->roles()->where('name', request()->segment(1))->first()->name }}/dsp/printview/{{$dsp->id}}" class="btn btn-primary  btn-md btn-block" target="_blank"><i class="fa fa-print"></i> Print</a>
                        <br><br>
                    </div>
                    @endif
                </div>
                
                @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance') || auth()->user()->hasRole('anr')|| auth()->user()->hasRole('legal'))
                <div class="row">                    
                    <div class="col-sm-3" style="margin-left: 15px;">                    
                        <select class="form-control" name="cari_penyanyi" id="singer" placeholder="Select Penyanyi">
                            <option value="">- Select Penyanyi -</option>
                            @foreach($penyanyis as $singers)
                                <option value="{{$singers->name_master}}">{{$singers->name_master}}</option>
                            @endforeach
                        </select>
                        <br>
                    </div>                    
                </div>                          
                @endif    
            @endif

            @if(!empty($type))
                @if($type == 'csv')
                    <div class="form-group" id="type-content">
                        {!! Form::label('file', '', ['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-sm-10" >
                            <input id="input-id" type="file" name="csv" data-preview-file-type="text" >
                        </div>
                    </div>
                    <button type="button" class="btn btn-success pull-right" disabled id="btn-process-csv">Process CSV</button>
                @endif
            @endif
            <div id="content-dsp"></div>
            <div class="form-group">
                <div class="col-sm-12 table-responsive">
                    <table class="table table-bordered" id="table-dsp-detail">
                        <thead>
                            <tr>                                
                                <th>Track Title</th>
                                <th>Release Title</th>                               
                                <th>Singer</th>
                                
                                @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance')|| auth()->user()->hasRole('anr') || auth()->user()->hasRole('singer'))   
                                    <th>Revenue Penyanyi (Rp)</th>
                                @endif

                                @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance')|| auth()->user()->hasRole('anr')|| auth()->user()->hasRole('songwriter'))  
                                <th>Revenue Pencipta (Rp)</th>
                                @endif

                                @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance')|| auth()->user()->hasRole('anr'))
                                <th>Net Revenue (Rp)</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $totSing =0;
                                $totSong =0;
                                $totRev = 0;
                                $totRev2 = 0;
                            ?>
                            @if($formType == 'view' )
                                @php $no =0; @endphp
                                @foreach($all_dsp as $rows)

                                    @php
                                        $net_revenue = App\Model\DspDetails::where(['isrc'=>$rows->isrc,'dsp_id'=>$dsp->id])
                                        ->sum('net_revenue_idr');

                                        $totRev2 = $totRev2+$net_revenue;
                                    @endphp
                                    <tr>
                                        <td style="color: orange;"><b>{{$rows->track_title}}</b></td>
                                        <td>{{$rows->release_title}}</td>
                                        <td style="color: orange;">{{$rows->artist_name}}</td>

                                        @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance') || auth()->user()->hasRole('anr') || auth()->user()->hasRole('singer'))
                                            <td class="number">0</td>
                                        @endif
                                        @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance')|| auth()->user()->hasRole('anr')|| auth()->user()->hasRole('songwriter'))
                                            <td class="number">0</td>
                                        @endif

                                        @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('anr')  || auth()->user()->hasRole('finance') )
                                            <td class="number">{{number_format((float)round($net_revenue,2),0)}}</td>
                                        @endif
                                    </tr>
                                @endforeach

                                @foreach($masterlagu as $row)
                                    @php
                                        $net_revenue_idr = App\Model\DspDetails::where(['isrc'=>$row->isrc,'dsp_id'=>$dsp->id])
                                        ->sum('net_revenue_idr');

                                        $sing = ($net_revenue_idr*$row->percentage_penyanyi);
                                        $song = ($net_revenue_idr*$row->percentage_pencipta);

                                        $totSing = $totSing + $sing;
                                        $totSong = $totSong + $song;
                                        $totRev = $totRev+$net_revenue_idr;
                                    @endphp
                                    <tr>
                                        <td><a href="{{url('')}}/{{ auth()->user()->roles()->where('name', request()->segment(1))->first()->name }}/dsp/{{$dsp->id}}/dsp_detail/{{$row->isrc}}" style="text-decoration: underline;" ><b>{{$row->track_title}}</a></b></td>
                                        <td>{{$row->release_title}}</td>
                                        <td>
                                            @php
                                                $sing = "";
                                            @endphp 
                                            @foreach($row->rolelagu as $key => $singer)
                                                @if(!empty($singer->penyanyi))
                                                    @php 
                                                        $sing .= $singer->penyanyi->name_master." & ";
                                                    @endphp
                                                @endif
                                            @endforeach
                                            @php 
                                                echo rtrim($sing," & ");
                                            @endphp
                                        </td>

                                        @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance')|| auth()->user()->hasRole('anr') || auth()->user()->hasRole('singer'))
                                            <td class="number">{{number_format((float)round($sing,2),0)}}</td>
                                        @endif

                                        @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance') || auth()->user()->hasRole('anr') || auth()->user()->hasRole('songwriter'))   
                                            <td class="number">{{number_format((float)round($song,2),0)}}</td>
                                        @endif

                                        @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance') || auth()->user()->hasRole('anr') )   
                                            <td class="number">{{number_format((float)round($net_revenue_idr,2),0)}}</td>
                                        @endif
                                    </tr>
                                    @php $no ++; @endphp
                                @endforeach
                            @endif
                        </tbody>
                        @php
                           $totRev = $totRev+ $totRev2;
                        @endphp

                        
                        <tfoot>
                            <tr style="font-weight: bold;">
                                <td>Total</td>
                                <td></td>
                                <td></td>
                                @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance') || auth()->user()->hasRole('anr') || auth()->user()->hasRole('singer'))
                                    <td class="number">{{number_format((float)round($totSing,2),0)}}</td>
                                @endif

                                @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance')|| auth()->user()->hasRole('anr') || auth()->user()->hasRole('songwriter'))
                                    <td class="number">{{number_format((float)round($totSong,2),0)}}</td>
                                @endif

                                @if(auth()->user()->hasRole('admin') || auth()->user()->hasRole('finance') || auth()->user()->hasRole('anr'))
                                    <td class="number">{{number_format((float)round($totRev,2),0)}}</td>
                                @endif
                            </tr>
                        </tfoot>
                    </table>

                    <br>

                </div>
            </div>
        </div>
        @if($formType == 'view')
            <a href="{{url('')}}/{{ auth()->user()->roles()->where('name', request()->segment(1))->first()->name }}/dsp/" class="btn btn-success pull-right" id="btn-add-total">Back</a>
        @endif
    </section>
</div>
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/piexif.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/sortable.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/purify.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/themes/fa/theme.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.js"></script>
<script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-select.min.js') }}"></script>
@include('layouts.datatables_js')
<script>
    
    $(document).ready(function(){
        $('.datepicker').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            //viewMode: 'months',
            // minViewMode: 'months',
            format: 'dd-mm-yyyy'
        });

        $('.monthpicker').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            viewMode: 'months',
            minViewMode: 'months',
            format: 'mm'
        });
    
        $('.yearpicker').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            viewMode: 'years',
            minViewMode: 'years',
            format: 'yyyy'
        });
        
        $('#type').change(function(){
            if($(this).val() == 1){
                $('#type-content').show();
            } else {
                $('#type-content').hide();
            }
        });
    
        $("#input-id").fileinput({
            uploadUrl: "#",
            showUpload: false,
            allowedFileExtensions: ["csv", 'xlsx'],
            fileActionSettings: {
                showUpload: false,
            }
        });
    
        $('#input-id').change(function(){
            if($(this).get(0).files.length === 0){
                $('#btn-process-csv').attr('disabled', true); 
            }else{
                $('#btn-process-csv').attr('disabled', false);
            }
        });
    
        $('#btn-process-csv').click(function(){
            readCsv();
        });
    
        @if($formType == 'edit' || $formType == 'view')
        $('.manual').show();
        $('.reporting_month_a').attr('disabled', true);
        $(".actions ul").append('<li aria-hidden="false" style=""><a href="#" role="menuitem" id="btn-save">Save And Submit</a></li>');
        $('#reporting_month').val('{{ $dsp->reporting_month }}');
        $('#reporting_month_val').val('{{ $dsp->reporting_month }}');
        @endif
    
        @if($formType == 'edit')
        $('#detail-field').show();
        $('#estimate-field').show();
        $('#cost-field').show();
        $('#dsp-field').show();
        @endif
    });
    
    var form = $("#example-form");
    @if($formType != 'view')
        form.children("div").steps({
            labels: {finish: "Save As Draft"},
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            onStepChanging: function (event, currentIndex, newIndex)
            {
                if(currentIndex == 0){
                    if($('#type').val() == 1){
                        $('#detail-field').hide();
                        $('#estimate-field').hide();
                        @if(auth()->user()->roles->first()->name == 'finance')
                        $('#cost-field').hide();
                        @endif
                        $('#dsp-field').hide();
                        readCsv();
                    } else {    
                        $('#detail-field').show();
                        $('#estimate-field').show();
                        @if(auth()->user()->roles->first()->name == 'finance')
                        $('#cost-field').show();
                        @endif
                        $('#dsp-field').show();
                    }
    
                    if(form.valid() == true){
                        if($('#btn-save').length < 1){
                            $(".actions ul").append('<li aria-hidden="false" style=""><a href="#" role="menuitem" id="btn-save">Save And Submit</a></li>');
                        }
    
                        var month = $('#month_b').val();
                        var year = $('#year_b').val();
                        $('.month_a').val(month);
                        $('.month_a').attr('disabled', true);
                        $('.year_a').val(year);
                        $('.year_a').attr('disabled', true);
                    }
                }
                
                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onFinishing: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled";
                $('#estimate_gross_dsp').removeClass('required');
                @if(auth()->user()->roles->first()->name == 'finance')
                $('#cost_production').removeClass('required');
                @endif
                
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                $('#status').val(7);
                form.submit();
            }
        });
    @endif   
    
    $(".actions").on("click","#btn-save",function(){
        $('#estimate_gross_dsp').removeClass('required');
        @if(auth()->user()->roles->first()->name == 'finance')
        $('#cost_production').removeClass('required');
        @endif
        $('#status').val(3);
        form.submit();
    });
    
    $(".actions").on("click","#btn-update",function(){
        $('#status').val(3);
        form.submit();
    });
    
    $('#btn-dsp').click(function(){
    form.validate().settings.ignore = ":disabled";
    
    if(form.valid()){
        table.row.add( [ index, $('#detail_id option:selected').html(), $('#estimate_gross_dsp').val(),'<button type="button" data-id="'+$('#detail_id').val()+'" class="btn btn-default edit">Edit</button>'] )
            .draw()
            .node();
    
        $('#content-dsp').append('<input type="hidden" name="dsp_detail[]" value="'+[$('#detail_id').val(), $('#estimate_gross_dsp').val()]+'">');
    
        $('.rm-detail').find('[value='+$('#detail_id').val()+']').remove();
        $('.rm-detail').selectpicker('refresh');
        $('#estimate_gross_dsp').val('');
        @if(auth()->user()->roles->first()->name == 'finance')
        $('#cost_production').val('');
        @endif
        index++;
    }
    
    });

    var table = $("#table-dsp-detail").DataTable(
        {
            dom             : 'rtlip',
            order           : [[ 4, "asc"]],
            processing      : true,
            serverMethod    : 'post',
            responsive      : true,
            autoWidth       : false,
            aLengthMenu     : [[10, 50, 100, 250, -1], [10, 50, 100, 250, 'All']],
        }
    );

    var dspdetail = [];
    var index = 1;
    
    table.on('click', '.edit', function(e){
        var data = table.row( $(this).parents('tr') ).data();    
        initModal($(this).attr('data-id'),data);
    });
    
    function initModal(id,arr){
        $('#myModal').modal('show');    
        $('#m_index').val(arr[0]);
        $('#m_detail_id').val(arr[1]);
        $('#m_detail_id_val').val(id);
        $('#m_estimate_gross_dsp').val(arr[2]);
        @if(auth()->user()->roles->first()->name == 'finance')
            $('#m_cost_production').val(arr[3]);
        @endif
    }
    
    $('#update').click(function(){
        var idx = parseInt($('#m_index').val()) - 1;        
        var d = table.row( idx ).data();
        d[2] = $('#m_estimate_gross_dsp').val();
        table.row(idx).data(d).invalidate();        
        $('#myModal').modal('hide');
        
        $('input[name="dsp_detail[]"').eq(idx).val([$('#m_detail_id_val').val(), d[2], d[3]])
    });
    
    $('#btn-add-total').click(function(){
        $('#modal-add-total').modal('show');
    });
    
    $('#modal-add-total').on('hidden.bs.modal', function () {
        $('#total_gross_dsp_val').val($('#total_gross_dsp').val());
    })

    $('#singer').on('change', function(){
        console.log(this.value);

            table.search(this.value).draw(); 
        });
</script>
@endsection