@extends('layouts.crud')
@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
@endsection
@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Multi Uploads</h2>
        <!-- <ol class="breadcrumb">
            <li>
                <a href="#">Multi Uploads</a>
            </li>
            <li class="active">
                <strong></strong>
            </li>
        </ol> -->
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Personal Information
                </div>
                <div class="panel-body">
                {!! Form::open(['route' => 'creator.multiUpload.postUpload', 'class' => 'form', 'id' => 'form-upload', 'files' => true]) !!}
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- Title Field -->
                            <div class="form-group">
                                {!! Form::label('title', "Select platforms you'd like to publish", []) !!}
                                <br>
                                <span class="button-checkbox">
                                    <button type="button" class="btn" data-color="primary"><i class="fa fa-twitter-square"></i></button>
                                    <input type="checkbox" name="platforms[]" value="twitter" class="hidden" />
                                </span>
                                <span class="button-checkbox">
                                    <button type="button" class="btn" data-color="primary"><i class="fa fa-youtube-square"></i></button>
                                    <input type="checkbox" name="platforms[]" value="youtube" class="hidden" />
                                </span>
                                <span class="button-checkbox">
                                    <button type="button" class="btn" data-color="primary"><i class="icon-if_Dailymotion_1298733"></i></button>
                                    <input type="checkbox" name="platforms[]" value="dailymotion" class="hidden" />
                                </span>
                                
                            </div>

                            <!-- Description Field -->
                            <div class="form-group">
                                {!! Form::label('desc', "Select the videos you'd like to publish", []) !!}
                                <input id="input-id" type="file" name="video" class="file" data-preview-file-type="text" >
                                
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <!-- Submit Field -->
                            <div class="pull-right">
                                {!! Form::submit('Save', ['class' => 'btn btn-primary', 'id'=>'btn-submit']) !!}
                                <a href="{{route('creator.multiUpload.getUpload')}}" class="btn btn-default">Cancel</a>
                            </div>  
                        </div>
                    </div>
                    
                </div>
                     
            </div>
            {!! Form::close() !!}
            {!! Form::open(['route' => 'creator.multiUpload.create', 'method' => 'post', 'id' => 'form-redirect']) !!}
                <input type="hidden" name="platforms[]" id="platforms-create"/>
                <input type="hidden" name="video" id="video-create"/>
                <input type="hidden" name="title" id="title-create"/>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
@section('scripts')
<!-- piexif.min.js is only needed for restoring exif data in resized images and when you 
    wish to resize images before upload. This must be loaded before fileinput.min.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/piexif.min.js" type="text/javascript"></script>
<!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview. 
    This must be loaded before fileinput.min.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/sortable.min.js" type="text/javascript"></script>
<!-- purify.min.js is only needed if you wish to purify HTML content in your preview for 
    HTML files. This must be loaded before fileinput.min.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/plugins/purify.min.js" type="text/javascript"></script>
<!-- popper.min.js below is needed if you use bootstrap 4.x. You can also use the bootstrap js 
   3.3.x versions without popper.min.js. -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/js/fileinput.min.js"></script>
<!-- optionally if you need a theme like font awesome theme you can include it as mentioned below -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.8/themes/fa/theme.js"></script>
<script src="https://unpkg.com/sweetalert2@7.21.1/dist/sweetalert2.all.js"></script>
<script>

$("#form-upload").submit(function(e){
    var arr = [];
    $('input[name="platforms[]"]:checked').each(function () {
        arr.push($(this).val());
    });
    
    $('.file-input').append(
        '<div class="progress">'+
            '<div style="width: 0%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="35" role="progressbar" class="progress-bar progress-bar-success" id="progress-upload">'+
            '<span id="percentage-val">60% Complete</span>'+
            '</div>'+
        '</div>'
    );
    $.ajax({
        xhr: function(){
            var file_data = $('#input-id').prop('files')[0];
            var form_data = new FormData();                  
            form_data.append('video', file_data);
            for (var i = 0; i < arr.length; i++) {
                form_data.append('platforms[]', arr[i]);
            }
            var xhr = new window.XMLHttpRequest();
            
            xhr.open('POST', '{{ route("creator.multiUpload.postUpload") }}', true);
            
            xhr.setRequestHeader('X-CSRF-TOKEN','{{ csrf_token() }}');
                // Handle progress
                //Upload progress
            xhr.upload.addEventListener("progress", function(evt){
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
			
                    var percentageDisplay = (parseFloat(percentComplete).toFixed(2) * 100) + '%';
                    //Do something with upload progress
                    $('#progress-upload').css('width', percentageDisplay);
                    $('#percentage-val').text(percentageDisplay);
                }
            }, false);
            //Download progress
            xhr.addEventListener("progress", function(evt){
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    //Do something with download progress
                    console.log(percentComplete);
                }
            }, false);
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4) { // `DONE`
                    status = xhr.status;
                    
                    if (status == 200) {
                        data = JSON.parse(xhr.responseText);
                        
                        $('#platforms-create').val(data.platforms);
                        $('#video-create').val(data.video);
                        $('#title-create').val(data.title);
                        $('#form-redirect').submit();
                    } else {
                    }
                }
            };
            xhr.send(form_data);   
            
        },
    });
    $('#btn-submit').attr('disabled', true);
    
    return false;
 });
 
$("#input-id").fileinput({
    uploadUrl: "#",
    showBrowse: false,
    browseOnZoneClick: true,
    showUpload: false,
    allowedFileTypes: ["video"],
    allowedFileExtensions: ["mp4"],
    maxFileSize: 1024000,
    fileActionSettings: {
        showUpload: false,
    }
});

$(function () {
        $('.button-checkbox').each(function () {

            // Settings
            var $widget = $(this),
                $button = $widget.find('button'),
                $checkbox = $widget.find('input:checkbox'),
                color = $button.data('color'),
                settings = {
                    on: {
                        icon: 'glyphicon glyphicon-check'
                    },
                    off: {
                        icon: 'glyphicon glyphicon-unchecked'
                    }
                };

            // Event Handlers
            $button.on('click', function () {
                
                $checkbox.prop('checked', !$checkbox.is(':checked'));
                $checkbox.triggerHandler('change');
                updateDisplay();
                
            });
            
            $checkbox.on('change', function () {
                $.get( "{{url('creator/platforms/connectionCheck')}}/" + $(this).val(), { }, function(data) {
                    if(data.status == false){
                        $checkbox.prop('checked', false);
                        swal({
                            type: 'warning',
                            title: 'Something wrong',
                            text: 'Please check connection at platforms menu!',
                        })
                    }
                    updateDisplay()
                });

                
            });

            // Actions
            function updateDisplay() {
                var isChecked = $checkbox.is(':checked');

                // Set the button's state
                $button.data('state', (isChecked) ? "on" : "off");

                // Set the button's icon
                $button.find('.state-icon')
                    .removeClass()
                    .addClass('state-icon ' + settings[$button.data('state')].icon);

                // Update the button's color
                if (isChecked) {
                    $button
                        .removeClass('btn-default')
                        .addClass('btn-' + color + ' active');
                }
                else {
                    $button
                        .removeClass('btn-' + color + ' active')
                        .addClass('btn-default');
                }
            }

            // Initialization
            function init() {

                updateDisplay();

                // Inject the icon if applicable
                if ($button.find('.state-icon').length == 0) {
                    $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
                }
            }
            init();
        });
    });
</script>
@endsection
