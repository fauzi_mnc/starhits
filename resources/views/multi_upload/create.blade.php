@extends('layouts.crud')
@section('css')
<link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/bootstrap-tagsinput.css') }}" rel="stylesheet">
<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>

@endsection
@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Multi Uploads</h2>
        <!-- <ol class="breadcrumb">
            <li>
                <a href="#">Multi Uploads</a>
            </li>
            <li class="active">
                <strong></strong>
            </li>
        </ol> -->
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Form
                </div>
                <div class="panel-body">
                    {!! Form::open(['route' => ['creator.multiUpload.store'], 'class' => 'form', 'files' => true, 'id' => 'form-upload']) !!}
                    <!-- <div class="progress progress-striped active">
                        <div style="width: 75%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="75" role="progressbar" class="progress-bar progress-bar-danger">
                            <span class="sr-only">40% Complete (success)</span>
                        </div>
                    </div> -->
                    {!! Form::hidden('video', $video, []); !!}
                    <div class="row">
                        <div class="col-lg-6">
                            <!-- Title Field -->
                            <div class="form-group">
                                {!! Form::label('title', 'Title:', []) !!}
                                {!! Form::text('title', $title, ['class' => 'form-control']) !!}
                                
                            </div>

                            <!-- Description Field -->
                            <div class="form-group">
                                {!! Form::label('desc', 'Description:', []) !!}
                                {!! Form::textarea('desc', null, ['class' => 'form-control']) !!}
                                
                            </div>

                            <!-- tags Field -->
                            <div class="form-group">
                                {!! Form::label('tags', 'Tags:', []) !!}
                                {!! Form::text('tags', null, ['class' => 'form-control', 'data-role' => 'tagsinput']) !!}
                                
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <!-- Platforms Field -->
                            <div class="form-group">
                                {!! Form::label('platform', 'Platforms:', []) !!}
                                <br>
                                <span class="button-checkbox">
                                    <button type="button" class="btn" data-color="primary"><i class="fa fa-twitter-square"></i></button>
                                    <input type="checkbox" name="platforms[]" value="twitter" class="hidden" {!! isset($platforms['twitter']) ? 'checked' : '' !!} />
                                </span>
                                <span class="button-checkbox">
                                    <button type="button" class="btn" data-color="primary"><i class="fa fa-youtube-square"></i></button>
                                    <input type="checkbox" name="platforms[]" value="youtube" class="hidden" {!! isset($platforms['youtube']) ? 'checked' : '' !!} />
                                </span>
                                <span class="button-checkbox">
                                    <button type="button" class="btn" data-color="primary"><i class="icon-if_Dailymotion_1298733"></i></button>
                                    <input type="checkbox" name="platforms[]" value="dailymotion" class="hidden" {!! isset($platforms['dailymotion']) ? 'checked' : '' !!} />
                                </span>
                                
                            </div>

                            <!-- Thumbnail Field -->
                            <div class="form-group">
                                {!! Form::label('thumbnail', 'Thumbnail:', []) !!}                              
                                <div class="kv-photo center-block text-center">
                                    <input id="thumbnail" name="thumbnail" type="file" class="file-loading">
                                </div>

                                <div id="kv-thumbnail-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
                                
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <!-- Submit Field -->
                            <div class="pull-right">
                                <button class="btn btn-primary" id="btn-publish">
                                Save
                                </button>
                                <a href="{!! route('creator.multiUpload.getUpload') !!}" class="btn btn-default">Cancel</a>
                            </div>  
                        </div>
                    </div>
                </div>
                
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
@section('scripts')
<script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-tagsinput.min.js') }}"></script>
<script>

    $("#thumbnail").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-thumbnail-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="{{ asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "jpeg"]
    });

    $(function () {
        $('.button-checkbox').each(function () {

            // Settings
            var $widget = $(this),
                $button = $widget.find('button'),
                $checkbox = $widget.find('input:checkbox'),
                color = $button.data('color'),
                settings = {
                    on: {
                        icon: 'glyphicon glyphicon-check'
                    },
                    off: {
                        icon: 'glyphicon glyphicon-unchecked'
                    }
                };

            // Event Handlers
            $button.on('click', function () {
                $checkbox.prop('checked', !$checkbox.is(':checked'));
                $checkbox.triggerHandler('change');
                updateDisplay();
            });
            $checkbox.on('change', function () {
                updateDisplay();
            });

            // Actions
            function updateDisplay() {
                var isChecked = $checkbox.is(':checked');

                // Set the button's state
                $button.data('state', (isChecked) ? "on" : "off");

                // Set the button's icon
                $button.find('.state-icon')
                    .removeClass()
                    .addClass('state-icon ' + settings[$button.data('state')].icon);

                // Update the button's color
                if (isChecked) {
                    $button
                        .removeClass('btn-default')
                        .addClass('btn-' + color + ' active');
                }
                else {
                    $button
                        .removeClass('btn-' + color + ' active')
                        .addClass('btn-default');
                }
            }

            // Initialization
            function init() {

                updateDisplay();

                // Inject the icon if applicable
                if ($button.find('.state-icon').length == 0) {
                    $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
                }
            }
            init();
        });
    });

    $('#btn-publish').click(function(){
        $('#btn-publish').empty()
        $(this).prepend('<i class="fa fa-spinner fa-spin"></i> Loading..');
        $(this).attr('disabled', true);
        $('#btn-draft').attr('disabled', true);
        $('#form-upload').submit();
    });
</script>
@endsection