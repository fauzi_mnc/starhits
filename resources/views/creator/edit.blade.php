@extends('layouts.crud')

@section('css')
<link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">
<link href="{{ asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet">
<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>

@endsection

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Creator</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Creator</a>
            </li>
            <li class="active">
                <strong>Edit</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Create Edit</h5>
                </div>
                <div class="ibox-content">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            Personal Information
                        </div>
                        <div class="panel-body">
                        @if(auth()->user()->roles->first()->name === 'admin')
                            {!! Form::model($user, ['route' => ['creator.update', $user->id], 'method' => 'POST', 'class' => 'form-horizontal', 'files' => true]) !!}
                        @elseif(auth()->user()->roles->first()->name === 'user')
                            {!! Form::model($user, ['route' => ['user.creator.update', $user->id], 'method' => 'POST', 'class' => 'form-horizontal', 'files' => true]) !!}
                        @elseif(auth()->user()->roles->first()->name === 'finance')
                            {!! Form::model($user, ['route' => ['finance.creator.update', $user->id], 'method' => 'POST', 'class' => 'form-horizontal', 'files' => true]) !!}
                        @else
                            {!! Form::model($user, ['route' => ['creator.update', $user->id], 'method' => 'POST', 'class' => 'form-horizontal', 'files' => true]) !!}
                        @endif

                        <!-- Contract -->
                        <div class="form-group">
                            {!! Form::label('number_contract', 'No Contract:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-10">
                                {!! Form::text('number_contract', null, ['class' => 'form-control']) !!}
                            </div>

                            <div class="col-md-3 col-md-offset-2" style="margin-top:10px;">
                                <div class="form-group" style="margin-left:0;margin-right:0;">
                                    <label for="contract_from">From Date</label>
                                    <div class="input-group date" id="datepickerFrom">
                                        {!! Form::text('contract_from', null, ['class' => 'form-control', 'id' => 'contract_from' ]) !!}
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3" style="margin-top:10px;">
                                <div class="form-group" style="margin-left:0;margin-right:0;">
                                    <label for="contract_to">To Date</label>
                                    <div class="input-group date" id="datepickerTo">
                                        {!! Form::text('contract_to', null, ['class' => 'form-control', 'id' => 'contract_to' ]) !!}
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Name Field -->
                        <div class="form-group">
                            {!! Form::label('name', 'Name:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-10">
                                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('email', 'Email:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-10">
                                {!! Form::text('email', null, ['class' => 'form-control', 'maxlength' => 60, 'required' => true]) !!}
                            </div>
                        </div>

                        <!-- Picture Field -->
                        <div class="form-group">
                            {!! Form::label('image', 'Picture:', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-10">
                                <div class="kv-photo center-block text-center">
                                    <input id="photo" name="image" type="file" class="file-loading"  accept="image/x-png">
                                </div>

                                <div id="kv-photo-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
                            </div>
                        </div>
                        <hr>

                        <!-- IMG Feature Home Field -->
                        <!-- Name Field -->
                        <div class="form-group">
                            {!! Form::label('name', 'Is Feature Home:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-3">
                                <input type="checkbox" name="is_feature" id="feature_home" class="form-control feature_home" <?php if($user->is_feature_home==1){echo "checked";} ?> >
                            </div>
                        </div>

                        <div class="form-group hide" id="feature_home_div">
                            {!! Form::label('channel', 'Sorting :', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-10 col-md-10">
                                <div class="col-sm-2 col-md-2" style="padding:0; margin-bottom:10px;">
                                    <select class="form-control" name="feature_sort_home" >
                                        <option value="">select</option>
                                        <option value="1" <?php if($user->feature_sort_home=="1"){ echo "selected";} ?> >1</option>
                                        <option value="2"<?php if($user->feature_sort_home=="2"){ echo "selected";} ?> >2</option>
                                        <option value="3" <?php if($user->feature_sort_home=="3"){ echo "selected";} ?> >3</option>
                                        <option value="4" <?php if($user->feature_sort_home=="4"){ echo "selected";} ?> >4</option>
                                        <option value="5" <?php if($user->feature_sort_home=="5"){ echo "selected";} ?> >5</option>
                                    </select>
                                </div>
                            </div>

                            {!! Form::label('image', 'Image Feature Home:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-3 col-md-3">
                                <div class="kv-photo center-block text-center">
                                    <input id="feature1" name="feature1" type="file" class="file-loading"  accept="image/x-png">
                                </div>
                                <div id="kv-photo-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                {!! Form::label('channel', 'Image Width : ', ['class' => 'col-sm-5 control-label']) !!}
                                {!! Form::text('img_width1', null, ['class' => 'form-control', 'id' => 'img_width1','readonly']) !!}<br>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                {!! Form::label('channel', 'Image Height :', ['class' => 'col-sm-5 control-label']) !!}
                                {!! Form::text('img_height1', null, ['class' => 'form-control', 'id' => 'img_height1','readonly']) !!}
                            </div>
                            <div class="col-sm-4 col-md-4" id="info_img1">
                                {!! Form::label('info_img', 'Size : 460 x 760', ['class' => 'col-sm-8 control-label']) !!}
                            </div>
                        </div>
                        <hr style="">

                        <!-- IMG Feature PAGE Field -->
                        <div class="form-group">
                            {!! Form::label('name', 'Is Feature Page:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-3">
                                <input type="checkbox" id="feature_page" name="is_feature_page" class="form-control feature_page" <?php if($user->is_feature_page==1){echo "checked";} ?>>
                            </div>
                        </div>
                        
                        <div class="form-group hide" id="feature_page_div">
                            {!! Form::label('channel', 'Sorting :', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-10 col-md-10">
                                <div class="col-sm-2 col-md-2" style="padding:0; margin-bottom:10px;">
                                    <select class="form-control" name="feature_sort_page" id="feature_sort_page" onchange="featurePageText(this);">
                                        <option value="">select</option>
                                        <option value="1" <?php if($user->feature_sort_page=="1"){ echo "selected";} ?> >1</option>
                                        <option value="2" <?php if($user->feature_sort_page=="2"){ echo "selected";} ?> >2</option>
                                        <option value="3" <?php if($user->feature_sort_page=="3"){ echo "selected";} ?> >3</option>
                                        <option value="4" <?php if($user->feature_sort_page=="4"){ echo "selected";} ?> >4</option>
                                        <option value="5" <?php if($user->feature_sort_page=="5"){ echo "selected";} ?> >5</option>
                                    </select>
                                </div>
                            </div>

                            {!! Form::label('image', 'Image Feature Page:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-3 col-md-3">
                                <div class="kv-photo center-block text-center">
                                    <input id="feature2" name="feature2" type="file" class="file-loading"  accept="image/x-png">
                                </div>
                                <div id="kv-photo-errors-2" class="center-block alert alert-block alert-danger" style="display:none"></div>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                {!! Form::label('channel', 'Image Width : ', ['class' => 'col-sm-5 control-label']) !!}
                                {!! Form::text('img_width2', null, ['class' => 'form-control', 'id' => 'img_width2','readonly']) !!}<br>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                {!! Form::label('channel', 'Image Height :', ['class' => 'col-sm-5 control-label']) !!}
                                {!! Form::text('img_height2', null, ['class' => 'form-control', 'id' => 'img_height2','readonly']) !!}
                            </div>
                            <div class="col-sm-4 col-md-4" id="info_img_size">
                                <p class="text-center" id="info_img2"></p>
                            </div>
                        </div>
                        <hr>

                        <!-- IMG 1280x607 Field -->
                        <div class="form-group">
                            {!! Form::label('image', 'Image Header Creator:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-3 col-md-3">
                                <div class="kv-photo center-block text-center">
                                    <input id="feature3" name="img_header" type="file" class="file-loading"  accept="image/x-png">
                                </div>
                                <div id="kv-photo-errors-3" class="center-block alert alert-block alert-danger" style="display:none"></div>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                {!! Form::label('channel', 'Image Width : ', ['class' => 'col-sm-5 control-label']) !!}
                                {!! Form::text('img_width3', null, ['class' => 'form-control', 'id' => 'img_width3','readonly']) !!}<br>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                {!! Form::label('channel', 'Image Height :', ['class' => 'col-sm-5 control-label']) !!}
                                {!! Form::text('img_height3', null, ['class' => 'form-control', 'id' => 'img_height3','readonly']) !!}
                            </div>
                            <div class="col-sm-4 col-md-4" id="info_img3">
                                {!! Form::label('info_img3', 'SIZE : 1280x380', ['class' => 'col-sm-8 control-label']) !!}
                            </div>
                        </div>


                        <!-- IMG 634x638 Field -->
                        <div class="form-group">
                            {!! Form::label('image', 'Image Content Creator:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-3 col-md-3">
                                <div class="kv-photo center-block text-center">
                                    <input id="feature4" name="img_content" type="file" class="file-loading"  accept="image/x-png">
                                </div>
                                <div id="kv-photo-errors-4" class="center-block alert alert-block alert-danger" style="display:none"></div>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                {!! Form::label('channel', 'Image Width : ', ['class' => 'col-sm-5 control-label']) !!}
                                {!! Form::text('img_width4', null, ['class' => 'form-control', 'id' => 'img_width4','readonly']) !!}<br>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                {!! Form::label('channel', 'Image Height :', ['class' => 'col-sm-5 control-label']) !!}
                                {!! Form::text('img_height4', null, ['class' => 'form-control', 'id' => 'img_height4','readonly']) !!}
                            </div>
                            <div class="col-sm-4 col-md-4" id="info_img4">
                                {!! Form::label('info_img4', 'SIZE : 634 x 638', ['class' => 'col-sm-8 control-label']) !!}
                            </div>
                        </div>

                        <!-- SELECT IS CORPORATE -->
                        <div class="form-group">
                            {!! Form::label('channel', 'Creator Type :', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-3">
                                <select class="form-control" name="corporate_creator" required="">
                                    <option value="">== Select ==</option>
                                    <option value="creator_personal" <?php if($user->corporate_creator=="creator_personal"){ echo "selected";} ?> >Creator Personal</option>
                                    <option value="creator_corporate" <?php if($user->corporate_creator=="creator_corporate"){ echo "selected";} ?>>Creator Corporate</option>
                                </select>
                            </div>
                        </div>

                        <!-- Channel Field -->
                        <div class="form-group">
                            {!! Form::label('channel', 'Channel Youtube:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-10">
                                {!! Form::text('provider_id', null, ['class' => 'form-control', 'id' => 'channel','readonly']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('', '', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-10" id="youtube-profile">
                                <button type="button" class="btn btn-default pull-right" id="btn-check">Check</button>
                            </div>
                        </div>

                        <input id="image" name="image" type="hidden" value="{{$user->image}}">
                        <input id="subscribers" name="subscribers"  value="{{$user->subscribers}}" type="hidden">

                        <!-- Biodata Field -->
                        <div class="form-group">
                            {!! Form::label('bio', 'Biodata:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-10">
                                {!! Form::textarea('biodata', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <!-- Password Field -->
                        <div class="form-group">
                            {!! Form::label('password', 'Password:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-8">
                                {!! Form::text('password', null, ['class' => 'form-control', 'disabled' => true]) !!}
                            </div>

                            <div class="col-sm-2">
                                <button type="button" id="reset-pwd" class="btn btn-default">Reset</button>
                            </div>

                        </div>

                        <!-- Cover Field -->
                        <div class="form-group">
                            {!! Form::label('cover', 'Cover:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-10">
                                <div class="kv-photo center-block text-center">
                                    <input id="cover" name="cover" type="file" class="file-loading">
                                </div>

                                <div id="kv-cover-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
                            </div>
                        </div>

                        <!-- Percentage Field -->
                        <div class="form-group">
                            {!! Form::label('percentage', 'Percentage:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-10">
                                {!! Form::text('percentage', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>

                        <!-- Unit Field -->
                        <div class="form-group">
                            {!! Form::label('unit', 'Unit:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-10">
                                {!! Form::text('unit', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <!-- Active Field -->
                        <div class="form-group hide">
                            {!! Form::label('is-active', 'Is Active', ['class' => 'col-sm-3 control-label']) !!}

                            <div class="col-sm-9">
                                <div class="switch">
                                    <div class="onoffswitch">
                                        <input type="checkbox" class="onoffswitch-checkbox recommend" name="is_active" id="is-active">
                                        <label class="onoffswitch-label" for="is-active">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Show Field -->
                        <div class="form-group hide">
                            {!! Form::label('is-show', 'Is Show', ['class' => 'col-sm-3 control-label']) !!}

                            <div class="col-sm-9">
                                <div class="switch">
                                    <div class="onoffswitch">
                                        <input type="checkbox" class="onoffswitch-checkbox recommend" name="is_show" id="is-show">
                                        <label class="onoffswitch-label" for="is-show">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-heading">
                            Social Media
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="input-group m-b"><span class="input-group-addon"><i class="fa fa-instagram"></i></span>
                                        <input type="text" name="socials[]" value="{{ ($ig) ? $ig->url : '' }}" placeholder="https://www.instagram.com/username" class="form-control">
                                    </div>
                                    <div class="input-group m-b"><span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                        <input type="text" name="socials[]" value="{{ ($fb) ? $fb->url : '' }}" placeholder="https://www.facebook.com/username" class="form-control">
                                    </div>
                                    <div class="input-group m-b"><span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                        <input type="text" name="socials[]" value="{{ ($tw) ? $tw->url : '' }}" placeholder="https://twitter.com/username" class="form-control">
                                    </div>
                                    <!-- <div class="input-group m-b"><span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                                        <input type="text" name="socials[]" value="{{ ($gp) ? $gp->url : '' }}" placeholder="https://plus.google.com/u/0/username" class="form-control">
                                    </div> -->
                                </div>
                            </div>
                        </div>

                        <div class="panel-heading">
                            Connect To Instagram
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                {!! Form::label('username_instagram', 'Username Instagram:', ['class' => 'col-sm-2 control-label']) !!}

                                <div class="col-sm-10">
                                    <div class="input-group">
                                        {!! Form::text('username_instagram', $user->detailInfluencer ? $user->detailInfluencer->username : null, ['class' => 'form-control required']) !!}
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-primary pull-right" id="btn-check-instagram">Check</button>
                                        </span>
                                    </div>

                                    <div id="instagram-profile">
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- Submit Field -->
                    
                    @if(auth()->user()->roles->first()->name === 'admin')
                    <div class="pull-right">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{{route('creator.index')}}" class="btn btn-default">Cancel</a>
                    </div>
                    @elseif(auth()->user()->roles->first()->name === 'user')
                    <div class="pull-right">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{{route('user.creator.index')}}" class="btn btn-default">Cancel</a>
                    </div>
                    @elseif(auth()->user()->roles->first()->name === 'finance')
                    <div class="pull-right">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{{route('finance.creator.index')}}" class="btn btn-default">Cancel</a>
                    </div>
                    @else
                    <div class="pull-right">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{{route('creator.index')}}" class="btn btn-default">Cancel</a>
                    </div>

                    @endif

                    {!! Form::close() !!}
                </div>
            </div>
@endsection
@section('scripts')
<script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script>

    var featurePage = document.getElementById('feature_sort_page');
    var infoImg = document.getElementById('info_img2');
    if ( featurePage.value == " " ) {
        infoImg.innerHTML = "<span class='text-primary'><b>Please Select Sort</b></span>";
    }else if ( featurePage.value == "1" ) {
        infoImg.innerHTML = "<span class='text-primary'><b>SIZE : 523x320</b></span>";
    }else if ( featurePage.value == "2" ) {
        infoImg.innerHTML = "<span class='text-primary'><b>SIZE : 587x367</b></span>";
    }else if ( featurePage.value == "3" ) {
        infoImg.innerHTML = "<span class='text-primary'><b>SIZE : 706x483</b></span>";
    }else if ( featurePage.value == "4" ) {
        infoImg.innerHTML = "<span class='text-primary'><b>SIZE : 448x637</b></span>";
    }else if ( featurePage.value == "5" ) {
        infoImg.innerHTML = "<span class='text-primary'><b>SIZE : 675x450</b></span>";
    }
    
    function featurePageText(sel)
    {
        var featurePage = document.getElementById('feature_sort_page');
        var infoImg = document.getElementById('info_img2');
        if ( sel.options[sel.selectedIndex].value == "" ) {
            infoImg.innerHTML = "<span class='text-danger'><b>Please Select Sort</b></span>";
        }else if ( sel.options[sel.selectedIndex].value == "1" ) {
            infoImg.innerHTML = "<span class='text-primary'><b>SIZE : 587x367</b></span>";
        }
        else if ( sel.options[sel.selectedIndex].value == "2" ) {
            infoImg.innerHTML = "<span class='text-primary'><b>SIZE : 523x320</b></span>";
        }
        else if ( sel.options[sel.selectedIndex].value == "3" ) {
            infoImg.innerHTML = "<span class='text-primary'><b>SIZE : 706x483</b></span>";
        }
        else if ( sel.options[sel.selectedIndex].value == "4" ) {
            infoImg.innerHTML = "<span class='text-primary'><b>SIZE : 448x637</b></span>";
        }
        else if ( sel.options[sel.selectedIndex].value == "5" ) {
            infoImg.innerHTML = "<span class='text-primary'><b>SIZE : 675x450</b></span>";
        }

    }

$( document ).ready(function() {

    $('#datepickerFrom').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format:'dd/mm/yyyy',
    });
    $('#datepickerTo').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format:'dd/mm/yyyy',
    });

    <?php if($user->is_feature_home==1){ ?>
        document.getElementById("feature_home_div").classList.remove('hide');
    <?php } ?>
    <?php if($user->is_feature_page==1){ ?>
        document.getElementById("feature_page_div").classList.remove('hide');
    <?php } ?>

    $("#feature_page").click(function(e) {
        if($('input.feature_page').is(':checked')) {
            document.getElementById("feature_page_div").classList.remove('hide');
        }else{
            document.getElementById("feature_page_div").classList.add('hide');
        }
    });

    $("#feature_home").click(function(e) {
        if($('input.feature_home').is(':checked')) {
            document.getElementById("feature_home_div").classList.remove('hide');
        }else{
            document.getElementById("feature_home_div").classList.add('hide');
        }
    });
});


    $("#cover").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-cover-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        //defaultPreviewContent: '<img src="{{asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
        defaultPreviewContent: '<img src="{{ ($user->cover) ? asset($user->cover) : asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "jpeg"]
    });

    $("#photo").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
       // defaultPreviewContent: '<img src="{{asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
       defaultPreviewContent: '<img src="{{ ($user->image) ? asset($user->image) : asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "jpeg"]
    });

    
    $("#feature1").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="{{ ($user->feature_home) ? asset($user->feature_home) : asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["png"]
    });
    
    var _URL = window.URL || window.webkitURL;
    $("#feature1").change(function(e) {
        var file, img;
        if ((file = this.files[0])) {
            img = new Image();
            img.onload = function() {
                //alert(this.width + " " + this.height);
                $("#img_width1").val(this.width);
                $("#img_height1").val(this.height);
            };
            img.onerror = function() {
                alert( "not a valid file: " + file.type);
            };
            img.src = _URL.createObjectURL(file);
        }
    });

    $("#feature2").fileinput({ /*JSCRIPT feature_page*/
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: true,
        showCaption: false,
        browseLabel: ' Image Feature',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-2',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="{{ ($user->feature_page) ? asset($user->feature_page) : asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',

        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["png"]
    });
    
    var _URL = window.URL || window.webkitURL;
    $("#feature2").change(function(e) {
        var file, img;
        if ((file = this.files[0])) {
            img = new Image();
            img.onload = function() {
                //alert(this.width + " " + this.height);
                $("#img_width2").val(this.width);
                $("#img_height2").val(this.height);

                /*if(this.width > 300 || this.height > 300 ){
                    document.getElementById("info_img2").classList.remove('hide');
                    $("#info_desc_img2").html("Errror : Max Width = 300px & Max Height = 300px");
                }else{
                    document.getElementById("info_img2").classList.remove('hide');
                    $("#info_desc_img2").html("Valid : Max Width = 300px & Max Height = 300px");
                }*/
            };
            img.onerror = function() {
                alert( "not a valid file: " + file.type);
            };
            img.src = _URL.createObjectURL(file);
        }
    });



    $("#feature3").fileinput({ ///1280x607 img_header
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: true,
        showCaption: false,
        browseLabel: ' Image Header',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-3',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="{{ ($user->img_header) ? asset($user->img_header) : asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["png"]
    });
    
    var _URL = window.URL || window.webkitURL;
    $("#feature3").change(function(e) {
        var file, img;
        if ((file = this.files[0])) {
            img = new Image();
            img.onload = function() {
                $("#img_width3").val(this.width);
                $("#img_height3").val(this.height);
            };
            img.onerror = function() {
                alert( "not a valid file: " + file.type);
            };
            img.src = _URL.createObjectURL(file);
        }
    });


    $("#feature4").fileinput({ ///634x638 img_content
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: true,
        showCaption: false,
        browseLabel: ' Image Content',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-4',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="{{ ($user->img_content) ? asset($user->img_content) : asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["png"]
    });
    
    var _URL = window.URL || window.webkitURL;
    $("#feature4").change(function(e) {
        var file, img;
        if ((file = this.files[0])) {
            img = new Image();
            img.onload = function() {
                $("#img_width4").val(this.width);
                $("#img_height4").val(this.height);
           };
            img.onerror = function() {
                alert( "not a valid file: " + file.type);
            };
            img.src = _URL.createObjectURL(file);
        }
    });


    $('#password').attr('disabled', true);
    $('#reset-pwd').click(function(){
        $('#password').val('');
        $('#password').attr('disabled', false);
    });

    $('#btn-submit').click(function(e){

if($('#username_instagram').val() != '' && $('#element-instagram').length == 0){
    $('#username_instagram').parent().parent().find('.error').remove();
    $('#username_instagram').parent().parent().append('<label class="error">Please Click Check Button!</label>');

} else {
    $('form').submit();
}

return false;
});

/*$("#channel").change(function(){
     $('#youtube-profile').html('<div class="col-sm-10" id="youtube-profile"><button type="button" class="btn btn-default pull-right"id="btn-check">Check</button></div>');
});
*/

function nFormatter(num, digits) {
    var si = [
        { value: 1, symbol: "" },
        { value: 1E3, symbol: "k" },
        { value: 1E6, symbol: "M" },
        { value: 1E9, symbol: "G" },
        { value: 1E12, symbol: "T" },
        { value: 1E15, symbol: "P" },
        { value: 1E18, symbol: "E" }
    ];
    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i;
    for (i = si.length - 1; i > 0; i--) {
        if (num >= si[i].value) {
        break;
        }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
}

if($('#channel').val()){
        $.get( "{{route('creator.check')}}", { url:  $('#channel').val()}, function(data) {
            if(data.result == false){
                $('#youtube-profile').prepend(
                    '<div class="alert alert-danger">Invalid Youtube Channel</div>'
                );

                setTimeout(() => {
                    $('.alert-danger').remove();
                }, 5000);

            } else {
                $('#channel').val(data.result.id);
                $('#youtube-profile').empty();
                $('#youtube-profile').append(
                    '<div class="feed-element">'+
                        '<a href="#" class="pull-left">'+
                            '<img alt="image" class="img-circle" src="'+data.result.snippet.thumbnails.medium.url+'" style="height:100px;width:100px;">'+
                        '</a>'+
                        '<div class="media-body">'+
                            '<strong>'+data.result.snippet.title+'</strong> <br>'+
                            '<small class="text-muted">'+nFormatter(data.result.statistics.subscriberCount)+' subscribers</small>'+
                        '</div>'+
                    '</div>'
                );
                $('#subscribers').val(data.result.statistics.subscriberCount);
                //$('#image').val(data.result.snippet.thumbnails.medium.url);
            }
        });
    }


    $('#btn-check-instagram').click(function(){
        $.get( "{{route('influencer.instagram.check')}}", { username:  $('#username_instagram').val()}, function(data) {

            console.log(data);
            if(data.result == false){
                $('#instagram-profile').prepend(
                    '<div class="alert alert-danger">Invalid username Instagram</div>'
                );

                setTimeout(() => {
                    $('.alert-danger').remove();
                }, 5000);
                
            } else {
                $('#username_instagram').val(data.result.userName);
                $('#instagram-profile').empty();
                $('#instagram-profile').append(
                    '<div class="feed-element" id="element-instagram">'+
                        '<a href="#" class="pull-left">'+
                            '<img alt="image" class="img-circle" src="'+data.result.profilePicture+'" style="height:100px;width:100px;">'+
                        '</a>'+
                        '<div class="media-body">'+
                            '<h3>'+data.result.userName+'</h3> <br>'+
                            '<strong style="padding: 0px 10px;">'+data.result.mediaCount+' post</strong><strong style="padding: 0px 10px;">'+data.result.followers+' followers</strong><strong style="padding: 0px 10px;">'+ data.result.following +' following</small></strong>'+
                        '</div>'+
                    '</div>'
                );
                $('#username_instagram').parent().parent().find('.error').remove();
            }
        });
    });

    $('#btn-check').click(function(){
        $.get( "{{route('creator.check')}}", { url:  $('#channel').val()}, function(data) {
            if(data.result == false){
                $('#youtube-profile').prepend(
                    '<div class="alert alert-danger">Invalid Youtube Channel</div>'
                );

                setTimeout(() => {
                    $('.alert-danger').remove();
                }, 5000);

            } else {
                $('#channel').val(data.result.id);
                $('#youtube-profile').empty();
                $('#youtube-profile').append(
                    '<div class="feed-element">'+
                        '<a href="#" class="pull-left">'+
                            '<img alt="image" class="img-circle" src="'+data.result.snippet.thumbnails.medium.url+'" style="height:100px;width:100px;">'+
                        '</a>'+
                        '<div class="media-body">'+
                            '<strong>'+data.result.snippet.title+'</strong> <br>'+
                            '<small class="text-muted">'+nFormatter(data.result.statistics.subscriberCount)+' subscribers</small>'+
                        '</div>'+
                    '</div>'
                );
                $('#subscribers').val(data.result.statistics.subscriberCount);
                //$('#image').val(data.result.snippet.thumbnails.medium.url);
            }
        });

        $('#btn-submit').attr('disabled', false);
    });


    /*@if(!empty($user->detailInfluencer))
    if($('#username_instagram').val() != ''){
        $.get( "{{route('influencer.instagram.check')}}", { username:  $('#username_instagram').val()}, function(data) {
            if(data.result == false){
                $('#instagram-profile').prepend(
                    '<div class="alert alert-danger">Invalid username Instagram</div>'
                );

                setTimeout(() => {
                    $('.alert-danger').remove();
                }, 5000);

            } else {
                $('#username_instagram').val(data.result.userName);
                $('#instagram-profile').empty();
                $('#instagram-profile').append(
                    '<div class="feed-element" id="element-instagram">'+
                        '<a href="#" class="pull-left">'+
                            '<img alt="image" class="img-circle" src="'+data.result.profilePicture+'" style="height:100px;width:100px;">'+
                        '</a>'+
                        '<div class="media-body">'+
                            '<h3>'+data.result.userName+'</h3> <br>'+
                            '<strong style="padding: 0px 10px;">'+data.result.mediaCount+' post</strong>  <strong style="padding: 0px 10px;">'+data.result.followers+' followers</strong>  <$
                        '</div>'+
                    '</div>'
                );
            }
        });
    }
    @endif*/
</script>
@endsection
