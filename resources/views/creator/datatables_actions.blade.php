@if(auth()->user()->roles->first()->name == 'admin')
    {!! Form::open(['route' => ['creator.destroy', $c->id], 'method' => 'delete']) !!}
    <div class='btn-group'>
        <a href="{{ route('influencer.edit', $c->id) }}" class='btn btn-info btn-xs' title="Add To Influencer">
            <i class="glyphicon glyphicon-star"></i>
        </a>
        <a href="{{ route('creator.edit', $c->id) }}" class='btn btn-primary btn-xs' title="Edit Creator">
            <i class="glyphicon glyphicon-edit"></i> 
        </a>
        {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', [
            'type' => 'submit',
            'class' => 'btn btn-warning btn-xs',
            'title' => 'Delete Creator',
            'onclick' => "return confirm('Do you want to delete this creator?')",
            'name' => 'action',
            'value' => 'del'
        ]) !!}
        @if($c->is_active == 1)
        {!! Form::button('Inactive', [
            'type' => 'submit',
            'class' => 'btn btn-danger btn-xs',
            'title' => 'Inactive Creator',
            'onclick' => "return confirm('Do you want to inactive this creator?')",
            'name' => 'action',
            'value' => 'inact'
        ]) !!}
        @else
        {!! Form::button('Active', [
            'type' => 'submit',
            'class' => 'btn btn-success btn-xs',
            'title' => 'Activated Creator',
            'onclick' => "return confirm('Do you want to activated this creator?')",
            'name' => 'action',
            'value' => 'act'
        ]) !!}
        @endif
        @if($c->is_show == 1)
        {!! Form::button('Hide', [
            'type' => 'submit',
            'class' => 'btn btn-danger btn-xs',
            'title' => 'Inactive Creator',
            'onclick' => "return confirm('Do you want to hide this creator?')",
            'name' => 'action',
            'value' => 'hideact'
        ]) !!}
        @else
        {!! Form::button('Show', [
            'type' => 'submit',
            'class' => 'btn btn-success btn-xs',
            'title' => 'Activated Creator',
            'onclick' => "return confirm('Do you want to show this creator?')",
            'name' => 'action',
            'value' => 'showact'
        ]) !!}
        @endif
    </div>
    {!! Form::close() !!}
@else
    {!! Form::open(['route' => ['creator.destroy', $c->id], 'method' => 'delete']) !!}
    <div class='btn-group'>
        <a href="{{ route('influencer.edit', $c->id) }}" class='btn btn-default btn-xs' title="Add To Influencer">
            <i class="glyphicon glyphicon-star"></i>
        </a>
        <a href="{{ route('creator.edit', $c->id) }}" class='btn btn-default btn-xs' title="Edit Creator">
            <i class="glyphicon glyphicon-edit"></i>
        </a>
        @if($c->is_active == 1)
        {!! Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
            'type' => 'submit',
            'class' => 'btn btn-danger btn-xs',
            'title' => 'Inactive Creator',
            'onclick' => "return confirm('Do you want to inactive this creator?')",
            'name' => 'action',
            'value' => 'inact'
        ]) !!}
        @else
        {!! Form::button('<i class="glyphicon glyphicon-ok"></i>', [
            'type' => 'submit',
            'class' => 'btn btn-success btn-xs',
            'title' => 'Activated Creator',
            'onclick' => "return confirm('Do you want to activated this creator?')",
            'name' => 'action',
            'value' => 'act'
        ]) !!}
        @endif
        {{-- {!! Form::button('<i class="glyphicon glyphicon-remove"></i>', [
            'type' => 'submit',
            'class' => 'btn btn-danger btn-xs',
            'title' => 'Delete Creator',
            'onclick' => "return confirm('Do you want to delete this creator?')",
            'name' => 'action',
            'value' => 'del'
        ]) !!} --}}
    </div>
    {!! Form::close() !!}
@endif