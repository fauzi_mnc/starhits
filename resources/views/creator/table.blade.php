@section('css')
    @include('layouts.datatables_css')
@endsection

<table class="table" id="table">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th class="text-center">Image Source</th>
            <th class="text-center">Name</th>
            <th class="text-center">Email</th>
            <th class="text-center">Active</th>
            <th class="text-center">Status</th>
            @if(auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'legal' || auth()->user()->roles->first()->name == 'anr')
            <th class="text-center">Action</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @foreach($creator AS $c)
        <tr>
            <td data-sort="{{ strtotime($c->updated_at) }}" class="text-center">
                @php
                    $parsed = parse_url($c->image);
                    $path = $parsed['path'];
                    $path_parts = explode('/', $path);
                    $desired_output = $path_parts[0];
                @endphp
                @if($desired_output == 'uploads')
                <img src="{{ url('/').'/'.$c->image }}" class="img img-circle img-thumbnail" style="width:40px;height:40px;">
                @else
                <img src="{{ $c->image }}" class="img img-circle img-thumbnail" style="width:40px;height:40px;">
                @endif
            </td>
            <td class="text-center">
                @php
                    $parsed = parse_url($c->image);
                    $path = $parsed['path'];
                    $path_parts = explode('/', $path);
                    $desired_output = $path_parts[0];
                @endphp
                @if($desired_output == 'uploads')
                Uploaded
                @else
                Youtube
                @endif
            </td>
            <td>
                <b>
                {{ $c->name }}
                </b>
            </td>
            <td>{{ $c->email }}</td>
            <td>{{ ($c->is_active ==1) ? 'Ya' : 'Tidak'  }}</td>
            <td>{{ ($c->is_show == 1) ? 'Ya' : 'Tidak' }}</td>
            @if(auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'legal' || auth()->user()->roles->first()->name == 'anr')
            <td class="text-center">@include('creator.datatables_actions')</td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>

@section('scripts')
@include('layouts.datatables_js')
<script>
    $(document).ready(function () {
        var dataTable = $('#table').DataTable({
            @if(auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'legal' || auth()->user()->roles->first()->name == 'anr')
            dom             : 'Bfrtlip',
            @endif
            order           : [[ 0, "desc"]],
            processing      : true,
            serverMethod    : 'post',
            responsive      : true,
            autoWidth       : false,
            aLengthMenu     : [[10, 50, 100, 250, -1], [10, 50, 100, 250, 'All']],
            buttons         : [
                                @if(auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'legal' || auth()->user()->roles->first()->name == 'anr')
                                { 
                                    "extend" : 'create', 
                                    "text" : '<i class="fa fa-plus"></i> Add New',
                                    "className" : 'btn-primary',
                                    "action" : function( e, dt, button, config){ 
                                        window.location = "creator/create";
                                    }
                                }
                                @endif
                            ],
        });
        $(".alert-danger").fadeTo(3000, 1000).slideUp(1000, function(){
            $(".alert-danger").slideUp(1000);
        });
        $(".alert-success").fadeTo(3000, 1000).slideUp(1000, function(){
            $(".alert-success").slideUp(1000);
        });
        //lengthmenu -> add a margin to the right and reset clear 
        $(".dataTables_length").css('clear', 'none');
        $(".dataTables_length").css('margin-right', '20px');

        //info -> reset clear and padding
        $(".dataTables_info").css('clear', 'none');
        $(".dataTables_info").css('padding', '0');
        $("div").removeClass("ui-toolbar");

        $('#singer').on('change', function(){
            $('#album').val('');
            dataTable.search(this.value).draw(); 
        });
        $('#album').on('change', function(){
            $('#singer').val('');
            dataTable.search(this.value).draw();   
        });
        $('#status').on('change', function(){
            $('#singer').val('');
            $('#album').val('');
            dataTable.search(this.value).draw();   
        });
        $('.form-inline').find('.clear-filtering').on('click', function(e) {
            $('#singer').val('');
            $('#album').val('');
            $('#status').val('');
            dataTable.search(this.value).draw();
            e.preventDefault();
        });
    });
</script>
@endsection