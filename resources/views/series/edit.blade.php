@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Series</h2>
        <ol class="breadcrumb">
            <li>
                @if(auth()->user()->roles->first()->name === 'admin')
                <a href="{{route('admin.serie.index')}}">Series</a>
                @elseif(auth()->user()->roles->first()->name === 'user')
                <a href="{{route('user.serie.index')}}">Series</a>
                @else
                <a href="{{route('creator.serie.index')}}">Series</a>
                @endif
            </li>
            <li class="active">
                <strong>Edit</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Series</h5>
                </div>
                <div class="ibox-content">
                @if(auth()->user()->roles->first()->name === 'admin')
                    {!! Form::model($series, ['route' => ['admin.serie.update', $series->id], 'method' => 'put', 'class' => 'form-horizontal', 'id' => 'forms', 'files' => true]) !!}
                        @include('series.fields', ['formType' => 'edit'])
                    {!! Form::close() !!}
                @elseif(auth()->user()->roles->first()->name === 'user')
                    {!! Form::model($series, ['route' => ['user.serie.update', $series->id], 'method' => 'put', 'class' => 'form-horizontal', 'id' => 'forms', 'files' => true]) !!}
                        @include('series.fields', ['formType' => 'edit'])
                    {!! Form::close() !!}
                @else
                    {!! Form::model($series, ['route' => ['creator.serie.update', $series->id], 'method' => 'put', 'class' => 'form-horizontal', 'id' => 'forms', 'files' => true]) !!}
                        @include('series.fields', ['formType' => 'edit'])
                    {!! Form::close() !!}
                @endif
                </div>
            </div>

@endsection

