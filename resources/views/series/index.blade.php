@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Series Table</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Series</a>
            </li>
            <li class="active">
                <strong>Table</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Series</h5>
                    @if(auth()->user()->roles->first()->name === 'admin')
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{route('admin.serie.create')}}">Add New</a>
                    @elseif(auth()->user()->roles->first()->name === 'user')
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{route('user.serie.create')}}">Add New</a>
                    @else
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{route('creator.serie.create')}}">Add New</a>
                    @endif
                </div>

                <div class="ibox-content">
                    @include('series.table')
                </div>
            </div>
        </div>
    </div>
@endsection