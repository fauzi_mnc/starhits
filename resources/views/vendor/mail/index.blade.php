@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            Star Hits Indonesia
        @endcomponent
    @endslot
    <body style="margin:0px; background: #f8f8f8; ">
        <div style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;" width="100%">
            <div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
                <div style="padding: 40px; background: #fff;">
                    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tbody>
                            <tr>
                                <td style="border-bottom:1px solid #f6f6f6;">
                                    <h1 style="font-size:14px; font-family:arial; margin:0px; font-weight:bold;">
                                        Dear Star Hits Indonesia,
                                    </h1>
                                    <p style="margin-top:0px; color:#bbbbbb;">
                                        This is to let you know that you have received a new message from {{$email}}, following this message:
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:10px 0 30px 0;">
                                    <p>
                                        {{$message}}
                                    </p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
{{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset
{{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} Star Hits Indonesia 2018 All Right Reserved.
        @endcomponent
    @endslot
@endcomponent
