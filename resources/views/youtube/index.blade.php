@extends('layouts.crud')

@section('css')
<link href="{{ asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet">
@endsection

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-file-audio-o"></i> Youtube Analytics</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Youtube Analytics</a>
            </li>
            <li class="active">
                <strong>Table</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-heading">
                    Select Date Reports Youtube Analytics
                </div>
                <div class="panel-body" >
                    {!! Form::open(['route' => ['youtube.analytics.detail'], 'files' => true, 'class' => 'form-horizontal', 'id' => 'analyticsform']) !!}
                    {{-- <div class="col-md-12 col-sm-12" style="padding:20px;">
                        {!! Form::label('Type Reporting', '', ['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-md-4 col-sm-4" >
                            {!! Form::select('typereporting', $typereporting, '',['class' => 'form-control', 'id'=>'typereporting', 'required']) !!}
                        </div>
                    </div> --}}
                    <div id="selectmonth" class="col-md-12 col-sm-12" style="padding:20px; display: block;">
                        {!! Form::label('Month', '', ['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-md-4 col-sm-4" >
                            {!! Form::text('month', '', ['class' => 'form-control monthpicker', 'autocomplete'=>'off', 'id' => 'month']) !!}
                        </div>
                    </div>
                    <div id="selectyear" class="col-md-12 col-sm-12" style="padding:20px; display: block;">
                        {!! Form::label('Year', '', ['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-md-4 col-sm-4" >
                            {!! Form::text('year', '', ['class' => 'form-control yearpicker', 'autocomplete'=>'off', 'id' => 'year']) !!}
                        </div>
                    </div>
                    <!-- Submit Field -->
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            {!! Form::submit( 'Process', ['class' => 'btn btn-success', 'name' => 'submitbutton', 'value' => 'save'])!!}
                            <a href="javascript:window.history.back();" class="btn btn-default">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
    <script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.monthpicker').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                viewMode: 'months',
                minViewMode: 'months',
                format: 'mm'
            });
            $('.yearpicker').datepicker({
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true,
                viewMode: 'years',
                minViewMode: 'years',
                format: 'yyyy'
            });
            $('#typereporting').change(function(){
                if($(this).val() == 0){
                    $('#selectmonth').show();
                    $('#selectyear').show();
                }else if($(this).val() == 1) {
                    $('#selectmonth').show();
                    $('#selectyear').show()
                }else{
                    $('#selectmonth').hide();
                    $('#selectyear').show()
                }
            });
            $("#analyticsform").validate({
                rules: {
                    typereporting: {
                        required:true
                    },
                    month: {
                        required:true
                    },
                    year : {
                        required:true
                    }
                },
                messages:{
                    typereporting:{
                        required:"Silahkan pilih tipe report!"
                    },
                    month:{
                        required:"Wajib di isi!"
                    },
                    year:{
                        required:"Wajib di isi!"
                    }
                }
            });
        });
    </script>
@endsection