@section('css')
    @include('layouts.datatables_css')
    <link href="{{ asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet">
@endsection

<table class="table" id="table">
    <thead>
        <tr>
            <th class="text-center">Channel Name</th>
            <th class="text-center">Unit</th>
            <th class="text-center">Subscribers Week #1</th>
            <th class="text-center">Subscribers Week #2</th>
            <th class="text-center">Subscribers Week #3</th>
            <th class="text-center">Subscribers Week #4</th>
        </tr>
    </thead>
    <tbody>
        @php
            $i = 1;
        @endphp
        @foreach($chanalytics AS $cha)
        @php
        if(!empty(explode(',', $cha->subscribers)[0])){
            $subscribers1 = explode(',', $cha->subscribers)[0];
        }else{
            $subscribers1 = 0;
        }
        if(!empty(explode(',', $cha->subscribers)[1])){
            $subscribers2 = explode(',', $cha->subscribers)[1];
        }else{
            $subscribers2 = 0;
        }
        if(!empty(explode(',', $cha->subscribers)[2])){
            $subscribers3 = explode(',', $cha->subscribers)[2];
        }else{
            $subscribers3 = 0;
        }
        if(!empty(explode(',', $cha->subscribers)[3])){
            $subscribers4 = explode(',', $cha->subscribers)[3];
        }else{
            $subscribers4 = 0;
        }
        $sumsubsweek = $subscribers1+$subscribers2+$subscribers3+$subscribers4;
        
        @endphp
        <tr>
            <td>{{ $cha->name_channel }}</td>
            <td>{{ $cha->unit }}</td>
            <td class="text-center">{{ $subscribers1 }}</td>
            <td class="text-center">{{ $subscribers2 }}</td>
            <td class="text-center">{{ $subscribers3 }}</td>
            <td class="text-center">{{ $subscribers4 }}</td>
        </tr>
        @php
            $i++;
        @endphp
        @endforeach
    </tbody>
</table>

@section('scripts')
@include('layouts.datatables_js')
<script>
    $(document).ready(function () {
        var dataTable = $('#table').DataTable({
            @if(auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'legal' || auth()->user()->roles->first()->name == 'anr')
            dom             : 'Bfrtlip',
            @endif
            order           : [[ 1, "asc"]],
            processing      : true,
            responsive      : true,
            autoWidth       : false,
            aLengthMenu     : [[10, 50, 100, 250, -1], [10, 50, 100, 250, 'All']],
            columnDefs      : [{ "width": "15%", "targets": 1 }, { "width": "15%", "targets": 2 }, { "width": "15%", "targets": 3 }, { "width": "15%", "targets": 4 }],
            buttons         : [
                                        // { 
                                        //     "text" :'<i class="fa fa-file-excel-o"></i> CSV',
                                        //     "className" : 'btn-primary',
                                        //     'action' : function( e, dt, button, config){ 
                                        //         window.open("{{route('youtube.analytics.detail.export_csv', $date)}}", "_blank");
                                        //     }
                                        // },
                                        { 
                                            "text" :'<i class="fa fa-file-excel-o"></i> Excel',
                                            "className" : 'btn-primary',
                                            'action' : function( e, dt, button, config){ 
                                                window.open("{{route('youtube.analytics.detail.export_excell', $date)}}", "_blank");
                                            }
                                        },
                                        { 
                                            "text" :'<i class="fa fa-file-pdf-o"></i> PDF',
                                            "className" :'btn-primary',
                                            'action' : function( e, dt, button, config){ 
                                                window.open("{{route('youtube.analytics.detail.export_print', $date)}}", "_blank");
                                            }
                                        }
                            ],
        });
        $(".alert-danger").fadeTo(3000, 1000).slideUp(1000, function(){
            $(".alert-danger").slideUp(1000);
        });
        $(".alert-success").fadeTo(3000, 1000).slideUp(1000, function(){
            $(".alert-success").slideUp(1000);
        });
        //lengthmenu -> add a margin to the right and reset clear 
        $(".dataTables_length").css('clear', 'none');
        $(".dataTables_length").css('margin-right', '20px');

        //info -> reset clear and padding
        $(".dataTables_info").css('clear', 'none');
        $(".dataTables_info").css('padding', '0');
        $("div").removeClass("ui-toolbar");
    });
</script>
<script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script>
    $(document).ready(function(){
        $('.monthpicker').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            viewMode: 'months',
            minViewMode: 'months',
            format: 'mm'
        });
        $('.yearpicker').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            viewMode: 'years',
            minViewMode: 'years',
            format: 'yyyy'
        });
    });
</script>
@endsection