<?php
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-type: text/csv");
$filename = "Reporting-Youtube-Analytic_".date("Y-m-d").".csv";
header("Content-Disposition: attachment; filename=".$filename."");
header("Pragma: no-cache");
header("Expires: 0");

echo'<div style="width:100%; text-align:center;">
    <img style="width:350px;" src="{{ asset("img/hitsrecords.png") }}">
    <h2 style="margin:5px;">PT. Suara Mas Abadi</h2>';
if(auth()->user()->roles->first()->name === 'admin'){
    echo"<p>Data Master Lagu</p>";
}else{
    echo"<p>Daftar Lagu".auth()->user()->name."</p>";
}
echo"</div>";
echo"No,Judul,Album,Tanggal Release,Penyanyi / Publishing,Pencipta,ISRC,UPC,% Penyanyi,% Pencipta / Publishing, % Rights Recording, Country \n";
$n = 1;
foreach ($masterlagu as $m) {
    echo $n.",".$m->track_title.",".$m->release_title.",";

    if($m->release_date=="0000-00-00"){
        echo ",";
    }else{
    echo date("d-m-Y", strtotime($m->release_date)).",";
    }

    $sing = "";
    foreach($m->rolelagu as $key => $singer){
        if(!empty($singer->penyanyi)){
            $sing .= $singer->penyanyi->name_master." & ";
        }
    }
    echo rtrim($sing," & ").",";

    
    $song = "";
    foreach($m->rolelagu as $key => $songwriter){
        if(!empty($songwriter->pencipta)){
            $song .= $songwriter->pencipta->name_master." & ";
        }
    }
    echo rtrim($song," & ").",";
        
    echo $m->isrc.",".$m->upc.",".sprintf("%.0f%%", $m->percentage_penyanyi * 100).",".sprintf("%.0f%%", $m->percentage_pencipta * 100).",".sprintf("%.0f%%", $m->percentage_rights * 100).",";

    $coun = "";
    foreach($m->rolelagucountry as $key => $country){
        if(!empty($country->country)){
            $coun .= $country->country->country_name." & ";
        }
    }
    echo rtrim($coun," & ").",";

    echo "\n";
    $n++;
}
die;
?>