@extends('layouts.crud')
@section('css')
<link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />

<style>
    .kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
        margin: 0;
        padding: 0;
        border: none;
        box-shadow: none;
        text-align: center;
    }
    .kv-photo .file-input {
        display: table-cell;
        max-width: 220px;
    }
    .kv-reqd {
        color: red;
        font-family: monospace;
        font-weight: normal;
    }
    .wizard > .content > .body{
        position: relative !important;
    }
    strong{
        padding-right: 30px;
    }
</style>


@endsection

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Video Report</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Video Report</a>
            </li>
            <li class="active">
                <strong>Create</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')

        @php 
            $numF = 1;
        @endphp

        {!! Form::open(['route' => 'youtube.audits.report.upload', 'class' => 'form-horizontal', 'id' => 'creatorStore', 'files' => true]) !!}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Create Video Report</h5>
                </div>
                <div class="ibox-content">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            Video Report Information
                        </div>
                        <div class="panel-body">
                        
                        @foreach($partner as $partner)
                            <!-- Name Field -->
                            <div class="form-group">
                                {!! Form::label('yt_report', 'File Report :', ['class' => 'col-sm-2 control-label']) !!}

                                <div class="col-sm-3">
                                    {!! Form::file('yt_report[]', ['class' => 'form-control', 'id' => 'yt_report']) !!}
                                </div>
                                <div class="col-md-5">
                                    <div class="well well-sm">{{$partner->display_name}}</div>

                                    <input type="hidden" name="content_id[]" value="{{$partner->content_id}}">
                                </div>
                                <!-- <div class="col-sm-2">
                                    <span id="add_file" class="btn btn-success"><i class="fa fa-plus"></i> Add</span>
                                </div> -->
                            </div>
                        @endforeach

                       <!--  <div class="form-group">
                            <div id="ContainerFile"></div>
                        </div> -->
                    </div>
                </div>

                    
                     <!-- Submit Field -->
                    @if(auth()->user()->roles->first()->name === 'finance')
                    <div class="pull-right">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary', 'id'=>'btn-submits', 'form'=>'creatorStore']) !!}
                        <a href="{{route('video-yt.report.index')}}" class="btn btn-default">Cancel</a>
                    </div>
                    @else
                    <div class="pull-right">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary', 'id'=>'btn-submits', 'form'=>'creatorStore']) !!}
                        <a href="{{route('youtube.audits.report.index')}}" class="btn btn-default">Cancel</a>
                    </div>       
                    @endif

                    {!! Form::close() !!}
                </div>
            </div>
@endsection
@section('scripts')
<script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/chosen.jquery.min.js') }}"></script>
<script src="{{ asset('js/plugins/typeahed.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/tinymce.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

<script>
$('select').selectpicker();

$("#cover").fileinput({
    overwriteInitial: true,
    maxFileSize: 1500,
    showClose: false,
    showCaption: false,
    browseLabel: '',
    removeLabel: '',
    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-cover-errors-1',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img src="{{ asset('img/default_avatar_male.jpg') }}" alt="Your cover" style="width:160px">',
    layoutTemplates: {main2: '{preview} {remove} {browse}'},
    allowedFileExtensions: ["png"]
});
$("#cover2").fileinput({
    overwriteInitial: true,
    maxFileSize: 1500,
    showClose: false,
    showCaption: false,
    browseLabel: '',
    removeLabel: '',
    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-cover-errors-2',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img src="{{ asset('img/default_avatar_male.jpg') }}" alt="Your cover" style="width:160px">',
    layoutTemplates: {main2: '{preview} {remove} {browse}'},
    allowedFileExtensions: ["png"]
});

$("#porto_header").fileinput({
    overwriteInitial: true,
    maxFileSize: 1500,
    showClose: false,
    showCaption: false,
    browseLabel: '',
    removeLabel: '',
    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-photo-errors-1',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img src="{{ asset('img/default_avatar_male.jpg') }}" alt="Your photo" style="width:160px">',
    layoutTemplates: {main2: '{preview} {remove} {browse}'},
    allowedFileExtensions: ["png"]
});

$("#img_thumbnail").fileinput({
    overwriteInitial: true,
    maxFileSize: 1500,
    showClose: false,
    showCaption: false,
    browseLabel: '',
    removeLabel: '',
    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-photo-errors-1',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img src="{{ asset('img/default_avatar_male.jpg') }}" alt="Your photo" style="width:160px">',
    layoutTemplates: {main2: '{preview} {remove} {browse}'},
    allowedFileExtensions: ["png"]
});

@if(!empty($numF)) //Variabel Global fileER
    var x = <?=$numF?>;
@else 
    var x = 0;
@endif

$(document).ready(function(){
    initTinyMce();

    /*+++++++++++Tambah element Field fileer*/
    $("#add_file").click(function (e) {
        //e.preventDefault();

        // if(x <= MaxInputs){
            var list_file = '<label class="col-sm-2 control-label">File Report :</label>';

                list_file += '<div class="col-sm-8"><input class="form-control" id="yt_report" required="" name="yt_report[]" type="file"></div>';

                list_file += '<div class="col-sm-2" >';
                list_file += '<button id="'+ (x) +'" class="btn btn-danger remove-file" ><i class="fa fa-remove"></i></button>';
                list_file += '</div>';

            var btn = '<div class="row"><div class="col-xs-12 col-sm-7 col-md-7">&nbsp;</div><div class="col-xs-12 col-sm-1 col-md-1"  ></div></div>';

            var newIn = '<div class="row-file'+ x +'">' +
                        '' + list_file +
                        btn + '</div>';
            
            $('#ContainerFile').append(newIn);

            x++;
        // }else{
        //     alert('Max 10 Penyanyi');
        // }
        return false;
    });
    $("body").on("click",".remove-file", function(e){ //user click on remove text
        console.log(x);
            if( x > 1 ) {
                var choice = confirm("Are you sure to delete this item?");
                if(choice == true){
                    console.log(x);
                    var fieldNumx = this.id.valueOf();
                    var fieldIDx = ".row-file" + fieldNumx;
                    $(this).remove();
                    $(fieldIDx).remove();
                    //$(this).parent('div').remove(); //remove text box
                    x--; //decrement textbox
                }
            }
        return false;
    });
});

// init tiny mce
function initTinyMce(){
    tinymce.init({
        selector: "textarea",
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        },
        entity_encoding : "raw",
        element_format : 'html',
        allow_html_in_named_anchor: true,
        forced_root_block : 'p',
        file_picker_types: 'image',
        schema: 'html5',
        images_upload_credentials: true,
        automatic_uploads: false,
        theme: "modern",
        paste_data_images: true,
        plugins: [
        "advlist autolink lists link image media  charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime nonbreaking save table contextmenu directionality",
        "template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor | media image",
        image_advtab: true,
        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
            
            // Note: In modern browsers input[type="file"] is functional without 
            // even adding it to the DOM, but that might not be the case in some older
            // or quirky browsers like IE, so you might want to add it to the DOM
            // just in case, and visually hide it. And do not forget do remove it
            // once you do not need it anymore.
        
            input.onchange = function() {
                var file = this.files[0];
            
                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function () {
                // Note: Now we need to register the blob in TinyMCEs image blob
                // registry. In the next release this part hopefully won't be
                // necessary, as we are looking to handle it internally.
                var id = 'blobid' + (new Date()).getTime();
                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                var base64 = reader.result.split(',')[1];
                var blobInfo = blobCache.create(id, file, base64);
                blobCache.add(blobInfo);
        
                // call the callback and populate the Title field with the file name
                cb(blobInfo.blobUri(), { title: file.name });
                };
            };
            
            input.click();
        }
    });
}
// end tiny mce

// // youtube url parser to get id
// function youtube_parser(url){
//     var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
//     var match = url.match(regExp);
//     return (match&&match[7].length==11)? match[7] : false;
// }
// // end youtube url parser to get id

// /*$('#btn-check').click(function(){*/
// function getYoutube(){
//     var id_yt =$("#id-youtube").val();
//     var getVids = youtube_parser(id_yt);
//     checkUrlYoutube(getVids);
// }
    
// //});
//  // check id youtube
// function checkUrlYoutube(id_youtube) {
//     console.log(id_youtube);
//     $.ajax({
//         @if(auth()->user()->roles->first()->name === 'admin')
//             url: "{{route('admin.posts.check')}}",
//         @elseif(auth()->user()->roles->first()->name === 'user')
//             url: "{{route('user.posts.check')}}",
//         @else
//             url: "{{route('creator.posts.check')}}",
//         @endif
//         method: "GET", 
//         data: {
//         id: id_youtube
//     },
//     success: function(response){
//         console.log(response);
//         if(response.status == 'success'){
//             var data = response.result;
//             var tags = data.snippet.tags;
//             //console.log(data.snippet.description);

//             // set data if true
//             $('#id-youtube').val(data.id);
//             $('#title-preview').val(data.snippet.title);
//             $('.prev-video').removeClass('hide');
//             $('#embed-video').empty();
//             $('#embed-video').append(data.player.embedHtml);

//             if(formType != 'edit'){
//                 tinymce.activeEditor.setContent(data.snippet.description);
//                 $.each(tags, function(index, value){
//                     $('#tags-input').tagsinput('add', value);
//                 });
//             }
//             toastr.success(response.message);
//         }
//         else
//         {
//             toastr.error(response.message);
//         }
        
//         },
//         error: function(response){
//         toastr.error(response);
//     }
//     });
// }
// end check id youtube
$('.multinput').tagsinput({
    tagClass: 'label label-primary',
    maxTags: 5,
});
</script>
@endsection