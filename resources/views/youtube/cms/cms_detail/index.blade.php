@extends('layouts.crud')

@section('breadcrumb')
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script src="https://cdn.jsdelivr.net/morris.js/0.4.1/morris.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"> </script> 


<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Influencer</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Influencer</a>
            </li>
            <li class="active">
                <strong>View</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2"></div>
</div>
@endsection

@section('css')
@include('layouts.datatables_css')
@endsection 
<style type="text/css">
    /*#morrisBar, #morrisBar2{
          height:250px;
    }*/
    .morris-default-style{
        margin-top: -20px;
        font-size: 10px;
    }

    .emp-profile{
        padding: 3%;
        margin-top: 0;
        border-radius: 0.5rem;
        background: #fff;
    }
    .profile-img{
        margin-left: 0px;
    }

    .profile-head h3{
        color: #333;
    }
    .profile-head h5{
        color: #0062cc;
    }
    .profile-edit-btn{
        border: none;
        border-radius: 1.5rem;
        width: 70%;
        padding: 2%;
        font-weight: 600;
        color: #6c757d;
        cursor: pointer;
    }
    .proile-rating{
        font-size: 13px;
        color: #818182;
        margin-top: 20px;
    }
    .proile-rating span{
        color: #495057;
        font-size: 13px;
        font-weight: 600;
    }
    .profile-head .nav-tabs{
        margin-bottom:5%;
    }
    .profile-head .nav-tabs li{
        font-weight:600;
        border: none;
    }
    .profile-head .nav-tabs li.active{
        border: none;
        border-bottom:2px solid #0062cc;
    }
    .profile-work{
        padding: 0;
        margin-top: -100px;
        margin-left: 0px;
    }
    .profile-work p{
        font-size: 12px;
        color: #818182;
        font-weight: 600;
        margin-top: 10%;
    }
    .profile-work a{
        text-decoration: none;
        color: #495057;
        font-weight: 600;
        font-size: 14px;
    }
    .profile-work ul{
        list-style: none;
    }
    .profile-tab label{
        font-weight: 600;
    }
    .profile-tab p{
        font-weight: 600;
        color: #0062cc;
    }
    #home{

    }

    .social-media-wrapper {
        padding: 0;
    }

    .social-media {
        list-style-type: none;
        padding: 0;
    }

    .social-media {
        display: inline-block;
        margin: 2px 3px;
        width: 45px;
        height: 45px;
        text-align: center;
        background: #878787;
        border-radius: 45px;
        color: #ffffff;
    }

    .social-media:hover {
        background: #4c3480;
    }
    .social-media {
        display: inline-block;
        height: 45px;
        width: 45px;
        line-height: 45px;
        font-size: 45px;
    }
    .social-media i.fa {
        color: #ffffff;
        font-size: 25px;
        line-height: 45px;
    }
    .panel , .panel-heading{
        border-radius: 0px;
    }
    .engagement-rate>.panel>.panel-body>center{
        text-align: center;
        font-size: 42px;
    }
    .num-statis{
        font-size: 25px;
    }
</style>

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
<div class="row">
    <div class="panel">
        <div class="panel-body">
            <div class=" emp-profile">
                <div class="row">
                    <div class="col-md-3">
                        <div class="profile-img">
                            <img src="{{$datas->details->profile_picture_ig}}" alt="" height="180" />
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="profile-head">
                            <h3>
                                {{$datas->name}}
                            </h3>
                            <h5>
                                {{$datas->details->business_category_name}}
                            </h5>
                            <!-- <p class="proile-rating"> 
                                <span>Biography :</span>
                                <?php echo str_replace("?"," ",$datas->details->biography) ?>
                            </p> -->
                            <br><br>

                            <p>
                                <a class="social-media" href="https://instagram.com/{{$datas->details->username}}">
                                    <i class="fa fa-instagram " aria-hidden="true"></i> 
                                </a>
                                <b>{{$datas->details->username}}</b>                       
                            </p>
                            <p>
                                <a class="social-media" href="{{$datas->details->external_url}}">
                                    <i class="fa fa-globe " aria-hidden="true"></i> 
                                </a>                      
                                <b>{{$datas->details->external_url}}</b>
                            </p>
                        </div>
                    </div>
                </div>
                <br>        
                <div class="row">
                    <div class="col-md-8">
                        <div class="panel panel-default">
                            <div class="panel-heading">Instagram Statistics</div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-4 num-statis">
                                            {{bd_nice_number($datas->details->followers_ig)}}
                                        </div>
                                        <div class="col-md-4 num-statis">
                                            {{bd_nice_number($datas->details->avglike)}}
                                        </div>
                                        <div class="col-md-4 num-statis">
                                            {{bd_nice_number($datas->details->avgcomment)}}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <i class="fa fa-user"></i> Followers
                                        </div>
                                        <div class="col-md-4">
                                            <i class="fa fa-thumbs-o-up"></i> Avg Like
                                        </div>
                                        <div class="col-md-4">
                                            <i class="fa fa-comments"></i> Avg Comment
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 engagement-rate">
                        <div class="panel panel-default">
                            <div class="panel-heading">Engagement Rate</div>
                            <div class="panel-body">
                                <center><b>{{number_format($datas->details->engagement, 2) }}%</b></center>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Engagement Rate</div>
                            <div class="panel-body">
                                <div class="col-md-2"> Rp. {{number_format($datas->instagram_video_posting_rate,2)}},- </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-2"></div>
                                <div class="col-md-2"></div>
                            </div>
                        </div>
                    </div>

                </div>   

                    

                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class=" panel-heading">Follower Growth</div>
                            <div class=" panel-body"><div id="morrisBar" style="height: 250px;"></div></div>
                        </div>                
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class=" panel-heading">Engagements</div>
                            <div class=" panel-body"><div id="morrisBar2" style="height: 250px;"></div></div>
                        </div>                
                    </div>
                </div>

            </div>
        </div>

        
        
    </div>        
</div>


<script>

$( document ).ready(function() {

    $.ajax({
        type: 'GET',
        url: '{{route("user.cms.apidetail")}}',
        dataType:'JSON',
        success: function(response){
            console.log(response); // Native return is as required
            Morris.Line({
                element: 'morrisBar',
                resize: true,
                data: response,
                xkey: 'date',
                ykeys: ['views','likes','dislike','comments','shares','cpm','cardClicks','monetizedPlaybacks'],
                labels: ['views','likes','dislike','comments','shares','cpm','cardClicks','monetizedPlaybacks'],
                fillOpacity: 0.7,
                smooth: true,
                pointFillColors: ['#ffffff'],
                pointStrokeColors: ['#2c82c9','e74c3c','e72c3c','e44c3a','e44s2c','e44b3c','e44a3c','e44d3c'],
                lineColors: ['#2c82c9','e74c3c','e72c3c','e44c3a','e44s2c','e44b3c','e44a3c','e44d3c'],
                hideHover: 'auto',
          });

          /* Morris.Line({
                element: 'morrisBar2',
                data: response,
                xkey: 'created_at',
                ykeys: ['engagement'],
                labels: ['engagement'],
                fillOpacity: 0.7,
                smooth: true,
                resize: true,
                hideHover: 'auto',
                //lineColors: ['#2c82c9', '#ec644b']
                pointFillColors: ['#ffffff'],
                pointStrokeColors: ['#d64541'],
                lineColors: ['#e74c3c'],
          });*/
        }
    });
});
/*var chart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [{
            label: 'My First dataset',
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: [0, 10, 5, 2, 20, 30, 45]
        }]
    },
    options: {}
});*/

/*var chart = new Chart(ctx2, {
    type: 'line',
    data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [{
            label: 'My First dataset',
            backgroundColor: '#2c82c9',
            borderColor: '#3498db',
            data: [0, 10, 5, 2, 20, 30, 45]
        }]
    },
    options: {}
});*/

/*$.getJSON('{{route("influencer.api",$datas->id)}}', function (json) {
    var jason_data = JSON.stringify(json);
    var jasonstuff = (jason_data.replace(/\"/g, "")); 
    //console.log('data'.jason_data);

    Morris.Line({
        element: 'mydiv',
        data: json,
        xkey: ['day'],
        ykeys: ['pageviews'],
        labels: ['PageViews'],
        parseTime : false
    });
});*/

/*$.ajax({
    type: 'GET',
    url: '{{route("influencer.api",$datas->id)}}',
    dataType:'JSON',
    success: function (response) {
        $.each(response, function(i, json){
            console.log("data : "+json.created_at);
            Morris.Line({
                element: 'myChart2',
                data: response,
                xkey: ['created_at'],
                ykeys: ['engagement'],
                labels: ['Engagement'],
                parseTime : false
            });
        });
    } 
});*/

</script>
    @section('scripts')
        @include('layouts.datatables_js')
        <script>    
            $(document).ready(function(){
                $("#home").tab('show');
            });
</script>
    @endsection


@endsection