@section('css')
    @include('layouts.datatables_css')
@endsection
<div class="table-responsive">
<table class="table" id="table">
    <thead>
        <tr>
            <th class="text-center">Name</th>
            <th class="text-center">Associated On</th>
            <th class="text-center">Channels</th>
            <th class="text-center">Views (CMS)</th>
            <th class="text-center">Ads Views </th>
            <th class="text-center">EstimateWatch</th>
            <th class="text-center">Subscribers</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $channel=0;
        $views=0;
        $watch=0;
        $subs=0;
        $monetizedPlaybacks=0;
        
        ?>
        @foreach($cmslist AS $c)
        <?php
            $channel=$channel+$c->countchannel;
            $views=$views+$c->countviews;
            $watch=$watch+$c->EstimateWatch;
            $subs=$subs+$c->countsub;
            $monetizedPlaybacks=$monetizedPlaybacks+$c->monetizedPlaybacks;
        
        ?>

        <tr>
            <td>
                <a href="{{$c->cms->content_id}}">
                    <span class="small"><b>
                    {{ $c->cms->display_name }}
                    </b></span>
                </a>
            </td>
            <td class="text-center"><span class="small">{{ date('d F Y',strtotime($c->channel->publishat)) }}</span></td>
            <td class="text-center">
                <span class="small">{{ bd_nice_number($c->countchannel) }}</span>
            </td>
             <td class="text-center"><span class="small">{{ bd_nice_number($c->countviews) }}</span></td>
             <td class="text-center"><span class="small">{{ bd_nice_number($c->monetizedPlaybacks) }}</span></td>

            <td class="text-center"><span class="small">{{ bd_nice_number($c->EstimateWatch) }}</span></td>
            <td class="text-center"><span class="small">{{ bd_nice_number($c->countsub) }}</span></td>
            
        </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="2">TOTAL
            </td>
            <td class="text-center">
                <span class="small">{{ bd_nice_number($channel) }}</span>
            </td>
             <td class="text-center"><span class="small">{{ bd_nice_number($views) }}</span></td>
             <td class="text-center"><span class="small">{{ bd_nice_number($monetizedPlaybacks) }}</span></td>
            <td class="text-center"><span class="small">{{ bd_nice_number($watch) }}</span></td>
            <td class="text-center"><span class="small">{{ bd_nice_number($subs) }}</span></td>
            
        </tr>
    </tfoot>
</table>
</div>

@section('scripts')
@include('layouts.datatables_js')
<script>
    $(document).ready(function () {
        var dataTable = $('#table').DataTable({
            dom             : 'rtlip',
            // "language"      :   {
            //                         "emptyTable":   '<div class="text-center"><h1>Grow With Us</h1><h2>We need to connect to your YouTube Account.</h2><p>Your data &amp; privacy is our prime priority. We will collect the following data only</p> <section id="busareas" class="wrapper"><div class="inner"><div class="container"><div class="row"><div class="two columns"> <i class="fa fa-user fa-5x"></i><hr /><p class="text-box small">Your Name</p></div><div class="two columns"> <i class="fa fa-envelope fa-5x"></i><hr /><p class="text-box small">Email Address</p></div><div class="two columns"> <i class="fa fa-line-chart fa-5x"></i><hr /><p class="text-box small">Change Statistic</p></div><div class="two columns"> <i class="fa fa-info-circle fa-5x"></i><hr /><p class="text-box small">Info Statistic</p></div></div></div></div> </section><div style="margin:10px 0;"> <button class="btn btn-primary btn-lg">Authenticate Your Channel</button></div><p class="small">Authenticate Youtube CMS with Google Account, Please Connect Your Channel</p></div>',
            //                     },
            order           : [[ 0, "desc"]],
            processing      : true,
            serverMethod    : 'post',
            responsive      : true,
            autoWidth       : false,
            aLengthMenu     : [[10, 50, 100, 250, -1], [10, 50, 100, 250, 'All']],
            buttons         : [
                                @if(auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'legal' || auth()->user()->roles->first()->name == 'anr')
                                { 
                                    "extend" : 'create', 
                                    "text" : '<i class="fa fa-plus"></i> Add New',
                                    "className" : 'btn-primary',
                                    "action" : function( e, dt, button, config){ 
                                        window.location = "mastersinger/create";
                                    }
                                }
                                @endif
                            ],
        });
        $(".alert-danger").fadeTo(3000, 1000).slideUp(1000, function(){
            $(".alert-danger").slideUp(1000);
        });
        $(".alert-success").fadeTo(3000, 1000).slideUp(1000, function(){
            $(".alert-success").slideUp(1000);
        });
        //lengthmenu -> add a margin to the right and reset clear 
        $(".dataTables_length").css('clear', 'none');
        $(".dataTables_length").css('margin-right', '20px');

        //info -> reset clear and padding
        $(".dataTables_info").css('clear', 'none');
        $(".dataTables_info").css('padding', '0');
        $("div").removeClass("ui-toolbar");

        $('#singer').on('change', function(){
            $('#album').val('');
            dataTable.search(this.value).draw(); 
        });
        $('#album').on('change', function(){
            $('#singer').val('');
            dataTable.search(this.value).draw();   
        });
        $('#status').on('change', function(){
            $('#singer').val('');
            $('#album').val('');
            dataTable.search(this.value).draw();   
        });
        $('.form-inline').find('.clear-filtering').on('click', function(e) {
            $('#singer').val('');
            $('#album').val('');
            $('#status').val('');
            dataTable.search(this.value).draw();
            e.preventDefault();
        });
    });
</script>
@endsection