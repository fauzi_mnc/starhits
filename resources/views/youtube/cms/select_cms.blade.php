@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/paper/bootstrap.min.css" rel="stylesheet">
@endsection
@extends('layouts.crud')
@section('contentCrud')
@include('flash::message')
    <div class="container">
        <div class="text-center">
            <h1>Connect CMS</h1>
            <p>Select CMS to Authenticate</p>
        </div>
        {!! Form::open(['route' => ['update.connect.cms'], 'files' => true, 'class' => 'form-horizontal', 'id' => 'example-form']) !!}
        <div class="selectall" style="margin:30px auto;">
            <div><input type="checkbox" name="selectall1" id="selectall1" class="all" value="1"> <label for="selectall1">Select all</label></div>
            <hr style="border-color:#d1d1d1;">
            @foreach($cmslist AS $l)
            <div><input type="checkbox" name="check[]" id="check{{$l->id}}" value="{{$l->id}}"> <label for="check{{$l->id}}">{{$l->display_name}}</label></div>
            @endforeach
            <div class="text-center">
                <input type="submit" class="btn btn-primary" value="Connect">
            </div>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
@section('scripts')
    <script src="{{asset('js/jquery.checkboxall-1.0.min.js')}}"></script>
    <script>
		$('.selectall').checkboxall();
    </script>
@endsection
