@extends('layouts.crud')
@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Authentication</h2>
        <ol class="breadcrumb">
            <li>
                <strong>Youtube Authentication</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="text-center">
            <h1>Grow With Us</h1>
            <h2>We need to connect to your YouTube Account.</h2>
            <p>Your data &amp; privacy is our prime priority. We will collect the following data only</p>
            <section id="busareas" class="wrapper">
                <div class="inner">
                    <div class="container">
                        <div class="row">
                            <div class="two columns">
                                <i class="fa fa-user fa-5x"></i>
                                <hr />
                                <p class="text-box small">Your Name</p>
                            </div>
                            <div class="two columns">
                                <i class="fa fa-envelope fa-5x"></i>
                                <hr />
                                <p class="text-box small">Email Address</p>
                            </div>
                            <div class="two columns">
                                <i class="fa fa-line-chart fa-5x"></i>
                                <hr />
                                <p class="text-box small">Change Statistic</p>
                            </div>
                            <div class="two columns">
                                <i class="fa fa-info-circle fa-5x"></i>
                                <hr />
                                <p class="text-box small">Info Statistic</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <div style="margin:10px 0;">
                <a href="{{url('user/auth/youtube')}}"><button class="btn btn-primary btn-lg">Authenticate Your Channel</button></a>
            </div>
            <p class="small">Authenticate Youtube CMS with Google Account, Please Connect Your Channel</p>
        </div>
    </div>
@endsection
