@extends('layouts.crud')

@section('css')
<link href="{{ asset('css/plugins/switchery/switchery.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/bootstrap-select.min.css') }}" rel="stylesheet">
<style>
.wizard > .content > .body{
    position: relative !important;
}
strong{
    padding-right: 30px;
}
</style>
@endsection

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Creator</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Creator</a>
            </li>
            <li class="active">
                <strong>Setting</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Setting
            </div>
            <div class="panel-body">
                {!! Form::model($user, ['route' => ['self.creator.update', $user->id, $context], 'method' => 'put', 'class' => 'form', 'files' => true, 'id' => 'example-form']) !!}
                <div>
                    @if($context == 'personal')
                    <input type="hidden" value="personal" name="context">
                    <h3>Personal Information</h3>
                    <section class="">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    {!! Form::label('name', 'Full Name:', []) !!}
                                    {!! Form::text('name', null, ['class' => 'form-control required']) !!}
                                    
                                </div>
                                <div class="form-group">
                                    {!! Form::label('phone', 'Phone Number:', []) !!}
                                    {!! Form::text('phone', null, ['class' => 'form-control required']) !!}
                                    
                                </div>
                                <div class="form-group">
                                    {!! Form::label('country', 'Country:', []) !!}
                                    {!! Form::select('country', $countries, null, ['class' => 'form-control', 'placholder' => 'Select Country']) !!}
                                    
                                </div>
                                <div class="form-group">
                                    {!! Form::label('password', 'Password:', []) !!}
                                    
                                    <div class="input-group">
                                        {!! Form::password('password', ['class' => 'form-control required']) !!}
                                        <span class="input-group-btn"> 
                                            <button type="button" class="btn btn-warning pull-right" id="reset-pwd">Reset</button>
                                        </span>
                                    </div>    
                                    
                                </div>
                                <div class="form-group">
                                    {!! Form::label('username', 'Username Instagram:', []) !!}
                                    {!! Form::text('username', $user->detailInfluencer->username, ['class' => 'form-control required']) !!}
                                    <br>
                                    <button type="button" class="btn btn-primary pull-right" id="btn-check">Check</button>                                            
                                </div>
                                <div class="form-group" id="instagram-profile">
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    {!! Form::label('category', 'Creator Category:', []) !!}
                                    {!! Form::select('category[]', $categories, explode(',', $user->detailInfluencer->category), ['class' => 'form-control selectpicker', 'multiple']) !!}
                                    
                                </div>
                                <div class="form-group">
                                    {!! Form::label('email', 'Email:', []) !!}
                                    {!! Form::email('email', null, ['class' => 'form-control required']) !!}
                                    
                                </div>
                                <div class="form-group">
                                    {!! Form::label('gender', 'Gender:', []) !!}
                                    <br>
                                    <label class="radio-inline">
                                        <input type="radio" name="gender" value="F" {{ ($user->gender == 'F') ? 'checked' : '' }}>Female
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="gender" value="M" {{ ($user->gender == 'M') ? 'checked' : '' }}>Male
                                    </label>
                                    
                                </div>
                                <div class="form-group">
                                    {!! Form::label('city', 'Town/City:', []) !!}
                                    {!! Form::select('city', $cities, null, ['class' => 'form-control selectpicker', 'placholder' => 'Select City', 'data-live-search' => 'true', 'data-size' => 7]) !!}
                                    
                                </div>
                                <div class="form-group">
                                    {!! Form::label('is_active', 'Is Active:', []) !!}
                                    <br>
                                    {!! Form::checkbox('is_active', null, ($user->detailInfluencer->is_active == 1) ? 'on' : '',['class' => 'form-control js-switch required', 'data-switchery' => true]) !!}
                                
                                </div>
                            </div>
                        </div>
                    </section>
                    @elseif($context == 'rate_card')
                    <input type="hidden" value="rate_card" name="context">
                    <h3>Rate Card</h3>
                    <section class="">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    {!! Form::label('instagram_photo_posting_rate', 'Harga Posting Photo Instagram:', []) !!}
                                    {!! Form::text('instagram_photo_posting_rate', (empty($user->detailInfluencer->instagram_photo_posting_rate)) ? " " : $user->detailInfluencer->instagram_photo_posting_rate, ['class' => 'form-control']) !!}
                                    
                                </div>
                                <div class="form-group">
                                    {!! Form::label('instagram_video_posting_rate', 'Harga Posting Video Instagram:', []) !!}
                                    {!! Form::text('instagram_video_posting_rate', (empty($user->detailInfluencer->instagram_video_posting_rate)) ? " " : $user->detailInfluencer->instagram_video_posting_rate, ['class' => 'form-control']) !!}
                                    
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    {!! Form::label('instagram_story_posting_rate', 'Harga Posting Story Instagram:', []) !!}
                                    {!! Form::text('instagram_story_posting_rate', (empty($user->detailInfluencer->instagram_story_posting_rate)) ? " " : $user->detailInfluencer->instagram_story_posting_rate, ['class' => 'form-control']) !!}
                                    
                                </div>
                                <div class="form-group">
                                    {!! Form::label('youtube_posting_rate', 'Harga Posting Youtube:', []) !!}
                                    {!! Form::text('youtube_posting_rate', (empty($user->detailInfluencer->youtube_posting_rate)) ? " " : $user->detailInfluencer->youtube_posting_rate, ['class' => 'form-control']) !!}
                                    
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    {!! Form::label('ig_highlight_rate', 'Harga Instagram Highlight:', []) !!}
                                    {!! Form::text('ig_highlight_rate', (empty($user->detailInfluencer->ig_highlight_rate)) ? " " : $user->detailInfluencer->ig_highlight_rate, ['class' => 'form-control']) !!}
                                    
                                </div>
                            </div>
                            
                            @if(auth()->user()->hasRole('admin'))
                            <div class="col-lg-6">
                                <div class="form-group">
                                    {!! Form::label('package_rate', 'Harga Package:', []) !!}
                                    {!! Form::text('package_rate', (empty($user->detailInfluencer->package_rate)) ? " " : $user->detailInfluencer->package_rate, ['class' => 'form-control']) !!}
                                    
                                </div>
                            </div>
                            @endif
                            
                        </div>
                    </section>
                    @elseif($context == 'payment')
                    <input type="hidden" value="payment" name="context">
                    <h3>Payment Information</h3>
                    <section>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    {!! Form::label('bank_id', 'Bank:', []) !!}
                                    
                                    {!! Form::select('bank_id', $banks,  (empty($user->detailInfluencer->bank_id)) ? " " : $user->detailInfluencer->bank_id, ['class' => 'form-control selectpicker', 'data-live-search' => 'true', 'data-size' => 7]) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('bank_account_number', 'Bank Account Number:', []) !!}
                                    {!! Form::text('bank_account_number', (empty($user->detailInfluencer->bank_account_number)) ? " " : $user->detailInfluencer->bank_account_number, ['class' => 'form-control required']) !!}
                                    
                                </div>
                                <div class="form-group">
                                    {!! Form::label('bank_holder_name', 'Bank Holder Name:', []) !!}
                                    {!! Form::text('bank_holder_name',  (empty($user->detailInfluencer->bank_holder_name)) ? " " : $user->detailInfluencer->bank_holder_name, ['class' => 'form-control required']) !!}
                                    
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    {!! Form::label('bank_location', 'Bank Location:', []) !!}
                                    {!! Form::text('bank_location',  (empty($user->detailInfluencer->bank_location)) ? " " : $user->detailInfluencer->bank_location, ['class' => 'form-control']) !!}
                                    
                                </div>
                                <div class="form-group">
                                    {!! Form::label('npwp', 'NPWP:', []) !!}
                                    {!! Form::text('npwp',  (empty($user->detailInfluencer->npwp)) ? " " : $user->detailInfluencer->npwp, ['class' => 'form-control required']) !!}
                                    
                                </div>
                            </div>
                        </div>
                    </section>
                    @elseif($context == 'social_media')
                    <input type="hidden" value="social_media" name="context">
                    <h3>Social Media</h3>
                    <section>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="input-group m-b"><span class="input-group-addon"><i class="fa fa-instagram"></i></span>
                                    <input type="text" id="username_instagram" name="socials[]" value="{{ ($ig) ? $ig->url : '' }}" placeholder="username" class="form-control" maxlength="100">
                                    <span class="input-group-btn"> 
                                            <button type="button" class="btn btn-primary pull-right" id="btn-check-instagram">Check</button>
                                        </span>
                                </div>
                                <div id="instagram-profile">
                                </div>
                                <div class="input-group m-b"><span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                    <input type="text" name="socials[]" value="{{ ($fb) ? $fb->url : '' }}" placeholder="https://www.facebook.com/username" class="form-control" maxlength="100">
                                </div>
                                <div class="input-group m-b"><span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                    <input type="text" name="socials[]" value="{{ ($tw) ? $tw->url : '' }}" placeholder="https://twitter.com/username" class="form-control" maxlength="100">
                                </div>
                                <div class="input-group m-b"><span class="input-group-addon"><i class="fa fa-google-plus"></i></span>
                                    <input type="text" name="socials[]" value="{{ ($gp) ? $gp->url : '' }}" placeholder="https://plus.google.com/u/0/username" class="form-control" maxlength="100">
                                </div>
                            </div>
                        </div>
                    </section>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary">Save Change</button>
                <a href="{{route('self.creator.edit', [auth()->id(), 'personal'])}}" class="btn btn-default">Cancel</a>
                </form>
            </div>

        </div>
        
    </div>
</div>
@endsection


@section('scripts')
    <script src="{{ asset('js/plugins/switchery/switchery.js') }}"></script>
    <script src="{{ asset('js/plugins/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('js/plugins/typeahed.min.js') }}"></script>
    
    <script>
        $(document).ready(function(){
            var elem = document.querySelector('.js-switch');
            var switchery = new Switchery(elem, { disabled: true });
    
            $.get("{{ route('bank.index') }}", function(data){
                
                $("#bank_id_display").typeahead({ 
                    source:data.result,
                    afterSelect: function (item) {
                        $('#bank_id').val(item.id);
                    }
                });
            },'json');
    
        });
    
    @if($context == 'personal')
    $('#password').attr('disabled', true);
    $('#reset-pwd').click(function(){
        $('#password').val('');
        $('#password').attr('disabled', false);
    });
    @endif

    @if($context == 'social_media')
    $('#btn-check-instagram').click(function(){
        var username = $('#username_instagram').val().substr($('#username_instagram').val().lastIndexOf('/') + 1);
        $.get( "{{route('influencer.instagram.check')}}", { username:  username}, function(data) {
            if(data.result == false){
                $('#instagram-profile').prepend(
                    '<div class="alert alert-danger">Invalid username Instagram</div>'
                );

                setTimeout(() => {
                    $('.alert-danger').remove();
                }, 5000);
                
            } else {
                $('#instagram-profile').empty();
                $('#instagram-profile').append(
                    '<div class="feed-element" id="element-instagram">'+
                        '<a href="#" class="pull-left">'+
                            '<img alt="image" class="img-circle" src="'+data.result.profilePicture+'" style="height:100px;width:100px;">'+
                        '</a>'+
                        '<div class="media-body">'+
                            '<h3>'+data.result.userName+'</h3> <br>'+
                            '<strong style="padding: 0px 10px;">'+data.result.mediaCount+' post</strong><strong style="padding: 0px 10px;">'+data.result.followers+' followers</strong><strong style="padding: 0px 10px;">'+ data.result.following +' following</small></strong>'+
                        '</div>'+
                    '</div>'
                );
                $('#username_instagram').parent().parent().find('.error').remove();
            }
        });

    });

    $('form').submit(function(e){
        
        if($('#username_instagram').val() != '' && $('#element-instagram').length == 0){
            $('#username_instagram').parent().parent().find('.error').remove();
            $('#username_instagram').parent().parent().prepend('<label class="error">Please Click Check Button!</label>');
            e.preventDefault();
        } else {
            $('form').submit();
        }
        
        return false;
    });
    @endif

    @if($user->detailInfluencer)
    if($('#username_instagram').val() != ''){
        var username = $('#username_instagram').val().substr($('#username_instagram').val().lastIndexOf('/') + 1);
        $.get( "{{route('influencer.instagram.check')}}", { username:  username}, function(data) {
            if(data.result == false){
                $('#instagram-profile').prepend(
                    '<div class="alert alert-danger">Invalid username Instagram</div>'
                );

                setTimeout(() => {
                    $('.alert-danger').remove();
                }, 5000);
                
            } else {
                $('#username_instagram').val(data.result.userName);
                $('#instagram-profile').empty();
                $('#instagram-profile').append(
                    '<div class="feed-element" id="element-instagram">'+
                        '<a href="#" class="pull-left">'+
                            '<img alt="image" class="img-circle" src="'+data.result.profilePicture+'" style="height:100px;width:100px;">'+
                        '</a>'+
                        '<div class="media-body">'+
                            '<h3>'+data.result.userName+'</h3> <br>'+
                            '<strong style="padding: 0px 10px;">'+data.result.mediaCount+' post</strong>  <strong style="padding: 0px 10px;">'+data.result.followers+' followers</strong>  <strong style="padding: 0px 10px;">'+ data.result.following +' following</small></strong>'+
                        '</div>'+
                    '</div>'
                );
            }
        });
    }
    @endif
    </script>
@endsection