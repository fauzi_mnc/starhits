@section('css')
    @include('layouts.datatables_css')
@endsection

<table class="table" id="table">
    <thead>
        <tr>
            <th class="text-center">CMS</th>
            <th class="text-center">CMS ID</th>
            <th class="text-center">Total Video</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $no =1; ?>
        @foreach($cms AS $data)
            <tr>
                <td>{{ $data->display_name }}</td>
                <td>{{ $data->content_id }}</td>
                <td align="right">{{ number_format($data->total_video) }}</td>
                <td class="text-center"><a href="{{route('youtube.audits.cms',$data->content_id)}}" class="btn btn-primary"><i class="fa fa-eye"></i></a></td>
            </tr>
            <?php $no++; ?>
        @endforeach
    </tbody>
</table>

@section('scripts')
@include('layouts.datatables_js')
<script>
    $(document).ready(function () {
        var dataTable = $('#table').DataTable({
            @if(auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'legal' || auth()->user()->roles->first()->name == 'anr')
            dom             : 'frtlip',
            @endif
            order           : [[ 0, "desc"]],
            processing      : true,
            serverMethod    : 'post',
            responsive      : true,
            autoWidth       : false,
            aLengthMenu     : [[10, 50, 100, 250, -1], [10, 50, 100, 250, 'All']],
            buttons         : false,
        });
        $(".alert-danger").fadeTo(3000, 1000).slideUp(1000, function(){
            $(".alert-danger").slideUp(1000);
        });
        $(".alert-success").fadeTo(3000, 1000).slideUp(1000, function(){
            $(".alert-success").slideUp(1000);
        });
        //lengthmenu -> add a margin to the right and reset clear 
        $(".dataTables_length").css('clear', 'none');
        $(".dataTables_length").css('margin-right', '20px');

        //info -> reset clear and padding
        $(".dataTables_info").css('clear', 'none');
        $(".dataTables_info").css('padding', '0');
        $("div").removeClass("ui-toolbar");

        $('#singer').on('change', function(){
            $('#album').val('');
            dataTable.search(this.value).draw(); 
        });
        $('#album').on('change', function(){
            $('#singer').val('');
            dataTable.search(this.value).draw();   
        });
        $('#status').on('change', function(){
            $('#singer').val('');
            $('#album').val('');
            dataTable.search(this.value).draw();   
        });
        $('.form-inline').find('.clear-filtering').on('click', function(e) {
            $('#singer').val('');
            $('#album').val('');
            $('#status').val('');
            dataTable.search(this.value).draw();
            e.preventDefault();
        });
    });
</script>
@endsection