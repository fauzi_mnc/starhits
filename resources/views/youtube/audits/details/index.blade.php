@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Youtube Audits Report</h2>
        <ol class="breadcrumb">
            <li>
                Youtube Audits Report
            </li>
            <li class="active">
                <strong>Table</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Youtube Audits Report</h5>
                </div>
                <div class="ibox-content">
                    @include('youtube.audits.home.table')
                </div>
            </div>
        </div>
    </div>
@endsection
