@section('css')
    @include('layouts.datatables_css')
@endsection

<a class="btn btn-warning pull-left" style="margin-top: -10px;margin-bottom: 5px" href="{{route('youtube.audits.cms')}}"><i class="fa fa-arrow-left"></i> Back</a>

<table class="table" id="table">
    <thead>
        <tr>
            <th class="text-center">Upload Date</th>
            <th class="text-center">GrandTotal</th>
            <th class="text-center">active content</th>
            <th class="text-center">Blank</th>
            <th class="text-center">Block</th>
            <th class="text-center">Track</th>
            <th class="text-center">Monetized Owner</th>
            <th class="text-center">Split Monetized</th>
            <th class="text-center">% Not activated</th>
            <th class="text-center">% Copyright Claim</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        @php 
            $no =1;
        @endphp
        @foreach($cmsaudit AS $data)
            <tr>
                <td>{{ date('d F Y',strtotime($data->created_at)) }}</td>
                <td class="text-center">{{number_format($data->total_video)}}</td>
                <td class="text-center">{{number_format($data->active_monetize)}}</td>
                <td class="text-center"><a href="{{route('youtube.audits.cms.content.export_csv', ['cms' => $data->content_id, 'field' => 'Blank', 'time' => date('Y-m-d', strtotime($data->created_at))])}}">{{number_format($data->content_blank)}}</a></td>
                <td class="text-center"><a href="{{route('youtube.audits.cms.content.export_csv', ['cms' => $data->content_id, 'field' => 'Block', 'time' => date('Y-m-d', strtotime($data->created_at))])}}">{{number_format($data->content_block)}}</a></td>
                <td class="text-center"><a href="{{route('youtube.audits.cms.content.export_csv', ['cms' => $data->content_id, 'field' => 'Track', 'time' => date('Y-m-d', strtotime($data->created_at))])}}">{{number_format($data->content_track)}}</a></td>
                <td class="text-center"><a href="{{route('youtube.audits.cms.content.export_csv', ['cms' => $data->content_id, 'field' => 'Monetize', 'time' => date('Y-m-d', strtotime($data->created_at))])}}">{{number_format($data->content_monetized_owner)}}</a></td>
                <td class="text-center"><a href="{{route('youtube.audits.cms.content.export_csv', ['cms' => $data->content_id, 'field' => 'split_monetize', 'time' => date('Y-m-d', strtotime($data->created_at))])}}">{{number_format($data->content_split_monetized)}}</a></td>
                <td class="text-center">{{number_format($data->persen_content_non_activated*100, 0)}}%</td>
                <td class="text-center">{{number_format($data->persen_copyright_claim*100, 0)}}%</td>
                <td class="text-center">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Update
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a data-toggle="modal" data-target="#updateBlank">Blank</a></li>
                            <li><a data-toggle="modal" data-target="#updateBlock">Block</a></li>
                            <li><a data-toggle="modal" data-target="#updateTrack">Track</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <?php $no++; ?>
        @endforeach
    </tbody>
</table>

<!-- Blank -->
<div id="updateBlank" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Blank</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['route' => 'youtube.audits.cms.content.update', 'class' => 'form-horizontal', 'id' => 'blankUpdate', 'files' => true]) !!}
                    {!! Form::label('blank_update', 'File Update :', ['class' => 'control-label']) !!}
                    {!! Form::file('file', ['class' => 'form-control', 'id' => 'blank_update']) !!}
                    <input type="hidden" name="data" value="Blank">
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                {!! Form::submit('Submit', ['class' => 'btn btn-primary', 'form'=>'blankUpdate', 'style' => 'margin-bottom:0;']) !!}
                <button type="button" class="btn btn-default" style="margin-bottom:0;" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Block -->
<div id="updateBlock" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Block</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['route' => 'youtube.audits.cms.content.update', 'class' => 'form-horizontal', 'id' => 'blockUpdate', 'files' => true]) !!}
                    {!! Form::label('block_update', 'File Update :', ['class' => 'control-label']) !!}
                    {!! Form::file('file', ['class' => 'form-control', 'id' => 'block_update']) !!}
                    <input type="hidden" name="data" value="Block">
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                {!! Form::submit('Submit', ['class' => 'btn btn-primary', 'form'=>'blockUpdate', 'style' => 'margin-bottom:0;']) !!}
                <button type="button" class="btn btn-default" style="margin-bottom:0;" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Track -->
<div id="updateTrack" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Track</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['route' => 'youtube.audits.cms.content.update', 'class' => 'form-horizontal', 'id' => 'trackUpdate', 'files' => true]) !!}
                    {!! Form::label('track_update', 'File Update :', ['class' => 'control-label']) !!}
                    {!! Form::file('file', ['class' => 'form-control', 'id' => 'track_update']) !!}
                    <input type="hidden" name="data" value="Track">
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                {!! Form::submit('Submit', ['class' => 'btn btn-primary', 'form'=>'trackUpdate', 'style' => 'margin-bottom:0;']) !!}
                <button type="button" class="btn btn-default" style="margin-bottom:0;" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@section('scripts')
@include('layouts.datatables_js')
<script>
    $(document).ready(function () {
        var dataTable = $('#table').DataTable({
            @if(auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'legal' || auth()->user()->roles->first()->name == 'anr')
            dom             : 'frtlip',
            @endif
            order           : [[ 0, "desc"]],
            processing      : true,
            serverMethod    : 'post',
            responsive      : true,
            scrollY         : 800,
            scrollX         : true,
            scrollCollapse  : true,
            autoWidth       : false,
            aLengthMenu     : [[10, 50, 100, 250, -1], [10, 50, 100, 250, 'All']],
            buttons         : false,
        });
        $(".alert-danger").fadeTo(3000, 1000).slideUp(1000, function(){
            $(".alert-danger").slideUp(1000);
        });
        $(".alert-success").fadeTo(3000, 1000).slideUp(1000, function(){
            $(".alert-success").slideUp(1000);
        });
        //lengthmenu -> add a margin to the right and reset clear 
        $(".dataTables_length").css('clear', 'none');
        $(".dataTables_length").css('margin-right', '20px');

        //info -> reset clear and padding
        $(".dataTables_info").css('clear', 'none');
        $(".dataTables_info").css('padding', '0');
        $("div").removeClass("ui-toolbar");

        $('#singer').on('change', function(){
            $('#album').val('');
            dataTable.search(this.value).draw(); 
        });
        $('#album').on('change', function(){
            $('#singer').val('');
            dataTable.search(this.value).draw();   
        });
        $('#status').on('change', function(){
            $('#singer').val('');
            $('#album').val('');
            dataTable.search(this.value).draw();   
        });
        $('.form-inline').find('.clear-filtering').on('click', function(e) {
            $('#singer').val('');
            $('#album').val('');
            $('#status').val('');
            dataTable.search(this.value).draw();
            e.preventDefault();
        });
    });
</script>
@endsection