<?php
    ini_set('memory_limit', '-1');
    $n = 1;
    $cms_name = str_replace(" ", "_", strtolower($cmsname->display_name));
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");
    header("Content-type: text/csv");
    $filename = "reporting-youtube-audit-".$cms_name."-".$field."-".$time.".csv";
    header("Content-Disposition: attachment; filename=".$filename."");
    header("Pragma: no-cache");
    header("Expires: 0");

    echo "content_id,";
    echo "video_id,";
    echo "channel_id,";
    echo "channel_display_name,";
    echo "time_published,";
    echo "video_title,";
    echo "video_length,";
    echo "views,";
    echo "video_privacy_status,";
    echo "effective_policy,";
    echo "asset_id,";
    echo "reason\n";

    foreach ($cmsaudit as $d) {
        echo 
            $d->content_id.','.
            $d->video_id.','.
            $d->channel_id.','.
            '"'.$d->channel_display_name.'",'.
            $d->time_published.','.
            '"'.$d->video_title.'",'.
            $d->video_length.','.
            $d->views.','.
            $d->video_privacy_status.','.
            $d->effective_policy.','.
            $d->asset_id;
        echo "\n";
        $n++;
    }
    die;
?>