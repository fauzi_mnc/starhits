@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Youtube Audits Report Content ID</h2>
        <ol class="breadcrumb">
            <li>
                Youtube Audits Report Content ID
            </li>
            <li class="active">
                <strong>Table</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h3>{{$cms->display_name}}</h3>
                </div>

                <div class="ibox-content">
                    @include('youtube.audits.details.contentid.table')
                </div>
            </div>
        </div>
    </div>
@endsection
