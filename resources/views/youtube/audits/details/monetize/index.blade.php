@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Youtube Audits Report Monetize</h2>
        <ol class="breadcrumb">
            <li>
                Youtube Audits Report Monetize
            </li>
            <li class="active">
                <strong>Table</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    @if(auth()->user()->roles->first()->name === 'admin')
                    <h3>{{$cms->display_name}}</h3>
                    @elseif(auth()->user()->roles->first()->name === 'creator')
                    <h3>{{$channel->name}}</h3>
                    @endif
                </div>

                <div class="ibox-content">
                    @include('youtube.audits.details.monetize.table')
                </div>
            </div>
        </div>
    </div>
@endsection
