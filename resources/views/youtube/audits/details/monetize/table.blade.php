@section('css')
    @include('layouts.datatables_css')
@endsection

@if(auth()->user()->roles->first()->name === 'admin')
    <a class="btn btn-warning pull-left" style="margin-top: -10px;margin-bottom: 5px" href="{{route('youtube.audits.cms')}}"><i class="fa fa-arrow-left"></i> Back</a>

    <table class="table" id="table">
        <thead>
            <tr>
                <th class="text-center">Upload Date</th>
                <th class="text-center">GrandTotal</th>
                <th class="text-center">active monetize</th>
                <th class="text-center">not active monetize</th>
                <th class="text-center">No Display ads</th>
                <th class="text-center">No Overlay ads</th>
                <th class="text-center">No sponsored card</th>
                <th class="text-center">No Skippable video ads</th>
                <th class="text-center">No Non-skippable video ads</th>
                {{-- <th class="text-center">% Format ads not complete</th> --}}
                {{-- <th class="text-center">Non active monetize</th> --}}
                <th class="text-center">Copyright Claim</th>
            </tr>
        </thead>
        <tbody>
            <?php $no =1; ?>
            @foreach($cmsaudit AS $data)
                <?php
                    $notComplete = ($data->no_display_ads + $data->no_overlay_ads + $data->no_sponsor_card + $data->no_skippable_video_ads +$data->no_nonskippable_video_ads);
                ?>
                <tr>
                    <td>{{ date('d F Y',strtotime($data->created_at)) }}</td>
                    <td class="text-center">{{number_format($data->total_video)}}</td>
                    <td class="text-center">{{number_format($data->active_monetize)}}</td>
                    <td class="text-center"><a href="{{route('youtube.audits.cms.monetize.export_csv', ['cms' => $data->content_id, 'field' => 'no_active_monetize', 'time' => date('Y-m-d', strtotime($data->created_at))])}}">{{number_format($data->no_active_monetize)}}</a></td>
                    <td class="text-center"><a href="{{route('youtube.audits.cms.monetize.export_csv', ['cms' => $data->content_id, 'field' => 'display_ads_enabled', 'time' => date('Y-m-d', strtotime($data->created_at))])}}">{{number_format($data->no_display_ads)}}</a></td>
                    <td class="text-center"><a href="{{route('youtube.audits.cms.monetize.export_csv', ['cms' => $data->content_id, 'field' => 'overlay_ads_enabled', 'time' => date('Y-m-d', strtotime($data->created_at))])}}">{{number_format($data->no_overlay_ads)}}</a></td>
                    <td class="text-center"><a href="{{route('youtube.audits.cms.monetize.export_csv', ['cms' => $data->content_id, 'field' => 'sponsored_cards_enabled', 'time' => date('Y-m-d', strtotime($data->created_at))])}}">{{number_format($data->no_sponsor_card)}}</a></td>
                    <td class="text-center"><a href="{{route('youtube.audits.cms.monetize.export_csv', ['cms' => $data->content_id, 'field' => 'skippable_video_ads_enabled', 'time' => date('Y-m-d', strtotime($data->created_at))])}}">{{number_format($data->no_skippable_video_ads)}}</a></td>
                    <td class="text-center"><a href="{{route('youtube.audits.cms.monetize.export_csv', ['cms' => $data->content_id, 'field' => 'nonskippable_video_ads_enabled', 'time' => date('Y-m-d', strtotime($data->created_at))])}}">{{number_format($data->no_nonskippable_video_ads)}}</a></td>
                    {{-- <td class="text-center"><a href="#">{{number_format($notComplete)}}</a></td>
                    <td class="text-center"><a href="#">{{number_format($data->no_active_monetize)}}</a></td> --}}
                    <td class="text-center"><a href="{{route('youtube.audits.cms.monetize.export_csv', ['cms' => $data->content_id, 'field' => 'claimed_by_another_owner', 'time' => date('Y-m-d', strtotime($data->created_at))])}}">{{(number_format($data->copyright_claim))}}</a></td>
                </tr>
                <?php $no++; ?>
            @endforeach
        </tbody>
    </table>
@elseif(auth()->user()->roles->first()->name === 'creator')
    <a class="btn btn-warning pull-left" style="margin-top: -10px;margin-bottom: 5px" href="{{route('youtube.audits.channel')}}"><i class="fa fa-arrow-left"></i> Back</a>

    <table class="table" id="table">
        <thead>
            <tr>
                <th class="text-center">Upload Date</th>
                <th class="text-center">GrandTotal</th>
                <th class="text-center">active monetize</th>
                <th class="text-center">No Display ads</th>
                <th class="text-center">No Overlay ads</th>
                <th class="text-center">No sponsored card</th>
                <th class="text-center">No Skippable video ads</th>
                <th class="text-center">No Non-skippable video ads</th>
                {{-- <th class="text-center">% Format ads not complete</th> --}}
                {{-- <th class="text-center">Non active monetize</th> --}}
                <th class="text-center">Copyright Claim</th>
            </tr>
        </thead>
        <tbody>
            <?php $no =1; ?>
            @foreach($channelaudit AS $data)
                <?php
                    $notComplete = ($data->no_display_ads + $data->no_overlay_ads + $data->no_sponsor_card + $data->no_skippable_video_ads +$data->no_nonskippable_video_ads);
                ?>
                <tr>
                    <td>{{ date('d F Y',strtotime($data->created_at)) }}</td>
                    <td class="text-center">{{number_format($data->total_video)}}</td>
                    <td class="text-center">{{number_format($data->active_monetize)}}</td>

                    <td class="text-center"><a href="{{route('youtube.audits.channel.monetize.export_csv', ['cms' => $data->content_id, 'field' => 'display_ads_enabled', 'time' => date('Y-m-d', strtotime($data->created_at))])}}">{{number_format($data->no_display_ads)}}</a></td>
                    <td class="text-center"><a href="{{route('youtube.audits.channel.monetize.export_csv', ['cms' => $data->content_id, 'field' => 'overlay_ads_enabled', 'time' => date('Y-m-d', strtotime($data->created_at))])}}">{{number_format($data->no_overlay_ads)}}</a></td>
                    <td class="text-center"><a href="{{route('youtube.audits.channel.monetize.export_csv', ['cms' => $data->content_id, 'field' => 'sponsored_cards_enabled', 'time' => date('Y-m-d', strtotime($data->created_at))])}}">{{number_format($data->no_sponsor_card)}}</a></td>
                    <td class="text-center"><a href="{{route('youtube.audits.channel.monetize.export_csv', ['cms' => $data->content_id, 'field' => 'skippable_video_ads_enabled', 'time' => date('Y-m-d', strtotime($data->created_at))])}}">{{number_format($data->no_skippable_video_ads)}}</a></td>
                    <td class="text-center"><a href="{{route('youtube.audits.channel.monetize.export_csv', ['cms' => $data->content_id, 'field' => 'nonskippable_video_ads_enabled', 'time' => date('Y-m-d', strtotime($data->created_at))])}}">{{number_format($data->no_nonskippable_video_ads)}}</a></td>
                    {{-- <td class="text-center"><a href="#">{{number_format($notComplete)}}</a></td>
                    <td class="text-center"><a href="#">{{number_format($data->no_active_monetize)}}</a></td> --}}
                    <td class="text-center"><a href="{{route('youtube.audits.channel.monetize.export_csv', ['cms' => $data->content_id, 'field' => 'claimed_by_another_owner', 'time' => date('Y-m-d', strtotime($data->created_at))])}}">{{(number_format($data->copyright_claim))}}</a></td>
                </tr>
                <?php $no++; ?>
            @endforeach
        </tbody>
    </table>
@endif
@section('scripts')
@include('layouts.datatables_js')
<script>
    $(document).ready(function () {
        var dataTable = $('#table').DataTable({
            @if(auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'legal' || auth()->user()->roles->first()->name == 'anr')
            dom             : 'frtlip',
            @endif
            order           : [[ 0, "desc"]],
            processing      : true,
            serverMethod    : 'post',
            responsive      : true,
            scrollY         : 800,
            scrollX         : true,
            scrollCollapse  : true,
            autoWidth       : false,
            aLengthMenu     : [[10, 50, 100, 250, -1], [10, 50, 100, 250, 'All']],
            buttons         : false,
        });
        $(".alert-danger").fadeTo(3000, 1000).slideUp(1000, function(){
            $(".alert-danger").slideUp(1000);
        });
        $(".alert-success").fadeTo(3000, 1000).slideUp(1000, function(){
            $(".alert-success").slideUp(1000);
        });
        //lengthmenu -> add a margin to the right and reset clear 
        $(".dataTables_length").css('clear', 'none');
        $(".dataTables_length").css('margin-right', '20px');

        //info -> reset clear and padding
        $(".dataTables_info").css('clear', 'none');
        $(".dataTables_info").css('padding', '0');
        $("div").removeClass("ui-toolbar");

        $('#singer').on('change', function(){
            $('#album').val('');
            dataTable.search(this.value).draw(); 
        });
        $('#album').on('change', function(){
            $('#singer').val('');
            dataTable.search(this.value).draw();   
        });
        $('#status').on('change', function(){
            $('#singer').val('');
            $('#album').val('');
            dataTable.search(this.value).draw();   
        });
        $('.form-inline').find('.clear-filtering').on('click', function(e) {
            $('#singer').val('');
            $('#album').val('');
            $('#status').val('');
            dataTable.search(this.value).draw();
            e.preventDefault();
        });
    });
</script>
@endsection