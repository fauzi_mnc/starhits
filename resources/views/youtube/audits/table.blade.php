@section('css')
    @include('layouts.datatables_css')
    <link href="{{ asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet">
@endsection

<table class="table" id="table">
    <thead>
        <tr>
            <th class="text-center">CMS</th>
            <th class="text-center">GrandTotal</th>
            <th class="text-center">active_monetize</th>
            <th class="text-center">No Display ads</th>
            <th class="text-center">No Overlay ads</th>
            <th class="text-center">No sponsored card</th>
            <th class="text-center">No Skippable video ads</th>
            <th class="text-center">No Non-skippable video ads</th>
            <th class="text-center">Not active monetize </th>
            <th class="text-center">Copyright Claim</th>
            <th class="text-center">ContentID Blank</th>
            <th class="text-center">ContentID Block</th>
            <th class="text-center">ContentID Track</th>
            <th class="text-center">ContentID Monetized Owner</th>
            <th class="text-center">ContentID Split Monetized</th>
            <th class="text-center">% Non active monetize</th>
            <th class="text-center">% Limited Ads</th>
            <th class="text-center">% Format ads not complete</th>
            <th class="text-center">% Content ID not activated</th>
            <th class="text-center">% Copyright Claim</th>          
        </tr>
    </thead>
    <tbody>
        @foreach($cmsaudit AS $data)
        <tr>
            <td class="text-center">{{$data->display_name}}</td>
            <td class="text-center">{{number_format($data->total_video)}}</td>
            <td class="text-center">{{number_format($data->active_monetize)}}</td>
            <td class="text-center">{{number_format($data->no_display_ads)}}</td>
            <td class="text-center">{{number_format($data->no_overlay_ads)}}</td>
            <td class="text-center">{{number_format($data->no_sponsor_card)}}</td>
            <td class="text-center">{{number_format($data->no_skippable_video_ads)}}</td>
            <td class="text-center">{{number_format($data->no_nonskippable_video_ads)}}</td>
            <td class="text-center">{{number_format($data->no_active_monetize)}} </td>
            <td class="text-center">{{number_format($data->copyright_claim)}}</td>
            <td class="text-center">{{number_format($data->content_blank)}}</td>
            <td class="text-center">{{number_format($data->content_block)}}</td>
            <td class="text-center">{{number_format($data->content_track)}}</td>
            <td class="text-center">{{number_format($data->content_monetized_owner)}}</td>
            <td class="text-center">{{number_format($data->content_split_monetized)}}</td>
            <td class="text-center">{{($data->persen_non_act_monetize)}}</td>
            <td class="text-center">{{($data->persen_limited_ads)}}</td>
            <td class="text-center">{{($data->persen_format_ads_not_complete)}}</td>
            <td class="text-center">{{($data->persen_content_non_activated)}}</td>
            <td class="text-center">{{($data->persen_copyright_claim)}}</td>
        </tr>
        @endforeach
    </tbody>
</table>

@section('scripts')
@include('layouts.datatables_js')
<script>
    $(document).ready(function () {
        /*$.get("{{route('video-yt.report.Bcxz')}}", function(data, status){
            console.log("Data: " + data + "\nStatus: " + status);
        });*/

        var dataTable = $('#table').DataTable({
            @if(auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'legal' || auth()->user()->roles->first()->name == 'anr')
            dom             : 'Bfrtlip',
            @endif
            order           : [[ 1, "asc"]],
            processing      : true,
            responsive      : true,
            autoWidth       : false,
            aLengthMenu     : [[10, 50, 100, 250, -1], [10, 50, 100, 250, 'All']],
            columnDefs      : [{ "width": "15%", "targets": 1 }, { "width": "15%", "targets": 2 }, { "width": "15%", "targets": 3 }, { "width": "15%", "targets": 4 }],
            buttons         : [
                              
                            ],
        });
        $(".alert-danger").fadeTo(3000, 1000).slideUp(1000, function(){
            $(".alert-danger").slideUp(1000);
        });
        $(".alert-success").fadeTo(3000, 1000).slideUp(1000, function(){
            $(".alert-success").slideUp(1000);
        });
        //lengthmenu -> add a margin to the right and reset clear 
        $(".dataTables_length").css('clear', 'none');
        $(".dataTables_length").css('margin-right', '20px');

        //info -> reset clear and padding
        $(".dataTables_info").css('clear', 'none');
        $(".dataTables_info").css('padding', '0');
        $("div").removeClass("ui-toolbar");

        $('.form-horizontal').$('#month').on('change', function(){
            dataTable.search(this.value).draw(); 
        });
        $('.form-horizontal').find('.clear-filtering').on('click', function(e) {
            $('#month').val('');
            dataTable.search(this.value).draw();
            e.preventDefault();
        });
    });
</script>
<script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script>
    $(document).ready(function(){
        $('.monthpicker').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            viewMode: 'months',
            minViewMode: 'months',
            format: 'mm'
        });
        $('.yearpicker').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            viewMode: 'years',
            minViewMode: 'years',
            format: 'yyyy'
        });
        var typereporting = document.getElementById("typereporting").value;
        if(typereporting == 0){
            $('#selectmonth').show();
            $('#selectyear').show();
        }else if(typereporting == 1) {
            $('#selectmonth').show();
            $('#selectyear').show()
        }else{
            $('#selectmonth').hide();
            $('#selectyear').show()
        }
        $('#typereporting').change(function(){
            if($(this).val() == 0){
                $('#selectmonth').show();
                $('#selectyear').show();
            }else if($(this).val() == 1) {
                $('#selectmonth').show();
                $('#selectyear').show()
            }else{
                $('#selectmonth').hide();
                $('#selectyear').show()
            }
        });
        $("#analyticsform").validate({
            rules: {
                typereporting: {
                    required:true
                },
                month: {
                    required:true
                },
                year : {
                    required:true
                }
            },
            messages:{
                typereporting:{
                    required:"Silahkan pilih tipe report!"
                },
                month:{
                    required:"Wajib di isi!"
                },
                year:{
                    required:"Wajib di isi!"
                }
            }
        });
    });
</script>
@endsection