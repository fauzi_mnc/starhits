<?php
header("application/xls");    
header("Content-Disposition: attachment; filename=Report_Youtube_Analytics_{$date}.xls");  
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header("Expires: 0");
?>

<html>
<head>
</head>
<body>
@foreach($unit as $units)
        @if($units->unit !='')
            <table style="width:100%" border="1">
                <tr >
                    <td align="center" ><b>#</b></td>
                    <td align="center" colspan="5"><b>{{$units->unit}}</b></td>
                </tr>
                
                <tr class="silver">
                    <th>No</th>
                    <th>Channel Name</th>
                    <th>Program</th>
                    <th>Subscribers Week #1</th>
                    <th>Subscribers Week #2</th>
                    <th>Subscribers Week #3</th>
                    <th>Subscribers Week #4</th>
                    <th>% Increasing Subs</th>
                </tr>  
                @php
                    $num = 1;
	                // $date = date("Y-m");
                    $rows = DB::select("SELECT name_channel, unit, program, `subscribers` FROM (SELECT youtube_analitic.name_channel, users.`unit`, users.`program`, `user_id`, Concat(Group_concat(youtube_analitic.`subscribers` ORDER BY youtube_analitic.`subscribers` asc), ',') AS `subscribers` FROM youtube_analitic INNER JOIN users ON users.id = youtube_analitic.user_id WHERE Date_format(youtube_analitic.created_at, '%Y-%m') = '".$date."' AND users.is_active = 1 GROUP BY youtube_analitic.name_channel, `user_id`) aggr WHERE `unit` = '".$units->unit."' ORDER BY `unit`");

                    $countR = count($rows);

                    for($i=0; $i < $countR; $i++) { 
                        if(empty(explode(',', $rows[$i]->subscribers)[0])) {$sub1=0;}
                        else{$sub1= explode(',', $rows[$i]->subscribers)[0];}

                        if(empty(explode(',', $rows[$i]->subscribers)[1])) {$sub2=0;}
                        else{$sub2= explode(',', $rows[$i]->subscribers)[1];}

                        if(empty(explode(',', $rows[$i]->subscribers)[2])) {$sub3=0;}
                        else{$sub3= explode(',', $rows[$i]->subscribers)[2];}

                        if(empty(explode(',', $rows[$i]->subscribers)[3])) {$sub4=0;}
                        else{ $sub4= explode(',', $rows[$i]->subscribers)[3]; }
                        
                        
                        if($sub1!="0" && $sub2=="0" && $sub3=="0" && $sub4=="0"){
                            $incres=0;
                        }elseif($sub1!="0" && $sub2!="0" && $sub3=="0" && $sub4=="0"){
                            $incres = number_format((float)(($sub2-$sub1)/$sub1)*100, 2, '.', '');
                        }elseif($sub1!="0" && $sub2!="0" && $sub3!="0" && $sub4=="0"){
                            $incres = number_format((float)(($sub3-$sub2)/$sub2)*100, 2, '.', '');
                        }elseif($sub1!="0" && $sub2!="0" && $sub3!="0" && $sub4!="0"){
                            $incres = number_format((float)(($sub4-$sub3)/$sub3)*100, 2, '.', '');
                        }else{
                            $incres = 0;
                        }
                @endphp
                        <tr >
                            <td align="center">{{$num}}</td>
                            <td>{{$rows[$i]->name_channel}}</td>
                            <td>{{$rows[$i]->program}}</td>
                            <td align="right">{{$sub1}}</td>
                            <td align="right">{{$sub2}}</td>
                            <td align="right">{{$sub3}}</td>
                            <td align="right">{{$sub4}}</td>
                            <td align="right">{{$incres}}</td>
                        </tr>   

                @php
                    $num++;
                    }
                @endphp 
            </table><br>
        @endif
    @endforeach
</body>
</html> 
