@section('css')
    @include('layouts.datatables_css')
@endsection

@if(auth()->user()->roles->first()->name === 'admin')
    <table class="table" id="table">
        <thead>
            <tr>
                <th class="text-center">CMS</th>
                <th class="text-center">Last Update</th>
                <th class="text-center">Active Monetize (%)</th>
                <th class="text-center">Not Active Monetize (%)</th>
                <th class="text-center">Copyright Claims (%)</th>
                {{-- <th class="text-center">Content ID Not Activated (%)</th> --}}
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $no =1; ?>
            @foreach($cms AS $data)
            <?php
                $videoAudit = DB::table('youtube_video_audit_cms')
                    ->where(['content_id'=>$data->content_id])
                    ->orderBy('created_at', 'desc')
                    ->first();

                $fullMonetize = ($videoAudit->active_monetize - ($videoAudit->no_display_ads + $videoAudit->no_overlay_ads + $videoAudit->no_sponsor_card + $videoAudit->no_skippable_video_ads + $videoAudit->no_nonskippable_video_ads)) / $videoAudit->active_monetize;
                $notActiveMonetize = ($videoAudit->no_active_monetize - $videoAudit->active_monetize);
                $copyrightClaim = ($videoAudit->copyright_claim - $videoAudit->active_monetize);
                $contentIDnotActivated = (($videoAudit->content_block + $videoAudit->content_blank + $videoAudit->content_track) / $videoAudit->active_monetize);
            ?>
                <tr>
                    <td>{{ $data->display_name }}</td>
                    <td class="text-center">{{ date('d F Y',strtotime($videoAudit->created_at)) }}</td>
                    <td class="text-center">{{ number_format($fullMonetize*100, 2)}}%</td>
                    <td class="text-center">{{ number_format($videoAudit->persen_non_act_monetize*100, 2)}}%</td>
                    <td class="text-center">{{ number_format($videoAudit->persen_copyright_claim*100, 2)}}%</td>
                    {{-- <td class="text-center">{{ number_format($videoAudit->persen_content_non_activated*100, 2)}}%</td> --}}
                    <td class="text-center">
                        <a href="{{route('youtube.audits.cms.monetize',$data->content_id)}}" class="btn btn-success  btn-xs">
                            Monetize
                        </a>
                        <a href="{{route('youtube.audits.cms.content',$data->content_id)}}" class="btn btn-primary btn-xs">
                            Content ID
                        </a>
                    </td>
                </tr>
                <?php $no++; ?>
            @endforeach
        </tbody>
    </table>
@elseif(auth()->user()->roles->first()->name === 'creator')
    <table class="table" id="table">
        <thead>
            <tr>
                <th class="text-center">Channel</th>
                <th class="text-center">Last Update</th>
                <th class="text-center">Active Monetize (%)</th>
                <th class="text-center">Not Active Monetize (%)</th>
                <th class="text-center">Copyright Claims (%)</th>
                {{-- <th class="text-center">Content ID Not Activated (%)</th> --}}
                <th class="text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            <?php $no =1; ?>
            @foreach($channel AS $data)
            <?php
                $videoAudit = DB::table('youtube_video_audit_channel')
                    ->where(['channel_id'=>$data->provider_id])
                    ->orderBy('created_at', 'desc')
                    ->first();
                // dump($videoAudit);

                $fullMonetize = ($videoAudit->active_monetize - ($videoAudit->no_display_ads + $videoAudit->no_overlay_ads + $videoAudit->no_sponsor_card + $videoAudit->no_skippable_video_ads + $videoAudit->no_nonskippable_video_ads)) / $videoAudit->active_monetize;
                $notActiveMonetize = ($videoAudit->no_active_monetize - $videoAudit->active_monetize);
                $copyrightClaim = ($videoAudit->copyright_claim - $videoAudit->active_monetize);
                $contentIDnotActivated = (($videoAudit->content_block + $videoAudit->content_blank + $videoAudit->content_track) / $videoAudit->active_monetize);
            ?>
                <tr>
                    <td>{{ $data->provider_id }}</td>
                    <td class="text-center">{{ date('d F Y',strtotime($videoAudit->created_at)) }}</td>
                    <td class="text-center">{{ number_format($fullMonetize*100, 2)}}%</td>
                    <td class="text-center">{{ number_format($videoAudit->persen_non_act_monetize*100, 2)}}%</td>
                    <td class="text-center">{{ number_format($videoAudit->persen_copyright_claim*100, 2)}}%</td>
                    {{-- <td class="text-center">{{ number_format($videoAudit->persen_content_non_activated*100, 2)}}%</td> --}}
                    <td class="text-center">
                        <a href="{{route('youtube.audits.channel.monetize',$data->provider_id)}}" class="btn btn-success btn-xs">
                            Monetize
                        </a>
                        <a href="{{route('youtube.audits.channel.content',$data->provider_id)}}" class="btn btn-primary btn-xs">
                            Content ID
                        </a>
                    </td>
                </tr>
                <?php $no++; ?>
            @endforeach
        </tbody>
    </table>
@endif
@section('scripts')
    @include('layouts.datatables_js')
    <script>
        $(document).ready(function () {
            var dataTable = $('#table').DataTable({
                @if(auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'legal' || auth()->user()->roles->first()->name == 'anr')
                dom             : 'frtlip',
                @endif
                order           : [[ 0, "desc"]],
                processing      : true,
                serverMethod    : 'post',
                responsive      : true,
                autoWidth       : false,
                aLengthMenu     : [[10, 50, 100, 250, -1], [10, 50, 100, 250, 'All']],
                buttons         : false,
            });
            $(".alert-danger").fadeTo(3000, 1000).slideUp(1000, function(){
                $(".alert-danger").slideUp(1000);
            });
            $(".alert-success").fadeTo(3000, 1000).slideUp(1000, function(){
                $(".alert-success").slideUp(1000);
            });
            //lengthmenu -> add a margin to the right and reset clear 
            $(".dataTables_length").css('clear', 'none');
            $(".dataTables_length").css('margin-right', '20px');

            //info -> reset clear and padding
            $(".dataTables_info").css('clear', 'none');
            $(".dataTables_info").css('padding', '0');
            $("div").removeClass("ui-toolbar");

            $('#singer').on('change', function(){
                $('#album').val('');
                dataTable.search(this.value).draw(); 
            });
            $('#album').on('change', function(){
                $('#singer').val('');
                dataTable.search(this.value).draw();   
            });
            $('#status').on('change', function(){
                $('#singer').val('');
                $('#album').val('');
                dataTable.search(this.value).draw();   
            });
            $('.form-inline').find('.clear-filtering').on('click', function(e) {
                $('#singer').val('');
                $('#album').val('');
                $('#status').val('');
                dataTable.search(this.value).draw();
                e.preventDefault();
            });
        });
    </script>
@endsection