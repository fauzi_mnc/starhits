<html>
<head>
<style>
body{
    margin: 10px;
}
table{
    margin: 20px 0px;
}
table, th, td{
    border-collapse: collapse;
}
.table-bordered table, .table-bordered th, .table-bordered td {
    border: 2px solid #000;
}
th, td {
    padding: 5px;
}
.signature{
    padding: 40px 50px;
}
.silver{
    background-color: #f0f0f0;
}
</style>
</head>
<body onload="print();">
<div style="width:100%; text-align:center;">
    <img style="width:350px;" src="{{ asset('img/hitsrecords.png') }}">
    <h2 style="margin:5px;">PT. Suara Mas Abadi</h2>
    @if(auth()->user()->roles->first()->name === 'admin')
    <p>Data Youtube Weekly Analytics</p>
    <p><strong>{{ date('F Y', strtotime($date)) }}</p>
    @else
    <p>Daftar Lagu {{ auth()->user()->name }}</p>
    @endif
</div>
    @foreach($unit as $units)
        @if($units->unit !='')
            <table class="table-bordered" style="width:100%" border="1">
                <tr>
                    <td align="center" ><b>#</b></td>
                    <td align="center" colspan="7"><b>{{$units->unit}}</b></td>
                </tr>
                
                <tr class="silver">
                    <th>No</th>
                    <th>Channel Name</th>
                    <th>Program</th>
                    <th>Subscribers Week #1</th>
                    <th>Subscribers Week #2</th>
                    <th>Subscribers Week #3</th>
                    <th>Subscribers Week #4</th>
                    <th>% Increasing Subs</th>
                </tr>  
                @php
                    $num = 1;

                    $rows = DB::select("SELECT name_channel, unit, program, `subscribers` FROM (SELECT youtube_analitic.name_channel, users.`unit`, users.`program`, `user_id`, Concat(Group_concat(youtube_analitic.`subscribers` ORDER BY youtube_analitic.`subscribers` asc), ',') AS `subscribers` FROM youtube_analitic INNER JOIN users ON users.id = youtube_analitic.user_id WHERE Date_format(youtube_analitic.created_at, '%Y-%m') = '".$date."' AND users.is_active = 1 GROUP BY youtube_analitic.provider_id, `user_id`) aggr WHERE `unit` = '".$units->unit."' ORDER BY `unit`");

                    $countR = count($rows);

                    for($i=0; $i < $countR; $i++) { 
                        if(empty(explode(',', $rows[$i]->subscribers)[0])) {$sub1=0;}
                        else{$sub1= explode(',', $rows[$i]->subscribers)[0];}

                        if(empty(explode(',', $rows[$i]->subscribers)[1])) {$sub2=0;}
                        else{$sub2= explode(',', $rows[$i]->subscribers)[1];}

                        if(empty(explode(',', $rows[$i]->subscribers)[2])) {$sub3=0;}
                        else{$sub3= explode(',', $rows[$i]->subscribers)[2];}

                        if(empty(explode(',', $rows[$i]->subscribers)[3])) {$sub4=0;}
                        else{ $sub4= explode(',', $rows[$i]->subscribers)[3]; }
                        
                        
                        if($sub1!="0" && $sub2=="0" && $sub3=="0" && $sub4=="0"){
                            $incres=0;
                        }elseif($sub1!="0" && $sub2!="0" && $sub3=="0" && $sub4=="0"){
                            $incres = number_format((float)(($sub2-$sub1)/$sub1)*100, 2, '.', '');
                        }elseif($sub1!="0" && $sub2!="0" && $sub3!="0" && $sub4=="0"){
                            $incres = number_format((float)(($sub3-$sub2)/$sub2)*100, 2, '.', '');
                        }elseif($sub1!="0" && $sub2!="0" && $sub3!="0" && $sub4!="0"){
                            $incres = number_format((float)(($sub4-$sub3)/$sub3)*100, 2, '.', '');
                        }else{
                            $incres = 0;
                        }
                @endphp
                        <tr >
                            <td align="center">{{$num}}</td>
                            <td>{{$rows[$i]->name_channel}}</td>
                            <td>{{$rows[$i]->program}}</td>
                            <td align="right">{{$sub1}}</td>
                            <td align="right">{{$sub2}}</td>
                            <td align="right">{{$sub3}}</td>
                            <td align="right">{{$sub4}}</td>
                            <td align="right">{{$incres}}%</td>
                        </tr>   

                @php
                $num++;
                    }
                @endphp 
            </table><br>
        @endif
    @endforeach
</body>
</html> 