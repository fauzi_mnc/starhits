@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Brands</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{route('brand.index')}}">Brands</a>
            </li>
            <li class="active">
                <strong>Edit</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Brands</h5>
                </div>
                <div class="ibox-content">
                    {!! Form::model($user, ['route' => ['brand.update', $user->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) !!}
                        @include('brand.fields', ['formType' => 'edit'])
                    {!! Form::close() !!}
                </div>
            </div>

@endsection

