<div class='btn-group'>
    <a href="{{ route('brand.show', $c->id) }}" class='btn btn-default btn-xs' title="Show Brand">
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="{{ route('brand.edit', $c->id) }}" class='btn btn-default btn-xs' title="Edit Brand">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
</div>