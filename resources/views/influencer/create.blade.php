@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Influencer</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Influencer</a>
            </li>
            <li class="active">
                <strong>Create</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Create Influencer
                </div>
                <div class="panel-body">
                    {!! Form::open(['route' => ['influencer.store'], 'id' => 'example-form', 'files' => true]) !!}
                        @include('influencer.fields', ['formType' => 'create'])
                    </form>
                </div>

            </div>
            
        </div>
    </div>
@endsection