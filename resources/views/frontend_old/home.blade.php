@push('meta')    
        <meta property="og:url" content="{{ url()->current() }}" />
        <meta property="og:type" content="#" />
        <meta property="og:title" content="@if(isset($models['website_title'])){{ $models['website_title'] }}@endif" />
        <meta property="og:description" content="@if(isset($models['website_slogan'])){{ $models['website_slogan'] }}@endif" />
        <meta property="og:image" content="{{url($models['website_logo_header'])}}" />
        <meta property="fb:app_id" content="952765028218113" />
@endpush

@extends('layouts.frontend')

@push('preloader')
<div class="se-pre-con">
    <div class="col-sm-12 text-center pre-con">
        <div class="col">
            <div class="logo-preloader">
                <img alt="" class="img-fluid" src="{{ asset('frontend/assets/img/logo-home.png') }}">
            </div>
        </div>
        <div class="col">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg">
                <defs>
                    <filter id="gooey">
                        <fegaussianblur in="SourceGraphic" result="blur" stddeviation="10">
                        </fegaussianblur>
                        <fecolormatrix in="blur" mode="matrix" result="goo" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7">
                        </fecolormatrix>
                        <feblend in="SourceGraphic" in2="goo">
                        </feblend>
                    </filter>
                </defs>
            </svg>
            <div class="blob blob-0"></div>
            <div class="blob blob-1"></div>
            <div class="blob blob-2"></div>
            <div class="blob blob-3"></div>
            <div class="blob blob-4"></div>
            <div class="blob blob-5"></div>
        </div>
    </div>
</div>
@endpush

@section('content')

@if(empty($SiteConfig['configs']['is_streaming']))
<div class="container-fluid top-content">
    <div class="row">
        <div class="col-md-8">
            <div class="slider-homepage">
                @foreach($videoStarHits as $value)
                <div>
                    <div class="top-block">
                        <div class="top-media">
                            <div class="thumb-homepage">
                                @if($value->post_type == 'automatic')
                                <a class="play" href="{{ url('/videos/'.$value->slug) }}">
                                    <img src="{{asset('frontend/assets/img/icon/play_mobile_480_black.png')}}" alt="Play">
                                </a>
                                <a class="target" href="{{ url('/videos/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                                @elseif($value->post_type == 'manual')
                                    @if(!empty($value->attr_6))
                                    <a class="play" href="{{ url('/article/'.$value->slug) }}">
                                        <img src="{{asset('frontend/assets/img/icon/play_mobile_480_black.png')}}" alt="Play">
                                    </a>
                                    <a class="target" href="{{ url('/article/'.$value->slug) }}">
                                        {{$value->title}}
                                    </a>
                                    @elseif(empty($value->attr_6))
                                    <a class="target" href="{{ url('/article/'.$value->slug) }}">
                                        {{$value->title}}
                                    </a>
                                    @endif
                                @endif
                            </div>


                            <!-- Check status header -->
                            @if($value->post_type == 'automatic')
                                <a href="{{ url('/videos/'.$value->slug) }}">
                                    @if(!empty($value->image))
                                        <img class="img-fluid" src="{{ url('/'). Image::url($value->image,886,490,array('crop')) }}"/>
                                    @else
                                        <?php $headers = get_headers("https://i3.ytimg.com/vi/".$value->attr_6."/maxresdefault.jpg", 1);
                                            $header = substr($headers[0], 9, 3);
                                        ?>

                                        <?php if($header == 404):?>
                                            <img class="img-fluid" src="https://i3.ytimg.com/vi/{{ $value->attr_6}}/mqdefault.jpg"/>
                                        <?php else:?>
                                            <img class="img-fluid" src="https://i3.ytimg.com/vi/{{ $value->attr_6}}/maxresdefault.jpg"/>
                                        <?php endif?>
                                    @endif
                                </a>
                                @elseif($value->post_type == 'manual')
                                <a href="{{ url('/article/'.$value->slug) }}">
                                    @if(!empty($value->image))
                                        <img class="img-fluid" src="{{ url('/'). Image::url($value->image,886,490,array('crop')) }}"/>
                                    @else
                                        <?php $headers = get_headers("https://i3.ytimg.com/vi/".$value->attr_6."/maxresdefault.jpg", 1);
                                            $header = substr($headers[0], 9, 3);
                                        ?>
                                        <?php if($header == 404):?>
                                            <img class="img-fluid" src="https://i3.ytimg.com/vi/{{ $value->attr_6}}/mqdefault.jpg"/>
                                        <?php else:?>
                                            <img class="img-fluid" src="https://i3.ytimg.com/vi/{{ $value->attr_6}}/maxresdefault.jpg"/>
                                        <?php endif?>
                                    @endif
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="col-md-4 listing-block">
            <div class="title">
                <h7>
                    LATEST FROM STAR HITS
                </h7>
            </div>
            <div class="all-media">
                <div class="{{ count($videoStarHits)>=1? 'all-media-child':'' }}">
                    @forelse($videoStarHits as $value)
                    <div class="media">
                        @if(!empty($value->attr_7))
                            <div class="item-top-overlay">
                                {{ $duration->formatted($value->attr_7) }}
                            </div>
                        @else

                        @endif

                        @if($value->post_type == 'automatic')
                            <a href="{{ url('/videos/'.$value->slug) }}">
                                @if(!empty($value->image))
                                    <img class="d-flex align-self-start img-fluid top-overlay" src="{{ url('/'). Image::url($value->image,550,350,array('crop')) }}"/>
                                @else
                                    <img class="d-flex align-self-start img-fluid top-overlay" src="https://i3.ytimg.com/vi/{{ $value->attr_6}}/mqdefault.jpg" style="width: 513; height: 350;"/>
                                @endif
                            </a>
                            @elseif($value->post_type == 'manual')
                            <a href="{{ url('/article/'.$value->slug) }}">
                                @if(!empty($value->image))
                                    <img class="d-flex align-self-start img-fluid top-overlay" src="{{ url('/'). Image::url($value->image,550,350,array('crop')) }}"/>
                                @else
                                    <img class="d-flex align-self-start img-fluid top-overlay" src="https://i3.ytimg.com/vi/{{ $value->attr_6}}/mqdefault.jpg" style="width: 513; height: 350;"/>
                                @endif
                            </a>
                        @endif
                        <div class="media-body pl-3">
                            @if(!empty($value->series()->first()->title) OR !empty($value->channelVideo()->first()->title))
                            <div class="media-title-series">
                                @if(!empty($value->series()->first()->title))
                                <a href="#">
                                        {{$value->series()->first()->title}}
                                    @else
                                        {{$value->channelVideo()->first()->title}}
                                </a>
                                @endif
                            </div> 
                            @else

                            @endif
                            <div class="media-title">
                                @if($value->post_type == 'manual')
                                <a href="{{ url('/article/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                                @else
                                <a href="{{ url('/videos/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                                @endif
                            </div>
                        </div>
                    </div>
                    @empty
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@else
@include('frontend.partials._live-stream')
@endif
<div class="col-sm-12 main-ads text-center">
    <!-- starhits ads -->
    {{-- <ins class="adsbygoogle"
        style="display:block;width:728px;height:90px margin: 0 auto;"
        data-ad-client="ca-pub-5195976246447231"
        data-ad-slot="4189685044"
        data-ad-format="auto"
        data-full-width-responsive="true"></ins>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({});
    </script> --}}
    {{-- <a href="#"><img class="center-ads" src="{{ asset('frontend/assets/img/ads-728x90-web-series.jpg') }}" alt=""></a> --}}
</div>

<div class="container-fluid top-videos">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <div class="title">
                <h7>
                    WEEKLY TOP VIDEOS
                </h7>
            </div>
        </div>
        <div class="col-md-12">
            <div class="{{ count($weeklyTopVideos)>=1? 'slider-top-videos':'' }}">
                @forelse($weeklyTopVideos as $value)
                <div>
                    <div class="item">
                        <div class="imgTitle">
                            <div class="top-videos-overlay">
                                <a href="{{ url('/videos/'.$value->slug) }}">
                                    @if(!empty($value->image))
                                        <img src="{{ url('/'). Image::url($value->image,400,225,array('crop')) }}" class="img-fluid top-videos-overlay" alt="">
                                    @else
                                        <img class="img-fluid top-videos-overlay" src="https://i3.ytimg.com/vi/{{ $value->attr_6}}/mqdefault.jpg" style="width: 400; height: 225;"/>
                                    @endif
                                </a>
                                @if(!empty($value->attr_7))
                                    <div class="item-top-videos-overlay">
                                        {{ $duration->formatted($value->attr_7) }}
                                    </div>
                                @else

                                @endif
                            </div>
                        </div>
                        <p>
                            <a href="{{ url('/videos/'.$value->slug) }}">
                                {{$value->title}}
                            </a>
                        </p>
                    </div>
                </div>
                @empty
                <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                            There is no data to display!
                        </div>
                    </br>
                </div>
                @endforelse
            </div>
        </div>
    </div>
</div>
<div class="container-fluid top-hits">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <div class="title">
                <h7>
                    STAR HITS SERIES
                </h7>
            </div>
        </div>
        <div class="col-md-12">
            <div class="{{ count($seriesStarHits)>=1? 'slider-top-hits':'' }}">
                @forelse($seriesStarHits as $value)
                <div>
                    <div class="item">
                        <article class="caption">
                            <img class="caption__media img-fluid" src="{{ url('/'). Image::url($value->image,350,500,array('crop')) }}"/>
                            <a href="{{ url('/series/'.$value->slug) }}">
                                <div class="caption__overlay">
                                    <h1 class="caption__overlay__title">
                                        {{$value->title}}
                                    </h1>
                                    <h7 class="caption__overlay__content">
                                        {{$value->totalVideo}} Videos
                                    </h7>
                                    @if(!empty($value->excerpt))
                                    <p class="caption__overlay__content">
                                        {!!strip_tags($value->excerpt)!!}
                                    </p>
                                    @else
                                    <p class="caption__overlay__content">
                                        {!!strip_tags(str_limit($value->content, 100))!!}
                                    </p>
                                    @endif
                                </div>
                            </a>
                        </article>
                        <p>
                            <a href="{{ url('/series/'.$value->slug) }}">
                                {{$value->title}}
                            </a>
                        </p>
                        <p class="videos">
                            <a href="#">
                                {{$value->totalVideo}} Videos
                            </a>
                        </p>
                    </div>
                </div>
                @empty
                <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                            There is no data to display!
                        </div>
                    </br>
                </div>
                @endforelse
            </div>
        </div>
    </div>
</div>
<div class="container-fluid top-pro">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <div class="title">
                <h7>
                    STAR PRO SERIES
                </h7>
            </div>
        </div>
        <div class="col-md-12">
            <div class="{{ count($seriesStarPro)>=1? 'slider-top-pro':'' }}">
                @forelse($seriesStarPro as $value)
                <div>
                    <div class="item">
                        <article class="caption">
                            <img class="caption__media img-fluid" src="{{ url('/'). Image::url($value->image,350,500,array('crop')) }}"/>
                            <a href="{{ url('/series/'.$value->slug) }}">
                                <div class="caption__overlay">
                                    <h1 class="caption__overlay__title">
                                        {{$value->title}}
                                    </h1>
                                    <h7 class="caption__overlay__content">
                                        {{$value->totalVideo}} Videos
                                    </h7>
                                    @if(!empty($value->excerpt))
                                    <p class="caption__overlay__content">
                                        {!!strip_tags($value->excerpt)!!}
                                    </p>
                                    @else
                                    <p class="caption__overlay__content">
                                        {!!strip_tags(str_limit($value->content, 100))!!}
                                    </p>
                                    @endif
                                </div>
                            </a>
                        </article>
                        <p>
                            <a href="{{ url('/series/'.$value->slug) }}">
                                {{$value->title}}
                            </a>
                        </p>
                        <p class="videos">
                            <a href="#">
                                {{$value->totalVideo}} Videos
                            </a>
                        </p>
                    </div>
                </div>
                @empty
                <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                            There is no data to display!
                        </div>
                    </br>
                </div>
                @endforelse
            </div>
        </div>
    </div>
</div>
<div class="container-fluid recommended-videos">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <div class="title">
                <h7>
                    RECOMMENDED VIDEOS
                </h7>
            </div>
        </div>
        <div class="col-md-12">
            <div class="{{ count($recomendedVideos)>=1? 'slider-recommended-videos':'' }}">
                @forelse($recomendedVideos as $value)
                <div>
                    <div class="item">
                        <div class="imgTitle">
                            <div class="top-videos-overlay">
                                <a href="{{ url('/videos/'.$value->slug) }}">
                                    @if(!empty($value->image))
                                        <img class="img-fluid top-videos-overlay" src="{{ url('/'). Image::url($value->image,400,225,array('crop')) }}"/>
                                    @else
                                        <img class="img-fluid top-videos-overlay" src="https://i3.ytimg.com/vi/{{ $value->attr_6}}/mqdefault.jpg" style="width: 400; height: 250;"/>
                                    @endif
                                </a>
                                @if(!empty($value->attr_7))
                                    <div class="item-top-videos-overlay">
                                        {{ $duration->formatted($value->attr_7) }}
                                    </div>
                                @else

                                @endif
                            </div>
                        </div>
                        <p>
                            <a href="{{ url('/videos/'.$value->slug) }}">
                                {{$value->title}}
                            </a>
                        </p>
                    </div>
                </div>
                @empty
                <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                            There is no data to display!
                        </div>
                    </br>
                </div>
                @endforelse
            </div>
        </div>
    </div>
</div>
<div class="container-fluid others-videos">
    <div class="col-sm-12">
        <div class="col-sm-12">
            <div class="title">
                <h7>
                    OTHERS VIDEOS
                </h7>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="{{ count($otherVideos)>=1? 'row text-center text-lg-left item-others-videos loadMore':'' }}">
                @forelse($otherVideos as $value)
                <div class="col-sm-3 col-others-videos">
                    <div class="item">
                        <div class="imgTitle">
                            <div class="top-videos-overlay">
                                <a href="{{ url('/videos/'.$value->slug) }}">
                                    @if(!empty($value->image))
                                        <img class="img-fluid top-videos-overlay" src="{{ url('/'). Image::url($value->image,400,225,array('crop')) }}"/>
                                    @else
                                        <img class="img-fluid top-videos-overlay" src="https://i3.ytimg.com/vi/{{ $value->attr_6}}/mqdefault.jpg" style="width: 400; height: 250;"/>
                                    @endif
                                </a>
                                @if(!empty($value->attr_7))
                                    <div class="item-top-videos-overlay">
                                        {{ $duration->formatted($value->attr_7) }}
                                    </div>
                                @else

                                @endif
                            </div>
                        </div>
                        <p>
                            <a href="{{ url('/videos/'.$value->slug) }}">
                                {{$value->title}}
                            </a>
                        </p>
                    </div>
                </div>
                @empty
                <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                            There is no data to display!
                        </div>
                    </br>
                </div>
                @endforelse
            </div>
        </div>
    </div>
</div>
@endsection