<div class="container-fluid top-content-single">
    <div class="live-streaming">
        <div class="row">
            <!-- <div class="col-sm-2 my-auto">
                <div class="left-ads text-center">
                    <a href="#">
                        <img alt="" src="{{-- asset('frontend/assets/img/sidebar-ads-1.jpg') --}}"/>
                    </a>
                </div>
            </div> -->
            <div class="col-sm-8 top-live">
                <div class="item">
                    <div class="top-block">
                        <div class="top-media">
                            <div class="video-wrap">
                                <div class="video">
                                    <div class="youtube embed-responsive embed-responsive-16by9">
                                        <iframe src="https://www.youtube.com/embed/{{$SiteConfig['configs']['website_streaming_url']}}" width="560" height="315" frameborder="0" allowfullscreen></iframe>
                                        <div class="thumb">
                                            <div class="target-big" href="#">
                                                Live
                                                <span class="dot">
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                </div>
                            </div>
                        </div>
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 my-auto">
                <!-- <div class="right-ads text-center">
                    <a href="#">
                        <img alt="" src="{{-- asset('frontend/assets/img/sidebar-ads-2.jpg') --}}"/>
                    </a>
                </div> -->
                <iframe src="https://www.youtube.com/live_chat?v={{$SiteConfig['configs']['website_streaming_url']}}&embed_domain=devapp.mncgroup.com" style="width:100%;height: 450px;" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
