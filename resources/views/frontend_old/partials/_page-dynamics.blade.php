@push('meta')    
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:title" content="{{ $metaTag->title }}" />
    <meta property="og:description" content="{{ \Illuminate\Support\Str::words(preg_replace('/&#?[a-z0-9]+;/i','', strip_tags($metaTag->content)), 80 ) }}" />
    @if(!empty($metaTag->image))
    <meta property="og:image" content="{{ $metaTag->image }}" />
    @else
    <meta property="og:image" content="{{url($models['website_logo_header'])}}" />
    @endif
    <meta property="fb:app_id" content="952765028218113" />
@endpush

@extends('layouts.frontend')

@section('title', $pagesDynamic->title)

@section('content')

<div class="container-fluid blank-page">
    <div class="col-sm-12">
        <div class="title">
            <h2>
                {{ $pagesDynamic->title }}
            </h2>
        </div>
        <hr>
            <div class="col-md-12" style="padding: 0;">                  
                <div class="text">
                    <p>
                        <b>
                            <font size="5">
                                {{ $pagesDynamic->subtitle }}
                            </font>
                        </b>
                    </p>
                    <p>
                        {!! $pagesDynamic->content !!}
                    </p>
                </div>
            </div>
        </hr>
    </div>
</div>

@endsection