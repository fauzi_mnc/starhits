<style type="text/css">

</style>

<div id="footer">
    <div class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-2 col-md-3 col-sm-2">
                    <div class="footer_dv">
                        <img alt="about" class="img-fluid" src="{{asset('frontend/assets/img/starhitstitle-1.png')}}">
                        </img>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-5">
                    <div class="footer_dv">
                        <h7>
                            About Star Hits
                        </h7>
                        <p class="about">
                            {{$SiteConfig['configs']['website_title_about']}}
                        </p>
                        <p>
                            <a href="{{url('page/about-us')}}">
                                Read More
                            </a>
                        </p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-4 col-sm-5">
                    <div class="row footer_dv">
                        <div class="col-sm-12 subs">
                            <hgroup>
                                <h7>
                                    Subscribe to Star Hits
                                </h7>
                            </hgroup>
                            <div class="well">
                                <form action="{{ url('newsletter') }}" method="POST">
                                    <div class="input-group">
                                        <input class="btn btn-lg" name="subsc_email" id="subsc_email" type="email" placeholder="Email" required>
                                        {{ csrf_field() }}
                                        <button class="btn btn-newsletter btn-lg" type="submit">Subscribe now</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-sm-12 sos">
                            <a href="{{$SiteConfig['configs']['website_url_facebook']}}" target="_blank" class="icon-link round-corner facebook fill">
                                <i class="fa fa-facebook fa-lg"></i>
                            </a>
                            <a href="{{$SiteConfig['configs']['website_url_youtube']}}" target="_blank" class="icon-link round-corner youtube fill">
                                <i class="fa fa-youtube fa-lg">
                                </i>
                            </a>
                            <a href="{{$SiteConfig['configs']['website_url_twitter']}}" target="_blank" class="icon-link round-corner twitter fill">
                                <i class="fa fa-twitter fa-lg">
                                </i>
                            </a>
                            <a href="{{$SiteConfig['configs']['website_url_google']}}" target="_blank" class="icon-link round-corner google-plus fill">
                                <i class="fa fa-google-plus fa-lg">
                                </i>
                            </a>
                            <a href="mailto:{{$SiteConfig['configs']['website_url_mail']}}" target="_blank" class="icon-link round-corner envelope fill">
                                <i class="fa fa-envelope fa-lg">
                                </i>
                            </a>
                            <a href="{{$SiteConfig['configs']['website_url_instagram']}}" target="_blank" class="icon-link round-corner instagram fill">
                                <i class="fa fa-instagram fa-lg">
                                </i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="privacy">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <ul class="list-inline">
                        @foreach($SiteConfig['pages'] as $value)
                        <li class="list-inline-item">
                            <a href="{{ url('/page/'.$value->slug) }}">
                                {{$value->title}}
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <footer class="py-3">
        <div class="copyright">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        <p class="reserved">
                            <a href="#">
                                Star Hits 2018 - 2019 All Right Reserved.
                            </a>
                        </p>
                    </div>
                    <div class="col-md-7">
                        <p class="tagline">
                            <a href="#">
                                SOUTHEAST ASIA'S LARGEST AND MOST INTEGRATED MEDIA GROUP
                            </a>
                        </p>
                    </div>
                    <div class="col-md-1">
                        <p class="logo-white">
                            <img alt="" src="{{asset('frontend/assets/img/mnc_media.png')}}"/>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>