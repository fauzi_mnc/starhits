<div aria-hidden="true" aria-labelledby="loginModalLabel" class="modal fade" id="loginModal" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="col text-center">
                    <div class="avatar">
                        <img alt="StarHits" src="{{asset('frontend/assets/img/black_logo.png')}}">
                    </div>
                </div>
                <div class="inline-block" style="display:absolute;">
                    <button type="button" class="close float-right" aria-label="Close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <p>
                        Login with your Star Hits account
                    </p>
                </div>
                <div class="w-75 mx-auto">
                    <form id="form-login" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <!-- <div class="social-btn text-center">
                            <button onclick="mncdigLogin('mJ1L0wMxzH2HFcqc')" class="btn btn-danger btn-block btn-lg">
                                <i>
                                    <img src="{{asset('frontend/assets/img/icon/sso.png')}}" alt="Google">
                                </i>
                                    Login with SSO MNC Digital 
                            </button>
                        </div>
                        <div class="or-seperator">
                            <i>
                                or
                            </i>
                        </div> -->
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input class="form-control" name="email" placeholder="" value="{{ old('email') }}" required="required" style="font-family:Fontawesome;" type="email">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" class="form-control" name="password" placeholder="" required="required" style="font-family:Fontawesome;" type="password">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input check" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}><p style="font-size: 10px; margin-top: 3px;"> Remember me</p>
                                    </label>
                                </div>
                            </div>
                        </div>
                        @if(env('APP_ENV') != 'local')
                        <div class="form-group{{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}" style="margin: 10px auto;">
                            {!! NoCaptcha::display() !!}
                                @if ($errors->has('g-recaptcha-response'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>
                                @endif
                        </div>
                        @endif
                        <div class="form-group">
                            <button class="btn btn-primary btn-lg btn-block login-btn" type="submit">
                                Log in
                            </button>
                        </div>
                        <p class="text-center small">
                            <a href="#" data-toggle="modal" data-target="#resModal" data-dismiss="modal" aria-label="Close">
                                Forgot Password?
                            </a>
                        </p>
                        <div class="or-seperator">
                        </div>
                        <p class="text-center small">
                            Not a user yet? 
                            <a href="#" data-toggle="modal" data-target="#regRoleModal" data-dismiss="modal" aria-label="Close">
                                Sign up now!
                            </a>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

{!! NoCaptcha::renderJs() !!}

@push('scripts')
{{-- <script type="text/javascript">
    @if (count($errors) > 0)
        $('#loginModal').modal('show');
    @endif
</script> --}}
@endpush