<!-- Stored in resources/views/child.blade.php -->
@extends('layouts.frontend')

@section('title', 'Creators')

@section('content')
<div class="container-fluid creators">
    <div class="col-md-12">
        <div class="col-sm-12">
            <div class="title">
                <div class="col-sm-12" style="padding: 0;">
                    <div class="row">
                        <div class="col-sm-8" style="padding: 0;">
                            <h7>
                                CREATORS
                            </h7>
                        </div>
                        <div class="col-sm-4" style="padding: 0;">
                            <a class="btn btn-filter filter" data-target=".bs-example-modal-lg-filter" data-toggle="modal" href="#">
                                <i class="fa fa-filter">
                                </i>
                                Filter
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12" style="padding: 0 10px 0px 10px">
            <div class="{{ count($creators)>=1? 'row text-center text-lg-left item-creators':'' }}">
                @forelse($creators as $value)
                <div class="col-lg-3 col-md-4 col-xs-6 col-creators">
                    <a href="{{ url('/creators/'.$value->slug) }}">
                        <img class="img-fluid" src="{{ url('/'). Image::url($value->cover,400,250,array('crop')) }}"/>
                    </a>
                    <div class="title-creators">
                        <h7>
                            <a href="{{ url('/creators/'.$value->slug) }}">
                                {{$value->name}}
                            </a>
                        </h7>
                    </div>
                    <div class="videos">
                        <p>
                            <a href="#">
                                {{$value->totalVideos}} Videos
                            </a>
                        </p>
                    </div>
                </div>
                @empty
                <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                            There is no data to display!
                        </div>
                </div>
                @endforelse
            </div>
        </div>
        <div class="col-md-12 page">
            <div class="row navigation">
                {{ $creators->links('vendor.pagination.custom') }}
            </div>
        </div>
    </div>
</div>

@include('frontend.creators.modals-filter')

@endsection
