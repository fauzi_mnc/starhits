<!-- Stored in resources/views/child.blade.php -->
@extends('layouts.frontend')

@section('title', 'Channels')

@push('preloader')
<div class="se-pre-con">
    <div class="col-sm-12 text-center pre-con">
        <div class="col">
            <div class="logo-preloader">
                <img alt="" class="img-fluid" src="{{ asset('frontend/assets/img/logo-home.png') }}"></img>
            </div>
        </div>
        <div class="col">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg">
                <defs>
                    <filter id="gooey">
                        <fegaussianblur in="SourceGraphic" result="blur" stddeviation="10">
                        </fegaussianblur>
                        <fecolormatrix in="blur" mode="matrix" result="goo" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7">
                        </fecolormatrix>
                        <feblend in="SourceGraphic" in2="goo">
                        </feblend>
                    </filter>
                </defs>
            </svg>
            <div class="blob blob-0"></div>
            <div class="blob blob-1"></div>
            <div class="blob blob-2"></div>
            <div class="blob blob-3"></div>
            <div class="blob blob-4"></div>
            <div class="blob blob-5"></div>
        </div>
    </div>
</div>
@endpush

@section('content')

<div class="container-fluid channels">
    <div class="row title">
        <div class="col-md-12">
            <h4>
                Channels
            </h4>
        </div>
    </div>
    <div class="col-md-12">
        <div class="{{ count($channels)>=1? 'sub-channels loadMore':'' }}">
        @forelse($channels as $value)
            <div class="col-sm-12 item-sub-channels">
                <div>
                    <div class="row mid-sub-channels">
                        <div class="col-sm-2 item-mid-sub-channels-image">
                            <a class="d-block" href="{{ url('/channels/'.$value->slug) }}">
                                <img src="{{ url('/'). Image::url($value->image,150,150,array('crop')) }}" />
                            </a>
                        </div>
                        <div class="col-sm-5 item-mid-sub-channels-text">
                            <div class="item-mid-sub-channels-text-title">
                                <h4>
                                    <a href="{{ url('/channels/'.$value->slug) }}">
                                        {{ strtoupper($value->title) }}
                                    </a>
                                </h4>
                            </div>
                            <div class="item-mid-sub-channels-text-paragraph">
                                <p>
                                    {!! str_limit($value->content, 100) !!}
                                </p>
                            </div>
                            <div class="item-mid-sub-channels-text-videos">
                                <label>
                                    {{ $value->totalvideo }} Videos | {{ $value->totalseries }} Series
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row mid-sub-channels-videos">
                        <div class="col-md-12" style="padding: 0;">
                            <div class="title">
                                <h7>
                                    LATEST {{ strtoupper($value->title) }}
                                </h7>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding: 0;">
                            <div class="{{ count($videos[$value->id])>=1? 'slider-mid-sub-channels-videos':'' }}">
                                @forelse($videos[$value->id] as $vid)
                                <div>
                                    <div class="item">
                                        <div class="imgTitle">
                                            <div class="mid-sub-channels-videos-overlay">
                                                <a href="{{ url('/videos/'.$vid->slug) }}">
                                                    @if($vid->image)
                                                        <img class="img-fluid mid-sub-channels-videos-overlay" src="{{ url('/'). Image::url($vid->image,400,225,array('crop')) }}" />
                                                    @else
                                                        <img class="img-fluid mid-sub-channels-videos-overlay" src="https://img.youtube.com/vi/{{ $vid->attr_6}}/mqdefault.jpg" style="width: 400; height: 250;" />
                                                    @endif
                                                </a>
                                                @if(!empty($value->attr_7))
                                                    <div class="item-mid-sub-channels-videos-overlay">
                                                        {{ $duration->formatted($vid->attr_7) }}
                                                    </div>
                                                @else

                                                @endif
                                            </div>
                                        </div>
                                        <p>
                                            <a href="{{ url('/videos/'.$vid->slug) }}">
                                                {{ $vid->title }}
                                            </a>
                                        </p>
                                    </div>
                                </div>
                                @empty
                                <div class="col-sm-12">
                                <br>
                                    <div class="alert alert-warning text-center" role="alert">
                                      There is no data to display!
                                    </div>
                                </div>
                                @endforelse
                            </div>
                        </div>
                    </div>
                    <div class="row mid-sub-channels-series">
                        <div class="col-md-12" style="padding: 0;">
                            <div class="title">
                                <h7>
                                    LATEST {{ strtoupper($value->title) }} SERIES
                                </h7>
                            </div>
                        </div>
                        <div class="col-sm-12" style="padding: 0;">
                            <div class="{{ count($series[$value->id])>=1? 'slider-mid-sub-channels-series':'' }}">
                                @forelse($series[$value->id] as $ser)
                                <div>
                                    <div class="item">
                                        <article class="caption">
                                            <img class="caption__media img-fluid" src="{{ url('/'). Image::url($ser->image,400,550,array('crop')) }}" />
                                            <a href="{{ url('/series/'.$ser->slug) }}">
                                                <div class="caption__overlay">
                                                    <h1 class="caption__overlay__title">
                                                        {{ $ser->title }}
                                                    </h1>
                                                    <h7 class="caption__overlay__content">
                                                        {{ $ser->totalVideo }} Videos
                                                    </h7>
                                                    @if(!empty($ser->excerpt))
                                                    <p class="caption__overlay__content">
                                                        {!!strip_tags($ser->excerpt)!!}
                                                    </p>
                                                    @else
                                                    <p class="caption__overlay__content">
                                                        {!!strip_tags(str_limit($ser->content, 100))!!}
                                                    </p>
                                                    @endif
                                                </div>
                                            </a>
                                        </article>
                                        <p>
                                            <a href="{{ url('/series/'.$ser->slug) }}">
                                                {{ $ser->title }}
                                            </a>
                                        </p>
                                        <p class="videos">
                                            <a href="#">
                                                {{ $ser->totalVideo }} Videos
                                            </a>
                                        </p>
                                    </div>
                                </div>
                                @empty
                                <div class="col-sm-12">
                                <br>
                                    <div class="alert alert-warning text-center" role="alert">
                                      There is no data to display!
                                    </div>
                                </div>
                                @endforelse
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @empty
        <div class="col-sm-12">
        <br>
            <div class="alert alert-warning text-center" role="alert">
              There is no data to display!
            </div>
        </div>
        @endforelse
        </div>
    </div>
</div>

@endsection
