
@section('css')
<link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">
<link href="{{ asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet">

<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@include('layouts.datatables_css')
@endsection
<!-- Title Field -->
<div class="form-group">
    {!! Form::label('title', 'Title:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => 125]) !!}
    </div>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('category', 'Category:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('category', ['0' => 'Ads Manual', '1' => 'Adsense'], null, ['placeholder' => 'Select Category', 'class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>

<!-- Status Field -->
<div class="form-group manual">
    {!! Form::label('type', 'Type:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('type', ['0' => 'Image', '1' => 'Video'], null, ['placeholder' => 'Select Type', 'class' => 'form-control']) !!}
    </div>
</div>

<!-- Picture Field -->
<div class="form-group image manual" style="display: none;">
    {!! Form::label('image', 'Picture:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <div class="kv-photo center-block text-center">
            <input id="photo" name="image" type="file" class="file-loading">
        </div>

        <div id="kv-photo-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
    </div>
</div>

<!-- Picture Field -->
<div class="form-group video manual" style="display: none;">
    {!! Form::label('video', 'Video:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <div class="kv-photo center-block text-center">
            <input id="video" name="video" type="file" class="file-loading">
        </div>

        <div id="kv-photo-errors-2" class="center-block alert alert-block alert-danger" style="display:none"></div>
    </div>
</div>

<div class="form-group manual">
    {!! Form::label('youtube_id', 'URL:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('youtube_id', null, ['class' => 'form-control', 'maxlength' => 250]) !!}
        <span class="help-block m-b-none">Navigating URL when you click on Ads.</span>
    </div>
</div>

<!-- Status Field -->
<div class="form-group manual">
    {!! Form::label('target', 'URL Target:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('target', ['0' => 'Open link in new window', '1' => 'Open link in current window'], null, ['placeholder' => 'Select URL Target', 'class' => 'form-control']) !!}
    </div>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('position', 'Position:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('position', ['0' => 'Ads on top video', '1' => 'Ads inside headers', '2' => 'Ads under article', '3' => 'Ads right content I', '4' => 'Ads right content II', '5' => 'Ads under featured video homepage'], null, ['placeholder' => 'Select Position', 'class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>

<!-- Title Field -->
<div class="form-group manual" id="data_5">
    {!! Form::label('dr', 'Date range:', ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        <div class="input-group input-daterange">
            {!! Form::text('start_date', isset($ads->start_date) ? $ads->start_date->format('Y-m-d') : '', ['class' => 'input-sm form-control']) !!}
            <span class="input-group-addon">to</span>
            {!! Form::text('end_date', isset($ads->end_date) ? $ads->end_date->format('Y-m-d') : '', ['class' => 'input-sm form-control']) !!}
        </div>
    </div>
</div>

<!-- Status Field -->
<div class="form-group manual">
    {!! Form::label('status', 'Status:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('status', ['0' => 'Inactive', '1' => 'Active'], null, ['placeholder' => 'Select Status', 'class' => 'form-control']) !!}
    </div>
</div>

<!-- Script Field -->
<div class="form-group adsense">
    {!! Form::label('script', 'Script:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::textarea('script', null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- Submit Field -->
<div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ads.index') !!}" class="btn btn-default">Cancel</a>
    </div>
</div>

@section('scripts')
<script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/tinymce.min.js') }}"></script>
<script>


    var formType = '{{ $formType }}';
    $('#photo').val('');
    $('#data_5 .input-daterange').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: 'yyyy-mm-dd'
    });

    $("#photo").fileinput({
        overwriteInitial: true,
        maxFileSize: 1024,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="{{ ($formType == 'edit' && $ads->image) ? asset($ads->image) : asset('img/upload.png') }}" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "jpeg", "gif"]
    });

    $("#video").fileinput({
        overwriteInitial: true,
        maxFileSize: 8192,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-2',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<video src="{{ ($formType == 'edit' && $ads->video) ? asset($ads->video) : asset('img/upload.png') }}" controls style="width:160px"></video>',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["mp4", "avi", "mov", "wmv","wma"]
    });

    $('select[name="type"]').change(function(){
        if ($(this).val() == "1") {
            $('.image').hide();
            $('.video').show();
        }else{
            $('.image').show();
            $('.video').hide();
        }
    });

    $('select[name="category"]').change(function(){
        if ($(this).val() == "1") {
            $('.manual').hide();
            $('.adsense').show();
        }else{
            $('.manual').show();
            $('.adsense').hide();
        }
    });

    var cat = $('#category').val();
    if (cat == "1") {
            $('.manual').hide();
            $('.adsense').show();
        }else{
            $('.manual').show();
            $('.adsense').hide();
            var type = $('#type').val();
            if (type == "1") {
                    $('.image').hide();
                    $('.video').show();
                }else{
                    $('.image').show();
                    $('.video').hide();
                }
        }
</script>
@if($formType == 'edit')
@include('layouts.datatables_js')
<script>
    $.fn.dataTable.ext.buttons.create = {
        action: function (e, dt, button, config) {
            $('#myModal').modal('show');
        }
    };

    var hash = document.location.hash;
    if (hash) {
        console.log(hash);
        $('.nav-tabs a[href="'+hash+'"]').tab('show')
    }

    // Change hash for page-reload
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
    window.location.hash = e.target.hash;
    });
</script>
@endif
@endsection