
@if(auth()->user()->roles->first()->name == 'finance')
    <div class='btn-group'>
        @if($status == 3 || $status == 5 || $status == 4 || $status == 6)
            <a href="{{ route('finance.revenue.view', $id) }}" class='btn btn-default btn-xs' title="Show Revenue">
                <i class="fa fa-eye"></i>
            </a>
        @endif
        @if(($status == 3 || $status == 4))
        <a href="{{ route('finance.revenue.download', $id) }}" class='btn btn-default btn-xs' title="Download Revenue PDF">
            <i class="fa fa-file-pdf-o"></i>
        </a>
        @endif
        @if(($status == 3 || $status == 4))
        <a href="{{ route('finance.revenue.excel', $id) }}" class='btn btn-default btn-xs' title="Download Revenue Excel">
            <i class="fa fa-file-excel-o"></i>
        </a>
        @endif
        @if($status == 3)
        
        <a href="{{ route('finance.revenue.edit', $id) }}" data-url="{{ route('revenue.edit', $id) }}" class='btn btn-default btn-xs' title="Edit Revenue">
            <i class="fa fa-pencil"></i>
        </a>
        @endif

        @if($status == 5)

        <a href="#" data-url="{{ route('finance.revenue.edit', $id) }}" class='btn btn-default btn-xs edit-revision' title="Edit Revenue">
            <i class="fa fa-pencil"></i>
        </a>
        @endif
    </div>
@else
    <div class='btn-group'>
        @if($status == 3 || $status == 5 || $status == 4 || $status == 6)
            <a href="{{ route('revenue.view', $id) }}" class='btn btn-default btn-xs' title="Show Revenue">
                <i class="fa fa-eye"></i>
            </a>
        @endif
        @if(($status == 3 || $status == 4))
        <a href="{{ route('revenue.download', $id) }}" class='btn btn-default btn-xs' title="Download Revenue PDF">
            <i class="fa fa-file-pdf-o"></i>
        </a>
        @endif
        @if(($status == 3 || $status == 4))
        <a href="{{ route('revenue.excel', $id) }}" class='btn btn-default btn-xs' title="Download Revenue Excel">
            <i class="fa fa-file-excel-o"></i>
        </a>
        @endif
        @if($status == 1)

        <a href="{{ route('revenue.edit', $id) }}" data-url="{{ route('revenue.edit', $id) }}" class='btn btn-default btn-xs' title="Edit Revenue">
            <i class="fa fa-pencil"></i>
        </a>
        @endif

        @if($status == 5)

        <a href="#" data-url="{{ route('revenue.edit', $id) }}" class='btn btn-default btn-xs edit-revision' title="Edit Revenue">
            <i class="fa fa-pencil"></i>
        </a>
        @endif
    </div>
@endif