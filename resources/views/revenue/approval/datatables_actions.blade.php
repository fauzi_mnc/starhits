@if(auth()->user()->hasRole('finance'))<div class='btn-group'>
    @if($status == 4 || $status == 5 || $status == 6)
    <a href="{{ route('finance.revenue.view', $id) }}" class='btn btn-default' title="Show Revenue">
        <i class="fa fa-eye"></i>
    </a>
    @endif
    @if($status == 3)
    <a href="{{ route('finance.revenue.download.report_pdf', $id) }}" class='btn btn-default' target="_blank" title="Download Revenue">
        <i class="fa fa-print"></i>
    </a>
    @endif
</div>
@else
<div class='btn-group'>
    @if($status == 4 || $status == 5 || $status == 6)
    <a href="{{ route('revenue.view', $id) }}" class='btn btn-default' title="Show Revenue">
        <i class="fa fa-eye"></i>
    </a>
    @endif
    @if($status == 3)
    <a href="{{ route('revenue.download.report_pdf', $id) }}" class='btn btn-default' target="_blank" title="Download Revenue">
        <i class="fa fa-print"></i>
    </a>
    <a href="{{ route('revenue.status_approved', $id) }}" class='btn btn-primary' onclick="return confirm('Are you sure?')">
        Approved
    </a>
    <a href="{{ route('revenue.status_revision', $id) }}" class='btn btn-warning' onclick="return confirm('Are you sure?')">
        Revision
    </a>
    <a href="{{ route('revenue.status_rejected', $id) }}" class='btn btn-danger' onclick="return confirm('Are you sure?')">
        Rejected
    </a>
    @endif
</div>
@endif
