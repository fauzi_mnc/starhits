@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Revenue</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Revenue</a>
            </li>
            <li class="active">
                <strong>View</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
        <div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content animated fadeInUp">
                    <div class="ibox">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="m-b-md">
                                        <img src="{{asset('frontend/assets/img/black_logo.png')}}" class="img-responsive pull-right">
                                    </div>
                                    @if($revenue->status_paid ==3)
                                        <!-- <img src="{{asset('/img/paid_.png')}}" width="100" class="hide" style="position: relative; margin-left: 300px; margin-bottom: -150px;"> -->
                                    @endif
                                    <dl class="dl-horizontal">
                                        <h3><strong>Report Revenue {{{auth()->user()->name}}} {{{$months[$revenue->month]}}} {{{$revenue->year}}}</strong></h3>
                                        <h3>Periode Cetak {{{$months[date('m')]}}} {{{date('Y')}}}</h3>
                                    </dl>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <dl class="dl-horizontal">
                                        @foreach($revenue->creator as $creator)
                                        @if($creator->id == auth()->user()->id)
                                            <dt>Revenue:</dt> <dd>Rp. {{{number_format($creator->pivot->revenue,0,",",".")}}}</dd>
                                            <dt>Cost Production:</dt> <dd>Rp. {{{number_format($creator->pivot->cost_production,0,",",".")}}}</dd>
                                            <dt>Nett Revenue:</dt> <dd>Rp. {{{number_format($creator->pivot->nett_revenue,0,",",".")}}}</dd>
                                                @if($creator->pivot->status_paid==3)
                                                    <dt>Payment Date:</dt> <dd>{{$creator->pivot->date_payment}}</dd>
                                                        <img src="{{asset('/img/paid_.png')}}" width="100" class="" style="position: relative; margin-left: 300px; margin-top: -100px;">
                                                @endif
                                        @endif
                                        @endforeach
                                    </dl>
                                </div>
                                <div class="col-lg-12">
                                    <dl class="dl-horizontal" >

                                        <h5>*Nett Revenue Belum dipotong pajak.</h5>
                                    </dl>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <a href="{{ route('creator.revenue.pdf', $revenue->id) }}" class="btn btn-default pull-right" style="margin-right: 10px;"><i class="fa fa-print"></i> Print</a>
                                    <a href="{{route('creator.revenue.index')}}" class="btn btn-success pull-right" style="margin-right: 10px;">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection