
<div class='btn-group'>
    <a href="{{ route('creator.revenue.view', $id) }}" class='btn btn-default' title="Show Revenue">
        <i class="fa fa-eye"></i>
    </a>
    <a href="{{ route('creator.revenue.pdf', $id) }}" class='btn btn-default' title="Download Revenue">
        <i class="fa fa-print"></i>
    </a>
</div>
