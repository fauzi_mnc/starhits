@extends('layouts.crud')
@section('css')
    @include('layouts.datatables_css')
@endsection
@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Approver & Checker</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Revenue</a>
            </li>
            <li class="active">
                <strong>Approver & Checker</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Approver</h5>
                </div>

                <div class="ibox-content">
                    <form style="margin-bottom: 10px;" class="form-horizontal" role="form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- Name field -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Name</label>

                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="name_approver" id="name-approver" required>
                                            <input type="hidden" name="id_approver" id="id-approver">
                                            <input type="hidden" name="row" id="idx-row-approver">
                                        </div>
                                    </div>
                                    <!-- Name field -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Email</label>

                                        <div class="col-md-6">
                                            <input type="email" class="form-control" name="email_approver" id="email-approver" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-9">
                                            <button type="button" class="btn btn-primary pull-right" id="btn-submit-approver" data-id="1">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Row -->
                        </div>                         
                    </form>
                    <table class="table table-bordered" id="table-revenue-approver">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($approver as $key => $value)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$value->name}}</td>
                                    <td>{{$value->email}}</td>
                                    <td> 
                                        <button type="button" class='btn btn-default btn-update' data-type="1" data-id="{{$value->id}}" title="Edit Approver">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach     
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Checker</h5>
                </div>

                <div class="ibox-content">
                    <form style="margin-bottom: 10px;" class="form-horizontal" role="form">
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- Name field -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Name</label>

                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="name_checker" id="name-checker" required>
                                            <input type="hidden" name="id_checker" id="id-checker">
                                            <input type="hidden" name="row" id="idx-row-checker">
                                        </div>
                                    </div>
                                    <!-- Name field -->
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Email</label>

                                        <div class="col-md-6">
                                            <input type="email" class="form-control" name="email_checker" id="email-checker" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-9">
                                            <button type="button" class="btn btn-primary pull-right" id="btn-submit-checker" data-id="2">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /Row -->
                        </div>                         
                    </form>
                    <table class="table table-bordered" id="table-revenue-checker">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($checker as $key => $value)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$value->name}}</td>
                                    <td>{{$value->email}}</td>
                                    <td> 
                                        <button type="button" class='btn btn-default btn-update' data-type="2" data-id="{{$value->id}}" title="Edit Checker">
                                            <i class="fa fa-pencil"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach     
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
@include('layouts.datatables_js')
<script>

    $(document).ready(function(){
        var table_approver = $('#table-revenue-approver').DataTable();
        var table_checker = $('#table-revenue-checker').DataTable();

        function postApprover(form_data, table)
        {
            $.ajax({
                url: "{{ route('revenue.approver.store') }}",
                method: "POST",
                data: form_data,
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                success: function(response){

                    if(response.status == 'success')
                    {
                        toastr.success(response.message);
                        var data = response.data;
                        table.row.add( [
                            response.length,
                            data.name,
                            data.email,
                            '<button href="#" class="btn btn-default btn-update" data-type="'+data.type+'" data-id="'+data.id+'" title="Edit Approver"><i class="fa fa-pencil"></i></button>'
                        ] ).draw( false );   

                    }else{
                        toastr.error(response.message);
                    }
                    
                },
                error: function(response){
                    toastr.error(response.message);
                }
            });
        }

        function updateApprover(form_data, table, idx_row)
        {
            $.ajax({
                url: "{{ route('revenue.approver.update') }}",
                method: "PUT",
                data: form_data,
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                success: function(response){

                    if(response.status == 'success')
                    {
                        toastr.success(response.message);
                        //console.log(response);

                        var data = response.data;
                        var temp;

                        if(data.type == 1){
                            temp = table_approver.row(parseInt(idx_row)-1).data();
                        }else{
                            temp = table_checker.row(parseInt(idx_row)-1).data();
                        }

                        //console.log(temp);
                        temp[1] = data.name;
                        temp[2] = data.email;
                        temp[3] = '<button class="btn btn-default btn-update" data-type="'+data.type+'" data-id="'+data.id+'" title="Edit Approver"><i class="fa fa-pencil"></i></button>';

                        table_approver.row(parseInt(idx_row)-1).data(temp).invalidate();
                        if(data.type == 1){
                            table_approver.row(parseInt(idx_row)-1).data(temp).invalidate();
                        }else{
                            table_checker.row(parseInt(idx_row)-1).data(temp).invalidate();
                        }

                        if(data.type == 1){
                            $('#id-approver').val("");
                            $('#name-approver').val("");
                            $('#email-approver').val("");
                            $('idx-row-approver').val("");
                            $('#btn-submit-approver').text('Submit');
                        }else{
                            $('#id-checker').val("");
                            $('#name-checker').val("");
                            $('#email-checker').val("");
                            $('idx-row-checker').val("");
                            $('#btn-submit-checker').text('Submit');
                        }

                    }else{
                        toastr.error(response.message);
                    }
                    
                },
                error: function(response){
                    toastr.error(response.message);
                }
            });
        }

        $('#btn-submit-approver').click(function(e){
            e.preventDefault();
            var form_data = {
                id : $('#id-approver').val(),
                name: $('#name-approver').val(),
                email: $('#email-approver').val(),
                type: $(this).data('id')
            }

            var row = $('#idx-row-approver').val();
            //console.log(row);
            if($('#id-approver').val()){
                updateApprover(form_data, table_approver, row);
            }
            else{
                postApprover(form_data, table_approver);
            }
                        
        });

        $('#btn-submit-checker').click(function(e){
            e.preventDefault();
            var form_data = {
                id : $('#id-checker').val(),
                name: $('#name-checker').val(),
                email: $('#email-checker').val(),
                type: $(this).data('id')
            }

            var row = $('#idx-row-checker').val();
            //console.log(row);
            if($('#id-checker').val()){
                updateApprover(form_data, table_checker, row);
            }
            else{
                postApprover(form_data, table_checker);
            }          
        });

        $('#table-revenue-approver').on('click', '.btn-update', function(e){
            var data = table_approver.row( $(this).parents('tr') ).data();
            $('#id-approver').val($(this).data('id'));
            $('#name-approver').val(data[1]);
            $('#email-approver').val(data[2]);
            $('#idx-row-approver').val(data[0]);
            $('#btn-submit-approver').text('Update');

            //console.log($('#id-approver').val());
        });

        $('#table-revenue-checker').on('click', '.btn-update', function(e){
            var data = table_checker.row( $(this).parents('tr') ).data();
            $('#id-checker').val($(this).data('id'));
            $('#name-checker').val(data[1]);
            $('#email-checker').val(data[2]);
            $('#idx-row-checker').val(data[0]);
            $('#btn-submit-checker').text('Update');

            //console.log($('#id-checker').val());
        });
    });

</script>
@endsection
