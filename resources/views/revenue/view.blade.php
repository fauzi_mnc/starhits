@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Revenue</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Revenue</a>
            </li>
            <li class="active">
                <strong>View</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                View Revenue
            </div>
            <div class="panel-body">
                {!! Form::model($revenue, ['route' => ['revenue.update', $revenue->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true, 'id' => 'example-form', 'files' => true]) !!}
                    @include('revenue.fields', ['formType' => 'view'])
                </form>
            </div>

        </div>
        
    </div>
</div>
@endsection