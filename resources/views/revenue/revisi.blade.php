@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Revenue</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Revenue</a>
            </li>
            <li class="active">
                <strong>Edit</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                Revisi Revenue 
            </div>
            <div class="panel-body">
                @if(auth()->user()->roles->first()->name == 'admin')
                {!! Form::model($revenue, ['route' => ['revenue.post.revisi', $revenue->id], 'method' => 'post', 'class' => 'form-horizontal', 'files' => true, 'id' => 'example-form', 'files' => true]) !!}
                @elseif(auth()->user()->roles->first()->name == 'finance')
                {!! Form::model($revenue, ['route' => ['finance.revenue.post.revisi', $revenue->id], 'method' => 'post', 'class' => 'form-horizontal', 'files' => true, 'id' => 'example-form', 'files' => true]) !!}
                @endif
                @include('revenue.fields_revisi', ['formType' => 'edit'])
                </form>
            </div>

        </div>
        
    </div>
</div>




@endsection