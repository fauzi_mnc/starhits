<!doctype html>
<html amp lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1,initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <link rel="preload" as="script" href="https://cdn.ampproject.org/v0.js">
    <link rel="preconnect dns-prefetch" href="https://fonts.gstatic.com/" crossorigin>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <!-- Import other AMP Extensions here -->
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
    {{-- <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script> --}}

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,900,900i&display=swap" rel="stylesheet">
    <style amp-custom>
        {{ include"frontend/assets/css/bootstrap.css" }}
        {{ include"css/inspinia.css" }}

        *,
        ::after,
        ::before {
            box-sizing: border-box
        }

        html {
            font-family: sans-serif;
            line-height: 1.15;
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: transparent
        }

        article,
        aside,
        figcaption,
        figure,
        footer,
        header,
        hgroup,
        main,
        nav,
        section {
            display: block
        }

        body {
            margin: 0;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            text-align: left;
            background-color: #fff
        }

        [tabindex="-1"]:focus {
            outline: 0
        }

        hr {
            box-sizing: content-box;
            height: 0;
            overflow: visible
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            margin-top: 0;
            margin-bottom: .5rem
        }

        p {
            margin-top: 0;
            margin-bottom: 1rem
        }

        abbr[data-original-title],
        abbr[title] {
            text-decoration: none;
            -webkit-text-decoration: none dotted;
            text-decoration: none dotted;
            cursor: help;
            border-bottom: 0;
            -webkit-text-decoration-skip-ink: none;
            text-decoration-skip-ink: none
        }

        address {
            margin-bottom: 1rem;
            font-style: normal;
            line-height: inherit
        }

        dl,
        ol,
        ul {
            margin-top: 0;
            margin-bottom: 1rem
        }

        ol ol,
        ol ul,
        ul ol,
        ul ul {
            margin-bottom: 0
        }

        dt {
            font-weight: 700
        }

        dd {
            margin-bottom: .5rem;
            margin-left: 0
        }

        blockquote {
            margin: 0 0 1rem
        }

        b,
        strong {
            font-weight: bolder
        }

        small {
            font-size: 80%
        }

        sub,
        sup {
            position: relative;
            font-size: 75%;
            line-height: 0;
            vertical-align: baseline
        }

        sub {
            bottom: -.25em
        }

        sup {
            top: -.5em
        }

        a {
            color: #d9b336;
            text-decoration: none;
            background-color: transparent
        }

        a:hover {
            color: #b9992e;
            text-decoration: none
        }

        a:not([href]):not([tabindex]) {
            color: inherit;
            text-decoration: none
        }

        a:not([href]):not([tabindex]):focus,
        a:not([href]):not([tabindex]):hover {
            color: inherit;
            text-decoration: none
        }

        a:not([href]):not([tabindex]):focus {
            outline: 0
        }

        code,
        kbd,
        pre,
        samp {
            font-family: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
            font-size: 1em
        }

        pre {
            margin-top: 0;
            margin-bottom: 1rem;
            overflow: auto
        }

        figure {
            margin: 0 0 1rem
        }

        img {
            vertical-align: middle;
            border-style: none
        }

        svg {
            overflow: hidden;
            vertical-align: middle
        }

        table {
            border-collapse: collapse
        }

        caption {
            padding-top: .75rem;
            padding-bottom: .75rem;
            color: #6c757d;
            text-align: left;
            caption-side: bottom
        }

        th {
            text-align: inherit
        }

        label {
            display: inline-block;
            margin-bottom: .5rem
        }

        button {
            border-radius: 0
        }

        button:focus {
            outline: 1px dotted;
            outline: 5px auto -webkit-focus-ring-color
        }

        button,
        input,
        optgroup,
        select,
        textarea {
            margin: 0;
            font-family: inherit;
            font-size: inherit;
            line-height: inherit
        }

        button,
        input {
            overflow: visible
        }

        button,
        select {
            text-transform: none
        }

        select {
            word-wrap: normal
        }

        [type=button],
        [type=reset],
        [type=submit],
        button {
            -webkit-appearance: button
        }

        [type=button]:not(:disabled),
        [type=reset]:not(:disabled),
        [type=submit]:not(:disabled),
        button:not(:disabled) {
            cursor: pointer
        }

        [type=button]::-moz-focus-inner,
        [type=reset]::-moz-focus-inner,
        [type=submit]::-moz-focus-inner,
        button::-moz-focus-inner {
            padding: 0;
            border-style: none
        }

        input[type=checkbox],
        input[type=radio] {
            box-sizing: border-box;
            padding: 0
        }

        input[type=date],
        input[type=datetime-local],
        input[type=month],
        input[type=time] {
            -webkit-appearance: listbox
        }

        textarea {
            overflow: auto;
            resize: vertical
        }

        fieldset {
            min-width: 0;
            padding: 0;
            margin: 0;
            border: 0
        }

        legend {
            display: block;
            width: 100%;
            max-width: 100%;
            padding: 0;
            margin-bottom: .5rem;
            font-size: 1.5rem;
            line-height: inherit;
            color: inherit;
            white-space: normal
        }

        progress {
            vertical-align: baseline
        }

        [type=number]::-webkit-inner-spin-button,
        [type=number]::-webkit-outer-spin-button {
            height: auto
        }

        [type=search] {
            outline-offset: -2px;
            -webkit-appearance: none
        }

        [type=search]::-webkit-search-decoration {
            -webkit-appearance: none
        }

        ::-webkit-file-upload-button {
            font: inherit;
            -webkit-appearance: button
        }

        output {
            display: inline-block
        }

        summary {
            display: list-item;
            cursor: pointer
        }

        template {
            display: none
        }

        [hidden] {
            display: none
        }

        #container,
        #form-container {
            display: flex;
            background: #ffffff;
            width: auto;
        }

        #container {
            flex-direction: row;
            width: 100vw;
            height: 100vh
        }

        form {
            width: auto;
        }
        

        @media(max-width:870px) {
            #form-container {
                background: #ffffff;
                justify-content: stretch
            }
        }

        #picture-container {
            background-color: orange;
            width: 60vw;
            padding-top: 100vh;
            position: relative;
            -webkit-clip-path: polygon(0 0, 77% 0, 100% 100%, 0 100%);
            clip-path: polygon(0 0, 77% 0, 100% 100%, 0 100%)
        }

        #picture-container img {
            object-fit: cover
        }

        @media(max-width:860px) {
            #picture-container {
                display: none
            }
            #form-container {
                width: 100%;
                background: url('../frontend/assets/images/Layer-40.png') no-repeat center center fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
                box-shadow: inset 0 0 0 2000px rgba(255, 250, 150, 0.3);
                background-color: #ffffff;
                background-repeat: no-repeat;
                background-size: auto;
                align-items: center;
                text-align: center
            }

            form {
                width: 80%;
                padding: 0;
                margin: 0 auto
            }
            .or{
                color:white;
            }
        }

        #form-container {
            background-color: #ffffff;
            align-items: center;
            text-align: center
        }

        form {
            padding: 0;
            margin: 0 auto
        }
        
        .form-group {
            padding: 1em 0
        }

        input {
            border-radius: 2em;
            line-height: 2em;
            padding: 0 1em;
            border: 1px solid #2f4f4f;
            font-size: 14px;
            width: 250px;
            display: block;
        }

        input:focus{
            border-color: orange;
            outline: none;
            display: block;
        }

        input[type=submit] {
            background-color: orange;
            border-color: transparent;
            padding: 0 3em
        }

        :-ms-input-placeholder {
            color: #212121;
            font-weight: 700
        }

        ::-webkit-input-placeholder {
            color: #212121;
            font-weight: 700
        }

        ::placeholder {
            color: #212121;
            font-weight: 700
        }

        .help-block {
            font-size: 12px;
            color: red;
        }
    </style>
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>

    <link rel="canonical" href=".">
    <title>Starhits</title>
</head>
<body>
    <div id="container">
        <div id="picture-container">
            <amp-img src="{{ asset('frontend/assets/images/Layer-40.png')}}" layout="fill" width="1" height="1"></amp-img>
        </div>
        <div id="form-container">
            <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}
                <amp-img src="{{ asset('frontend/assets/images/logo.png')}}" layout="responsive" width="162" height="52"></amp-img>

                <h2 style="font-size: 20px;color:#000000;margin: 1rem;" class="logo-name">Reset Password</h2>
                @if (session('status'))
                <span class="help-block">
                    {{ session('status') }}
                </span>
                @endif
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="text" class="" name="email" value="{{ old('email') }}" placeholder="enter your email" required autofocus>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <input type="submit" value="Send Password Reset Link">
                <a href="{{route('login')}}"><small>Back to login</small></a>
                <!-- <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <a class="btn btn-sm btn-white btn-block" href="register.html">Create an account</a> -->
            </form>
        </div>
    </div>
    <script async="" src="//www.google-analytics.com/analytics.js"></script>
    <script src="{{ asset('js/jquery/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script>
    {!! NoCaptcha::renderJs() !!}
    <script>
    $(document).ready(function() {
      $("#form-login").on("submit",function(e) {
        //e.preventDefault();
        var form = $(this);
        var field = form.find("input[name=password]");
        var hash = btoa(field.val());
        var hash2 = btoa(hash);
        var slice = hash2.substr(hash2.length - 2);
        hash2 = hash2.slice(0, -2);
        var random_char = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        var slice_random = random_char.substring(0, 14); //only get 14 characters
        var new_str = hash2+slice_random+slice;
        field.val(new_str);
      });

      if (self === top) {
            var antiClickjack = document.getElementById("antiClickjack");
            antiClickjack.parentNode.removeChild(antiClickjack);
        } else {
            top.location = self.location;
        }
    });

    $(".toggle-password").click(function() {

    $(this).toggleClass("fa-eye fa-eye-slash");
        var input = $($(this).attr("toggle"));
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
    </script>
</body>

</html>