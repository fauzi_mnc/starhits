@extends('layouts.crud')
@section('css')
    @include('layouts.datatables_css')

<link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">



<!-- <link rel="stylesheet" href="https://www.cssscript.com/demo/animated-customizable-range-slider-pure-javascript-rslider-js/css/rSlider.min.css">
<script src="https://www.cssscript.com/demo/animated-customizable-range-slider-pure-javascript-rslider-js/js/rSlider.min.js"></script> -->
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css">

<style type="text/css">
    .filter-hidden{
        transition: 0.5s;
    }
    .navy-bg{
        transition: 0.5s;
    }
    .navy-bg:hover{
        background-color: #10705c;
        transition: 0.5s;
    }
</style>

@endsection
@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Browse Influencer</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Campaign</a>
            </li>
            <li>
                View
            </li>
            <li class="active">
                <strong>Browse Influencers</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
    @include('flash::message')
    <div class="row border-bottom white-bg dashboard-header">
        <!-- <div class="col-sm-12"> -->
            <!-- <h3>Filter Influencers</h3>
            <hr> -->

        <div class="col-sm-2"><h3>Filter Influencers</h3></div>
        <div class="col-sm-10">
            <button type="button" class="btn btn-sm btn-primary pull-right" id="hidden_filter"> Show Filter</button>
        </div>

       <!--  </div> -->
        <div class="col-sm-2 filter-hidden">
            <label>Gender : </label>
            <div style="display: inline-block; vertical-align: top;">
                <div class="i-checks"><label> <input type="radio" value="" checked name="gender"> <i></i> All </label></div>
                <div class="i-checks"><label> <input type="radio" value="M" name="gender"> <i></i> Male </label></div>
                <div class="i-checks"><label> <input type="radio" value="F" name="gender"> <i></i> Female </label></div>
            </div>
        </div>
        <div class="col-sm-3 filter-hidden">
            <label>Followers : </label>
            <div style="display: inline-block; vertical-align: top;">
                <div class="i-checks "><label> <input type="radio" data-id="1" name="follower"> <i></i> Less than 5k </label></div>
                <div class="i-checks "><label> <input type="radio" data-id="2" name="follower"> <i></i> 5k-10k </label></div>
                <div class="i-checks "><label> <input type="radio" data-id="3" name="follower"> <i></i> 10k-100k </label></div>
                <div class="i-checks "><label> <input type="radio" data-id="4" name="follower"> <i></i> 100k-500k </label></div>
                <div class="i-checks "><label> <input type="radio" data-id="5" name="follower"> <i></i> 500k-1000k+ </label></div>
            </div>
        </div>

        <div class="col-sm-4 filter-hidden">
            <div class="row">                
                <div class="col-sm-12">
                    <label>Filter Username :</label>
                    <div class="input-group">
                        <input type="text" onkeyup="exec();" name="query" id="query" placeholder="username" class="form-control">
                        <span class="input-group-btn">
                        <button type="button" class="btn btn-sm btn-primary" id="gas"> Filter</button> </span>
                    </div>                    
                </div>
                <div class="col-sm-12">&nbsp;</div>
                <div class="col-sm-12">
                    <label>Categories : </label>
                    <div style="display:inline-block; vertical-align: top;">
                        <select id="multipleselect" class="form-control" multiple="multiple" onchange="exec();">
                            @foreach($categories as $val)
                                <option value="{{$val->id}}">{{$val->title}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            
        </div>
        <!-- <div class="col-sm-3 filter-hidden">
            <button type="button" class="btn btn-sm btn-danger" id="hapus"> Clear Filter</button>
        </div> -->

        

       
        
    </div>
    <!-- <div class="row border-bottom white-bg ">
        <div class="col-sm-2"><br><h3>Filter Influencers</h3><br></div>
        <div class="col-sm-10">
            <br>
            <button type="button" class="btn btn-sm btn-primary pull-right" id="hidden_filter"><i class="glyphicon glyphicon-chevron-up"></i> Hidden Filter</button>
            <br>
        </div>
    </div> -->      

    <div class="row list">
        @foreach($influencers as $influencer)
        {{--  @php
            dd($influencer->username);
        @endphp --}}

        <a href="{{route('influencer.diagram.index',$influencer->id)}}">

            <div class="col-md-4" data-aos="zoom-in" data-aos-offset="150"
         data-aos-easing="ease-in-sine">
                <div class="widget-head-color-box navy-bg p-lg text-center">
                    <div class="m-b-md">
                    <h2 class="font-bold no-margins">
                        {{$influencer->name}}
                        {{--  {{dd($influencer->detailInfluencer['username'])}}  --}}
                    </h2>
                        @if(!empty($influencer->detailInfluencer['username']))
                        <small>{{ $influencer->detailInfluencer['username'] }} </small>
                        @else
                        <small><br></small>
                        @endif
                    </div>
                     
                        @if (!empty($influencer->detailInfluencer['profile_picture_ig']) || @getimagesize($influencer->detailInfluencer['profile_picture_ig']))
                            <img width="200" src="{{ $influencer->detailInfluencer['profile_picture_ig'] }}" class="img-circle circle-border m-b-md" alt="profile">
                        @else
                            <img width="200" src="{{ asset('images/nophoto.jpg') }}" class="img-circle circle-border m-b-md" alt="profile">
                        @endif
                    
                    <div>
                        @if(!empty($influencer->detailInfluencer['followers_ig']))
                        <span><strong>{{ bd_nice_number($influencer->detailInfluencer['followers_ig']) }} Followers</strong></span>
                        @else
                        <span><strong>0 Followers</strong></span>
                        @endif
                    </div>
                </div>
            </div>
        </a>
        @endforeach
    </div>
    <div class="row border-bottom white-bg dashboard-header" style="margin-top: 10px;">
        <div class="col-sm-12">
            @if(auth()->user()->roles->first()->name === 'admin')
            <a href="{{route('influencer.index')}}" class="btn btn-default pull-right" style="margin-right: 15px">Back</a>
            @endif
        </div>
    </div>
@endsection
@section('scripts')
<script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
    @include('layouts.datatables_js')
    <script>

        var max_followers = 0;
        var min_followers = 0;
        var cat_id = null;
        var username = null;
        var gender = null;
        var influencers = [];
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        var list = $('.list');

        var table = $('#table-influencer').DataTable({
            "lengthChange": false,
            "searching": false
        });

        $('#query').bind("change keyup", function() {
            var val = $(this).val();
            var regex = /[%*]/g;
            if (val.match(regex)) {
                val = val.replace(regex, "");
                $(this).val(val);
            }
            $("p").html(val);
        });

        $('#hapus').click(function(){
            $('.i-checks').iCheck('uncheck');
            $('#query').val('');

            max_followers = 0;
            min_followers = 0;
            cat_id = null;
            username = null;
            gender = null;
            getData();
        });

        $('#gas').click(function(){
            exec();
        });

        $('#btn-followers').click(function(){
            $('.btn-followers').prop('disabled', false);
            $('.btn-all').prop('disabled', false);
            $(this).prop('disabled', true);
            var id = $(this).data("id");

            switch (id) {
                case 1:
                    max_followers = 5000;
                    min_followers = 0;
                    break;
                case 2:
                    max_followers = 10000;
                    min_followers = 5000;
                    break;
                case 3:
                    max_followers = 100000;
                    min_followers = 10000;
                    break;
                case 4:
                    max_followers = 500000;
                    min_followers = 100000;
                    break;
                case 5:
                    max_followers = 1000000000;
                    min_followers = 500000;
                    break;
            }

            getData();

        });

        $('.btn-all').click(function(){
            $('.btn-followers').prop('disabled', false);
            $(this).prop('disabled', true);
            max_followers = 0;
            min_followers = 0;
            getData();
        });

        $('.btn-category').click(function(){
            $('.btn-category').prop('disabled', false);
            $(this).prop('disabled', true);
            var id = $(this).data("id");

            cat_id = id;
            getData();
        });

        $('#btn-save').click(function(){
            console.log('save');
            $.each($("input[name='influencers[]']"), function () {
                //console.log($(this));
                if($(this).is(":checked"))
                {   
                    influencers.push($(this).data("id"));
                }
            });
            //console.log(influencers);

            $.ajax({
                url: "{{route('admin.campaign.addtotableinfluencers')}}",
                method: "GET",
                data: {
                    id_campaign: {{Request::segment(3)}},
                    id_influencers: influencers
                },
                success: function(response){
                    console.log(response);
                    if(response.result.status == 'success'){
                        console.log('test');
                        toastr.success("Influencer added");
                        window.location.replace("{{ route('admin.campaign.look', Request::segment(3)) }}");
                    }else{
                        toastr.error("Insufficient campaigns budget. Please edit budget first.");
                    }                    
                },
                error: function(response){
                    console.log(response);
                }
            }); 
        });

        function exec(){
            var cat = $('[name="cat"]:checked').map(function(){
                            return $(this).val();
                        }).toArray();
            console.log(cat);

            var id = $('[name="follower"]:checked').data("id");
            console.log(id);

            switch (id) {
                case 1:
                    max_followers = 5000;
                    min_followers = 0;
                    break;
                case 2:
                    max_followers = 10000;
                    min_followers = 5000;
                    break;
                case 3:
                    max_followers = 100000;
                    min_followers = 10000;
                    break;
                case 4:
                    max_followers = 500000;
                    min_followers = 100000;
                    break;
                case 5:
                    max_followers = 1000000000;
                    min_followers = 500000;
                    break;
            }
            console.log(id);

            var gen = $('[name="gender"]:checked').val();
            console.log($('[name="gender"]:checked').val());

            var user = $('[name="query"]').val();
            console.log("user :"+ user);

            cat_id = cat;
            username = user;
            gender = gen;
            getData();
        }

        function getData()
        {   
            $.ajax({
                @if(auth()->user()->roles->first()->name === 'admin')
                    url: "{{route('admin.influencer.getInfluencer')}}",
                @else
                    url: "{{route('brand.influencer.getInfluencer')}}",
                @endif
                method: "GET",
                data: {
                    max_followers: max_followers,
                    min_followers: min_followers,
                    cat_id: cat_id,
                    username: username,
                    gender: gender,
                },
                success: function(response){
                    console.log(response);
                    if(response.result.length > 0){
                        list.empty();
                        $.each(response.result, function (index, value) {
                            list.append('<div class="col-lg-4"><div class="widget-head-color-box navy-bg p-lg text-center"><div class="m-b-md"><h2 class="font-bold no-margins">'+value.name+'</h2><small>'+value.detail_influencer.username+'</small></div><img width="200" src="'+value.detail_influencer.profile_picture_ig+'" class="img-circle  circle-border m-b-md" alt="profile"><div><span>'+value.detail_influencer.followers_ig+' Followers</span></div></div></div>');
                        });
                    }
                    else
                    {
                        list.empty();
                        list.append('<div class="col-lg-12" style="margin-top: 20px;"><div class="ibox float-e-margins"><div class="ibox-content text-center p-md"><h2><span class="text-navy">Sorry, the user you are looking for was not found.</span><p></div></div></div>');
                    }
                    
                },
                error: function(response){
                    console.log(response);
                }
            });            
            
        }
        $(document).ready(function(){
            //$('#multipleselect').multiselect();
            $('#multipleselect').multiselect({
                includeSelectAllOption : true,
                nonSelectedText: 'Select an Option'
              });

            AOS.init();
            $('.filter-hidden').hide();
        });

        $('#hidden_filter').click(function(){
            $('.filter-hidden').toggle();
            $("#hidden_filter").text($("#hidden_filter").text() == ' Show Filter' ? ' Hidden Filter' : ' Show Filter');
        });

         /*(function () {
            'use strict';

            var init = function () {                


                var slider3 = new rSlider({
                    target: '#slider3',
                    values: {min: 0, max: 1000},
                    step: 10,
                    range: true,
                    set: [10, 40],
                    scale: false,
                    labels: false,                   
                    //set:    null, // an array of preselected values
                    width:    500,
                   // labels:   true,
                    tooltip:  true,
                    onChange: function (vals) {
                        console.log(vals);
                    }
                });

            };
            window.onload = init;
        })();*/

    </script>
@endsection
