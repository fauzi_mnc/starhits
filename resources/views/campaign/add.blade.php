@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Campaign Add Influencer</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Campaign</a>
            </li>
            <li class="active">
                <strong>Table</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
            @if(auth()->user()->roles->first()->name === 'admin')
            {!! Form::model($camp, ['route' => ['admin.campaign.infl'], 'method' => 'post', 'class' => 'form-horizontal', 'files' => true]) !!}
            @else
            {!! Form::model($camp, ['route' => ['brand.campaign.infl'], 'method' => 'post', 'class' => 'form-horizontal', 'files' => true]) !!}
            @endif
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Campaign</h5>
                </div>
                <div class="ibox-content">
                        @include('campaign.influencer', ['formType' => 'edit'])
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Search Influencer</h5>
                        </div>

                        <div class="ibox-content">
                            {!! $dataTable->table(['width' => '100%']) !!}
                        </div>
                    </div>
                </div>
            </div>
            @include('flash::message')
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Influencer</th>
                                            <th>Instagram Video Posting Rate</th>
                                            <th>Instagram Story Posting Rate</th>
                                            <th>Instagram Photo Posting Rate</th>
                                            <th>Youtube Posting Rate</th>
                                            <th>IG Highlight Rate</th>
                                            <th>Package Rate</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($infl as $val)
                                        <tr>
                                            <td><input type="hidden" name="check[]" value="{{$val->id}}">{{$val->name}}</td>
                                            <td>{{$val->detailInfluencer->instagram_video_posting_rate}}</td>
                                            <td>{{$val->detailInfluencer->instagram_story_posting_rate}}</td>
                                            <td>{{$val->detailInfluencer->instagram_photo_posting_rate}}</td>
                                            <td>{{$val->detailInfluencer->youtube_posting_rate}}</td>
                                            <td>{{$val->detailInfluencer->ig_highlight_rate}}</td>
                                            <td>{{$val->detailInfluencer->package_rate}}</td>
                                            @foreach($val->influencer as $infl)
                                                @if($infl->campaign_id == $id AND $infl->user_id == $val->id)
                                                    @if($infl->approval == 4 || is_null($infl->approval))
                                                        <td>Process</td>
                                                        @if($infl->approval == 4)
                                                            @if(auth()->user()->roles->first()->name === 'admin')
                                                            <td><a href="/admin/campaign/{{$val->id}}/{{Request::segment(3)}}/rft"><i class="fa fa-times text-danger"></i></a></td>
                                                            @else
                                                            <td><a href="/brand/campaign/{{$val->id}}/{{Request::segment(3)}}/rft"><i class="fa fa-times text-danger"></i></a></td>
                                                            @endif
                                                        @elseif(is_null($infl->approval))
                                                            @if(auth()->user()->roles->first()->name === 'admin')
                                                            <td>@if($camp->status != 3)<a href="/admin/campaign/{{$val->id}}/{{Request::segment(3)}}/rif" onclick="return confirm('Are you sure want to remove this influencer?')"><i class="fa fa-times text-danger"></i></a>@endif</td>
                                                            @else
                                                            <td>@if($camp->status != 3)<a href="/brand/campaign/{{$val->id}}/{{Request::segment(3)}}/rif" onclick="return confirm('Are you sure want to remove this influencer?')"><i class="fa fa-times text-danger"></i></a>@endif</td>
                                                            @endif
                                                        @endif
                                                    @else
                                                    <td>{{!empty($infl->approval) && $infl->approval === 1 ? 'Approved' : 'Rejected'}}</td>
                                                    <td></td>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                        @if($camp->status != 3)
                                        {!! Form::submit('Process', ['class' => 'btn btn-primary pull-right']) !!}
                                        <!-- <a href="{!! route('admin.campaign.browse', $id) !!}" id="browse-influencer" class="btn btn-success pull-right" style="margin-right: 15px">Browse Influencer</a> -->
                                        @endif
                                        @if(auth()->user()->roles->first()->name === 'admin')
                                        <a href="{!! route('admin.campaign.index') !!}" class="btn btn-default">Back</a>
                                        @else
                                        <a href="{!! route('brand.campaign.index') !!}" class="btn btn-default">Back</a>
                                        @endif
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            {!! Form::close() !!}
@endsection
