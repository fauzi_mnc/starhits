
@section('css')
<link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">
<link href="{{ asset('js/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/bootstrap-tagsinput.css') }}" rel="stylesheet">
<link href="{{ asset('css/chosen.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">

<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}

.mail{
    margin-right: 1%;
}
</style>
@include('layouts.datatables_css')
@endsection
{{ Form::hidden('id', $camp->camp_id) }}
<!-- Brand Field -->
<div class="form-group">
    {!! Form::label('brand_id', 'Influencer:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('influencer', $user, null, ['placeholder' => 'Select Influencer', 'class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('product', 'Post to:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::select('post_to', $post, null, ['placeholder' => 'Select Post To', 'class' => 'form-control', 'required' => 'required']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('budget', 'Post Budget:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <div class="input-group m-b">
            <span class="input-group-addon">Rp.</span>
            {!! Form::text('post_budget', null, ['class' => 'form-control', 'maxlength' => 15, 'required' => 'required']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-12">
        {!! Form::submit('Update', ['class' => 'btn btn-primary pull-right']) !!}
        <a href="{!! route('admin.campaign.index') !!}" class="btn btn-default">Back</a>
    </div>
</div>
<!-- Submit Field -->

@section('scripts')
<script src="{{ asset('js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/chosen.jquery.min.js') }}"></script>
<script src="{{ asset('js/plugins/typeahed.min.js') }}"></script>
<script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>
<script>
    var formType = '{{ $formType }}'; 
</script>
@if($formType == 'edit')
@include('layouts.datatables_js')
<script>
    $.fn.dataTable.ext.buttons.create = {
        action: function (e, dt, button, config) {
            $('#myModal').modal('show');
        }
    };

    var hash = document.location.hash;
    if (hash) {
        console.log(hash);
        $('.nav-tabs a[href="'+hash+'"]').tab('show')
    }

    // Change hash for page-reload
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
    window.location.hash = e.target.hash;
    });
</script>
{!! $dataTable->scripts() !!}
@include('layouts.datatables_limit')
@endif
@endsection