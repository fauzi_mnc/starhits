@section('css')
    @include('layouts.datatables_css')
@endsection

<table class="table" id="table">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th class="text-center">Title</th>
            <th class="text-center">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $no =1; ?>
        @foreach($datas AS $data)
        <tr>
            <td data-sort="{{ strtotime($data->updated_at) }}" class="text-center">
                <img src="{{url('')}}/{{ $data->img_thumbnail }}" class="img img-thumbnail" style="max-width:100px;">
            </td>
            <td>{{ $data->porto_title }}</td>
            <td align="center">
                <a href="{{route('portofolio.edit',$data->id_portofolio)}}" class="btn-primary btn btn-sm">
                    <i class="fa fa-pencil"></i>
                </a>
                <a href="{{route('portofolio.destroy',$data->id_portofolio)}}" class="btn-danger btn btn-sm">
                    <i class="fa fa-trash"></i>
                </a>
            </td>
        </tr>
        <?php $no++; ?>
        @endforeach
    </tbody>
</table>

@section('scripts')
@include('layouts.datatables_js')
<script>
    $(document).ready(function () {
        var dataTable = $('#table').DataTable({
            @if(auth()->user()->roles->first()->name == 'admin' || auth()->user()->roles->first()->name == 'legal' || auth()->user()->roles->first()->name == 'anr')
            dom             : 'Bfrtlip',
            @endif
            order           : [[ 0, "desc"]],
            processing      : true,
            serverMethod    : 'post',
            responsive      : true,
            autoWidth       : false,
            aLengthMenu     : [[10, 50, 100, 250, -1], [10, 50, 100, 250, 'All']],
            buttons         : [{ 
                                    "extend" : 'create', 
                                    "text" : '<i class="fa fa-plus"></i> Add New',
                                    "className" : 'btn-primary',
                                    "action" : function( e, dt, button, config){ 
                                        window.location = "portofolio/create";
                                    }
                                }],
        });
        $(".alert-danger").fadeTo(3000, 1000).slideUp(1000, function(){
            $(".alert-danger").slideUp(1000);
        });
        $(".alert-success").fadeTo(3000, 1000).slideUp(1000, function(){
            $(".alert-success").slideUp(1000);
        });
        //lengthmenu -> add a margin to the right and reset clear 
        $(".dataTables_length").css('clear', 'none');
        $(".dataTables_length").css('margin-right', '20px');

        //info -> reset clear and padding
        $(".dataTables_info").css('clear', 'none');
        $(".dataTables_info").css('padding', '0');
        $("div").removeClass("ui-toolbar");

        $('#singer').on('change', function(){
            $('#album').val('');
            dataTable.search(this.value).draw(); 
        });
        $('#album').on('change', function(){
            $('#singer').val('');
            dataTable.search(this.value).draw();   
        });
        $('#status').on('change', function(){
            $('#singer').val('');
            $('#album').val('');
            dataTable.search(this.value).draw();   
        });
        $('.form-inline').find('.clear-filtering').on('click', function(e) {
            $('#singer').val('');
            $('#album').val('');
            $('#status').val('');
            dataTable.search(this.value).draw();
            e.preventDefault();
        });
    });
</script>
@endsection