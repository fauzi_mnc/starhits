@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Portofolio</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Portofolio</a>
            </li>
            <li class="active">
                <strong>Table</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@include('flash::message')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Portofolio</h5>
                </div>
                <div class="ibox-content">
                    @include('portofolio.table')
                </div>
            </div>
        </div>
    </div>
@endsection
