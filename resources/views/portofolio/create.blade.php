@extends('layouts.crud')
@section('css')
<link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />

<style>
    .kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
        margin: 0;
        padding: 0;
        border: none;
        box-shadow: none;
        text-align: center;
    }
    .kv-photo .file-input {
        display: table-cell;
        max-width: 220px;
    }
    .kv-reqd {
        color: red;
        font-family: monospace;
        font-weight: normal;
    }
    .wizard > .content > .body{
        position: relative !important;
    }
    strong{
        padding-right: 30px;
    }
</style>


@endsection

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Portofolio</h2>
        <ol class="breadcrumb">
            <li>
                <a href="#">Portofolio</a>
            </li>
            <li class="active">
                <strong>Create</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')

        {!! Form::open(['route' => 'portofolio.store', 'class' => 'form-horizontal', 'id' => 'creatorStore', 'files' => true]) !!}
        
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Create Portofolio</h5>
                </div>
                <div class="ibox-content">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            Portofolio Information
                        </div>
                        <div class="panel-body">
                        
                        <!-- Name Field -->
                        <div class="form-group">
                            {!! Form::label('porto_title', 'Title Porto:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-10">
                                {!! Form::text('porto_title', null, ['class' => 'form-control', 'maxlength' => 60, 'required' => true]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('id_user', 'Creator:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-10">
                                <select name="id_user[]" title="Select Creators" class="form-control" required="" multiple data-live-search="true">
                                    @foreach( $creator as $creator)
                                    <option value="{{$creator->id}}">{{$creator->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('porto_type', 'Type :', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-10">
                                <select name="porto_type" title="Select Type" class="form-control" required="">
                                    <option value="digital">Digital Campaign</option>
                                    <option value="production">Production Service</option>
                                </select>
                            </div>
                        </div>

                        <!-- Picture Field -->
                        <div class="form-group">
                            {!! Form::label('porto_header', 'Image Header:', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-2" style="margin-top: 5px">
                               <small> IMG Size : 1125 x 337</small>
                            </div>
                            <div class="col-sm-10">
                                <div class="kv-photo center-block text-center">
                                    <input id="porto_header" name="porto_header" type="file" class="file-loading"  accept="image/x-png">
                                    <!-- <small id="" class="form-text text-muted font-italic">Input Video Id etc: https://www.youtube.com/watch?v=<code>FcOctsNXyjk</code></small> -->
                                </div>

                                <div id="kv-photo-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
                            </div>
                            
                        </div>

                        <!-- Picture Field -->
                        <div class="form-group">
                            {!! Form::label('img_thumbnail', 'Image Thumbnail :', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-2" style="margin-top: 5px">
                               <small> IMG Size : 600 x 600</small>
                            </div>
                            <div class="col-sm-10">
                                <div class="kv-photo center-block text-center">
                                    <input id="img_thumbnail" name="img_thumbnail" type="file" class="file-loading"  accept="image/x-png">
                                </div>

                                <div id="kv-photo-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
                            </div>
                        </div>
                       
                        <!-- Description 1 Field -->
                        <div class="form-group">
                            {!! Form::label('title_desc1', 'Title Description 1:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-10">
                                {!! Form::text('porto_content', null, ['class' => 'form-control', 'id' => 'title_desc1']) !!}
                            </div>
                        </div>
                        <div class="form-group">                        
                            {!! Form::label('content', 'Description 1 :', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-9">
                                {!! Form::textarea('content1', null, ['id' => 'description', 'class' => 'form-control']) !!}
                            </div>
                        </div>


                        <!-- Description 2 Field -->
                        <div class="form-group">
                            {!! Form::label('title_desc2', 'Title Description 2:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-10">
                                {!! Form::text('campaign_title', null, ['class' => 'form-control', 'id' => 'title_desc2']) !!}
                            </div>
                        </div>
                        <div class="form-group">                        
                            {!! Form::label('content1', 'Description 2 :', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-9">
                                {!! Form::textarea('campaign_content', null, ['id' => 'description2', 'class' => 'form-control']) !!}
                            </div>
                        </div>

                        <!-- Description 2 Field -->>
                        <div class="form-group">                        
                            {!! Form::label('isi_content', 'Description 3 :', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-9">
                                {!! Form::textarea('isi_content', null, ['id' => 'isi_content', 'class' => 'form-control']) !!}
                            </div>
                        </div>

                        <!-- Photo1 Field -->
                        <div class="form-group">
                            {!! Form::label('cover', 'Photo 1:', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-2" style="margin-top: 5px">
                               <small> IMG Size : 310 x 464</small>
                            </div>
                            <div class="col-sm-10">
                                <div class="kv-photo center-block text-center">
                                    <input id="cover" name="porto_img1" type="file" class="file-loading">
                                </div>

                                <div id="kv-cover-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
                            </div>
                        </div>
                        <!-- Cover Field -->
                        <div class="form-group">
                            {!! Form::label('cover2', 'Photo 2:', ['class' => 'col-sm-2 control-label']) !!}
                            <div class="col-sm-2" style="margin-top: 5px">
                               <small> IMG Size : 436 x 546</small>
                            </div>
                            <div class="col-sm-10">
                                <div class="kv-photo center-block text-center">
                                    <input id="cover2" name="porto_img2" type="file" class="file-loading">
                                </div>

                                <div id="kv-cover-errors-2" class="center-block alert alert-block alert-danger" style="display:none"></div>
                            </div>
                        </div>

                        <!-- Video Field -->
                        <div class="form-group">
                            {!! Form::label('channel', 'Video Youtube:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-10">
                                <input type="text" name="media_content" id="media_content" class="multinput form-control"  required="">
                                <small id="media_content" class="form-text text-muted font-italic">Input Video Id etc: https://www.youtube.com/watch?v=<code>FcOctsNXyjk</code></small>
                            </div>
                        </div>
                        <!-- Views Field -->
                        <div class="form-group">
                            {!! Form::label('porto_views', 'Views:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-2">
                                {!! Form::number('porto_views', null, ['class' => 'form-control', 'id' => 'porto_views', 'aria-describedby' => 'porto_views']) !!}
                                <small id="porto_views" class="form-text text-muted font-italic">Only input numbers</small>
                            </div>
                        </div>
                        <!-- Engagement Field -->
                        <div class="form-group">
                            {!! Form::label('porto_engagement', 'Engagement:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-2">
                                {!! Form::number('porto_engagement', null, ['class' => 'form-control', 'id' => 'porto_engagement', 'aria-describedby' => 'porto_engagement']) !!}
                                <small id="porto_engagement" class="form-text text-muted font-italic">Only input numbers</small>
                            </div>
                        </div>
                        <!-- Performance Field -->
                        <div class="form-group">
                            {!! Form::label('porto_performance', 'Performance:', ['class' => 'col-sm-2 control-label']) !!}

                            <div class="col-sm-2">
                                {!! Form::number('porto_performance', null, ['class' => 'form-control', 'id' => 'porto_performance', 'aria-describedby' => 'porto_performance']) !!}
                                <small id="porto_performance" class="form-text text-muted font-italic">Only input numbers</small>
                            </div>
                        </div>
                    </div>
                </div>

                    
                     <!-- Submit Field -->
                    @if(auth()->user()->roles->first()->name === 'finance')
                    <div class="pull-right">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary', 'id'=>'btn-submits', 'form'=>'creatorStore']) !!}
                        <a href="{{route('finance.creator.index')}}" class="btn btn-default">Cancel</a>
                    </div>
                    @else
                    <div class="pull-right">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary', 'id'=>'btn-submits', 'form'=>'creatorStore']) !!}
                        <a href="{{route('creator.index')}}" class="btn btn-default">Cancel</a>
                    </div>       
                    @endif

                    {!! Form::close() !!}
                </div>
            </div>
@endsection
@section('scripts')
<script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/chosen.jquery.min.js') }}"></script>
<script src="{{ asset('js/plugins/typeahed.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/tinymce.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

<script>
$('select').selectpicker();

$("#cover").fileinput({
    overwriteInitial: true,
    maxFileSize: 1500,
    showClose: false,
    showCaption: false,
    browseLabel: '',
    removeLabel: '',
    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-cover-errors-1',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img src="{{ asset('img/default_avatar_male.jpg') }}" alt="Your cover" style="width:160px">',
    layoutTemplates: {main2: '{preview} {remove} {browse}'},
    allowedFileExtensions: ["png"]
});
$("#cover2").fileinput({
    overwriteInitial: true,
    maxFileSize: 1500,
    showClose: false,
    showCaption: false,
    browseLabel: '',
    removeLabel: '',
    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-cover-errors-2',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img src="{{ asset('img/default_avatar_male.jpg') }}" alt="Your cover" style="width:160px">',
    layoutTemplates: {main2: '{preview} {remove} {browse}'},
    allowedFileExtensions: ["png"]
});

$("#porto_header").fileinput({
    overwriteInitial: true,
    maxFileSize: 1500,
    showClose: false,
    showCaption: false,
    browseLabel: '',
    removeLabel: '',
    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-photo-errors-1',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img src="{{ asset('img/default_avatar_male.jpg') }}" alt="Your photo" style="width:160px">',
    layoutTemplates: {main2: '{preview} {remove} {browse}'},
    allowedFileExtensions: ["png"]
});

$("#img_thumbnail").fileinput({
    overwriteInitial: true,
    maxFileSize: 1500,
    showClose: false,
    showCaption: false,
    browseLabel: '',
    removeLabel: '',
    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-photo-errors-1',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img src="{{ asset('img/default_avatar_male.jpg') }}" alt="Your photo" style="width:160px">',
    layoutTemplates: {main2: '{preview} {remove} {browse}'},
    allowedFileExtensions: ["png"]
});


$(document).ready(function(){
    initTinyMce();
});

// init tiny mce
function initTinyMce(){
    tinymce.init({
        selector: "textarea",
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        },
        entity_encoding : "raw",
        element_format : 'html',
        allow_html_in_named_anchor: true,
        forced_root_block : 'p',
        file_picker_types: 'image',
        schema: 'html5',
        images_upload_credentials: true,
        automatic_uploads: false,
        theme: "modern",
        paste_data_images: true,
        plugins: [
        "advlist autolink lists link image media  charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime nonbreaking save table contextmenu directionality",
        "template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor | media image",
        image_advtab: true,
        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
            
            // Note: In modern browsers input[type="file"] is functional without 
            // even adding it to the DOM, but that might not be the case in some older
            // or quirky browsers like IE, so you might want to add it to the DOM
            // just in case, and visually hide it. And do not forget do remove it
            // once you do not need it anymore.
        
            input.onchange = function() {
                var file = this.files[0];
            
                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function () {
                // Note: Now we need to register the blob in TinyMCEs image blob
                // registry. In the next release this part hopefully won't be
                // necessary, as we are looking to handle it internally.
                var id = 'blobid' + (new Date()).getTime();
                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                var base64 = reader.result.split(',')[1];
                var blobInfo = blobCache.create(id, file, base64);
                blobCache.add(blobInfo);
        
                // call the callback and populate the Title field with the file name
                cb(blobInfo.blobUri(), { title: file.name });
                };
            };
            
            input.click();
        }
    });
}
// end tiny mce

// // youtube url parser to get id
// function youtube_parser(url){
//     var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
//     var match = url.match(regExp);
//     return (match&&match[7].length==11)? match[7] : false;
// }
// // end youtube url parser to get id

// /*$('#btn-check').click(function(){*/
// function getYoutube(){
//     var id_yt =$("#id-youtube").val();
//     var getVids = youtube_parser(id_yt);
//     checkUrlYoutube(getVids);
// }
    
// //});
//  // check id youtube
// function checkUrlYoutube(id_youtube) {
//     console.log(id_youtube);
//     $.ajax({
//         @if(auth()->user()->roles->first()->name === 'admin')
//             url: "{{route('admin.posts.check')}}",
//         @elseif(auth()->user()->roles->first()->name === 'user')
//             url: "{{route('user.posts.check')}}",
//         @else
//             url: "{{route('creator.posts.check')}}",
//         @endif
//         method: "GET", 
//         data: {
//         id: id_youtube
//     },
//     success: function(response){
//         console.log(response);
//         if(response.status == 'success'){
//             var data = response.result;
//             var tags = data.snippet.tags;
//             //console.log(data.snippet.description);

//             // set data if true
//             $('#id-youtube').val(data.id);
//             $('#title-preview').val(data.snippet.title);
//             $('.prev-video').removeClass('hide');
//             $('#embed-video').empty();
//             $('#embed-video').append(data.player.embedHtml);

//             if(formType != 'edit'){
//                 tinymce.activeEditor.setContent(data.snippet.description);
//                 $.each(tags, function(index, value){
//                     $('#tags-input').tagsinput('add', value);
//                 });
//             }
//             toastr.success(response.message);
//         }
//         else
//         {
//             toastr.error(response.message);
//         }
        
//         },
//         error: function(response){
//         toastr.error(response);
//     }
//     });
// }
// end check id youtube
$('.multinput').tagsinput({
    tagClass: 'label label-primary',
    maxTags: 5,
});
</script>
@endsection