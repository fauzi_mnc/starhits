@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Posts</h2>
        <ol class="breadcrumb">
            <li>
                @if(auth()->user()->roles->first()->name === 'admin')
                    <a href="{{route('admin.posts.index')}}">Posts</a>
                @elseif(auth()->user()->roles->first()->name === 'user')
                <a href="{{route('user.posts.index')}}">Posts</a>
                @else
                    <a href="{{route('creator.posts.index')}}">Posts</a>
                @endif
            </li>
            <li class="active">
                <strong>Edit Manual</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Edit Post Manual</h5>
        </div>
        <div class="ibox-content">
            @if(auth()->user()->roles->first()->name === 'admin')
                {!! Form::model($posts, ['route' => ['admin.posts.update', $posts->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) !!}
            @elseif(auth()->user()->roles->first()->name === 'user')
            {!! Form::model($posts, ['route' => ['user.posts.update', $posts->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) !!}
            @else
                {!! Form::model($posts, ['route' => ['creator.posts.update', $posts->id], 'method' => 'put', 'class' => 'form-horizontal', 'files' => true]) !!}
            @endif
            @include('posts.fields', ['formType' => 'edit', 'postType' => 'manual'])
            {!! Form::close() !!}
        </div>
    </div>
@endsection

