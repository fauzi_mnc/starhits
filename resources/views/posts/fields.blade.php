
@section('css')
<link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/plugins/bootstrap-tagsinput.css') }}" rel="stylesheet">
<link href="{{ asset('css/chosen.min.css') }}" rel="stylesheet">
<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
.btn-preview {
    margin-right: 15px;
}
</style>
@include('layouts.datatables_css')
@endsection 

@if($postType == 'automatic')
    <!-- Creator Field -->
    @if(auth()->user()->roles->first()->name === 'admin')
        <div class="form-group">
            {!! Form::label('creator', 'Creator*', ['class' => 'col-sm-3 control-label']) !!}

            <div class="col-sm-9">
                {!! Form::select('created_by', $user, isset($posts->user_id) ? $posts->user_id : null, ['class' => 'form-control', 'id' => 'creator', 'required' => true, 'placeholder' => 'Select Creator']) !!}
            </div>
        </div>
    @elseif(auth()->user()->roles->first()->name === 'user')
        {{ Form::hidden('created_by', auth()->user()->id) }}
    @endif

    <!-- Series field -->
    <div class="form-group">
        {!! Form::label('series', 'Series*', ['class' => 'col-sm-3 control-label']) !!}

        <div class="col-sm-9">
            {!! Form::select('parent_id', $series, null, ['id' => 'series', 'class' => 'form-control', 'required' => 'required']) !!}
        </div>
    </div>

    <!-- Link id video Field -->
    <div class="form-group">
        {!! Form::label('id_youtube', 'ID/URL Youtube*', ['class' => 'col-sm-3 control-label']) !!}

        <div class="col-sm-9">
            {!! Form::text('attr_6', null, ['id' => 'id-youtube', 'class' => 'form-control', 'required' => 'required', 'maxlength' => 60]) !!}
        </div>
    </div>
    <div class="form-group">
        <button type="button" class="btn btn-w-m btn-primary btn-preview pull-right">Preview</button>
    </div>

    <!-- Link embed video Field -->
    <div class="form-group prev-video hide">
        {!! Form::label('title_video', 'Video Preview', ['class' => 'col-sm-3 control-label']) !!}

        <div class="col-sm-9">
            {!! Form::text('title', null, ['id' => 'title-preview', 'class' => 'form-control', 'readonly' => 'true']) !!}
        </div>
    </div>

    <div class="form-group prev-video hide">
        <div class=col-sm-3></div>
        <div id="embed-video" class="col-sm-9">
        </div>
    </div>
@else
    <!-- Channel field -->
    <div class="form-group">
        {!! Form::label('channel', 'Channel*', ['class' => 'col-sm-3 control-label']) !!}

        <div class="col-sm-9">
            {!! Form::select('attr_1', $channel, null, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    </div>

    <!-- Link id video Field -->
    <div class="form-group">
        {!! Form::label('id_youtube', 'ID/URL Youtube (opsional)', ['class' => 'col-sm-3 control-label', 'maxlength' => 60]) !!}

        <div class="col-sm-9">
            {!! Form::text('attr_6', null, ['id' => 'id-youtube', 'class' => 'form-control']) !!}
        </div>
    </div>

    <!-- Title Field -->
    <div class="form-group prev-video">
        {!! Form::label('title_video', 'Title*', ['class' => 'col-sm-3 control-label']) !!}

        <div class="col-sm-9">
            {!! Form::text('title', null, ['id' => 'title-preview', 'class' => 'form-control', 'required' => 'required', 'maxlength' => 125]) !!}
        </div>
    </div>
@endif

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('content', 'Description', ['class' => 'col-sm-3 control-label']) !!}

    <div class="col-sm-9">
        {!! Form::textarea('content', null, ['id' => 'description', 'class' => 'form-control']) !!}
    </div>
</div>

@if($postType == 'automatic')
    @if(auth()->user()->roles->first()->name === 'admin')
    <!-- Relate with Field -->
    <div class="form-group">
        {!! Form::label('relate_with', 'Relate with', ['class' => 'col-sm-3 control-label']) !!}

        <div class="col-sm-9">
            {!! Form::select('user_id[]', $user, $selected_user, ['class' => 'form-control chosen-select', 'multiple']) !!}
        </div>
    </div>
    @endif
@endif
    
<!-- Tags Field -->
<div class="form-group">
    {!! Form::label('tag', 'Tag (maks 5)', ['class' => 'col-sm-3 control-label']) !!}

    <div class="col-sm-9">
    {!! Form::text('tags', null, ['id' => 'tags-input', 'class' => 'form-control']) !!}
    </div>
    {!! Form::text('is_active', null, ['id' => 'type', 'class' => 'hide form-control']) !!}
    {!! Form::text('postType', $postType, ['id' => 'type', 'class' => 'hide form-control']) !!}
    {!! Form::text('create', null, ['id' => 'create', 'class' => 'hide form-control']) !!}
</div>

<!-- Picture Field -->
<div class="form-group">
    {!! Form::label('image', 'Thumbnail (Custom)', ['class' => 'col-sm-3 control-label']) !!}

    <div class="col-sm-9">
        <div class="kv-photo center-block text-center">
            <input id="photo" name="image" type="file" class="file-loading">
        </div>

        <div id="kv-photo-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
    </div>
</div>

<!-- Picture Field -->
<div class="form-group hide">
    {!! Form::label('is-featured', 'Is Recommend', ['class' => 'col-sm-3 control-label']) !!}

    <div class="col-sm-9">
        <div class="switch">
            <div class="onoffswitch">
                <input type="checkbox" class="onoffswitch-checkbox recommend" name="is_featured" id="is-featured">
                <label class="onoffswitch-label" for="is-featured">
                    <span class="onoffswitch-inner"></span>
                    <span class="onoffswitch-switch"></span>
                </label>
            </div>
        </div>
    </div>
</div>

<!-- Submit Field -->
<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
    @if(auth()->user()->roles->first()->name === 'admin')
    <a href="{!! route('admin.posts.index') !!}" class="btn btn-default">Cancel</a>
    @elseif(auth()->user()->roles->first()->name === 'user')
    <a href="{!! route('user.posts.index') !!}" class="btn btn-default">Cancel</a>
    @else
    <a href="{!! route('creator.posts.index') !!}" class="btn btn-default">Cancel</a>
    @endif
    {!! Form::submit('Save as draft', ['class' => 'btn btn-warning btn-draft']) !!}
    {!! Form::submit('Save and Publish', ['class' => 'btn btn-primary btn-publish']) !!}
    {!! Form::submit('Save and Create New', ['class' => 'btn btn-success btn-create']) !!}
    </div>
</div>

@section('scripts')
<script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-tagsinput.min.js') }}"></script>
<script src="{{ asset('js/plugins/chosen.jquery.min.js') }}"></script>
<script src="{{ asset('js/plugins/typeahed.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/jquery.tinymce.min.js') }}"></script>
<script src="{{ asset('js/plugins/tinymce/tinymce.min.js') }}"></script>
<script>

    var formType = '{{ $formType }}';
    var postType = '{{ $postType}}';
    
    $(document).ready(function(){
        initSourceTags();
        initTinyMce();
        initFileInput();
        initBindings();

        // get data from youtube if edit automatic
        if(formType == 'edit' && postType == 'automatic'){
            checkUrlYoutube($('#id-youtube').val());
        }
    });

    // youtube url parser to get id
    function youtube_parser(url){
        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        var match = url.match(regExp);
        return (match&&match[7].length==11)? match[7] : false;
    }
    // end youtube url parser to get id

    // call function youtube parser with condition
    function callParser(){
        var url = $('#id-youtube').val();
        var substring1 = "youtube";
        var substring2 = "youtu.be";
        if(url.includes(substring1) == true || url.includes(substring2) == true){
            var id = youtube_parser(url);           
            if(postType == 'automatic'){
                checkUrlYoutube(id);
            }else{
                $('#id-youtube').val(id); 
            }
        }else{
            if(postType == 'automatic'){
                checkUrlYoutube(url);
            }
        }
    }
    // end call function youtube parser with condition

    // check id youtube
    function checkUrlYoutube(id_youtube) {
        $.ajax({
        @if(auth()->user()->roles->first()->name === 'admin')
            url: "{{route('admin.posts.check')}}",
        @elseif(auth()->user()->roles->first()->name === 'user')
            url: "{{route('user.posts.check')}}",
        @else
            url: "{{route('creator.posts.check')}}",
        @endif
            method: "GET", 
            data: {
            id: id_youtube
        },
        success: function(response){
            console.log(response);
            if(response.status == 'success'){
                var data = response.result;
                var tags = data.snippet.tags;
                //console.log(data);

                // set data if true
                $('#id-youtube').val(data.id);
                $('#title-preview').val(data.snippet.title);
                $('.prev-video').removeClass('hide');
                $('#embed-video').empty();
                $('#embed-video').append(data.player.embedHtml);
                //console.log($('#description').html(data.snippet.description);
                if(formType != 'edit'){
                    tinymce.activeEditor.setContent(data.snippet.description);
                    $.each(tags, function(index, value){
                        $('#tags-input').tagsinput('add', value);
                    });
                }
                toastr.success(response.message);
            }
            else
            {
                toastr.error(response.message);
            }
            
            },
            error: function(response){
            toastr.error(response);
        }
        });
    }
    // end check id youtube

    // get series by creator
    function getSeries(_id_user){
        $.ajax({
            @if(auth()->user()->roles->first()->name === 'admin')
            url: "{{ route('admin.posts.getSeries') }}",
            @elseif(auth()->user()->roles->first()->name === 'user')
            url: "{{ route('user.posts.getSeries') }}",
            @else
            url: "{{ route('creator.posts.getSeries') }}",
            @endif
            method: "POST",
            data: {
                id_user: _id_user
            },
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            success: function(response){
                var counter = 0;
                var $el = $("#series");
                $el.empty(); // remove old options
                $.each(response, function(key,value) {
                    $el.append($("<option></option>")
                        .attr("value", key).text(value));
                    counter += 1;
                });
                
                if(counter > 0){
                    toastr.success("Mengambil Series Berhasil");
                }else{
                    toastr.error("Series tidak ada silahkan memilih creator yang lain.");
                }
            },
            error: function(response){
                console.log(response);
            }
        });
    }
    // end get series by creator

    // init source tags
    function initSourceTags(){
        var source_ = new Array();
        @foreach ($terms as $value)
            source_.push('{{$value}}');
        @endforeach

        $('#tags-input').tagsinput({
            tagClass: 'label label-primary',
            maxTags: 5,
            typeahead: {
                afterSelect: function(val) { this.$element.val(""); },
                source: source_
            }
        });
    }
    // end init source tags

    // init tiny mce
    function initTinyMce(){
        tinymce.init({
            selector: "textarea",
            setup: function (editor) {
                editor.on('change', function () {
                    tinymce.triggerSave();
                });
            },
            entity_encoding : "raw",
            element_format : 'html',
            allow_html_in_named_anchor: true,
            forced_root_block : 'p',
            file_picker_types: 'image',
            schema: 'html5',
            images_upload_credentials: true,
            automatic_uploads: false,
            theme: "modern",
            paste_data_images: true,
            plugins: [
            "advlist autolink lists link image media  charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime nonbreaking save table contextmenu directionality",
            "template paste textcolor colorpicker textpattern"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | forecolor | media image",
            image_advtab: true,
            file_picker_callback: function(cb, value, meta) {
                var input = document.createElement('input');
                input.setAttribute('type', 'file');
                input.setAttribute('accept', 'image/*');
                
                // Note: In modern browsers input[type="file"] is functional without 
                // even adding it to the DOM, but that might not be the case in some older
                // or quirky browsers like IE, so you might want to add it to the DOM
                // just in case, and visually hide it. And do not forget do remove it
                // once you do not need it anymore.
            
                input.onchange = function() {
                    var file = this.files[0];
                
                    var reader = new FileReader();
                    reader.readAsDataURL(file);
                    reader.onload = function () {
                    // Note: Now we need to register the blob in TinyMCEs image blob
                    // registry. In the next release this part hopefully won't be
                    // necessary, as we are looking to handle it internally.
                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);
            
                    // call the callback and populate the Title field with the file name
                    cb(blobInfo.blobUri(), { title: file.name });
                    };
                };
                
                input.click();
            }
        });
    }
    // end tiny mce

    // init file input
    function initFileInput(){
        $('#photo').val('');

        $("#photo").fileinput({
            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: '',
            removeLabel: '',
            browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
            removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-photo-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            defaultPreviewContent: '<img src="{{ ($formType == 'edit' && $posts->image) ? $posts->image : asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
            layoutTemplates: {main2: '{preview} {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "jpeg"]
        });
    }
    // end init file input

    // init event bindings
    function initBindings(){
        $('.chosen-select').chosen({width: "100%"});

        $('.btn-publish').click(function() {
            if(postType != 'automatic'){
                callParser();
            }
            $('#type').val(1);
            $('#create').val(0);
        });

        $('.btn-draft').click(function(e) {
            if(postType != 'automatic'){
                callParser();
            }
            $('#type').val(0);
            $('#create').val(0);
        });

        $('.btn-create').click(function(e) {
            if(postType != 'automatic'){
                callParser();
            }
            $('#type').val(1);
            $('#create').val(1);
        });

        // get preview from youtube
        $(".btn-preview").click(function(){
            callParser();
        });

        $('#creator').change(function() {
            getSeries($('#creator').val());
        });
    }
    // end init event bindings
    
</script>
@if($formType == 'edit')
@include('layouts.datatables_js')
<script>
    $.fn.dataTable.ext.buttons.create = {
    action: function (e, dt, button, config) {
            $('#myModal').modal('show');
        }
    };

    var hash = document.location.hash;
    if (hash) {
        console.log(hash);
        $('.nav-tabs a[href="'+hash+'"]').tab('show')
    }

    // Change hash for page-reload
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
    window.location.hash = e.target.hash;
    });
</script>
@endif
@endsection