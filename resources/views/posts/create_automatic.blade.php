@extends('layouts.crud')

@section('breadcrumb')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Posts</h2>
        <ol class="breadcrumb">
            <li>
                @if(auth()->user()->roles->first()->name === 'admin')
                    <a href="{{route('admin.posts.index')}}">Posts</a>
                @elseif(auth()->user()->roles->first()->name === 'user')
                    <a href="{{route('user.posts.index')}}">Posts</a>
                @else
                    <a href="{{route('creator.posts.index')}}">Posts</a>
                @endif
            </li>
            <li class="active">
                <strong>Create Automatic</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection
@section('contentCrud')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@include('flash::message')
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h5>Create Post Automatic</h5>
        </div>
        <div class="ibox-content">
        @if(auth()->user()->roles->first()->name === 'admin')
            {!! Form::open(['route' => 'admin.posts.store', 'class' => 'form-horizontal', 'files' => true]) !!}
        @elseif(auth()->user()->roles->first()->name === 'user')
            {!! Form::open(['route' => 'user.posts.store', 'class' => 'form-horizontal', 'files' => true]) !!}
        @else
            {!! Form::open(['route' => 'creator.posts.store', 'class' => 'form-horizontal', 'files' => true]) !!}
        @endif
            @include('posts.fields', ['formType' => 'create', 'postType' => 'automatic'])

            {!! Form::close() !!}
        </div>
    </div>
@endsection
