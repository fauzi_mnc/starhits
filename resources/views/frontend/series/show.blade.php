@extends('layouts.frontend')

@section('css')
    @include('frontend.includes.extensions.amp-detail-series')
@endsection

@section('content')
<!-- BEGIN MAIN CONTENT -->
<div class="col-xs-12 col-sm-12 col-content">
    <div class="row">
        <div class="col-md-9 mx-auto">
            <div>
                <amp-youtube id="myLiveChannel" data-videoid="{{ $defaultvideo[0]['attr_6'] }}" width="358" height="204" layout="responsive">
                    <amp-img src="https://i.ytimg.com/vi/{{ $defaultvideo[0]['attr_6'] }}/maxresdefault_live.jpg" placeholder layout="fill"/>
                </amp-youtube>
            </div>

            <div class="d-flex justify-content-center" style="padding: 2em">
                <div class="d-flex align-items-center">
                    <div class="icon-container-18">
                        <amp-img src="{{asset('frontend/assets/icon/icon-eye.png')}}" layout="responsive" width="1" height="1"></amp-img>
                    </div>
                <div class="pl-2 pr-5">{{ bd_nice_number((int)$defaultvideo[0]['viewed']) }}</div>
                </div>
                <div class="d-flex align-items-center">
                    <div class="icon-container-18">
                        <amp-img src="{{asset('frontend/assets/icon/icon-comments.png')}}" layout="responsive" width="1" height="1">
                        </amp-img>
                    </div>
                    <div class="pl-2 pr-5">{{ bd_nice_number((int)$defaultvideo[0]['attr_4']) }}</div>
                </div>
                <div class="d-flex align-items-center">
                    <div class="icon-container-18">
                        <amp-img src="{{asset('frontend/assets/icon/icon-like.png')}}" layout="responsive" width="1" height="1"></amp-img>
                    </div>
                    <div class="pl-2 pr-5">{{ bd_nice_number((int)$defaultvideo[0]['attr_2']) }}</div>
                </div>
                <div class="d-flex align-items-center">
                    <div class="icon-container-18">
                        <amp-img src="{{asset('frontend/assets/icon/icon-dislike.png')}}" layout="responsive" width="1" height="1">
                        </amp-img>
                    </div>
                    <div class="pl-2">{{ bd_nice_number((int)$defaultvideo[0]['attr_3']) }}</div>
                </div>
            </div>

            {{-- <div style="padding: 2em 0">
                <amp-selector id="videoPlaylist"
                    class="carousel__preview"
                    on="select:videoPlayer.goToSlide(index=event.targetOption)"
                    layout="container">
                    <amp-layout layout="fixed" width="120" height="80" class="carousel__preview__item">
                        <amp-img option="0"
                        selected
                        src="frontend/assets/images/Layer-578-copy-4.png"
                        width="120"
                        height="80"
                        alt="a sample image"></amp-img>
                        <div class="carousel__preview__title">Episode 2</div>
                    </amp-layout>
                    
                    <amp-layout layout="fixed" width="120" height="80" class="carousel__preview__item">
                        <amp-img option="1"
                        src="frontend/assets/images/Layer-578-copy-4.png"
                        width="120"
                        height="80"
                        alt="a sample image"></amp-img>
                        <div class="carousel__preview__title">Episode 3</div>
                    </amp-layout>
                    
                    <amp-layout layout="fixed" width="120" height="80" class="carousel__preview__item">
                        <amp-img option="2"
                        src="frontend/assets/images/Layer-578-copy-4.png"
                        width="120"
                        height="80"
                        alt="a sample image"></amp-img>
                        <div class="carousel__preview__title">Episode 4</div>
                    </amp-layout>
                  </amp-selector>
            </div> --}}

            <div class="py-2">
                <div class="row">
                    {{-- <div class="col-xl-3">
                        <amp-img layout="responsive" width="258" height="403" src="../frontend/assets/images/Layer-989.png"></amp-img>
                    </div> --}}
                    <div class="col-xl-12">
                        <h1>{{ $defaultvideo[0]['title'] }}</h1>
                        <div>
                            <p class="m-0">{{ $series->title }}</p>
                            <p class="m-0">{{ $series->created_at->format('d, F Y') }}</p>
                            <p class="m-0">By: {{ $series->users->name }}</p>
                        </div>
                        <div style="line-height:2;padding:30px 0;">
                            {{ strip_tags($defaultvideo[0]['content']) }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="d-flex justify-content-xl-end align-items-center">
                <div class="mr-3"><strong>share on: </strong></div>
                <div>
                    <a href="https://www.facebook.com/sharer/sharer.php?u={{ url('/videos/'.$video->slug) }}" target="_blank">
                        <div class="icon-container-32">
                            <amp-img src="{{asset('frontend/assets/icon/iconfinder_facebook_circle_black_107153.png')}}" layout="responsive"
                                width="1" height="1"></amp-img>
                        </div>
                    </a>
                </div>
                <div>
                    <a href="https://twitter.com/intent/tweet?url={{ url('/videos/'.$video->slug) }}">
                        <div class="icon-container-32">
                            <amp-img src="{{asset('frontend/assets/icon/iconfinder_43-twitter_104461.png')}}" layout="responsive" width="1"
                                height="1"></amp-img>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div id="orange-box-flip">
        <div class="row">
            @if(empty(json_decode($moreCreator)))
            <div class="col-sm-10 mx-auto d-block my-5">
                <h1 class="font-weight-900">MORE FROM CREATOR</h1>
                <br>
                    <div class="font-size-sm text-center">
                        There is no data to display!
                    </div>
                <br>
            </div>
            @else
            <div class="col-md-11 mx-auto">
                <h1 class="font-weight-900">MORE FROM CREATOR</h1>
                <amp-carousel id="latestVideoSeriesDetail" height="300" layout="fixed-height">
                    @foreach($moreCreator as $creator)
                    <div class="slide px-2">
                        <div class="card" style="position: relative">
                            <amp-img src="https://i3.ytimg.com/vi/{{ $creator->attr_6 }}/mqdefault.jpg" width="345" height="194" class="more-from-creator"></amp-img>
                            <a href="{{ url('/videos').'/'.$creator->slug }}" class="card-img-overlay"></a>
                        </div>
                        
                        <div class="caption" style="padding:20px 0;">
                            <amp-fit-text layout="fixed-height" height="30" min-font-size="16" max-font-size="16" class="mb-5">{{ $creator->title }}</amp-fit-text>
                        </div>
                    </div>
                    @endforeach
                </amp-carousel>                            
            </div>
            @endif
        </div>
    </div>
    <div class='row'>
        @if(empty(json_decode($recomendedSeries)))
        <div class="col-sm-10 mx-auto d-block my-5">
            <h1 class="font-weight-900">RECOMENDED SERIES</h1>
            <br>
                <div class="font-size-sm text-center">
                    There is no data to display!
                </div>
            <br>
        </div>
        @else
        <div class="col-md-11 mx-auto" style="margin-bottom: 5rem">
            <h1 class="font-weight-900">RECOMENDED SERIES</h1>
            <amp-carousel id="latestSeriesDetailSeries" height="500" layout="fixed-height">
                @foreach ($recomendedSeries as $recomended)
                <div class="slide">
                    <div class="card" style="position: relative">
                        <amp-img src="{{ $recomended->image }}" width="258" height="408"></amp-img>
                        <a href="{{ $recomended->slug }}" class="card-img-overlay"></a>
                    </div>
                    
                    <div class="caption" style="padding:15px 0;">
                        <amp-fit-text layout="fixed-height" height="50" max-font-size="20">{{ $recomended->title }}</amp-fit-text>
                        <div class="small">{{ $recomended->totalVideo }} Videos</div>
                    </div>
                </div>
                @endforeach
            </amp-carousel>
        </div>
        @endif
    </div>    
</div>
@endsection