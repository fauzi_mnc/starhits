@extends('layouts.frontend')

@section('title', 'Series') 

@section('content')
<div class="col-xs-12 col-sm-12 col-content p-0 mt-2">
    <div id="profile-picture">
        <div class="particle"
            style="width: 34vw;height:62vh;position: absolute;left:0;bottom:0;transform:translate(-50%,50%)">
            <amp-img src="{{asset('frontend/assets/img/background/bg-circle.png')}}" width="473" height="474" layout="responsive"
                style="transform: rotate(45deg)">
            </amp-img>
        </div>

        <amp-img src="{{asset('frontend/assets/img/background/bg_series.jpg')}}" width="1280" height="357" layout="responsive">
        </amp-img>

        <div id="page-title-series" class="page-title-series">
            <h1 class="font-size-md font-style-i" style="color:#fff">STARHITS</h1>
            <h1 class="font-style-i font-size-xl font-weight-900">
                <span class="text-outline">SERIES</span>
            </h1>
        </div>
    </div>

    {{-- <div>
        <div class="nav">
            <ul id="main">
                <li><a href="#"><span class="text-dark">MOST SUBCRIBERS</span></a></li>
                <li><a href="#"><span class="text-dark">MOST VIEWS</span></a></li>
                <li>
                    <form class="form-inline" method="POST"
                        action-xhr="" target="_top"
                        style="margin: -0.45em 1em;">
                        <div class="d-flex">
                            <div>
                                <amp-img src="frontend/assets/svgs/search.svg" layout="fixed" width="18" height="18"></amp-img>
                            </div>
                            <input class="form-control rounded-0" type="search" name="term"
                            placeholder="SEARCH BY NAME" required>
                        </div>
                    </form>
                </li>
            </ul>

            <div id="dropdown-container">
                <amp-accordion class="nav__dropdown">
                    <section>
                        <header class="nav__dropdown__header text-dark"><span class="show-xl">OUR
                            </span>CREATORS<span class="dropdown-symbol"></span></header>
                        <ul class="nav__dropdown__list">
                            <li class="nav__dropdown__listitem">
                                <a href="#"><span class="text-dark font-weight-bold text-nowrap" style="font-size:14px;">CORPORATE CREATORS</span></a>
                            </li>
                        </ul>
                    </section>
                </amp-accordion>
            </div>
        </div>
    </div> --}}

    <div class="px-lg-5 mt-5">
        <amp-list id="ampseries" class="list" reset-on-refresh layout="responsive" width="100"
            height="60" src="https://starhits.id/api/series" binding="refresh" load-more="manual"
            load-more-bookmark="next_page_url">
            <div overflow role="button" aria-label="SHOW MORE" class="text-center pt-5">
                <span class="load-more-link">SHOW MORE</span>
            </div>
            <template type="amp-mustache">
                <div class="card">
                    <amp-img class="card-img" src="@{{image}}" width="300" height="500"></amp-img>
                    <div class="card-img-overlay">
                        <div class="preview-popup">
                            <div class="preview-popup-description">
                                <small>@{{totalVideo}} Videos</small>
                                <p class="subtitle">
                                    <strong>
                                        @{{title}}
                                    </strong>
                                </p>
                                <amp-fit-text width="250" height="150" max-font-size="12">
                                @{{content}}
                                </amp-fit-text>
                            </div>
                            <a class="preview-popup-link" href="{{ route('series') }}/@{{slug}}">watch video</a>
                        </div>
                    </div>
                </div>
                <div class="caption">
                    <h6>@{{title}}</h6>
                    <p><small>@{{totalVideo}} Videos</small></p>
                </div>
            </template>
            <div fallback>FALLBACK</div>
            <div placeholder>PLACEHOLDER</div>
            <amp-list-load-more load-more-failed>ERROR</amp-list-load-more>
            <amp-list-load-more load-more-end></amp-list-load-more>
            <amp-list-load-more load-more-button>
                <div class="amp-load-more">
                    <button class="load-more-link">LOAD MORE</button>
                </div>
            </amp-list-load-more>
        </amp-list>
    </div>
</div>
@endsection