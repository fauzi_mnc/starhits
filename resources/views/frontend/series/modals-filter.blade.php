<div aria-labelledby="myLargeModalLabel" class="modal fade bs-example-modal-lg-filter" role="dialog" tabindex="1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="col-md-12">
                <div class="top-filter-area">
                    <div class="container-fluid">
                        <form action="/series-filter" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                <!--<div class="col"></div>-->
                                <div class="col-sm-4">
                                    <input class="form-control" name="type" placeholder="Video by Series" type="text">
                                    </input>
                                </div>
                                <div class="col-sm-4">
                                    <select class="select-box" name="sortby">
                                        <option selected="selected" value="0">
                                            Sort by
                                        </option>
                                        <option value="1">
                                            Most Viewed
                                        </option>
                                        <option value="2">
                                            Most Liked
                                        </option>
                                        <option value="3">
                                            Most Commented
                                        </option>
                                        <option value="4">
                                            Published Date
                                        </option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <button class="btn btn-filter btn-lg btn-block site-btn" name="search">
                                        <i class="fa fa-search">
                                        </i>
                                        Search
                                    </button>
                                </div>
                                <!--<div class="col"></div>-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>