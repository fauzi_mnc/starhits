<!-- Navigation -->
<div class="header fixed-top">
    <nav class="navbar navbar-expand-lg navbar-dark" style="overflow: hidden;">
        <div class="container">
            <a class="navbar-brand" href="{{url('/')}}">
                <img alt="" class="logo" src="{{url($SiteConfig['configs']['website_logo_header'])}}"/>
            </a>
            <button aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-target="#navbarResponsive" data-toggle="collapse" type="button">
                <span class="navbar-toggler-icon">
                </span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{url('/')}}">
                            Home
                            <span class="sr-only">
                                (current)
                            </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('series')}}">
                            Series
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('creators')}}">
                            Creators
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('channels')}}">
                            Channels
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('join')}}">
                            Join Us
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('page/about-us')}}">
                            About Us
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('contact')}}">
                            Contact Us
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <div class="text-center">
                                <div class="col-xs-12 main-ads text-center">
                                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                    <!-- starhits ads -->
                                    <ins class="adsbygoogle"
                                         style="display:block;width:320px;height:50px"
                                         data-ad-client="ca-pub-5195976246447231"
                                         data-ad-slot="4189685044"
                                         data-ad-format="auto"
                                         data-full-width-responsive="true"></ins>
                                    <script>
                                    (adsbygoogle = window.adsbygoogle || []).push({});
                                    </script>
                                    {{-- <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                    <ins class="adsbygoogle"
                                        style="display:inline-block;width:468px;height:60px"
                                        data-ad-client="ca-pub-5195976246447231"
                                        data-ad-slot="4189685044"></ins>
                                    <script>
                                    (adsbygoogle = window.adsbygoogle || []).push({});
                                    </script> --}}
                                    {{-- <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                    <!-- starhits ads -->
                                    <ins class="adsbygoogle"
                                         style="display:block"
                                         data-ad-client="ca-pub-5195976246447231"
                                         data-ad-slot="4189685044"
                                         data-ad-format="auto"
                                         data-full-width-responsive="true"></ins>
                                    <script>
                                    (adsbygoogle = window.adsbygoogle || []).push({});
                                    </script> --}}
                                    {{-- <a href="#"><img class="center-ads" src="{{ asset('frontend/assets/img/ads-330x43-web-series.jpg') }}" alt=""></a> --}}
                                </div>
                        </div>
                    </li>
                </ul>
                <ul class="navbar-nav pull-rights">
                    <li class="nav-item">
                        <div class="separated">
                        </div>
                        @foreach($SiteConfig['adsInsideHeaders'] as $value)
                            @if(empty($value->target))
                            <a class="nav-link dissapear" href="{{$value->youtube_id}}" target="_blank">
                                <img alt="" class="ads d-none d-sm-block d-sm-none d-md-block .d-md-none .d-lg-block .d-lg-none .d-xl-block" src="{{ url('/'). Image::url($value->image,935,123,array('crop')) }}"/>
                            </a>
                            @else
                            <a class="nav-link dissapear" href="{{$value->youtube_id}}">
                                <img alt="" class="ads d-none d-sm-block d-sm-none d-md-block .d-md-none .d-lg-block .d-lg-none .d-xl-block" src="{{ url('/'). Image::url($value->image,935,123,array('crop')) }}"/>
                            </a>
                            @endif
                        @endforeach
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">
                            <span class="fa fa-search fa-lg" data-target=".bs-example-modal-lg" data-toggle="modal">
                                <div class="mobile">
                                    Search
                                </div>
                            </span>
                        </a>
                    </li>
                    <li class="nav-item">
                        @guest
                        <a class="nav-link" href="#">
                            <span class="fa fa-user fa-lg" data-target="#loginModal" data-toggle="modal">
                            </span>
                        </a>
                        @else
                        <div class="dropdown show">
                            <a aria-expanded="false" aria-haspopup="true" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="dropdownMenuLink">
                                <span class="fa fa-user fa-lg">
                                </span>
                            </a>
                            <div aria-labelledby="dropdownMenuLink" class="dropdown-menu">
                                @role('admin')
                                <a class="dropdown-item" href="{{ url('admin/dashboard') }}" target="_blank">
                                    Dashboard Admin
                                </a>
                                @endrole
                                @role('creator')
                                <a class="dropdown-item" href="{{ url('creator/serie') }}" target="_blank">
                                    My Account
                                </a>
                                @endrole
                                @role('influencer')
                                <a class="dropdown-item" href="{{ url('influencer/campaign') }}" target="_blank">
                                    My Account
                                </a>
                                @endrole
                                @role('member')
                                <a class="dropdown-item" href="{{ route('self.member.edit', auth()->user()->id) }}" target="_blank">
                                    My Account
                                </a>
                                @endrole
                                <a class="dropdown-item" href="{{ url('/logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                    Log Out
                                </a>
                                <form action="{{ url('/logout') }}" id="logout-form" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    <input style="display: none;" type="submit" value="logout">
                                    </input>
                                </form>
                            </div>
                        </div>
                        @endguest
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>