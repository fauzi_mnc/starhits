@push('meta')    
        <meta property="og:url" content="{{ url()->current() }}" />
        <meta property="og:title" content="{{ $metaTag->title }}" />
        <meta property="og:description" content="{{ $metaTag->content }}" />
        @if(!empty($metaTag->image))
        <meta property="og:image" content="{{ $metaTag->image }}" />
        @else
        <meta property="og:image" content="https://img.youtube.com/vi/{{ $metaTag->attr_6}}/hqdefault.jpg" />
        @endif
        <meta property="fb:app_id" content="952765028218113" />
@endpush
<!-- Stored in resources/views/child.blade.php -->
@extends('layouts.frontend')

@section('title', $singleVideos->title)

@push('preloader')
<div class="se-pre-con">
    <div class="col-sm-12 text-center pre-con">
        <div class="col">
            <div class="logo-preloader">
                <img alt="" class="img-fluid" src="{{ asset('frontend/assets/img/logo-home.png') }}">
                </img>
            </div>
        </div>
        <div class="col">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg">
                <defs>
                    <filter id="gooey">
                        <fegaussianblur in="SourceGraphic" result="blur" stddeviation="10">
                        </fegaussianblur>
                        <fecolormatrix in="blur" mode="matrix" result="goo" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7">
                        </fecolormatrix>
                        <feblend in="SourceGraphic" in2="goo">
                        </feblend>
                    </filter>
                </defs>
            </svg>
            <div class="blob blob-0"></div>
            <div class="blob blob-1"></div>
            <div class="blob blob-2"></div>
            <div class="blob blob-3"></div>
            <div class="blob blob-4"></div>
            <div class="blob blob-5"></div>
        </div>
    </div>
</div>
@endpush

@section('content')

{{--  Page Content  --}}
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <div class="top-content-single">
                <div class="top-slider">
                    <div class="item">
                        <div class="top-block">
                            <div class="top-media">
                                <div class="video-wrap">
                                    <div class="video">
                                        <div class="alert-close pull-right"><img src="{{asset('frontend/assets/img/icon/close.png')}}" alt=""></div>
                                        <div class="youtube embed-responsive embed-responsive-16by9">
                                            {!! $singleVideos->link !!}
                                            <div class="thumb">
                                                <a class="target-big" href="{{ url('/videos/'.$singleVideos->slug) }}">
                                                    {{ $singleVideos->title }}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="articles">
                <div class="row">
                    <div class="col-md-12 main-article">
                        <div class="entry-social">
                            @guest
                                &nbsp;
                            @else
                            <div class="a2a_kit bm">
                                <a href="{{route('videos.bookmark', ['id' => $singleVideos->id])}}" class="{{ $bookmarkClass }}">
                                    <div class="mobile-text">
                                        Bookmark
                                    </div>
                                </a>
                            </div>
                            @endguest
                            <div class="a2a_kit wa">
                                <a class="a2a_button_whatsapp link-social what-color" href="whatsapp://send?text=Hello World!" data-action="share/whatsapp/share" target="_blank">
                                    <div class="mobile-text">
                                        Share
                                    </div>
                                </a>
                            </div>
                            <div class="a2a_kit fb">
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ url('/videos/'.$singleVideos->slug) }}" target="_blank">
                                    <div class="mobile-text">
                                        Share
                                    </div>
                                </a>
                            </div>
                            <div class="a2a_kit twitter">
                                <a href="https://twitter.com/intent/tweet?url={{ url('/videos/'.$singleVideos->slug) }}" target="_blank">
                                    <div class="mobile-text">
                                        Tweet
                                    </div>
                                </a>
                            </div>
                            <div class="a2a_kit gplus">
                                <a href="https://plus.google.com/share?url={{ url('/videos/'.$singleVideos->slug) }}" target="_blank">
                                    <div class="mobile-text">
                                        Share
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="title">
                            <h2>
                                {{$singleVideos->title}}
                            </h2>
                        </div>
                        <div class="sub-title">
                            <div class="row">
                                <div class="col-md-12 main-sub-title">
                                    <h4>
                                        {{$singleVideos->channelVideo->title}}
                                    </h4>
                                </div>
                            </div>
                        </div>
                        <div class="sub-title">
                            <div class="row">
                                <div class="col-md-12 views-2">
                                    <section>
                                        By
                                        <strong>
                                            <a href="{{ url('/creators/'.$singleVideos->users->slug) }}">
                                                {{$singleVideos->users->name}}
                                            </a>
                                        </strong>
                                        @if(!empty($singleVideos->viewed))
                                            <i aria-hidden="true" class="fa fa-eye"></i>
                                            {!! bd_nice_number((int)$singleVideos->viewed) !!}
                                        @endif

                                        @if(!empty($singleVideos->viewed))
                                        <i aria-hidden="true" class="fa fa-comments-o"></i>
                                            {!! bd_nice_number((int)$singleVideos->attr_4) !!}
                                        @endif
                                        @if(!empty($singleVideos->attr_4))
                                        <i aria-hidden="true" class="fa fa-thumbs-up"></i>
                                            {!! bd_nice_number((int)$singleVideos->attr_2) !!}
                                        @endif
                                        @if(!empty($singleVideos->attr_3))
                                        <i aria-hidden="true" class="fa fa-thumbs-down"></i>
                                            {!! bd_nice_number((int)$singleVideos->attr_3) !!}
                                        @endif
                                    </section>
                                </div>
                            </div>
                        </div>
                        <div class="paragraph">
                            <p class="main-paragraph">
                                {!!$singleVideos->content!!}
                            </p>
                        </div>
                        <div class="col-sm-12 main-ads-articles text-center">
                            <!-- @foreach($SiteConfig['adsUnderContent'] as $value)
                                @if(empty($value->target))
                                <a href="{{$value->youtube_id}}" target="_blank">
                                    <img alt="" class="center-ads" src="{{ url('/'). Image::url($value->image,935,123,array('crop')) }}"/>
                                </a>
                                @else
                                <a href="{{$value->youtube_id}}">
                                    <img alt="" class="center-ads" src="{{ url('/'). Image::url($value->image,935,123,array('crop')) }}"/>
                                </a>
                                @endif
                            @endforeach -->

                            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                            <!-- Single Post -->
                            <ins class="adsbygoogle"
                                style="display:inline-block;width:728px;height:90px"
                                data-ad-client="ca-pub-5195976246447231"
                                data-ad-slot="9318663500"></ins>
                            <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>
                        </div>
                        <br>
                            <div id="disqus_thread">
                            </div>
                        </br>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="listing-block">
                <div class="title">
                    <div class="row">
                        <div class="col">
                            <h6>
                                {{strtoupper($singleVideos->users->name)}}
                            </h6>
                        </div>
                        <!-- Auto Next --> 
                        <div class="col play">
                            <p>
                                <span class="auto">
                                    AUTO NEXT
                                </span>
                                <label class="switch">
                                    <input id="autonext" type="checkbox">
                                        <span class="slider round"></span>
                                </label>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="all-media">
                    <div class="all-media-child">
                        @if(sizeof($videos)>0)
                            @foreach($videos as $value)
                        <div class="media">
                            @if(!empty($value->attr_7))
                            <div class="item-top-overlay">
                                {{ $duration->formatted($value->attr_7) }}
                            </div>
                            @else

                                @endif
                            <a href="{{ url('/videos/'.$value->slug) }}">
                                @if(!empty($value->image))
                                <img class="d-flex align-self-start img-fluid top-overlay" src="{{ url('/'). Image::url($value->image,513,350,array('crop')) }}"/>
                                @else
                                <img class="d-flex align-self-start img-fluid top-overlay" src="https://img.youtube.com/vi/{{ $value->attr_6}}/hqdefault.jpg" style="width: 513; height: 350;"/>
                                @endif
                            </a>
                            <div class="media-body pl-3">
                                <div class="media-title-series">
                                    <a href="{{ url('/videos/'.$value->slug) }}">
                                        {{$value->series()->first()->title}}
                                    </a>
                                </div>
                                <div class="media-title">
                                    <a href="{{ url('/videos/'.$value->slug) }}">
                                        {{$value->title}}
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="sidebar my-auto">
                <div class="main-ads-mobile text-center">
                    <div class="sidebar-ads-1">
                        @foreach($SiteConfig['adsRightContentI'] as $value)
                            @if(empty($value->target))
                            <a href="{{$value->youtube_id}}" target="_blank">
                                <img alt="" src="{{ url('/'). Image::url($value->image,144,542,array('crop')) }}"/>
                            </a>
                            @else
                            <a href="{{$value->youtube_id}}">
                                <img alt="" src="{{ url('/'). Image::url($value->image,144,542,array('crop')) }}"/>
                            </a>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="main-ads text-center">
                    <div class="sidebar-ads-1">
                        @foreach($SiteConfig['adsRightContentI'] as $value)
                            @if(empty($value->target))
                            <a href="{{$value->youtube_id}}" target="_blank">
                                <img alt="" src="{{ url('/'). Image::url($value->image,144,542,array('crop')) }}"/>
                            </a>
                            @else
                            <a href="{{$value->youtube_id}}">
                                <img alt="" src="{{ url('/'). Image::url($value->image,144,542,array('crop')) }}"/>
                            </a>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="main-ads text-center">
                    <div class="sidebar-ads-2">
                        @foreach($SiteConfig['adsRightContentII'] as $value)
                            @if(empty($value->target))
                            <a href="{{$value->youtube_id}}" target="_blank">
                                <img alt="" src="{{ url('/'). Image::url($value->image,144,542,array('crop')) }}"/>
                            </a>
                            @else
                            <a href="{{$value->youtube_id}}">
                                <img alt="" src="{{ url('/'). Image::url($value->image,144,542,array('crop')) }}"/>
                            </a>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid top-videos">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        MORE FROM CREATORS
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="{{ count($moreVideos)>=1? 'slider-top-videos':'' }}">
                    @forelse($moreVideos as $value)
                    <div>
                        <div class="item">
                            <div class="imgTitle">
                                <div class="top-videos-overlay">
                                    <a href="{{ url('/videos/'.$value->slug) }}">
                                        @if(!empty($value->image))
                                            <img class="img-fluid top-videos-overlay" src="{{ url('/'). Image::url($value->image,400,225,array('crop')) }}"/>
                                        @else
                                            <img class="img-fluid top-videos-overlay" src="https://img.youtube.com/vi/{{ $value->attr_6}}/mqdefault.jpg" style="width: 400; height: 250;"/>
                                        @endif
                                    </a>
                                    @if(!empty($value->attr_7))
                                    <div class="item-top-videos-overlay">
                                        {{ $duration->formatted($value->attr_7) }}
                                    </div>
                                    @else
                                        &nbsp;
                                    @endif
                                </div>
                            </div>
                            <p>
                                <a href="{{ url('/videos/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                            </p>
                        </div>
                    </div>
                    @empty
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid top-videos">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        RELATED VIDEOS
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="{{ count($relatedVideos)>=1? 'slider-top-videos':'' }}">
                    @forelse($relatedVideos as $value)
                    <div>
                        <div class="item">
                            <div class="imgTitle">
                                <div class="top-videos-overlay">
                                    <a href="{{ url('/videos/'.$value->slug) }}">
                                        @if(!empty($value->image))
                                            <img class="img-fluid top-videos-overlay" src="{{ url('/'). Image::url($value->image,886,490,array('crop')) }}"/>
                                        @else
                                            <img class="img-fluid top-videos-overlay" src="https://img.youtube.com/vi/{{ $value->attr_6}}/mqdefault.jpg" style="width: 886; height: 490;"/>
                                        @endif
                                    </a>
                                    @if(!empty($value->attr_7))
                                    <div class="item-top-videos-overlay">
                                        {{ $duration->formatted($value->attr_7) }}
                                    </div>
                                    @else
                                        &nbsp;
                                    @endif
                                </div>
                            </div>
                            <p>
                                <a href="{{ url('/videos/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                            </p>
                        </div>
                    </div>
                    @empty
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid top-pro">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        RECOMMENDED SERIES
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="{{ count($recomendedSeries)>=1? 'slider-top-pro':'' }}">
                    @forelse($recomendedSeries as $value)
                    <div>
                        <div class="item">
                            <article class="caption">
                                <img class="caption__media img-fluid" src="{{ url('/'). Image::url($value->image,400,550,array('crop')) }}"/>
                                <a href="{{ url('/series/'.$value->slug) }}">
                                    <div class="caption__overlay">
                                        <h1 class="caption__overlay__title">
                                            {{$value->title}}
                                        </h1>
                                        <h7 class="caption__overlay__content">
                                            {{$value->totalVideo}} Videos
                                        </h7>
                                        @if(!empty($value->excerpt))
                                        <p class="caption__overlay__content">
                                            {!!strip_tags($value->excerpt)!!}
                                        </p>
                                        @else
                                        <p class="caption__overlay__content">
                                            {!!strip_tags(str_limit($value->content, 100))!!}
                                        </p>
                                        @endif
                                    </div>
                                </a>
                            </article>
                            <p>
                                <a href="{{ url('/series/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                            </p>
                            <p class="videos">
                                <a href="#">
                                    {{$value->totalVideo}} Videos
                                </a>
                            </p>
                        </div>
                    </div>
                    @empty
                    <div class="col-sm-12">
                        <br>
                            <div class="alert alert-warning text-center" role="alert">
                                There is no data to display!
                            </div>
                        </br>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


{{--  @push('fixedban')

<div id="fixedban">
    <div class="second"><a id='close-fixedban' onclick='document.getElementById(&apos;fixedban&apos;).style.display = &apos;none&apos;;' style='cursor:pointer;'><img alt='close' src='{{asset("frontend/assets/img/icon/close.png")}}' title='close button' style='vertical-align:middle;'/></a></div>
    <div class="third">
        {!! $singleVideos->link !!}
    </div>
</div>

@endpush  --}}

@push('scripts')
    {{-- <script type="text/javascript" src="https://www.youtube.com/iframe_api"></script> --}}
    <script type="text/javascript">
        //console.log($('#player'));
        var autoNext = false;
        $("#autonext").on('change', function() {
            if ($(this).is(':checked')) {
                //autoNext = $(this).is(':checked');
                //alert(autoNext);// To verify
                var tag = document.createElement('script');
                tag.id = 'iframe-demo';
                tag.src = 'https://www.youtube.com/iframe_api';
                var firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                var player;
                function onYouTubeIframeAPIReady() {
                    player = new YT.Player('player', {
                        events: {
                        'onReady': onPlayerReady,
                        'onStateChange': onPlayerStateChange
                        }
                    });
                }
                function onPlayerReady(event) {
                    document.getElementById('player').style.borderColor = '#FF6D00';
                }
                function changeBorderColor(playerStatus) {
                    var color;
                    if (playerStatus == -1) {
                    color = "#37474F"; // unstarted = gray
                    } else if (playerStatus == 0) {
                    color = "#FFFF00"; // ended = yellow
                    } else if (playerStatus == 1) {
                    color = "#33691E"; // playing = green
                    } else if (playerStatus == 2) {
                    color = "#DD2C00"; // paused = red
                    } else if (playerStatus == 3) {
                    color = "#AA00FF"; // buffering = purple
                    } else if (playerStatus == 5) {
                    color = "#FF6DOO"; // video cued = orange
                    }
                    if (color) {
                    document.getElementById('player').style.borderColor = color;
                    }
                }
                function onPlayerStateChange(event) {
                    changeBorderColor(event.data);
                }
                //console.log(player);
            }
            else {
                //autoNext = $(this).is(':checked');
                //alert(autoNext);// To verify
            }
        });
    </script>
@endpush