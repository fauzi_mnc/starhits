@extends('layouts.frontend')

@section('title', 'Creators') 

@section('content')
    <div class="col-xs-12 col-sm-12 col-content p-0 mt-3">
        <div class="particle" style="width:34vw;height:62vh;position: absolute;right:5%;top:10%">
            <amp-img src="{{asset('frontend/assets/images/bg-circle.png') }}" width="473" height="474" layout="responsive"
                style="transform: rotate(45deg)">
            </amp-img>
        </div>

        <div class="particle" style="width: 50px;height:92px;position: absolute;left:5%;top:40%;z-index: -1;">
            <amp-img src="{{asset('frontend/assets/images/bg-triangle.png') }}" width="50" height="92" layout="responsive">
            </amp-img>
        </div>

        <div class="particle" style="width: 34vw;height:62vh;position: absolute;left: 5%;top:75%">
            <amp-img src="{{asset('frontend/assets/images/bg-circle.png') }}" width="473" height="474" layout="responsive"
                style="transform: rotate(45deg)">
            </amp-img>
        </div>

        <div class="particle" style="width: 34vw;height:62vh;position: absolute;right:1%;top:50%">
            <amp-img src="{{asset('frontend/assets/images/bg-circle.png') }}" width="473" height="474" layout="responsive"
                style="transform: rotate(45deg)">
            </amp-img>
        </div>

        {{-- <div style="position: relative;">
            <amp-img src="{{asset('frontend/assets/img/creator_profile_1.png')}}" width="1280" height="607" layout="responsive">
            </amp-img>
            <div style="width: 50vw;position: absolute;bottom: 5%;right: 10%">
                <amp-img src="{{asset('frontend/assets/img/creator_profile_3.png')}}" width="670" height="447" layout="responsive">
                </amp-img>
            </div>
            <div style="width: 55vw;position: absolute;bottom: 5%;right: 12%;top: 0">
                <h1 class="font-size-xl font-weight-900 font-style-i text-black">CREATORS</h1>
                <h1 class="font-size-xl font-weight-900 font-style-i text-right text-outline-black">CORPORATE</h1>
            </div>

        </div> --}}

        <div id="profile-picture">
    
            <amp-img src="{{asset('frontend/assets/img/creator-profile.png')}}" width="1280" height="357" layout="responsive">
            </amp-img>
    
            <div style="width: 55vw;position: absolute;bottom: 5%;left: 12%;top: 0">
                <h1 class="font-size-xl font-weight-900 font-style-i text-black">CREATORS</h1>
                <h1 class="font-size-xl font-weight-900 font-style-i text-right text-outline">CORPORATE</h1>
            </div>
        </div>

        <div>
            <div class="nav">
                <ul id="main">
                    <li>
                        <a href="?f=most-subscribers">
                            @if(!empty(Request::query()) && Request::query('f') == 'most-subscribers')
                            <span class="text-white">MOST SUBCRIBERS</span>
                            @else
                            <span class="text-dark">MOST SUBCRIBERS</span>
                            @endif
                        </a>
                    </li>
                    <li>
                        <a href="?f=most-views">
                            @if(!empty(Request::query()) && Request::query('f') == 'most-views')
                            <span class="text-white">MOST VIEWS</span>
                            @else
                            <span class="text-dark">MOST VIEWS</span>
                            @endif
                        </a>
                    </li>
                    <li hidden>
                        <form class="form-inline" method="POST"
                            action-xhr="https://starhits.id" target="_top"
                            style="margin: -0.45em 1em;">
                            <div class="d-flex">
                                <div>
                                    <amp-img src="{{asset('frontend/assets/svgs/search.svg') }}" layout="fixed" width="18" height="18"></amp-img>
                                </div>
                                <input class="form-control rounded-0" type="search" name="term"
                                placeholder="SEARCH BY NAME" required>
                            </div>
                        </form>
                    </li>
                </ul>

                <div id="dropdown-container">
                    <amp-accordion class="nav__dropdown">
                        <section>
                            <header class="nav__dropdown__header text-white">
                                <span class="show-xl">CORPORATE</span> CREATORS<span class="dropdown-symbol"></span></header>
                            <ul class="nav__dropdown__list">
                                <li class="nav__dropdown__listitem">
                                    <a href="{{url('/creators/personal')}}"><span class="text-dark" style="font-size:14px;">OUR CREATORS</span></a>
                                </li>
                            </ul>
                        </section>
                    </amp-accordion>
                </div>
            </div>

            <div style="position: relative;" class="col-md-10 mx-auto mt-3">
                @if(empty(Request::query()))
                <amp-list id="amp-list-tab-creator" class="list-more mb-5" reset-on-refresh layout="fixed-height" height="480" src="https://starhits.id/api/creators/corporate" binding="refresh" load-more="manual" load-more-bookmark="next_page_url">
                @else
                <amp-list id="amp-list-tab-creator" class="list-more mb-5" reset-on-refresh layout="fixed-height" height="480" src="https://starhits.id/api/creators/corporate?f={{ Request::query('f') }}" binding="refresh" load-more="manual" load-more-bookmark="next_page_url">
                @endif
                    <div hidden role="button" aria-label="SHOW MORE" class="text-center pt-5">
                        <span class="load-more-link">SHOW MORE</span>
                    </div>

                    <template type="amp-mustache">
                        <div class="card">
                            <amp-img class="card-img" src="{{url('/').'/'}}@{{image}}" layout="responsive" width="322"
                                height="256"></amp-img>
                            <div class="card-img-overlay px-xl-4 preview-popup">
                                <h6 class="preview-popup-title">@{{name}}</h6>
                                <div class="my-3 preview-popup-info">
                                    <div class="d-flex align-items-center">
                                        <amp-layout class="preview-popup-icon" layout="fixed" width="18"
                                            height="18">
                                            <amp-img layout="responsive" width="1" height="1"
                                                src="{{asset('frontend/assets/svgs/youtube-black.svg') }}"></amp-img>
                                        </amp-layout>
                                        <span>@{{yt_subs}}</span>
                                    </div>

                                    <div class="d-flex align-items-center">
                                        <amp-layout class="preview-popup-icon" layout="fixed" width="18"
                                            height="18">
                                            <amp-img layout="responsive" width="1" height="1"
                                                src="{{asset('frontend/assets/svgs/instagram-black.svg') }}"></amp-img>
                                        </amp-layout>
                                        <span>@{{followers_ig}}</span>
                                    </div>

                                    <div class="d-flex align-items-center">
                                        <amp-layout class="preview-popup-icon" layout="fixed" width="18"
                                            height="18">
                                            <amp-img layout="responsive" width="1" height="1"
                                                src="{{asset('frontend/assets/svgs/twitter-black.svg') }}"></amp-img>
                                        </amp-layout>
                                        <span>@{{followers_twitter}}</span>
                                    </div>

                                    {{-- <div class="d-flex align-items-center">
                                        <amp-layout class="preview-popup-icon" layout="fixed" width="18"
                                            height="18">
                                            <amp-img layout="responsive" width="1" height="1"
                                                src="{{asset('frontend/assets/svgs/facebook-f-black.svg') }}"></amp-img>
                                        </amp-layout>
                                        <span>@{{socials.1.followers}}</span>
                                    </div> --}}

                                </div>
                                <a class="preview-popup-link" href="{{ url('/creators') }}/@{{slug}}">See profile</a>
                            </div>
                        </div>
                    </template>
                    <div fallback>FALLBACK</div>
                    <div placeholder>Loading...</div>
                    <amp-list-load-more load-more-failed>ERROR</amp-list-load-more>
                    <amp-list-load-more load-more-end></amp-list-load-more>
                    <amp-list-load-more load-more-button>
                        <div class="amp-load-more">
                            <button class="load-more-link">LOAD MORE</button>
                        </div>
                    </amp-list-load-more>
                </amp-list>
            </div>
        </div>
    </div>
@endsection