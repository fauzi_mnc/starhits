@extends('layouts.frontend')

@section('css')
    @include('frontend.includes.extensions.amp-creator-video-list')
@endsection

@section('content')
    <div id="profile-picture-video-list">
        <div
            style="width: 34vw;height:62vh;position: absolute;left:0;bottom:0;transform:translate(-50%,50%)">
            <amp-img src="{{ asset('frontend/assets/images/bg-circle.png') }}" width="473" height="474" layout="responsive"
                style="transform: rotate(45deg)">
            </amp-img>
        </div>

        <amp-img src="{{ url('/').'/'.$creators->img_header }}" width="1280" height="357" layout="responsive">
        </amp-img>

        <div id="page-title-video-list" class="text-center">
            <h1 class="font-size-md" style="color: #fff;">{{ strtoupper($creators->name) }}</h1>
            <h1 class="font-style-i font-size-xl font-weight-900">
                <span class="text-outline">VIDEO</span>
            </h1>
        </div>
    </div>

    {{-- <nav class="nav">
        <ul id="main">
            <li><a href="#" class="text-dark">MOST SUBCRIBERS</a></li>
            <li><a href="#" class="text-dark">MOST VIEWS</a></li>
            <li>
                <form class="form-inline" method="POST"
                    action-xhr="" target="_top"
                    style="margin: -0.45em 1em;">
                    <div class="d-flex">
                        <div>
                            <amp-img src="{{ asset('frontend/assets/svgs/search.svg') }}" layout="fixed" width="20" height="20"></amp-img>
                        </div>
                        <input class="form-control rounded-0" type="search" name="term"
                        placeholder="SEARCH BY NAME" required>
                    </div>
                </form>
            </li>
        </ul>

        <div id="dropdown-container">
            <amp-accordion class="nav__dropdown">
                <section>
                    <header class="nav__dropdown__header text-dark"><span class="show-xl">CHANNEL
                        </span>CREATOR<span class="dropdown-symbol"></span></header>
                    <ul class="nav__dropdown__list">
                        <li class="nav__dropdown__listitem">
                            <a href="#" class="text-dark">CORPORATE CREATOR</a>
                        </li>
                    </ul>
                </section>
            </amp-accordion>
        </div>
    </nav> --}}

    <div style="position: relative">
        <amp-list id="videoList" class="list" reset-on-refresh layout="responsive" width="600"
        height="180" src="https://starhits.id/api/creators/video-list/{{ $slug }}" binding="refresh"
            load-more="manual" load-more-bookmark="next_page_url">
            <template type="amp-mustache">
                <div class="card border-0">
                    <amp-img class="card-img" src="https://img.youtube.com/vi/@{{attr_6}}/mqdefault.jpg" width="349" height="211" layout="responsive">
                    </amp-img>
                    <a href="{{url('/videos').'/'}}@{{slug}}" class="card-img-overlay icon-play"></a>
                </div>
                    
                <div class="card-body caption-video-list mb-5">
                    {{-- <div class="title">@{{title}}</div>
                    <div class="small"><span class="dot"></span> @{{created_at}}</div> --}}
                    <amp-fit-text layout="fixed-height" height="50" max-font-size="18">@{{title}}
                    </amp-fit-text>
                </div>
            </template>
            <div fallback>FALLBACK</div>
            <div placeholder>PLACEHOLDER</div>
            <amp-list-load-more load-more-failed>ERROR</amp-list-load-more>
            <amp-list-load-more load-more-end></amp-list-load-more>
            <amp-list-load-more load-more-button>
                <div class="amp-load-more">
                    <button class="load-more-link">LOAD MORE</button>
                </div>
            </amp-list-load-more>
        </amp-list>
    </div>
@endsection