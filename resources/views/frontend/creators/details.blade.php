@extends('layouts.frontend')

@section('title', $metaTag->name)

@section('meta')    
        <meta property="og:url" content="{{ url()->current() }}" />
        <meta property="og:title" content="{{ $metaTag->name }}" />
        <meta property="og:description" content="{{ $metaTag->biodata }}" />
        <meta property="og:image" content="{{ url($metaTag->image) }}" />
        <meta property="fb:app_id" content="952765028218113" />
@endsection

@section('content')
    <div class="col-content">
        <div class="d-flex justify-content-between"
            style="position: absolute;top: 0;right: 15Px;left: 15px">
            <div>&nbsp;</div>
            <div style="width: 473px;height:474px;padding-top: 150px;padding-right: 92px">
                <amp-img src="{{asset('frontend/assets/images/bg-circle.png')}}" width="473" height="474" layout="responsive"
                    style="transform: rotate(45deg)">
                </amp-img>
            </div>
            <div style="width:50px;height:92px">
                <amp-img src="{{asset('frontend/assets/images/bg-triangle.png')}}" width="50" height="92">
                </amp-img>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 mt-4 px-0">
                <amp-img src="{{ url('/'). Image::url($creators->img_header,1280,380,array('crop')) }}" width="1280" height="380"
                    layout="responsive">
                </amp-img>

                <div id="page-title-creators-details">
                    <h1 class="font-style-i font-size-md font-weight-900 text-center">
                        <span>{{ $creators->name }}</span>
                    </h1>
                    <div class="text-center">
                        <a href="https://youtube.com/channel/{{ $creators->provider_id }}" class="creator-details-assets-link">
                            <amp-img src="{{asset('frontend/assets/svgs/youtube-black.svg')}}" width="18" height="18"></amp-img>
                        </a>
                        @foreach ($creators->socials as $socials)
                            @if($socials->name=='ig')
                                @if(!empty($socials->url))
                                    <a href="{{$socials->url}}" target="_blank" class="creator-details-assets-link">
                                        <amp-img src="{{asset('frontend/assets/svgs/instagram-black.svg')}}" width="18" height="18"></amp-img>
                                    </a>
                                @else
                                    <a href="#" class="creator-details-assets-link">
                                        <amp-img src="{{asset('frontend/assets/svgs/instagram-black.svg')}}" width="18" height="18"></amp-img>
                                    </a>
                                @endif
                            @elseif($socials->name=='fb')
                                @if(!empty($socials->url))
                                    <a href="{{$socials->url}}" target="_blank" class="creator-details-assets-link">
                                        <amp-img src="{{asset('frontend/assets/svgs/facebook-f-black.svg')}}" width="18" height="18"></amp-img>
                                    </a>
                                @else
                                    <a href="#" class="creator-details-assets-link">
                                        <amp-img src="{{asset('frontend/assets/svgs/facebook-f-black.svg')}}" width="18" height="18"></amp-img>
                                    </a>
                                @endif
                            @elseif($socials->name=='twitter')
                                @if(!empty($socials->url))
                                    <a href="{{$socials->url}}" target="_blank" class="creator-details-assets-link">
                                        <amp-img src="{{asset('frontend/assets/svgs/twitter-black.svg')}}" width="18" height="18"></amp-img>
                                    </a> 
                                @else
                                    <a href="#" class="creator-details-assets-link">
                                        <amp-img src="{{asset('frontend/assets/svgs/twitter-black.svg')}}" width="18" height="18"></amp-img>
                                    </a> 
                                @endif
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5 pt-5 pl-xl-5 ml-xl-5">
                <h1 class="mb-3 font-size-md font-weight-700"><span class="creators-details text-outline">GET TO</span> KNOW</h1>
                <div>
                    {{ $creators->biodata }}
                </div>

                <h1 class="py-3 font-size-md font-weight-700"><span class="creators-details text-outline">FOLLOWERS</span></h1>

                <div class="d-columns-2">
                    <div class="d-flex align-items-center py-3">
                        <amp-img class="mr-2" src="{{asset('frontend/assets/svgs/youtube-black.svg')}}" width="32" height="32">
                        </amp-img>
                        <span class="h4 m-0">{{ $creators->yt_subs }}</span>
                    </div>
                    <div class="d-flex align-items-center py-3">
                        <amp-img class="mr-2" src="{{asset('frontend/assets/svgs/twitter-black.svg')}}" width="32" height="32">
                        </amp-img>
                        <span class="h4 m-0">{{ ($creators->followers_twitter == NULL) ? '0' : $creators->followers_twitter }}</span>
                </div>
                    <div class="d-flex align-items-center py-3">
                        <amp-img class="mr-2" src="{{asset('frontend/assets/svgs/instagram-black.svg')}}" width="32" height="32">
                        </amp-img>
                        <span class="h4 m-0">{{ ($creators->followers_ig == NULL) ? '0' : $creators->followers_ig }}</span>
                    </div>
                    <div class="d-flex align-items-center py-3">
                        <amp-img class="mr-2" src="{{asset('frontend/assets/svgs/facebook-f-black.svg')}}" width="32" height="32">
                        </amp-img>
                        <span class="h4 m-0">{{ ($creators->followers_fb == NULL) ? '0' : $creators->followers_fb }}</span>
                    </div>
                </div>

                <a id="see-portofolio-link" href="#">see portofolio</a>

            </div>
            <div class="col-md-5 pt-5">
                <amp-img src="{{  url('/').'/'.$creators->img_content }}" width="334" height="334" layout="responsive" style="z-index: 2">
                </amp-img>
            </div>
        </div>

        <div id="orange-box">
            <div class="row">
                <div class="col-xl-11 text-md-right text-center py-5" style="margin: auto;">
                    <a class="link-xl bg-white" href="{{ route('creators.slug.videolist', $creators->slug ) }}">VIEW POPULAR VIDEO</a>
                </div>
            </div>

            <h1 class="text-center font-size-md p-3 font-weight-700"><span class="creators-details text-outline">LATEST</span> VIDEO</h1>

            <div class="col-md-11 mx-auto">
                @if(empty(json_decode($latestVideos)))
                <div class="col-sm-10 mx-auto d-block my-5">
                    <br>
                        <div class="font-size-sm text-center">
                            There is no data to display!
                        </div>
                    <br>
                </div>
                @else
                    <amp-carousel id="latestVideosCreatorsDetail" height="450" layout="fixed-height" type="carousel">
                        @foreach($latestVideos as $items)
                        @if(!empty(json_decode($items)))
                        <div class="slide mx-3">
                            <div class="card" style="position: relative">
                                @if(!empty($items->image))
                                <amp-img src="{{ url('/'). Image::url($items->image,400,225,array('crop')) }}" width="345" height="194" class="imgCreatorsDetail"></amp-img>
                                @else
                                <amp-img src="https://img.youtube.com/vi/{{ $items->attr_6}}/mqdefault.jpg" width="345" height="194" class="imgCreatorsDetail"></amp-img>
                                @endif
                                <div class="card-img-overlay">
                                    <a class="view-videos-link" href="{{ url('/videos/'.$items->slug) }}">view video</a>
                                </div>
                            </div>
                            <div class="caption">
                                <a href="{{ url('/videos/'.$items->slug) }}">
                                    <h6>{{$items->title}}</h6>
                                    <p class="small">{{str_replace('Subtitle','',$items->subtitle)}}</p>
                                </a>
                            </div>
                        </div>
                        @else

                        @endif

                        @endforeach

                        @if(count($latestVideos))
                        <div class="slide mx-3">
                            <div style="position: relative">
                                <amp-layout width="354" height="194" layout="responsive" type="slides"></amp-layout>
                                <!-- <amp-img src="https://via.placeholder.com/345x194.png/ffa500/ffa500" width="345" height="194"></amp-img> -->
                                <a class="link-xl center-both" href="{{ route('creators.slug.videolist', $creators->slug ) }}"><span
                                        class="text-white">VIEW ALL</span></a>
                            </div>
                            <div class="caption">
                                <h6>&nbsp;</h6>
                                <p>&nbsp;</p>
                            </div>
                        </div>
                        @else

                        @endif
                    </amp-carousel>
                @endif
            </div>

            <div class="bg-circle" style="width:34vw;height:62vh;position: absolute;left: -5vw;">
                <amp-img src="{{asset('frontend/assets/images/bg-circle.png')}}" width="473" height="474" layout="responsive"
                    style="transform: rotate(279deg)">
                </amp-img>
            </div>
        </div>

        <h1 class="text-center font-size-md py-5 font-weight-700 "><span class="creators-details text-outline">LATEST</span> SERIES</h1>

        <div class="row">
            @if(empty(json_decode($latestSeries)))
            <div class="col-sm-10 mx-auto d-block my-5">
                <br>
                    <div class="font-size-sm text-center">
                        There is no data to display!
                    </div>
                <br>
            </div>
            @else
            <div class="col-md-11 mx-auto" style="margin-bottom: 5em">
                <amp-carousel id="latestSeriesCreatorsDetail" width="auto" height="584" layout="fixed-height"
                    type="carousel">
                    @foreach($latestSeries as $items)
                    <div class="card">
                        <amp-img class="card-img" src="{{ url('/').$items->image }}" width="350" height="584">
                        </amp-img>
                        <div class="card-img-overlay">
                            <p>{{$items->totalVideo}} Videos</p>
                            <h6>{{$items->title}}</h6>
                            <amp-fit-text height="200" layout="fixed-height" max-font-size="14">
                                {{ strip_tags($items->excerpt)}}
                            </amp-fit-text>
                            <a class="view-videos-link" href="{{ url('/series/'.$items->slug) }}">view videos</a>
                        </div>
                    </div>
                    @endforeach
                </amp-carousel>
            </div>
            @endif
        </div>
    </div>
@endsection