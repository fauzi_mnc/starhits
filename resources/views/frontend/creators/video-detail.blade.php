@extends('layouts.frontend')

@section('css')
    @include('frontend.includes.extensions.amp-creator-video-details')
@endsection

@section('content')
    <div class="col-content">
        <div class="row">
            <div class="col-md-8 mx-auto">
                <div>
                    <amp-youtube id="myLiveChannel" data-videoid="{{ $singleVideos['attr_6'] }}" width="358" height="204" layout="responsive">
                        <amp-img src="https://i.ytimg.com/vi/{{ $singleVideos['attr_6'] }}/maxresdefault_live.jpg" placeholder width="358" height="204" layout="responsive"/>
                    </amp-youtube>
                </div>

                <div class="d-flex justify-content-center counting">
                        <div class="d-flex align-items-center">
                            <div class="icon-container-18">
                                <amp-img src="{{asset('frontend/assets/icon/icon-eye.png')}}" layout="responsive" width="1" height="1"></amp-img>
                            </div>
                        <div class="numcount">{{ bd_nice_number((int)$singleVideos['viewed']) }}</div>
                        </div>
                        <div class="d-flex align-items-center">
                            <div class="icon-container-18">
                                <amp-img src="{{asset('frontend/assets/icon/icon-comments.png')}}" layout="responsive" width="1" height="1">
                                </amp-img>
                            </div>
                            <div class="numcount">{{ bd_nice_number((int)$singleVideos['attr_4']) }}</div>
                        </div>
                        <div class="d-flex align-items-center">
                            <div class="icon-container-18">
                                <amp-img src="{{asset('frontend/assets/icon/icon-like.png')}}" layout="responsive" width="1" height="1"></amp-img>
                            </div>
                            <div class="numcount">{{ bd_nice_number((int)$singleVideos['attr_2']) }}</div>
                        </div>
                        <div class="d-flex align-items-center">
                            <div class="icon-container-18">
                                <amp-img src="{{asset('frontend/assets/icon/icon-dislike.png')}}" layout="responsive" width="1" height="1">
                                </amp-img>
                            </div>
                            <div class="numcount">{{ bd_nice_number((int)$singleVideos['attr_3']) }}</div>
                        </div>
                    </div>

                {{-- <div style="padding: 2em 0">
                    <amp-selector id="videoPlaylist" class="carousel-preview"
                        on="select:videoPlayer.goToSlide(index=event.targetOption)" layout="container">
                        <amp-img option="0" selected src="https://unsplash.it/60/40?image=10" width="120"
                            height="80" alt="a sample image"></amp-img>
                        <amp-img option="1" src="https://unsplash.it/60/40?image=11" width="120" height="80"
                            alt="a sample image"></amp-img>
                        <amp-img option="2" src="https://unsplash.it/60/40?image=12" width="120" height="80"
                            alt="a sample image"></amp-img>
                        <amp-img option="3" src="https://unsplash.it/60/40?image=13" width="120" height="80"
                            alt="a sample image"></amp-img>
                    </amp-selector>
                </div> --}}



                <div class="py-2">
                    <h2>{{ $singleVideos->title }}</h2>
                    <p class="m-0">{{ date_format($singleVideos->created_at, 'd F Y') }}</p>
                    <p>Creator: {{ $singleVideos->users->name }}</p>
                    <p class="text-justify">{{ strip_tags($singleVideos['content']) }}</p>
                </div>
                <div class="d-flex justify-content-xl-end align-items-center">
                    <div class="mr-3"><strong>share on: </strong></div>
                    <div>
                        <a href="https://www.facebook.com/sharer/sharer.php?u={{ url('/videos/'.$singleVideos->slug) }}">
                            <div class="icon-container-32">
                                <amp-img src="{{ asset('frontend/assets/icon/iconfinder_facebook_circle_black_107153.png') }}"
                                    layout="responsive" width="1" height="1"></amp-img>
                            </div>
                        </a>
                    </div>
                    <div>
                        <a href="https://twitter.com/intent/tweet?url={{ url('/videos/'.$singleVideos->slug) }}">
                            <div class="icon-container-32">
                                <amp-img src="{{ asset('frontend/assets/icon/iconfinder_43-twitter_104461.png') }}"
                                    layout="responsive" width="1" height="1"></amp-img>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>


        <div id="orange-box-flip">
            <div class="row">
                @if(empty(json_decode($moreVideos)))
                <div class="col-sm-10 mx-auto d-block my-5">
                    <h1 class="font-weight-700">NEXT EPISODE</h1>
                    <br>
                        <div class="font-size-sm text-center">
                            There is no data to display!
                        </div>
                    <br>
                </div>
                @else
                <div class="col-md-11 mx-auto">
                    <h1 class="font-weight-700">NEXT EPISODE</h1>
                    <amp-carousel id="latestVideoDetails" height="340" layout="fixed-height">
                        @foreach($moreVideos as $items)
                            <div class="slide mx-2">
                                <div class="card border-0" style="position: relative">
                                    @if(!empty($items->image))
                                    <amp-img src="{{ url('/'). Image::url($items->image,400,225,array('crop')) }}" width="345" height="194"></amp-img>
                                    @else
                                    <amp-img src="https://img.youtube.com/vi/{{ $items->attr_6}}/mqdefault.jpg" width="345" height="194"></amp-img>
                                    @endif
                                    <a href="{{ url('/videos/'.$items->slug) }}" class="card-img-overlay icon-play"></a>
                                </div>

                                <div class="caption mb-5" style="padding:20px 0;">
                                    <amp-fit-text layout="fixed-height" height="30" min-font-size="16" max-font-size="16" class="mb-5">{{ $items->title }}</amp-fit-text>
                                </div>
                            </div>
                        @endforeach
                            <div class="slide mx-2">
                                <div style="position: relative">
                                    <amp-layout width="354" height="194" layout="fixed"></amp-layout>
                                    <a class="link-xl center-both" href="{{ route('creators.slug.videolist', $userVideos->slug ) }}"><span
                                            class="text-white">VIEW ALL</span></a>
                                </div>
                                <div class="caption">
                                    <h6>&nbsp;</h6>
                                    <p>&nbsp;</p>
                                </div>
                            </div>
                    </amp-carousel>
                </div>
                @endif
            </div>
        </div>

        <div class='row'>
            @if(empty(json_decode($recomendedSeries)))
            <div class="col-sm-10 mx-auto d-block my-5">
                <h1 class="mb-3">RECOMENDED SERIES</h1>
                <br>
                    <div class="font-size-sm text-center">
                        There is no data to display!
                    </div>
                <br>
            </div>
            @else
            <div class="col-md-11 mx-auto" style="margin-bottom: 5rem">
                <h1 class="mb-3">RECOMENDED SERIES</h1>
                <amp-carousel id="row-latest-video" height="500" layout="fixed-height">
                    @foreach($recomendedSeries as $items)
                    <div class="slide mx-2">
                        <div class="card border-0" style="position: relative">
                            <amp-img src="{{ url('/').$items->image }}" width="258" height="408"></amp-img>
                            <a href="{{ route('series').'/'.$items->slug }}" class="card-img-overlay"></a>
                        </div>

                        <div class="caption" style="padding:15px 0;">
                            <amp-fit-text layout="fixed-height" height="50" max-font-size="20">{{ $items->title }}</amp-fit-text>
                            <div class="small">{{ $items->totalVideos }} Videos</div>
                        </div>
                    </div>
                    @endforeach
                </amp-carousel>
            </div>
            @endif
        </div>
    </div>
@endsection