@extends('layouts.frontend')

@section('title', 'Creators') 

@section('content')
    <div class="col-xs-12 col-sm-12 col-content p-0 mt-3">
        <div class="particle" style="width:34vw;height:62vh;position: absolute;right:5%;top:10%">
            <amp-img src="frontend/assets/images/bg-circle.png" width="473" height="474" layout="responsive"
                style="transform: rotate(45deg)">
            </amp-img>
        </div>

        <div class="particle" style="width: 50px;height:92px;position: absolute;left:5%;top:40%;z-index: -1;">
            <amp-img src="frontend/assets/images/bg-triangle.png" width="50" height="92" layout="responsive">
            </amp-img>
        </div>

        <div class="particle" style="width: 34vw;height:62vh;position: absolute;left: 5%;top:75%">
            <amp-img src="frontend/assets/images/bg-circle.png" width="473" height="474" layout="responsive"
                style="transform: rotate(45deg)">
            </amp-img>
        </div>

        <div class="particle" style="width: 34vw;height:62vh;position: absolute;right:1%;top:50%">
            <amp-img src="frontend/assets/images/bg-circle.png" width="473" height="474" layout="responsive"
                style="transform: rotate(45deg)">
            </amp-img>
        </div>

        <div id="profile-picture">
    
            <amp-img src="{{asset('frontend/assets/img/creator-profile.png')}}" width="1280" height="357" layout="responsive">
            </amp-img>
    
            <div style="width: 55vw;position: absolute;bottom: 5%;left: 12%;top: 0">
                <h1 class="font-size-xl font-weight-900 font-style-i text-black">CREATORS</h1>
                <h1 class="font-size-xl font-weight-900 font-style-i text-right text-outline">PROFILE</h1>
            </div>
            <div id="navi"></div>
        </div>
        <div>
            <div class="nav">
                <ul id="main">
                    <li>
                        <a href="?f=most-subscribers">
                            @if(!empty(Request::query()) && Request::query('f') == 'most-subscribers')
                            <span class="text-white">MOST SUBCRIBERS</span>
                            @else
                            <span class="text-dark">MOST SUBCRIBERS</span>
                            @endif
                        </a>
                    </li>
                    <li>
                        <a href="?f=most-views">
                            @if(!empty(Request::query()) && Request::query('f') == 'most-views')
                            <span class="text-white">MOST VIEWS</span>
                            @else
                            <span class="text-dark">MOST VIEWS</span>
                            @endif
                        </a>
                    </li>
                    <li hidden>
                        <form class="form-inline" method="POST"
                            action-xhr="https://starhits.id" target="_top"
                            style="margin: -0.45em 1em;">
                            <div class="d-flex">
                                <div>
                                    <amp-img src="frontend/assets/svgs/search.svg" layout="fixed" width="18" height="18"></amp-img>
                                </div>
                                <input class="form-control rounded-0" type="search" name="term"
                                placeholder="SEARCH BY NAME" required>
                            </div>
                        </form>
                    </li>
                </ul>

                <div id="dropdown-container">
                    <amp-accordion class="nav__dropdown">
                        <section>
                            <header class="nav__dropdown__header text-dark">
                                <span class="show-xl">OUR</span> CREATORS<span class="dropdown-symbol"></span></header>
                            <ul class="nav__dropdown__list">
                                <li class="nav__dropdown__listitem">
                                    <a href="{{url('/creators/corporate')}}"><span class="text-dark" style="font-size:14px;">CORPORATE CREATORS</span></a>
                                </li>
                            </ul>
                        </section>
                    </amp-accordion>
                </div>
            </div>

            @if(empty(Request::query()))
            <div style="position: relative" class="col-md-10 mx-auto pt-2">
                <amp-list id="ampfeature" class="list" src="https://starhits.id/api/creators/feature-page" width="100"
                height="900"
                layout="responsive">
                    <template type="amp-mustache">
                        <div class="card">
                            <amp-img class="card-img" src="@{{feature_page}}" layout="responsive" width="@{{image_width}}"
                                height="@{{image_height}}">
                            </amp-img>

                            <div class="card-img-overlay">
                                <div class="preview-popup-triangle mt-auto"></div>
                                    <div class="px-xl-4 preview-popup">
                                    <h6 class="preview-popup-title">@{{name}}</h6>
                                    <div class="my-3 preview-popup-info">
                                        <div class="d-flex align-items-center">
                                            <amp-layout class="preview-popup-icon" layout="fixed" width="18"
                                                height="18">
                                                <amp-img layout="responsive" width="1" height="1"
                                                    src="frontend/assets/svgs/youtube-black.svg"></amp-img>
                                            </amp-layout>
                                            <span>@{{yt_subs}}</span>
                                        </div>

                                        <div class="d-flex align-items-center">
                                            <amp-layout class="preview-popup-icon" layout="fixed" width="18"
                                                height="18">
                                                <amp-img layout="responsive" width="1" height="1"
                                                    src="frontend/assets/svgs/instagram-black.svg"></amp-img>
                                            </amp-layout>
                                            <span>@{{followers_ig}}</span>
                                        </div>

                                        <div class="d-flex align-items-center">
                                            <amp-layout class="preview-popup-icon" layout="fixed" width="18"
                                                height="18">
                                                <amp-img layout="responsive" width="1" height="1"
                                                    src="frontend/assets/svgs/twitter-black.svg"></amp-img>
                                            </amp-layout>
                                            <span>@{{followers_twitter}}</span>
                                        </div>

                                        {{-- <div class="d-flex align-items-center">
                                            <amp-layout class="preview-popup-icon" layout="fixed" width="18"
                                                height="18">
                                                <amp-img layout="responsive" width="1" height="1"
                                                    src="frontend/assets/svgs/facebook-f-black.svg"></amp-img>
                                            </amp-layout>
                                            <span>@{{socials.1.followers}}</span>
                                        </div> --}}
                                    </div>

                                        <a class="preview-popup-link" href="{{ url('/creators') }}/@{{slug}}">See profile</a>
                                </div>
                            </div>

                        </div>
                    </template>
                    <div overflow class="list-overflow">
                        See more
                    </div>
                </amp-list>   
                
                <div id="secbutton" class="text-center pt-5" style="margin-bottom: 5rem;">
                    {{-- <a id="refreshbutton" class="load-more-link button d-inline" on="tap:ampfeature.refresh">REFRESH LIST</a> --}}
                    <a id="playerbutton" class="load-more-link button d-inline" on="tap:player.show,playerbutton.hide,secbutton.hide">LOAD MORE</a>
                </div>
            </div>
            @endif
                        
            @if(empty(Request::query()))
            <div id="player" hidden style="position: relative;" class="col-md-10 mx-auto">
            @else
            <div style="position: relative;" class="col-md-10 mx-auto mt-4">
            @endif
                @if(empty(Request::query()))
                <amp-list id="amp-list-tab-creator" class="list-more mb-5" reset-on-refresh layout="fixed-height" height="300" src="https://starhits.id/api/creators/no-feature-page" binding="refresh" load-more="manual" load-more-bookmark="next_page_url">
                @else
                <amp-list id="amp-list-tab-creator" class="list-more mb-5" reset-on-refresh layout="fixed-height" height="500" src="https://starhits.id/api/creators/{{ Request::query('f') }}" binding="refresh" load-more="manual" load-more-bookmark="next_page_url">
                @endif
                    <div overflow role="button" aria-label="SHOW MORE" class="text-center pt-5">
                        <span class="load-more-link">SHOW MORE</span>
                    </div>

                    <template type="amp-mustache">
                        <div class="card">
                            @if(empty(Request::query()))
                            <amp-img class="card-img" src="{{url('/').'/'}}@{{image}}" layout="responsive" width="@{{image_width}}"
                                height="@{{image_height}}"></amp-img>
                            @else
                            <amp-img class="card-img" src="@{{image}}" layout="responsive" width="322"
                            height="256"></amp-img>
                            @endif
                            <div class="card-img-overlay px-xl-4 preview-popup">
                                <h6 class="preview-popup-title">@{{name}}</h6>
                                <div class="my-3 preview-popup-info">
                                    <div class="d-flex align-items-center">
                                        <amp-layout class="preview-popup-icon" layout="fixed" width="18"
                                            height="18">
                                            <amp-img layout="responsive" width="1" height="1"
                                                src="frontend/assets/svgs/youtube-black.svg"></amp-img>
                                        </amp-layout>
                                        <span>@{{yt_subs}}</span>
                                    </div>

                                    <div class="d-flex align-items-center">
                                        <amp-layout class="preview-popup-icon" layout="fixed" width="18"
                                            height="18">
                                            <amp-img layout="responsive" width="1" height="1"
                                                src="frontend/assets/svgs/instagram-black.svg"></amp-img>
                                        </amp-layout>
                                        <span>@{{followers_ig}}</span>
                                    </div>

                                    <div class="d-flex align-items-center">
                                        <amp-layout class="preview-popup-icon" layout="fixed" width="18"
                                            height="18">
                                            <amp-img layout="responsive" width="1" height="1"
                                                src="frontend/assets/svgs/twitter-black.svg"></amp-img>
                                        </amp-layout>
                                        <span>@{{followers_twitter}}</span>
                                    </div>

                                    {{-- <div class="d-flex align-items-center">
                                        <amp-layout class="preview-popup-icon" layout="fixed" width="18"
                                            height="18">
                                            <amp-img layout="responsive" width="1" height="1"
                                                src="frontend/assets/svgs/facebook-f-black.svg"></amp-img>
                                        </amp-layout>
                                        <span>@{{socials.1.followers}}</span>
                                    </div> --}}

                                </div>
                                <a class="preview-popup-link" href="{{ url('/creators') }}/@{{slug}}">See profile</a>
                            </div>
                        </div>
                    </template>
                    <div fallback>FALLBACK</div>
                    <div placeholder>Loading...</div>
                    <amp-list-load-more load-more-failed>ERROR</amp-list-load-more>
                    <amp-list-load-more load-more-end></amp-list-load-more>
                    <amp-list-load-more load-more-button>
                        <div class="amp-load-more">
                            <button class="load-more-link">LOAD MORE</button>
                        </div>
                    </amp-list-load-more>
                </amp-list>
            </div>
        </div>
    </div>
@endsection