@push('meta')    
        <meta property="og:url" content="{{ url()->current() }}" />
        <meta property="og:title" content="{{ $metaTag->title }}" />
        <meta property="og:description" content="{{ $metaTag->content }}" />
        <meta property="og:image" content="{{ url($metaTag->image) }}" />
        <meta property="fb:app_id" content="952765028218113" />
@endpush
<!-- Stored in resources/views/child.blade.php -->
@extends('layouts.frontend')

@section('title', $metaTag->title)

@section('content')

<div class="container-fluid single-channels">
    <div class="col-sm-12">
        <div class="channels-name">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-2 item-channels-image">
                        <a class="d-block" href="#">
                            <img src="{{ url('/'). Image::url($channels->image,150,150,array('crop')) }}" />
                        </a>
                    </div>
                    <div class="col-sm-5 item-channels-text">
                        <div class="item-channels-text-title">
                            <h4>
                                {{$channels->title}}
                            </h4>
                        </div>
                        <div class="item-channels-text-paragraph">
                            <p>
                                {!!$channels->content!!}
                            </p>
                        </div>
                        <div class="item-channels-text-videos">
                            <label>
                                {{$channels->totalVideo}} Videos | {{$channels->totalSeries}} Series
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="latest-channels-name-videos">
            <div class="col-md-12">
                <div class="title">
                    <h7>
                        LATEST {{ strtoupper($channels->title) }} VIDEOS
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="{{ count($latestChannelsVideo)>=1? 'slider-latest-channels-name-videos':'' }}">
                    @forelse($latestChannelsVideo as $value)
                    <div>
                        <div class="item">
                            <div class="imgTitle">
                                <div class="latest-channels-name-videos-overlay">
                                    <a href="{{ url('/videos/'.$value->slug) }}">
                                        @if(!empty($value->image))
                                            <img class="img-fluid latest-channels-name-videos-overlay" src="{{ url('/'). Image::url($value->image,400,225,array('crop')) }}" />
                                        @else
                                            <img class="img-fluid latest-channels-name-videos-overlay" src="https://img.youtube.com/vi/{{ $value->attr_6}}/mqdefault.jpg" style="width: 400; height: 250;" />
                                        @endif
                                    </a>
                                    @if(!empty($value->attr_7))
                                        <div class="item-latest-channels-name-videos-overlay">
                                            {{ $duration->formatted($value->attr_7) }}
                                        </div>
                                    @else

                                    @endif
                                </div>
                            </div>
                            <p>
                                <a href="{{ url('/videos/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                            </p>
                        </div>
                    </div>
                    @empty
                    <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                          There is no data to display!
                        </div>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
        <div class="popular-channels-name-videos">
            <div class="col-md-12">
                <div class="title">
                    <h7>
                        POPULAR {{ strtoupper($channels->title) }} VIDEOS
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="{{ count($popularChannelsVideo)>=1? 'slider-popular-channels-name-videos':'' }}">
                    @forelse($popularChannelsVideo as $value)
                    <div>
                        <div class="item">
                            <div class="imgTitle">
                                <div class="popular-channels-name-videos-overlay">
                                    <a href="{{ url('/videos/'.$value->slug) }}">
                                        @if(!empty($value->image))
                                            <img class="img-fluid latest-channels-name-videos-overlay" src="{{ url('/'). Image::url($value->image,400,225,array('crop')) }}" />
                                        @else
                                            <img class="img-fluid latest-channels-name-videos-overlay" src="https://img.youtube.com/vi/{{ $value->attr_6}}/mqdefault.jpg" style="width: 400; height: 250;" />
                                        @endif
                                    </a>
                                    @if(!empty($value->attr_7))
                                        <div class="item-latest-channels-name-videos-overlay">
                                            {{ $duration->formatted($value->attr_7) }}
                                        </div>
                                    @else

                                    @endif
                                </div>
                            </div>
                            <p>
                                <a href="{{ url('/videos/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                            </p>
                        </div>
                    </div>
                    @empty
                    <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                          There is no data to display!
                        </div>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
        <div class="latest-channels-name-series">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        LATEST {{ strtoupper($channels->title) }} SERIES
                    </h7>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="{{ count($latestSeries)>=1? 'slider-latest-channels-name-series':'' }}">
                    @forelse($latestSeries as $value)
                    <div>
                        <div class="item">
                            <article class="caption">
                                <img class="caption__media img-fluid" src="{{ url('/'). Image::url($value->image,400,550,array('crop')) }}" />
                                <a href="{{ url('/series/'.$value->slug) }}">
                                    <div class="caption__overlay">
                                        <h1 class="caption__overlay__title">
                                            {{$value->title}}
                                        </h1>
                                        <h7 class="caption__overlay__content">
                                            {{$value->totalVideo}} Videos
                                        </h7>
                                        <p class="caption__overlay__content">
                                            {!!str_limit($value->content, 200)!!}
                                        </p>
                                    </div>
                                </a>
                            </article>
                            <p>
                                <a href="{{ url('/series/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                            </p>
                            <p class="videos">
                                <a href="#">
                                    {{$value->totalVideo}} Videos
                                </a>
                            </p>
                        </div>
                    </div>
                    @empty
                    <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                          There is no data to display!
                        </div>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
        <div class="popular-channels-name-series">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        POPULAR {{ strtoupper($channels->title) }} SERIES
                    </h7>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="{{ count($popularSeries)>=1? 'slider-popular-channels-name-series':'' }}">
                    @forelse($popularSeries as $value)
                    <div>
                        <div class="item">
                            <article class="caption">
                                <img class="caption__media img-fluid" src="{{ url('/'). Image::url($value->image,400,550,array('crop')) }}" />
                                <a href="{{ url('/series/'.$value->slug) }}">
                                    <div class="caption__overlay">
                                        <h1 class="caption__overlay__title">
                                            {{$value->title}}
                                        </h1>
                                        <h7 class="caption__overlay__content">
                                            {{$value->totalVideo}} Videos
                                        </h7>
                                        @if(!empty($value->excerpt))
                                        <p class="caption__overlay__content">
                                            {!!strip_tags($value->excerpt)!!}
                                        </p>
                                        @else
                                        <p class="caption__overlay__content">
                                            {!!strip_tags(str_limit($value->content, 100))!!}
                                        </p>
                                        @endif
                                    </div>
                                </a>
                            </article>
                            <p>
                                <a href="{{ url('/series/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                            </p>
                            <p class="videos">
                                <a href="#">
                                    {{$value->totalVideo}} Videos
                                </a>
                            </p>
                        </div>
                    </div>
                    @empty
                    <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                          There is no data to display!
                        </div>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid others-videos">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        OTHERS VIDEOS
                    </h7>
                </div>
            </div>
            <div class="col-md-12">
                <div class="{{ count($otherVideos)>=1? 'row text-center text-lg-left item-others-videos':'' }}">
                    @forelse($otherVideos as $value)
                    <div class="col-sm-3 col-others-videos">
                        <div class="item">
                            <div class="imgTitle">
                                <div class="top-videos-overlay">
                                    <a href="{{ url('/videos/'.$value->slug) }}">
                                        @if(!empty($value->image))
                                            <img class="img-fluid top-videos-overlay" src="{{ url('/'). Image::url($value->image,400,225,array('crop')) }}" />
                                        @else
                                            <img class="img-fluid top-videos-overlay" src="https://img.youtube.com/vi/{{ $value->attr_6}}/mqdefault.jpg" style="width: 400; height: 250;" />
                                        @endif
                                    </a>
                                    @if(!empty($value->attr_7))
                                        <div class="item-top-videos-overlay">
                                            {{ $duration->formatted($value->attr_7) }}
                                        </div>
                                    @else

                                    @endif
                                </div>
                            </div>
                            <p>
                                <a href="{{ url('/videos/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                            </p>
                        </div>
                    </div>
                    @empty
                    <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                          There is no data to display!
                        </div>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid more-from-channels">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        MORE FROM CHANNELS
                    </h7>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="{{ count($moreVideos)>=1? 'row text-center text-lg-left item-more-from-channels loadMore':'' }}">
                    @forelse($moreVideos as $value)
                    <div class="col-sm-3 col-more-from-channels">
                        <div class="item">
                            <div class="imgTitle">
                                <div class="top-videos-overlay">
                                    <a href="{{ url('/videos/'.$value->slug) }}">
                                        @if(!empty($value->image))
                                            <img class="img-fluid top-videos-overlay" src="{{ url('/'). Image::url($value->image,400,225,array('crop')) }}" />
                                        @else
                                            <img class="img-fluid top-videos-overlay" src="https://img.youtube.com/vi/{{ $value->attr_6}}/mqdefault.jpg" style="width: 400; height: 250;" />
                                        @endif
                                    </a>
                                    @if(!empty($value->attr_7))
                                        <div class="item-top-videos-overlay">
                                            {{ $duration->formatted($value->attr_7) }}
                                        </div>
                                    @else

                                    @endif
                                </div>
                            </div>
                            <p>
                                <a href="{{ url('/videos/'.$value->slug) }}">
                                    {{$value->title}}
                                </a>
                            </p>
                        </div>
                    </div>
                    @empty
                    <div class="col-sm-12">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                          There is no data to display!
                        </div>
                    </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>

@endsection