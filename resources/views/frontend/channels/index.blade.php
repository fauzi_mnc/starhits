@extends('layouts.frontend')

@section('title', 'Channels') 

@section('content')
<div class="col-content">
    <div style="width: 34vw;height:62vh;position: absolute;top:360px;">
        <amp-img src="{{asset('frontend/assets/img/background/bg-circle.png')}}" width="473" height="474" layout="responsive"
            style="transform: rotate(45deg)">
        </amp-img>
    </div>

    <div style="width: 34vw;height:62vh;position: absolute;top:132px;right:0">
        <amp-img src="{{asset('frontend/assets/img/background/bg-circle.png')}}" width="473" height="474" layout="responsive"
            style="transform: rotate(45deg)">
        </amp-img>
    </div>

    <div style="width: 34vw;height:62vh;position: absolute;bottom:200px">
        <amp-img src="{{asset('frontend/assets/img/background/bg-circle.png')}}" width="473" height="474" layout="responsive"
            style="transform: rotate(45deg)">
        </amp-img>
    </div>

    <div class="profile">
        <div class="profile__container">
            <div class="profile__picture">
                <amp-img src="{{asset('frontend/assets/img/background/channel_bg_1.jpg')}}" width="363" height="567" layout="responsive">
                </amp-img>
            </div>
            <div class="profile__picture">
                <amp-img src="{{asset('frontend/assets/img/background/channel_bg_2.jpg')}}" width="363" height="567" layout="responsive">
                </amp-img>
            </div>
            <div class="profile__picture">
                <amp-img src="{{asset('frontend/assets/img/background/channel_bg_3.jpg')}}" width="363" height="567" layout="responsive">
                </amp-img>
            </div>
        </div>

        <div class="profile__container">
            <div class="page-title">
                <h1 class="text-outline font-style-i font-weight-900 font-size-xl">OUR</h1>
                <h1 class="font-style-i font-weight-900 font-size-xl">CHANNEL</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 p-0">
            <amp-selector id="channelselector" class="tabnavs d-block text-center text-nowrap" role="tablist" on="select:myTabPanels.toggle(index=event.targetOption, value=true)">
                @foreach ($channels as $item)
                    <div class="d-inline-block px-4" id="{{ strtolower($item->slug) }}Tab" role="tab" aria-controls="{{ strtolower($item->slug) }}Panel" option="{{ $item->sorting }}" 
                    @if(Request::query('f')=='starpro')
                    {{ ($item->sorting == 2) ? 'selected' : ''}}
                    @else
                    {{ ($item->sorting == 0) ? 'selected' : ''}}
                    @endif
                    >
                        {{ ucwords($item->title) }}
                    </div>
                @endforeach
            </amp-selector>
        </div>
        <div class="col-12">
            <amp-selector id="myTabPanels" class="tabpanels">
                @foreach ($channels as $item)
                    <div id="{{ strtolower($item->slug) }}Panel" aria-labelledby="{{ strtolower($item->slug) }}Tab" role="tabpanel" option 
                    @if(Request::query('f')=='starpro')
                    {{ ($item->sorting == 2) ? 'selected' : ''}}
                    @else
                    {{ ($item->sorting == 0) ? 'selected' : ''}}
                    @endif
                    >
                        <div class="container mx-auto py-5">
                            <div class="row">
                                <div class="col-md-3 pb-3">
                                    <amp-img class="border border-dark" layout="responsive" width="360"
                                    height="360" src="{{ $item->image }}"></amp-img>
                                </div>

                                <div class="col-md-9">
                                    <h2 class="font-weight-900">{{ ucwords($item->title) }}</h2>
                                    <p>{{ $item->content }}</p>
                                    <p class="detailpanels">
                                        <b>
                                            {{ $item->totalvideo }} Videos | {{ $item->totalseries }} Series
                                        </b>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="row bg-orange pt-5">
                            @if(empty(json_decode($item)))
                            <div class="col-sm-10 mx-auto d-block my-5">
                                <h1 class="mb-5">
                                    <span class="text-left text-outline font-size-md font-italic" style="font-size: 5vw;">VIDEOS</span>
                                    <span class="text-center font-size-sm font-italic">{{ strtoupper($item->title) }}</span>
                                </h1>
                                <br>
                                    <div class="font-size-sm text-center">
                                        There is no data to display!
                                    </div>
                                <br>
                            </div>
                            @else
                            <div class="col-md-11 mx-auto">
                                <div class="col-xl-12 mx-auto">
                                    <h1 class="mb-5">
                                        <span class="text-left text-outline font-size-md font-italic" style="font-size: 5vw;">VIDEOS</span>
                                        <span class="text-center font-size-sm font-italic">{{ strtoupper($item->title) }}</span>
                                    </h1>
                                </div>

                                <div class="latest-{{ strtolower($item->slug) }}-video" style="position: relative">
                                    <amp-list id="ampchannels" class="list" reset-on-refresh
                                        layout="fixed-height" height="540"
                                        src="https://starhits.id/api/channel-latest-video/{{ strtolower($item->slug) }}"
                                        binding="refresh" load-more="manual" load-more-bookmark="next_page_url">
                                        <template type="amp-mustache">
                                            <amp-img src="https://i3.ytimg.com/vi/@{{attr_6}}/hqdefault.jpg" width="376" height="246"></amp-img>
                                            <div class="preview-popup">
                                                <a class="preview-popup-link" href="{{ url('/videos/') }}/@{{slug}}">Watch
                                                    video</a>
                                            </div>
                                            <div class="caption">
                                                <amp-fit-text layout="fixed-height" height="40"
                                                    max-font-size="14" style="font-weight: 550;">
                                                    @{{subtitle}}</amp-fit-text>
                                                <div class="title">By @{{users.name}}</div>
                                            </div>
                                        </template>
                                        <div fallback>FALLBACK</div>
                                        <div placeholder>LOADING...</div>
                                        <amp-list-load-more load-more-failed>ERROR</amp-list-load-more>
                                        <amp-list-load-more load-more-end></amp-list-load-more>
                                        <amp-list-load-more load-more-button>
                                            <div class="amp-load-more">
                                                <button class="load-more-link">LOAD
                                                        MORE</button>
                                            </div>
                                        </amp-list-load-more>
                                    </amp-list>
                                </div>
                            </div>
                            @endif
                        </div>

                        <div class="row pt-5">
                            @if(empty(json_decode($item->series)))
                            <div class="col-sm-10 mx-auto d-block my-5">
                                <h1 class="mb-5">
                                    <span class="text-left text-outline-black font-size-md font-italic" style="font-size: 5vw;">SERIES</span>
                                    <span class="text-center font-size-sm font-italic">{{ strtoupper($item->title) }}</span>
                                </h1>
                                <br>
                                    <div class="font-size-sm text-center">
                                        There is no data to display!
                                    </div>
                                <br>
                            </div>
                            @else
                            <div class="col-md-11 mx-auto">
                                <h1 class="mb-5">
                                    <span class="text-left text-outline-black font-size-md font-italic" style="font-size: 5vw;">SERIES</span>
                                    <span class="text-center font-size-sm font-italic">{{ strtoupper($item->title) }}</span>
                                </h1>

                                <div class="latest-{{ strtolower($item->slug) }}-video" style="position: relative">
                                    <amp-list id="ampserieschannels" class="list" reset-on-refresh
                                        layout="fixed-height" height="564"
                                        src="https://starhits.id/api/channel-latest-series/{{ strtolower($item->slug) }}"
                                        binding="refresh" load-more="manual" load-more-bookmark="next_page_url">
                                        <template type="amp-mustache">
                                            <amp-img src="@{{image}}" width="300" height="429"></amp-img>
                                            <div class="preview-popup">
                                                <div class="preview-popup-description">
                                                    <small>@{{totalVideo}} Videos</small>
                                                    <p class="subtitle">
                                                        <strong>
                                                            @{{title}}
                                                        </strong>
                                                    </p>
                                                    <amp-fit-text layout="fixed-height" height="150"
                                                        max-font-size="12" style="font-weight: 300">
                                                        @{{content}}</amp-fit-text>
                                                </div>
                                                <a class="preview-popup-link" href="{{ url('/series/') }}/@{{slug}}">watch
                                                    video</a>
                                            </div>
                                            <div class="caption">
                                                <h6>@{{title}}</h6>
                                                <p><small>@{{totalVideo}} Videos</small></p>
                                            </div>
                                        </template>
                                        <div fallback>FALLBACK</div>
                                        <div placeholder>LOADING...</div>
                                        <amp-list-load-more load-more-failed>ERROR</amp-list-load-more>
                                        <amp-list-load-more load-more-end></amp-list-load-more>
                                        <amp-list-load-more load-more-button>
                                            <div class="amp-load-more">
                                                <button class="load-more-link">LOAD
                                                        MORE</button>
                                            </div>
                                        </amp-list-load-more>
                                    </amp-list>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            </amp-selector>
        </div>
    </div>
</div>
@endsection