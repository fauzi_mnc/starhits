<!-- Stored in resources/views/child.blade.php -->
@extends('layouts.frontend')

@section('title', 'Grow with us')

@section('content')

<div class="container-fluid join">
    <div class="row">
        <div class="content-text">
            <div class="col-md-12">
                <div class="top-half">
                    <button class="join">
                        GROW WITH US
                    </button>
                    <h5 class="top-title">
                        Apply For Partnership
                    </h5>
                    <div class="top-description">
                        <p class="description-text">
                            To begin with, we need to connect to your YouTube account.
                        </p>
                        <p class="description-text">
                            Your data & privacy is our prime priority. We will collect the following data only
                        </p>
                    </div>
                    <div class="icons">
                        <div class="row">
                            @foreach($partnerShip as $value)
                            <div class="col-md-4">
                                <img class="icon1" src="{{ url('/'). Image::url($value->image,98,98,array('crop')) }}">
                                    <p class="icon1-description">
                                        {{$value->title}}
                                    </p>
                                </img>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @guest
                    <a href="{{url('auth/youtube')}}">
                        <button class="btn authenticate">
                            AUTHENTICATE YOUR CHANNEL
                        </button>
                    </a>
                    @endguest
                </div>
            </div>
            <div class="container-fluid join-recommended">
                <div class="title">
                    <h7>
                        SOME OF OUR CREATORS
                    </h7>
                </div>
                <div class="col-md-12">
                    <div class="{{ count($someCreators)>=1? 'row text-center text-lg-left item-others-videos':'' }}">
                        @forelse($someCreators as $value)
                        <div class="col-sm-3 col-others-videos">
                            <div class="item">
                                <div class="imgTitle">
                                    <div class="top-videos-overlay">
                                        <a href="{{ url('/creators/'.$value->slug) }}">
                                            @if(!empty($value->cover))
                                                <img alt="" class="img-fluid top-videos-overlay" src="{{ url('/'). Image::url($value->cover,400,250,array('crop')) }}"/>
                                            @else
                                                <img class="img-fluid top-videos-overlay" src="{{ url('/'). Image::url('frontend/assets/img/icon/default.png',400,250,array('crop')) }}"/>
                                            @endif
                                        </a>
                                    </div>
                                </div>
                                <p>
                                    <a href="{{ url('/creators/'.$value->slug) }}">
                                        {{$value->name}}
                                    </a>
                                </p>
                                <label>
                                    <a href="#">
                                        {{$value->totalVideos}} Videos
                                    </a>
                                </label>
                            </div>
                        </div>
                        @empty
                        <div class="col-sm-12">
                            <br>
                                <div class="alert alert-warning text-center" role="alert">
                                    There is no data to display!
                                </div>
                            </br>
                        </div>
                        @endforelse
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="bottom-half">
                        <div class="bottom-info">
                            <h5 class="bottom-title">
                                Why You Should Join Us?
                            </h5>
                            <p class="bottom-description">
                                To begin with, we need to connect to your YouTube account.
                                <br>
                                    Your data & privacy is our prime priority. We will collect the following data only
                                </br>
                            </p>
                            <div class="icons">
                                <div class="row">
                                    <div class="col">
                                    </div>
                                    <div class="col-md-7">
                                        <div class="row">
                                            @foreach($joinUs as $value)
                                            <div class="col-md-4 item-icons">
                                                <div class="left-square">
                                                    <img alt="" class="img-fluid" src="{{ url('/'). Image::url($value->image,165,165,array('crop')) }}">
                                                    </img>
                                                </div>
                                                <p class="square-description">
                                                    {{$value->title}}
                                                </p>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection