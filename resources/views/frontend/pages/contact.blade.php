@extends('layouts.frontend')

@section('title', 'Contact') 

@section('content')
    <div class="bg-orange-contact"></div>
    <div class="col-content">
        <div style="width: 1029px;height:672px;position: absolute;top:0;right:0">
            <amp-img src="{{asset('frontend/assets/images/bg-contact.png')}}" width="1029" height="672" layout="responsive">
            </amp-img>
        </div>

        <div id="section-title-contact">
            <amp-img src="{{asset('frontend/assets/img/contact_bg.jpg')}}" width="864" height="576" layout="responsive"
                style="margin-left: 33.3333333%"></amp-img>
            <div id="page-title-contact">
                <h3><i>LETS MAKE THINGS TOGETHER BY</i></h3>
                <h1 class="font-size-xl font-weight-900 font-style-i text-outline m-0 p-0">CONTACT</h1>
                <h1 class="font-size-xl font-weight-900 font-style-i m-0 p-0">OUR PEOPLE</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                {{-- <amp-img src="{{asset('frontend/assets/img/our_office.jpg')}}" width="618" height="488" layout="responsive">
                </amp-img> --}}
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.5209928147037!2d106.76508091476903!3d-6.194776995515346!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f73614f75133%3A0xb9ed9e08c264e19a!2sStar%20Hits!5e0!3m2!1sen!2sid!4v1570507085869!5m2!1sen!2sid" id="maps" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>

            <div class="col-md-6 d-xl-flex flex-xl-column justify-content-xl-center pl-xl-5">
                <h1>
                    <span class="text-outline">OUR</span>&nbsp;<span>OFFICE</span>
                </h1>

                <div class="row" style="margin-top: 32px">
                    <div class="col-md-1 col-2">
                        <amp-img src="frontend/assets/icon/icon-building.png" width="38" height="34"></amp-img>
                    </div>

                    <div class="col-md-11 col-10">
                        <p>
                            MNC STUDIOS Tower II Lt. 2
                        </p>
                        <p>
                            Jl. Raya Perjuangan no.1, Kebon Jeruk,
                        </p>
                        <p>
                            Jakarta Barat 11530, Indonesia
                        </p>
                    </div>
                </div>

                <div class="row" style="margin-top: 32px">
                    <div class="col-md-1 col-2">
                        <amp-img src="frontend/assets/icon/icon-phone.png" width="29" height="29"></amp-img>
                    </div>

                    <div class="col-md-11 col-10">
                        <p>
                            (+62)21 5366-1736
                        </p>
                    </div>
                </div>

                <div class="row" style="margin-top: 32px">
                    <div class="col-md-1 col-2">
                        <amp-img src="frontend/assets/icon/icon-mail.png" width="30" height="20"></amp-img>
                    </div>

                    <div class="col-md-11 col-10">
                        <p>
                            starhits@mncgroup.com
                        </p>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-11 text-center mx-auto my-5">
                <h1 class="bg-orange py-4">
                    <span class="text-outline">DROP US</span> A LINE</h1>
                <p class="my-5">Always open to meet new people, so give us a call or visit us at work.</p>
                <div class="row" id="formContact">
                    <div class="col-md-10 text-center mx-auto mt-5">
                        <form method="POST" action="{{route('contact')}}#formContact" target="_top">
                        {{ csrf_field() }}
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <input class="form-control form-control-lg rounded-0" type="text" name="name" placeholder="Full Name" required>
                                    </div>
                                    <div class="form-group" style="margin-bottom: 0">
                                        <input class="form-control form-control-lg rounded-0" type="email"
                                            name="email" placeholder="Mail Address" required>
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <div class="form-group h-100">
                                        <textarea class="form-control form-control-lg rounded-0 h-100"
                                            name="notes" placeholder="Notes" required></textarea>
                                    </div>
                                </div>
                            </div>
                            @if(env('APP_ENV') != 'prod')
                            <div class="text-center mt-2 {{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                {!! NoCaptcha::display() !!}
                                @if ($errors->has('g-recaptcha-response'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>
                                @endif
                            </div>
                            @endif
                            <div class="text-center">
                                <input class="btn btn-lg btn-warning px-5 mt-3 font-weight-bold rounded-0"
                                    type="submit" value="SEND" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection