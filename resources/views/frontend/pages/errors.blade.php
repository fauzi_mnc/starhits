@extends('layouts.frontend')

@section('title', 'Not Found') 

@section('content')
    <div class="container not-found">
        <div class="row">
            <div class="col-md-12 text-center my-5">
                <label>
                    This is somewhat embarrasing, isn't it?
                    <br>
                        <small>
                            Sorry, we couldn't find what you were looking for. But, please enjoy these contents from our site.
                        </small>
                    </br>
                </label>
                <h1>404</h1>
            </div>
        </div>
    </div>

    <div id="orange-box-404">
            <h1 class="text-center font-size-md p-3 font-weight-700"><span class="text-outline">RECOMMENDED</span> VIDEO</h1>
    
            <div class="row">
            @if($recomendedVideos=='')
                <div class="col-sm-10 mx-auto">
                    <br>
                        <div class="alert alert-warning text-center" role="alert">
                            There is no data to display!
                        </div>
                    <br>
                </div>
            @else
                <div class="col-xl-10 mx-auto">
                    <amp-carousel id="recommendedPage404" height="450" layout="fixed-height">
                        @foreach($recomendedVideos as $items)
                        <div class="slide mx-3" style="padding: 10px">
                            <div class="card" style="position: relative">
                                @if(!empty($items->image))
                                <amp-img src="{{ url('/'). Image::url($items->image,400,225,array('crop')) }}" width="345" height="194"></amp-img>
                                @else
                                <amp-img src="https://img.youtube.com/vi/{{ $items->attr_6}}/mqdefault.jpg" width="345" height="194"></amp-img>
                                @endif
                                <div class="card-img-overlay">
                                    <a class="view-videos-link-404" href="{{ url('/videos/'.$items->slug) }}">view video</a>
                                </div>
                            </div>
                            <div class="caption-404">
                                <h6>{{ substr($items->title, 0, 20)}}</h6>
                                <p class="small">{{str_replace('Subtitle','',$items->subtitle)}}</p>
                            </div>
                        </div>
                        @endforeach
                    </amp-carousel>
                </div>
            </div>
            @endif
            <div class="bg-circle" style="width:34vw;height:62vh;position: absolute;left: -5vw;">
                <amp-img src="{{asset('frontend/assets/images/bg-circle.png')}}" width="473" height="474" layout="responsive"
                    style="transform: rotate(279deg)">
                </amp-img>
            </div>
        </div>

    
    <h1 class="text-center font-size-md py-5 font-weight-700"><span class="text-outline-black">RECOMMENDED</span> SERIES</h1>

    <div class="row">
        <div class="col-xl-10 mx-auto" style="margin-bottom: 5em">

            <amp-carousel id="latestSeriesPage404" width="auto" height="584" layout="fixed-height"
                type="carousel">
                @foreach($recomendedSeries as $items)
                <div class="card">
                    <amp-img class="card-img" src="{{ url('/').$items->image }}" width="350" height="584">
                    </amp-img>

                    <div class="card-img-overlay">
                        <p>{{$items->totalVideo}} Videos</p>
                        <h6>{{$items->title}}</h6>
                        <amp-fit-text height="200" layout="fixed-height" max-font-size="14">
                            {{strip_tags($items->excerpt)}}
                        </amp-fit-text>
                        <a class="view-videos-link-404" href="{{ url('/series/'.$items->slug) }}">view videos</a>
                    </div>
                </div>
                @endforeach
            </amp-carousel>
        </div>
    </div>
@endsection