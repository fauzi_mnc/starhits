@extends('layouts.frontend')

@section('title', 'Multi Channel Network') 

@section('content')

    <div class="row my-5">
        <div class="col-xl-10 mx-auto">
            <div class="row" id="whatmcn">
                <div class="col-md-7 col-md-5" id="core-business">
                    <h1 class="mb-3" style="font-weight: 700;">WHAT IS<br><span class="text-outline"  style="font-weight: 600;">MCN</span><br>(Multi Channel Network)?</h1>

                    {{-- <amp-accordion id="core-business-list" expand-single-section animate>
                        <section expanded>
                            <h4 style="font-weight: 600;">Multi Channel Network</h4>
                            <p>
                                We aim to effectively distribute content across media platforms to boost audience reach, monetization, and/or sales.
                            </p>
                        </section>
                        <section>
                            <h4 style="font-weight: 600;">Influencer Management</h4>
                            <p>
                                As a management agency for influencers and content creators, we help managing well-planned campaign and assistance to launch campaigns for brands and marketers.
                            </p>
                        </section>
                        <section>
                            <h4 style="font-weight: 600;">Creative Digital Campaign</h4>
                            <p>
                                We maximize the presence of every content we produce using digital-based marketing plan comprehensively by utilizing digital ads, articles, videos, creators, and influencers.
                            </p>
                        </section>
                        <section>
                            <h4 style="font-weight: 600;">Production Service</h4>
                            <p>
                                We have our in-house team and latest facilities to create effective TVC advertisement, web series, short video, and design assets for our clients.
                            </p>
                        </section>
                    </amp-accordion> --}}
                </div>
                <div class="col-md-5" style="position:relative;">
                    <p>
                        MCN is an official partner of youtube that works with video platforms to offer assistance to a channel owner in area such as: content development, cross promotion, digital rights management, monetisation / sales and audience development
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div id="orange-box">
        <h1 class="font-size-md font-weight-900 text-center my-5 who page-title">WHY <span class="text-outline">JOIN US?</span></h1>
        <div class="row px-3 text-center mb-5 text-black">
            <div class="col-md-4 col-sm-6 text-center mt-5">
                <amp-img id="img-join" src="{{asset('frontend/assets/img/icon/icon-fair-revenue-share-rev.png')}}" width="100" height="100"layout="responsive"></amp-img>
                <h4 class="title-joinus text-black">Fair Revenue Share</h4>
                <p class="text-black">Revenue Share up to 90% for creator</p>
            </div>
            <div class="col-md-4 col-sm-6 text-center mt-5">
                <amp-img id="img-join" src="{{asset('frontend/assets/img/icon/icon-on-time-payment-rev.png')}}" width="100" height="100"layout="responsive"></amp-img>
                <h4 class="title-joinus">On Time Payment</h4>
                <p>Revenue sharing will be paid after 2 weeks</p>
            </div>
            <div class="col-md-4 col-sm-6 text-center mt-5">
                <amp-img id="img-join" src="{{asset('frontend/assets/img/icon/icon-tools-rev.png')}}" width="100" height="100"layout="responsive"></amp-img>
                <h4 class="title-joinus">Advance Tools</h4>
                <p>PR on our Fanpage system with millions of fans and visits</p>
            </div>
            <div class="col-md-4 col-sm-6 text-center mt-5">
                <amp-img id="img-join" src="{{asset('frontend/assets/img/icon/icon-music-bank-rev.png')}}" width="100" height="100"layout="responsive"></amp-img>
                <h4 class="title-joinus">Music Bank & sound effect Library</h4>
                <p>Large selection of music and sound effect for videos</p>
            </div>
            <div class="col-md-4 col-sm-6 text-center mt-5">
                <amp-img id="img-join" src="{{asset('frontend/assets/img/icon/icon-promotion-rev.png')}}" width="100" height="100"layout="responsive"></amp-img>
                <h4 class="title-joinus">Promotion</h4>
                <p>Integrated marketing and promotion support</p>
            </div>
            <div class="col-md-4 col-sm-6 text-center mt-5">
                <amp-img id="img-join" src="{{asset('frontend/assets/img/icon/icon-brand-deals-rev.png')}}" width="100" height="100"layout="responsive"></amp-img>
                <h4 class="title-joinus">Brand Deal</h4>
                <p>Brand approach and collaboration with MNC Integrated Sales</p>
            </div>
            <div class="col-md-4 col-sm-6 text-center mt-5">
                <amp-img id="img-join" src="{{asset('frontend/assets/img/icon/icon-high-revenue-rev.png')}}" width="100" height="100"layout="responsive"></amp-img>
                <h4 class="title-joinus">High Revenue</h4>
                <p>Creator get multiple youtube ads for used on video</p>
            </div>
            <div class="col-md-4 col-sm-6 text-center mt-5">
                <amp-img id="img-join" src="{{asset('frontend/assets/img/icon/icon-training-rev.png')}}" width="100" height="100"layout="responsive"></amp-img>
                <h4 class="title-joinus">Free Training</h4>
                <p>Creators will get training on the latest products from YouTube</p>
            </div>
            <div class="col-md-4 col-sm-6 text-center mt-5">
                <amp-img id="img-join" src="{{asset('frontend/assets/img/icon/icon-production-facilities-rev.png')}}" width="100" height="100"layout="responsive"></amp-img>
                <h4 class="title-joinus">Production Facilities</h4>
                <p>Free use of the studios and properties</p>
            </div>
        </div>
    </div>
    <div class="row my-5">
        <div class="col-xl-10 mx-auto">
            <h1 class="text-center" style="font-weight: 700;">STARHIT's <span class="text-outline-black"  style="font-weight: 700;">CONTENT ID</span></h1>
            <div class="row" id="whatmcn">
                <div class="col-md-5 col-md-5" id="core-business">
                    <h1>Video</h1>
                    If you upload a video on YouTube, and find your video modified, you can control your video, imagine if someone uses your video without your permission. 
                    <h1>Composition Song</h1>
                    When you create a song, we can monitor who re-arranges your song, becomes a midi song, karaoke version & anyone who changes the composition of your song from the original version.
                    <h1>Sound Recording</h1>
                    if you officially release a song, and your song is used as a back song, lyric video, or video compilations , you will be able to control the song.
                </div>
                <div class="col-md-7" style="position:relative;">
                    <amp-youtube id="myLiveChannel" data-videoid="9g2U12SsRns" width="358" height="204" layout="responsive">
                        <amp-img src="https://i.ytimg.com/vi/9g2U12SsRns/maxresdefault_live.jpg" placeholder layout="fill"/>
                    </amp-youtube>
                    <p class="text-center small">ONE OF THE BIGGEST BENEFITS OF JOINING A MCN IS RECEIVING COPYRIGHT CLAIM PROTECTION.</p>
                </div>
            </div>
        </div>
    </div>
@endsection