@extends('layouts.frontend')

@section('title', 'FAQ') 

@section('content')
    <div class="col-content">
        <div style="padding:0;margin:0 -15px;position:relative">
            <amp-img src="{{('frontend/assets/img/header.jpg')}}" width="1278" height="353" layout="responsive">
            </amp-img>

            <div id="page-title-faq">
                <h1 class="font-style-i display-2 font-weight-900 text-center">
                    FAQ
                </h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-9 mx-auto py-5 mb-5">
                <amp-accordion id="faqs-list" expand-single-section animate>
                    <section expanded>
                        <h2 class="faq__header">What does Star Hits do?</h1>
                        <p class="faq__answer">
                            Star Hits management focused on monetization of content on YouTube. We deliver maximum value for your youtube catalogue through a combination of platform expertise and proprietary star hits tools. and we can help creaetor for production to regular video youtube content.
                        </p>
                    </section>
                    <section>
                        <h2 class="faq__header">Why work with Star Hits?</h1>
                        <p class="faq__answer">
                            Star Hits has expertise in all aspects of YouTube's many moving parts, including the Content ID system and new feature for ads from youtube. We can handle everything from reference file creation, geoblocking and conflict managment to upload scheduling, tagging/titling strategies, analityc and playlist management.
                        </p>
                    </section>
                    <section>
                        <h2 class="faq__header">What do Star Hits services cost?</h1>
                        <p class="faq__answer">
                            We usually work on a revenue share basis. This means that you don't have to find a budget for our services.
                        </p>
                    </section>
                    <section>
                        <h2 class="faq__header">We or someone else manage(s) our YouTube channel, can we still work with you?</h1>
                        <p class="faq__answer">
                            Absolutely! One of the things we pride ourselves on is finding a way to add value to your current processes. Working on a revenue share and with a license to content gives us maximum flexibility to serve our clients in unique ways.
                        </p>
                    </section>
                </amp-accordion>
            </div>
        </div>
    </div>
@endsection