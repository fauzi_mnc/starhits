@extends('layouts.frontend')

@section('title', 'About') 

@section('content')
    <div class="d-flex justify-content-between"
        style="position: absolute;top: 0;right: 15Px;left: 15px">
        <div>&nbsp;</div>
        <div style="width: 34.63vw;height:34.63vw">
            <amp-img src="{{asset('frontend/assets/img/background/bg-circle.png')}}" width="473" height="474" layout="responsive"
                style="transform: rotate(45deg)">
            </amp-img>
        </div>
        <div style="width:50px;height:92px">
            <amp-img src="{{asset('frontend/assets/img/background/bg-triangle.png')}}" width="50" height="92">
            </amp-img>
        </div>
        </div>

        <div class="row">
        <div class="col-6" style="padding:0;">
            <amp-img src="{{asset('frontend/assets/img/about_us.png')}}" width="744" height="449" layout="responsive">
            </amp-img>
        </div>
        <div class="col-6 d-flex align-items-center">
            <div class="col-xl-10 page-title" style="margin: auto">
                <div class="font-style-i font-size-xl font-weight-900 text-outline-black">
                    ABOUT
                </div>
                <div class="font-style-i font-size-xl font-weight-900">
                    US
                </div>
            </div>
        </div>
    </div>

    <div id="orange-box">
        <div class="container">
            <h1 class="font-size-md who font-weight-900 mb-3 text-center">WHO WE ARE</h1>
            <div class="row">
                <div style="width: 34.63vw;height:34.63vw;position:absolute;left:-11rem;margin-top: 10rem;">
                    <amp-img src="{{asset('frontend/assets/img/background/bg-circle.png')}}" width="473" height="474" layout="responsive"
                        style="transform: rotate(45deg)">
                    </amp-img>
                </div>
                <div class="col-lg-6 col-md-12 mobile">
                    {{-- <p class="text-white text-justify">
                        Star Hits is Digital media company that provides services, in terms of creating, distributing, managing, monetization of content.
                        <br><br>
                        As a content creation agency, Star Hits enables content creators to efficiently distribute their multimedia content across the most popular video streaming and social media platforms to their local and global audience. And they can do so with minimum effort, so they can focus on bigger ideas. As a partner, we provide content producers, Multi-Channel Network (MCN), brands, and media agencies to gain the exposure they need. We do so by executing content production, managing video distribution, providing comprehensive insights from every available source, and helping to find influencers that suit your needs and negotiate them assisted by our certified Account Strategist.
                    </p> --}}
                    <p class="text-white text-justify">
                        Star Hits is Digital media company that provides services, in terms of creating, distributing, managing, monetization of content.
                        <br><br>
                        We do so by executing content production, managing video distribution, providing comprehensive insights from every available source, and helping to find influencers that suit your needs and negotiate them assisted by our certified Account Strategist.
                    </p>
                </div>
                <div class="col-lg-6 col-md-12 mobile">
                    <amp-img src="{{asset('frontend/assets/img/whatwedo.png')}}"" height="320" width="308" layout="responsive">
                    </amp-img>
                </div>
                <div class="col-lg-6 col-md-12 desktop">
                    <p class="text-white text-justify" id="align-center">
                        Star Hits is Digital media company that provides services, in terms of creating, distributing, managing, monetization of content.
                        <br><br>
                        We do so by executing content production, managing video distribution, providing comprehensive insights from every available source, and helping to find influencers that suit your needs and negotiate them assisted by our certified Account Strategist.
                    </p>
                    {{-- <p class="text-white text-justify">
                        We are bound to do things differently. By combining expertise in technology, networking, and creative content-making, we help influencers to be seen and brands recognized. Over the past few years, many people are dubbed as influencers or key opinion leaders on various media platform, connecting people with brands and vice versa. However, the process behind content creation starting from concept to execution and distribution is a rough one. With our service, we are aiming to change the way content is produced and distributed to maximize the audience reach.
                    </p>
                    <p class="text-white text-justify">
                        As a content creation agency, Star Hits enables content creators to efficiently distribute their multimedia content across the most popular video streaming and social media platforms to their local and global audience. And they can do so with minimum effort, so they can focus on bigger ideas. As a partner, we provide content producers, Multi-Channel Network (MCN), brands, and media agencies to gain the exposure they need. We do so by executing content production, managing video distribution, providing comprehensive insights from every available source, and helping to find influencers that suit your needs and negotiate them assisted by our certified Account Strategist.
                    </p> --}}
                </div>
                <div class="col-lg-6 col-md-12 desktop">
                    <div id="parentdiv" class="text-center">
                        <div id="parentdiv2">
                            <h2>What we do?</h2>
                            <p>Creative Digital Campaign</p>
                            <div class="div2" style="position: absolute; top: 170px; left: -65px;">
                                <div class="panah1"></div>
                                <h3 class="ps">Production Service</h3>
                                <p>Video & Design</p>
                            </div>
                            <div class="div2" style="position: absolute; top: -104px; left: 95px;">
                                <div class="panah3"></div>
                                <h3 class="mcn">Multi Channel Network</h3>
                                <p>Creators & Influencers</p>
                            </div>
                            <div class="div2" style="position: absolute; top: 170px; left: 260px;">
                                <div class="panah2"></div>
                                <h3 class="bd">Brand Deals</h3>
                                <p>Sales Force</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xl-10 mx-auto my-5">
            <div class="row">
                <div class="col-xl-5 col-md-5" id="core-business">
                    <h1 class="mb-3" style="font-weight: 700;"><span class="text-outline"  style="font-weight: 600;">CORE</span> BUSINESS</h1>

                    {{-- <amp-accordion id="core-business-list" expand-single-section animate>
                        <section expanded>
                            <h4 style="font-weight: 600;">Multi Channel Network</h4>
                            <p>
                                We aim to effectively distribute content across media platforms to boost audience reach, monetization, and/or sales.
                            </p>
                        </section>
                        <section>
                            <h4 style="font-weight: 600;">Influencer Management</h4>
                            <p>
                                As a management agency for influencers and content creators, we help managing well-planned campaign and assistance to launch campaigns for brands and marketers.
                            </p>
                        </section>
                        <section>
                            <h4 style="font-weight: 600;">Creative Digital Campaign</h4>
                            <p>
                                We maximize the presence of every content we produce using digital-based marketing plan comprehensively by utilizing digital ads, articles, videos, creators, and influencers.
                            </p>
                        </section>
                        <section>
                            <h4 style="font-weight: 600;">Production Service</h4>
                            <p>
                                We have our in-house team and latest facilities to create effective TVC advertisement, web series, short video, and design assets for our clients.
                            </p>
                        </section>
                    </amp-accordion> --}}

                    <ol class="text-left cbusines">
                        <li class="mb-3" style="font-size:1.5rem;font-weight:bold;border-bottom:1px solid #000000;"><a href="{{ route('mcn') }}"><h4 style="font-weight: 600;">Multi Channel Network</h4></a></li>
                        <li class="mb-3" style="font-size:1.5rem;font-weight:bold;border-bottom:1px solid #000000;"><a href="{{ route('creators') }}#navi"><h4 style="font-weight: 600;">Influencer Management</h4></a></li>
                        <li class="mb-3" style="font-size:1.5rem;font-weight:bold;border-bottom:1px solid #000000;"><a href="{{ route('portfolio') }}?s=digital"><h4 style="font-weight: 600;">Creative Digital Campaign</h4></a></li>
                        <li class="mb-3" style="font-size:1.5rem;font-weight:bold;border-bottom:1px solid #000000;"><a href="{{ route('portfolio') }}?s=production"><h4 style="font-weight: 600;">Production Service</h4></a></li>
                    </ol>
                </div>
                <div class="col-xl-7 col-md-7" style="position:relative;">
                    <div id="img-container-1">
                        <div>
                            <div id="figure-001">
                                <div>
                                    <amp-img src="{{asset('frontend/assets/img/core_bussines_1.png')}}" width="375" height="564"
                                        layout="responsive">
                                    </amp-img>
                                </div>
                            </div>
                            <div id="figure-002">
                                <div>
                                    <amp-img src="{{asset('frontend/assets/img/core_bussines_2.png')}}" width="409" height="289"
                                        layout="responsive">
                                    </amp-img>
                                </div>
                            </div>
                            <div id="figure-003">
                                <div>
                                    <amp-img src="{{asset('frontend/assets/img/core_bussines_3.png')}}" width="280" height="480"
                                        layout="responsive">
                                    </amp-img>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- <div id="orange-box" class="mb-5">
        <h1 class="font-size-md font-weight-900 text-center my-5 who">WHY <span class="text-outline">JOIN US?</span></h1>
        <div class="row px-3 text-center mb-5 text-black">
            <div class="col-md-4 col-sm-6 text-center mt-5">
                <amp-img id="img-join" src="{{asset('frontend/assets/img/icon/icon-fair-revenue-share-rev.png')}}" width="100" height="100"layout="responsive"></amp-img>
                <h4 class="title-joinus text-black">Fair Revenue Share</h4>
                <p class="text-black">Revenue Share up to 90% for creator</p>
            </div>
            <div class="col-md-4 col-sm-6 text-center mt-5">
                <amp-img id="img-join" src="{{asset('frontend/assets/img/icon/icon-on-time-payment-rev.png')}}" width="100" height="100"layout="responsive"></amp-img>
                <h4 class="title-joinus">On Time Payment</h4>
                <p>Revenue sharing will be paid after 2 weeks</p>
            </div>
            <div class="col-md-4 col-sm-6 text-center mt-5">
                <amp-img id="img-join" src="{{asset('frontend/assets/img/icon/icon-tools-rev.png')}}" width="100" height="100"layout="responsive"></amp-img>
                <h4 class="title-joinus">Advance Tools</h4>
                <p>PR on our Fanpage system with millions of fans and visits</p>
            </div>
            <div class="col-md-4 col-sm-6 text-center mt-5">
                <amp-img id="img-join" src="{{asset('frontend/assets/img/icon/icon-music-bank-rev.png')}}" width="100" height="100"layout="responsive"></amp-img>
                <h4 class="title-joinus">Music Bank & sound effect Library</h4>
                <p>Large selection of music and sound effect for videos</p>
            </div>
            <div class="col-md-4 col-sm-6 text-center mt-5">
                <amp-img id="img-join" src="{{asset('frontend/assets/img/icon/icon-promotion-rev.png')}}" width="100" height="100"layout="responsive"></amp-img>
                <h4 class="title-joinus">Promotion</h4>
                <p>Integrated marketing and promotion support</p>
            </div>
            <div class="col-md-4 col-sm-6 text-center mt-5">
                <amp-img id="img-join" src="{{asset('frontend/assets/img/icon/icon-brand-deals-rev.png')}}" width="100" height="100"layout="responsive"></amp-img>
                <h4 class="title-joinus">Brand Deal</h4>
                <p>Brand approach and collaboration with MNC Integrated Sales</p>
            </div>
            <div class="col-md-4 col-sm-6 text-center mt-5">
                <amp-img id="img-join" src="{{asset('frontend/assets/img/icon/icon-high-revenue-rev.png')}}" width="100" height="100"layout="responsive"></amp-img>
                <h4 class="title-joinus">High Revenue</h4>
                <p>Creator get multiple youtube ads for used on video</p>
            </div>
            <div class="col-md-4 col-sm-6 text-center mt-5">
                <amp-img id="img-join" src="{{asset('frontend/assets/img/icon/icon-training-rev.png')}}" width="100" height="100"layout="responsive"></amp-img>
                <h4 class="title-joinus">Free Training</h4>
                <p>Creators will get training on the latest products from YouTube</p>
            </div>
            <div class="col-md-4 col-sm-6 text-center mt-5">
                <amp-img id="img-join" src="{{asset('frontend/assets/img/icon/icon-production-facilities-rev.png')}}" width="100" height="100"layout="responsive"></amp-img>
                <h4 class="title-joinus">Production Facilities</h4>
                <p>Free use of the studios and properties</p>
            </div>
        </div>
    </div> --}}

    <div class="row affiliates mb-5">
        <div class="col-xl-10 mx-auto text-center">
            <h1 class="font-size-md font-weight-900"><span class="text-outline">OUR</span> AFFILIATES</h1>
            <div class="row">
                <div class="col-md-6 col-sm-6 text-center bg-smn">
                    <a href="http://www.starmedianusantara.com/" target="_blank">
                        <amp-img src="{{asset('frontend/assets/img/background/bg-1280x720-logo-smn.png')}}" width="240" height="118"layout="responsive"></amp-img>
                    </a>
                </div>
                <div class="col-xl-6 col-sm-6 text-center bg-hits">
                    <a href="http://hitsrecords.co.id" target="_blank">
                        <amp-img src="{{asset('frontend/assets/img/background/bg-1280x720-logo-hits.png')}}" width="240" height="118"layout="responsive"></amp-img>
                    </a>
                </div>
            </div>
        </div>
    </div>

    {{-- <div class="row" style="margin-bottom: 50px;">
        <div class="col-xl-5" style="margin-left: -15px;padding-bottom: 3rem">
            <amp-img src="{{asset('frontend/assets/img/revenue_share.png')}}" style="box-shadow: black 20px 20px 0" width="509"
                height="738" layout="responsive"></amp-img>
        </div>
        <div class="col-xl-7">
            <div style="padding-left: 4vw" id="revenue-share">
                <h1><span class="text-outline">REVENUE</span> SHARE</h1>
                <p>
                    Lorem ipsum dolor sit amet, sectetur adipiscing elit, sed do eiusmod tempor
                    incididunt
                    ut labore et dolore magna aliqua ut enim ad minim.Lorem ipsum dolor sit amet,
                    sectetur
                    adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua ut enim ad
                    minim. Lorem ipsum dolor sit amet, sectetur adipiscing elit, sed do eiusmod
                    tempor incididunt ut
                    labore et dolore magna aliqua ut enim ad minim.
                </p>

                <div class="row">
                    <div class="col-xl-9 revenue-share-article">
                        <h3>STARTER</h3>
                        <div class="polygon-3-right"></div>
                        <p>What you’ll get:</p>
                        <ul>
                            <li>Content ID (Video, Sound Recording & Composition Song)</li>
                            <li>Star Hits Tools</li>
                            <li>No Minimum Revenue</li>
                            <li>Monthly, Channel Review & Payment Report</li>
                            <li>Brand Deals</li>
                            <li>Promotion</li>
                            <li>Digital Rights Management</li>
                            <li>Studio Facilities</li>
                        </ul>
                    </div>
                    <div class="col-xl-3" style="display: flex;align-items:center;text-align:center;">
                        <h1>90:10</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-9 revenue-share-article">
                        <h3>PRO</h3>
                        <div class="polygon-3-right"></div>
                        <p>What you’ll get:</p>
                        <ul>
                            <li>Content ID (Video, Sound Recording & Composition Song)</li>
                            <li>Star Hits Tools</li>
                            <li>No Minimum Revenue</li>
                            <li>Monthly, Channel Review & Payment Report</li>
                            <li>Exclusive Brand Deals</li>
                            <li>Promotion</li>
                            <li>Digital Rights Management</li>
                            <li>Studio Facilities</li>
                            <li>Conceptualizing, Shooting, & Content Editing</li>
                        </ul>
                        <ul style="list-style-type:none;">
                            <li class="small">*The 70:30 revenue share applies after cutting production cost from brand deals.</li>
                        </ul>
                    </div>
                    <div class="col-xl-3" style="display: flex;align-items:center;text-align:center;">
                        <h1>70:30</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col" style="text-align: center;padding-top: 2em;">
                    <a href="https://dashboard.starhits.id/joinnetwork.html?id=OTE=&langId=en" id="join-us-link-xxl">JOIN US NOW</a>
                </div>
            </div>
        </div>
    </div> --}}
    {{-- <div class="row">
        <div class="col-12 text-center p-0">
            <a href="https://dashboard.starhits.id/joinnetwork.html?id=OTE=&langId=en" id="join-us-link-xxl">JOIN US NOW</a>
        </div>
    </div> --}}


@endsection