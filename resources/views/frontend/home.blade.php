@extends('layouts.frontend')

@section('heading')
    
    <div class="col-header-bg mb-5"></div>
    <div id="bg-circle-1">
        <amp-img src="frontend/assets/images/bg-circle.png" width="473" height="474" layout="responsive"
            style="transform: rotate(-90deg)">
        </amp-img>
    </div>
@endsection

@section('content')
    {{-- BEGIN LIVE STREAM --}}
    {{-- <div class="live-stream mt-4">
        <h1 class="title-our-business text-center"><span class="text-outline">LIVE</span> STREAMING</h1>
        <h2 class="text-center">Shout Out! Apa Kata Ainun Idol Setelah Tereliminasi?</h2>
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <!-- <p class="p-0">[Live] on 10 December 2019, 2:30 PM</p> -->
                <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/GhliI0GRhk0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                <amp-youtube id="myLiveChannel" data-videoid="97IDmo0X5uU" width="1024" height="480" layout="responsive">
                    <amp-img src="https://i.ytimg.com/vi/97IDmo0X5uU/maxresdefault_live.jpg" placeholder width="358" height="204" layout="responsive"/>
                </amp-youtube>
                <!-- <amp-iframe width="200" height="100"
                sandbox="allow-scripts allow-same-origin"
                layout="responsive"
                    src="https://www.youtube.com/live_chat?v=hWh-is90Fkg&embed_domain=dev.starhits.id">
                </amp-iframe> -->
            </div>
            <div class="col-md-4 col-sm-12">
                <iframe allowfullscreen="" frameborder="0" id="iframeStream" src="https://www.youtube.com/live_chat?v=97IDmo0X5uU&embed_domain=starhits.id"></iframe>
            </div>
        </div>
    </div> --}}

    <div class="col-home-slider my-3">
        {{-- <div class="col-header-bg mb-5"></div>
        <div id="bg-circle-1">
            <amp-img src="frontend/assets/images/bg-circle.png" width="473" height="474" layout="responsive"
                style="transform: rotate(-90deg)">
            </amp-img>
        </div> --}}
        <div class="row">
            <div class="col-12 pt-5 my-3">
                <amp-carousel id="carouselWithPreview" class="carouselhome" layout="responsive" width="2" height="1" autoplay
                delay="10000" type="slides"
                on="slideChange:carouselWithPreviewSelector.toggle(index=event.index, value=true, class=selected)">
                
                    <div class="slide">
                        <div class="slide__img embed-responsive embed-responsive-1by1">
                            <div class="embed-responsive-item">
                                <amp-img src="{{asset('frontend/assets/img/slider/slider_1.png')}}" layout="responsive" width="704" height="704"></amp-img>
                            </div>
                        </div>

                        <h4 class="heading-slider">
                            <span>LEADING</span>
                            <span class="text-outline-black">MULTI CHANNEL<br>NETWORK</span>
                        </h4>

                        <div class="explore_us small">
                            <a href="{{route('mcn')}}">EXPLORE US</a>
                        </div>
                    </div>

                    <div class="slide">
                        <div class="slide__img embed-responsive embed-responsive-1by1">
                            <div class="embed-responsive-item">
                                <amp-img src="{{asset('frontend/assets/img/slider/slider_2_flip.png')}}" layout="responsive" width="704" height="704"></amp-img>
                            </div>
                        </div>

                        <h3 class="heading-slider" style="top:3em;">
                            <span class="text-outline-black">HOME OF</span>
                            <span style="padding-left:1em;">CREATORS</span>
                        </h3>

                        <div class="explore_us small">
                            <a href="{{route('creators')}}#navi">EXPLORE US</a>
                        </div>
                    </div>

                </amp-carousel>

                <amp-selector id="carouselWithPreviewSelector" class="slide__selector"
                    on="select:carouselWithPreview.goToSlide(index=event.targetOption)" layout="container">

                    <amp-layout layout="fixed" width="22" height="22">
                        <div option="0" class="slide__selector__item" selected></div>
                    </amp-layout>

                    <amp-layout layout="fixed" width="22" height="22">
                        <div option="1" class="slide__selector__item"></div>
                    </amp-layout>

                </amp-selector>

                <div class="col-home-info d-flex flex-lg-column justify-content-around font-weight-bold mt-0">
                    <div class="mb-lg-3">
                        <div>Watchtime</div>
                        <h3 class="font-weight-bold">{{ bd_nice_numbers($EstimateWatch) }}</h3>
                        {{--<h3 class="font-weight-bold">{{ bd_nice_numbers('121308046616') }}</h3>--}}
                    </div>

                    <div class="mb-lg-3">
                        <div>Views</div>
                        <h3 class="font-weight-bold">{{ bd_nice_numbers($viewCount) }}</h3>
                        {{--<h3 class="font-weight-bold">{{ bd_nice_numbers('23049859042') }}</h3>--}}
                    </div>

                    <div class="mb-lg-3">
                        <div>Subscribers</div>
                        <h3 class="font-weight-bold">{{ bd_nice_numbers($subsCount) }}</h3>
                        {{-- <h3 class="font-weight-bold">{{ bd_nice_numbers('51786524') }}</h3> --}}
                    </div>

                    <div class="mb-lg-3">
                        <div>Videos</div>
                        <h3 class="font-weight-bold">{{ bd_nice_numbers($videoCount) }}</h3>
                        {{--<h3 class="font-weight-bold">{{ bd_nice_numbers('342650') }}</h3> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN OUR BUSINESS-->
    <div class="col-our-business p-5 mt-5">
        <h1 class="title-our-business text-center"><span class="text-outline">OUR</span> BUSINESS</h1>

        <div class="row">
            <div class="col-12 col-lg-5 col-our-business-img">
                <div class="embed-responsive embed-responsive-1by1">
                    <div class="embed-responsive-item">
                        <div class="col-our-business__bg">
                            <amp-img layout="responsive" width="1" height="1"
                                src="{{asset('frontend/assets/img/background/bg-our-business-01.png')}}"></amp-img>
                        </div>

                        <div class="our-business-img-01" style="margin-left:40px;margin-top:10px;">
                            <amp-img src="{{asset('frontend/assets/img/icon/icon-yt.png')}}" class="img-icon-yt" width="36" height="26">
                            </amp-img>
                            <amp-img src="{{asset('frontend/assets/img/ourbusiness_1.png')}}" class="img-our-bussiness" width="255"
                                height="170" layout="responsive"></amp-img>
                        </div>

                        <div class="our-business-img-02">
                            <amp-img src="{{asset('frontend/assets/img/icon/icon-love.png')}}" class="img-icon-love" width="35"
                                height="26"><span>1</span></amp-img>
                            <amp-img src="{{asset('frontend/assets/img/ourbusiness_2.png')}}" class="img-our-bussiness" width="200"
                                height="300" layout="responsive"></amp-img>
                        </div>
                        <div class="our-business-img-03" style="">
                            <amp-img src="{{asset('frontend/assets/img/icon/icon-msg.png')}}" class="img-icon-msg" width="43"
                                height="32"><span>17</span></amp-img>
                            <amp-img src="{{asset('frontend/assets/img/ourbusiness_3.png')}}" class="img-our-bussiness" width="309"
                                height="206" layout="responsive"></amp-img>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-lg-7 col-our-business-list">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="row">
                            <div class="col-12 col-our-business-item">
                                <h1>01</h1>
                                <a href="{{route('mcn')}}"><h4 class="our-business-heading">Multi Channel Network</h4></a>
                                <p class="our-business-content">We aim to effectively distribute content across media platforms to boost audience reach, monetization, and/or sales.</p>
                            </div>

                            <div class="col-12 col-our-business-item">
                                <h1>02</h1>
                                <a href="{{route('creators')}}#navi"><h4 class="our-business-heading">Influencer Management</h4></a>
                                <p class="our-business-content">As a management agency for influencers and content creators, we help managing well-planned campaign and assistance to launch campaigns for brands and marketers.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="row">
                            <div class="col-12 col-our-business-item">
                                <h1>03</h1>
                                <a href="{{ route('portfolio') }}?s=digital"><h4 class="our-business-heading">Creative Digital Campaign</h4></a>
                                <p class="our-business-content">We maximize the presence of every content we produce using digital-based marketing plan  comprehensively by utilizing digital ads, articles, videos, creators, and influencers.</p>
                            </div>

                            <div class="col-12 col-our-business-item">
                                <h1>04</h1>
                                <a href="{{ route('portfolio') }}?s=production"><h4 class="our-business-heading">Production Service</h4></a>
                                <p class="our-business-content">We have our in-house team and latest facilities to create effective TVC advertisement, web series, short video, and design assets for our clients.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column">

        <!-- BEGIN RECOMENDED VIDEO-->
        <div class="col-recomended-video">
            <div id="bg-triangle-1">
                <div style="transform: rotate(-90deg)">
                    <amp-img src="frontend/assets/images/bg-triangle.png" width="50" height="92" layout="responsive"></amp-img>
                </div>
            </div>

            <h3 class="title-recomended-video text-center m-0 p-0 mt-1 lh-1">RECOMENDED</h3>
            <h3 class="title-recomended-video text-center m-0 p-0 lh-1"><span class="text-outline">VIDEOS</span></h3>

            <div id="content-slider" class="col-md-10 mx-auto">
                <div class="content-slider">
                    <amp-carousel id="recomendedVideoSlide" type="carousel" layout="fixed-height" width="auto" height="1070">
                        
                        @foreach ($recomendedVideos as $value)
                        <a href="{{ url('/videos').'/'.$value->slug }}" class="d-inline-block">                        
                            <div class="slider-box">
                                <div class="slider-box__wrapper">
                                    <div class="slider-box__img">
                                        @if(!empty($value->image))
                                            <amp-img src="{{ url('/'). Image::url($value->image,400,225,array('crop')) }}" layout="responsive" width="320" height="180" class="img-cover"></amp-img>
                                        @else
                                        <amp-img src="https://i3.ytimg.com/vi/{{ $value->attr_6}}/mqdefault.jpg" layout="responsive" width="320" height="180" class="img-cover"></amp-img>
                                        @endif
                                    </div>
                                    <div class="overlay"></div>
                                </div>
                                <p class="small p-0 m-0">{{ date_format($value->created_at, 'd F Y') }}</p>
                                <p class="title-video mb-1">{{ substr($value->title, 0, 35) }}...</p>
                                {{-- <amp-fit-text layout="responsive" width="300" height="50" min-font-size="12"
                                    max-font-size="12">
                                    <span class="desc-video">{{ substr(str_replace('Subtitle', '', $value->subtitle), 0, 80) }}</span>
                                </amp-fit-text> --}}
                            </div>
                        </a>
                        @endforeach
                    </amp-carousel>
                </div>
                <div id="bg-triangle-2">
                    <amp-img src="frontend/assets/images/bg-triangle-1.png" layout="responsive" width="124" height="60"></amp-img>
                </div>
            </div>

            <div class="carousel-controls controls-home">
                <amp-layout layout="fixed" width="100" height="32" >
                    <div class="carousel-controls-arrow" tabindex="0" role="button" on="tap:recomendedVideoSlide.goToSlide(index=0)">&#x27F5;</div>
                </amp-layout>    
                
                <amp-layout layout="fixed" width="100" height="32">
                    <a href="{{route('videos')}}">VIEW ALL</a>
                </amp-layout>

                <amp-layout layout="fixed" width="100" height="32">
                    <div class="carousel-controls-arrow" tabindex="1" role="button" on="tap:recomendedVideoSlide.goToSlide(index=3)">&#x27F6;</div>
                </amp-layout>
            </div>
        </div>

        <!-- BEGIN CONTENT CREATOR-->
        <div class="col-content-creator order-1">
            <div class="bg-content-creator">

                <div class="text-center">
                    <h3 class="title-recomended-video">CONTENT <span class="text-outline">CREATORS</span>
                    </h3>
                </div>

                <div class="col-md-11 mx-auto">
                    <div class="col-recomended-video-slider max-width-1366">
                        <amp-carousel id="recomendedCreatorSlide" type="carousel" height="700">
                            @foreach($creatorsFeatureHome as $items)
                            <div class="slider-box col-12 col-sm-6 col-lg-3 col-xl-4">
                                <div class="col-recomended-video-slider__wrapper">
                                    <div class="col-recomended-video-slider__img">
                                        <div class="title-creator-content pt-3">
                                                <amp-fit-text layout="fixed-height" height="50" max-font-size="32px">
                                                    {{ $items->name }}
                                                </amp-fit-text> 
                                            </div>
                                        <amp-img src="{{ $items->feature_home }}" width="259" height="428" layout="responsive"></amp-img>
                                        <div class="content-text">
                                            <div class="social-media">
                                                <a href="https://youtube.com/channel/{{ $items->provider_id }}" target="_blank"><i class="fab fa-youtube"></i></a>
                                                @foreach ($items->socials as $social)
                                                    @if($social->name=='ig')
                                                        @if(!empty($social->url))
                                                        <a href="{{$social->url}}" target="_blank"><i class="fab fa-instagram"></i></a>
                                                        @else
                                                        <a href="#"><i class="fab fa-instagram text-instagram"></i></a>
                                                        @endif
                                                    @elseif($social->name=='fb')
                                                        @if(!empty($social->url))
                                                        <a href="{{$social->url}}" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                                        @else
                                                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                                                        @endif
                                                    @elseif($social->name=='twitter')
                                                        @if(!empty($social->url))
                                                        <a href="{{$social->url}}" target="_blank"><i class="fab fa-twitter"></i></a>
                                                        @else
                                                        <a href="#"><i class="fab fa-twitter"></i></a>
                                                        @endif
                                                    @endif
                                                @endforeach
                                            </div>
                                            <div class="btn-circle">
                                                <amp-layout layout="fixed" width="50" height="50">
                                                    &plus;
                                                </amp-layout>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </amp-carousel>
                        
                        <div class="carousel-controls controls-home">
                            <amp-layout layout="fixed" width="100" height="32">
                                <div class="carousel-controls-arrow" tabindex="1" role="button" on="tap:recomendedCreatorSlide.goToSlide(index=0)">&#x27F5;</div>
                            </amp-layout>
                            
                            <amp-layout layout="fixed" width="100" height="32">
                                <a href="{{route('creators')}}#navi">VIEW ALL</a>
                            </amp-layout>
                            
                            <amp-layout layout="fixed" width="100" height="32">
                                <div class="carousel-controls-arrow" tabindex="1" role="button" on="tap:recomendedCreatorSlide.goToSlide(index=1)">&#x27F6;</div>
                            </amp-layout>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
