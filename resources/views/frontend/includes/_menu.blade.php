{{-- {{dd(Request::path())}} --}}
<amp-sidebar id="sidebar-left" class="nav-sidebar" layout="nodisplay" side="left">
    <h6><a href="{{route('home')}}"><amp-img src="{{asset('frontend/assets/img/logo-home.png')}}" width="100" height="35"></amp-img></a></h6>
    <span tabindex="0" role="button" on="tap:sidebar-left.close" class="nav-sidebar-close">✕</span>
    <nav toolbar="(min-width: 784px)" toolbar-target="target-element-left">
        <ul class="w-100 nav-justified">
            <li class="nav-item text-left"><a href="{{url('')}}" class="header-menu-2 {{ Request::route()->uri =='/' ? 'active' : '' }} {{ Request::is('home') ? 'active' : '' }}">HOME</a></li>
            <li class="nav-item text-left"><a href="{{url('about-us')}}" class="header-menu-2 {{ Request::is('about-us*') ? 'active' : '' }}">ABOUT US</a></li>
            <li class="nav-item text-left"><a href="{{url('creators')}}#navi" class="header-menu-2 {{ Request::is('creators*') ? 'active' : '' }}">CREATORS</a></li>
            <li class="nav-item text-left"><a href="{{url('series')}}" class="header-menu-2 {{ Request::is('series*') ? 'active' : '' }}">SERIES</a></li>
            <li class="nav-item text-left"><a href="{{url('channels')}}" class="header-menu-2 {{ Request::is('channels*') ? 'active' : '' }}">CHANNELS</a></li>
            <li class="nav-item d-none d-lg-inline">
                @guest
                <div class="group-menu">
                    <span><a href="https://dashboard.starhits.id/joinnetwork.html">JOIN US</a></span>
                    <span class="delimiter">|</span>
                    <span><a href="{{url('login')}}">LOGIN</a></span>
                </div>
                @else
                <div class="group-menu">
                    @role('admin')
                    <span><a href="{{ url('admin/dashboard') }}">Account</a></span>
                    @endrole
                    @role('user')
                    <span><a href="{{ url('user/dashboard') }}">Account</a></span>
                    @endrole
                    @role('creator')
                    <span><a href="{{ url('creator/personal') }}">Account</a></span>
                    @endrole
                    @role('finance')
                    <span><a href="{{ url('finance/dashboard') }}">Account</a></span>
                    @endrole
                    @role('legal')
                    <span><a href="{{ url('legal/dashboard') }}">Account</a></span>
                    @endrole
                    @role('influencer')
                    <span><a href="{{ url('influencer/dashboard') }}">Account</a></span>
                    @endrole
                    @role('singer')
                    <span><a href="{{ url('singer/dashboard') }}">Account</a></span>
                    @endrole
                    @role('songwriter')
                    <span><a href="{{ url('songwriter/dashboard') }}">Account</a></span>
                    @endrole
                    <span class="delimiter">|</span>
                    <span><a href="{{ url('/logout') }}">Logout</a></span>
                </div>
                @endguest
            </li>
        </ul>
    </nav>
    <ul>
        <li><a href="{{route('portfolio')}}" class="header-menu-2">PORTOFOLIO</a></li>
        <li><a href="{{route('contact')}}" class="header-menu-2">CREATE PROJECT</a></li>
        <li><a href="https://dashboard.starhits.id/joinnetwork.html" class="header-menu-2">JOIN US</a></li>
        <li><a href="{{ route('login') }}" class="header-menu-2">LOGIN</a></li>
    </ul>
</amp-sidebar>
<amp-position-observer on="exit:showHeader.start; enter:hideHeader.start;" layout="nodisplay" intersection-ratios="1"></amp-position-observer>
<div><amp-position-observer on="exit:showHeader.start; enter:hideHeader.start;" layout="nodisplay"
    intersection-ratios="1"></amp-position-observer></div>