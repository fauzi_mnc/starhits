<style amp-custom>
    {{ include"frontend/assets/css/bootstrap.css" }}

    @-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}body{margin:0;font-family:Montserrat,sans-serif;font-size:1rem;font-weight:400;line-height:1.5;color:#212529;text-align:left;background-color:#fff;-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both;-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}.body{display:flex;flex-direction:column;max-width:100vw;min-height:100vh;overflow-x:hidden;overflow-y:auto;padding:100px 0 0;margin:0;position:relative}.content{flex:1 0 auto}.footer{flex-shrink:0;background-color:#000}a:hover{text-decoration:none}[role=button]{cursor:pointer}.nav-sidebar{background:#fff;color:#232323;fill:#232323;text-transform:uppercase;letter-spacing:.18rem;font-size:.875rem;padding:0}.nav-sidebar h6{margin-top:0;padding:15px 20px;background-color:#ccc;text-align:center;font-size:14px;margin-bottom:0}.nav-sidebar h3{padding-right:35px}.nav-sidebar-close{position:absolute;right:15px;top:15px}.nav-sidebar ul{list-style:none;padding-left:0;margin:0;width:300px}.nav-sidebar ul li{padding:15px;line-height:28px;border-bottom:1px solid #dee2e6}.nav-sidebar ul li a{display:block}.nav-sidebar a:hover{text-decoration:underline;fill:#232323}.nav-sidebar a{color:#232323;text-transform:none;letter-spacing:normal}div[class*="-sidebar-mask"]{opacity:.8}[role=button]:focus{outline:0}.header-nav{z-index:1050}#target-element-left .nav-item{display:flex;align-items:center;padding:1em}#target-element-left .header-menu-2:hover, #target-element-left .header-menu-2.active,
    #target-element-left .header-menu-1.active, .header-menu-1:hover {
        color: #ffffff;
        -webkit-text-stroke: 1px #f6c101;
        text-decoration: none
    }.header-menu-1{color:rgba(0,0,0,.58);font-size:x-small}#target-element-left .header-menu-2{color:#000;text-decoration:none;font-size:small}#target-element-left .header-menu-2,.group-menu a,.header-menu-1{font-weight:700}.group-menu{display:none;justify-content:space-evenly;background-color:#000;border-radius:9999px;padding:.5em 1em;white-space:nowrap}@media(min-width:992px){.group-menu{display:flex}}.group-menu a{color:#fff;font-size:small}.group-menu .delimiter{color:#fff;font-weight:700;margin:0 .5em}.group-menu a:hover{color:#f6c101;text-decoration:none}#colHeaderContentBg{margin:0 -15px}.fixed-top,.sticky-top{height:100px}html{scroll-behavior:smooth;overflow-x:hidden;overflow-y:scroll}.scrolltop-wrap{box-sizing:border-box;position:absolute;top:0;right:1em;bottom:0;pointer-events:none;-webkit-backface-visibility:hidden;backface-visibility:hidden;width:3rem}.scrolltop-wrap a{position:fixed;position:-webkit-sticky;position:sticky;top:-4rem;width:3rem;height:3rem;margin-bottom:-5rem;-webkit-transform:translateY(100vh);transform:translateY(100vh);-webkit-backface-visibility:hidden;backface-visibility:hidden;display:inline-block;text-decoration:none;-webkit-user-select:auto;-moz-user-select:auto;-ms-user-select:auto;user-select:auto;pointer-events:all;outline:0;overflow:hidden;font-size:2rem;color:orange;writing-mode:vertical-lr;text-align:center;background-color:rgba(0,123,255,.25);border-radius:10%}
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-weight: 700
    }

    .font-weight-900 {
        font-weight: 900
    }

    .font-style-i {
        font-style: italic
    }

    .font-size-xl {
        font-size: 8vw
    }

    .font-size-md {
        font-size: 4vw
    }

    .page-title {
        position: absolute;
        left: 0;
        top: 350px;
        width: 100vw;
        text-align: center
    }

    @media(min-width:1200px) {
        .page-title h1:first-child {
            transform: translateX(-1em)
        }

        .page-title h1:nth-child(2) {
            transform: translateX(1em)
        }
    }

    .text-outline {
        color: transparent;
        text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000
    }

    @supports((text-stroke:1px black) or (-webkit-text-stroke:1px black)) {
        .text-outline {
            color: transparent;
            -webkit-text-stroke: 1px white;
            text-stroke: 2px white;
            text-shadow: none
        }
    }

    #profile-picture {
        display: block;
        width: 100vw;
        position: relative;
        margin: 0 0 50px -30px;
        text-align: center
    }

    @media(min-width:768px) {
        #profile-picture {
            /* margin-bottom: 100px */
        }
    }

    @media(max-width:768px) {
        .fixed-top, .sticky-top {
            height: 70px;
        }
    }

    #page-title {
        position: absolute;
        width: 100vw;
        top: 50%;
        left: 50%;
        -webkit-transform: translate(-50%, -50%);
        -moz-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        -o-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
    }

    .nav {
        display: flex;
        flex-wrap: nowrap;
        flex-direction: row-reverse;
        justify-content: center;
        align-items: flex-start;
        margin: 3em 0;
        position: relative;
        -webkit-overflow-scrolling: touch;
        -ms-overflow-style: -ms-autohiding-scrollbar;
        background-color: orange;
        padding: 0 1rem;
        height: 50px
    }

    @media(max-width:1199px) {
        .nav {
            margin: 3em -30px
        }
    }

    .nav::-webkit-scrollbar {
        display: none
    }

    .nav__dropdown {
        position: absolute
    }

    .nav__dropdown__header,
    .nav__dropdown__header:focus {
        background-color: transparent;
        border: 0;
        font-weight: 700;
        width: 220px;
        outline: 0;
        padding: 0 1em;
        line-height: 50px;
        white-space: nowrap
    }

    .nav__dropdown__list {
        list-style: none;
        padding: 1px 0 0;
        width: 220px;
        z-index: 1
    }

    .nav__dropdown__listitem {
        padding: 1em;
        background-color: orange;
        width: 220px
    }

    #main {
        background-color: orange;
        position: relative;
        list-style: none;
        font-weight: 700;
        font-size: 0;
        text-transform: uppercase;
        padding-left: 120px;
        text-align: center;
        white-space: nowrap;
        overflow-x: auto;
        -webkit-overflow-scrolling: touch;
        -ms-overflow-style: -ms-autohiding-scrollbar
    }

    #main::-webkit-scrollbar {
        display: none
    }

    #main>li {
        background-color: orange;
        line-height: 50px;
        font-size: 1rem;
        display: inline-block;
        position: relative;
        cursor: pointer;
        min-width: 220px;
        z-index: 3
    }

    li {
        margin: 0
    }

    .dropdown-symbol::before {
        content: "\25BC";
        padding: 0 .5em
    }

    @media(max-width:1199px) {
        .show-xl {
            display: none
        }
    }

    @media(min-width:1200px) {
        #dropdown-container {
            position: relative;
            width: 220px
        }
    }

    :-ms-input-placeholder {
        color: #000;
        font-weight: 700
    }

    ::-ms-input-placeholder {
        color: #000;
        font-weight: 700
    }

    ::placeholder {
        color: #000;
        font-weight: 700;
        opacity: 1
    }

    input[type=search]:focus {
        background-color: transparent;
        outline: 0;
        box-shadow: none
    }

    input[type=search] {
        background-repeat: no-repeat;
        background-position: center right;
        background-size: contain;
        background-origin: content-box;
        background-color: transparent;
        border: 0;
        border-bottom: 1px solid;
        border-radius: 0;
        margin: 0
    }

    amp-list.list [role=list] {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-around;
        overflow: hidden;
    }

    .card,
    .card-img {
        border-radius: 0;
        border-color: transparent;
        object-fit: cover;
    }

    .card {
        padding: 8px
    }

    .card-img-overlay {
        border-radius: 1rem;
        display: flex;
        flex-direction: column;
        justify-content: flex-end;
        background: rgba(247, 226, 154, .8);
        -webkit-clip-path: polygon(0 0, 100% 77px, 100% 100%, 0 100%);
        clip-path: polygon(0 0, 100% 77px, 100% 100%, 0 100%);
        opacity: 0;
        transition: opacity 1s;
        top: 8px;
        left: 8px;
        bottom: 8px;
        right: 8px;
        object-fit: cover;
    }

    .preview-popup-description {
        margin-bottom: 1em
    }

    .preview-popup-link::before {
        content: ' ';
        border: 1px solid;
        border-radius: 50%;
        width: 1rem;
        height: 1rem;
        display: inline-block;
        margin-right: .5em;
        transform: translateY(20%)
    }

    .preview-popup-link {
        color: #000;
        font-weight: 700
    }

    [role=listitem]:hover .card-img-overlay {
        opacity: 1
    }

    [role=listitem] {
        position: relative;
        display: inline-block
    }

    .channel-preview {
        border-radius: 1rem
    }

    .subtitle {
        margin-top: 1em;
        margin-bottom: 0
    }

    .caption {
        padding: 1rem
    }

    .caption>.title {
        color: var(--secondary);
        font-size: .75em;
        position: relative
    }

    .amp-load-more {
        display: flex;
        justify-content: center;
        height: 156px
    }

    .load-more-link {
        background: 0 0;
        color: #000;
        font-weight: 700;
        margin: auto;
        border: 1px solid;
        border-radius: 24px;
        text-decoration: none;
        padding: .5em 3em;
        margin-top: 2rem
    }
</style>
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>