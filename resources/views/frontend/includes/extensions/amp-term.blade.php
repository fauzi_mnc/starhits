<style amp-custom>
    {{ include"frontend/assets/css/bootstrap.css" }}
    body {
        margin: 0;
        font-family: Montserrat, sans-serif;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #212529;
        text-align: left;
        background-color: #fff;
    }

    .body {
        display: flex;
        flex-direction: column;
        max-width: 100vw;
        min-height: 100vh;
        overflow-x: hidden;
        overflow-y: auto;
        padding: 50px 0 0;
        margin: 0;
        position: relative
    }

    @media(min-width:992px) {
        .body {
            padding: 100px 0 0
        }
    }

    .content {
        flex: 1 0 auto
    }

    .footer {
        flex-shrink: 0;
        background-color: #000
    }

    a:hover {
        text-decoration: none
    }

    [role=button] {
        cursor: pointer
    }

    .nav-sidebar {
        background: #fff;
        color: #232323;
        fill: #232323;
        text-transform: uppercase;
        letter-spacing: .18rem;
        font-size: .875rem;
        padding: 0
    }

    .nav-sidebar h6 {
        margin-top: 0;
        padding: 15px 20px;
        background-color: #ccc;
        text-align: center;
        font-size: 14px;
        margin-bottom: 0
    }

    .nav-sidebar h3 {
        padding-right: 35px
    }

    .nav-sidebar-close {
        position: absolute;
        right: 15px;
        top: 15px
    }

    .nav-sidebar ul {
        list-style: none;
        padding-left: 0;
        margin: 0;
        width: 300px
    }

    .nav-sidebar ul li {
        padding: 15px;
        line-height: 28px;
        border-bottom: 1px solid #dee2e6
    }

    .nav-sidebar ul li a {
        display: block
    }

    .nav-sidebar a:hover {
        text-decoration: underline;
        fill: #232323
    }

    .nav-sidebar a {
        color: #232323;
        text-transform: none;
        letter-spacing: normal
    }

    div[class*="-sidebar-mask"] {
        opacity: .8
    }

    [role=button]:focus {
        outline: 0
    }

    .header-nav {
        z-index: 1050
    }

    #target-element-left .nav-item {
        display: flex;
        align-items: center;
        padding: 1em
    }

    #target-element-left .header-menu-2:hover,
    .header-menu-1:hover {
        color: #f6c101;
        text-decoration: none
    }

    .header-menu-1 {
        color: rgba(0, 0, 0, .58);
        font-size: x-small
    }

    #target-element-left .header-menu-2 {
        color: #000;
        text-decoration: none;
        font-size: small
    }

    #target-element-left .header-menu-2,
    .group-menu a,
    .header-menu-1 {
        font-weight: 700
    }

    .group-menu {
        display: none;
        justify-content: space-evenly;
        background-color: #000;
        border-radius: 9999px;
        padding: .5em 1em;
        white-space: nowrap
    }

    @media(min-width:992px) {
        .group-menu {
            display: flex
        }
    }

    .group-menu a {
        color: #fff;
        font-size: small
    }

    .group-menu .delimiter {
        color: #fff;
        font-weight: 700;
        margin: 0 .5em
    }

    .group-menu a:hover {
        color: #f6c101;
        text-decoration: none
    }

    .fixed-top,
    .sticky-top {
        height: 50px
    }

    @media(min-width:992px) {

        .fixed-top,
        .sticky-top {
            height: 100px
        }
    }

    html {
        scroll-behavior: smooth;
        overflow-x: hidden;
        overflow-y: scroll
    }

    .scrolltop-wrap {
        box-sizing: border-box;
        position: absolute;
        top: 0;
        right: 1em;
        bottom: 0;
        pointer-events: none;
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
        width: 3rem
    }

    .scrolltop-wrap a {
        position: fixed;
        position: -webkit-sticky;
        position: sticky;
        top: -4rem;
        width: 3rem;
        height: 3rem;
        margin-bottom: -5rem;
        -webkit-transform: translateY(100vh);
        transform: translateY(100vh);
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
        display: inline-block;
        text-decoration: none;
        -webkit-user-select: auto;
        -moz-user-select: auto;
        -ms-user-select: auto;
        user-select: auto;
        pointer-events: all;
        outline: 0;
        overflow: hidden;
        font-size: 2rem;
        color: orange;
        writing-mode: vertical-lr;
        text-align: center;
        background-color: rgba(0, 123, 255, .25);
        border-radius: 10%
    }

    /*!
    * Custom page styles
    */
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-weight: 900
    }

    .font-weight-900 {
        font-weight: 900
    }

    .font-style-i {
        font-style: italic
    }

    @media(min-width: 768px) {
        .font-size-xl {
            font-size: 8vw
        }
    }

    #page-title {
        position: absolute;
        top: 50%;
        left: 0;
        right: 0;
        transform: translateY(-50%)
    }

    .faq__header {
        font-weight: 700;
        padding: 15px 80px 15px 0;
        border: 0;
        border-bottom: 1px solid #696969;
        background-color: transparent
    }

    .faq__header::after {
        content: '\276F';
        position: absolute;
        right: 0;
        top: 50%;
        transform: translateY(-50%);
        border: 2px solid #000;
        width: 50px;
        height: 50px;
        text-align: center;
        border-radius: 50%;
        writing-mode: vertical-lr
    }

    #faqs-list [expanded] .faq__header::after {
        content: '\276E'
    }

    .faq__header:focus {
        outline: 0
    }

    .faq__answer {
        padding: 3rem 0
    }
</style>
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>