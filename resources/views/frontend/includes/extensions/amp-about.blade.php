<style amp-custom>
    {{ include"frontend/assets/css/bootstrap.css" }}
@-webkit-keyframes -amp-start {
    from {
        visibility: hidden
    }
    to {
        visibility: visible
    }
}

@-moz-keyframes -amp-start {
    from {
        visibility: hidden
    }
    to {
        visibility: visible
    }
}

@-ms-keyframes -amp-start {
    from {
        visibility: hidden
    }
    to {
        visibility: visible
    }
}

@-o-keyframes -amp-start {
    from {
        visibility: hidden
    }
    to {
        visibility: visible
    }
}

@keyframes -amp-start {
    from {
        visibility: hidden
    }
    to {
        visibility: visible
    }
}

body {
    margin: 0;
    font-family: Montserrat, sans-serif;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    text-align: left;
    background-color: #fff;
    -webkit-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
    -moz-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
    -ms-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
    animation: -amp-start 8s steps(1, end) 0s 1 normal both;
    -webkit-animation: none;
    -moz-animation: none;
    -ms-animation: none;
    animation: none
}

.body {
    display: flex;
    flex-direction: column;
    max-width: 100vw;
    min-height: 100vh;
    overflow-x: hidden;
    overflow-y: auto;
    padding: 100px 0 0;
    margin: 0;
    position: relative
}

.content {
    flex: 1 0 auto
}

.footer {
    flex-shrink: 0;
    background-color: #000
}

a:hover {
    text-decoration: none
}

[role=button] {
    cursor: pointer
}

.nav-sidebar {
    background: #fff;
    color: #232323;
    fill: #232323;
    text-transform: uppercase;
    letter-spacing: .18rem;
    font-size: .875rem;
    padding: 0
}

.nav-sidebar h6 {
    margin-top: 0;
    padding: 15px 20px;
    background-color: #ccc;
    text-align: center;
    font-size: 14px;
    margin-bottom: 0
}

.nav-sidebar h3 {
    padding-right: 35px
}

.nav-sidebar-close {
    position: absolute;
    right: 15px;
    top: 15px
}

.nav-sidebar ul {
    list-style: none;
    padding-left: 0;
    margin: 0;
    width: 300px
}

.nav-sidebar ul li {
    padding: 15px;
    line-height: 28px;
    border-bottom: 1px solid #dee2e6
}

.nav-sidebar ul li a {
    display: block
}

.nav-sidebar a:hover {
    text-decoration: underline;
    fill: #232323
}

.nav-sidebar a {
    color: #232323;
    text-transform: none;
    letter-spacing: normal
}

div[class*="-sidebar-mask"] {
    opacity: .8
}

[role=button]:focus {
    outline: 0
}

.header-nav {
    z-index: 1050
}

#target-element-left .nav-item {
    display: flex;
    align-items: center;
    padding: 1em
}

#target-element-left .header-menu-2:hover,
#target-element-left .header-menu-2.active,
#target-element-left .header-menu-1.active,
.header-menu-1:hover {
    color: #ffffff;
    -webkit-text-stroke: 1px #f6c101;
    text-decoration: none
}

.header-menu-1 {
    color: rgba(0, 0, 0, .58);
    font-size: x-small
}

#target-element-left .header-menu-2 {
    color: #000;
    text-decoration: none;
    font-size: small
}

#target-element-left .header-menu-2,
.group-menu a,
.header-menu-1 {
    font-weight: 700
}

.group-menu {
    display: none;
    justify-content: space-evenly;
    background-color: #000;
    border-radius: 9999px;
    padding: .5em 1em;
    white-space: nowrap
}


.group-menu a {
    color: #fff;
    font-size: small
}

.group-menu .delimiter {
    color: #fff;
    font-weight: 700;
    margin: 0 .5em
}

.group-menu a:hover {
    color: #f6c101;
    text-decoration: none
}

#colHeaderContentBg {
    margin: 0 -15px
}

.fixed-top,
.sticky-top {
    height: 100px
}

html {
    scroll-behavior: smooth;
    overflow-x: hidden;
    overflow-y: scroll
}

.scrolltop-wrap {
    box-sizing: border-box;
    position: absolute;
    top: 0;
    right: 5rem;
    bottom: 0;
    pointer-events: none;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    width: 3rem
}

.scrolltop-wrap a {
    position: fixed;
    position: -webkit-sticky;
    position: sticky;
    top: -4rem;
    width: 3rem;
    height: 3rem;
    margin-bottom: -5rem;
    -webkit-transform: translateY(100vh);
    transform: translateY(100vh);
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    display: inline-block;
    text-decoration: none;
    -webkit-user-select: auto;
    -moz-user-select: auto;
    -ms-user-select: auto;
    user-select: auto;
    pointer-events: all;
    outline: 0;
    overflow: hidden;
    font-size: 2rem;
    color: orange;
    writing-mode: vertical-lr;
    text-align: center;
    background-color: rgba(0, 123, 255, .25);
    border-radius: 10%
}

h1,
h2,
h3,
h4,
h5,
h6 {
    font-weight: 900
}

.font-weight-900 {
    font-weight: 900
}

.font-style-i {
    font-style: italic
}

.font-size-xl {
    font-size: 8vw
}

.font-size-md {
    font-size: 4vw
}

.text-outline {
    color: #fff;
    text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000
}

@supports((text-stroke:1px black) or (-webkit-text-stroke:1px black)) {
    .text-outline {
        color: transparent;
        -webkit-text-stroke: 2px #000;
        text-stroke: 2px #000;
        text-shadow: none
    }
}

#orange-box {
    background-color: orange;
    margin: 0 -15px;
    padding: 15% 0;
    -webkit-clip-path: polygon(0 16.1vw, 100% 0, 100% calc(100% - (100vw * .161)), 0 100%);
    clip-path: polygon(0 16.1vw, 100% 0, 100% calc(100% - (100vw * .161)), 0 100%)
}

#orange {
    background-color: orange;
    margin: 50px -15px;
    padding: 10% 0;
}

#core-business-list {
    list-style-position: inside;
    padding-left: 0
}

#core-business-list {
    counter-reset: section
}

#core-business-list h4::before {
    counter-increment: section;
    content: counter(section) ". "
}

#core-business-list h4 {
    padding: 15px 40px 15px 0;
    border: 0;
    border-bottom: 1px solid #696969;
    background-color: transparent
}

#core-business-list h4::after {
    content: '+';
    font-family: 'Courier New', Courier, monospace;
    position: absolute;
    right: 1rem;
    top: 50%;
    transform: translateY(-50%);
    border: 1px solid #2f4f4f;
    line-height: 1.55rem;
    width: 1.7rem;
    text-align: center;
    border-radius: 50%
}

#core-business {
    display: flex;
    flex-direction: column;
    justify-content: center
}

#core-business-list [expanded] h4::after {
    content: '-'
}

#img-container-1 {
    position: relative;
    width: 100%;
    padding-top: 110%
}

#img-container-1>div {
    background: url(frontend/assets/images/background-2.png) no-repeat center;
    background-size: contain
}

#figure-001>div,
#figure-002>div,
#figure-003>div,
#img-container-1>div {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0
}

#figure-001 {
    width: 50.4994007%;
    padding-top: 75.7491011%;
    margin-top: 5%;
    position: absolute;
    right: 0
}

#figure-002 {
    width: 65.3615661%;
    padding-top: 46.1845785%;
    position: absolute;
    bottom: 0;
    right: 30%;
    transform: translateX(60%)
}

#figure-003 {
    width: 44.7463044%;
    padding-top: 46.1108895%;
    position: absolute;
    top: 50%;
    transform: translate(20%, -70%)
}

.revenue-share-article {
    padding: 3em 0;
}

#join-us-link-xxl {
    display: block;
    width: 90%;
    background: orange;
    color: #000;
    font-size: 24pt;
    font-weight: 700;
    margin: auto;
    border-radius: 24px;
    text-decoration: none
}

.floating_buttons {
    position: fixed;
    display: flex;
    flex-direction: column;
    background-color: #000;
    top: 50%;
    right: 0;
    padding: 1rem;
    border-radius: 8px 0 0 8px;
    z-index: 1050
}

.floating_buttons a+a {
    margin-top: 1rem
}

</style>
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>