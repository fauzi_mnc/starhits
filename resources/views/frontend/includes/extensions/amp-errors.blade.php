<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<style amp-custom>
    {{ include"frontend/assets/css/bootstrap.css" }}
    
    body {
        margin: 0;
        font-family: Montserrat, sans-serif;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #212529;
        text-align: left;
        background-color: #fff;
    }

    .body {
        display: flex;
        flex-direction: column;
        max-width: 100vw;
        min-height: 100vh;
        overflow-x: hidden;
        overflow-y: auto;
        padding: 50px 0 0;
        margin: 0;
        position: relative
    }

    @media(min-width:992px) {
        .body {
            padding: 100px 0 0
        }
    }

    .content {
        flex: 1 0 auto
    }

    .footer {
        flex-shrink: 0;
        background-color: #000
    }
    .creator-details-assets-link{
        color:orange;
    }

    .creator-details-assets-link:hover{
        color:orange;
    }

    a:hover {
        text-decoration: none
    }

    [role=button] {
        cursor: pointer
    }

    .nav-sidebar {
        background: #fff;
        color: #232323;
        fill: #232323;
        text-transform: uppercase;
        letter-spacing: .18rem;
        font-size: .875rem;
        padding: 0
    }

    .nav-sidebar h6 {
        margin-top: 0;
        padding: 15px 20px;
        background-color: #ccc;
        text-align: center;
        font-size: 14px;
        margin-bottom: 0
    }

    .nav-sidebar h3 {
        padding-right: 35px
    }

    .nav-sidebar-close {
        position: absolute;
        right: 15px;
        top: 15px
    }

    .nav-sidebar ul {
        list-style: none;
        padding-left: 0;
        margin: 0;
        width: 300px
    }

    .nav-sidebar ul li {
        padding: 15px;
        line-height: 28px;
        border-bottom: 1px solid #dee2e6
    }

    .nav-sidebar ul li a {
        display: block
    }

    .nav-sidebar a:hover {
        text-decoration: underline;
        fill: #232323
    }

    .nav-sidebar a {
        color: #232323;
        text-transform: none;
        letter-spacing: normal
    }

    div[class*="-sidebar-mask"] {
        opacity: .8
    }

    [role=button]:focus {
        outline: 0
    }

    .header-nav {
        z-index: 1050
    }

    #target-element-left .nav-item {
        display: flex;
        align-items: center;
        padding: 1em
    }

    #target-element-left .header-menu-2:hover,
    .header-menu-1:hover {
        color: #f6c101;
        text-decoration: none
    }

    .header-menu-1 {
        color: rgba(0, 0, 0, .58);
        font-size: x-small
    }

    #target-element-left .header-menu-2 {
        color: #000;
        text-decoration: none;
        font-size: small
    }

    #target-element-left .header-menu-2,
    .group-menu a,
    .header-menu-1 {
        font-weight: 700
    }

    .group-menu {
        display: none;
        justify-content: space-evenly;
        background-color: #000;
        border-radius: 9999px;
        padding: .5em 1em;
        white-space: nowrap
    }

    @media(min-width:992px) {
        .group-menu {
            display: flex
        }
    }

    .group-menu a {
        color: #fff;
        font-size: small
    }

    .group-menu .delimiter {
        color: #fff;
        font-weight: 700;
        margin: 0 .5em
    }

    .group-menu a:hover {
        color: #f6c101;
        text-decoration: none
    }

    .fixed-top,
    .sticky-top {
        height: 50px
    }

    @media(min-width:992px) {

        .fixed-top,
        .sticky-top {
            height: 100px
        }
    }

    html {
        scroll-behavior: smooth;
        overflow-x: hidden;
        overflow-y: scroll
    }

    .scrolltop-wrap {
        box-sizing: border-box;
        position: absolute;
        top: 0;
        right: 1em;
        bottom: 0;
        pointer-events: none;
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
        width: 3rem
    }

    .scrolltop-wrap a {
        position: fixed;
        position: -webkit-sticky;
        position: sticky;
        top: -4rem;
        width: 3rem;
        height: 3rem;
        margin-bottom: -5rem;
        -webkit-transform: translateY(100vh);
        transform: translateY(100vh);
        -webkit-backface-visibility: hidden;
        backface-visibility: hidden;
        display: inline-block;
        text-decoration: none;
        -webkit-user-select: auto;
        -moz-user-select: auto;
        -ms-user-select: auto;
        user-select: auto;
        pointer-events: all;
        outline: 0;
        overflow: hidden;
        font-size: 2rem;
        color: orange;
        writing-mode: vertical-lr;
        text-align: center;
        background-color: rgba(0, 123, 255, .25);
        border-radius: 10%
    }

    /*!
    * Custom page styles
    */
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
        font-weight: 900
    }

    .font-weight-900 {
        font-weight: 900
    }

    .font-style-i {
        font-style: italic
    }

    @media (min-width: 768px) {
        .font-size-xl {
            font-size: 8vw
        }
        
        .font-size-md {
            font-size: 4vw
        }
    }

    .text-outline {
        color: #fff;
        text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000
    }

    @supports((text-stroke:2px black) or (-webkit-text-stroke:2px black)) {
        .text-outline {
            color: transparent;
            -webkit-text-stroke: 2px #000;
            text-stroke: 2px #000;
            text-shadow: none
        }
    }

    #page-title {
        position: absolute;
        bottom: 0;
        left: 0;
        right: 0;
        background: #ffffff;
        opacity: 0.9;
        padding: 20px 0;
    }

    #page-title a {
        display: inline-block;
        line-height: 55px;
        width: 50px;
        height: 50px;
        border: 1px solid;
        border-radius: 50%
    }

    #page-title a+a {
        margin-left: 1em
    }

    .d-columns-2 {
        column-count: 2
    }

    #see-portofolio-link::before {
        content: ' ';
        border: 1px solid;
        border-radius: 50%;
        width: 1rem;
        height: 1rem;
        display: inline-block;
        margin-right: .5em;
        transform: translateY(20%)
    }

    #see-portofolio-link:hover {
        color: #a9a9a9
    }

    #see-portofolio-link {
        font-weight: 700;
        text-decoration: none;
        color: #000
    }

    #orange-box {
        background-color: orange;
        margin: 0 -15px;
        padding: 15% 0;
        position: relative;
        -webkit-clip-path: polygon(0 calc(100vw * .161), 100% 0, 100% calc(100% - (100vw * .161)), 0 100%);
        clip-path: polygon(0 calc(100vw * .161), 100% 0, 100% calc(100% - (100vw * .161)), 0 100%)
    }

    @media(min-width:1200px) {
        #orange-box {
            margin: -125px -15px 0
        }
    }

    .link-xl {
        border: 1px solid #fff;
        color: #000;
        font-size: 10pt;
        font-weight: 700;
        border-radius: 24px;
        text-decoration: none;
        padding: 1em 3em
    }

    .center-both {
        position: absolute;
        top: 50%;
        left: 50%;
        margin-right: -50%;
        transform: translate(-50%, -50%)
    }

    .card:hover amp-img {
        box-shadow: 0 0 8px 4px #fff
    }

    .card:hover .card-img-overlay {
        opacity: 1
    }

    #row-latest-video .card:hover {
        transform: scale(1.02)
    }

    .i-amphtml-scrollable-carousel-container {
        overflow-x: hidden!important;
        overflow-y: hidden!important;
    }

    .card-img-overlay {
        background-color: rgba(255, 156, 0, .7);
        display: flex;
        flex-direction: column;
        justify-content: flex-end;
        opacity: 0;
        max-width: 100%;
        width: 100%;
        transition: all 2s;
        white-space: break-spaces
    }

    .caption{
        padding:0;
        word-wrap: break-word;
        overflow-wrap: break-word;
        white-space: -moz-pre-wrap;
        /* Mozilla */
        white-space: -hp-pre-wrap;
        /* HP printers */
        white-space: -o-pre-wrap;
        /* Opera 7 */
        white-space: -pre-wrap;
        /* Opera 4-6 */
        white-space: pre-wrap;
        /* CSS 3 (and 2.1 as well, actually) */
        word-wrap: break-word;
        /* IE */
        word-break: break-all;
        width: 340px;
    }
    .caption h6{
        height:50px;
        margin-top:-30px;
    }

    .caption p{
        height:50px;
        margin-top:-30px;
    }

    #row-latest-video .card-img-overlay {
        -webkit-clip-path: polygon(0 0, 50% 0, 85% 100%, 0 100%);
        clip-path: polygon(0 0, 50% 0, 85% 100%, 0 100%)
    }

    #row-latest-series .card-img-overlay {
        clip-path: polygon(0 10%, 100% 25%, 100% 100%, 0 100%)
    }

    amp-fit-text {
        height: auto;
        white-space: normal
    }

    [role=button] {
        cursor: pointer
    }

    .view-videos-link::before {
        content: ' ';
        border: 1px solid;
        border-radius: 50%;
        width: 1rem;
        height: 1rem;
        display: inline-block;
        margin-right: .5em;
        transform: translateY(20%)
    }

    .view-videos-link {
        color: #000000;
        text-decoration: none
    }

    @media (min-width: 768px) {
        .text-md-right {
            text-align: right
        }
    }

    .content .not-found {
        margin-top: 20px;
    }

    .content .not-found h1 {
        color: #666666;
        font-size: 100px;
        font-weight: bold;
    }
</style>