<div class="footer text-white">
        <div class="row">
            <div class="col-lg-5 small">
                <amp-img src="{{asset('frontend/assets/images/logo-light.png')}}" width="125" height="43"></amp-img>
                <p class="middle">Star Hits is Digital media company that provides services, in terms of creating, distributing, managing, monetization of content.</p>
            </div>
            <div class="col-lg-2 text-center py-4">
                <amp-img src="{{asset('frontend/assets/icon/icon-youtube-certified.png')}}" width="103" height="63"></amp-img>
            </div>
            <div class="col-lg-5">
                <p class="text-center font-weight-bold">Subcribe our letter :</p>
                <div class="input-group input-group-sm mb-3">
                    <input type="email" class="form-control">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button">Send</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 text-center my-2">
                        <span class="px-0">
                            <a href="{{$SiteConfig['configs']['website_url_youtube']}}">
                                <amp-layout layout="fixed" width="24" height="24" class="social-footer">
                                    <amp-img layout="responsive" width="1" height="1" src="{{asset('frontend/assets/svgs/youtube.svg')}}">
                                    </amp-img>
                                </amp-layout>
                            </a>
                        </span>
                        <span class="px-3">
                            <a href="{{$SiteConfig['configs']['website_url_facebook']}}">
                                <amp-layout layout="fixed" width="24" height="24" class="social-footer">
                                    <amp-img layout="responsive" width="1" height="1"
                                        src="{{asset('frontend/assets/svgs/facebook-f.svg')}}"></amp-img>
                                </amp-layout>
                            </a>
                        </span>
                        <span class="px-3">
                            <a href="{{$SiteConfig['configs']['website_url_twitter']}}">
                                <amp-layout layout="fixed" width="24" height="24" class="social-footer">
                                    <amp-img layout="responsive" width="1" height="1" src="{{asset('frontend/assets/svgs/twitter.svg')}}">
                                    </amp-img>
                                </amp-layout>
                            </a>
                        </span>
                        <span class="px-3">
                            <a href="{{$SiteConfig['configs']['website_url_instagram']}}">
                                <amp-layout layout="fixed" width="24" height="24" class="social-footer">
                                    <amp-img layout="responsive" width="1" height="1" src="{{asset('frontend/assets/svgs/instagram.svg')}}">
                                    </amp-img>
                                </amp-layout>
                            </a>
                        </span>
                    </div>
                    <div class="col-md-6 text-center my-2">
                        <span class="small" style="vertical-align:top;position:absolute;left:38%;font-size:9px;"> Supported By </span>
                        <amp-img src="{{asset('frontend/assets/icon/icon-mncplay-white.png')}}" width="165" height="47"></amp-img>
                    </div>
                </div>
            </div>
        </div>

        <div class="row border-top mt-3" id="copyrights">
            <div class="col-lg-6 small d-flex flex-column flex-lg-row align-items-center justify-content-lg-between">
                <a class="text-white text-decoration-none py-1">COPYRIGHT &COPY; 2019 STARHITS</a>
                <a class="text-white text-decoration-none py-1" href="{{ route('contact') }}">CONTACT</a>
                <a class="text-white text-decoration-none py-1" href="{{ route('faq') }}">FAQ</a>
                <a class="text-white text-decoration-none py-1" href="{{ route('policy') }}">PRIVACY POLICY</a>
                <a class="text-white text-decoration-none py-1" href="{{ route('term') }}">TERM OF SERVICE</a>
            </div>

            <div class="col-lg-6 small d-flex flex-column flex-lg-row align-items-center justify-content-lg-between">
                <div class="mnc font-weight-bold small py-2 text-right" style="flex: 1 1 auto">
                    SOUTHEAST ASIA'S LARGEST AND MOST INTEGRATED MEDIA GROUP 
                </div>
                <div class="ml-lg-auto text-center px-2">
                    <amp-img src="{{asset('frontend/assets/img/mnc_media.png')}}" width="80" height="35"></amp-img>
                </div>
            </div>
        </div>
    </div>