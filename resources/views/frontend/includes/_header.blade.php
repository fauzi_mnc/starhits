<div class="d-none d-lg-flex w-100 header-nav fixed-top">
    <div class="d-flex align-items-center px-5">
        <a href="{{route('home')}}">
            {{-- <amp-img src="{{url($SiteConfig['configs']['website_logo_header'])}}" width="170" height="65"></amp-img> --}}
            <amp-img src="{{asset('frontend/assets/img/logo-home.png')}}" width="190" height="65"></amp-img>
        </a>
    </div>

    <div class="d-flex flex-column ml-auto">
        <div class="w-100">
            <div class="text-right">
                <a href="{{route('portfolio')}}" class="small px-2 header-menu-1">PORTOFOLIO</a>
                <a href="{{ route('contact') }}" class="small px-3 header-menu-1">CREATE PROJECT</a>
            </div>
        </div>
        <div class="w-100 text-nowrap">
            <div id="target-element-left"></div>
        </div>
    </div>
</div>

<div id="colHeaderContentBg" class="d-none d-lg-block fixed-top bg-white"></div>

<amp-animation id="showHeader" layout="nodisplay">
    <script type="application/json">
        {
            "duration": "100ms",
            "fill": "both",
            "iterations": "1",
            "direction": "alternate",
            "animations": [{
                "selector": "#colHeaderContentBg",
                "keyframes": [{
                    "visibility": "visible",
                    "opacity": "1"
                }]
            }]
        }
    </script>
</amp-animation>

<amp-animation id="hideHeader" layout="nodisplay">
    <script type="application/json">
        {
            "duration": "100ms",
            "fill": "both",
            "iterations": "1",
            "direction": "alternate",
            "animations": [{
                "selector": "#colHeaderContentBg",
                "keyframes": [{
                    "visibility": "hidden",
                    "opacity": "0"
                }]
            }]
        }
    </script>
</amp-animation>