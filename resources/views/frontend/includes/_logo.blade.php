<header class="d-flex align-items-center d-lg-none fixed-top bg-white p-3">
    <div role="button" tabindex="0" on="tap:sidebar-left.toggle">
        <span class="nav-menu-bar">☰</span>
    </div>
    <a href="{{route('home')}}" class="nav-menu-logo mx-auto pt-2"><amp-img src="{{asset('frontend/assets/images/logo.png')}}" width="102" height="35">
    </amp-img></a>
</header>