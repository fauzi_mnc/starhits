@push('meta')    
        <meta property="og:url" content="{{ url()->current() }}" />
        <meta property="og:type" content="#" />
        <meta property="og:title" content="@if(isset($models['website_title'])){{ $models['website_title'] }}@endif" />
        <meta property="og:description" content="@if(isset($models['website_slogan'])){{ $models['website_slogan'] }}@endif" />
        <meta property="og:image" content="{{url($models['website_logo_header'])}}" />
        <meta property="fb:app_id" content="952765028218113" />
@endpush
<!-- Stored in resources/views/child.blade.php -->
@extends('layouts.frontend')

@section('title', 'Contact us')

@section('content')

<div class="container-fluid contact">
    <div class="row">
        <div class="content-text">
            <div class="col-sm-12">
                <div class="title">
                    <h7>
                        CONTACT US
                    </h7>
                </div>
                <div class="inquiry-form">
                    <div class="col-sm-6 left-side">
                        <h3 class="head">
                            What can we help?
                        </h3>
                        <p class="info">
                            @if(isset($models['website_title_contact']))
                                {{ $models['website_title_contact'] }}
                            @endif
                        </p>
                        <h7 class="info-name">
                            ADDRESS
                        </h7>
                        <h6 class="info-details">
                            @if(isset($models['website_address']))
                                {!! $models['website_address'] !!}
                            @endif
                        </h6>
                        <br>
                            <h7 class="info-name">
                                PHONE
                            </h7>
                            <h6 class="info-details">
                                @if(isset($models['website_phone']))
                                    {{ $models['website_phone'] }}
                                @endif
                            </h6>
                            <br>
                                <h7 class="info-name">
                                    EMAIL
                                </h7>
                                <h6 class="info-details">
                                    @if(isset($models['website_email']))
                                        {{ $models['website_email'] }}
                                    @endif
                                </h6>
                                {{ $errors->contact->first('name') }}
                                {{ $errors->contact->first('email') }}
                                <br>
                                <form action="/contact" method="post">
                                	{{ csrf_field() }}
                                    <div class="inquiry">
                                        <input class="inquiry-fname" id="fname" name="name" placeholder="    First Name" required="" style="font-family:Arial, FontAwesome" type="text">
                                            <input class="inquiry-lname" id="lname" name="last_name" placeholder="    Last Name" required="" style="font-family:Arial, FontAwesome" type="text">
                                                <input class="inquiry-email" id="email" name="email" placeholder="    Email" required="" style="font-family:Arial, FontAwesome" type="email">
                                                   <textarea class= "inquiry-message" name="message" placeholder=" Your message"></textarea>
                                                   @if(env('APP_ENV') != 'local') 
                                                   <div class="form-group">
                                                        {!! NoCaptcha::display() !!}
                                                    </div>
                                                    @endif
                                                    <button class="inquiry-send" type="submit">
                                                        SEND
                                                    </button>
                                                </input>
                                            </input>
                                        </input>
                                    </div>
                                </form>
                                </br>
                            </br>
                        </br>
                    </div>
                    <div class="col-sm-6 right-side">
                        <div id="map">
                            <script>
                                function initMap() {
                                        var uluru = {lat:-6.194810, lng:106.767260};
                                        var map = new google.maps.Map(document.getElementById('map'), {
                                        zoom: 15,
                                        center: uluru
                                        });
                                        var marker = new google.maps.Marker({
                                        position: uluru,
                                        map: map
                                        });
                                        }
                            </script>
                            <script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDSzCvXcZP3lvGxY7Wf5jKurZEot4tdWSU&callback=initMap">
                            </script>
                        </div>
                        <br>
                            <br>
                            </br>
                        </br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
