@extends('layouts.frontend')

@section('title', 'Portfolio') 

@section('content')
    <div class="col-content">
        <div class="row mb-5">
            <div class="col-xl-11 mx-auto">
                <amp-img src="{{ asset('frontend/assets/images/Layer-600.png') }}" width="1092" height="495" layout="responsive">
                </amp-img>

                <div id="page-title-portfolio">
                    <h1 class="font-style-i font-size-md font-weight-900 text-center text-outline-black">
                        STARHITS
                    </h1>
                    <h1 class="font-style-i font-size-md font-weight-900 text-center">
                        PORTOFOLIO
                    </h1>
                </div>
            </div>
        </div>

        {{-- <div class="row">
            <div class="col-xl-12 bg-light px-xl-5 py-3 mt-3" style="overflow-x: auto;">
                <amp-carousel id="brandsSlider" width="auto" height="40" layout="fixed-height" type="slides" autoplay delay="3000">
                    <div class="d-flex justify-content-around">
                        <amp-img src="{{ asset('frontend/assets/images/brand-001.png') }}" layout="fixed" width="80" height="40"></amp-img>
                        <amp-img src="{{ asset('frontend/assets/images/brand-002.png') }}" layout="fixed" width="80" height="40"></amp-img>
                        <amp-img src="{{ asset('frontend/assets/images/brand-003.png') }}" layout="fixed" width="80" height="40"></amp-img>
                        <amp-img src="{{ asset('frontend/assets/images/brand-004.png') }}" layout="fixed" width="80" height="40"></amp-img>
                        <amp-img src="{{ asset('frontend/assets/images/brand-005.png') }}" layout="fixed" width="80" height="40"></amp-img>
                    </div>
                                        
                    <div class="d-flex justify-content-around">
                        <amp-img src="{{ asset('frontend/assets/images/brand-006.png') }}" layout="fixed" width="80" height="40"></amp-img>
                        <amp-img src="{{ asset('frontend/assets/images/brand-007.png') }}" layout="fixed" width="80" height="40"></amp-img>
                        <amp-img src="{{ asset('frontend/assets/images/brand-008.png') }}" layout="fixed" width="80" height="40"></amp-img>
                        <amp-img src="{{ asset('frontend/assets/images/brand-009.png') }}" layout="fixed" width="80" height="40"></amp-img>
                        <amp-img src="{{ asset('frontend/assets/images/brand-010.png') }}" layout="fixed" width="80" height="40"></amp-img>
                    </div>
                </amp-carousel>
            </div>
        </div> --}}

        <div class="row">
            <div class="col-xl-10 mx-auto">
                <div>
                    <amp-selector id="myTabsPortfolio" class="tabs-with-flex" role="tablist">
                        <div id="tab1" role="tab" aria-controls="tabpanel1" option selected>DIGITAL CAMPAIGN
                        </div>
                        <div id="tabpanel1" role="tabpanel" aria-labelledby="tab1">
                            <amp-list id="portfolioList" class="grid" reset-on-refresh single-item
                                layout="fixed-height" width="auto" height="600"
                                src="https://starhits.id/api/portfolio/digital" binding="refresh"
                                load-more="manual" load-more-bookmark="next">
                                <template type="amp-mustache">
                                    <a class="grid-item text-dark text-decoration-none" href="{{route('portfolio')}}/@{{id_portofolio}}">
                                        <div class="container-responsive">
                                            <div class="container-absolute">
                                                <div class="grid__img">
                                                    <amp-img class="grid__img__main" src="@{{porto_img1}}"
                                                        layout="responsive" width="300" height="300">
                                                    </amp-img>

                                                    <div style="margin-top: 1.5rem">
                                                        <h2 class="grid__img__brand">@{{porto_title}}</h2>
                                                    </div>
                                                    <div class="description text-center">
                                                        <div><strong>@{{porto_content}}</strong></div>

                                                        <amp-fit-text width="258" height="50" layout="fixed" max-font-size="10" style="font-size:11px;">
                                                            @{{content}}
                                                        </amp-fit-text>

                                                        <div class="description-with-bg">
                                                            <div><strong>@{{campaign_title}}</strong></div>
                                                            <amp-fit-text width="226" height="48" layout="fixed" max-font-size="10" style="font-size:11px;">
                                                            @{{campaign_content}}
                                                        </amp-fit-text>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </template>
                                <div overflow role="button" aria-label="SHOW MORE" class="text-right pt-5">
                                    <span class="load-more-link">SHOW MORE</span>
                                </div>
                                <div fallback>FALLBACK</div>
                                <div placeholder>PLACEHOLDER</div>
                                <amp-list-load-more load-more-failed>ERROR</amp-list-load-more>
                                <amp-list-load-more load-more-end></amp-list-load-more>
                                <amp-list-load-more load-more-button>
                                    <div class="amp-load-more" style="margin-bottom:3rem">
                                        <button class="load-more-link"><label>LOAD MORE</label></button>
                                    </div>
                                </amp-list-load-more>
                            </amp-list>
                        </div>

                        <div id="tab2" role="tab" aria-controls="tabpanel2" option>PRODUCTION SERVICE</div>
                        <div id="tabpanel2" role="tabpanel" aria-labelledby="tab2">
                            <amp-list id="portfolioList" class="grid" reset-on-refresh single-item
                                layout="fixed-height" width="auto" height="600"
                                src="https://starhits.id/api/portfolio/production" binding="refresh"
                                load-more="manual" load-more-bookmark="next">
                                <template type="amp-mustache">
                                    <a class="grid-item text-dark text-decoration-none" href="{{route('portfolio')}}/@{{id_portofolio}}">
                                        <div class="container-responsive">
                                            <div class="container-absolute">
                                                <div class="grid__img">
                                                    <amp-img class="grid__img__main" src="@{{porto_img1}}"
                                                        layout="responsive" width="300" height="300">
                                                    </amp-img>

                                                    <div style="margin-top: 1.5rem">
                                                        <h2 class="grid__img__brand">@{{porto_title}}</h2>
                                                    </div>
                                                    <div class="description text-center">
                                                        <div><strong>@{{porto_content}}</strong></div>

                                                        <amp-fit-text width="258" height="50" layout="fixed" max-font-size="10" style="font-size:11px;">
                                                            @{{content}}
                                                        </amp-fit-text>

                                                        <div class="description-with-bg">
                                                            <div><strong>@{{campaign_title}}</strong></div>
                                                            <amp-fit-text width="226" height="48" layout="fixed" max-font-size="10" style="font-size:11px;">
                                                            @{{campaign_content}}
                                                        </amp-fit-text>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </template>
                                <div overflow role="button" aria-label="SHOW MORE" class="text-right pt-5">
                                    <span class="load-more-link">SHOW MORE</span>
                                </div>
                                <div fallback>FALLBACK</div>
                                <div placeholder>PLACEHOLDER</div>
                                <amp-list-load-more load-more-failed>ERROR</amp-list-load-more>
                                <amp-list-load-more load-more-end></amp-list-load-more>
                                <amp-list-load-more load-more-button>
                                    <div class="amp-load-more" style="margin-bottom:3rem">
                                        <button class="load-more-link"><label>LOAD MORE</label></button>
                                    </div>
                                </amp-list-load-more>
                            </amp-list>
                        </div>

                    </amp-selector>
                </div>
                <div class="text-center" style="margin-bottom:5rem">
                    <a href="#" class="create-project-link">CREATE PROJECT</a>
                </div>
            </div>
        </div>
    </div>
@endsection