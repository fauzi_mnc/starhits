@extends('layouts.frontend')

@section('title', strtolower($datas->porto_title))

@section('content')
    <div class="col-content">
        <div class="row">
            <div class="col-xl-9 mx-auto">
                <div id="picture-container-0">
                    <amp-img layout="responsive" width="434" height="129"
                        src="{{ asset($datas->porto_header) }}"></amp-img>
                </div>

                <div id="picture-container-1" class="mt-3">
                    <h1 class="title-our-business text-center">{{$datas->porto_title}}</h1>
                </div>

                <div
                    style="display:block;width:2px;height:200px;background-color:darkslategray;margin:50px auto">
                </div>

                <h1 class="font-weight-900 text-center font-size-md"><span class="">{{$datas->porto_content}}</span></h1>
                <div style="padding-bottom: 50px">
                    {{ strip_tags($datas->content1) }}
                </div>

                <h1 class="font-weight-900 text-center font-size-md"><span class="">{{$datas->campaign_title}}</span></h1>
                <div style="padding-bottom: 50px">
                    {{ strip_tags($datas->campaign_content) }}
                </div>

                <div id="picture-container-2">
                    <div>
                        <div
                            style="width:50px;height:92px;position: absolute;top:50%;transform: translateY(-50%);">
                            <amp-img src="{{ asset('frontend/assets/images/bg-triangle.png') }}" width="50" height="92">
                            </amp-img>
                        </div>

                        <div
                            style="width: 35vw;height:35vw;position: absolute;top:50%;right:0;transform: translate(0, -50%)">
                            <amp-img src="{{ asset('frontend/assets/images/bg-circle.png') }}" width="473" height="474"
                                layout="responsive" style="transform: rotate(45deg)">
                            </amp-img>
                        </div>

                        <div data-z-index="1">
                            <div>
                                <amp-img layout="responsive" width="436" height="547"
                                    src="{{ asset($datas->porto_img1) }}"></amp-img>
                            </div>
                        </div>
                        <div data-z-index="2">
                            <div>
                                <amp-img layout="responsive" width="310" height="464"
                                    src="{{ asset($datas->porto_img2) }}"></amp-img>
                            </div>
                        </div>
                    </div>
                </div>

                <div style="padding-bottom: 50px;margin-top: 100px;">
                    {{ strip_tags($datas->isi_content) }}
                </div>


                <div id="video-container" class="col-md-8 mx-auto" style="padding-bottom: 5rem">
                    <amp-carousel width="358"
                        height="204"
                        layout="responsive"
                        type="slides">
                        @foreach($media_content as $v)
                        <amp-youtube id="myLiveChannel" data-videoid="{{ $v }}" width="358" height="204" layout="responsive">
                            <amp-img src="https://i.ytimg.com/vi/{{ $v }}/maxresdefault_live.jpg" placeholder width="358" height="204" layout="responsive"/>
                        </amp-youtube>
                        @endforeach
                    </amp-carousel>
                </div>

                <h1 class="font-weight-900 text-center font-size-md"><span
                        class="text-outline-black">THE</span> RESULTS</h1>

                <div
                    style="display:block;width:2px;height:100px;background-color:darkslategray;margin:50px auto">
                </div>

                <div id="counter">
                    <div>
                        <h4>Views</h4>
                        <h1 class="font-size-md">{{ bd_nice_numbers($datas->porto_views) }}</h1>
                    </div>
                    <div>
                        <h4>Engagement</h4>
                        <h1 class="font-size-md">{{ bd_nice_numbers($datas->porto_engagement) }}</h1>
                    </div>
                    <div>
                        <h4>Performance</h4>
                        <h1 class="font-size-md">{{ $datas->porto_performance }} %+</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection