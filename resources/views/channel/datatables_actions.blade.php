{!! Form::open(['route' => ['channel.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('channel.show', $id) }}" class='btn btn-default btn-xs' title="Show Channel">
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="{{ route('channel.edit', $id) }}" class='btn btn-default btn-xs' title="Edit Channel">
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    @if($is_active == 1)
    {!! Form::button('<i class="glyphicon glyphicon-ban-circle"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'title' => 'Inactive Channel',
        'onclick' => "return confirm('Do you want to inactive this channel?')",
        'name' => 'action',
        'value' => 'inact'
    ]) !!}
    @else
    {!! Form::button('<i class="glyphicon glyphicon-ok"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-success btn-xs',
        'title' => 'Activated Channel',
        'onclick' => "return confirm('Do you want to activated this channel?')",
        'name' => 'action',
        'value' => 'act'
    ]) !!}
    @endif
</div>
{!! Form::close() !!}