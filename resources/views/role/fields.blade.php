
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css">
@include('layouts.datatables_css')
<style>
    .multiselect-container {
        height: 200px;
        overflow-y: auto;
    }
</style>
@endsection

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('display_name', 'Name:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('display_name', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => 191]) !!}
    </div>
</div>

<!-- bio Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => 191]) !!}
    </div>
</div>


<!-- role Field -->
<!-- <div class="form-group">
    {!! Form::label('permissions', 'Permissions:', ['class' => 'col-sm-2 control-label']) !!}
    
    <div class="col-sm-6">
        {!! Form::select('permissions[]', $permissions, null, ['class' => 'selectpicker form-control', 'id' => 'permissions', 'multiple' => 'multiple']) !!}
    </div>

    <div class="col-sm-4">
        <table class="table table-bordered" id="table-permission">
            <thead>
                <tr>
                    <th>Selected Permission</th>
                </tr>
            </thead>
            <tbody>
                @if($formType == 'edit')
                @foreach($role->permissions as $permission)
                <tr>
                    <td>{{$permission->display_name}}</td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div> -->


<!-- Submit Field -->
<div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('role.index') !!}" class="btn btn-default">Cancel</a>
    </div>
</div>

@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
@include('layouts.datatables_js')
<script>
    // var table = $("#table-permission").DataTable({"dom": 'tp'});

    // $('#permissions').multiselect({
    //     onChange: function(element, checked) {
    //         if(checked){
    //             table.row.add( [ element.html() ] )
    //             .draw()
    //             .node();
    //         } else {
    //             var indexes = table.rows().eq( 0 ).filter( function (rowIdx) {
    //                 return table.cell( rowIdx, 0 ).data() === element.html() ? true : false;
    //             });
                
    //             table.rows( indexes ).remove().draw();
    //         }
    //     }
    // }); 
</script>

@endsection