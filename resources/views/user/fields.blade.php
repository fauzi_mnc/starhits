
@section('css')
<link href="{{ asset('css/fileinput.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/css/bootstrap-multiselect.css">
@include('layouts.datatables_css')
<style>
.kv-photo .krajee-default.file-preview-frame,.kv-photo .krajee-default.file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-photo .file-input {
    display: table-cell;
    max-width: 220px;
}
.kv-reqd {
    color: red;
    font-family: monospace;
    font-weight: normal;
}
</style>
@include('layouts.datatables_css')
@endsection

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => 30]) !!}
    </div>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        {!! Form::text('email', null, ['class' => 'form-control', 'required' => 'required', 'maxlength' => 50]) !!}
    </div>
</div>

<!-- Password Field -->
<div class="form-group">
    {!! Form::label('password', 'Password:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-{{ $formType == 'edit' ? '8' : '10' }}">
        {!! Form::password('password', array('class' => 'form-control', 'required' => 'required', 'minlength' => 12)) !!}
    </div>
    @if($formType == 'edit')
    <div class="col-sm-2">
        <button type="button" id="reset-pwd" class="btn btn-default">Reset</button>
    </div>
    @endif
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
        <div class="kv-photo center-block text-center">
            <input id="photo" name="image" type="file" class="file-loading">
        </div>

        <div id="kv-photo-errors-1" class="center-block alert alert-block alert-danger" style="display:none"></div>
    </div>
</div>

<!-- bio Field -->
<div class="form-group">
    {!! Form::label('is_active', 'Is Active:', ['class' => 'col-sm-2 control-label']) !!}

    <div class="col-sm-10">
    {!! Form::select('is_active', ['1' => 'active', '0' => 'Inactive', '' => 'select'], null, ['class' => 'form-control']) !!}
    </div>
</div>

<!-- role Field -->
<div class="form-group">
    {!! Form::label('roles', 'Roles:', ['class' => 'col-sm-2 control-label']) !!}
    
    <div class="col-sm-6">
       {{-- {!! Form::select('roles[]', $roles, null, ['class' => 'selectpicker form-control', 'id' => 'roles', 'multiple' => 'multiple']) !!}
       --}}
        <select name="roles[]" id="roles"  class="form-control selectpicker" multiple="multiple">
            @foreach($roles as $val)
                @if(!empty($user))
                    @php
                        $access_role = App\Model\AccessRole::where([
                                        'user_id'=>$user->id,
                                        'role_id'=>$val->id])
                                        ->first();
                    @endphp
                @endif
                <option value="{{$val->id}}" <?php if(!empty($access_role)){echo"selected";} ?> > {{$val->display_name}} </option>

            @endforeach
        </select>
    </div>
    
    <!-- <div class="col-sm-4">
        <table class="table table-bordered" id="table-role">
            <thead>
                <tr>
                    <th>Selected Role</th>
                </tr>
            </thead>
            <tbody>
                @if($formType == 'edit')
                @foreach($get_role as $role)
                <tr>
                    <td>{{$role->display_name}}</td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div> -->
</div>

<!-- Submit Field -->
<div class="form-group">
    <div class="col-sm-4 col-sm-offset-2">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('user.index') !!}" class="btn btn-default">Cancel</a>
    </div>
</div>

@section('scripts')
<script src="{{ asset('js/plugins/bootstrap-fileinput/piexif.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/sortable.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/purify.min.js') }}"></script>
<script src="{{ asset('js/plugins/bootstrap-fileinput/fileinput.min.js') }}"></script>
<script>
    
    var formType = '{{ $formType }}';
    $('#photo').val('');

    $("#photo").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-photo-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="{{ ($formType == 'edit' && $user->image) ? asset($user->image) : asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "jpeg"]
    });

    $("#cover").fileinput({
        overwriteInitial: true,
        maxFileSize: 1500,
        showClose: false,
        showCaption: false,
        browseLabel: '',
        removeLabel: '',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
        removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
        removeTitle: 'Cancel or reset changes',
        elErrorContainer: '#kv-cover-errors-1',
        msgErrorClass: 'alert alert-block alert-danger',
        defaultPreviewContent: '<img src="{{ ($formType == 'edit' && $user->cover) ? asset($user->cover) : asset('img/default_avatar_male.jpg') }}" alt="Your icon" style="width:160px">',
        layoutTemplates: {main2: '{preview} {remove} {browse}'},
        allowedFileExtensions: ["jpg", "png", "jpeg"]
    });
    
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.15/js/bootstrap-multiselect.js"></script>
@include('layouts.datatables_js')
<script>
    var table = $("#table-role").DataTable({"dom": 'tp'});
    @if($formType == 'edit')
    $('#password').attr('disabled', true);
    $('#reset-pwd').click(function(){
        $('#password').val('');
        $('#password').attr('disabled', false);
    });
    @endif
    $('#roles').multiselect({
        onChange: function(element, checked) {

            console.log(element);

            if(checked){
                table.row.add( [ element.html() ] )
                .draw()
                .node();
            } else {
                var indexes = table.rows().eq( 0 ).filter( function (rowIdx) {
                    return table.cell( rowIdx, 0 ).data() === element.html() ? true : false;
                });
                
                table.rows( indexes ).remove().draw();
            }
        }
    }); 
</script>

@endsection